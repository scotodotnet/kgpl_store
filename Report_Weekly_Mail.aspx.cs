﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Net.Mail;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Reports_Report_Weekly_Mail : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SSQL = "";
    ReportDocument RD_Report = new ReportDocument();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    bool Errflag;
    string FromDate = ""; string ToDate = "";
    DateTime frmDate_Date; DateTime toDate_Date;
    DataTable AutoDataTable = new DataTable();
    DataTable GenReceiptDT = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = "ATM";
        SessionLcode = "UNIT I";
        SessionFinYearCode = "1";
        SessionFinYearVal = "2016_2017";
        DateTime Date1 = DateTime.Now.AddDays(-7);
        DateTime Date2 = DateTime.Now.AddDays(-1);
        FromDate = Convert.ToString(Date1.ToString("dd/MM/yyyy"));
        ToDate = Convert.ToString(Date2.ToString("dd/MM/yyyy"));
        //FromDate = "01/02/2017";
        //ToDate = "28/02/2017";
        ClosingStock();

    }

    public void ClosingStock()
    {
        DataTable StockDT = new DataTable();
        DataTable Department_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable Purchase_DT = new DataTable();
        DataTable Issue_DT = new DataTable();

        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("Opening_Val");
        AutoDataTable.Columns.Add("PO_Val");
        AutoDataTable.Columns.Add("Issue_Val");


        if (FromDate != "")
        {
            frmDate_Date = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate_Date = Convert.ToDateTime(ToDate);
        }

        string[] finyear = SessionFinYearVal.Split('_');
        string FromDate_Chk = "01/04/" + finyear[0];
        DateTime fromDate_Chk = Convert.ToDateTime(FromDate_Chk);


        string[] FromDate_ChkStr = FromDate.Split('/');


        if (FromDate == FromDate_Chk)
        {
            SSQL = "Delete  Closing_Stock_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select Distinct DeptName from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int i = 0; i < Department_DT.Rows.Count; i++)
            {

                SSQL = "Select sum(Open_Value) as Open_Value,sum(Open_Qty) as OP_Qty from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + "And DeptName='" + Department_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + "And Trans_Type='OPENING RECEIPT'";
                //SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";

                Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "select sum(General_Value)+sum(PO_Value) +sum(Issue_Return_Value) as PO_Val,(Sum(General_Qty) + sum(PO_Qty)) as Pur_Qty";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And DeptName='" + Department_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate_Date.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate_Date.ToString("dd/MM/yyyy") + "',103)";

                Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "select sum(Issue_Value) as Issue_Val,sum(Issue_Qty) as Issue_Qty";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And DeptName='" + Department_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate_Date.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate_Date.ToString("dd/MM/yyyy") + "',103)";

                Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);



                SSQL = "Insert Into Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,DeptName,Opening_Val,OP_Qty,PO_Val,Pur_Qty,";
                SSQL = SSQL + "Issue_Val,Issue_Qty,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Department_DT.Rows[i]["DeptName"].ToString() + "',";
                if (Open_Stck.Rows.Count != 0)
                {
                    if (Open_Stck.Rows[0]["Open_Value"].ToString() != "")
                    {
                        SSQL = SSQL + " '" + Open_Stck.Rows[0]["Open_Value"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + " '0.00',";
                    }
                    if (Open_Stck.Rows[0]["OP_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + " '" + Open_Stck.Rows[0]["OP_Qty"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + " '0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + " '0.00','0.00',";
                }

                if (Purchase_DT.Rows.Count != 0)
                {
                    if (Purchase_DT.Rows[0]["PO_Val"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Purchase_DT.Rows[0]["PO_Val"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00',";
                    }
                    if (Purchase_DT.Rows[0]["Pur_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Pur_Qty"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }
                if (Issue_DT.Rows.Count != 0)
                {
                    if (Issue_DT.Rows[0]["Issue_Val"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Issue_DT.Rows[0]["Issue_Val"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00',";
                    }
                    if (Issue_DT.Rows[0]["Issue_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Issue_DT.Rows[0]["Issue_Qty"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }


                SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

            }

            //Get Closing Stock
            SSQL = "Select * from Closing_Stock_Print where";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate_Date.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate_Date.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " order by DeptName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(GenReceiptDT);
                RD_Report.Load(Server.MapPath("~/crystal/Weekly_Mail_ClosingStock.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add
                DataTable dtdCompanyAddress = new DataTable();
                DataTable Close_Det = new DataTable();

                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                //RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                
                //Sent Mail Code Start
                MailMessage mail = new MailMessage();
                mail.To.Add("vijayamurtha@gmail.com");
                mail.Bcc.Add("padmanabhanscoto@gmail.com");
                mail.From = new MailAddress("aatm2005@gmail.com");
                mail.Subject = "Stores Weekly Closing Stock Details";
                mail.Body = "Find the attachment for Stores Weekly Closing Stock Details";
                mail.IsBodyHtml = true;

                //mail.Attachments.Add(new Attachment(ms, "DayWise1.rpt", "crystal/DayWise1.rpt"));
                mail.Attachments.Add(new Attachment(RD_Report.ExportToStream(ExportFormatType.PortableDocFormat), "Weekly_Closing_Stock.pdf"));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
                smtp.Credentials = new System.Net.NetworkCredential("aatm2005@gmail.com", "Altius2005");  //Or your Smtp Email ID and Password
                smtp.EnableSsl = true;
                smtp.Send(mail);
                //Sent Mail Code End
                lblUploadSuccessfully.Text = "Mail Sent Successfully";
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }




        }
        else
        {

            SSQL = "Delete  Closing_Stock_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select Distinct DeptName from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            for (int i = 0; i < Department_DT.Rows.Count; i++)
            {

                SSQL = "select ((sum(Open_Value)+sum(General_Value)+sum(PO_Value) +sum(Issue_Return_Value))-(sum(Issue_Value))) as Open_Value,";
                SSQL = SSQL + " ((sum(Open_Qty)+sum(General_Qty)+sum(PO_Qty))-(sum(Issue_Qty))) as OP_Qty";
                SSQL = SSQL + " from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And DeptName='" + Department_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + frmDate_Date.AddDays(-1).ToString("dd/MM/yyyy") + "',103)";

                Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);


                SSQL = "select sum(General_Value)+sum(PO_Value) +sum(Issue_Return_Value) as PO_Val,";
                SSQL = SSQL + " (Sum(General_Qty) + sum(PO_Qty)) as Pur_Qty";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And DeptName='" + Department_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate_Date.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate_Date.ToString("dd/MM/yyyy") + "',103)";

                Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "select sum(Issue_Value) as Issue_Val,sum(Issue_Qty) as Issue_Qty";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And DeptName='" + Department_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate_Date.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate_Date.ToString("dd/MM/yyyy") + "',103)";

                Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);



                SSQL = "Insert Into Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,DeptName,Opening_Val,OP_Qty,PO_Val,Pur_Qty,";
                SSQL = SSQL + "Issue_Val,Issue_Qty,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Department_DT.Rows[i]["DeptName"].ToString() + "',";
                if (Open_Stck.Rows.Count != 0)
                {
                    if (Open_Stck.Rows[0]["Open_Value"].ToString() != "")
                    {
                        SSQL = SSQL + " '" + Open_Stck.Rows[0]["Open_Value"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00',";
                    }
                    if (Open_Stck.Rows[0]["OP_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + " '" + Open_Stck.Rows[0]["OP_Qty"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + " '0.00','0.00',";
                }

                if (Purchase_DT.Rows.Count != 0)
                {
                    if (Purchase_DT.Rows[0]["PO_Val"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Purchase_DT.Rows[0]["PO_Val"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00',";
                    }
                    if (Purchase_DT.Rows[0]["Pur_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Purchase_DT.Rows[0]["Pur_Qty"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }
                if (Issue_DT.Rows.Count != 0)
                {
                    if (Issue_DT.Rows[0]["Issue_Val"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Issue_DT.Rows[0]["Issue_Val"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00',";
                    }
                    if (Issue_DT.Rows[0]["Issue_Qty"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Issue_DT.Rows[0]["Issue_Qty"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00','0.00',";
                }


                SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);


            }


            //Get Closing Stock
            SSQL = "Select * from Closing_Stock_Print where";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate_Date.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate_Date.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " order by DeptName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(GenReceiptDT);
                RD_Report.Load(Server.MapPath("~/crystal/Weekly_Mail_ClosingStock.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add
                DataTable dtdCompanyAddress = new DataTable();
                DataTable Close_Det = new DataTable();

                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                //RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                //Sent Mail Code Start
                MailMessage mail = new MailMessage();
                mail.To.Add("sabapathy@scoto.in");
                mail.From = new MailAddress("aatm2005@gmail.com");
                mail.Subject = "Stores Weekly Closing Stock Details";
                mail.Body = "Find the attachment for Stores Weekly Closing Stock Details";
                mail.IsBodyHtml = true;

                //mail.Attachments.Add(new Attachment(ms, "DayWise1.rpt", "crystal/DayWise1.rpt"));
                mail.Attachments.Add(new Attachment(RD_Report.ExportToStream(ExportFormatType.PortableDocFormat), "Weekly_Closing_Stock.pdf"));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
                smtp.Credentials = new System.Net.NetworkCredential("aatm2005@gmail.com", "Altius2005");  //Or your Smtp Email ID and Password
                smtp.EnableSsl = true;
                smtp.Send(mail);
                //Sent Mail Code End
                lblUploadSuccessfully.Text = "Mail Sent Successfully";
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }


        }

    }
}
