﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Services;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class master_forms_get_company_list : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static List<string> GetCompCode(string empName)
    {
        BALDataAccess objdata = new BALDataAccess();
        List<string> Emp = new List<string>();
        string query = string.Format("SELECT Ccode FROM MstCompany WHERE Ccode LIKE '%{0}%'", empName);
        DataTable DT = new DataTable();
        DT = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 1; i <= DT.Rows.Count; i++)
        {
            Emp.Add(DT.Rows[i]["Ccode"].ToString());
        }

        //using (SqlConnection con = new SqlConnection(@"Data Source=.\sqlexpress;Initial Catalog=Test;Integrated Security=True"))
        //{
        //    using (SqlCommand cmd = new SqlCommand(query, con))
        //    {
        //        con.Open();
        //        SqlDataReader reader = cmd.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            Emp.Add(reader.GetString(0));
        //        }
        //    }
        //}
        return Emp;
    }
}
