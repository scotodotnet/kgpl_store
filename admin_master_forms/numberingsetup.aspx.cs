﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class admin_master_forms_numberingsetup : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Number Setup";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            li.Attributes.Add("class", "droplink active open");
            Form_Name_Load();
            Fin_Year_Code_Join();

            stock_Update();

        }
        

        Load_Data();

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;
        if (txtFormName.Text.ToString() == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Form Name..');", true);
        }

        //Check Prefix Already Save with Another Form
        query = "Select * from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FormName <> '" + txtFormName.Text + "' And Prefix='" + txtPrefix.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Prefix No Already Asign to Another Form..');", true);
        }

        //User Rights Check
        //bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "2", "Number Setup");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Number Setup Details..');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "2", "Number Setup");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Number Setup..');", true);
        //    }
        //}

        if (!ErrFlag)
        {
            query = "Select * from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FormName='" + txtFormName.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FormName='" + txtFormName.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            query = "Insert Into MstNumberingSetup(Ccode,Lcode,FormName,Prefix,StartNo,EndNo,Suffix,Username)";
            query = query + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtFormName.Text + "',";
            query = query + " '" + txtPrefix.Text + "','" + txtStartNo.Text + "','" + txtEndNo.Text + "',";
            query = query + "'" + txtSuffix.Text + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Number Setup Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Number Setup Details Updated Successfully');", true);
            }
            Load_Data();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtFormName.Text = "-Select-"; txtPrefix.Text = ""; txtStartNo.Text = "";
        txtEndNo.Text = "";
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FormName='" + txtFormName.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtPrefix.Text = DT.Rows[0]["Prefix"].ToString();
            txtStartNo.Text = DT.Rows[0]["StartNo"].ToString();
            txtEndNo.Text = DT.Rows[0]["EndNo"].ToString();
            //txtSuffix.Text = DT.Rows[0]["Suffix"].ToString();
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select FormName,Prefix,StartNo,EndNo,Suffix from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtFormName.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check
        bool ErrFlag = false;
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "2", "2", "Number Setup");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete Number Setup Details..');", true);
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FormName='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                query = "Delete from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FormName='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Number Setup Details Deleted Successfully');", true);
                Load_Data();
                Clear_All_Field();

            }
        }
    }

    private void Form_Name_Load()
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        txtFormName.Items.Clear();
        query = "Select * from MstFormName order by FormID Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtFormName.Items.Add("-Select-");
        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {
            txtFormName.Items.Add(Main_DT.Rows[i]["FormName"].ToString());
        }
    }
    protected void txtFormName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        if (txtFormName.Text != "-Select-")
        {
            query = "Select * from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FormName='" + txtFormName.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                txtPrefix.Text = DT.Rows[0]["Prefix"].ToString();
                txtStartNo.Text = DT.Rows[0]["StartNo"].ToString();
                txtEndNo.Text = DT.Rows[0]["EndNo"].ToString();
                //txtSuffix.Text = DT.Rows[0]["Suffix"].ToString();
            }
            else
            {
                txtPrefix.Text = ""; txtStartNo.Text = ""; txtEndNo.Text = "";
            }
        }
        else
        {
            txtPrefix.Text = ""; txtStartNo.Text = ""; txtEndNo.Text = "";
        }
    }

    private void Fin_Year_Code_Join()
    {
        string[] Fin_year_Split=SessionFinYearVal.Split('_');
        TransactionNoGenerate FinYearVal_RightLeft_Check = new TransactionNoGenerate();
        string Fin_year = "/" + FinYearVal_RightLeft_Check.RightVal(Fin_year_Split[0],2) + "-" + FinYearVal_RightLeft_Check.RightVal(Fin_year_Split[1],2);
        txtSuffix.Text = Fin_year;
    }
    private void stock_Update()
    {
        string query = "";
        DataTable Stock_DT = new DataTable();
        string Stck_Val = "";


        query = "Select Stock_Update from Stock_Update";
        Stock_DT =  objdata.RptEmployeeMultipleDetails(query);

        if (Stock_DT.Rows.Count != 0)
        {
            Stck_Val = Stock_DT.Rows[0]["Stock_Update"].ToString();
        }
        if (Stck_Val == "1")
        {
            btnStock_Update.Visible = true;
        }
        else
        {
            btnStock_Update.Visible = false;
        }

    }

    protected void btnStock_Update_Click(object sender, EventArgs e)
    {
       
        string query = "";
        DataTable Dept_DT = new DataTable();
        DataTable Stock_DT = new DataTable();
        DataTable StockChk_DT = new DataTable();
        DataTable Stock_Det = new DataTable();


        query = "select Distinct DeptName from Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "'";
        Dept_DT = objdata.RptEmployeeMultipleDetails(query);

        for (int i = 0; i < Dept_DT.Rows.Count; i++)
        {

            query = "select DeptCode,WarehouseCode,WarehouseName,ZoneName,BinName,ItemCode,ItemName from Stock_Ledger_All";
            query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "' And DeptName='" + Dept_DT.Rows[i]["DeptName"].ToString() + "'";
            Stock_Det = objdata.RptEmployeeMultipleDetails(query);

            for (int j = 0; j < Stock_Det.Rows.Count; j++)
            {
                query = "Select ItemName,(sum(Add_Qty)-sum(Minus_Qty)) as Stock_Qty,(sum(Add_Value)-sum(Minus_Value)) as Stock_Value from Stock_Ledger_All";
                query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "' And DeptName='" + Dept_DT.Rows[i]["DeptName"].ToString() + "'";
                query = query + " And ItemName='" + Stock_Det.Rows[j]["ItemName"].ToString() + "'";
                query = query + " group by ItemName";
                Stock_DT = objdata.RptEmployeeMultipleDetails(query);

                if (Stock_DT.Rows.Count != 0)
                {

                    query = "select *from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "' And ItemName='" + Stock_DT.Rows[0]["ItemName"].ToString() + "'";
                    StockChk_DT = objdata.RptEmployeeMultipleDetails(query);
                    if (StockChk_DT.Rows.Count != 0)
                    {
                        query = "Delete from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "' And ItemName='" + Stock_DT.Rows[0]["ItemName"].ToString() + "'";
                        objdata.RptEmployeeMultipleDetails(query);

                       // ErrFlag = false;

                        query = "insert into Stock_Current_All(Ccode,Lcode,FinYearCode,FinYearVal,ItemCode,ItemName,Stock_Qty,Stock_Value,DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,UserID,UserName)";
                        query = query + "Values ('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                        query = query + " '" + Stock_Det.Rows[j]["ItemCode"].ToString() + "','" + Stock_DT.Rows[0]["ItemName"].ToString() + "','" + Stock_DT.Rows[0]["Stock_Qty"].ToString() + "',";
                        query = query + " '" + Stock_DT.Rows[0]["Stock_Value"].ToString() + "','" + Stock_Det.Rows[j]["DeptCode"].ToString() + "','" + Dept_DT.Rows[i]["DeptName"].ToString() + "',";
                        query = query + " '" + Stock_Det.Rows[j]["WarehouseCode"].ToString() + "','" + Stock_Det.Rows[j]["WarehouseName"].ToString() + "','" + Stock_Det.Rows[j]["ZoneName"].ToString() + "',";
                        query = query + " '" + Stock_Det.Rows[j]["BinName"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";

                        objdata.RptEmployeeMultipleDetails(query);

                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Stock Details Updated Successfully');", true);
                       

                    }
                    else
                    {
                        //ErrFlag = false;

                        query = "insert into Stock_Current_All(Ccode,Lcode,FinYearCode,FinYearVal,ItemCode,ItemName,Stock_Qty,Stock_Value,DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,UserID,UserName)";
                        query = query + "Values ('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                        query = query + " '" + Stock_Det.Rows[j]["ItemCode"].ToString() + "','" + Stock_DT.Rows[0]["ItemName"].ToString() + "','" + Stock_DT.Rows[0]["Stock_Qty"].ToString() + "',";
                        query = query + " '" + Stock_DT.Rows[0]["Stock_Value"].ToString() + "','" + Stock_Det.Rows[j]["DeptCode"].ToString() + "','" + Dept_DT.Rows[i]["DeptName"].ToString() + "',";
                        query = query + " '" + Stock_Det.Rows[j]["WarehouseCode"].ToString() + "','" + Stock_Det.Rows[j]["WarehouseName"].ToString() + "','" + Stock_Det.Rows[j]["ZoneName"].ToString() + "',";
                        query = query + " '" + Stock_Det.Rows[j]["BinName"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";

                        objdata.RptEmployeeMultipleDetails(query);

                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Stock Details Updated Successfully');", true);
                       
                    }


                }
            }
        }



    }
}
