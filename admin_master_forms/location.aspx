﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="location.aspx.cs" Inherits="master_forms_location" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<link href="<%= ResolveUrl("assets/plugins/toastr/toastr.min.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("assets/js/master_list_jquery.min.js") %>'></script>
    <script src='<%= ResolveUrl("assets/js/master_list_jquery-ui.min.js") %>'></script>
    <link href="<%= ResolveUrl("assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>
        
<script type="text/javascript">

    function SaveRecord(groupName) {
            //Get control's values
            var Loc_Code = $.trim($('#<%=txtLocationCode.ClientID %>').val());
            var Loc_Name = $.trim($('#<%=txtLocationName.ClientID %>').val());
            var Add1 = $.trim($('#<%=txtAddress1.ClientID %>').val());
            var Add2 = $.trim($('#<%=txtAddress2.ClientID %>').val());
            var Loc_City = $.trim($('#<%=txtCity.ClientID %>').val());
            var Pin_Code = $.trim($('#<%=txtZipcode.ClientID %>').val());
            var Loc_Phone = $.trim($('#<%=txtPhone.ClientID %>').val());
            var Loc_Mobile = $.trim($('#<%=txtMobile.ClientID %>').val());
            var Loc_RegNo = $.trim($('#<%=txtReg_No.ClientID %>').val());

            if (typeof (Page_ClientValidate) == 'function') {
                Page_ClientValidate(groupName);
            }
            
            if (Page_IsValid) {
                //Jquery ajax call to server side method
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    //Url is the path of our web method (Page name/function name)
                    url: "location.aspx/Insert_Details",
                    //Pass paramenters to the server side function
                    data: "{'Loc_Code':'" + Loc_Code + "','Loc_Name':'" + Loc_Name + "','Add1':'" + Add1 + "','Add2':'" + Add2 + "','City':'" + Loc_City + "','PinCode':'" + Pin_Code + "','Phone':'" + Loc_Phone + "','Mobile':'" + Loc_Mobile + "','Reg_No':'" + Loc_RegNo + "'}",
                    success: function(response) {
                        //Success or failure message e.g. Record saved or not saved successfully
                        if (response.d == true) {
                            //Set message
                            ShowPopup("Saved");
                            //Reset controls
                            $("#<%=txtLocationCode.ClientID%>").val('');
                            $("#<%=txtLocationName.ClientID%>").val('');
                            $("#<%=txtAddress1.ClientID%>").val('');
                            $("#<%=txtAddress2.ClientID%>").val('');
                            $("#<%=txtCity.ClientID%>").val('');
                            $("#<%=txtZipcode.ClientID%>").val('');
                            $("#<%=txtPhone.ClientID%>").val('');
                            $("#<%=txtMobile.ClientID%>").val('');
                            $("#<%=txtReg_No.ClientID%>").val('');

                            $("#<%=btnSave.ClientID%>").val('Save');
                            $('#<%=txtLocationCode.ClientID %>').removeAttr("disabled");
                            
                            $("#<%=txtLocationCode.ClientID%>").focus();


                        }
                        else {
                            $('#dvResult').text("Record could't be saved");
                        }
                        //Fade Out to disappear message after 6 seconds
                        $('#dvResult').fadeOut(6000);
                    },
                    error: function(xhr, textStatus, error) {
                        //Show error message(if occured)
                        $('#dvResult').text("Error: " + error);
                    }
                });
            }
            else {
                  //Validation failure message
//                $('#dvResult').html('');
//                $('#dvResult').html(msg);
            }
            $('#dvResult').fadeIn();
        }
    </script>

<%--After Save Notification Start--%>
<script type="text/javascript">
    function ShowPopup(message) {
        $(function() {
            //Command: toastr["success"]("Have fun storming the castle!")

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-center",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            toastr.success('Location details saved successfully.');
        });
};
</script>
<%--After Save Notification End--%>

<%--Location Code Select List Script Saart--%>
    
    <script type="text/javascript">
        $(function () {
            initializer();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer);
        });
        function initializer() {
            $("#<%=txtLocationCode.ClientID %>").autocomplete({
                 
                 
                source: function (request, response) {
                
                        
                      $.ajax({
 
                          url: '<%=ResolveUrl("List_Service.asmx/GetLocation_Code") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) {
 
                              response($.map(data.d, function (item) {
 
                                  return {
                                      label: item.split('-')[0],
                                      val: item.split('-')[1],
                                  }
 
                              }))
 
                          },
 
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      $("#<%=hfLocationCode.ClientID %>").val(i.item.val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=hfLocationCode.ClientID %>").val(i.item.val);
 
                  }
 
              });
 
          }
      </script>
<%--Location Code Select List Script Saart--%>
  
<form id=Location_Form>
<%--<asp:scriptmanager id="ScriptManager1" runat="server" enablepartialrendering="true"> </asp:scriptmanager>--%>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <li><a href="../Dashboard.aspx">Home</a></li>
        <li><a href="#">Admin Master</a></li>
    </ol>
</div>
<div class="page-title">
    <div class="container">
        <h3>Location</h3>
    </div>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        
           <%-- <div class="panel-heading clearfix">
                <h3 class="panel-title">Location</h3>
            </div>--%>
            <div class="col-md-2"></div>
            <div class="col-md-8">
			<div class="panel panel-white">
				<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Main</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					
						<div class="form-group row">
							<label for="input-Default" class="col-sm-3 control-label">Location Code <span class="mandatory">*</span></label>
							<div class="col-sm-6">
                                <asp:TextBox ID="txtLocationCode" class="form-control" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="hfLocationCode" runat="server" />
                                <asp:RequiredFieldValidator ControlToValidate="txtLocationCode" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
							</div>
                            <asp:LinkButton ID="btnlocation_search" class="btn btn-primary btn-addon  btn-rounded btn-sm" runat="server" OnClick="btnlocation_search_Click"><i class="fa fa-search"></i>Search</asp:LinkButton>
                            <%--<asp:LinkButton ID="btnComCodeSearch" class="btn btn-primary btn-addon  btn-rounded btn-sm" runat="server" OnClick="btnComCodeSearch_Click"><i class="fa fa-search"></i>Search</asp:LinkButton>--%>
						</div>
						<div class="form-group row">
							<label for="input-Default" class="col-sm-3 control-label">Location Name <span class="mandatory">*</span></label>
							<div class="col-sm-9">
                                <asp:TextBox ID="txtLocationName" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtLocationName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
							</div>
						</div>
						<div class="form-group row">
							<label for="input-Default" class="col-sm-3 control-label">Location Address <span class="mandatory">*</span></label>
							<div class="col-sm-9">
	                            <asp:TextBox ID="txtAddress1" class="form-control" runat="server"></asp:TextBox>
	                            <asp:RequiredFieldValidator ControlToValidate="txtAddress1" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
	                            <br />
                                <asp:TextBox ID="txtAddress2" class="form-control" runat="server"></asp:TextBox><br />
                                
                                
							</div>
						</div>
						<div class="form-group row">
							<label for="input-Default" class="col-sm-3 control-label">City <span class="mandatory">*</span></label>
							<div class="col-sm-4">
                                <asp:TextBox ID="txtCity" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtCity" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
							</div>
							<label for="input-Default" class="col-sm-2 control-label">Zipcode <span class="mandatory">*</span></label>
							<div class="col-sm-3">
                                <asp:TextBox ID="txtZipcode" MaxLength="6" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtZipcode" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
							</div>
						</div>
						
						<div class="form-group row">
							<label for="input-Default" class="col-sm-3 control-label">Phone No</label>
							<div class="col-sm-4">
                                <asp:TextBox ID="txtPhone" class="form-control" runat="server"></asp:TextBox>
							</div>
							<label for="input-Default" class="col-sm-2 control-label">Mobile No</label>
							<div class="col-sm-3">
                                <asp:TextBox ID="txtMobile" class="form-control" runat="server"></asp:TextBox>
							</div>
						</div>
						
						<div class="form-group row">
							<label for="input-Default" class="col-sm-3 control-label">Registration No <span class="mandatory">*</span></label>
							<div class="col-sm-9">
	                            <asp:TextBox ID="txtReg_No" class="form-control" runat="server"></asp:TextBox>
	                            <asp:RequiredFieldValidator ControlToValidate="txtReg_No" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>                                
							</div>
						</div>
						
						<div class="txtcenter">
						    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save"  ValidationGroup="Validate_Field" OnClientClick="SaveRecord('Validate_Field');return false"/>
						    <asp:Button ID="BtnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
						</div>
						
						<div id="dvResult"></div>
                        
						
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-8 end -->
		    
		    <div class="col-md-2"></div>
		    
            
      
         
      </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->

</ContentTemplate>
</asp:UpdatePanel>
 
</form>
</asp:Content>

