﻿<%@ Page Language="C#" MasterPageFile="MainPage.master" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Dashboard" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <!-- Content Page Code Start Area -->

        <div class="page-breadcrumb">
            <ol class="breadcrumb container">
               <%-- <li><a href="#">Home</a></li>--%>
                <li><a href="Dashboard.aspx">Dashboard</a></li>
            </ol>
        </div>
        <div class="page-title">
            <div class="container">
                <h3>Dashboard</h3>
            </div>
        </div>
        <div id="main-wrapper" class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h4 class="panel-title">Department Stock</h4>
                        </div>
                        <div class="panel-body">
                            <div>
                                <asp:Chart ID="DeptChart" runat="server" Compression="100" 
                                    EnableViewState="True" Width="490px">
                                    <series>
                                        <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Pie"  Label="#VALX: #PERCENT"
                                            IsXValueIndexed="True">
                                        </asp:Series>
                                    </series>
                                    <chartareas>
                                        <asp:ChartArea Name="ChartArea1">
                                        </asp:ChartArea>
                                    </chartareas>
                                </asp:Chart>
                            </div>   
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h4 class="panel-title">TOP 10 Moving Item</h4>
                        </div>
                        <div class="panel-body">
                            <div>
                                <asp:Chart ID="FastMovingItemChart" runat="server" Compression="100" 
                                    EnableViewState="True" Width="530px">
                                    <series>
                                        <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Funnel"  Label="#VALX: #PERCENT"
                                            IsXValueIndexed="True">
                                        </asp:Series>
                                    </series>
                                    <chartareas>
                                        <asp:ChartArea Name="ChartArea1">
                                        </asp:ChartArea>
                                    </chartareas>
                                </asp:Chart>
                            </div>   
                        </div>
                    </div>
                </div>
            </div><!-- Row -->
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h4 class="panel-title">Purchase Department Costing</h4>
                        </div>
                        <div class="panel-body">
                            <div>
                                <div class="row">
                                    <div class="form-group col-md-4">
					                    <label for="exampleInputName">Month</label>
                                        <asp:DropDownList ID="txtMonth" runat="server" class="form-control">
                                            <asp:ListItem Value="--Select--" Text="--Select--"></asp:ListItem>
                                            <asp:ListItem Value="January" Text="January"></asp:ListItem>
                                            <asp:ListItem Value="February" Text="February"></asp:ListItem>
                                            <asp:ListItem Value="March" Text="March"></asp:ListItem>
                                            <asp:ListItem Value="April" Text="April"></asp:ListItem>
                                            <asp:ListItem Value="May" Text="May"></asp:ListItem>
                                            <asp:ListItem Value="June" Text="June"></asp:ListItem>
                                            <asp:ListItem Value="July" Text="July"></asp:ListItem>
                                            <asp:ListItem Value="August" Text="August"></asp:ListItem>
                                            <asp:ListItem Value="September" Text="September"></asp:ListItem>
                                            <asp:ListItem Value="October" Text="October"></asp:ListItem>
                                            <asp:ListItem Value="November" Text="November"></asp:ListItem>
                                            <asp:ListItem Value="December" Text="December"></asp:ListItem>
                                        </asp:DropDownList>
					                </div>
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Year<span class="mandatory">*</span></label>
                                        <asp:DropDownList ID="txtYear" runat="server" class="form-control"></asp:DropDownList>
					                </div>
					                <div class="form-group col-md-2">
					                    <br />
					                    <asp:Button ID="btnSearch" class="btn btn-success"  runat="server" Text="View" OnClick="btnSearch_Click"/>
					                </div>
                                </div>
                                <div>
                                    <asp:Chart ID="DeptPurchaseCostChart" runat="server" Compression="100" 
                                        EnableViewState="True" Width="490px">
                                        <series>
                                            <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Pie"  Label="#VALX : #VALY"
                                                IsXValueIndexed="True">
                                            </asp:Series>
                                        </series>
                                        <chartareas>
                                            <asp:ChartArea Name="ChartArea1">
                                            </asp:ChartArea>
                                        </chartareas>
                                    </asp:Chart>
                                </div>
                            </div>   
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h4 class="panel-title">TOP 10 Purchased Item</h4>
                        </div>
                        <div class="panel-body">
                            <div>
                                <asp:Chart ID="FastPurchaseItemChart" runat="server" Compression="100" 
                                    EnableViewState="True" Width="530px">
                                    <series>
                                        <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Funnel"  Label="#VALX : #VALY"
                                            IsXValueIndexed="True">
                                        </asp:Series>
                                    </series>
                                    <chartareas>
                                        <asp:ChartArea Name="ChartArea1">
                                        </asp:ChartArea>
                                    </chartareas>
                                </asp:Chart>
                            </div>   
                        </div>
                    </div>
                </div>
            </div><!-- Row -->
        </div><!-- Main Wrapper -->
        
</asp:Content>

