﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Inventory_Service_Entry_Main_Old : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SQL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Service Main";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Inventory"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Load_Data_Enquiry_Grid();
    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "NonStock Scrab");

        Session["Transaction_No"] = null;
        Response.Redirect("Service_Entry.aspx");

    }
    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string Enquiry_No_Str = e.CommandName.ToString();
        Session.Remove("Transaction_No");
        Session["Transaction_No"] = Enquiry_No_Str;
        Response.Redirect("Service_Entry.aspx");

    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {

        DataTable DT = new DataTable();
        SQL = "Select * from Service_Entry where Transaction_No='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(SQL);
        if (DT.Rows.Count != 0)
        {
            SQL = "Delete from Service_Entry where  Transaction_No='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Deleted Successfully');", true);


            Load_Data_Enquiry_Grid();

            Response.Redirect("Service_Entry_Main.aspx");
        }
    }
    private void Load_Data_Enquiry_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Transaction_No,Date,Item_Name from Service_Entry where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }
}
