﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Inventory_Adjustment_Stock_Entry : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionAdjustNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string query;
    DataTable Std_Pur_Main = new DataTable();
    DataTable Std_Pur_Sub = new DataTable();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Adjustment Stock Entry";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Inventory"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh_Material_Request_Item();
            Load_Data_Empty_AdjustmentBy();
            Load_Data_Empty_Dept();
            Load_Data_Empty_CostCenterName();

            if (Session["Adjust_No"] == null)
            {
                SessionAdjustNo = "";
            }
            else
            {
                SessionAdjustNo = Session["Adjust_No"].ToString();
                txtTransNo.Text = SessionAdjustNo;
                btnSearch_Click(sender, e);
            }
            
        }
        Load_OLD_data();
        
        
       
        Load_Data_Empty_ItemCode();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }

       

      
        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Adjustment Stock Entry");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Adjustment Stock Entry Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Adjustment Stock Entry");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Adjustment Stock Entry..');", true);
            }
        }
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Adjustment Stock Entry", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtTransNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            query = "Select * from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Adjustment_No='" + txtTransNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Adjustment_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Adjustment_Stock_Entry_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Adjustment_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into Adjustment_Stock_Entry_Main(Ccode,Lcode,FinYearCode,FinYearVal,Adjustment_No,Adjustmen_Date,AdjustmenBy,DeptCode,DeptName,CostCenterCode,CostCenterName,Remarks,UserID,UserName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "','" + txtAdjustmentBy.Value + "','" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "','" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "','" + txtRemarks.Text + "',";
            query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            //GatePass_IN_Main_Sub
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Adjustment_Stock_Entry_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Adjustment_No,Adjustmen_Date,ItemCode,ItemName,StockQty,StockValue,WarehouseCode,WarehouseName,ZoneName,BinName,AdjustQty,AdjustValue,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["StockQty"].ToString() + "','" + dt.Rows[i]["StockValue"].ToString() + "','" + dt.Rows[i]["WarehouseCode"].ToString() + "','" + dt.Rows[i]["WarehouseName"].ToString() + "','" + dt.Rows[i]["ZoneName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["AdjustQty"].ToString() + "','" + dt.Rows[i]["AdjustValue"].ToString() + "',";
                query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Adjustment Stock Entry Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Adjustment Stock Entry Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Adjust_No"] = txtTransNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {

        txtTransNo.Text = ""; txtDate.Text = ""; txtAdjustmentBy.Value = "-Select-";
        txtDepartmentName.SelectedValue = "-Select-"; txtCostCenterName.SelectedValue = "-Select-";
        txtRemarks.Text = ""; txtItemCodeHide.Value = "";
        txtItemName.Text = ""; txtStockQty.Text = ""; txtStockValue.Text = "";
        txtAdjustQty.Text = ""; txtAdjustValue.Text = ""; txtWarehouseCodeHide.Value = "";
        txtWarehouseNameHide.Value = ""; txtZoneNameHide.Value = ""; txtBinNameHide.Value = "";

       
        btnSave.Text = "Save";
        Initial_Data_Referesh_Material_Request_Item();
        Session.Remove("Adjust_No");
        //Load_Data_Enquiry_Grid();

        
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";
      


        if (txtAdjustQty.Text == "0.0" || txtAdjustQty.Text == "0" || txtAdjustQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Adjustment Qty...');", true);
        }
        if (txtAdjustValue.Text == "0.0" || txtAdjustValue.Text == "0" || txtAdjustValue.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Adjustment Qty...');", true);
        }

       

        //check with Item Code And Item Name
        query = "Select * from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCodeHide.Value + "' And ItemName='" + txtItemName.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
        }

        if (!ErrFlag)
        {
          
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('this Item Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                   


                    dr = dt.NewRow();
                    dr["ItemCode"] = txtItemCodeHide.Value;
                    dr["ItemName"] = txtItemName.Text;
                    dr["StockQty"] = txtStockQty.Text;
                    dr["StockValue"] = txtStockValue.Text;
                    dr["WarehouseCode"] = txtWarehouseCodeHide.Value;
                    dr["WarehouseName"] = txtWarehouseNameHide.Value;
                    dr["ZoneName"] = txtZoneNameHide.Value;
                    dr["BinName"] = txtBinNameHide.Value;
                    dr["AdjustQty"] = txtAdjustQty.Text;
                    dr["AdjustValue"] = txtAdjustValue.Text;
                  
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    txtItemCodeHide.Value = ""; txtItemName.Text = ""; txtAdjustValue.Text = "";
                    txtAdjustQty.Text = ""; txtStockQty.Text = ""; txtStockValue.Text = "";
                 
                }
            }
            else
            {
                dr["ItemCode"] = txtItemCodeHide.Value;
                dr["ItemName"] = txtItemName.Text;
                dr["StockQty"] = txtStockQty.Text;
                dr["StockValue"] = txtStockValue.Text;
                dr["WarehouseCode"] = txtWarehouseCodeHide.Value;
                dr["WarehouseName"] = txtWarehouseNameHide.Value;
                dr["ZoneName"] = txtZoneNameHide.Value;
                dr["BinName"] = txtBinNameHide.Value;
                dr["AdjustQty"] = txtAdjustQty.Text;
                dr["AdjustValue"] = txtAdjustValue.Text;

                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                txtItemCodeHide.Value = ""; txtItemName.Text = ""; txtAdjustValue.Text = "";
                txtAdjustQty.Text = ""; txtStockQty.Text = ""; txtStockValue.Text = ""; txtWarehouseCodeHide.Value = "";
                txtWarehouseNameHide.Value = ""; txtZoneNameHide.Value = ""; txtBinNameHide.Value = "";
                 
            }
        }
    }

    private void Initial_Data_Referesh_Material_Request_Item()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("StockQty", typeof(string)));
        dt.Columns.Add(new DataColumn("StockValue", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseCode", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseName", typeof(string)));
        dt.Columns.Add(new DataColumn("ZoneName", typeof(string)));
        dt.Columns.Add(new DataColumn("BinName", typeof(string)));
        dt.Columns.Add(new DataColumn("AdjustQty", typeof(string)));
        dt.Columns.Add(new DataColumn("AdjustValue", typeof(string)));

       
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Std_Purchase_Order_Main Table Load
        query = "select * from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Adjustment_No='" + txtTransNo.Text + "'";
        Std_Pur_Main = objdata.RptEmployeeMultipleDetails(query);
        if (Std_Pur_Main.Rows.Count > 0)
        {
            txtTransNo.Text = Std_Pur_Main.Rows[0]["Adjustment_No"].ToString();
            txtDate.Text = Std_Pur_Main.Rows[0]["Adjustmen_Date"].ToString();

            txtAdjustmentBy.Value = Std_Pur_Main.Rows[0]["AdjustmenBy"].ToString();
            txtDepartmentName.SelectedValue = Std_Pur_Main.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Std_Pur_Main.Rows[0]["DeptName"].ToString();
            txtCostCenterName.SelectedValue = Std_Pur_Main.Rows[0]["CostCenterCode"].ToString();
            txtCostCenterName.SelectedItem.Text = Std_Pur_Main.Rows[0]["CostCenterName"].ToString();
            txtRemarks.Text = Std_Pur_Main.Rows[0]["Remarks"].ToString();


            //Std_Purchase_Order_Main_Sub Table Load

            DataTable dt = new DataTable();


            query = "Select ItemCode,ItemName,StockQty,StockValue,AdjustQty,AdjustValue,WarehouseCode,WarehouseName,ZoneName,BinName from Adjustment_Stock_Entry_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Adjustment_No='" + txtTransNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);


            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }


        else
        {
            Clear_All_Field();
        }


    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Session.Remove("Adjust_No");
        Response.Redirect("Adjustment_Stock_Entry_Main.aspx");
    }

    protected void btnAdjustmentBy_Click(object sender, EventArgs e)
    {
        //modalPop_AdjustmentBy.Show();
    }
    private void Load_Data_Empty_AdjustmentBy()
    {
        
        string query = "";
        DataTable Main_DT = new DataTable();
        txtAdjustmentBy.Items.Clear();
        
        query = "Select * from MstEmployee where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by EmpName Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtAdjustmentBy.Items.Add("-Select-");
       
        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {
            txtAdjustmentBy.Items.Add(Main_DT.Rows[i]["EmpName"].ToString());
           
        }

    }

    protected void GridViewClick_AdjustmentBy(object sender, CommandEventArgs e)
    {
        //txtAdjustmentBy.Text = Convert.ToString(e.CommandArgument);
        //txtEmpID.Value = Convert.ToString(e.CommandName);

    }
    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();
        query = "Select ItemCode,ItemName,Stock_Qty,Stock_Value from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
       
        DT = objdata.RptEmployeeMultipleDetails(query);


        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string itemcode = commandArgs[0];
        string StckQty = commandArgs[1];
        string StckValue = commandArgs[2];

        string query = "";
        DataTable dt = new DataTable();

        query = "Select *from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' And ItemCode='" + itemcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
       

        txtItemCodeHide.Value = itemcode;
        txtItemName.Text = Convert.ToString(e.CommandName);
        txtStockQty.Text = StckQty;
        txtStockValue.Text = StckValue;
        txtWarehouseCodeHide.Value = dt.Rows[0]["WarehouseCode"].ToString();
        txtWarehouseNameHide.Value = dt.Rows[0]["WarehouseName"].ToString();
        txtZoneNameHide.Value = dt.Rows[0]["ZoneName"].ToString();
        txtBinNameHide.Value = dt.Rows[0]["BinName"].ToString();

    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();
    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnCostCenterName_Click(object sender, EventArgs e)
    {
        //modalPop_CostCenterName.Show();
    }
    private void Load_Data_Empty_CostCenterName()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtCostCenterName.Items.Clear();
        query = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCostCenterName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["CostcenterCode"] = "-Select-";
        dr["CostcenterName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCostCenterName.DataTextField = "CostcenterName";
        txtCostCenterName.DataValueField = "CostcenterCode";
        txtCostCenterName.DataBind();

    }

    protected void GridViewClick_CostCenterName(object sender, CommandEventArgs e)
    {
        //txtCostCenterCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtCostCenterName.Text = Convert.ToString(e.CommandName);


    }


}
