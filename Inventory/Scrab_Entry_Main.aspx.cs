﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Inventory_Scrab_Entry_Main : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Scrab Entry";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Inventory"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Load_Data_Enquiry_Grid();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Scrab Entry");
        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Scrab Entry..');", true);
        }
        else
        {
            Session.Remove("Scrab_Entry_No");
            Response.Redirect("Scrab_Entry.aspx");
        }
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {


        //string query = "";
        //bool ErrFlag = false;

        //DataTable dtdpurchase = new DataTable();
        //query = "select Status from Scrab_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Scrab_Entry_No='" + e.CommandName.ToString() + "'";
        //dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
        //string status = dtdpurchase.Rows[0]["Status"].ToString();

        //if (status == "" || status == "0")
        //{
            string Enquiry_No_Str = e.CommandName.ToString();
            Session.Remove("Scrab_Entry_No");
            Session["Scrab_Entry_No"] = Enquiry_No_Str;
            Response.Redirect("Scrab_Entry.aspx");
        //}
        //else if (status == "1")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Scrab Entry Already Approved..');", true);
        //}

    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Scrab Entry");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New Scrab Entry..');", true);
        }
        //User Rights Check End

        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        query = "Select * from Scrab_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Scrab_Entry_No='" + e.CommandName.ToString() + "' And Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Delete Scrab Entry Details Already Approved..');", true);
        }
        //Check With Already Approved End

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Scrab_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Scrab_Entry_No='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //Delete Main Table
                query = "Delete from Scrab_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Scrab_Entry_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                //Delete Main Sub Table
                query = "Delete from Scrab_Entry_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Scrab_Entry_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Scrab Entry Details Deleted Successfully');", true);
                Load_Data_Enquiry_Grid();
            }
        }
    }

    private void Load_Data_Enquiry_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Scrab_Entry_No,Scrab_Entry_Date,Verifiedby from Scrab_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

}
