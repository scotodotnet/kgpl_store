﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Inventory_meterial_issue_main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    bool ErrFlag = false;
    string RptName;
    string ButtonName;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Material Issue";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Inventory"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Load_Data_Enquiry_Grid();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Material Issue");
        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Material Issue..');", true);
        }
        else
        {
            Session.Remove("Mat_Issue_No");
            Response.Redirect("meterial_issue.aspx");
        }
    }


    protected void GridPrintEnquiryClick(object sender, CommandEventArgs e)
    {

        string CostCenter = "";
        string WareHouse = "";
        string DeptName = "";
        string MatIssNo = e.CommandName.ToString();
        string Issuedby = "";
        string ItemName = "";
        string FromDate = "";
        string ToDate = "";
        string ItemType = "";

        RptName = "Material Issue Report";
        ButtonName = "Issue Slip";

        // ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?RptName=" + RptName + "&ButtonName=" + ButtonName + "&InvoiceNo=" + e.CommandName.ToString(), "_blank", "");

        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?CostCenter=" + CostCenter + "&WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&MatIssNo=" + MatIssNo + "&MatIssBy=" + Issuedby + "&ItemName=" + ItemName + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&ItemType=" + ItemType + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");

    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        string Enquiry_No_Str = e.CommandName.ToString();
        DataTable dtMatIss = new DataTable();

        SSQL = "select Mat_Issue_Status from Meterial_Issue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + "And Mat_Issue_No='" + Enquiry_No_Str + "'";


        dtMatIss = objdata.RptEmployeeMultipleDetails(SSQL);
        string status = dtMatIss.Rows[0]["Mat_Issue_Status"].ToString();

        if (status == "" || status == "0")
        {
            Session.Remove("Mat_Issue_No");
            Session["Mat_Issue_No"] = Enquiry_No_Str;
            Response.Redirect("meterial_issue.aspx");
        }
        else if (status == "2")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Meterial Issue Already Rejected..');", true);
        }
        else if (status == "3")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Meterial Issue put in pending..');", true);
        }
        else if (status == "1")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approved Meterial Issue Cant Edit..');", true);
        }

    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Material Issue");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New Material Issue..');", true);
        }
        //User Rights Check End

        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        query = "Select * from Meterial_Issue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Issue_No='" + e.CommandName.ToString() + "' And Mat_Issue_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Delete Material Issue Details Already Approved..');", true);
        }
        //Check With Already Approved End

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Meterial_Issue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Issue_No='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //Delete Main Table
                query = "Delete from Meterial_Issue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Issue_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                //Delete Main Sub Table
                query = "Delete from Meterial_Issue_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Issue_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Material Issue Details Deleted Successfully');", true);
                Load_Data_Enquiry_Grid();
            }
        }
    }

    private void Load_Data_Enquiry_Grid()
    {


        string query = "";
        DataTable DT = new DataTable();
        query = "Select Mat_Issue_No,Mat_Issue_Date,Ledger_No,Takenby,Issuedby from Meterial_Issue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }
}
