﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Inventory_Unit_To_Unit_Stock_Transfer : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionMaterialStockTransferNo;
    string SessionFinYearCode;
    string SessionFinYearVal;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Unit To Unit Material Stock Transfer";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Inventory"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh_Material_Request_Item();
            Employee_Name_Load();

            Load_Data_Empty_FrmWareHouse();
            Load_Data_Empty_ToWareHouse();
            Load_Data_Empty_Dept();
            Load_Data_Empty_CostCenterName();
            
            Load_Data_Empty_FromZoneName();
            Load_Data_Empty_ToZoneName();
            Load_Data_Empty_FromBinName();
            Load_Data_Empty_ToBinName();



            if (Session["Unit_Stock_Transfer_No"] == null)
            {
                SessionMaterialStockTransferNo = "";
            }
            else
            {
                SessionMaterialStockTransferNo = Session["Unit_Stock_Transfer_No"].ToString();
                txtTransferNo.Text = SessionMaterialStockTransferNo;
                btnSearch_Click(sender, e);
            }
            Load_Location();
        }
        Load_OLD_data();
        Load_Data_Empty_ItemCode();
       
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }

        //Check From Warehouse Name
        query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtFromWarehouseName.SelectedValue + "' And WarehouseName='" + txtFromWarehouseName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with From Warehouse Name...');", true);
        }

        //Check To Warehouse Name
        query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtToWarehouseName.SelectedValue + "' And WarehouseName='" + txtToWarehouseName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with To Warehouse Name...');", true);
        }

        //Check Cost Center Name
        query = "Select * from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CostcenterCode='" + txtCostCenterName.SelectedValue + "' And CostcenterName='" + txtCostCenterName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Cost Center Name...');", true);
        }

        //Check Prepared by And Issued by
        if (txtPreparedBy.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Prepared by...');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Unit To Unit Material Stock Transfer");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Unit To Unit Material Stock Transfer Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Material Stock Transfer");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Unit To Unit Material Stock Transfer..');", true);
            }
        }
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Unit To Unit Material Stock Transfer", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtTransferNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            query = "Select * from Unit_To_Unit_Meterial_Stock_Transfer_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Unit_Stock_Transfer_No='" + txtTransferNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Unit_To_Unit_Meterial_Stock_Transfer_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Unit_Stock_Transfer_No='" + txtTransferNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Unit_To_Unit_Meterial_Stock_Transfer_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Unit_Stock_Transfer_No='" + txtTransferNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into Unit_To_Unit_Meterial_Stock_Transfer_Main(Ccode,Lcode,FinYearCode,FinYearVal,Unit_Stock_Transfer_No,Unit_Stock_Transfer_Date,Transfer_Unit_LCode,Transfer_Unit_Location,From_WarehouseCode,From_WarehouseName,To_WarehouseCode,To_WarehouseName,DeptCode,DeptName,CostCenterCode,CostCenterName,Preparedby,Others,UserID,UserName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransferNo.Text + "','" + txtDate.Text + "','" + ddlLocation.SelectedValue + "','" + ddlLocation.SelectedItem.Text + "','" + txtFromWarehouseName.SelectedValue + "','" + txtFromWarehouseName.SelectedItem.Text + "','" + txtToWarehouseName.SelectedValue + "','" + txtToWarehouseName.SelectedItem.Text + "','" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "','" + txtCostCenterName.SelectedValue + "',";
            query = query + " '" + txtCostCenterName.SelectedItem.Text + "','" + txtPreparedBy.Value + "','" + txtOthers.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            //Unit_To_Unit_Meterial_Stock_Transfer_Main_Sub
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Unit_To_Unit_Meterial_Stock_Transfer_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Unit_Stock_Transfer_No,Unit_Stock_Transfer_Date,ItemCode,ItemName,UOMCode,ValueType,FromZone,ToZone,FromBin,ToBin,TransferQty,Value,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransferNo.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "','" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["ValueType"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["FromZone"].ToString() + "','" + dt.Rows[i]["ToZone"].ToString() + "','" + dt.Rows[i]["FromBin"].ToString() + "','" + dt.Rows[i]["ToBin"].ToString() + "','" + dt.Rows[i]["TransferQty"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["Value"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Unit To Unit Material Stock Transfer Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Unit To Unit Material Stock Transfer Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Unit_Stock_Transfer_No"] = txtTransferNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtTransferNo.Text = ""; txtDate.Text = ""; txtFromWarehouseName.SelectedValue = "-Select-";
        txtToWarehouseName.SelectedValue = "-Select-"; txtCostCenterName.SelectedValue = "-Select-";
        txtPreparedBy.Value = "-Select-"; txtOthers.Text = ""; txtDepartmentName.SelectedValue = "-Select-";
        txtItemCodeHide.Value = ""; txtItemName.Text = ""; txtItemUOMHide.Value = ""; txtItemValueTypeHide.Value = "";
        txtFromZoneName.SelectedValue = null; txtToZoneName.SelectedValue = null; txtFromBinName.SelectedValue = null; txtToBinName.SelectedValue = null;
        txtTransferQty.Text = ""; txtValue.Text = ""; ddlLocation.SelectedIndex = 0;
         
        btnSave.Text = "Save";
        Initial_Data_Referesh_Material_Request_Item();
        Session.Remove("Unit_Stock_Transfer_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (txtTransferQty.Text == "0.0" || txtTransferQty.Text == "0" || txtTransferQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Transfer Qty...');", true);
        }
        if (txtValue.Text == "0.0" || txtValue.Text == "0" || txtValue.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Value...');", true);
        }

        //check with From Zone Name And From Bin Name
        query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtFromWarehouseName.SelectedValue + "' And ZoneName='" + txtFromZoneName.SelectedItem.Text + "' And BinName='" + txtFromBinName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with From Zone And From Bin Details..');", true);
        }

        //check with To Zone Name And To Bin Name
        query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtToWarehouseName.SelectedValue + "' And ZoneName='" + txtToZoneName.SelectedItem.Text + "' And BinName='" + txtToBinName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with To Zone And To Bin Details..');", true);
        }


        //check Item Stock
        query = "Select * from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCodeHide.Value + "' And WarehouseName='" + txtFromWarehouseName.SelectedItem.Text + "' And ZoneName='" + txtFromZoneName.SelectedItem.Text + "' And BinName='" + txtFromBinName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count != 0)
        {
            if (qry_dt.Rows[0]["Stock_Qty"].ToString() == "0.00")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('There is no stock');", true);
            }
            else if (Convert.ToDecimal(qry_dt.Rows[0]["Stock_Qty"]) < Convert.ToDecimal(txtTransferQty.Text))
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Transfer Qty. It exceeds Stock Qty..');", true);
            }

        }




        //check with Item Code And Item Name
        query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCodeHide.Value + "' And ItemShortName='" + txtItemName.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);


        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
        }

        if (!ErrFlag)
        {
            //UOM Code get
            string UOM_Code_Str = txtItemUOMHide.Value.ToString();
            string Valuation_Type = txtItemValueTypeHide.Value.ToString();

            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('this Item Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["ItemCode"] = txtItemCodeHide.Value;
                    dr["ItemName"] = txtItemName.Text;
                    dr["UOMCode"] = UOM_Code_Str;
                    dr["ValueType"] = Valuation_Type;
                    dr["FromZone"] = txtFromZoneName.SelectedItem.Text;
                    dr["ToZone"] = txtToZoneName.SelectedItem.Text;
                    dr["FromBin"] = txtFromBinName.SelectedItem.Text;
                    dr["ToBin"] = txtToBinName.SelectedItem.Text;
                    dr["TransferQty"] = txtTransferQty.Text;
                    dr["Value"] = txtValue.Text;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    txtItemCodeHide.Value = ""; txtItemName.Text = ""; txtItemUOMHide.Value = "";
                    txtItemValueTypeHide.Value = ""; txtFromZoneName.SelectedValue = null; txtToZoneName.SelectedValue = null;
                    txtFromBinName.SelectedValue = null; txtToBinName.SelectedValue = null; txtTransferQty.Text = ""; txtValue.Text = "";
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["ItemCode"] = txtItemCodeHide.Value;
                dr["ItemName"] = txtItemName.Text;
                dr["UOMCode"] = UOM_Code_Str;
                dr["ValueType"] = Valuation_Type;
                dr["FromZone"] = txtFromZoneName.SelectedItem.Text;
                dr["ToZone"] = txtToZoneName.SelectedItem.Text;
                dr["FromBin"] = txtFromBinName.SelectedItem.Text;
                dr["ToBin"] = txtToBinName.SelectedItem.Text;
                dr["TransferQty"] = txtTransferQty.Text;
                dr["Value"] = txtValue.Text;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                txtItemCodeHide.Value = ""; txtItemName.Text = ""; txtItemUOMHide.Value = "";
                txtItemValueTypeHide.Value = ""; txtFromZoneName.SelectedValue = null; txtToZoneName.SelectedValue = null;
                txtFromBinName.SelectedValue = null; txtToBinName.SelectedValue = null; txtTransferQty.Text = ""; txtValue.Text = "";
            }
        }
    }

    private void Initial_Data_Referesh_Material_Request_Item()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ValueType", typeof(string)));
        dt.Columns.Add(new DataColumn("FromZone", typeof(string)));
        dt.Columns.Add(new DataColumn("ToZone", typeof(string)));
        dt.Columns.Add(new DataColumn("FromBin", typeof(string)));
        dt.Columns.Add(new DataColumn("ToBin", typeof(string)));
        dt.Columns.Add(new DataColumn("TransferQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Value", typeof(string)));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSourceS;
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Unit_To_Unit_Meterial_Stock_Transfer_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Unit_Stock_Transfer_No='" + txtTransferNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["Unit_Stock_Transfer_Date"].ToString();
            ddlLocation.SelectedValue = Main_DT.Rows[0]["Transfer_Unit_LCode"].ToString();
            txtFromWarehouseName.SelectedValue = Main_DT.Rows[0]["From_WarehouseCode"].ToString();
            txtFromWarehouseName.SelectedItem.Text = Main_DT.Rows[0]["From_WarehouseName"].ToString();
            txtToWarehouseName.SelectedValue = Main_DT.Rows[0]["To_WarehouseCode"].ToString();
            txtToWarehouseName.SelectedItem.Text = Main_DT.Rows[0]["To_WarehouseName"].ToString();
            txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtCostCenterName.SelectedValue = Main_DT.Rows[0]["CostCenterCode"].ToString();
            txtCostCenterName.SelectedItem.Text = Main_DT.Rows[0]["CostCenterName"].ToString();
            txtPreparedBy.Value = Main_DT.Rows[0]["Preparedby"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();

            //Unit_To_Unit_Meterial_Stock_Transfer_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select ItemCode,ItemName,UOMCode,ValueType,FromZone,ToZone,FromBin,ToBin,TransferQty,Value from Unit_To_Unit_Meterial_Stock_Transfer_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Unit_Stock_Transfer_No='" + txtTransferNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("Unit_To_Unit_Stock_Transfer_Main.aspx");
    }

    private void Employee_Name_Load()
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        txtPreparedBy.Items.Clear();
        query = "Select * from MstEmployee where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by EmpName Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtPreparedBy.Items.Add("-Select-");
        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {
            txtPreparedBy.Items.Add(Main_DT.Rows[i]["EmpName"].ToString());
        }
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Unit To Unit Material Stock Transfer");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Unit To Unit Material Stock Transfer...');", true);
        //}
        //User Rights Check End

        query = "Select * from Unit_To_Unit_Meterial_Stock_Transfer_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Unit_Stock_Transfer_No='" + txtTransferNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Unit To Unit Material Stock Transfer Details..');", true);
        }

        query = "Select * from Unit_To_Unit_Meterial_Stock_Transfer_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Unit_Stock_Transfer_No='" + txtTransferNo.Text + "' And Mat_Stock_Transfer_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Unit To Unit Material Stock Transfer Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            query = "Update Unit_To_Unit_Meterial_Stock_Transfer_Main set Mat_Stock_Transfer_Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And Unit_Stock_Transfer_No='" + txtTransferNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            Stock_Add();
            Stock_Minus();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Unit To Unit Material Stock Transfer Details Approved Successfully..');", true);
        }
    }

    private void Stock_Add()
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();

        query = "Select * from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + ddlLocation.SelectedValue + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransferNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            query = "Delete from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + ddlLocation.SelectedValue + "'";
            query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransferNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }

        DateTime transDate = Convert.ToDateTime(txtDate.Text);

        //Insert Stock_Transaction_Ledger
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            query = "Insert Into Stock_Transaction_Ledger(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
            query = query + " Trans_Type,DeptCode,DeptName,CostCenterCode,CostCenterName,ItemCode,ItemName,Open_Qty,Open_Value,PO_Qty,PO_Value,General_Qty,General_Value,Issue_Qty,";
            query = query + " Issue_Value,Issue_Return_Qty,Issue_Return_Value,GP_IN_Qty,GP_IN_Value,GP_OUT_Qty,GP_OUT_Value,Transfer_Qty,Transfer_Value,Damage_Qty,Damage_Value,Spares_ReSales_Qty,Spares_ReSales_Value,WarehouseCode,WarehouseName,ZoneName,BinName,Supp_Code,";
            query = query + " Supp_Name,UserID,UserName) Values('" + SessionCcode + "','" + ddlLocation.SelectedValue + "','" + SessionFinYearCode + "',";
            query = query + " '" + SessionFinYearVal + "','" + txtTransferNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtDate.Text + "','UNIT STOCK TRANSFER','" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "','" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "',";
            query = query + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
            query = query + " '0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0',";
            query = query + " '" + dt.Rows[i]["TransferQty"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "','0.0','0.0','0.0','0.0','" + txtToWarehouseName.SelectedValue + "',";
            query = query + " '" + txtToWarehouseName.SelectedItem.Text + "','" + dt.Rows[i]["ToZone"].ToString() + "','" + dt.Rows[i]["ToBin"].ToString() + "',";
            query = query + " '','','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            CommonClass_Function.cls_Trans_Type = "UNIT STOCK TRANSFER";
            CommonClass_Function.cls_ItemCode = dt.Rows[i]["ItemCode"].ToString();
            CommonClass_Function.cls_ItemName = dt.Rows[i]["ItemName"].ToString();

            CommonClass_Function.cls_Add_Qty = dt.Rows[i]["TransferQty"].ToString();
            CommonClass_Function.cls_Add_Value = dt.Rows[i]["Value"].ToString();
            CommonClass_Function.cls_Minus_Qty = "0.0";
            CommonClass_Function.cls_Minus_Value = "0.0";


            CommonClass_Function.cls_DeptCode = txtDepartmentName.SelectedValue;
            CommonClass_Function.cls_DeptName = txtDepartmentName.SelectedItem.Text;
            CommonClass_Function.cls_CostCenterCode = txtCostCenterName.SelectedValue;
            CommonClass_Function.cls_CostCenterName = txtCostCenterName.SelectedItem.Text;
            CommonClass_Function.cls_WarehouseCode = txtToWarehouseName.SelectedValue;
            CommonClass_Function.cls_WarehouseName = txtToWarehouseName.SelectedItem.Text;
            CommonClass_Function.cls_ZoneName = dt.Rows[i]["ToZone"].ToString();
            CommonClass_Function.cls_BinName = dt.Rows[i]["ToBin"].ToString();
            CommonClass_Function.cls_Supp_Code = "";
            CommonClass_Function.cls_Supp_Name = "";
            CommonClass_Function.cls_UserID = SessionUserID;
            CommonClass_Function.cls_UserName = SessionUserName;
            //CommonClass_Function.cls_Stock_Qty = "";
            //CommonClass_Function.cls_Stock_Value = "";

            CommonClass_Function.Stock_Ledger_And_Current_Stock_Add(SessionCcode, ddlLocation.SelectedValue, SessionFinYearCode, SessionFinYearVal, txtTransferNo.Text, txtDate.Text);
        }




    }


    private void Stock_Minus()
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();

        query = "Select * from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransferNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            query = "Delete from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransferNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }

        DateTime transDate = Convert.ToDateTime(txtDate.Text);

        //Insert Stock_Transaction_Ledger
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            query = "Insert Into Stock_Transaction_Ledger(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
            query = query + " Trans_Type,DeptCode,DeptName,CostCenterCode,CostCenterName,ItemCode,ItemName,Open_Qty,Open_Value,PO_Qty,PO_Value,General_Qty,General_Value,Issue_Qty,";
            query = query + " Issue_Value,Issue_Return_Qty,Issue_Return_Value,GP_IN_Qty,GP_IN_Value,GP_OUT_Qty,GP_OUT_Value,Transfer_Qty,Transfer_Value,Damage_Qty,Damage_Value,Spares_ReSales_Qty,Spares_ReSales_Value,WarehouseCode,WarehouseName,ZoneName,BinName,Supp_Code,";
            query = query + " Supp_Name,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
            query = query + " '" + SessionFinYearVal + "','" + txtTransferNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtDate.Text + "','UNIT STOCK TRANSFER','" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "','" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "',";
            query = query + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
            query = query + " '0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0',";
            query = query + " '" + dt.Rows[i]["TransferQty"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "','0.0','0.0','0.0','0.0','" + txtFromWarehouseName.SelectedValue + "',";
            query = query + " '" + txtFromWarehouseName.SelectedItem.Text + "','" + dt.Rows[i]["FromZone"].ToString() + "','" + dt.Rows[i]["FromBin"].ToString() + "',";
            query = query + " '','','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            CommonClass_Function.cls_Trans_Type = "UNIT STOCK TRANSFER";
            CommonClass_Function.cls_ItemCode = dt.Rows[i]["ItemCode"].ToString();
            CommonClass_Function.cls_ItemName = dt.Rows[i]["ItemName"].ToString();

            CommonClass_Function.cls_Add_Qty = "0.0";
            CommonClass_Function.cls_Add_Value = "0.0";
            CommonClass_Function.cls_Minus_Qty = dt.Rows[i]["TransferQty"].ToString();
            CommonClass_Function.cls_Minus_Value = dt.Rows[i]["Value"].ToString();


            CommonClass_Function.cls_DeptCode = txtDepartmentName.SelectedValue;
            CommonClass_Function.cls_DeptName = txtDepartmentName.SelectedItem.Text;
            CommonClass_Function.cls_CostCenterCode = txtCostCenterName.SelectedValue;
            CommonClass_Function.cls_CostCenterName = txtCostCenterName.SelectedItem.Text;
            CommonClass_Function.cls_WarehouseCode = txtFromWarehouseName.SelectedValue;
            CommonClass_Function.cls_WarehouseName = txtFromWarehouseName.SelectedItem.Text;
            CommonClass_Function.cls_ZoneName = dt.Rows[i]["FromZone"].ToString();
            CommonClass_Function.cls_BinName = dt.Rows[i]["FromBin"].ToString();
            CommonClass_Function.cls_Supp_Code = "";
            CommonClass_Function.cls_Supp_Name = "";
            CommonClass_Function.cls_UserID = SessionUserID;
            CommonClass_Function.cls_UserName = SessionUserName;
            //CommonClass_Function.cls_Stock_Qty = "";
            //CommonClass_Function.cls_Stock_Value = "";

            CommonClass_Function.Stock_Ledger_And_Current_Stock_Add(SessionCcode, SessionLcode, SessionFinYearCode, SessionFinYearVal, txtTransferNo.Text, txtDate.Text);
        }




    }



    protected void btnFrmWareHouse_Click(object sender, EventArgs e)
    {
        //modalPop_FrmWareHouse.Show();
    }
    private void Load_Data_Empty_FrmWareHouse()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtFromWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtFromWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtFromWarehouseName.DataTextField = "WarehouseName";
        txtFromWarehouseName.DataValueField = "WarehouseCode";
        txtFromWarehouseName.DataBind();

    }

    protected void txtFromWarehouseName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_FromZoneName();
        Load_Data_Empty_FromBinName();
    }

    protected void GridViewClick_FrmWareHouse(object sender, CommandEventArgs e)
    {
        //txtFromWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtFromWarehouseName.Text = Convert.ToString(e.CommandName);


        Load_Data_Empty_FromZoneName();
        Load_Data_Empty_FromBinName();
    }

    protected void btnToWareHouse_Click(object sender, EventArgs e)
    {
        //modalPop_ToWareHouse.Show();
    }
    private void Load_Data_Empty_ToWareHouse()
    {

   
        string query = "";
        DataTable Main_DT = new DataTable();

        txtToWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtToWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtToWarehouseName.DataTextField = "WarehouseName";
        txtToWarehouseName.DataValueField = "WarehouseCode";
        txtToWarehouseName.DataBind();

    }

    protected void txtToWarehouseName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_ToZoneName();
        Load_Data_Empty_ToBinName();
    }

    protected void GridViewClick_ToWareHouse(object sender, CommandEventArgs e)
    {
        //txtToWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtToWarehouseName.Text = Convert.ToString(e.CommandName);

        Load_Data_Empty_ToZoneName();
        Load_Data_Empty_ToBinName();

    }

    protected void btnCostCenterName_Click(object sender, EventArgs e)
    {
        //modalPop_CostCenterName.Show();
    }

    private void Load_Data_Empty_CostCenterName()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtCostCenterName.Items.Clear();
        query = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCostCenterName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["CostcenterCode"] = "-Select-";
        dr["CostcenterName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCostCenterName.DataTextField = "CostcenterName";
        txtCostCenterName.DataValueField = "CostcenterCode";
        txtCostCenterName.DataBind();

    }

    protected void GridViewClick_CostCenterName(object sender, CommandEventArgs e)
    {
       // txtCostCenterCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtCostCenterName.Text = Convert.ToString(e.CommandName);


    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();


        query = "Select ItemCode,ItemShortName as ItemName,PurchaseUOM as UOMCode,ValuationType from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);


        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string itemcode = commandArgs[0];
        string UOMCode = commandArgs[1];
        string ValuationType = commandArgs[2];

        txtItemCodeHide.Value = itemcode;
        txtItemName.Text = Convert.ToString(e.CommandName);
        txtItemUOMHide.Value = UOMCode;
        txtItemValueTypeHide.Value = ValuationType;
    }


    protected void btnFromZoneName_Click(object sender, EventArgs e)
    {
        //modalPop_FromZoneName.Show();
    }

    private void Load_Data_Empty_FromZoneName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtFromZoneName.Items.Clear();
        query = "Select ZoneName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And WarehouseCode='" + txtFromWarehouseName.SelectedValue + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtFromZoneName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["ZoneName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtFromZoneName.DataTextField = "ZoneName";

        txtFromZoneName.DataBind();
    }

    protected void txtFromZoneName_SelectedIndexChanged(object sender, EventArgs e)
    {

        Load_Data_Empty_FromBinName();
    }

    protected void GridViewClick_FromZoneName(object sender, CommandEventArgs e)
    {
        txtFromZoneName.Text = Convert.ToString(e.CommandArgument);

        Load_Data_Empty_FromBinName();
    }


    protected void btnToZoneName_Click(object sender, EventArgs e)
    {
        //modalPop_ToZoneName.Show();
    }

    private void Load_Data_Empty_ToZoneName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtToZoneName.Items.Clear();
        query = "Select ZoneName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And WarehouseCode='" + txtToWarehouseName.SelectedValue + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtToZoneName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["ZoneName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtToZoneName.DataTextField = "ZoneName";

        txtToZoneName.DataBind();
    }

    protected void txtToZoneName_SelectedIndexChanged(object sender, EventArgs e)
    {

        Load_Data_Empty_ToBinName();
    }

    protected void GridViewClick_ToZoneName(object sender, CommandEventArgs e)
    {
        txtToZoneName.Text = Convert.ToString(e.CommandArgument);

        Load_Data_Empty_ToBinName();
    }


    protected void btnFromBinName_Click(object sender, EventArgs e)
    {
        //modalPop_FromBinName.Show();
    }

    private void Load_Data_Empty_FromBinName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtFromBinName.Items.Clear();
        query = "Select BinName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And WarehouseCode='" + txtFromWarehouseName.SelectedValue + "' And ZoneName='" + txtFromZoneName.SelectedItem.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtFromBinName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["BinName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtFromBinName.DataTextField = "BinName";

        txtFromBinName.DataBind();
    }

    protected void GridViewClick_FromBinName(object sender, CommandEventArgs e)
    {
        txtFromBinName.Text = Convert.ToString(e.CommandArgument);
    }

    protected void btnToBinName_Click(object sender, EventArgs e)
    {
        //modalPop_ToBinName.Show();
    }

    private void Load_Data_Empty_ToBinName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtToBinName.Items.Clear();
        query = "Select BinName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And WarehouseCode='" + txtToWarehouseName.SelectedValue + "' And ZoneName='" + txtToZoneName.SelectedItem.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtToBinName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["BinName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtToBinName.DataTextField = "BinName";

        txtToBinName.DataBind();
    }

    protected void GridViewClick_ToBinName(object sender, CommandEventArgs e)
    {
        txtToBinName.Text = Convert.ToString(e.CommandArgument);
    }
    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    public void Load_Location()
    {
        DataTable dtempty = new DataTable();

        DataTable dt = new DataTable();

        string query = "";

        query = "Select LCode,LCode + ' - ' + Location as Location from AdminRights where Ccode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);


        ddlLocation.DataSource = dt;


        ddlLocation.DataTextField = "Location";
        ddlLocation.DataValueField = "LCode";
        ddlLocation.DataBind();
    }

}
