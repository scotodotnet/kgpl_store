﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Adjustment_Stock_Entry_Approval_View.aspx.cs" Inherits="Inventory_Adjustment_Stock_Entry_Approval_View" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Stock Adjustment Entry Approve View</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Stock Adjustment Entry Approve View</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">				
				    <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Trans No<span class="mandatory">*</span></label>
					            <asp:Label ID="txtTransNo" runat="server" class="form-control"></asp:Label>					            
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Date</label>
					            <asp:Label ID="txtDate" MaxLength="20" class="form-control" runat="server"></asp:Label>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Adjustment By</label>
					            <asp:Label ID="txtAdjustmentBy" class="form-control" runat="server"></asp:Label>
					        </div>
					    </div>
					</div>
					
					     
					    <div class="col-md-12">
					    <div class="row">    
					    
					    <div class="form-group col-md-4">
					          <label for="exampleInputName">Department Name</label>
					            <asp:Label ID="txtDepartmentName" class="form-control" runat="server"></asp:Label>
					            <asp:HiddenField ID="txtDeptCodeHide" runat="server" />
					        </div>
					    <div class="form-group col-md-4">
					            <label for="exampleInputName">Cost Center Name</label>
					            <asp:Label ID="txtCostCenterName" class="form-control" runat="server"></asp:Label>
					            <asp:HiddenField ID="txtCostCenterCodeHide" runat="server" />
					        </div>
					    
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Remarks</label>
					            <asp:Label ID="txtRemarks" TextMode="MultiLine" class="form-control" runat="server"></asp:Label>
					        </div>
					    </div>
					</div>
					
					<div class="col-md-12">
					    <div class="row">
					        
					    </div>
					</div>
					
					
				    <div class="clearfix"></div>				    
				    <div class="timeline-options">
				        <h4 class="panel-title">Item Details</h4>
				    </div>
				
					
					
					<!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Panel ID="PanelmaterialRequestItem" runat="server">
					            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                        <HeaderTemplate>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Item Code</th>
                                                    <th>Item Name</th>
                                                    <th>StockQty</th>
                                                    <th>StockValue</th>
                                                    <th>AdjustQty</th>
                                                    <th>AdjustValue</th>
                                                   
                                                  
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Container.ItemIndex + 1 %></td>
                                            <td><%# Eval("ItemCode")%></td>
                                            <td><%# Eval("ItemName")%></td>
                                            <td><%# Eval("StockQty")%></td>
                                            <td><%# Eval("StockValue")%></td>
                                            <td><%# Eval("AdjustQty")%></td>
                                            <td><%# Eval("AdjustValue")%></td>
                                          
                                            
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate></table></FooterTemplate>                                
			                    </asp:Repeater>
			                </asp:Panel>
					    </div>
					</div>
					<!-- table End -->
					
					
					      	
						<div class="form-group row"></div>	
                        <!-- Button start -->
                        <div class="txtcenter">
                        <asp:Button ID="btnApprove" class="btn btn-success" runat="server" Text="Approve" OnClick="btnApprove_Click"/>
                        <asp:Button ID="btnPending" class="btn btn-success" runat="server" Text="Pending" 
                                onclick="btnPending_Click" />
                       <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                       <asp:Button ID="btnback" class="btn btn-success" runat="server" Text="Back" 
                                onclick="btnback_Click"/>
                      </div>
                        <!-- Button end -->
                    </div><!-- panel Body-->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
             <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->


</asp:Content>

