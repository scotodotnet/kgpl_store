﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Inventory_gate_pass_out : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassOutNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Gate Pass Out";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Inventory"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh_Material_Request_Item();
            Employee_Name_Load();
            Load_Data_Empty_Dept();
            Load_Data_Empty_WareHouse();
            Load_Data_Empty_CostCenterName();
            Load_Data_Empty_ZoneName();
            Load_Data_Empty_BinName();
            Load_Data_Empty_Supp1();
           // Load_Data_Empty_ItemCode();

            if (Session["GP_Out_No"] == null)
            {
                SessionGatePassOutNo = "";
            }
            else
            {
                SessionGatePassOutNo = Session["GP_Out_No"].ToString();
                txtGPOutNo.Text = SessionGatePassOutNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();
        Load_Data_Empty_ItemCode();
        //Load_Data_Empty_Supp1();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }

        //Check Supplier Name
        if (ddlSupplierName.SelectedItem.Text.ToString() != "-Select-")
        {
            query = "Select * from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And SuppCode='" + ddlSupplierName.SelectedValue + "' ";
            query = query + " And SuppName='" + ddlSupplierName.SelectedItem.Text + "'";

            qry_dt = objdata.RptEmployeeMultipleDetails(query);
            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Supplier Name...');", true);
            }
        }

        //Check Issued by And Prefered by

        //Check Department Name
        query = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + txtDepartmentName.SelectedValue + "' ";
        query = query + " And DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Department Name...');", true);
        }

        if (RdpStkType.SelectedValue == "1")
        {
            //Check Warehouse Name
            query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseName.SelectedValue + "'";
            query = query + "And WarehouseName='" + txtWarehouseName.SelectedItem.Text + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Warehouse Name...');", true);
            }
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Gate Pass OUT");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Gate Pass OUT Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Gate Pass OUT");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Gate Pass OUT..');", true);
            }
        }
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Gate Pass OUT", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtGPOutNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            query = "Select * from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
            query = query + "GP_Out_No ='" + txtGPOutNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                query = query + "And GP_Out_No='" + txtGPOutNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);

                query = "Delete from GatePass_Out_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                query = query + " And GP_Out_No='" + txtGPOutNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);

                query = "Delete from DC_Temp where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                query = query + " And InvoiceNo='" + txtGPOutNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into GatePass_Out_Main(Ccode,Lcode,FinYearCode,FinYearVal,GP_Out_No,GP_Out_Date,GP_Type,Supp_Code,Supp_Name,Supp_Det,DeliveryDate,Takenby,Issuedby,";
            query = query + " Preferedby,DeptCode,DeptName,CostCenterCode,CostCenterName,WarehouseCode,WarehouseName,Others,StockType,OpStatus ,UserID,UserName) Values(";
            query = query + " '" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtGPOutNo.Text + "',";
            query = query + " '" + txtDate.Text + "','" + RdpGPoutType.SelectedItem.Text + "','" + ddlSupplierName.SelectedValue + "',";
            query = query + " '" + ddlSupplierName.SelectedItem.Text + "','" + txtSupplierDet.Text + "','" + txtDeliveryDate.Text + "','" + txtThrough.Text + "',";
            query = query + " '" + txtIssuedBy.Value + "','" + txtPreferedBy.Value + "','" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "',";
            query = query + " '" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "','" + txtWarehouseName.SelectedValue + "',";
            query = query + "'" + txtWarehouseName.SelectedItem.Text + "','" + txtOthers.Text + "','" + RdpStkType.SelectedValue + "','0',";
            query = query + "'" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            //GatePass_Out_Main_Sub
            DataTable dt = new DataTable();

            DataTable dt_Temp = new DataTable();

            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into GatePass_Out_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,GP_Out_No,GP_Out_Date,ItemCode,ItemName,UOMCode,OutQty,Value,ZoneName,BinName,Remarks,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtGPOutNo.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["OutQty"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["BinName"].ToString() + "','" + dt.Rows[i]["Remarks"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";

                objdata.RptEmployeeMultipleDetails(query);


                query = "Select * from DC_Temp where InvoiceNo='" + txtGPOutNo.Text + "' and ItemName='" + dt.Rows[i]["ItemName"].ToString() + "'";

                dt_Temp = objdata.RptEmployeeMultipleDetails(query);

                if (dt_Temp.Rows.Count > 0)
                {

                }
                else
                {
                    query = "insert into DC_Temp(InvoiceNo,ItemName,CusQty,DCQty,QtyBalance,Ccode,Lcode,FinYearCode) values('" + txtGPOutNo.Text + "',";
                    query = query + "'" + dt.Rows[i]["ItemName"].ToString() + "','" + dt.Rows[i]["OutQty"].ToString() + "','0','0','" + SessionCcode + "',";
                    query = query + "'" + SessionLcode + "','" + SessionFinYearCode + "')";

                    objdata.RptEmployeeMultipleDetails(query);
                }

            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GatePass Out Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GatePass Out Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["GP_Out_No"] = txtGPOutNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        RdpStkType.SelectedValue = "1";
        txtThrough.Text = "";
        txtGPOutNo.Text = ""; txtDate.Text = ""; RdpGPoutType.SelectedValue = "1";
        ddlSupplierName.SelectedValue = "-Select-"; ddlSupplierName.SelectedItem.Text = "-Select-";
        txtSupplierDet.Text = ""; txtDeliveryDate.Text = ""; txtTakenby.Text = ""; txtIssuedBy.Value = "-Select-";
        txtPreferedBy.Value = "-Select-"; txtDepartmentName.SelectedValue = "-Select-"; 
        txtWarehouseName.SelectedValue = "-Select-"; txtOthers.Text = ""; txtCostCenterName.SelectedValue = "-Select-";
        txtItemName.Text = ""; 
        txtUOMCodeType.Text = "";
        txtOutQty.Text = "";
        txtValue.Text = ""; txtZoneName.SelectedValue = null; txtBinName.SelectedValue = null; txtRemarks.Text = "";

        btnSave.Text = "Save";
        Initial_Data_Referesh_Material_Request_Item();
        Session.Remove("GP_Out_No");
        //Load_Data_Enquiry_Grid();


        Load_Data_Empty_Dept();
        Load_Data_Empty_Supp1();
        Load_Data_Empty_ItemCode();
        Load_Data_Empty_WareHouse();
        Load_Data_Empty_CostCenterName();
        Load_Data_Empty_ZoneName();
        Load_Data_Empty_BinName();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (txtOutQty.Text == "0.0" || txtOutQty.Text == "0" || txtOutQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Out Qty...');", true);
        }
        if (txtValue.Text == "0.0" || txtValue.Text == "0" || txtValue.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Value...');", true);
        }

        if (RdpStkType.SelectedValue == "1")
        {
            if (txtBinName.SelectedItem.Text == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Bin Details..');", true);
            }

            //check with Zone Name And Bin Name
            query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseName.SelectedValue + "' And BinName='" + txtBinName.SelectedItem.Text + "'";
            //And ZoneName='" + txtZoneName.SelectedItem.Text + "' And BinName='" + txtBinName.SelectedItem.Text + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Bin Details..');", true);
            }

            //check with Item Code And Item Name
            //query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemName.SelectedValue + "' ";
            //query = query + " And ItemShortName='" + txtItemName.SelectedItem.Text + "'";
            query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCodeHide.Value + "' ";
            query = query + " And ItemShortName='" + txtItemName.Text + "'";

            qry_dt = objdata.RptEmployeeMultipleDetails(query);
            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
            }

        }


        if (RdpStkType.SelectedValue == "1")
        {
            if (!ErrFlag)
            {
                //check Item Stock
                //query = "Select * from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemName.SelectedValue + "'";
                query = "Select * from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" +txtItemCodeHide.Value + "'";

                qry_dt = objdata.RptEmployeeMultipleDetails(query);
                if (qry_dt.Rows.Count != 0)
                {
                    if (qry_dt.Rows[0]["Stock_Qty"].ToString() == "0.00")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('There is no stock');", true);
                    }
                    else if (Convert.ToDecimal(qry_dt.Rows[0]["Stock_Qty"]) < Convert.ToDecimal(txtOutQty.Text))
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Issue Qty. It exceeds Stock Qty..');", true);
                    }

                }
                else
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Item in Stock Details');", true);
                }

                if (ErrFlag)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('There is no stock');", true);
                }
            }

        }




        if (!ErrFlag)
        {
            //UOM Code get
            string UOM_Code_Str = txtUOMCodeType.Text.ToString();

            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemName.SelectedValue.ToString().ToUpper())
                    //{
                    //    ErrFlag = true;
                    //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('this Item Already Added..');", true);
                    //}
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('this Item Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    if (RdpStkType.SelectedValue == "2")
                    {
                        dr["ItemCode"] = txtItemName.Text;
                    }
                    else
                    {
                        //dr["ItemCode"] = txtItemName.SelectedValue;
                        dr["ItemCode"] = txtItemCodeHide.Value;
                    }
                    //dr["ItemName"] = txtItemName.SelectedItem.Text;
                    dr["ItemName"] = txtItemName.Text;
                    dr["UOMCode"] = txtUOMCodeType.Text;//UOM_Code_Str;
                    dr["OutQty"] = txtOutQty.Text;
                    dr["Value"] = txtValue.Text;
                    dr["ZoneName"] = txtBinName.SelectedItem.Text;
                    dr["BinName"] = txtBinName.SelectedItem.Text;
                    dr["Remarks"] = txtRemarks.Text;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    //txtItemName.SelectedValue = "-Select-"; txtItemName.SelectedItem.Text = "-Select-"; txtUOMCodeType.Text = ""; txtOutQty.Text = "";
                    txtItemName.Text = "";  txtUOMCodeType.Text = ""; txtOutQty.Text = "";
                    txtUOMCodeType.Text = "";
                    txtValue.Text = ""; txtZoneName.SelectedValue = null; txtBinName.SelectedValue = null; txtRemarks.Text = "";
                }
            }
            else
            {
                dr = dt.NewRow();
                if (RdpStkType.SelectedValue == "1")
                {
                    dr["ItemCode"] = txtItemName.Text;
                }
                else
                {
                    //dr["ItemCode"] = txtItemName.SelectedValue;
                    dr["ItemCode"] = txtItemCodeHide.Value;
                }

                //dr["ItemName"] = txtItemName.SelectedItem.Text;
                dr["ItemName"] = txtItemName.Text;
                dr["UOMCode"] = txtUOMCodeType.Text;//UOM_Code_Str;
                dr["OutQty"] = txtOutQty.Text;
                dr["Value"] = txtValue.Text;
                dr["ZoneName"] = txtBinName.SelectedItem.Text;
                dr["BinName"] = txtBinName.SelectedItem.Text;
                dr["Remarks"] = txtRemarks.Text;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                //txtItemName.SelectedValue = "-Select-"; txtItemName.SelectedItem.Text = "-Select-"; txtUOMCodeType.Text = ""; txtOutQty.Text = "";
                //txtUOMCodeType.Text = "";
                txtItemName.Text = ""; txtUOMCodeType.Text = ""; txtOutQty.Text = "";
                txtUOMCodeType.Text = "";
                txtValue.Text = ""; txtZoneName.SelectedValue = null; txtBinName.SelectedValue = null; txtRemarks.Text = "";
            }
        }
    }

    private void Initial_Data_Referesh_Material_Request_Item()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("OutQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Value", typeof(string)));
        dt.Columns.Add(new DataColumn("ZoneName", typeof(string)));
        dt.Columns.Add(new DataColumn("BinName", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSourceS;
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_Out_No='" + txtGPOutNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["GP_Out_Date"].ToString();
            RdpStkType.SelectedValue = Main_DT.Rows[0]["StockType"].ToString();
            RdpGPoutType.SelectedValue = Main_DT.Rows[0]["GP_Type"].ToString();
            ddlSupplierName.SelectedValue = Main_DT.Rows[0]["Supp_Code"].ToString();
            ddlSupplierName.SelectedItem.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtSupplierDet.Text = Main_DT.Rows[0]["Supp_Det"].ToString();
            txtDeliveryDate.Text = Main_DT.Rows[0]["DeliveryDate"].ToString();
            txtThrough.Text = Main_DT.Rows[0]["Takenby"].ToString();
            txtIssuedBy.Value = Main_DT.Rows[0]["Issuedby"].ToString();
            txtPreferedBy.Value = Main_DT.Rows[0]["Preferedby"].ToString();
            txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtCostCenterName.SelectedValue = Main_DT.Rows[0]["CostCenterCode"].ToString();
            txtCostCenterName.SelectedItem.Text = Main_DT.Rows[0]["CostCenterName"].ToString();
            txtWarehouseName.SelectedValue = Main_DT.Rows[0]["WarehouseCode"].ToString();
            txtWarehouseName.SelectedItem.Text = Main_DT.Rows[0]["WarehouseName"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();

            //GatePass_Out_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select ItemCode,ItemName,UOMCode,OutQty,Value,ZoneName,BinName,Remarks from GatePass_Out_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_Out_No='" + txtGPOutNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("gate_pass_out_main.aspx");
    }

    private void Employee_Name_Load()
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        txtIssuedBy.Items.Clear();
        txtPreferedBy.Items.Clear();
        query = "Select * from MstEmployee where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by EmpName Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtIssuedBy.Items.Add("-Select-");
        txtPreferedBy.Items.Add("-Select-");
        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {
            txtIssuedBy.Items.Add(Main_DT.Rows[i]["EmpName"].ToString());
            txtPreferedBy.Items.Add(Main_DT.Rows[i]["EmpName"].ToString());
        }
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Gate Pass OUT");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Gate Pass OUT...');", true);
        //}
        //User Rights Check End

        query = "Select * from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_Out_No='" + txtGPOutNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Gate Pass OUT Details..');", true);
        }

        query = "Select * from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_Out_No='" + txtGPOutNo.Text + "' And GP_Out_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Gate Pass OUT Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            query = "Update GatePass_Out_Main set GP_Out_Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And GP_Out_No='" + txtGPOutNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            if (RdpStkType.SelectedValue == "1")
            {
                Stock_Add();
            }

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Gate Pass OUT Details Approved Successfully..');", true);
        }
    }

    private void Stock_Add()
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();

        query = "Select * from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtGPOutNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            query = "Delete from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtGPOutNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }

        DateTime transDate = Convert.ToDateTime(txtDate.Text);

        //Insert Stock_Transaction_Ledger
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            query = "Insert Into Stock_Transaction_Ledger(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
            query = query + " Trans_Type,DeptCode,DeptName,CostCenterCode,CostCenterName,ItemCode,ItemName,Open_Qty,Open_Value,PO_Qty,PO_Value,General_Qty,General_Value,Issue_Qty,";
            query = query + " Issue_Value,Issue_Return_Qty,Issue_Return_Value,GP_IN_Qty,GP_IN_Value,GP_OUT_Qty,GP_OUT_Value,Transfer_Qty,Transfer_Value,WarehouseCode,WarehouseName,ZoneName,BinName,Supp_Code,";
            query = query + " Supp_Name,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
            query = query + " '" + SessionFinYearVal + "','" + txtGPOutNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtDate.Text + "','GATE PASS OUT','" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "','" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "',";
            query = query + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
            query = query + " '0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','" + dt.Rows[i]["OutQty"].ToString() + "',";
            query = query + " '" + dt.Rows[i]["Value"].ToString() + "','0.0','0.0','" + txtWarehouseName.SelectedValue + "','" + txtWarehouseName.SelectedItem.Text + "','" + dt.Rows[i]["BinName"].ToString() + "',";
            query = query + " '" + dt.Rows[i]["BinName"].ToString() + "','" + ddlSupplierName.SelectedValue + "','" + ddlSupplierName.SelectedItem.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            CommonClass_Function.cls_Trans_Type = "GATE PASS OUT";
            CommonClass_Function.cls_ItemCode = dt.Rows[i]["ItemCode"].ToString();
            CommonClass_Function.cls_ItemName = dt.Rows[i]["ItemName"].ToString();
            CommonClass_Function.cls_Add_Qty = "0.0";
            CommonClass_Function.cls_Add_Value = "0.0";
            CommonClass_Function.cls_Minus_Qty = dt.Rows[i]["OutQty"].ToString();
            CommonClass_Function.cls_Minus_Value = dt.Rows[i]["Value"].ToString();

            CommonClass_Function.cls_DeptCode = txtDepartmentName.SelectedValue;
            CommonClass_Function.cls_DeptName = txtDepartmentName.SelectedItem.Text;
            CommonClass_Function.cls_CostCenterCode = txtCostCenterName.SelectedValue;
            CommonClass_Function.cls_CostCenterName = txtCostCenterName.SelectedItem.Text;
            CommonClass_Function.cls_WarehouseCode = txtWarehouseName.SelectedValue;
            CommonClass_Function.cls_WarehouseName = txtWarehouseName.SelectedItem.Text;
            CommonClass_Function.cls_ZoneName = dt.Rows[i]["BinName"].ToString();
            CommonClass_Function.cls_BinName = dt.Rows[i]["BinName"].ToString();
            CommonClass_Function.cls_Supp_Code = ddlSupplierName.SelectedValue;
            CommonClass_Function.cls_Supp_Name = ddlSupplierName.SelectedItem.Text;
            CommonClass_Function.cls_UserID = SessionUserID;
            CommonClass_Function.cls_UserName = SessionUserName;
            //CommonClass_Function.cls_Stock_Qty = "";
            //CommonClass_Function.cls_Stock_Value = "";

            CommonClass_Function.Stock_Ledger_And_Current_Stock_Add(SessionCcode, SessionLcode, SessionFinYearCode, SessionFinYearVal, txtGPOutNo.Text, txtDate.Text);
        }




    }

    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        //modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();


    }


    protected void txtWarehouseName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_ZoneName();
        Load_Data_Empty_BinName();
    }
    protected void txtZoneName_SelectedIndexChanged(object sender, EventArgs e)
    {

        Load_Data_Empty_BinName();
    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtWarehouseName.Text = Convert.ToString(e.CommandName);


        Load_Data_Empty_ZoneName();
        Load_Data_Empty_BinName();
    }

    protected void btnZoneName_Click(object sender, EventArgs e)
    {
        //modalPop_ZoneName.Show();
    }

    private void Load_Data_Empty_ZoneName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtZoneName.Items.Clear();
        query = "Select ZoneName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And WarehouseCode='" + txtWarehouseName.SelectedValue + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtZoneName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["ZoneName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtZoneName.DataTextField = "ZoneName";

        txtZoneName.DataBind();
    }

    protected void GridViewClick_ZoneName(object sender, CommandEventArgs e)
    {
        txtZoneName.Text = Convert.ToString(e.CommandArgument);

        Load_Data_Empty_BinName();
    }

    protected void btnBinName_Click(object sender, EventArgs e)
    {
        //modalPop_BinName.Show();
    }

    private void Load_Data_Empty_BinName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtBinName.Items.Clear();
        query = "Select BinName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And WarehouseCode='" + txtWarehouseName.SelectedValue + "'"; //And ZoneName='" + txtZoneName.SelectedItem.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtBinName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["BinName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtBinName.DataTextField = "BinName";

        txtBinName.DataBind();
    }

    protected void GridViewClick_BinName(object sender, CommandEventArgs e)
    {
        txtBinName.Text = Convert.ToString(e.CommandArgument);
    }


    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

   
    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlSupplierName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["SuppCode"] = "-Select-";
        dr["SuppName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlSupplierName.DataTextField = "SuppName";
        ddlSupplierName.DataValueField = "SuppCode";

        ddlSupplierName.DataBind();
       

    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        ddlSupplierName.SelectedValue = Convert.ToString(e.CommandArgument);
        ddlSupplierName.SelectedItem.Text = Convert.ToString(e.CommandName);

        DataTable DT = new DataTable();
        string query = "Select *from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " and SuppCode='" + ddlSupplierName.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtSupplierDet.Text = DT.Rows[0]["Address1"].ToString() + " " + DT.Rows[0]["Address2"].ToString() + " " + DT.Rows[0]["City"].ToString(); 
        }
        else
        {
            txtSupplierDet.Text = "";
        }
    }


    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select ItemCode,ItemShortName as ItemName,PurchaseUOM as UOMCode,ValuationType from MstItemMaster where Ccode='" + SessionCcode + "' And ";
        //query = query + " Lcode ='" + SessionLcode + "'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //txtItemName.DataSource = DT;

        //DataRow dr = DT.NewRow();

        //dr["ItemCode"] = "-Select-";
        //dr["ItemName"] = "-Select-";

        //DT.Rows.InsertAt(dr, 0);

        //txtItemName.DataTextField = "ItemName";
        //txtItemName.DataValueField = "ItemCode";

        //txtItemName.DataBind();

        string query = "";
        DataTable DT = new DataTable();

        query = "Select  ItemCode,ItemShortName as ItemName,PurchaseUOM as UOMCode,ValuationType from MstItemMaster where Ccode='" + SessionCcode + "' And ";
        query = query + " Lcode ='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        //txtItemName.DataSource = DT;

        //DataRow dr = DT.NewRow();

        //dr["ItemCode"] = "-Select-";
        //dr["ItemName"] = "-Select-";

        //DT.Rows.InsertAt(dr, 0);

        //txtItemName.DataTextField = "ItemName";
        //txtItemName.DataValueField = "ItemCode";

        //txtItemName.DataBind();
        

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string itemcode = commandArgs[0];
        string UOMCode = commandArgs[1];
       


        txtItemCodeHide.Value = itemcode;
        txtItemName.Text = Convert.ToString(e.CommandName);
        //txtItemUOMHide.Value = UOMCode;
        txtUOMCodeType.Text = UOMCode;
        
    }

    protected void btnCostCenterName_Click(object sender, EventArgs e)
    {
        //modalPop_CostCenterName.Show();
    }
    private void Load_Data_Empty_CostCenterName()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtCostCenterName.Items.Clear();
        query = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCostCenterName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["CostcenterCode"] = "-Select-";
        dr["CostcenterName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCostCenterName.DataTextField = "CostcenterName";
        txtCostCenterName.DataValueField = "CostcenterCode";
        txtCostCenterName.DataBind();

    }

    protected void GridViewClick_CostCenterName(object sender, CommandEventArgs e)
    {
        //txtCostCenterCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtCostCenterName.Text = Convert.ToString(e.CommandName);
    }

    protected void ddlSupplierName_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable DT = new DataTable();
        string query = "Select *from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " and SuppCode='" + ddlSupplierName.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtSupplierDet.Text = DT.Rows[0]["Address1"].ToString() + " " + DT.Rows[0]["Address2"].ToString() + " " + DT.Rows[0]["City"].ToString();
        }
        else
        {
            txtSupplierDet.Text = "";
        }
    }

    protected void txtItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        //string query = "";
        //DataTable DT = new DataTable();

        ////query = "Select PurchaseUOM as UOMCode,ValuationType from MstItemMaster where Ccode='" + SessionCcode + "' And ";
        ////query = query + " Lcode ='" + SessionLcode + "' and ItemCode='" + txtItemName.SelectedValue + "'";
        //query = "Select PurchaseUOM as UOMCode,ValuationType from MstItemMaster where Ccode='" + SessionCcode + "' And ";
        //query = query + " Lcode ='" + SessionLcode + "' and ItemCode='" + txtItemCodeHide.Value + "'";

        //DT = objdata.RptEmployeeMultipleDetails(query);

        //txtUOMCodeType.Text = DT.Rows[0]["UOMCode"].ToString();
    }
}
