﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Inventory_Adjustment_Stock_Entry_Approval_View : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionAdjust_StockNo;
    string query;
    DataTable Std_Pur_Main = new DataTable();
    DataTable Std_Pur_Sub = new DataTable();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Stock Adjustment Entry Approve View";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Inventory"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            if (Session["Adjust_StockNo"] == null)
            {
                SessionAdjust_StockNo = "";
            }
            else
            {
                SessionAdjust_StockNo = Session["Adjust_StockNo"].ToString();
                txtTransNo.Text = SessionAdjust_StockNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Std Purchase Order");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Std Purchase Order...');", true);
        //}
        //User Rights Check End

        //query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtTransNo.Text + "'";
        //DT_Check = objdata.RptEmployeeMultipleDetails(query);
        //if (DT_Check.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Std Purchase Order Details..');", true);
        //}


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Adjustment_No='" + txtTransNo.Text + "' And Status = '1'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Order Details Approved.. Cannot Cancel it..');", true);

            }
        }


        DataTable dtd_REqNo = new DataTable();

        if (!ErrFlag)
        {

            query = "Select * from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Adjustment_No='" + txtTransNo.Text + "' And Status = '2'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Order Details Already Cancelled..');", true);
            }
            if (!ErrFlag)
            {
                query = "Update Adjustment_Stock_Entry_Main set Status='2',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Adjustment_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Stock Adjustment Entry Details Cancelled Successfully..');", true);
            }
        }
        Initial_Data_Referesh();
        Session["Adjust_StockNo"] = txtTransNo.Text;
        btnSearch_Click(sender, e);

    }

    private void Clear_All_Field()
    {
        txtTransNo.Text = ""; txtDate.Text = ""; txtAdjustmentBy.Text = "";
        txtRemarks.Text = "";

        Initial_Data_Referesh();
        Session.Remove("Adjust_StockNo");
        //Load_Data_Enquiry_Grid();
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Std Purchase Order");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Std Purchase Order...');", true);
        //}
        //User Rights Check End

        //query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtTransNo.Text + "'";
        //DT_Check = objdata.RptEmployeeMultipleDetails(query);
        //if (DT_Check.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Std Purchase Order Details..');", true);
        //}


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Adjustment_No='" + txtTransNo.Text + "' And Status = '2'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Order Details Cancelled.. Cannot Approved it..');", true);

            }
        }


        DataTable dtd_REqNo = new DataTable();

        if (!ErrFlag)
        {

            query = "Select * from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Adjustment_No='" + txtTransNo.Text + "' And Status = '1'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Stock Adjustment Entry Details Already Approved..');", true);
            }
            if (!ErrFlag)
            {
                query = "Update Adjustment_Stock_Entry_Main set Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Adjustment_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);

                Stock_Add();

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Stock Adjustment Entry Details Approved Successfully..');", true);
            }
        }
        Initial_Data_Referesh();
        Session["Adjust_StockNo"] = txtTransNo.Text;
        btnSearch_Click(sender, e);

    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("StockQty", typeof(string)));
        dt.Columns.Add(new DataColumn("StockValue", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseCode", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseName", typeof(string)));
        dt.Columns.Add(new DataColumn("ZoneName", typeof(string)));
        dt.Columns.Add(new DataColumn("BinName", typeof(string)));
        dt.Columns.Add(new DataColumn("AdjustQty", typeof(string)));
        dt.Columns.Add(new DataColumn("AdjustValue", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Std_Purchase_Order_Main Table Load
        query = "select * from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Adjustment_No='" + txtTransNo.Text + "'";
        Std_Pur_Main = objdata.RptEmployeeMultipleDetails(query);
        if (Std_Pur_Main.Rows.Count > 0)
        {
            txtTransNo.Text = Std_Pur_Main.Rows[0]["Adjustment_No"].ToString();
            txtDate.Text = Std_Pur_Main.Rows[0]["Adjustmen_Date"].ToString();
            
            txtAdjustmentBy.Text = Std_Pur_Main.Rows[0]["AdjustmenBy"].ToString();
            txtDeptCodeHide.Value = Std_Pur_Main.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.Text = Std_Pur_Main.Rows[0]["DeptName"].ToString();
            txtCostCenterCodeHide.Value = Std_Pur_Main.Rows[0]["CostCenterCode"].ToString();
            txtCostCenterName.Text = Std_Pur_Main.Rows[0]["CostCenterName"].ToString();
            txtRemarks.Text = Std_Pur_Main.Rows[0]["Remarks"].ToString();
            

            //Std_Purchase_Order_Main_Sub Table Load

            DataTable dt = new DataTable();

            query = "Select ItemCode,ItemName,StockQty,StockValue,AdjustQty,AdjustValue,WarehouseCode,WarehouseName,ZoneName,BinName from Adjustment_Stock_Entry_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Adjustment_No='" + txtTransNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            

            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();


            }

        else
        {
            Clear_All_Field();
        }


    }

    protected void btnPending_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Std Purchase Order");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Std Purchase Order...');", true);
        //}
        //User Rights Check End

        //query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtTransNo.Text + "'";
        //DT_Check = objdata.RptEmployeeMultipleDetails(query);
        //if (DT_Check.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Std Purchase Order Details..');", true);
        //}


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Adjustment_No='" + txtTransNo.Text + "' And Status = '2'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Stock Adjustment Entry Details Cancelled.. Cannot put to Pending..');", true);

            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Adjustment_No='" + txtTransNo.Text + "' And Status = '1'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Stock Adjustment Entry Details Approved.. Cannot put to Pending..');", true);

            }
        }


        DataTable dtd_REqNo = new DataTable();

        if (!ErrFlag)
        {

            query = "Select * from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Adjustment_No='" + txtTransNo.Text + "' And Status = '3'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Stock Adjustment Entry Details Already in Pending..');", true);
            }
            if (!ErrFlag)
            {
                query = "Update Adjustment_Stock_Entry_Main set Status='3',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Adjustment_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);



                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Stock Adjustment Entry Details Put in Pending..');", true);
            }
        }
        Initial_Data_Referesh();
        Session["Adjust_StockNo"] = txtTransNo.Text;
        btnSearch_Click(sender, e);

    }
    protected void btnback_Click(object sender, EventArgs e)
    {
        Session.Remove("Adjust_StockNo");
        Response.Redirect("Adjustment_Stock_Entry_Approval.aspx");
    }

    private void Stock_Add()
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();

        //query = "Select * from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        //query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
        //DT_Check = objdata.RptEmployeeMultipleDetails(query);
        //if (DT_Check.Rows.Count != 0)
        //{
        //    query = "Delete from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        //    query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
        //    objdata.RptEmployeeMultipleDetails(query);
        //}


        DateTime transDate = Convert.ToDateTime(txtDate.Text);

        //Insert Stock_Transaction_Ledger
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            //query = "Insert Into Stock_Transaction_Ledger(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
            //query = query + " Trans_Type,ItemCode,ItemName,Open_Qty,Open_Value,PO_Qty,PO_Value,General_Qty,General_Value,Issue_Qty,";
            //query = query + " Issue_Value,Issue_Return_Qty,Issue_Return_Value,GP_IN_Qty,GP_IN_Value,GP_OUT_Qty,GP_OUT_Value,Transfer_Qty,Transfer_Value,WarehouseCode,WarehouseName,ZoneName,BinName,Supp_Code,";
            //query = query + " Supp_Name,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
            //query = query + " '" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtDate.Text + "','Stock Adjustment Entry',";
            //query = query + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
            //query = query + " '0.0','0.0','0.0','0.0','0.0','0.0','" + dt.Rows[i]["IssueQty"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "','0.0',";
            //query = query + " '0.0','0.0','0.0','0.0','0.0','0.0','0.0','" + dt.Rows[i]["WarehouseCode"].ToString() + "',";
            //query = query + " '" + dt.Rows[i]["WarehouseName"].ToString() + "','" + dt.Rows[i]["ZoneName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "',";
            //query = query + " '','','" + SessionUserID + "','" + SessionUserName + "')";
            //objdata.RptEmployeeMultipleDetails(query);

            CommonClass_Function.cls_Trans_Type = "Stock Adjustment Entry";
            CommonClass_Function.cls_ItemCode = dt.Rows[i]["ItemCode"].ToString();
            CommonClass_Function.cls_ItemName = dt.Rows[i]["ItemName"].ToString();
            if (Convert.ToDecimal(dt.Rows[0]["StockQty"]) < Convert.ToDecimal(dt.Rows[i]["AdjustQty"]))
            {
                CommonClass_Function.cls_Add_Qty = dt.Rows[i]["AdjustQty"].ToString();
                CommonClass_Function.cls_Add_Value = dt.Rows[i]["AdjustValue"].ToString();
                CommonClass_Function.cls_Minus_Qty = "0.0";
                CommonClass_Function.cls_Minus_Value = "0.0";
            }
            else
            {
                CommonClass_Function.cls_Add_Qty = "0.0";
                CommonClass_Function.cls_Add_Value = "0.0";
                CommonClass_Function.cls_Minus_Qty = dt.Rows[i]["AdjustQty"].ToString();
                CommonClass_Function.cls_Minus_Value = dt.Rows[i]["AdjustValue"].ToString();
            }



            CommonClass_Function.cls_DeptCode = txtDeptCodeHide.Value;
            CommonClass_Function.cls_DeptName = txtDepartmentName.Text;
            CommonClass_Function.cls_CostCenterCode = txtCostCenterCodeHide.Value;
            CommonClass_Function.cls_CostCenterName = txtCostCenterName.Text;
            CommonClass_Function.cls_WarehouseCode = dt.Rows[i]["WarehouseCode"].ToString();
            CommonClass_Function.cls_WarehouseName = dt.Rows[i]["WarehouseName"].ToString();
            CommonClass_Function.cls_ZoneName = dt.Rows[i]["ZoneName"].ToString();
            CommonClass_Function.cls_BinName = dt.Rows[i]["BinName"].ToString();
            CommonClass_Function.cls_Supp_Code = "";
            CommonClass_Function.cls_Supp_Name = "";
            CommonClass_Function.cls_UserID = SessionUserID;
            CommonClass_Function.cls_UserName = SessionUserName;
            //CommonClass_Function.cls_Stock_Qty = "";
            //CommonClass_Function.cls_Stock_Value = "";

            CommonClass_Function.Stock_Ledger_And_Current_Stock_Add(SessionCcode, SessionLcode, SessionFinYearCode, SessionFinYearVal, txtTransNo.Text, txtDate.Text);
        }




    }





}
