﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="Service_Entry.aspx.cs" Inherits="Inventory_Service_Entry" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css" />
    <!--Ajax popup start-->
    <link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    

 


    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <h4>
                <li class="active">Service Entry</li>
            </h4>
        </ol>
    </div>
    <div id="main-wrapper" class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="col-md-9">
                    <div class="panel panel-white">
                        <div class="panel panel-primary">
                            <div class="panel-heading clearfix">
                                <h4 class="panel-title">Service Entry</h4>
                            </div>
                        </div>
                        <form class="form-horizontal">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Service Entry No<span class="mandatory">*</span></label>
                                            <asp:Label ID="txtSENo" runat="server" class="form-control"></asp:Label>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Date</label>
                                            <asp:TextBox ID="txtSEDate" MaxLength="20" class="form-control date-picker" runat="server" AutoComplete="Off"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtSEDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1"
                                                runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtSEDate" ValidChars="0123456789./">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Invoice No</label>
                                            <asp:TextBox ID="txtInvNo" MaxLength="20" class="form-control" runat="server"></asp:TextBox>

                                        </div>

                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Invoice Date</label>
                                            <asp:TextBox ID="txtInvDate" MaxLength="20" class="form-control date-picker" runat="server" AutoComplete="Off"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtInvDate" ValidChars="0123456789./">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Type</label>
                                            <asp:RadioButtonList ID="RdpStkType" class="form-control" runat="server"
                                                RepeatColumns="2">
                                                <asp:ListItem Value="1" Text="Stock" style="padding-right: 20px" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="No Stock"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                             
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName">Supplier Name</label>
                                            <asp:DropDownList ID="ddlSupplier" runat="server" class="js-states form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSupplier_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName">Supplier Details</label>
                                            <asp:TextBox ID="txtSupplierDet" class="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName">Department Name</label>

                                            <asp:DropDownList ID="txtDepartmentName" runat="server" class="js-states form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtDepartmentName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName">Engineer Name</label>
                                            <asp:TextBox ID="txtSerName" class="form-control"  runat="server"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName">Mobile No</label>
                                            <asp:TextBox ID="txtMobile" class="form-control"  runat="server"></asp:TextBox>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName">Others</label>
                                            <asp:TextBox ID="txtOthers" class="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="timeline-options">
                                        <h4 class="panel-title">Item Details</h4>
                                    </div>

                                    <div class="col-md-12" runat="server" visible="false">
                                        <div class="row">
                                            <div class="form-group col-md-5"></div>
                                            <div class="form-group col-md-2">
                                                <asp:CheckBox ID="chkFreeServ" runat="server" Text="Free Service" />
                                            </div>
                                        </div>
                                        <br />
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                <label for="exampleInputName">Item Name</label>
                                                <asp:TextBox ID="txtItemName" runat="server" class="form-control">
                                                </asp:TextBox>
                                            </div>

                                            <div class="form-group col-md-2">
                                                <label for="exampleInputName">UOM</label>
                                                <asp:TextBox ID="txtUom" class="form-control" runat="server" Text="Nos"></asp:TextBox>
                                            </div>

                                            <div class="form-group col-md-2">
                                                <label for="exampleInputName">Qty</label>
                                                <asp:TextBox ID="txtQty" class="form-control" runat="server"></asp:TextBox>
                                            </div>
                                            

                                            <div class="form-group col-md-2">
                                                <label for="exampleInputName">Rate</label>
                                                <asp:TextBox ID="txtRate" class="form-control" runat="server" OnTextChanged="txtRate_TextChanged" 
                                                    AutoPostBack="true"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" 
                                                    FilterType="Custom,Numbers" TargetControlID="txtRate" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <label for="exampleInputName">Value</label>
                                                <asp:TextBox ID="txtValue" class="form-control" runat="server" Enabled="false"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtValue" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2">
                                                <label for="exampleInputName">Disc. Per</label>
                                                <asp:TextBox ID="txtDiscPer" class="form-control" runat="server" Text="0.0" Enabled="false" AutoPostBack="true" OnTextChanged="txtDiscPer_TextChanged"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtDiscPer" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>

                                            <div class="form-group col-md-2">
                                                <label for="exampleInputName">Disc. Amt</label>
                                                <asp:TextBox ID="txtDiscAmt" class="form-control" runat="server" Text="0.0" AutoPostBack="true" OnTextChanged="txtDiscAmt_TextChanged"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtDiscAmt" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>

                                            <div class="form-group col-md-2">
                                                <label for="exampleInputName">GST Type</label>
                                                <asp:DropDownList ID="ddlGstType" runat="server" class="js-states form-control" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlGstType_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>

                                            <div class="form-group col-md-1">
                                                <label for="exampleInputName">%</label>
                                                <asp:TextBox ID="txtCGSTPer" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtCGSTPer" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>

                                            <div class="form-group col-md-2">
                                                <label for="exampleInputName">CGST Amt</label>
                                                <asp:TextBox ID="txtCGSTAmt" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtCGSTAmt" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>

                                            <div class="form-group col-md-1">
                                                <label for="exampleInputName">%</label>
                                                <asp:TextBox ID="txtSGSTPer" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtSGSTPer" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>

                                            <div class="form-group col-md-2">
                                                <label for="exampleInputName">SGST Amt</label>
                                                <asp:TextBox ID="txtSGSTAmt" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtSGSTAmt" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-md-1">
                                                <label for="exampleInputName">%</label>
                                                <asp:TextBox ID="txtIGSTPer" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtIGSTPer" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>

                                            <div class="form-group col-md-2">
                                                <label for="exampleInputName">IGST Amt</label>
                                                <asp:TextBox ID="txtIGSTAmt" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtIGSTAmt" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>

                                            <div class="form-group col-md-2">
                                                <label for="exampleInputName">Total Value</label>
                                                <asp:TextBox ID="txtTotAmt" class="form-control" runat="server" Text="0.0"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtTotAmt" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>

                                           
                                            
                                            <div class="form-group col-md-4">
                                                <label for="exampleInputName">Remarks</label>
                                                <asp:TextBox ID="txtRemarks" class="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <div class="form-group col-md-1">
                                                <br />
                                                <asp:Button ID="btnAddItem" Width="50" Height="30" class="btn-success" runat="server" Text="Add" ValidationGroup="Item_Validate_Field" OnClick="btnAddItem_Click" />
                                            </div>
                                        </div>
                                    </div>
                              
                                    <!-- table start -->
                                    <div class="col-md-12">
                                         <br /><br /><br />   
                                        <div class="row">
                                            <asp:Panel ID="PanelmaterialRequestItem" runat="server" ScrollBars="Horizontal">
                                                <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                    <HeaderTemplate>
                                                        <table id="example" class="table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>S.No</th>
                                                                    <th>Item Name</th>
                                                                    <th>UOM</th>
                                                                    <th>Qty</th>
                                                                    <th>Rate</th>
                                                                    <th>Value</th>
                                                                    <th>Disc Amt</th>
                                                                    <th>GST Type</th>
                                                                    <th>CGST</th>
                                                                    <th>SGST</th>
                                                                    <th>IGST</th>
                                                                    <th>Tot Amt</th>
                                                                    <th>Remarks</th>
                                                                    <th>Mode</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tbody>
                                                            <tr>
                                                                <td><%# Container.ItemIndex + 1 %></td>
                                                                <td><%# Eval("ItemName")%></td>
                                                                <td><%# Eval("UOM")%></td>
                                                                <td><%# Eval("Qty")%></td>
                                                                <td><%# Eval("Rate")%></td>
                                                                <td><%# Eval("Value")%></td>
                                                                <td><%# Eval("DiscAmt")%></td>
                                                                <td><%# Eval("GSTType")%></td>
                                                                <td><%# Eval("CGSTAmt")%></td>
                                                                <td><%# Eval("SGSTAmt")%></td>
                                                                <td><%# Eval("IGSTAmt")%></td>
                                                                <td><%# Eval("TotAmt")%></td>
                                                                <td><%# Eval("Remarks")%></td>
                                                                <td>
                                                                    <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                        Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("ItemName")%>'
                                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                                    </asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <!-- table End -->

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="form-group col-md-10"></div>
                                            <div class="form-group col-md-2">
                                                <label for="exampleInputName">Total Amt</label>
                                                <asp:TextBox ID="txtSubTotAmt" class="form-control" runat="server" Text="0.0"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtSubTotAmt" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="form-group col-md-6"></div>

                                            <div class="form-group col-md-2">
                                                <label for="exampleInputName">Packing Amt</label>
                                                <asp:TextBox ID="txtPckingGST" class="form-control" runat="server" Text="0.0" AutoPostBack="true" OnTextChanged="txtPckingGST_TextChanged"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtPckingGST" ValidChars="0123456789.">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>

                                            <div class="form-group col-md-2">
                                                <label for="exampleInputName">Other Amt</label>
                                                <asp:TextBox ID="txtOtherAmt" class="form-control" runat="server" Text="0"
                                                    AutoPostBack="true" OnTextChanged="txtOtherAmt_TextChanged"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtOtherAmt" ValidChars="0123456789.-">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>

                                            <div class="form-group col-md-2">
                                                <label for="exampleInputName">Net Amt</label>
                                                <asp:Label ID="txtNetAmt" runat="server" class="form-control"></asp:Label>

                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group row"></div>
                                    <!-- Button start -->
                                    <div class="txtcenter">
                                        <asp:Button ID="btnSave" class="btn btn-success" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                        <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                        <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" Text="Back" OnClick="btnBackRequest_Click" />
                                        <asp:Button ID="btnApprove" class="btn btn-success" runat="server" Text="Approve" OnClick="btnApprove_Click" />
                                    </div>
                                    <!-- Button end -->
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- panel white end -->
                </div>
                <!-- col-9 end -->
                <!-- Dashboard start -->
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-white" style="height: 100%;">
                        <div class="panel-heading">
                            <h4 class="panel-title">Dashboard Details</h4>
                            <div class="panel-control">
                            </div>
                        </div>
                        <div class="panel-body">
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Dashboard End -->
                <div class="col-md-2"></div>

            </div>
            <!-- col 12 end -->
        </div>
        <!-- row end -->
    </div>
    <!-- main-wrapper end -->
    
    <!-- Script Section-->
    <script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
    <script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
    <script src='<%= ResolveUrl("../assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js") %>'></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();
            $('.js-states').select2();
            $('.date-picker').datepicker(
                  {
                      format: "dd/mm/yyyy",
                      autoclose: true
                  });
        });
    </script>
    
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.js-states').select2();
                    $('.date-picker').datepicker(
                    {
                     format: "dd/mm/yyyy",
                     autoclose: true
                 });
                }
            });
        };
    </script>

    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>

    
    <script type="text/javascript">
        function pageLoad(sender, args) {
            if (!args.get_isPartialLoad()) {
                //  add our handler to the document's
                //  keydown event
                $addHandler(document, "keydown", onKeyDown);
            }
        }

        function onKeyDown(e) {
            if (e && e.keyCode == Sys.UI.Key.esc) {
                // if the key pressed is the escape key, dismiss the dialog

                $find('GatePassOut_Close').hide();
                $find('WareHouse_Close').hide();
                $find('Dept_Close').hide();
                $find('CostCenterName_Close').hide();
                $find('ZoneName_Close').hide();
                $find('BinName_Close').hide();
                $find('Item_Close').hide();
            }
        }
    </script>
    <!--Ajax popup End-->

</asp:Content>

