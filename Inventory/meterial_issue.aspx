<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="meterial_issue.aspx.cs" Inherits="Inventory_meterial_issue" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css" />
    <!--Ajax popup start-->
    <link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <%--Warehouse Name Select List Script Start--%>
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_Warehouse_Name();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Warehouse_Name);
        });
        function initializer_Warehouse_Name() {
            $("#<%=txtWarehouseName.ClientID %>").autocomplete({                 
                source: function (request, response) {                        
                      $.ajax({ 
                          url: '<%=ResolveUrl("../List_Service.asmx/GetWarehouse_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) {
                                  return {
                                      label: item.split('-')[0] + "-" + item.split('-')[1],
                                      val: item.split('-')[1],
                                      text_val:item.split('-')[0],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtWarehouseName.ClientID %>").val(i.item.val);
                      //$("#<%=txtWarehouseName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtWarehouseName.ClientID %>").val(i.item.text_val);
                      $("#<%=txtWarehouseCodeHide.ClientID %>").val(i.item.val);
                   return false;
                  }
              });
          }
      </script>--%>
    <%--Warehouse Name Select List Script End--%>

    <%--Department Name Select List Script Start--%>
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_Dept_Code();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Dept_Code);
        });
        function initializer_Dept_Code() {
            $("#<%=txtDepartmentName.ClientID %>").autocomplete({                 
                source: function (request, response) {                        
                      $.ajax({ 
                          url: '<%=ResolveUrl("../List_Service.asmx/GetDeptFull_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) {
                                  return {
                                      label: item.split('|')[0] + "|" + item.split('|')[1],
                                      val: item.split('|')[1],
                                      text_val:item.split('|')[0],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtDepartmentName.ClientID %>").val(i.item.val);
                      //$("#<%=txtDepartmentName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtDepartmentName.ClientID %>").val(i.item.text_val);
                      $("#<%=txtDeptCodeHide.ClientID %>").val(i.item.val);
                   return false;
                  }
              });
          }
      </script>--%>
    <%--Department Name Select List Script End--%>

    <%--Cost Center Name Select List Script Start--%>
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_CostCenterName();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_CostCenterName);
        });
        function initializer_CostCenterName() {
            $("#<%=txtCostCenterName.ClientID %>").autocomplete({                 
                source: function (request, response) {                        
                      $.ajax({ 
                          url: '<%=ResolveUrl("../List_Service.asmx/GetCostCenter_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) {
                                  return {
                                      label: item.split('-')[0] + "-" + item.split('-')[1],
                                      val: item.split('-')[1],
                                      text_val:item.split('-')[0],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtCostCenterName.ClientID %>").val(i.item.val);
                      //$("#<%=txtCostCenterName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtCostCenterName.ClientID %>").val(i.item.text_val);
                      $("#<%=txtCostCenterCodeHide.ClientID %>").val(i.item.val);
                   return false;
                  }
              });
          }
      </script>--%>
    <%--Cost Center Name Select List Script End--%>

    <%--Material Request No Select List Script Start--%>
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_Material_Request_No();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Material_Request_No);
        });
        function initializer_Material_Request_No() {
            $("#<%=txtMaterialRequestNo.ClientID %>").autocomplete({                 
                source: function (request, response) {                        
                      $.ajax({ 
                          url: '<%=ResolveUrl("../List_Service.asmx/GetMaterial_RequestNo_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) {
                                  return {
                                      label: item.split('|')[0],
                                      val: item.split('|')[1],
                                      text_val:item.split('|')[0],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtMaterialRequestNo.ClientID %>").val(i.item.val);
                      //$("#<%=txtMaterialRequestNo.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtMaterialRequestNo.ClientID %>").val(i.item.text_val);
                      $("#<%=txtMaterialRequestDate.ClientID %>").val(i.item.val);
                   return false;
                  }
              });
          }
      </script>--%>
    <%--Material Request No Select List Script End--%>

    <%--Item Select List Script Saart--%>
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_Item();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Item);
        });
        function initializer_Item() {
            $("#<%=txtItemName.ClientID %>").autocomplete({                 
                source: function (request, response) {
                      $.ajax({
                          url: '<%=ResolveUrl("../List_Service.asmx/GetItemCodeSearchMaterialRequestNoWise") %>',
                          data: "{ 'prefix': '" + request.term + "','Mat_Req_No': '" + $('#<%=txtMaterialRequestNo.ClientID %>').val() + "','IssueType':'" + $("[id*=RdpIssueType] input:checked").val() + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) { 
                                  return {                                  
                                      label: item.split('|')[0],
                                      val: item.split('|')[0],
                                      Item_Code:item.split('|')[1],
                                      UOM_Code:item.split('|')[2],
                                      Request_Qty:item.split('|')[3],
                                      Request_Date:item.split('|')[4],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtItemName.ClientID %>").val(i.item.val);
                      //$("#<%=txtItemName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtItemName.ClientID %>").val(i.item.val);
                      $("#<%=txtItemCodeHide.ClientID %>").val(i.item.Item_Code);
                      $("#<%=txtItemUOMHide.ClientID %>").val(i.item.UOM_Code);
                      $("#<%=txtRequestQty.ClientID %>").val(i.item.Request_Qty);
                      $("#<%=txtRequestDate.ClientID %>").val(i.item.Request_Date);
                   return false;
                  }
              }); 
          }
      </script>--%>
    <%--Item Select List Script End--%>

    <%--Zone Name Select List Script Saart--%>
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_ZoneName();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_ZoneName);
        });
        function initializer_ZoneName() {
            $("#<%=txtZoneName.ClientID %>").autocomplete({                 
                source: function (request, response) {
                      $.ajax({
                          url: '<%=ResolveUrl("../List_Service.asmx/GetZoneListDet") %>',
                          data: "{ 'prefix': '" + request.term + "','WarehouseName': '" + $('#<%=txtWarehouseCodeHide.ClientID %>').val() + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) { 
                                  return {                                  
                                      label: item.split('-')[0],
                                      val: item.split('-')[0],                                     
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtZoneName.ClientID %>").val(i.item.val);
                      //$("#<%=txtZoneName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtZoneName.ClientID %>").val(i.item.val);
                   return false;
                  }
              }); 
          }
      </script>--%>
    <%--Zone Name Select List Script End--%>

    <%--Bin Name Select List Script Saart--%>
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_BinName();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_BinName);
        });
        function initializer_BinName() {
            $("#<%=txtBinName.ClientID %>").autocomplete({                 
                source: function (request, response) {
                      $.ajax({
                          url: '<%=ResolveUrl("../List_Service.asmx/GetBinListDet") %>',
                          data: "{ 'prefix': '" + request.term + "','WarehouseName': '" + $('#<%=txtWarehouseCodeHide.ClientID %>').val() + "','ZoneName': '" + $('#<%=txtZoneName.ClientID %>').val() + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) { 
                                  return {                                  
                                      label: item.split('-')[0],
                                      val: item.split('-')[0],                                     
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtBinName.ClientID %>").val(i.item.val);
                      //$("#<%=txtBinName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtBinName.ClientID %>").val(i.item.val);
                   return false;
                  }
              }); 
          }
      </script>--%>
    <%--Zone Name Select List Script End--%>


    <div class="page-breadcrumb">
        <ol class="breadcrumb container">
            <h4>
                <li class="active">Material Issue</li>
            </h4>
        </ol>
    </div>
    <div id="main-wrapper" class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="col-md-9">
                    <div class="panel panel-white">
                        <div class="panel panel-primary">
                            <div class="panel-heading clearfix">
                                <h4 class="panel-title">Material Issue</h4>
                            </div>
                        </div>
                        <form class="form-horizontal">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Issue No<span class="mandatory">*</span></label>
                                            <asp:Label ID="txtIssueNo" runat="server" class="form-control"></asp:Label>

                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Date</label>
                                            <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server" AutoComplete="Off"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtDate" ValidChars="0123456789./">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>
                                        <div class="form-group col-md-4" runat="server" visible="false">
                                            <label for="exampleInputName">Ledger No</label>
                                            <asp:TextBox ID="txtLedgerNo" class="form-control" runat="server"></asp:TextBox>
                                            <%--<asp:RequiredFieldValidator ControlToValidate="txtLedgerNo" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
                                        </div>
                                        <div class="form-group col-md-4" runat="server" visible="false">
                                            <label for="exampleInputName">Cost Center Name</label>
                                            <asp:DropDownList ID="txtCostCenterName" runat="server" class="js-states form-control">
                                            </asp:DropDownList>
                                            <%-- <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtCostCenterName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator> --%>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName">Taken By</label>
                                            <select name="txtTakenBy" id="txtTakenBy" class="js-states form-control" runat="server">
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName">Issued By</label>
                                            <select name="txtIssuedBy" id="txtIssuedBy" class="js-states form-control" runat="server">
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row">


                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Remarks</label>
                                            <asp:TextBox ID="txtRemarks" class="form-control" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Issue Type</label>
                                            <asp:RadioButtonList ID="RbtOld_New" class="form-control" runat="server"
                                                RepeatColumns="2" AutoPostBack="true"
                                                OnSelectedIndexChanged="RbtOld_New_SelectedIndexChanged">

                                                <asp:ListItem Value="1" Text="New" Selected="True" style="padding-right: 30px"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Old"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>

                                        <div class="form-group col-md-3" runat="server">
                                            <label for="exampleInputName">Other's Unit</label>
                                            <br />
                                            <asp:CheckBox ID="chkOthers" runat="server" AutoPostBack="true" OnCheckedChanged="chkOthers_CheckedChanged" />
                                        </div>

                                        <div id="divTest" class="form-group col-md-3" runat="server" visible="false">
                                            <label for="exampleInputName">Other Unit</label>
                                            <asp:DropDownList ID="ddlOtherUnit" runat="server" class="js-states form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">

                                        <asp:RadioButtonList ID="RdpIssueType" class="form-control" runat="server" Visible="false"
                                            RepeatColumns="2" OnSelectedIndexChanged="RdpIssueType_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="1" Text="Material Request" style="padding-right: 30px"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Direct" Selected="True"></asp:ListItem>
                                        </asp:RadioButtonList>



                                        <div class="form-group col-md-4">

                                            <asp:TextBox ID="txtMaterialRequestNo" class="form-control" Visible="false" runat="server" Style="float: left; width: 80%;"></asp:TextBox>

                                            <asp:Button ID="btnMatReqNo" runat="server" Text="+" class="fa fa-plus"
                                                Style="float: left; padding: 9px;" OnClick="btnMatReqNo_Click" Visible="false" />

                                            <cc1:ModalPopupExtender ID="modalPop_MatReqNo" runat="server" PopupControlID="Panel6" TargetControlID="btnMatReqNo"
                                                CancelControlID="BtnClear_MatReqNo" BackgroundCssClass="modalBackground" BehaviorID="MatReqNo_Close">
                                            </cc1:ModalPopupExtender>

                                            <asp:Panel ID="Panel6" runat="server" CssClass="modalPopup" Style="display: none">
                                                <div class="header">
                                                    MaterialRequestNo Details
                                                </div>
                                                <div class="body">
                                                    <div class="col-md-12 headsize">

                                                        <asp:Repeater ID="Repeater_MatReqNo" runat="server" EnableViewState="false">
                                                            <HeaderTemplate>
                                                                <table id="MatRequestNo" class="display table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Mat_Req_No</th>
                                                                            <th>Mat_Req_Date</th>
                                                                            <th>View</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Eval("Mat_Req_No")%></td>
                                                                    <td><%# Eval("Mat_Req_Date")%></td>

                                                                    <td>
                                                                        <asp:LinkButton ID="btnEditMatReqNo" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                            Text="" OnCommand="GridViewClick_MatReqNo" CommandArgument='<%# Eval("Mat_Req_No")%>' CommandName='<%# Eval("Mat_Req_Date")%>'>
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <div class="footer" align="right">
                                                    <asp:Button ID="BtnClear_MatReqNo" class="btn btn-rounded" runat="server" Text="Close" />
                                                </div>
                                            </asp:Panel>



                                        </div>

                                        <%--  <label for="exampleInputName">Material Request Date</label>--%>
                                        <asp:TextBox ID="txtMaterialRequestDate" MaxLength="10" Visible="false" class="form-control" runat="server"></asp:TextBox>

                                    </div>
                                </div>




                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <%--<label for="exampleInputName" visible="false" >OldItem_Count</label>--%>
                                            <asp:TextBox ID="txtOld_NewCount" MaxLength="10" Visible="false" class="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="timeline-options">
                                    <h4 class="panel-title">Item Details</h4>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName">Item Name</label>
                                            <asp:DropDownList ID="ddlItemName" runat="server" class="js-states form-control"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hfUOMId" runat="server" />
                                        </div>



                                        <div class="form-group col-md-3">
                                            <label for="exampleInputName">Department Name</label>

                                            <asp:DropDownList ID="txtDepartmentName" runat="server" Style="color: darkblue" class="js-states form-control"
                                                AutoPostBack="true" OnSelectedIndexChanged="txtDepartmentName_SelectedIndexChanged">
                                            </asp:DropDownList>


                                            <%--<asp:TextBox ID="txtDepartmentName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					             <asp:Button ID="btnDept" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnDept_Click"/>--%>
                                            <%--<cc1:ModalPopupExtender ID="modalPop_Dept"  runat="server" PopupControlID="Panel2" TargetControlID="btnDept"
                       CancelControlID="BtnClear_Dept" BackgroundCssClass="modalBackground" BehaviorID="Dept_Close">
                    </cc1:ModalPopupExtender>--%>

                                            <%--<asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Department Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_Dept" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="Dept" class="display table">
                         <thead >
                         <tr>
                         <th>DeptCode</th>
                         <th>DeptName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("DeptCode")%></td>
                          <td><%# Eval("DeptName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_Dept" CommandArgument='<%# Eval("DeptCode")%>' CommandName='<%# Eval("DeptName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_Dept" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>

                                            <%--<asp:HiddenField ID="txtDeptCodeHide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtDepartmentName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName">Warehouse Name</label>

                                            <asp:DropDownList ID="txtWarehouseName" runat="server"
                                                class="js-states form-control" AutoPostBack="true"
                                                OnSelectedIndexChanged="txtWarehouseName_SelectedIndexChanged">
                                            </asp:DropDownList>


                                            <%--<asp:TextBox ID="txtWarehouseName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					              <asp:Button ID="btnWareHouse" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnWareHouse_Click"/>--%>
                                            <%--<cc1:ModalPopupExtender ID="modalPop_WareHouse"  runat="server" PopupControlID="Panel1" TargetControlID="btnWareHouse"
                       CancelControlID="BtnClear_WareHouse" BackgroundCssClass="modalBackground" BehaviorID="WareHouse_Close">
                    </cc1:ModalPopupExtender>--%>

                                            <%--<asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            WareHouse Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_WareHouse" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="WareHouse" class="display table">
                         <thead >
                         <tr>
                         <th>WarehouseCode</th>
                         <th>WarehouseName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("WarehouseCode")%></td>
                          <td><%# Eval("WarehouseName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditWareHouse" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_WareHouse" CommandArgument='<%# Eval("WarehouseCode")%>' CommandName='<%# Eval("WarehouseName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_WareHouse" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>


                                            <%--<asp:HiddenField ID="txtWarehouseCodeHide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtWarehouseName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName">Machine Name (Cost Center Name)</label>
                                            <asp:DropDownList ID="txtCostCenter_Sub" runat="server" class="js-states form-control">
                                            </asp:DropDownList>
                                            <%-- <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtCostCenter_Sub" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
                                        </div>


                                        <%-- <label for="exampleInputName">Request Qty</label>--%>
                                        <asp:TextBox ID="txtRequestQty" class="form-control" Visible="false" runat="server"></asp:TextBox>


                                        <%--<label for="exampleInputName">Request Date</label>--%>
                                        <asp:TextBox ID="txtRequestDate" MaxLength="20" class="form-control date-picker" Visible="false" runat="server"></asp:TextBox>

                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Issue Qty</label>
                                            <asp:TextBox ID="txtIssueQty" class="form-control" runat="server"
                                                OnTextChanged="txtIssueQty_TextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtIssueQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtIssueQty" ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Value</label>
                                            <asp:TextBox ID="txtValue" class="form-control" runat="server" Enabled="false"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtValue" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtValue" ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Stock Qty</label>
                                            <asp:Label ID="txtStockQty" class="form-control" runat="server" Text="0.00"></asp:Label>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="exampleInputName">Bin Name</label>
                                            <asp:TextBox ID="txtBinName" class="form-control" runat="server"></asp:TextBox>
                                            <%--<asp:Button ID="btnBinName" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnBinName_Click"/>--%>
                                            <%--<cc1:ModalPopupExtender ID="modalPop_BinName"  runat="server" PopupControlID="Panel5" TargetControlID="btnBinName"
                       CancelControlID="BtnClear_BinName" BackgroundCssClass="modalBackground" BehaviorID="BinName_Close">
                    </cc1:ModalPopupExtender>--%>
                                            <%--<asp:Panel ID="Panel5" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            BinName Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_BinName" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="BinName" class="display table">
                         <thead >
                         <tr>
                         <th>BinName</th>
                      
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("BinName")%></td>
                          <td>
                          <asp:LinkButton ID="btnEditBinName" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_BinName" CommandArgument='<%# Eval("BinName")%>' CommandName='Edit'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_BinName" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>



                                            <asp:RequiredFieldValidator ControlToValidate="txtBinName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">

                                        <div class="form-group col-md-2" runat="server" visible="false">
                                            <label for="exampleInputName">Zone Name</label>
                                            <asp:TextBox ID="txtZoneName" class="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="form-group row"></div>
                                <div class="txtcenter">
                                    <asp:Button ID="btnAddItem" Width="50" Height="30" class="btn-success" runat="server" Text="Add" ValidationGroup="Item_Validate_Field" OnClick="btnAddItem_Click" />
                                </div>


                                <!-- table start -->
                                <div class="col-md-12">
                                    <div class="row">
                                        <asp:Panel ID="PanelmaterialRequestItem" runat="server" ScrollBars="Horizontal">
                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="table table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>S.No</th>
                                                                <th>Item Code</th>
                                                                <th>Item Name</th>
                                                                <th>UOM</th>
                                                                <%--  <th>Requ. Qty</th>
                                                    <th>Requ. Date</th>--%>
                                                                <th>Issue Qty</th>
                                                                <th>Value</th>
                                                                <%-- <th>Zone</th>--%>
                                                                <th>Bin</th>
                                                                <th>ReuseType</th>
                                                                <th>Mode</th>

                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                        <td><%# Eval("ItemCode")%></td>
                                                        <td><%# Eval("ItemName")%></td>
                                                        <td><%# Eval("UOMCode")%></td>
                                                        <%--  <td><%# Eval("RequestQty")%></td>
                                            <td><%# Eval("RequestDate")%></td>--%>
                                                        <td><%# Eval("IssueQty")%></td>
                                                        <td><%# Eval("Value")%></td>
                                                        <%--<td><%# Eval("ZoneName")%></td>--%>
                                                        <td><%# Eval("BinName")%></td>
                                                        <td><%# Eval("ReuseType")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <!-- table End -->

                                <div class="form-group row"></div>
                                <!-- Button start -->
                                <div class="txtcenter">
                                    <asp:Button ID="btnSave" class="btn btn-success" runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                    <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                    <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" Text="Back" OnClick="btnBackRequest_Click" />
                                    <asp:Button ID="btnApprove" class="btn btn-success" runat="server" Text="Approve" OnClick="btnApprove_Click" />
                                </div>
                                <!-- Button end -->

                            </div>
                            <!-- panel body end -->
                        </form>
                    </div>
                    <!-- panel white end -->
                </div>
                <!-- col-9 end -->
                <!-- Dashboard start -->
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-white" style="height: 100%;">
                        <div class="panel-heading">
                            <h4 class="panel-title">Dashboard Details</h4>
                            <div class="panel-control">
                            </div>
                        </div>
                        <div class="panel-body">
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Dashboard End -->
                <div class="col-md-2"></div>

            </div>
            <!-- col 12 end -->
        </div>
        <!-- row end -->
    </div>
    <!-- main-wrapper end -->

    <script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>' type="text/javascript"></script>


    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            alert(msg);
        }
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                    $('.js-states').select2();
                }
            });
        };
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();
            $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
            $('.js-states').select2();
        });
    </script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            if (!args.get_isPartialLoad()) {
                //  add our handler to the document's
                //  keydown event
                $addHandler(document, "keydown", onKeyDown);
            }
        }

        function onKeyDown(e) {
            if (e && e.keyCode == Sys.UI.Key.esc) {
                // if the key pressed is the escape key, dismiss the dialog
                $find('WareHouse_Close').hide();
                $find('Dept_Close').hide();
                $find('CostCenterName_Close').hide();
                $find('ZoneName_Close').hide();
                $find('BinName_Close').hide();
                $find('MatReqNo_Close').hide();
                $find('Item_Close').hide();
            }
        }
    </script>
    <!--Ajax popup End-->

</asp:Content>

