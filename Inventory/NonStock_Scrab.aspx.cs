﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class NonStock_Scrab : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionMaterialTransactionNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SQL;
    string Monthofdate = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Non Stock Scrab";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Inventory"));
            li.Attributes.Add("class", "droplink active open");
            //Initial_Data_Referesh_Material_Request_Item();
            //Employee_Name_Load();
            //Load_Data_Empty_CostCenterName();
            //Load_Data_Empty_Dept();
            //Load_Data_Empty_WareHouse();

            if (Session["Transaction_No"] == null)
            {
                SessionMaterialTransactionNo = "";
            }
            else
            {
                SessionMaterialTransactionNo = Session["Transaction_No"].ToString();
                txtIssueNo.Text = SessionMaterialTransactionNo;
                btnSearch_Click(sender, e);
            }

           // RdpIssueType_SelectedIndexChanged(sender, e);
        }
       // Load_OLD_data();


        //Load_Data_Empty_ZoneName();
        //Load_Data_Empty_BinName();
        //Load_Data_Empty_MatRequestNo();
        //Load_Data_Empty_ItemCode();
    }


  


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();

        SQL = "Select Transaction_No,CONVERT(Varchar(20),Date,103) as Date ,Item_Name,Quantity,Rate,Description from Non_Stock_Scrab where Transaction_No='" + SessionMaterialTransactionNo + "'";
        dt = objdata.RptEmployeeMultipleDetails(SQL);
        if (dt.Rows.Count > 0)
        {
            txtIssueNo.Text = dt.Rows[0]["Transaction_No"].ToString();
           txtDate.Text = dt.Rows[0]["Date"].ToString();
            txtitemname.Text = dt.Rows[0]["Item_Name"].ToString();
            txtquantity.Text = dt.Rows[0]["Quantity"].ToString();

            txtrate.Text = dt.Rows[0]["Rate"].ToString();
            txtdescription.Text = dt.Rows[0]["Description"].ToString();

            btnSave.Text = "Update";
        }
    }

    public void Load_Month()
    {
       

      string date=txtDate.Text.ToString();
      string[] FromDate_ChkStr = date.Split('/');
      string ran = FromDate_ChkStr[1].ToString();


      int mnth = Convert.ToInt32(ran.ToString());

        if(mnth == 01)
        {
            Monthofdate = "january";
        }
        else if (mnth == 02)
        {
            Monthofdate = "February";
        }

        else if (mnth == 03)
        {
            Monthofdate = "March";
        }
        else if (mnth == 04)
        {
            Monthofdate = "April";
        }

        else if (mnth == 05)
        {
            Monthofdate = "May";
        }

        else if (mnth == 06)
        {
            Monthofdate = "June";
        }
        else if (mnth == 07)
        {
            Monthofdate = "July";
        }

        else if (mnth == 08)
        {
            Monthofdate = "August";
        }
        else if (mnth == 09)
        {
            Monthofdate = "September";
        }
        else if (mnth == 10)
        {
            Monthofdate = "October";
        }
        else if (mnth == 11)
        {
            Monthofdate = "November";
        }
        else if (mnth == 12)
        {
            Monthofdate = "December";
        }
    
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        Load_Month();
        try
        {
            string Save_Mode = "Insert";
            bool ErrFlag = false;
           
            DataTable dt = new DataTable();
            if (txtDate.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('Select Date...');", true);
            }
            if (txtitemname.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('Enter Item Name...');", true);
            }
            if (txtrate.Text  == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('Enter The Rate...');", true);
            }
            if (btnSave.Text != "Update")
            {
                if (!ErrFlag)
                {
                    TransactionNoGenerate TransNO = new TransactionNoGenerate();
                    string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "NonStock Scrab", SessionFinYearVal);
                    if (Auto_Transaction_No == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                    }
                    else
                    {
                       txtIssueNo.Text = Auto_Transaction_No;
                    }
                }
            }
          

            SQL = "Select * from Non_Stock_Scrab where Transaction_No='" + txtIssueNo.Text + "'  ";
            dt = objdata.RptEmployeeMultipleDetails(SQL);
            if (dt.Rows.Count > 0)
            {
                SQL = "Delete From Non_Stock_Scrab where Transaction_No='" + txtIssueNo.Text + "' ";
                dt = objdata.RptEmployeeMultipleDetails(SQL);
            }

            SQL = "Insert Into Non_Stock_Scrab (Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Month,Date,Item_Name,Quantity,Rate,Description) values (";
            SQL = SQL + "'" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtIssueNo.Text + "',";
            SQL = SQL + " '" + Monthofdate.ToString() + "' ,'" + txtDate.Text + "','" + txtitemname.Text + "','" + txtquantity.Text + "' ,'" + txtrate.Text + "','" + txtdescription.Text + "') ";
            dt = objdata.RptEmployeeMultipleDetails(SQL);



        }
        catch (Exception Ex)
        {

        }
        Clear();
        Response.Redirect("NonStockScrab_Main.aspx");
    }
    public void Clear()
    {
        txtIssueNo.Text = "";
        txtDate.Text="";
        txtitemname.Text = "";
        txtquantity.Text = "";
        txtrate.Text = "";
        txtdescription.Text = "";
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear();
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("NonStockScrab_Main.aspx");
    }

}
