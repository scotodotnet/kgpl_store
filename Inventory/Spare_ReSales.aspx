﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Spare_ReSales.aspx.cs" Inherits="Inventory_Spare_ReSales" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#WareHouse').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#WareHouse').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Dept').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Dept').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#CostCenterName').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#CostCenterName').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ZoneName').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ZoneName').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#BinName').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#BinName').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable').dataTable();               
            }
        });
    };
</script>
<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
    
  $find('WareHouse_Close').hide();
  $find('Dept_Close').hide();
  $find('CostCenterName_Close').hide();
  $find('ZoneName_Close').hide();
  $find('BinName_Close').hide();
 
  $find('Item_Close').hide();
     }
   } 
 </script>
<!--Ajax popup End-->

<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Spares ReSales</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Spares ReSales</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Trans No<span class="mandatory">*</span></label>
					            <asp:Label ID="txtTransNo" runat="server" class="form-control"></asp:Label>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Date</label>
					            <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Department Name <span class="mandatory">*</span> </label>
					            
					            
					            <asp:DropDownList ID="txtDepartmentName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					             <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtDepartmentName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					            
					            <%--<asp:TextBox ID="txtDepartmentName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					             <asp:Button ID="btnDept" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnDept_Click"/>--%>
                   <%-- <cc1:ModalPopupExtender ID="modalPop_Dept"  runat="server" PopupControlID="Panel2" TargetControlID="btnDept"
                       CancelControlID="BtnClear_Dept" BackgroundCssClass="modalBackground" BehaviorID="Dept_Close">
                    </cc1:ModalPopupExtender>--%>
                   
                    <%--<asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Department Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_Dept" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="Dept" class="display table">
                         <thead >
                         <tr>
                         <th>DeptCode</th>
                         <th>DeptName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("DeptCode")%></td>
                          <td><%# Eval("DeptName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_Dept" CommandArgument='<%# Eval("DeptCode")%>' CommandName='<%# Eval("DeptName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_Dept" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            <%--<asp:HiddenField ID="txtDeptCodeHide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtDepartmentName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        
					        </div>
					        </div>
					        <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Cost Center Name <span class="mandatory">*</span> </label>
					            
					            
					            <asp:DropDownList ID="txtCostCenterName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					             <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtCostCenterName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					            
					            
					            <%--<asp:TextBox ID="txtCostCenterName" class="form-control" runat="server"  style="float: left;width: 80%;"></asp:TextBox>
					             <asp:Button ID="btnCostCenterName" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnCostCenterName_Click"/>--%>
					       
					            <%--<cc1:ModalPopupExtender ID="modalPop_CostCenterName"  runat="server" PopupControlID="Panel3" TargetControlID="btnCostCenterName"
                       CancelControlID="BtnClear_CostCenterName" BackgroundCssClass="modalBackground" BehaviorID="CostCenterName_Close">
                    </cc1:ModalPopupExtender>--%>
                    
                                <%--<asp:Panel ID="Panel3" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            CostCenterName Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_CostCenterName" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="CostCenterName" class="display table">
                         <thead >
                         <tr>
                         <th>CostcenterCode</th>
                         <th>CostcenterName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("CostcenterCode")%></td>
                          <td><%# Eval("CostcenterName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditCostCenterName" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_CostCenterName" CommandArgument='<%# Eval("CostcenterCode")%>' CommandName='<%# Eval("CostcenterName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_CostCenterName" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            
					           <%-- <asp:HiddenField ID="txtCostCenterCodeHide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtCostCenterName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        
					   
					       
					       <div class="form-group col-md-4">
					            <label for="exampleInputName">Warehouse Name <span class="mandatory">*</span></label>
					            
					            
					            <asp:DropDownList ID="txtWarehouseName" runat="server" 
                                    class="js-states form-control" AutoPostBack="true" 
                                    onselectedindexchanged="txtWarehouseName_SelectedIndexChanged">
                                </asp:DropDownList>
					             <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtWarehouseName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					            
					            
					            <%--<asp:TextBox ID="txtWarehouseName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					              <asp:Button ID="btnWareHouse" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnWareHouse_Click"/>--%>
					            <%--<cc1:ModalPopupExtender ID="modalPop_WareHouse"  runat="server" PopupControlID="Panel1" TargetControlID="btnWareHouse"
                       CancelControlID="BtnClear_WareHouse" BackgroundCssClass="modalBackground" BehaviorID="WareHouse_Close">
                    </cc1:ModalPopupExtender>--%>
                    
                    <%--<asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            WareHouse Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_WareHouse" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="WareHouse" class="display table">
                         <thead >
                         <tr>
                         <th>WarehouseCode</th>
                         <th>WarehouseName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("WarehouseCode")%></td>
                          <td><%# Eval("WarehouseName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditWareHouse" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_WareHouse" CommandArgument='<%# Eval("WarehouseCode")%>' CommandName='<%# Eval("WarehouseName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_WareHouse" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            <%--<asp:HiddenField ID="txtWarehouseCodeHide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtWarehouseName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Party Name <span class="mandatory">*</span></label>
					          <asp:TextBox ID="txtPartyName" class="form-control" runat="server"></asp:TextBox>
					          <asp:RequiredFieldValidator ControlToValidate="txtPartyName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
					        </div>
					        
					         </div>
					</div>
					
					<div class="col-md-12">
					    <div class="row">
					        
					     
					    <div class="form-group col-md-4">
					           				            
					          <label for="exampleInputName">Mobile No <span class="mandatory">*</span></label>	
					          <div class="form-group row">
					                <div class="col-sm-4">
					               
							            <asp:TextBox ID="txtMblCode" Text="+91" class="form-control" runat="server" ReadOnly="true"></asp:TextBox>
							        </div>
							        <div class="col-sm-8">
                                        <asp:TextBox ID="txtMobile_no" MaxLength="10" class="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ControlToValidate="txtMobile_no" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="[0-9]{10}" runat="server" class="form_error" 
                                            ControlToValidate="txtMobile_no" ValidationGroup="Validate_Field" ErrorMessage="Mobile.No length 10">
                                        </asp:RegularExpressionValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtMobile_no" ValidChars="0123456789">
                                        </cc1:FilteredTextBoxExtender>
                                    </div>
                                </div>
					        </div>
					     
					          <div class="form-group col-md-4">
					            <label for="exampleInputName">Party Details <span class="mandatory">*</span></label>
					          <asp:TextBox ID="txtPartyDetails" class="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
					          
					          <asp:RequiredFieldValidator ControlToValidate="txtPartyDetails" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
					          
					        </div>
					    
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Remarks</label>
					            <asp:TextBox ID="txtRemarks" class="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
					        </div>
					        
					        
					         </div>
					        </div>
					       <div class="col-md-12">
					    <div class="row"> 
					        
					         <div class="form-group col-md-4">
					            <label for="exampleInputName">Note</label>
					            <asp:TextBox ID="txtNote" class="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
					        </div>
					    </div>
					</div>
				
						
					<div class="clearfix"></div>				    
				    <div class="timeline-options">
				        <h4 class="panel-title">Item Details</h4>
				    </div>
				    <div class="col-md-12">
					    <div class="row">
					        
					       
					       <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Name</label>
					            <asp:TextBox ID="txtItemName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					             <asp:Button ID="btnItemCode" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnItemCode_Click"/>
                                
                                 <cc1:ModalPopupExtender ID="modalPop_ItemCode"  runat="server" PopupControlID="Panel7" TargetControlID="btnItemCode"
                       CancelControlID="BtnItemClose" BackgroundCssClass="modalBackground" BehaviorID="Item_Close">
                    </cc1:ModalPopupExtender>
                         <asp:Panel ID="Panel7" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Item Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_ItemCode" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="ItemTable" class="display table">
                         <thead >
                         <tr>
                         <th>ItemCode</th>
                         <th>ItemName</th>
                         <th>UOMCode</th>
                        
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ItemCode")%></td>
                          <td><%# Eval("ItemName")%></td>
                          <td><%# Eval("UOMCode")%></td>
                         
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEdit_ItemCode" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ItemCode" CommandArgument='<%# Eval("ItemCode")+","+ Eval("UOMCode") %>' CommandName='<%# Eval("ItemName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnItemClose" class="btn btn-rounded" runat="server" Text="Close" />
					     </div>
					     </asp:Panel>
					            
					            
					            
					            <asp:HiddenField ID="txtItemCodeHide" runat="server" />
					            <asp:HiddenField ID="txtItemUOMHide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtItemName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Zone Name</label>
					            
					            <asp:DropDownList ID="txtZoneName" runat="server" class="js-states form-control" AutoPostBack="true" onselectedindexchanged="txtZoneName_SelectedIndexChanged">
                          </asp:DropDownList>
					             <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtZoneName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator17" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator> 
					            
					            
					            <%--<asp:TextBox ID="txtZoneName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					             <asp:Button ID="btnZoneName" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnZoneName_Click"/>--%>
					          <%--<cc1:ModalPopupExtender ID="modalPop_ZoneName"  runat="server" PopupControlID="Panel4" TargetControlID="btnZoneName"
                       CancelControlID="BtnClear_ZoneName" BackgroundCssClass="modalBackground" BehaviorID="ZoneName_Close">
                    </cc1:ModalPopupExtender>--%>  
					            <%--<asp:Panel ID="Panel4" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            ZoneName Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_ZoneName" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="ZoneName" class="display table">
                         <thead >
                         <tr>
                         <th>ZoneName</th>
                      
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ZoneName")%></td>
                          <td>
                          <asp:LinkButton ID="btnEditZoneName" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ZoneName" CommandArgument='<%# Eval("ZoneName")%>' CommandName='Edit'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_ZoneName" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					           <%-- <asp:RequiredFieldValidator ControlToValidate="txtZoneName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Bin Name</label>
					            
					      <asp:DropDownList ID="txtBinName" runat="server" class="js-states form-control">
                          </asp:DropDownList>
					      <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtBinName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                          </asp:RequiredFieldValidator>
					            
					            
					            <%--<asp:TextBox ID="txtBinName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					             <asp:Button ID="btnBinName" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnBinName_Click"/>--%>
					             <%--<cc1:ModalPopupExtender ID="modalPop_BinName"  runat="server" PopupControlID="Panel5" TargetControlID="btnBinName"
                       CancelControlID="BtnClear_BinName" BackgroundCssClass="modalBackground" BehaviorID="BinName_Close">
                    </cc1:ModalPopupExtender>--%> 
					   <%--<asp:Panel ID="Panel5" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            BinName Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_BinName" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="BinName" class="display table">
                         <thead >
                         <tr>
                         <th>BinName</th>
                      
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("BinName")%></td>
                          <td>
                          <asp:LinkButton ID="btnEditBinName" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_BinName" CommandArgument='<%# Eval("BinName")%>' CommandName='Edit'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_BinName" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            
					            <%--<asp:RequiredFieldValidator ControlToValidate="txtBinName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        
					        
					        
					       
					        </div>
					        </div>
					        
					      <div class="col-md-12">
					    <div class="row">  
					    
					    
					    <div class="form-group col-md-2">
					            <label for="exampleInputName">Qty</label>
					            <asp:TextBox ID="txtQty" class="form-control" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtQty" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>		
                            <div class="form-group col-md-2">
					            <label for="exampleInputName">Value</label>
					            <asp:TextBox ID="txtValue" class="form-control" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtValue" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtValue" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					        <div class="form-group col-md-1">
					            <br />
					            <asp:Button ID="btnAddItem" Width="50" Height="30" class="btn-success"  runat="server" Text="Add" ValidationGroup="Item_Validate_Field"  OnClick="btnAddItem_Click"/>
					        </div>
					    </div>
					</div>
						
					<!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Panel ID="PanelmaterialRequestItem" runat="server">
					            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                        <HeaderTemplate>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Item Code</th>
                                                    <th>Item Name</th>
                                                    <th>UOM</th>
                                                    <th>Qty</th>
                                                    <th>Value</th>
                                                    <th>Zone</th>
                                                    <th>Bin</th>
                                                 
                                                    <th>Mode</th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Container.ItemIndex + 1 %></td>
                                            <td><%# Eval("ItemCode")%></td>
                                            <td><%# Eval("ItemName")%></td>
                                            <td><%# Eval("UOMCode")%></td>
                                            <td><%# Eval("Qty")%></td>
                                            <td><%# Eval("Value")%></td>
                                            <td><%# Eval("ZoneName")%></td>
                                            <td><%# Eval("BinName")%></td>
                                           
                                            <td>
                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                    Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>' 
                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate></table></FooterTemplate>                                
			                    </asp:Repeater>
			                </asp:Panel>
					    </div>
					</div>
					<!-- table End -->
					
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                        <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                        <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" Text="Back" OnClick="btnBackRequest_Click"/>
                        <asp:Button ID="btnApprove" class="btn btn-success" runat="server" Text="Approve" OnClick="btnApprove_Click"/>
                    </div>
                    <!-- Button end -->
                                      
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 



</asp:Content>

