﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="meterial_stock_transfer.aspx.cs" Inherits="Inventory_meterial_stock_transfer" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#FromWareHouse').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#FromWareHouse').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ToWareHouse').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ToWareHouse').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Dept').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Dept').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#CostCenterName').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#CostCenterName').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable').dataTable();               
            }
        });
    };
</script>



<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#FromZoneName').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#FromZoneName').dataTable();               
            }
        });
    };
</script>




<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ToZoneName').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ToZoneName').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#FromBinName').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#FromBinName').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ToBinName').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ToBinName').dataTable();               
            }
        });
    };
</script>


<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
    
  $find('FrmWareHouse_Close').hide();
  $find('ToWareHouse_Close').hide();
  $find('Dept_Close').hide();
  $find('CostCenterName_Close').hide();
  $find('Item_Close').hide();
 $find('FromZoneName_Close').hide();
 $find('ToZoneName_Close').hide();
 $find('FromBinName_Close').hide();
 $find('ToBinName_Close').hide();
 
 
     }
   } 
 </script>
<!--Ajax popup End-->


<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Material Stock Transfer</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Material Stock Transfer</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Transfer No<span class="mandatory">*</span></label>
					            <asp:Label ID="txtTransferNo" runat="server" class="form-control"></asp:Label>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Date</label>
					            <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server" AutoComplete="Off"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">From Warehouse</label>
					            
					             <asp:DropDownList ID="txtFromWarehouseName" runat="server" 
                                    class="js-states form-control" AutoPostBack="true" 
                                    onselectedindexchanged="txtFromWarehouseName_SelectedIndexChanged">
                                </asp:DropDownList>
					             <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtFromWarehouseName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					            
					            
					           <%-- <asp:TextBox ID="txtFromWarehouseName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnFrmWareHouse" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnFrmWareHouse_Click"/>--%>
					            <%--<cc1:ModalPopupExtender ID="modalPop_FrmWareHouse"  runat="server" PopupControlID="Panel1" TargetControlID="btnFrmWareHouse"
                       CancelControlID="BtnClear_FrmWareHouse" BackgroundCssClass="modalBackground" BehaviorID="FrmWareHouse_Close">
                    </cc1:ModalPopupExtender>--%>
                    
                    <%--<asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            WareHouse Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_FrmWareHouse" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="FromWareHouse" class="display table">
                         <thead >
                         <tr>
                         <th>WarehouseCode</th>
                         <th>WarehouseName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("WarehouseCode")%></td>
                          <td><%# Eval("WarehouseName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditFrmWareHouse" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_FrmWareHouse" CommandArgument='<%# Eval("WarehouseCode")%>' CommandName='<%# Eval("WarehouseName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_FrmWareHouse" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            
					           <%-- <asp:HiddenField ID="txtFromWarehouseCodeHide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtFromWarehouseName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">To Warehouse</label>
					            
					            <asp:DropDownList ID="txtToWarehouseName" runat="server" 
                                    class="js-states form-control" AutoPostBack="true" 
                                    onselectedindexchanged="txtToWarehouseName_SelectedIndexChanged">
                                </asp:DropDownList>
					             <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtToWarehouseName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					            
					            
					            <%--<asp:TextBox ID="txtToWarehouseName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnToWareHouse" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnToWareHouse_Click"/>--%>
					            <%--<cc1:ModalPopupExtender ID="modalPop_ToWareHouse"  runat="server" PopupControlID="Panel2" TargetControlID="btnToWareHouse"
                       CancelControlID="BtnClear_ToWareHouse" BackgroundCssClass="modalBackground" BehaviorID="ToWareHouse_Close">
                    </cc1:ModalPopupExtender>--%>
                    
                    <%--<asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            WareHouse Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_ToWareHouse" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="ToWareHouse" class="display table">
                         <thead >
                         <tr>
                         <th>WarehouseCode</th>
                         <th>WarehouseName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("WarehouseCode")%></td>
                          <td><%# Eval("WarehouseName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditToWareHouse" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ToWareHouse" CommandArgument='<%# Eval("WarehouseCode")%>' CommandName='<%# Eval("WarehouseName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_ToWareHouse" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            
					            <%--<asp:HiddenField ID="txtToWarehouseCodeHide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtToWarehouseName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4" runat="server" visible="false">
					            <label for="exampleInputName">Cost Center Name</label>
					            
					            <asp:DropDownList ID="txtCostCenterName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					             <%--<asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtCostCenterName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					            
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Prepared By</label>
					            <select name="txtPreparedBy" id="txtPreparedBy" class="js-states form-control" runat="server">
	                            </select>
					        </div>
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Department Name</label>
					            
					            <asp:DropDownList ID="txtDepartmentName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					             <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtDepartmentName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					            
					            
					            <%--<asp:TextBox ID="txtDepartmentName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					             <asp:Button ID="btnDept" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnDept_Click"/>--%>
                    <%--<cc1:ModalPopupExtender ID="modalPop_Dept"  runat="server" PopupControlID="Panel9" TargetControlID="btnDept"
                       CancelControlID="BtnClear_Dept" BackgroundCssClass="modalBackground" BehaviorID="Dept_Close">
                    </cc1:ModalPopupExtender>--%>
                   
                    <%--<asp:Panel ID="Panel9" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Department Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_Dept" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="Dept" class="display table">
                         <thead >
                         <tr>
                         <th>DeptCode</th>
                         <th>DeptName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("DeptCode")%></td>
                          <td><%# Eval("DeptName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_Dept" CommandArgument='<%# Eval("DeptCode")%>' CommandName='<%# Eval("DeptName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_Dept" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            <%--<asp:HiddenField ID="txtDeptCodeHide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtDepartmentName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Others</label>
					            <asp:TextBox ID="txtOthers" class="form-control" runat="server"></asp:TextBox>
					        </div>
					       </div>
					       </div> 
					     <div class="col-md-12">
					    <div class="row">   
					        
					    </div>
					</div>
					
					<div class="clearfix"></div>				    
				    <div class="timeline-options">
				        <h4 class="panel-title">Item Details</h4>
				    </div>
				    <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Name</label>
					            <asp:TextBox ID="txtItemName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnItemCode" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnItemCode_Click"/>
                                
                                 <cc1:ModalPopupExtender ID="modalPop_ItemCode"  runat="server" PopupControlID="Panel5" TargetControlID="btnItemCode"
                       CancelControlID="BtnItemClose" BackgroundCssClass="modalBackground" BehaviorID="Item_Close">
                    </cc1:ModalPopupExtender>
                         <asp:Panel ID="Panel5" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Item Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_ItemCode" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="ItemTable" class="display table">
                         <thead >
                         <tr>
                         <th>ItemCode</th>
                         <th>ItemName</th>
                          <th>UOMCode</th>
                           <th>ValuationType</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ItemCode")%></td>
                          <td><%# Eval("ItemName")%></td>
                          <td><%# Eval("UOMCode")%></td>
                          <td><%# Eval("ValuationType")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEdit_ItemCode" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ItemCode" CommandArgument='<%# Eval("ItemCode")+","+ Eval("UOMCode")+","+ Eval("ValuationType") %>' CommandName='<%# Eval("ItemName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnItemClose" class="btn btn-rounded" runat="server" Text="Close" />
					     </div>
					     </asp:Panel>
					            
					            
					            <asp:HiddenField ID="txtItemCodeHide" runat="server" />
					            <asp:HiddenField ID="txtItemUOMHide" runat="server" />
					            <asp:HiddenField ID="txtItemValueTypeHide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtItemName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <div class="form-group col-md-4" runat="server" visible="false">
					            <label for="exampleInputName">From Zone</label>
					            
					            
					      <asp:DropDownList ID="txtFromZoneName" runat="server" class="js-states form-control" AutoPostBack="true" onselectedindexchanged="txtFromZoneName_SelectedIndexChanged">
                          </asp:DropDownList>
					             <%--<asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtFromZoneName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator17" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					            
					            
					            <%--<asp:TextBox ID="txtFromZoneName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnFromZoneName" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnFromZoneName_Click"/>--%>
					         <%-- <cc1:ModalPopupExtender ID="modalPop_FromZoneName"  runat="server" PopupControlID="Panel4" TargetControlID="btnFromZoneName"
                       CancelControlID="BtnClear_FromZoneName" BackgroundCssClass="modalBackground" BehaviorID="FromZoneName_Close">
                    </cc1:ModalPopupExtender>--%>  
					            <%--<asp:Panel ID="Panel4" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            ZoneName Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_FromZoneName" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="FromZoneName" class="display table">
                         <thead >
                         <tr>
                         <th>ZoneName</th>
                      
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ZoneName")%></td>
                          <td>
                          <asp:LinkButton ID="btnEditFromZoneName" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_FromZoneName" CommandArgument='<%# Eval("ZoneName")%>' CommandName='Edit'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_FromZoneName" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            
					            
					           <%-- <asp:RequiredFieldValidator ControlToValidate="txtFromZoneName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        <div class="form-group col-md-4" runat="server" visible="false">
					            <label for="exampleInputName">To Zone</label>
					            
					            <asp:DropDownList ID="txtToZoneName" runat="server" class="js-states form-control" AutoPostBack="true" onselectedindexchanged="txtToZoneName_SelectedIndexChanged">
                          </asp:DropDownList>
					             <%--<asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtToZoneName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					            
					            
					            <%--<asp:TextBox ID="txtToZoneName" class="form-control" runat="server" style="float: left;width: 81%;"></asp:TextBox>
					            <asp:Button ID="btnToZoneName" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnToZoneName_Click"/>--%>
					         <%-- <cc1:ModalPopupExtender ID="modalPop_ToZoneName"  runat="server" PopupControlID="Panel6" TargetControlID="btnToZoneName"
                       CancelControlID="BtnClear_ToZoneName" BackgroundCssClass="modalBackground" BehaviorID="ToZoneName_Close">
                    </cc1:ModalPopupExtender>--%>  
					            <%--<asp:Panel ID="Panel6" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            ZoneName Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_ToZoneName" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="ToZoneName" class="display table">
                         <thead >
                         <tr>
                         <th>ZoneName</th>
                      
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ZoneName")%></td>
                          <td>
                          <asp:LinkButton ID="btnEditToZoneName" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ToZoneName" CommandArgument='<%# Eval("ZoneName")%>' CommandName='Edit'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_ToZoneName" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            <%--<asp:RequiredFieldValidator ControlToValidate="txtToZoneName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					         <div class="form-group col-md-4">
					            <label for="exampleInputName">From Bin</label>
					            
					            
					      <asp:DropDownList ID="txtFromBinName" runat="server" class="js-states form-control">
                          </asp:DropDownList>
					      <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtFromBinName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                          </asp:RequiredFieldValidator>
					            
					            
					            <%--<asp:TextBox ID="txtFromBinName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnFromBinName" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnFromBinName_Click"/>--%>
					             <%--<cc1:ModalPopupExtender ID="modalPop_FromBinName"  runat="server" PopupControlID="Panel7" TargetControlID="btnFromBinName"
                       CancelControlID="BtnClear_FromBinName" BackgroundCssClass="modalBackground" BehaviorID="FromBinName_Close">
                    </cc1:ModalPopupExtender>--%> 
					   <%--<asp:Panel ID="Panel7" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            BinName Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_FromBinName" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="FromBinName" class="display table">
                         <thead >
                         <tr>
                         <th>BinName</th>
                      
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("BinName")%></td>
                          <td>
                          <asp:LinkButton ID="btnEditFromBinName" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_FromBinName" CommandArgument='<%# Eval("BinName")%>' CommandName='Edit'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_FromBinName" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            <%--<asp:RequiredFieldValidator ControlToValidate="txtFromBinName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">To Bin</label>
					            
					            <asp:DropDownList ID="txtToBinName" runat="server" class="js-states form-control">
                          </asp:DropDownList>
					      <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtToBinName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                          </asp:RequiredFieldValidator>
					            
					            
					            <%--<asp:TextBox ID="txtToBinName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnToBinName" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnToBinName_Click"/>--%>
					             <%--<cc1:ModalPopupExtender ID="modalPop_ToBinName"  runat="server" PopupControlID="Panel8" TargetControlID="btnToBinName"
                       CancelControlID="BtnClear_ToBinName" BackgroundCssClass="modalBackground" BehaviorID="ToBinName_Close">
                    </cc1:ModalPopupExtender>--%> 
					   <%--<asp:Panel ID="Panel8" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            BinName Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_ToBinName" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="ToBinName" class="display table">
                         <thead >
                         <tr>
                         <th>BinName</th>
                      
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("BinName")%></td>
                          <td>
                          <asp:LinkButton ID="btnEditToBinName" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ToBinName" CommandArgument='<%# Eval("BinName")%>' CommandName='Edit'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_ToBinName" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            
					            <%--<asp:RequiredFieldValidator ControlToValidate="txtToBinName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        </div>
					        </div>
					        
					         <div class="col-md-12">
					    <div class="row">
					       
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Transfer Qty</label>
					            <asp:TextBox ID="txtTransferQty" class="form-control" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtTransferQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTransferQty" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Value</label>
					            <asp:TextBox ID="txtValue" class="form-control" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtValue" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtValue" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-1">
					            <br />
					            <asp:Button ID="btnAddItem" Width="50" Height="30" class="btn-success"  runat="server" Text="Add" ValidationGroup="Item_Validate_Field"  OnClick="btnAddItem_Click"/>
					        </div>
					    </div>
					</div>	
                    
                    <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Panel ID="PanelmaterialRequestItem" runat="server">
					            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                        <HeaderTemplate>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Item Code</th>
                                                    <th>Item Name</th>
                                                    <th>UOM</th>
                                                    <th>Val. Type</th>
                                                    <%--<th>From Zone</th>--%>
                                                    <th>To Zone</th>
                                                    <%--<th>From Bin</th>--%>
                                                    <th>To Bin</th>
                                                    <th>Trans. Qty</th>
                                                    <th>Value</th>
                                                    <th>Mode</th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Container.ItemIndex + 1 %></td>
                                            <td><%# Eval("ItemCode")%></td>
                                            <td><%# Eval("ItemName")%></td>
                                            <td><%# Eval("UOMCode")%></td>
                                            <td><%# Eval("ValueType")%></td>
                                            <%--<td><%# Eval("FromZone")%></td>--%>
                                            <td><%# Eval("ToZone")%></td>
                                            <%--<td><%# Eval("FromBin")%></td>--%>
                                            <td><%# Eval("ToBin")%></td>
                                            <td><%# Eval("TransferQty")%></td>
                                            <td><%# Eval("Value")%></td>
                                            <td>
                                                <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                    Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>' 
                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate></table></FooterTemplate>                                
			                    </asp:Repeater>
			                </asp:Panel>
					    </div>
					</div>
					<!-- table End -->
					
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                        <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                        <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" Text="Back" OnClick="btnBackRequest_Click"/>
                        <asp:Button ID="btnApprove" class="btn btn-success" runat="server" Text="Approve" OnClick="btnApprove_Click"/>
                    </div>
                    <!-- Button end -->
                                      
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 
</asp:Content>

