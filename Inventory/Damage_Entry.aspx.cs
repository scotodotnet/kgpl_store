﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Inventory_Damage_Entry : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionDamageEntryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Damage Entry";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Inventory"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh_Material_Request_Item();
            Employee_Name_Load();
            Load_Data_Empty_Dept();
            Load_Data_Empty_CostCenterName();
            Load_Data_Empty_WareHouse();


            Load_Data_Empty_ZoneName();
            Load_Data_Empty_BinName();

            if (Session["Damage_Entry_No"] == null)
            {
                SessionDamageEntryNo = "";
            }
            else
            {
                SessionDamageEntryNo = Session["Damage_Entry_No"].ToString();
                txtTransNo.Text = SessionDamageEntryNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();
        
        Load_Data_Empty_ItemCode();
        
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }

        
        //Check Warehouse Name
        query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseName.SelectedValue + "' And WarehouseName='" + txtWarehouseName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Warehouse Name...');", true);
        }

        //Check Department Name
        query = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + txtDepartmentName.SelectedValue + "' And DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Department Name...');", true);
        }

        //Check Cost Center Name
        query = "Select * from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CostcenterCode='" + txtCostCenterName.SelectedValue + "' And CostcenterName='" + txtCostCenterName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Cost Center Name...');", true);
        }

        //Check Taken by And Issued by
        if (txtDamagedBy.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Damaged by...');", true);
        }
        if (txtVerifiedBy.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Taken by...');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Damage Entry");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Damage Entry Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Damage Entry");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Damage Entry..');", true);
            }
        }
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Damage Entry", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtTransNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            query = "Select * from Damage_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Damage_Entry_No='" + txtTransNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Damage_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Damage_Entry_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Damage_Entry_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Damage_Entry_No='" + txtTransNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into Damage_Entry_Main(Ccode,Lcode,FinYearCode,FinYearVal,Damage_Entry_No,Damage_Entry_Date,DeptCode,DeptName,CostCenterCode,CostCenterName,WarehouseCode,WarehouseName,Damagedby,Verifiedby,Remarks,Note,UserID,UserName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "','" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "','" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "','" + txtWarehouseName.SelectedValue + "','" + txtWarehouseName.SelectedItem.Text + "',";
            query = query + " '" + txtVerifiedBy.Value + "','" + txtDamagedBy.Value + "','" + txtRemarks.Text + "','" + txtNote.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            //Meterial_Issue_Main_sub
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Damage_Entry_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Damage_Entry_No,Damage_Entry_Date,ItemCode,ItemName,UOMCode,Qty,Rate,ZoneName,BinName,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["Qty"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["ZoneName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Damage Entry Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Damage Entry Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Damage_Entry_No"] = txtTransNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtTransNo.Text = ""; txtDate.Text = ""; txtWarehouseName.SelectedValue = "-Select-";
        txtDepartmentName.SelectedValue = "-Select-"; txtCostCenterName.SelectedValue = "-Select-";
        txtDamagedBy.Value = ""; txtVerifiedBy.Value = ""; txtRemarks.Text = ""; txtNote.Text = "";
        txtDamagedBy.Value = "-Select-"; txtVerifiedBy.Value = "-Select-";
        txtItemCodeHide.Value = ""; txtItemName.Text = ""; txtItemUOMHide.Value = ""; txtQty.Text = "";
        txtValue.Text = ""; txtZoneName.SelectedValue = null; txtBinName.SelectedValue = null;
        

        btnSave.Text = "Save";
        Initial_Data_Referesh_Material_Request_Item();
        Session.Remove("Damage_Entry_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (txtQty.Text == "0.0" || txtQty.Text == "0" || txtQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Issue Qty...');", true);
        }
        if (txtValue.Text == "0.0" || txtValue.Text == "0" || txtValue.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Value...');", true);
        }

        //check with Item Code And Item Name
        query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCodeHide.Value + "' And ItemShortName='" + txtItemName.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
        }


        //check with Zone Name And Bin Name
        query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseName.SelectedValue + "' And ZoneName='" + txtZoneName.SelectedItem.Text + "' And BinName='" + txtBinName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Zone And Bin Details..');", true);
        }

     

        if (!ErrFlag)
        {
            //UOM Code get
            string UOM_Code_Str = txtItemUOMHide.Value.ToString();

            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('this Item Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["ItemCode"] = txtItemCodeHide.Value;
                    dr["ItemName"] = txtItemName.Text;
                    dr["UOMCode"] = UOM_Code_Str;
                    dr["Qty"] = txtQty.Text;
                 
                    dr["Value"] = txtValue.Text;
                    dr["ZoneName"] = txtZoneName.Text;
                    dr["BinName"] = txtBinName.Text;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    txtItemCodeHide.Value = ""; txtItemName.Text = ""; txtItemUOMHide.Value = ""; txtQty.Text = "";
                    txtValue.Text = ""; txtZoneName.SelectedValue = null; txtBinName.SelectedValue = null;
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["ItemCode"] = txtItemCodeHide.Value;
                dr["ItemName"] = txtItemName.Text;
                dr["UOMCode"] = UOM_Code_Str;
                dr["Qty"] = txtQty.Text;
                dr["Value"] = txtValue.Text;
                dr["ZoneName"] = txtZoneName.Text;
                dr["BinName"] = txtBinName.Text;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                txtItemCodeHide.Value = ""; txtItemName.Text = ""; txtItemUOMHide.Value = ""; txtQty.Text = "";
                txtValue.Text = ""; txtZoneName.SelectedValue = null; txtBinName.SelectedValue = null;
            }
        }
    }

    private void Initial_Data_Referesh_Material_Request_Item()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("Qty", typeof(string)));
        dt.Columns.Add(new DataColumn("Value", typeof(string)));
        dt.Columns.Add(new DataColumn("ZoneName", typeof(string)));
        dt.Columns.Add(new DataColumn("BinName", typeof(string)));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSourceS;
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Damage_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Damage_Entry_No='" + txtTransNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["Damage_Entry_Date"].ToString();
            txtWarehouseName.SelectedValue = Main_DT.Rows[0]["WarehouseCode"].ToString();
            txtWarehouseName.SelectedItem.Text = Main_DT.Rows[0]["WarehouseName"].ToString();
            txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtCostCenterName.SelectedValue = Main_DT.Rows[0]["CostCenterCode"].ToString();
            txtCostCenterName.SelectedItem.Text = Main_DT.Rows[0]["CostCenterName"].ToString();
            txtVerifiedBy.Value = Main_DT.Rows[0]["Verifiedby"].ToString();
            txtDamagedBy.Value = Main_DT.Rows[0]["Damagedby"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();
            txtNote.Text = Main_DT.Rows[0]["Note"].ToString();
           
            //Pur_Order_Receipt_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select ItemCode,ItemName,UOMCode,Qty,Rate as Value,ZoneName,BinName from Damage_Entry_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Damage_Entry_No='" + txtTransNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("Damage_Entry_Main.aspx");
    }

    private void Employee_Name_Load()
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        txtDamagedBy.Items.Clear();
        txtVerifiedBy.Items.Clear();
        query = "Select * from MstEmployee where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by EmpName Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDamagedBy.Items.Add("-Select-");
        txtVerifiedBy.Items.Add("-Select-");
        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {
            txtDamagedBy.Items.Add(Main_DT.Rows[i]["EmpName"].ToString());
            txtVerifiedBy.Items.Add(Main_DT.Rows[i]["EmpName"].ToString());
        }
    }


  

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Damage Entry");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Damage Entry...');", true);
        //}
        //User Rights Check End

        query = "Select * from Damage_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Damage_Entry_No='" + txtTransNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Damage Entry Details..');", true);
        }

        query = "Select * from Damage_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Damage_Entry_No='" + txtTransNo.Text + "' And Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Damage Entry Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            query = "Update Damage_Entry_Main set Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And Damage_Entry_No='" + txtTransNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            Stock_Add();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Damage Entry Details Approved Successfully..');", true);
        }
    }

    private void Stock_Add()
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();

        query = "Select * from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            query = "Delete from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTransNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }


        DateTime transDate = Convert.ToDateTime(txtDate.Text);

        //Insert Stock_Transaction_Ledger
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            query = "Insert Into Stock_Transaction_Ledger(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
            query = query + " Trans_Type,DeptCode,DeptName,CostCenterCode,CostCenterName,ItemCode,ItemName,Open_Qty,Open_Value,PO_Qty,PO_Value,General_Qty,General_Value,Issue_Qty,";
            query = query + " Issue_Value,Issue_Return_Qty,Issue_Return_Value,GP_IN_Qty,GP_IN_Value,GP_OUT_Qty,GP_OUT_Value,Transfer_Qty,Transfer_Value,Damage_Qty,Damage_Value,WarehouseCode,WarehouseName,ZoneName,BinName,Supp_Code,";
            query = query + " Supp_Name,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
            query = query + " '" + SessionFinYearVal + "','" + txtTransNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtDate.Text + "','Damage Entry','" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "','" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "',";
            query = query + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
            query = query + " '0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0',";
            query = query + " '0.0','0.0','0.0','0.0','0.0','0.0','0.0','" + dt.Rows[i]["Qty"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "','" + txtWarehouseName.SelectedValue + "',";
            query = query + " '" + txtWarehouseName.Text + "','" + dt.Rows[i]["ZoneName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "',";
            query = query + " '','','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            CommonClass_Function.cls_Trans_Type = "Damage Entry";
            CommonClass_Function.cls_ItemCode = dt.Rows[i]["ItemCode"].ToString();
            CommonClass_Function.cls_ItemName = dt.Rows[i]["ItemName"].ToString();
            CommonClass_Function.cls_Add_Qty = "0.0";
            CommonClass_Function.cls_Add_Value = "0.0";
            CommonClass_Function.cls_Minus_Qty = dt.Rows[i]["Qty"].ToString();
            CommonClass_Function.cls_Minus_Value = dt.Rows[i]["Value"].ToString();
            CommonClass_Function.cls_DeptCode = txtDepartmentName.SelectedValue;
            CommonClass_Function.cls_DeptName = txtDepartmentName.SelectedItem.Text;
            CommonClass_Function.cls_CostCenterCode = txtCostCenterName.SelectedValue;
            CommonClass_Function.cls_CostCenterName = txtCostCenterName.SelectedItem.Text;
            CommonClass_Function.cls_WarehouseCode = txtWarehouseName.SelectedValue;
            CommonClass_Function.cls_WarehouseName = txtWarehouseName.SelectedItem.Text;
            CommonClass_Function.cls_ZoneName = dt.Rows[i]["ZoneName"].ToString();
            CommonClass_Function.cls_BinName = dt.Rows[i]["BinName"].ToString();
            CommonClass_Function.cls_Supp_Code = "";
            CommonClass_Function.cls_Supp_Name = "";
            CommonClass_Function.cls_UserID = SessionUserID;
            CommonClass_Function.cls_UserName = SessionUserName;
            //CommonClass_Function.cls_Stock_Qty = "";
            //CommonClass_Function.cls_Stock_Value = "";

            CommonClass_Function.Stock_Ledger_And_Current_Stock_Add(SessionCcode, SessionLcode, SessionFinYearCode, SessionFinYearVal, txtTransNo.Text, txtDate.Text);
        }




    }
  

    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
       // modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();

    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtWarehouseName.Text = Convert.ToString(e.CommandName);


        Load_Data_Empty_ZoneName();
        Load_Data_Empty_BinName();
    }


    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
       // txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnCostCenterName_Click(object sender, EventArgs e)
    {
        //modalPop_CostCenterName.Show();
    }
    private void Load_Data_Empty_CostCenterName()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtCostCenterName.Items.Clear();
        query = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCostCenterName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["CostcenterCode"] = "-Select-";
        dr["CostcenterName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCostCenterName.DataTextField = "CostcenterName";
        txtCostCenterName.DataValueField = "CostcenterCode";
        txtCostCenterName.DataBind();

    }

    protected void GridViewClick_CostCenterName(object sender, CommandEventArgs e)
    {
        //txtCostCenterCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtCostCenterName.Text = Convert.ToString(e.CommandName);


    }


    protected void btnZoneName_Click(object sender, EventArgs e)
    {
        //modalPop_ZoneName.Show();
    }

    private void Load_Data_Empty_ZoneName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtZoneName.Items.Clear();
        query = "Select ZoneName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And WarehouseCode='" + txtWarehouseName.SelectedValue + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtZoneName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["ZoneName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtZoneName.DataTextField = "ZoneName";

        txtZoneName.DataBind();
    }

    protected void GridViewClick_ZoneName(object sender, CommandEventArgs e)
    {
        txtZoneName.Text = Convert.ToString(e.CommandArgument);

        Load_Data_Empty_BinName();
    }

    protected void btnBinName_Click(object sender, EventArgs e)
    {
        //modalPop_BinName.Show();
    }

    private void Load_Data_Empty_BinName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtBinName.Items.Clear();
        query = "Select BinName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And WarehouseCode='" + txtWarehouseName.SelectedValue + "' And ZoneName='" + txtZoneName.SelectedItem.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtBinName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["BinName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtBinName.DataTextField = "BinName";

        txtBinName.DataBind();
    }

    protected void GridViewClick_BinName(object sender, CommandEventArgs e)
    {
        txtBinName.Text = Convert.ToString(e.CommandArgument);
    }


    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();


        query = "Select ItemCode,ItemShortName as ItemName,PurchaseUOM as UOMCode,ValuationType from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);


        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string itemcode = commandArgs[0];
        string UOMCode = commandArgs[1];


        txtItemCodeHide.Value = itemcode;
        txtItemName.Text = Convert.ToString(e.CommandName);
        txtItemUOMHide.Value = UOMCode;

    }


    protected void txtWarehouseName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_ZoneName();
        Load_Data_Empty_BinName();
    }
    protected void txtZoneName_SelectedIndexChanged(object sender, EventArgs e)
    {

        Load_Data_Empty_BinName();
    }

}
