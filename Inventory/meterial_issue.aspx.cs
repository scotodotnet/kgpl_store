﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Inventory_meterial_issue : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionMaterialIssueNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Material Issue";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Inventory"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh_Material_Request_Item();
            Employee_Name_Load();
            Load_Data_Empty_CostCenterName();
            Load_Data_Empty_Dept();
            Load_Data_Empty_WareHouse();
            Load_Data_Empty_ItemCode();
            Load_Data_Empty_OtherUnit();

            Load_Data_Empty_ItemType();

            if (Session["Mat_Issue_No"] == null)
            {
                SessionMaterialIssueNo = "";
            }
            else
            {
                SessionMaterialIssueNo = Session["Mat_Issue_No"].ToString();
                txtIssueNo.Text = SessionMaterialIssueNo;
                btnSearch_Click(sender, e);
            }

            RdpIssueType_SelectedIndexChanged(sender, e);

        }
        Load_OLD_data();


        //Load_Data_Empty_ZoneName();
        //Load_Data_Empty_BinName();
        Load_Data_Empty_MatRequestNo();

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }

        //Check Material Request No
        if (RdpIssueType.SelectedValue == "1")
        {
            SSQL = "Select * from Meterial_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Req_No='" + txtMaterialRequestNo.Text + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Material Request No...');", true);
            }
        }


        //Check Cost Center Name
        SSQL = "Select * from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CostcenterCode='" + txtCostCenter_Sub.SelectedValue + "' And CostcenterName='" + txtCostCenter_Sub.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Cost Center Name...');", true);
        }

        //Check Taken by And Issued by
        if (txtIssuedBy.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Issued by...');", true);
        }
        if (txtTakenBy.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Taken by...');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Material Issue");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Material Issue Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Material Issue");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Material Issue..');", true);
            }
        }
        //User Rights Check End


        //Other Unit 

        int OtherState = 0;

        if (chkOthers.Checked)
        {
            OtherState = 1;
        }

        //Other Unit End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Material Issue", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtIssueNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            string Mat_Type = "";
            SSQL = "Select * from Meterial_Issue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Issue_No='" + txtIssueNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                SSQL = "Delete from Meterial_Issue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Issue_No='" + txtIssueNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "Delete from Meterial_Issue_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Issue_No='" + txtIssueNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            //Response.Write(strValue);
            //Insert Main Table
            SSQL = "Insert Into Meterial_Issue_Main(Ccode,Lcode,FinYearCode,FinYearVal,Mat_Issue_No,Ledger_No,Mat_Issue_Date,CostCenterCode,CostCenterName,Takenby,Issuedby,";
            SSQL = SSQL + " Remarks,OtherUnit_Status,OtherUnitCode,OtherUnitName,IssueType,Mat_Req_No,Mat_Req_Date,UserID,UserName) Values('" + SessionCcode + "',";
            SSQL = SSQL + "'" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtIssueNo.Text + "','" + txtLedgerNo.Text + "',";
            SSQL = SSQL + "'" + txtDate.Text + "','" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "','" + txtTakenBy.Value + "',";
            SSQL = SSQL + "'" + txtIssuedBy.Value + "','" + txtRemarks.Text + "','" + OtherState + "','" + ddlOtherUnit.SelectedValue + "','" + ddlOtherUnit.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + RdpIssueType.SelectedValue + "','" + txtMaterialRequestNo.Text + "','" + txtMaterialRequestDate.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            //Meterial_Issue_Main_sub
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["ReuseType"].ToString() == "OLD")
                {
                    Mat_Type = "OLD";
                }
                else
                {
                    Mat_Type = "NEW";
                }


                SSQL = "Insert Into Meterial_Issue_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Mat_Issue_No,Mat_Issue_Date,Mat_Req_No,Mat_Req_Date,ItemCode,ItemName,";
                SSQL = SSQL + "UOMCode,DeptCode,DeptName,RequestQty,RequestDate,IssueQty,Value,StockQty,WarehouseCode,WarehouseName,ZoneName,";
                SSQL = SSQL + "BinName,UserID,UserName,Mat_Type,CostCenterCode,CostCenterName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                SSQL = SSQL + "'" + SessionFinYearVal + "','" + txtIssueNo.Text + "','" + txtDate.Text + "','" + txtMaterialRequestNo.Text + "',";
                SSQL = SSQL + "'" + txtMaterialRequestDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                SSQL = SSQL + "'" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["DeptCode"].ToString() + "','" + dt.Rows[i]["DeptName"].ToString() + "',";
               // SSQL = SSQL + "'" + dt.Rows[i]["ItemTypeCode"].ToString() + "','" + dt.Rows[i]["ItemType"].ToString() + "',";
                SSQL = SSQL + "'" + dt.Rows[i]["RequestQty"].ToString() + "','" + dt.Rows[i]["RequestDate"].ToString() + "','" + dt.Rows[i]["IssueQty"].ToString() + "',";
                SSQL = SSQL + "'" + dt.Rows[i]["Value"].ToString() + "','" + dt.Rows[i]["StockQty"].ToString() + "','" + dt.Rows[i]["WarehouseCode"].ToString() + "',";
                SSQL = SSQL + "'" + dt.Rows[i]["WarehouseName"].ToString() + "', '" + dt.Rows[i]["BinName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "',";
                SSQL = SSQL + "'" + SessionUserID + "','" + SessionUserName + "','" + Mat_Type + "','" + dt.Rows[i]["CostCenterCode"].ToString() + "',";
                SSQL = SSQL + "'" + dt.Rows[i]["CostCenterName"].ToString() + "')";

                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Material Issue Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Material Issue Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Mat_Issue_No"] = txtIssueNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtIssueNo.Text = ""; txtDate.Text = ""; txtWarehouseName.SelectedValue = "-Select-";
        txtDepartmentName.SelectedValue = "-Select-"; txtCostCenterName.SelectedValue = "-Select-";
        txtIssuedBy.Value = ""; txtTakenBy.Value = ""; txtRemarks.Text = ""; RdpIssueType.SelectedValue = "1";
        txtMaterialRequestNo.Text = ""; txtMaterialRequestDate.Text = ""; txtMaterialRequestNo.Enabled = true;
        txtMaterialRequestDate.Enabled = true; txtIssuedBy.Value = "-Select-"; txtTakenBy.Value = "-Select-";
        txtMaterialRequestNo.Enabled = true; txtMaterialRequestDate.Enabled = true;
        txtRequestQty.Enabled = true; txtRequestDate.Enabled = true;
        ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-";
        //hfUOMId.Value = "";
        txtRequestQty.Text = "";
        txtRequestDate.Text = ""; txtIssueQty.Text = ""; txtValue.Text = ""; txtZoneName.Text = ""; txtBinName.Text = "";
        btnMatReqNo.Enabled = true; txtStockQty.Text = "0.00";
        txtLedgerNo.Text = "";
        btnSave.Text = "Save";
        Initial_Data_Referesh_Material_Request_Item();
        Session.Remove("Mat_Issue_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string SSQL = "";
        string Old_New_Type = "";
        Old_New_Type = RbtOld_New.SelectedValue;
        if (Old_New_Type == "1")
        {
            txtOld_NewCount.Text = "NEW";
        }
        else
        {
            txtOld_NewCount.Text = "OLD";

        }

        if (txtIssueQty.Text == "0.0" || txtIssueQty.Text == "0" || txtIssueQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Issue Qty...');", true);
        }

        if (Old_New_Type == "2")
        {
            if (txtValue.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Value...');", true);
            }
        }
        else
        {
            if (txtValue.Text == "0.0" || txtValue.Text == "0" || txtValue.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Value...');", true);
            }
        }


        if (RdpIssueType.SelectedValue == "1")
        {
            if (txtRequestQty.Text == "0.0" || txtRequestQty.Text == "0" || txtRequestQty.Text == "" || txtRequestDate.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Request Qty And Date...');", true);
            }
        }
        //Check CostCenter Name
        SSQL = "Select * from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CostcenterCode='" + txtCostCenter_Sub.SelectedValue + "' And CostcenterName='" + txtCostCenter_Sub.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Costcenter Name...');", true);
        }

        //Check Warehouse Name
        SSQL = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseName.SelectedValue + "' ";
        qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Warehouse Name...');", true);
        }

        //Check Department Name
        SSQL = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + txtDepartmentName.SelectedValue + "' And DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Department Name...');", true);
        }
        //check with Zone Name And Bin Name
        SSQL = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseName.SelectedValue + "'";
        //And BinName='" + txtBinName.Text + "'";
        //And ZoneName='" + txtZoneName.Text + "' And BinName='" + txtBinName.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Bin Details..');", true);
        }

        //check with Item Code And Item Name
        if (RdpIssueType.SelectedValue == "1")
        {
            SSQL = "Select * from Meterial_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And ItemName='" + ddlItemName.SelectedItem.Text + "' And Mat_Req_No='" + txtMaterialRequestNo.Text + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else
        {
            SSQL = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And ItemShortName='" + ddlItemName.SelectedItem.Text.Trim() + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        }

        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
        }

        //check Item Stock
        if (Old_New_Type == "2")
        {
            SSQL = "Select (SUM(Add_Qty))-(SUM(Minus_Qty))as Stock_Qty  from Reuse_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And FinYearCode='" + SessionFinYearCode + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else
        {

            //SSQL = "Select * from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And FinYearCode='" + SessionFinYearCode + "'";
            //qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select ((sum(Add_Qty)) - sum(Minus_Qty)) as Stock_Qty from Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
            //SSQL = SSQL + " And DeptCode='" + DT.Rows[0]["DeptCode"].ToString() + "' And DeptName='" + DT.Rows[0]["DeptName"].ToString() + "'";

            qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        }


        if (qry_dt.Rows.Count != 0)
        {
            if (qry_dt.Rows[0]["Stock_Qty"].ToString() == "0.00")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('There is no stock');", true);
            }
            else if (Convert.ToDecimal(qry_dt.Rows[0]["Stock_Qty"]) < Convert.ToDecimal(txtIssueQty.Text))
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Issue Qty. It exceeds Stock Qty..');", true);
            }

        }


        if (!ErrFlag)
        {

            if (Old_New_Type == "2")
            {
                SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName, Add_Qty as Stock_Qty from Reuse_Stock_Ledger_All  where   Trans_Type!='MATERIAL REUSE' and Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
                qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else
            {
                //SSQL = "Select * from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And FinYearCode='" + SessionFinYearCode + "'";

                SSQL = "Select ((sum(Add_Qty)) - sum(Minus_Qty)) as Stock_Qty from Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
                qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
            }

            //check Item Stock

            if (qry_dt.Rows.Count != 0)
            {
                if (qry_dt.Rows[0]["Stock_Qty"].ToString() == "0.00")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('There is no stock');", true);
                }
                else if (Convert.ToDecimal(qry_dt.Rows[0]["Stock_Qty"]) < Convert.ToDecimal(txtIssueQty.Text))
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Issue Qty. It exceeds Stock Qty..');", true);
                }

            }
            else
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Item in Stock Details');", true);
            }

            if (ErrFlag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('There is no stock');", true);
            }
        }


        if (!ErrFlag)
        {
            //UOM Code get
            string UOM_Code_Str = hfUOMId.Value.ToString();

            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == ddlItemName.SelectedValue.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('this Item Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["ItemCode"] = ddlItemName.SelectedValue;
                    dr["ItemName"] = ddlItemName.SelectedItem.Text;
                    dr["UOMCode"] = UOM_Code_Str;
                    dr["DeptCode"] = txtDepartmentName.SelectedValue;
                    dr["DeptName"] = txtDepartmentName.SelectedItem.Text;
                    //dr["ItemTypeCode"] = ddlItemType.SelectedValue;
                    //dr["ItemType"] = ddlItemType.SelectedItem.Text;
                    dr["WarehouseCode"] = txtWarehouseName.SelectedValue;
                    dr["WarehouseName"] = txtWarehouseName.SelectedItem.Text;
                    dr["RequestQty"] = txtRequestQty.Text;
                    dr["RequestDate"] = txtRequestDate.Text;
                    dr["IssueQty"] = txtIssueQty.Text;
                    dr["Value"] = txtValue.Text;
                    dr["StockQty"] = txtStockQty.Text;
                    dr["ZoneName"] = txtBinName.Text;
                    dr["BinName"] = txtBinName.Text;
                    dr["ReuseType"] = txtOld_NewCount.Text;
                    dr["CostcenterCode"] = txtCostCenter_Sub.SelectedValue;
                    dr["CostcenterName"] = txtCostCenter_Sub.SelectedItem.Text;


                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-"; hfUOMId.Value = ""; txtRequestQty.Text = "";
                    //ddlItemType.SelectedValue = "Select"; ddlItemType.SelectedItem.Text = "Select";
                    txtRequestDate.Text = ""; txtIssueQty.Text = ""; txtValue.Text = ""; txtZoneName.Text = ""; txtBinName.Text = "";
                    txtDepartmentName.SelectedValue = "-Select-"; txtWarehouseName.SelectedValue = "-Select-";
                    //txtCostCenter_Sub.SelectedItem.Text = "-Select-"; txtCostCenter_Sub.SelectedValue = "-Select-";
                    txtStockQty.Text = "0.00";
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["ItemCode"] = ddlItemName.SelectedValue;
                dr["ItemName"] = ddlItemName.SelectedItem.Text;
                dr["UOMCode"] = UOM_Code_Str;
                dr["DeptCode"] = txtDepartmentName.SelectedValue;
                dr["DeptName"] = txtDepartmentName.SelectedItem.Text;
                //dr["ItemTypeCode"] = ddlItemType.SelectedValue;
                //dr["ItemType"] = ddlItemType.SelectedItem.Text;
                dr["WarehouseCode"] = txtWarehouseName.SelectedValue;
                dr["WarehouseName"] = txtWarehouseName.SelectedItem.Text;
                dr["RequestQty"] = txtRequestQty.Text;
                dr["RequestDate"] = txtRequestDate.Text;
                dr["IssueQty"] = txtIssueQty.Text;
                dr["Value"] = txtValue.Text;
                dr["StockQty"] = txtStockQty.Text;
                dr["ZoneName"] = txtBinName.Text;
                dr["BinName"] = txtBinName.Text;
                dr["CostcenterCode"] = txtCostCenter_Sub.SelectedValue;
                dr["CostcenterName"] = txtCostCenter_Sub.SelectedItem.Text;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                ddlItemName.SelectedValue = ""; ddlItemName.SelectedItem.Text = ""; hfUOMId.Value = ""; txtRequestQty.Text = "";
                txtRequestDate.Text = ""; txtIssueQty.Text = ""; txtValue.Text = ""; txtZoneName.Text = ""; txtBinName.Text = "";
                //ddlItemType.SelectedValue = "Select"; ddlItemType.SelectedItem.Text = "Select";
                txtDepartmentName.SelectedValue = "-Select-"; txtWarehouseName.SelectedValue = "-Select-";
                txtCostCenter_Sub.SelectedItem.Text = "-Select-"; txtCostCenter_Sub.SelectedValue = "-Select-";
                txtStockQty.Text = "0.00";
            }
        }
    }

    private void Initial_Data_Referesh_Material_Request_Item()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));

        dt.Columns.Add(new DataColumn("DeptCode", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        //dt.Columns.Add(new DataColumn("ItemTypeCode", typeof(string)));
        //dt.Columns.Add(new DataColumn("ItemType", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseCode", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseName", typeof(string)));

        dt.Columns.Add(new DataColumn("RequestQty", typeof(string)));
        dt.Columns.Add(new DataColumn("RequestDate", typeof(string)));
        dt.Columns.Add(new DataColumn("IssueQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Value", typeof(string)));
        dt.Columns.Add(new DataColumn("StockQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ZoneName", typeof(string)));
        dt.Columns.Add(new DataColumn("BinName", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuseType", typeof(string)));
        dt.Columns.Add(new DataColumn("CostcenterCode", typeof(string)));
        dt.Columns.Add(new DataColumn("CostcenterName", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSourceS;
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";



        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        qry = "Select * from Meterial_Issue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Issue_No='" + txtIssueNo.Text + "' And Mat_Issue_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(qry);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
        }


        if (ErrFlag)
        {
            //ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Delete Material Issue Details Already Approved..');", true);
        }

        else
        {
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
                {
                    dt.Rows.RemoveAt(i);
                    dt.AcceptChanges();
                }
            }
            ViewState["ItemTable"] = dt;
            Load_OLD_data();
        }

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        SSQL = "Select * from Meterial_Issue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Issue_No='" + txtIssueNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["Mat_Issue_Date"].ToString();
            txtLedgerNo.Text = Main_DT.Rows[0]["Ledger_No"].ToString();

            // txtWarehouseCodeHide.Value = Main_DT.Rows[0]["WarehouseCode"].ToString();
            // txtWarehouseName.Text = Main_DT.Rows[0]["WarehouseName"].ToString();
            // txtDeptCodeHide.Value = Main_DT.Rows[0]["DeptCode"].ToString();
            // txtDepartmentName.Text = Main_DT.Rows[0]["DeptName"].ToString();

            txtCostCenterName.SelectedValue = Main_DT.Rows[0]["CostCenterCode"].ToString();
            txtCostCenterName.SelectedItem.Text = Main_DT.Rows[0]["CostCenterName"].ToString();
            txtTakenBy.Value = Main_DT.Rows[0]["Takenby"].ToString();
            txtIssuedBy.Value = Main_DT.Rows[0]["Issuedby"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();
            RdpIssueType.SelectedValue = Main_DT.Rows[0]["IssueType"].ToString();
            if (RdpIssueType.SelectedValue == "1")
            {
                txtMaterialRequestNo.Enabled = true; txtMaterialRequestDate.Enabled = true;
                txtRequestQty.Enabled = true; txtRequestDate.Enabled = true;
                txtMaterialRequestNo.Text = Main_DT.Rows[0]["Mat_Req_No"].ToString();
                txtMaterialRequestDate.Text = Main_DT.Rows[0]["Mat_Req_Date"].ToString();
            }
            else
            {
                txtMaterialRequestNo.Enabled = false; txtMaterialRequestDate.Enabled = false;
                txtRequestQty.Enabled = false; txtRequestDate.Enabled = false;
                txtMaterialRequestNo.Text = ""; txtMaterialRequestDate.Text = "";
                ddlItemName.SelectedValue = ""; ddlItemName.SelectedItem.Text = ""; hfUOMId.Value = "";
                txtRequestQty.Text = ""; txtRequestDate.Text = "";
            }
            //Pur_Order_Receipt_Main_Sub Table Load
            DataTable dt = new DataTable();
            SSQL = "Select ItemCode,ItemName,UOMCode,DeptCode,DeptName,RequestQty,RequestDate,IssueQty,Value,StockQty,";
            SSQL = SSQL + "WarehouseCode,WarehouseName,ZoneName,BinName,Mat_Type as ReuseType,CostCenterCode,CostCenterName from Meterial_Issue_Main_Sub ";
            SSQL = SSQL + "Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ";
            SSQL = SSQL + "Mat_Issue_No ='" + txtIssueNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("meterial_issue_main.aspx");
    }

    private void Employee_Name_Load()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();
        txtIssuedBy.Items.Clear();
        txtTakenBy.Items.Clear();
        SSQL = "Select * from MstEmployee where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by EmpName Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtIssuedBy.Items.Add("-Select-");
        txtTakenBy.Items.Add("-Select-");
        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {
            txtIssuedBy.Items.Add(Main_DT.Rows[i]["EmpName"].ToString());
            txtTakenBy.Items.Add(Main_DT.Rows[i]["EmpName"].ToString());
        }
    }


    protected void RdpIssueType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RdpIssueType.SelectedValue == "1")
        {
            txtMaterialRequestNo.Enabled = true;
            txtMaterialRequestDate.Enabled = true;
            btnMatReqNo.Enabled = true;
            txtRequestQty.Enabled = true;
            txtRequestDate.Enabled = true;

            //btnWareHouse.Enabled = true;
            //btnDept.Enabled = true;

        }
        else
        {
            txtMaterialRequestNo.Enabled = false;
            txtMaterialRequestDate.Enabled = false;
            btnMatReqNo.Enabled = false;
            txtRequestQty.Enabled = false;
            txtRequestDate.Enabled = false;
            txtMaterialRequestNo.Text = ""; txtMaterialRequestDate.Text = "";
            ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-"; hfUOMId.Value = "";
            txtRequestQty.Text = ""; txtRequestDate.Text = "";
        }
        if (RdpIssueType.SelectedValue == "1")
        {
            Load_Data_Empty_MatRequestNo();
        }
        Load_Data_Empty_ItemCode();

    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Material Issue");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Material Issue...');", true);
        //}
        //User Rights Check End

        SSQL = "Select * from Meterial_Issue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Issue_No='" + txtIssueNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Material Issue Details..');", true);
        }

        SSQL = "Select * from Meterial_Issue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Issue_No='" + txtIssueNo.Text + "' And Mat_Issue_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Material Issue Details Already Approved..');", true);
        }




        if (!ErrFlag)
        {
            SSQL = "Update Meterial_Issue_Main set Mat_Issue_Status='1' where Ccode='" + SessionCcode + "'";
            SSQL = SSQL + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + "  And Mat_Issue_No='" + txtIssueNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            Stock_Add();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Material Issue Details Approved Successfully..');", true);
        }
    }

    private void Stock_Add()
    {
        string SSQL = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();


        string Old_New_Type = "";
        Old_New_Type = RbtOld_New.SelectedValue;
        if (Old_New_Type == "1")
        {
            txtOld_NewCount.Text = "NEW";
        }
        else
        {
            txtOld_NewCount.Text = "OLD";

        }


        SSQL = "Select * from Reuse_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtIssueNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Check.Rows.Count != 0)
        {
            SSQL = "Delete from Reuse_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtIssueNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
        }


        SSQL = "Select * from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtIssueNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Check.Rows.Count != 0)
        {
            SSQL = "Delete from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtIssueNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
        }


        DateTime transDate = Convert.ToDateTime(txtDate.Text);

        //Insert Stock_Transaction_Ledger
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {

            if (dt.Rows[i]["ReuseType"].ToString() == "OLD")
            {
                SSQL = "Insert Into Reuse_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
                SSQL = SSQL + " Trans_Type,DeptCode,DeptName,CostCenterCode,CostCenterName,ItemCode,ItemName,Add_Qty,Add_Value,Minus_Qty,Minus_Value,WarehouseCode,WarehouseName,";
                SSQL = SSQL + " ZoneName,BinName,Supp_Code,Supp_Name,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtIssueNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtDate.Text + "',";
                SSQL = SSQL + " 'MATERIAL REUSE','" + dt.Rows[i]["DeptCode"].ToString() + "','" + dt.Rows[i]["DeptName"].ToString() + "','" + dt.Rows[i]["CostCenterCode"].ToString() + "','" + dt.Rows[i]["CostCenterName"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                SSQL = SSQL + " '0.0','0.0','" + dt.Rows[i]["IssueQty"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "','" + dt.Rows[i]["WarehouseCode"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["WarehouseName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "',";
                SSQL = SSQL + " '','','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else
            {
                //SSQL = "Insert Into Stock_Transaction_Ledger(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
                //SSQL = SSQL + " Issue_Value,Issue_Return_Qty,Issue_Return_Value,GP_IN_Qty,GP_IN_Value,GP_OUT_Qty,GP_OUT_Value,Transfer_Qty,Transfer_Value,Damage_Qty,Damage_Value,Spares_ReSales_Qty,Spares_ReSales_Value,WarehouseCode,WarehouseName,ZoneName,BinName,Supp_Code,";
                //SSQL = SSQL + " Supp_Name,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                //SSQL = SSQL + " '" + SessionFinYearVal + "','" + txtIssueNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtDate.Text + "','MATERIAL ISSUE','" + dt.Rows[i]["DeptCode"].ToString() + "','" + dt.Rows[i]["DeptName"].ToString() + "','" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "',";
                //SSQL = SSQL + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                //SSQL = SSQL + " '0.0','0.0','0.0','0.0','0.0','0.0','" + dt.Rows[i]["IssueQty"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "','0.0',";
                //SSQL = SSQL + " '0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','" + dt.Rows[i]["WarehouseCode"].ToString() + "',";
                //SSQL = SSQL + " '" + dt.Rows[i]["WarehouseName"].ToString() + "','" + dt.Rows[i]["ZoneName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "',";
                //SSQL = SSQL + " '','','" + SessionUserID + "','" + SessionUserName + "')";
                //objdata.RptEmployeeMultipleDetails(SSQL);


                SSQL = "Insert Into Stock_Transaction_Ledger(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
                SSQL = SSQL + " Trans_Type,DeptCode,DeptName,CostCenterCode,CostCenterName,ItemCode,ItemName,Open_Qty,Open_Value,PO_Qty,PO_Value,General_Qty,General_Value,Issue_Qty,";
                SSQL = SSQL + " Issue_Value,Issue_Return_Qty,Issue_Return_Value,GP_IN_Qty,GP_IN_Value,GP_OUT_Qty,GP_OUT_Value,Transfer_Qty,Transfer_Value,Damage_Qty,Damage_Value,Spares_ReSales_Qty,Spares_ReSales_Value,WarehouseCode,WarehouseName,ZoneName,BinName,Supp_Code,";
                SSQL = SSQL + " Supp_Name,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                SSQL = SSQL + " '" + SessionFinYearVal + "','" + txtIssueNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtDate.Text + "','MATERIAL ISSUE','" + dt.Rows[i]["DeptCode"].ToString() + "','" + dt.Rows[i]["DeptName"].ToString() + "','" + dt.Rows[i]["CostCenterCode"].ToString() + "','" + dt.Rows[i]["CostCenterName"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                SSQL = SSQL + " '0.0','0.0','0.0','0.0','0.0','0.0','" + dt.Rows[i]["IssueQty"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "','0.0',";
                SSQL = SSQL + " '0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','" + dt.Rows[i]["WarehouseCode"].ToString() + "',";
                SSQL = SSQL + " '" + dt.Rows[i]["WarehouseName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "',";
                SSQL = SSQL + " '','','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                CommonClass_Function.cls_Trans_Type = "MATERIAL ISSUE";
                CommonClass_Function.cls_ItemCode = dt.Rows[i]["ItemCode"].ToString();
                CommonClass_Function.cls_ItemName = dt.Rows[i]["ItemName"].ToString();
                CommonClass_Function.cls_Add_Qty = "0.0";
                CommonClass_Function.cls_Add_Value = "0.0";
                CommonClass_Function.cls_Minus_Qty = dt.Rows[i]["IssueQty"].ToString();
                CommonClass_Function.cls_Minus_Value = dt.Rows[i]["Value"].ToString();
                CommonClass_Function.cls_DeptCode = dt.Rows[i]["DeptCode"].ToString();
                CommonClass_Function.cls_DeptName = dt.Rows[i]["DeptName"].ToString();
                CommonClass_Function.cls_CostCenterCode = txtCostCenterName.SelectedValue;
                CommonClass_Function.cls_CostCenterName = txtCostCenterName.SelectedItem.Text;
                CommonClass_Function.cls_WarehouseCode = dt.Rows[i]["WarehouseCode"].ToString();
                CommonClass_Function.cls_WarehouseName = dt.Rows[i]["WarehouseName"].ToString();
                CommonClass_Function.cls_ZoneName = dt.Rows[i]["BinName"].ToString();
                CommonClass_Function.cls_BinName = dt.Rows[i]["BinName"].ToString();
                CommonClass_Function.cls_Supp_Code = "";
                CommonClass_Function.cls_Supp_Name = "";
                CommonClass_Function.cls_UserID = SessionUserID;
                CommonClass_Function.cls_UserName = SessionUserName;
                //CommonClass_Function.cls_Stock_Qty = "";
                //CommonClass_Function.cls_Stock_Value = "";

                CommonClass_Function.Stock_Ledger_And_Current_Stock_Add(SessionCcode, SessionLcode, SessionFinYearCode, SessionFinYearVal, txtIssueNo.Text, txtDate.Text);
            }


        }




    }
    protected void txtIssueQty_TextChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;

        string Old_New_Type = "";
        Old_New_Type = RbtOld_New.SelectedValue;

        if (RdpIssueType.SelectedValue == "1")
        {

            if (Convert.ToDecimal(txtRequestQty.Text) < Convert.ToDecimal(txtIssueQty.Text))
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Issue Quantity.Lesser or equal to Request Qty');", true);
            }
            else
            {
                ErrFlag = false;
            }
        }

        if (!ErrFlag)
        {
            if (txtIssueQty.Text != "") { txtValue.Text = "0.00"; }

            if (Old_New_Type == "2")
            {
                //check Item Stock
                SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,BinName,(SUM(Add_Qty)-SUM(Minus_Qty)) as Stock_Qty from Reuse_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And FinYearCode='" + SessionFinYearCode + "' group by DeptCode,DeptName,WarehouseCode,WarehouseName,BinName";
                qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else
            {
                //check Item Stock
                //SSQL = "Select * from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And FinYearCode='" + SessionFinYearCode + "'";

                SSQL = "Select ((sum(Add_Qty)) - sum(Minus_Qty)) as Stock_Qty from Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";

                qry_dt = objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (qry_dt.Rows.Count != 0)
            {
                if (qry_dt.Rows[0]["Stock_Qty"].ToString() == "0.00")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('There is no stock');", true);
                }
                else if (Convert.ToDecimal(qry_dt.Rows[0]["Stock_Qty"]) < Convert.ToDecimal(txtIssueQty.Text))
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Issue Qty. It exceeds Stock Qty..');", true);
                }
                //Get Value
                if (!ErrFlag)
                {



                    string Stock_Qty = "0.00";
                    string Stock_Value = "0.00";
                    //Get Stock Qty
                    DataTable DT_ST = new DataTable();
                    if (Old_New_Type == "2")
                    {
                        SSQL = " Select (sum(Add_Qty) - sum(Minus_Qty)) as Stock_Qty from Reuse_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
                        SSQL = SSQL + " And DeptCode='" + txtDepartmentName.SelectedValue + "' And DeptName='" + txtDepartmentName.SelectedItem.Text + "' And FinYearCode='" + SessionFinYearCode + "'";
                        DT_ST = objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                    else
                    {

                        //SSQL = "Select ((sum(General_Qty) + sum(Open_Qty)) - sum(Issue_Qty)) as Stock_Qty from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And FinYearCode='" + SessionFinYearCode + "'";
                        //SSQL = SSQL + " And DeptCode='" + txtDepartmentName.SelectedValue + "' And DeptName='" + txtDepartmentName.SelectedItem.Text + "'";


                        SSQL = "Select (sum(Add_Qty) - sum(Minus_Qty))  as Stock_Qty from Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And FinYearCode='" + SessionFinYearCode + "'";
                        SSQL = SSQL + " And DeptCode='" + txtDepartmentName.SelectedValue + "' And DeptName='" + txtDepartmentName.SelectedItem.Text + "'";

                        DT_ST = objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                    if (DT_ST.Rows.Count != 0)
                    {
                        Stock_Qty = DT_ST.Rows[0]["Stock_Qty"].ToString();
                    }
                    if (Stock_Qty == "") { Stock_Qty = "0.00"; }
                    //Get Stock Value
                    if (Old_New_Type == "2")
                    {
                        SSQL = " Select sum(Add_Qty)  - sum(Minus_Qty) as Stock_Value from Reuse_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And FinYearCode='" + SessionFinYearCode + "'";
                        SSQL = SSQL + " And DeptCode='" + txtDepartmentName.SelectedValue + "' And DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
                        DT_ST = objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                    else
                    {
                        SSQL = "Select (sum(Add_Value) - sum(Minus_Value)) as Stock_Value from Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And FinYearCode='" + SessionFinYearCode + "'";
                        SSQL = SSQL + " And DeptCode='" + txtDepartmentName.SelectedValue + "' And DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
                        DT_ST = objdata.RptEmployeeMultipleDetails(SSQL);
                    }


                    if (DT_ST.Rows.Count != 0)
                    {
                        Stock_Value = DT_ST.Rows[0]["Stock_Value"].ToString();
                    }
                    txtStockQty.Text = Stock_Qty;

                    if (Stock_Qty == "0.00")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('There is no stock');", true);
                    }
                    else if (Convert.ToDecimal(Stock_Qty) < Convert.ToDecimal(txtIssueQty.Text))
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Issue Qty. It exceeds Stock Qty..');", true);
                    }
                    if (!ErrFlag)
                    {
                        string Get_Stock_Item_Rate = "0.0";
                        if (Old_New_Type == "2")
                        {
                            Get_Stock_Item_Rate = "0.0";
                        }
                        else
                        {
                            Get_Stock_Item_Rate = (Convert.ToDecimal(Stock_Value) / Convert.ToDecimal(Stock_Qty)).ToString();
                        }



                        string Issue_Qty = txtIssueQty.Text.ToString();
                        string Issue_Value = (Convert.ToDecimal(Issue_Qty) * Convert.ToDecimal(Get_Stock_Item_Rate)).ToString();
                        Issue_Value = (Math.Round(Convert.ToDecimal(Issue_Value), 2, MidpointRounding.AwayFromZero)).ToString();
                        txtValue.Text = Issue_Value;
                    }
                }


            }
            else
            {
                ErrFlag = true;
            }

            if (ErrFlag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('There is no stock');", true);
            }
        }
        txtBinName.Focus();

    }


    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        // modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        SSQL = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();


        //string SSQL = "";
        //DataTable DT = new DataTable();


        //SSQL = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

        //DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //Repeater_WareHouse.DataSource = DT;
        //Repeater_WareHouse.DataBind();
        //Repeater_WareHouse.Visible = true;

    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtWarehouseName.Text = Convert.ToString(e.CommandName);




        //Load_Data_Empty_ZoneName();
        //Load_Data_Empty_BinName();
    }
    protected void txtWarehouseName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        SSQL = "Select ZoneName,BinName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And WarehouseCode='" + txtWarehouseName.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            txtZoneName.Text = DT.Rows[0]["BinName"].ToString();
            txtBinName.Text = DT.Rows[0]["BinName"].ToString();
        }
        else
        {
            txtZoneName.Text = "";
            txtBinName.Text = "";
        }
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {


        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        SSQL = "Select distinct DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

        //if (ddlItemName.DataSource != null && ddlItemName.SelectedValue.Text != "-Select-")
        //{
        //    string DeptQuery = "";
        //    DeptQuery = "select DeptCode from MstItemMaster where ItemCode='" + ddlItemName.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        //    DataTable dt = new DataTable();
        //    dt = objdata.RptEmployeeMultipleDetails(DeptQuery);
        //    if (dt.Rows.Count > 0)
        //    {
        //        SSQL = SSQL + " and DeptCode='" + dt.Rows[0]["DeptCode"].ToString() + "'";
        //    }
        //}
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();


        //string SSQL = "";
        //DataTable DT = new DataTable();

        //DT.Columns.Add("DeptCode");
        //DT.Columns.Add("DeptName");

        //SSQL = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        //DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //Repeater_Dept.DataSource = DT;
        //Repeater_Dept.DataBind();
        //Repeater_Dept.Visible = true;

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnCostCenterName_Click(object sender, EventArgs e)
    {
        //modalPop_CostCenterName.Show();
    }
    private void Load_Data_Empty_CostCenterName()
    {
        string SSQL = "";
        DataTable Main_DT = new DataTable();

        txtCostCenterName.Items.Clear();
        SSQL = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        txtCostCenterName.DataSource = Main_DT;
        txtCostCenter_Sub.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["CostcenterCode"] = "-Select-";
        dr["CostcenterName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCostCenterName.DataTextField = "CostcenterName";
        txtCostCenterName.DataValueField = "CostcenterCode";
        txtCostCenter_Sub.DataTextField = "CostcenterName";
        txtCostCenter_Sub.DataValueField = "CostcenterCode";
        txtCostCenterName.DataBind();
        txtCostCenter_Sub.DataBind();

    }

    protected void GridViewClick_CostCenterName(object sender, CommandEventArgs e)
    {
        //txtCostCenterCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtCostCenterName.Text = Convert.ToString(e.CommandName);


    }


    protected void btnZoneName_Click(object sender, EventArgs e)
    {
        // modalPop_ZoneName.Show();
    }

    //private void Load_Data_Empty_ZoneName()
    //{
    //    string SSQL = "";
    //    DataTable DT = new DataTable();
    //    SSQL = "Select ZoneName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    //    SSQL = SSQL + " And WarehouseCode='" + txtWarehouseCodeHide.Value + "'";
    //    DT = objdata.RptEmployeeMultipleDetails(SSQL);
    //    if (DT.Rows.Count != 0)
    //    {
    //        txtZoneName.Text = DT.Rows[0]["ZoneName"].ToString();
    //    }

    //    //Repeater_ZoneName.DataSource = DT;
    //    //Repeater_ZoneName.DataBind();
    //    //Repeater_ZoneName.Visible = true;
    //}

    protected void GridViewClick_ZoneName(object sender, CommandEventArgs e)
    {
        txtZoneName.Text = Convert.ToString(e.CommandArgument);

        //Load_Data_Empty_BinName();
    }

    protected void btnBinName_Click(object sender, EventArgs e)
    {
        // modalPop_BinName.Show();
    }

    //private void Load_Data_Empty_BinName()
    //{
    //    string SSQL = "";
    //    DataTable DT = new DataTable();
    //    SSQL = "Select BinName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    //    SSQL = SSQL + " And WarehouseCode='" + txtWarehouseCodeHide.Value + "' And ZoneName='" + txtZoneName.Text + "'";
    //    DT = objdata.RptEmployeeMultipleDetails(SSQL);
    //    if (DT.Rows.Count != 0)
    //    {
    //        txtBinName.Text = DT.Rows[0]["BinName"].ToString();
    //    }

    //    //Repeater_BinName.DataSource = DT;
    //    //Repeater_BinName.DataBind();
    //    //Repeater_BinName.Visible = true;
    //}

    protected void GridViewClick_BinName(object sender, CommandEventArgs e)
    {
        txtBinName.Text = Convert.ToString(e.CommandArgument);
    }


    protected void btnMatReqNo_Click(object sender, EventArgs e)
    {
        modalPop_MatReqNo.Show();
    }

    private void Load_Data_Empty_MatRequestNo()
    {
        string SSQL = "";
        DataTable DT = new DataTable();
        SSQL = "Select Mat_Req_No,Mat_Req_Date from Meterial_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);


        Repeater_MatReqNo.DataSource = DT;
        Repeater_MatReqNo.DataBind();
        Repeater_MatReqNo.Visible = true;

    }

    protected void GridViewClick_MatReqNo(object sender, CommandEventArgs e)
    {
        txtMaterialRequestNo.Text = Convert.ToString(e.CommandArgument);
        txtMaterialRequestDate.Text = Convert.ToString(e.CommandName);

        Load_Data_Empty_ItemCode();
    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        //modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {
        string SSQL = "";

        string IssueType = "";
        string Old_New_Type = "";


        IssueType = RdpIssueType.SelectedValue;
        Old_New_Type = RbtOld_New.SelectedValue;
        DataTable DT = new DataTable();
        if (IssueType == "1")
        {
            SSQL = "Select ItemCode,ItemName,UOMCode,ReuiredQty,ReuiredDate from Meterial_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And Mat_Req_No='" + txtMaterialRequestNo.Text + "'";

            SSQL = "";

            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            //Repeater_ItemCode_MatReq.DataSource = DT;
            //Repeater_ItemCode_MatReq.DataBind();
            //Repeater_ItemCode_MatReq.Visible = true;
            //Repeater_ItemCode.Visible = false;



        }
        else if (Old_New_Type == "2")
        {

            DataRow dr;

            SSQL = "Select distinct ItemCode,ItemName from Reuse_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            // SSQL = SSQL + " And Mat_Trans_No='" + txtMaterialRequestNo.Text + "'";


            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            ddlItemName.DataSource = DT;
            dr = DT.NewRow();

            dr["ItemCode"] = "-Select-";
            dr["ItemName"] = "-Select-";

            DT.Rows.InsertAt(dr, 0);

            ddlItemName.DataTextField = "ItemName";
            ddlItemName.DataValueField = "ItemCode";

            ddlItemName.DataBind();

        }
        else
        {
            DataRow dr;


            //,IM.PurchaseUOM as UOMCode

            SSQL = "Select  SC.ItemCode,SC.ItemName from Stock_Current_All SC ";
            SSQL = SSQL + "inner join MstItemMaster IM on SC.ItemCode=IM.ItemCode where SC.Ccode='" + SessionCcode + "' And SC.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + "And IM.Ccode='" + SessionCcode + "' And IM.Lcode='" + SessionLcode + "' And SC.FinYearCode='" + SessionFinYearCode + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlItemName.DataSource = DT;
            dr = DT.NewRow();

            dr["ItemCode"] = "-Select-";
            dr["ItemName"] = "-Select-";

            DT.Rows.InsertAt(dr, 0);

            ddlItemName.DataTextField = "ItemName";
            ddlItemName.DataValueField = "ItemCode";

            ddlItemName.DataBind();
        }


    }

    protected void GridViewClick_ItemCode_MatReq(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        DataTable DT = new DataTable();


        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string itemcode = commandArgs[0];
        string UOMCode = commandArgs[1];
        string ReuiredQty = commandArgs[2];
        string ReuiredDate = commandArgs[3];

        ddlItemName.SelectedValue = itemcode;
        ddlItemName.SelectedItem.Text = Convert.ToString(e.CommandName);
        hfUOMId.Value = UOMCode;
        txtRequestQty.Text = ReuiredQty;
        txtRequestDate.Text = ReuiredDate;

        SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,Stock_Qty from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count != 0)
        {
            txtDepartmentName.SelectedValue = DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = DT.Rows[0]["DeptName"].ToString();
            txtWarehouseName.SelectedValue = DT.Rows[0]["WarehouseCode"].ToString();
            txtWarehouseName.SelectedItem.Text = DT.Rows[0]["WarehouseName"].ToString();
            txtZoneName.Text = DT.Rows[0]["BinName"].ToString();
            txtBinName.Text = DT.Rows[0]["BinName"].ToString();
            txtStockQty.Text = DT.Rows[0]["Stock_Qty"].ToString();
        }

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {

        string Old_New_Type = "";
        Old_New_Type = RbtOld_New.SelectedValue;
        if (Old_New_Type == "1")
        {
            txtOld_NewCount.Text = "NEW";
        }
        else
        {
            txtOld_NewCount.Text = "OLD";

        }
        string SSQL = "";
        DataTable DT = new DataTable();

        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string itemcode = commandArgs[0];
        //string UOMCode = commandArgs[1];

        ddlItemName.SelectedValue = itemcode;
        ddlItemName.SelectedItem.Text = Convert.ToString(e.CommandName);
        //hfUOMId.Value = UOMCode;
        string IssueType = "";


        IssueType = RdpIssueType.SelectedValue;
        if (Old_New_Type == "1")
        {
            //btnWareHouse.Enabled = false;
            //btnDept.Enabled = false;

            SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,Stock_Qty from  Stock_Current_All   where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else if (Old_New_Type == "2")
        {
            SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,Add_Qty as Stock_Qty  from Reuse_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        if (DT.Rows.Count != 0)
        {
            txtDepartmentName.SelectedValue = DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = DT.Rows[0]["DeptName"].ToString();
            txtWarehouseName.SelectedValue = DT.Rows[0]["WarehouseCode"].ToString();
            txtWarehouseName.SelectedItem.Text = DT.Rows[0]["WarehouseName"].ToString();
            txtZoneName.Text = DT.Rows[0]["BinName"].ToString();
            txtBinName.Text = DT.Rows[0]["BinName"].ToString();

            DataTable DT_ST = new DataTable();
            if (Old_New_Type == "2")
            {
                SSQL = " Select (sum(Add_Qty) - sum(Minus_Qty)) as Stock_Qty from Reuse_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
                SSQL = SSQL + " And DeptCode='" + DT.Rows[0]["DeptCode"].ToString() + "' And DeptName='" + DT.Rows[0]["DeptName"].ToString() + "'";
                DT_ST = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else
            {
                //Get Stock Qty

                //SSQL = "Select ((sum(General_Qty)) - sum(Issue_Qty)) as Stock_Qty from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
                //SSQL = SSQL + " And DeptCode='" + DT.Rows[0]["DeptCode"].ToString() + "' And DeptName='" + DT.Rows[0]["DeptName"].ToString() + "'";


                SSQL = "Select ((sum(Add_Qty)) - sum(Minus_Qty)) as Stock_Qty from Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
                SSQL = SSQL + " And DeptCode='" + DT.Rows[0]["DeptCode"].ToString() + "' And DeptName='" + DT.Rows[0]["DeptName"].ToString() + "'";

                DT_ST = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (DT_ST.Rows.Count != 0)
            {
                txtStockQty.Text = DT_ST.Rows[0]["Stock_Qty"].ToString();
            }
            else
            {
                txtStockQty.Text = "0.00";

            }
        }
    }

    protected void txtDepartmentName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        string Stock_Qty = "0.00";
        //Get Stock Qty
        DataTable DT_ST = new DataTable();
        SSQL = "Select ((sum(General_Qty) + sum(Open_Qty)) - sum(Issue_Qty)) as Stock_Qty from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
        SSQL = SSQL + " And DeptCode='" + txtDepartmentName.SelectedValue + "' And DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
        DT_ST = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_ST.Rows.Count != 0)
        {
            Stock_Qty = DT_ST.Rows[0]["Stock_Qty"].ToString();
        }
        if (Stock_Qty == "") { Stock_Qty = "0.00"; }
        txtStockQty.Text = Stock_Qty;
    }
    protected void RbtOld_New_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_ItemCode();
    }

    protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
    {

        string Old_New_Type = "";
        Old_New_Type = RbtOld_New.SelectedValue;
        if (Old_New_Type == "1")
        {
            txtOld_NewCount.Text = "NEW";
        }
        else
        {
            txtOld_NewCount.Text = "OLD";

        }

        string SSQL = "";
        DataTable DT = new DataTable();
        string IssueType = "";

        //SSQL = "";
        //SSQL = "Select DeptCode,CostCenterCode, from MstItemMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        //SSQL = SSQL + " and ItemCode='" + ddlItemName.SelectedValue + "'";
        //DataTable dt_DeptCode = new DataTable();
        //dt_DeptCode = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (dt_DeptCode.Rows.Count > 0)
        //{
        //    txtDepartmentName.SelectedValue = dt_DeptCode.Rows[0]["DeptCode"].ToString();
        //}



        SSQL = "Select PurchaseUOM AS UOM,CostCenterCode from MstItemMaster Where ItemShortName='" + ddlItemName.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        hfUOMId.Value = DT.Rows[0]["UOM"].ToString();
        txtCostCenterName.SelectedValue = DT.Rows[0]["CostCenterCode"].ToString();

        IssueType = RdpIssueType.SelectedValue;
        if (Old_New_Type == "1")
        {
            //btnWareHouse.Enabled = false;
            //btnDept.Enabled = false;

            SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,Stock_Qty from  Stock_Current_All   where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else if (Old_New_Type == "2")
        {
            SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,Add_Qty as Stock_Qty  from Reuse_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        if (DT.Rows.Count != 0)
        {
            txtDepartmentName.SelectedValue = DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = DT.Rows[0]["DeptName"].ToString();
            txtWarehouseName.SelectedValue = DT.Rows[0]["WarehouseCode"].ToString();
            txtWarehouseName.SelectedItem.Text = DT.Rows[0]["WarehouseName"].ToString();
            txtZoneName.Text = DT.Rows[0]["BinName"].ToString();
            txtBinName.Text = DT.Rows[0]["BinName"].ToString();

            DataTable DT_ST = new DataTable();
            if (Old_New_Type == "2")
            {
                SSQL = " Select (sum(Add_Qty) - sum(Minus_Qty)) as Stock_Qty from Reuse_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
                SSQL = SSQL + " And DeptCode='" + DT.Rows[0]["DeptCode"].ToString() + "' And DeptName='" + DT.Rows[0]["DeptName"].ToString() + "'";
                DT_ST = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else
            {
                //Get Stock Qty

                //SSQL = "Select ((sum(General_Qty)) - sum(Issue_Qty)) as Stock_Qty from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
                //SSQL = SSQL + " And DeptCode='" + DT.Rows[0]["DeptCode"].ToString() + "' And DeptName='" + DT.Rows[0]["DeptName"].ToString() + "'";


                SSQL = "Select ((sum(Add_Qty)) - sum(Minus_Qty)) as Stock_Qty from Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
                SSQL = SSQL + " And DeptCode='" + DT.Rows[0]["DeptCode"].ToString() + "' And DeptName='" + DT.Rows[0]["DeptName"].ToString() + "'";

                DT_ST = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (DT_ST.Rows.Count != 0)
            {
                txtStockQty.Text = DT_ST.Rows[0]["Stock_Qty"].ToString();
            }
            else
            {
                txtStockQty.Text = "0.00";

            }
        }


    }

    protected void chkOthers_CheckedChanged(object sender, EventArgs e)
    {

        if (chkOthers.Checked)
        {
            divTest.Visible = true;
            Load_Data_Empty_OtherUnit();
        }
        else
        {
            divTest.Visible = false;
        }

    }

    private void Load_Data_Empty_OtherUnit()
    {
        string SSQL = "select OtherUnitCode,OtherUnitName from mstOtherUnit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        DataTable DT = new DataTable();
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        ddlOtherUnit.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["OtherUnitCode"] = "-Select-";
        dr["OtherUnitName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlOtherUnit.DataTextField = "OtherUnitName";
        ddlOtherUnit.DataValueField = "OtherUnitCode";

        ddlOtherUnit.DataBind();
    }


    private void Load_Data_Empty_ItemType()
    {
        //string query = "";
        //DataTable Main_DT = new DataTable();

        //ddlItemType.Items.Clear();
        //query = "Select ItemTypeCode,ItemType from MstItemType where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

        //Main_DT = objdata.RptEmployeeMultipleDetails(query);
        //ddlItemType.DataSource = Main_DT;

        //DataRow dr = Main_DT.NewRow();

        //dr["ItemTypeCode"] = "-Select-";
        //dr["ItemType"] = "-Select-";

        //Main_DT.Rows.InsertAt(dr, 0);

        //ddlItemType.DataTextField = "ItemType";
        //ddlItemType.DataValueField = "ItemTypeCode";
        //ddlItemType.DataBind();
    }

    protected void ddlItemType_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
