﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Inventory_Adjustment_Stock_Entry_Approval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionAdjustStockNo;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Stock Adjustment Entry Approve";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Inventory"));
            li.Attributes.Add("class", "droplink active open");
            purchase_Request_Status_Add();

        }
        Load_Data_Empty_Grid();
        //Load_OLD_data();

    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        string Std_No_Str = e.CommandArgument.ToString();
        Session.Remove("Adjust_StockNo");
        Session["Adjust_StockNo"] = Std_No_Str;
        Response.Redirect("Adjustment_Stock_Entry_Approval_View.aspx");
      
    }



    private void purchase_Request_Status_Add()
    {
        txtRequestStatus.Items.Clear();

        txtRequestStatus.Items.Add("Pending List");
        txtRequestStatus.Items.Add("MD Pending List");
        txtRequestStatus.Items.Add("Approved List");
        txtRequestStatus.Items.Add("Rejected List");
    }
    private void Load_Data_Empty_Grid()
    {

        string query = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();


      


        if (txtRequestStatus.Text == "Pending List")
        {
            query = "Select Adjustment_No,Adjustmen_Date,AdjustmenBy from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And (Status = '0' or Status is null)";
            DT = objdata.RptEmployeeMultipleDetails(query);

            Repeater_StdPurOrder.DataSource = DT;
            Repeater_StdPurOrder.DataBind();
            Repeater_StdPurOrder.Visible = true;
        }
        else if (txtRequestStatus.Text == "MD Pending List")
        {
            query = "Select Adjustment_No,Adjustmen_Date,AdjustmenBy from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '3'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            Repeater_StdPurOrder.DataSource = DT;
            Repeater_StdPurOrder.DataBind();
            Repeater_StdPurOrder.Visible = true;
        }
        else if (txtRequestStatus.Text == "Approved List")
        {
            query = "Select Top 30 Adjustment_No,Adjustmen_Date,AdjustmenBy from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '1' ORDER BY Adjustmen_Date Desc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_StdPurOrder.DataSource = DT;
            Repeater_StdPurOrder.DataBind();
            Repeater_StdPurOrder.Visible = true;
        }
        else if (txtRequestStatus.Text == "Rejected List")
        {
            query = "Select Top 30 Adjustment_No,Adjustmen_Date,AdjustmenBy from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '2' ORDER BY Adjustmen_Date Desc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_StdPurOrder.DataSource = DT;
            Repeater_StdPurOrder.DataBind();
            Repeater_StdPurOrder.Visible = true;
        }
    }
    protected void txtRequestStatus_SelectedIndexChanged(object sender, EventArgs e)
    {

        string query = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();




        if (txtRequestStatus.Text == "Pending List")
        {
            query = "Select Adjustment_No,Adjustmen_Date,AdjustmenBy from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And (Status = '0' or Status is null)";
            DT = objdata.RptEmployeeMultipleDetails(query);

            Repeater_StdPurOrder.DataSource = DT;
            Repeater_StdPurOrder.DataBind();
            Repeater_StdPurOrder.Visible = true;
        }
        else if (txtRequestStatus.Text == "MD Pending List")
        {
            query = "Select Adjustment_No,Adjustmen_Date,AdjustmenBy from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '3'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            Repeater_StdPurOrder.DataSource = DT;
            Repeater_StdPurOrder.DataBind();
            Repeater_StdPurOrder.Visible = true;
        }
        else if (txtRequestStatus.Text == "Approved List")
        {
            query = "Select Top 30 Adjustment_No,Adjustmen_Date,AdjustmenBy from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '1' ORDER BY Adjustmen_Date Desc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_StdPurOrder.DataSource = DT;
            Repeater_StdPurOrder.DataBind();
            Repeater_StdPurOrder.Visible = true;
        }
        else if (txtRequestStatus.Text == "Rejected List")
        {
            query = "Select Top 30 Adjustment_No,Adjustmen_Date,AdjustmenBy from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '2' ORDER BY Adjustmen_Date Desc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_StdPurOrder.DataSource = DT;
            Repeater_StdPurOrder.DataBind();
            Repeater_StdPurOrder.Visible = true;
        }
   
    }


}
