﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Inventory_gate_pass_in : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Gate Pass In";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Inventory"));
            li.Attributes.Add("class", "droplink active open");

            Load_Data_GST();
            Load_TaxData();


            Initial_Data_Referesh_Material_Request_Item();
            Load_Data_Empty_Dept();
            Load_Data_Empty_CostCenterName();
            Load_Data_Empty_WareHouse();
            Load_Data_Empty_ZoneName();
            Load_Data_Empty_BinName();
            Load_Data_Empty_SuppName();

            //Load_Data_Empty_GatePassOut();

            if (Session["GP_IN_No"] == null)
            {
                SessionGatePassINNo = "";
            }
            else
            {
                SessionGatePassINNo = Session["GP_IN_No"].ToString();
                txtGPINNo.Text = SessionGatePassINNo;
                btnSearch_Click(sender, e);
            }
          txtBal.Attributes.Add("readonly", "readonly");
        }
        Load_OLD_data();
       
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;
        string FreeService = "";


        if (chkFreeServ.Checked == true)
        {
            FreeService = "1";
        }
        else
        {
            FreeService = "0";
        }


        if (RdpStkType.SelectedValue == "1")
        {
            if (ddlGPOutNo.SelectedItem.Text == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Gate Pass Out No.');", true);
            }

            if (txtWarehouseName.SelectedItem.Text == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Warwhouse.');", true);
            }

            if (txtCostCenterName.SelectedItem.Text == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Cost Center.');", true);
            }


            //Check Gate Pass Out No
            if (ddlGPOutNo.SelectedItem.Text != "-Select-")
            {
                query = "Select * from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_Out_No='" + ddlGPOutNo.SelectedItem.Text + "'";
                qry_dt = objdata.RptEmployeeMultipleDetails(query);
                if (qry_dt.Rows.Count == 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Gate Pass Out No...');", true);
                }
            }

            //Check Supplier Name
            if (ddlSupplier.SelectedItem.Text.ToString() != "-Select-")
            {
                query = "Select * from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
                query = query + " And GP_Out_No='" + ddlGPOutNo.SelectedItem.Text + "' And Supp_Code='" + ddlSupplier.SelectedValue + "' And Supp_Name='" + ddlSupplier.SelectedItem.Text + "'";
                qry_dt = objdata.RptEmployeeMultipleDetails(query);
                if (qry_dt.Rows.Count == 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Supplier Name...');", true);
                }
            }

            //Check Warehouse Name
            query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseName.SelectedValue + "' And WarehouseName='" + txtWarehouseName.SelectedItem.Text + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Warehouse Name...');", true);
            }
        }

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }



        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Gate Pass IN");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Gate Pass IN Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Gate Pass IN");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Gate Pass IN..');", true);
            }
        }
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Gate Pass IN", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtGPINNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            query = "Select * from GatePass_IN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_IN_No='" + txtGPINNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from GatePass_IN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_IN_No='" + txtGPINNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from GatePass_IN_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_IN_No='" + txtGPINNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into GatePass_IN_Main(Ccode,Lcode,FinYearCode,FinYearVal,GP_IN_No,GP_IN_Date,GP_Out_No,GP_Out_Date,Supp_Code,Supp_Name,";
            query = query + "Supp_Det,Others,DeptCode,DeptName,CostCenterCode,CostCenterName,WarehouseCode,WarehouseName,StockType,UserID,UserName,";
            query = query + "SubTotal,OtherAmt,PackingAmt,NetTotal,ServiceType,InvNo,InvDate) Values('" + SessionCcode + "',";
            query = query + "'" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtGPINNo.Text + "','" + txtDate.Text + "',";
            query = query + "'" + ddlGPOutNo.SelectedItem.Text + "','" + txtGPOutDate.Text + "','" + ddlSupplier.SelectedValue + "','" + ddlSupplier.SelectedItem.Text + "',";
            query = query + "'" + txtSupplierDet.Text + "','" + txtOthers.Text + "','" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "',";
            query = query + "'" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "','" + txtWarehouseName.SelectedValue + "',";
            query = query + "'" + txtWarehouseName.SelectedItem.Text + "','" + RdpStkType.SelectedValue + "','" + SessionUserID + "','" + SessionUserName + "',";
            query = query + "'" + txtSubTotAmt.Text + "','" + txtOtherAmt.Text + "','" + txtPckingGST.Text + "','" + txtNetAmt.Text + "','" + FreeService + "',";
            query = query + "'" + txtInvNo.Text + "','" + txtInvDate.Text + "')";
            objdata.RptEmployeeMultipleDetails(query);

            //GatePass_IN_Main_Sub

            string Tot_Qty = "0";
            string Temp_Bag = "0";
            string Temp_Qty = "";
            string Balance_Bag = "0";
            string Balance_Qty = "0";

            string Customer_Bg = "", Customer_Qty = "", DC_Bag = "", DC_Qty = "";
            DataTable dt_Temp = new DataTable();
            DataTable dt = new DataTable();
            DataTable dt_Cust = new DataTable();
            DataTable dt_DC = new DataTable();

            //DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into GatePass_IN_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,GP_IN_No,GP_IN_Date,GP_Out_No,GP_Out_Date,ItemCode,ItemName,UOMCode,OutQty,INQty,ScrabQty,";
                query = query + "Rate,Value,DiscPer,DiscAmt,PackingAmt,GSTType,CGSTPer,CGSTAmt,SGSTPer,SGSTAmt,IGSTPer,IGSTAmt,TotAmt,ZoneName,BinName,Remarks,UserID,UserName)";
                query = query + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtGPINNo.Text + "',";
                query = query + "'" + txtDate.Text + "','" + ddlGPOutNo.SelectedItem.Text + "','" + txtGPOutDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["ItemName"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["OutQty"].ToString() + "','" + dt.Rows[i]["INQty"].ToString() + "','" + dt.Rows[i]["ScrabQty"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["Rate"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "','" + dt.Rows[i]["DiscPer"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["DiscAmt"].ToString() + "','" + dt.Rows[i]["PackingAmt"].ToString() + "','" + dt.Rows[i]["GSTType"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["CGSTPer"].ToString() + "','" + dt.Rows[i]["CGSTAmt"].ToString() + "','" + dt.Rows[i]["SGSTPer"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["SGSTAmt"].ToString() + "','" + dt.Rows[i]["IGSTPer"].ToString() + "','" + dt.Rows[i]["IGSTAmt"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["TotAmt"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["Remarks"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);



                DataTable dt_Check = new DataTable();

                //Check data already available or not
                query = "select ISNULL(CusQty , '0') as CusQty,ISNULL(DCQty , '0') as DCQty,ISNULL(QtyBalance , '0') as QtyBalance from DC_Temp where ";
                query = query + " InvoiceNo ='" + ddlGPOutNo.SelectedItem.Text + "' and ItemName='" + dt.Rows[i]["ItemName"].ToString() + "' and ";
                query = query + " Ccode ='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FinYearCode='" + SessionFinYearCode + "'";

                dt_Check = objdata.RptEmployeeMultipleDetails(query);
                if (dt_Check.Rows.Count > 0)
                {

                    //Update Temp

                    //Temp_Bag = dt_Check.Rows[0]["BagBalance"].ToString();
                    //Temp_Qty = dt_Check.Rows[0]["QtyBalance"].ToString();

                    DataTable DT_POInv = new DataTable();
                    DataTable DT_Inv = new DataTable();

                    query = "select * from GatePass_IN_Main_Sub where GP_Out_No='" + ddlGPOutNo.SelectedItem.Text + "' and ItemName='" + dt.Rows[i]["ItemName"].ToString() + "'";
                    query = query + " and FinyearCode='" + SessionFinYearCode + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    DT_POInv = objdata.RptEmployeeMultipleDetails(query);

                    string Cust_Bag = "0";
                    string Cust_Qty = "0";

                    Temp_Qty = DT_POInv.Rows[0]["InQty"].ToString();

                    query = "select sum(CAST(DS.InQty as decimal(18,2))) as Qty  from GatePass_IN_Main DM Inner join GatePass_IN_Main_Sub DS on DM.GP_IN_No=DS.GP_IN_No ";
                    query = query + "where DM.GP_Out_No='" + ddlGPOutNo.SelectedItem.Text + "' And DS.ItemName='" + dt.Rows[i]["ItemName"].ToString() + "'";

                    DT_Inv = objdata.RptEmployeeMultipleDetails(query);


                    //Cust_Bag = DT_Inv.Rows[0]["Bag"].ToString();
                    Cust_Qty = DT_Inv.Rows[0]["Qty"].ToString();


                    Balance_Bag = (Convert.ToDecimal(Temp_Bag) - Convert.ToDecimal(Cust_Bag)).ToString();
                    Balance_Qty = (Convert.ToDecimal(Temp_Qty) - Convert.ToDecimal(Cust_Qty)).ToString();

                    query = " Update DC_Temp set ";
                    query = query + " DCQty='" + dt.Rows[i]["InQty"].ToString() + "', QtyBalance='" + Balance_Qty + "' where InvoiceNo='" + ddlGPOutNo.SelectedItem.Text + "' ";
                    query = query + " and ItemName='" + dt.Rows[i]["ItemName"].ToString() + "'";

                    objdata.RptEmployeeMultipleDetails(query);


                    query = "select sum(CAST(CusQty as decimal(18,2))) as CusQty from DC_Temp ";
                    query = query + " where InvoiceNo='" + ddlGPOutNo.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";

                    dt_Cust = objdata.RptEmployeeMultipleDetails(query);

                    if (dt_Cust.Rows.Count > 0)
                    {
                        //Customer_Bg = dt_Cust.Rows[0]["CusNoofBag"].ToString();
                        Customer_Qty = dt_Cust.Rows[0]["CusQty"].ToString();
                    }

                    query = "Select sum(CAST(GIS.INQty as decimal(18,2))) as Qty from GatePass_IN_Main GIM ";
                    query = query + " inner join GatePass_IN_Main_Sub GIS on GIS.GP_IN_No=GIM.GP_IN_No and GIM.Ccode=GIS.Ccode and GIM.Lcode=GIS.Lcode";
                    query = query + " where GIM.Ccode='" + SessionCcode + "' and GIM.Lcode='" + SessionLcode + "' and GIM.GP_Out_No='" + ddlGPOutNo.SelectedItem.Text + "'";
                    dt_DC = objdata.RptEmployeeMultipleDetails(query);

                    if (dt_DC.Rows.Count > 0)
                    {
                        //DC_Bag = dt_DC.Rows[0]["Bag"].ToString();
                        DC_Qty = dt_DC.Rows[0]["Qty"].ToString();
                    }

                    if ((Convert.ToDecimal(Customer_Qty) == Convert.ToDecimal(DC_Qty)))
                    {
                        query = "Update GatePass_Out_Main set OpStatus='1' where GP_Out_No='" + ddlGPOutNo.SelectedItem.Text + "' and ";
                        query = query + "Ccode ='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                    }

                }
                else
                {

                }
            }



            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GatePass IN Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GatePass IN Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["GP_IN_No"] = txtGPINNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        RdpStkType.SelectedValue = "1";

        txtGPINNo.Text = ""; txtDate.Text = ""; ddlGPOutNo.SelectedValue= null; txtGPOutDate.Text = ""; ddlSupplier.SelectedValue = ""; ddlSupplier.SelectedItem.Text = ""; txtSupplierDet.Text = "";
        txtOthers.Text = ""; txtWarehouseName.SelectedValue = "-Select-";
        txtDepartmentName.SelectedValue = "-Select-"; txtCostCenterName.SelectedValue = "-Select-";
        txtItemCodeHide.Value = ""; ddlItemName.SelectedValue = null; txtItemUOMHide.Value = ""; txtOutQty.Text = ""; txtINQty.Text = "";
        txtValue.Text = ""; txtZoneName.SelectedValue = null; txtBinName.SelectedValue = null; txtRemarks.Text = ""; txtBal.Text = "";

        btnSave.Text = "Save";
        Initial_Data_Referesh_Material_Request_Item();
        Session.Remove("GP_IN_No");
        //Load_Data_Enquiry_Grid();

        Load_Data_Empty_GatePassOut();
        Load_Data_Empty_WareHouse();
        Load_Data_Empty_ItemCode();
        Load_Data_Empty_ZoneName();
        Load_Data_Empty_BinName();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";
        decimal InQty = Convert.ToDecimal(txtINQty.Text);
        string BalQty = Request.Form[txtBal.UniqueID];

        if (RdpStkType.SelectedValue == "1")
        {
            if (txtBinName.SelectedItem.Text == "-Select-")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the BinName...');", true);
            }
        }

        if (txtOutQty.Text == "0.0" || txtOutQty.Text == "0" || txtOutQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Out Qty...');", true);
        }

        if (chkFreeServ.Checked != true)
        {
            if (txtINQty.Text == "0.0" || txtINQty.Text == "0" || txtINQty.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the IN Qty...');", true);
            }
        }

        if (chkFreeServ.Checked != true)
        {
            if (txtValue.Text == "0.0" || txtValue.Text == "0" || txtValue.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Value...');", true);
            }
        }
      
        if (RdpStkType.SelectedValue == "1")
        {
            if (BalQty == "0.0" || BalQty == "0")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Balance Qty is Zero Check the Item...');", true);
            }

            //check with Zone Name And Bin Name
            query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseName.SelectedValue + "' And BinName='" + txtBinName.SelectedItem.Text + "'";
            //And ZoneName='" + txtZoneName.SelectedItem.Text + "' And BinName='" + txtBinName.SelectedItem.Text + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Bin Details..');", true);
            }


            if (InQty > Convert.ToDecimal(BalQty))
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check In Qty.. In Qty less or equal to Balance Qty.');", true);
            }

            //check with Item Code And Item Name
            query = "Select * from GatePass_Out_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_Out_No='" + ddlGPOutNo.SelectedItem.Text + "' And ItemCode='" + ddlItemName.SelectedValue + "' And ItemName='" + ddlItemName.SelectedItem.Text + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
            }
        }

        if (!ErrFlag)
        {
            //UOM Code get
            //string UOM_Code_Str = txtItemUOMHide.Value.ToString();

            string UOM_Code_Str = txtUom.Text.Replace("'","");

            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('this Item Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    if (RdpStkType.SelectedValue == "2")
                    {
                        dr["ItemCode"] = ddlItemName.SelectedItem.Text;
                    }
                    else
                    {
                        dr["ItemCode"] = txtItemCodeHide.Value;
                    }
                    dr["ItemName"] = ddlItemName.SelectedItem.Text;
                    dr["UOMCode"] = UOM_Code_Str;
                    dr["OutQty"] = txtOutQty.Text;
                    dr["INQty"] = txtINQty.Text;
                    dr["ScrabQty"] = txtScarbQty.Text;
                    dr["Rate"] = txtRate.Text;
                    dr["Value"] = txtValue.Text;
                    dr["DiscPer"] = txtDiscPer.Text;
                    dr["DiscAmt"] = txtDiscAmt.Text;
                    dr["PackingAmt"] = txtPackAmt.Text;
                    dr["GSTType"] = ddlGstType.SelectedItem.Text;
                    dr["CGSTPer"] = txtCGSTPer.Text;
                    dr["CGSTAmt"] = txtCGSTAmt.Text;
                    dr["SGSTPer"] = txtSGSTPer.Text;
                    dr["SGSTAmt"] = txtSGSTAmt.Text;
                    dr["IGSTPer"] = txtIGSTPer.Text;
                    dr["IGSTAmt"] = txtIGSTAmt.Text;
                    dr["TotAmt"] = txtTotAmt.Text;
                    dr["ZoneName"] = txtBinName.SelectedItem.Text;
                    dr["BinName"] = txtBinName.SelectedItem.Text;
                    dr["Remarks"] = txtRemarks.Text;
                    dt.Rows.Add(dr);

                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    txtItemCodeHide.Value = ""; ddlItemName.SelectedItem.Text = "-Select-"; txtItemUOMHide.Value = ""; txtOutQty.Text = ""; txtINQty.Text = "";
                    txtValue.Text = ""; txtZoneName.SelectedValue = null; txtBinName.SelectedValue = null; txtRemarks.Text = ""; txtBal.Text = "";
                    txtRate.Text = "";txtDiscPer.Text = "0.0"; txtDiscAmt.Text = "0.0"; txtPackAmt.Text = "0.0";ddlGstType.SelectedValue = null;
                    txtCGSTPer.Text = "0";txtCGSTAmt.Text = "0.0";txtSGSTPer.Text = "0";txtSGSTAmt.Text = "0.0";txtIGSTPer.Text = "0.0";
                    txtIGSTAmt.Text = "0.0";txtTotAmt.Text = "0.0";
                    txtScarbQty.Text = "0.0";
                }
            }
            else
            {
                dr = dt.NewRow();
                if (RdpStkType.SelectedValue == "1")
                {
                    dr["ItemCode"] = ddlItemName.SelectedItem.Text;
                }
                else
                {
                    dr["ItemCode"] = txtItemCodeHide.Value;
                } 
                dr["ItemName"] = ddlItemName.SelectedItem.Text;
                dr["UOMCode"] = UOM_Code_Str;
                dr["OutQty"] = txtOutQty.Text;
                dr["INQty"] = txtINQty.Text;
                dr["ScrabQty"] = txtScarbQty.Text;
                dr["Value"] = txtValue.Text;
                dr["ZoneName"] = txtBinName.SelectedItem.Text;
                dr["BinName"] = txtBinName.SelectedItem.Text;
                dr["Remarks"] = txtRemarks.Text;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                txtItemCodeHide.Value = ""; ddlItemName.SelectedItem.Text = "-Select-"; txtItemUOMHide.Value = ""; txtOutQty.Text = ""; txtINQty.Text = "";
                txtValue.Text = ""; txtZoneName.SelectedValue = null; txtBinName.SelectedValue = null; txtRemarks.Text = ""; txtBal.Text = "";
               
            }
        }
        txtSubTotAmt.Text = "0";
        DataTable dtTot = (DataTable)ViewState["ItemTable"];
        double dSubTotal = 0.0;
        for (int i=0;i<dtTot.Rows.Count;i++)
        {
            dSubTotal = dSubTotal + Convert.ToDouble(dtTot.Rows[i]["TotAmt"].ToString());
        }

        txtSubTotAmt.Text = dSubTotal.ToString();
        
        txtNetAmt.Text = txtSubTotAmt.Text;
    }

    private void Initial_Data_Referesh_Material_Request_Item()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("OutQty", typeof(string)));
        dt.Columns.Add(new DataColumn("INQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ScrabQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("Value", typeof(string)));

        dt.Columns.Add(new DataColumn("DiscPer", typeof(string)));
        dt.Columns.Add(new DataColumn("DiscAmt", typeof(string)));

        dt.Columns.Add(new DataColumn("PackingAmt", typeof(string)));
        dt.Columns.Add(new DataColumn("GSTType", typeof(string)));

        dt.Columns.Add(new DataColumn("CGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTAmt", typeof(string)));

        dt.Columns.Add(new DataColumn("SGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTAmt", typeof(string)));

        dt.Columns.Add(new DataColumn("IGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTAmt", typeof(string)));

        dt.Columns.Add(new DataColumn("TotAmt", typeof(string)));

        dt.Columns.Add(new DataColumn("ZoneName", typeof(string)));
        dt.Columns.Add(new DataColumn("BinName", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSourceS;
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from GatePass_IN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_IN_No='" + txtGPINNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["GP_IN_Date"].ToString();

            RdpStkType.SelectedValue = Main_DT.Rows[0]["StockType"].ToString();


            ddlGPOutNo.DataSource = Main_DT;
            ddlGPOutNo.DataTextField = "GP_Out_No";
            ddlGPOutNo.DataBind();

            ddlGPOutNo.SelectedItem.Text = Main_DT.Rows[0]["GP_Out_No"].ToString();
            txtGPOutDate.Text = Main_DT.Rows[0]["GP_Out_Date"].ToString();
            ddlSupplier.SelectedValue = Main_DT.Rows[0]["Supp_Code"].ToString();
            ddlSupplier.SelectedItem.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtSupplierDet.Text = Main_DT.Rows[0]["Supp_Det"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();
            txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtCostCenterName.SelectedValue = Main_DT.Rows[0]["CostCenterCode"].ToString();
            txtCostCenterName.SelectedItem.Text = Main_DT.Rows[0]["CostCenterName"].ToString();
            txtWarehouseName.SelectedValue = Main_DT.Rows[0]["WarehouseCode"].ToString();
            txtWarehouseName.SelectedItem.Text = Main_DT.Rows[0]["WarehouseName"].ToString();


            txtSubTotAmt.Text = Main_DT.Rows[0]["SubTotal"].ToString();
            txtNetAmt.Text = Main_DT.Rows[0]["NetTotal"].ToString();
            txtPckingGST.Text = Main_DT.Rows[0]["PackingAmt"].ToString();
            txtOtherAmt.Text = Main_DT.Rows[0]["OtherAmt"].ToString();

            //GatePass_IN_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select * from GatePass_IN_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_IN_No='" + txtGPINNo.Text + "'";
            //query = "Select ItemCode,ItemName,UOMCode,OutQty,INQty,Value,ZoneName,BinName,Remarks from GatePass_IN_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_IN_No='" + txtGPINNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("gate_pass_in_main.aspx");
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Gate Pass IN");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Gate Pass IN...');", true);
        //}
        //User Rights Check End

        query = "Select * from GatePass_IN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_IN_No='" + txtGPINNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Gate Pass IN Details..');", true);
        }

        query = "Select * from GatePass_IN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_IN_No='" + txtGPINNo.Text + "' And GP_IN_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Gate Pass IN Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            query = "Update GatePass_IN_Main set GP_IN_Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And GP_IN_No='" + txtGPINNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            //GatePass Out Details 
            //query = "Update GatePass_Out_Main set Close_State='1' where Ccode='" + SessionCcode + "'";
            //query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            //query = query + "  And GP_Out_No='" + ddlGPOutNo.SelectedItem.Text + "'";
            //objdata.RptEmployeeMultipleDetails(query);

            if (RdpStkType.SelectedValue == "1")
            {
                Stock_Add();
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Gate Pass IN Details Approved Successfully..');", true);
        }
    }

    private void Stock_Add()
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();

        query = "Select * from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtGPINNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            query = "Delete from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtGPINNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }

        DateTime transDate = Convert.ToDateTime(txtDate.Text);

        //Insert Stock_Transaction_Ledger
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            query = "Insert Into Stock_Transaction_Ledger(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
            query = query + " Trans_Type,DeptCode,DeptName,CostCenterCode,CostCenterName,ItemCode,ItemName,Open_Qty,Open_Value,PO_Qty,PO_Value,General_Qty,General_Value,Issue_Qty,";
            query = query + " Issue_Value,Issue_Return_Qty,Issue_Return_Value,GP_IN_Qty,GP_IN_Value,GP_OUT_Qty,GP_OUT_Value,Transfer_Qty,Transfer_Value,Damage_Qty,Damage_Value,Spares_ReSales_Qty,Spares_ReSales_Value,WarehouseCode,WarehouseName,ZoneName,BinName,Supp_Code,";
            query = query + " Supp_Name,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
            query = query + " '" + SessionFinYearVal + "','" + txtGPINNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtDate.Text + "','GATE PASS IN','" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "','" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "',";
            query = query + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
            query = query + " '0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','" + dt.Rows[i]["INQty"].ToString() + "',";
            query = query + " '" + dt.Rows[i]["Value"].ToString() + "','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','" + txtWarehouseName.SelectedValue + "','" + txtWarehouseName.SelectedItem.Text + "','" + dt.Rows[i]["BinName"].ToString() + "',";
            query = query + " '" + dt.Rows[i]["BinName"].ToString() + "','" + ddlSupplier.SelectedValue + "','" + ddlSupplier.SelectedItem.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            CommonClass_Function.cls_Trans_Type = "GATE PASS IN";
            CommonClass_Function.cls_ItemCode = dt.Rows[i]["ItemCode"].ToString();
            CommonClass_Function.cls_ItemName = dt.Rows[i]["ItemName"].ToString();
            CommonClass_Function.cls_Add_Qty = dt.Rows[i]["INQty"].ToString();
            CommonClass_Function.cls_Add_Value = dt.Rows[i]["Value"].ToString();
            CommonClass_Function.cls_Minus_Qty = "0.0";
            CommonClass_Function.cls_Minus_Value = "0.0";

            CommonClass_Function.cls_DeptCode = txtDepartmentName.SelectedValue;
            CommonClass_Function.cls_DeptName = txtDepartmentName.SelectedItem.Text;
            CommonClass_Function.cls_CostCenterCode = txtCostCenterName.SelectedValue;
            CommonClass_Function.cls_CostCenterName = txtCostCenterName.SelectedItem.Text;
            CommonClass_Function.cls_WarehouseCode = txtWarehouseName.SelectedValue;
            CommonClass_Function.cls_WarehouseName = txtWarehouseName.SelectedItem.Text;
            CommonClass_Function.cls_ZoneName = dt.Rows[i]["BinName"].ToString();
            CommonClass_Function.cls_BinName = dt.Rows[i]["BinName"].ToString();
            CommonClass_Function.cls_Supp_Code = ddlSupplier.SelectedValue;
            CommonClass_Function.cls_Supp_Name = ddlSupplier.SelectedItem.Text;
            CommonClass_Function.cls_UserID = SessionUserID;
            CommonClass_Function.cls_UserName = SessionUserName;
            //CommonClass_Function.cls_Stock_Qty = "";
            //CommonClass_Function.cls_Stock_Value = "";

            CommonClass_Function.Stock_Ledger_And_Current_Stock_Add(SessionCcode, SessionLcode, SessionFinYearCode, SessionFinYearVal, txtGPINNo.Text, txtDate.Text);
        }
    }

    
    private void Load_Data_Empty_GatePassOut()
    {
        string query = "";

        bool error = false;

        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();
        DataTable DT3 = new DataTable();
      
        DataTable DT = new DataTable();

        DT.Columns.Add("GP_Out_No");
        DT.Columns.Add("GP_Out_Date");

        {
            query = "Select GP_Out_No,GP_Out_Date from GatePass_Out_Main  where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + " And Supp_Code='" + ddlSupplier.SelectedValue + "' And GP_Out_Status='1' And OpStatus='0'";
            //And Supp_Name = '" + ddlSupplier.SelectedItem.Text  + "'
            DT1 = objdata.RptEmployeeMultipleDetails(query);
            if (DT1.Rows.Count != 0)
            {
                int k = 0;
                for (int i = 0; i < DT1.Rows.Count; i++)
                {
                    error = false;
                    query = "select OutQty from GatePass_Out_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_Out_No='" + DT1.Rows[i]["GP_Out_No"].ToString() + "'";
                    DT2 = objdata.RptEmployeeMultipleDetails(query);

                    for (int j = 0; j < DT2.Rows.Count; j++)
                    {
                        query = "select sum(InQty) as ReceivedQty from GatePass_IN_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And  GP_Out_No='" + DT1.Rows[i]["GP_Out_No"].ToString() + "'";
                        DT3 = objdata.RptEmployeeMultipleDetails(query);

                        if (DT3.Rows[0]["ReceivedQty"].ToString() != "" && DT2.Rows[j]["OutQty"].ToString() != null)
                        {
                            query = "";
                            query = "select sum(OutQty) as outQty from GatePass_Out_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_Out_No='" + DT1.Rows[i]["GP_Out_No"].ToString() + "'";
                            DataTable dt_outQty = new DataTable();
                            dt_outQty = objdata.RptEmployeeMultipleDetails(query);
                            if (Convert.ToDecimal(DT3.Rows[0]["ReceivedQty"]) < Convert.ToDecimal(dt_outQty.Rows[0]["outQty"].ToString()))
                            {
                                error = true;
                            }
                        }
                        else
                        {
                            error = true;
                        }
                    }
                    if (error)
                    {
                        DT.Rows.Add();
                        DT.Rows[k]["GP_Out_No"] = DT1.Rows[i]["GP_Out_No"].ToString();
                        DT.Rows[k]["GP_Out_Date"] = DT1.Rows[i]["GP_Out_Date"].ToString();
                        k = k + 1;

                    }
                    //DT.Rows.Add();
                    //DT.Rows[k]["PO_Order_No"] = DT1.Rows[i]["Gen_PO_No"].ToString();
                    //DT.Rows[k]["PO_Order_Date"] = DT1.Rows[i]["Gen_PO_Date"].ToString();
                    //k = k + 1;
                }

            }
        }

        ddlGPOutNo.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["GP_Out_No"] = "-Select-";
        dr["GP_Out_Date"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlGPOutNo.DataTextField = "GP_Out_No";
        ddlGPOutNo.DataValueField = "GP_Out_Date";
        ddlGPOutNo.DataBind();
        
    }

    protected void GridViewClick_GatePassOut(object sender, CommandEventArgs e)
    {
        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string GP_Out_No = commandArgs[0];
        string Supp_Code = commandArgs[1];
        string Supp_Name = commandArgs[2];
        string Supp_Det = commandArgs[3];

        ddlGPOutNo.SelectedItem.Text = GP_Out_No;
        txtGPOutDate.Text = Convert.ToString(e.CommandName);
        ddlSupplier.SelectedValue = Supp_Code;
        ddlSupplier.SelectedItem.Text = Supp_Name;

        DataTable DT = new DataTable();
        string query = "Select *from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " and SuppCode='" + ddlSupplier.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtSupplierDet.Text = DT.Rows[0]["Address1"].ToString() + " " + DT.Rows[0]["Address2"].ToString() + " " + DT.Rows[0]["City"].ToString();
        }
        else
        {
            txtSupplierDet.Text = "";
        }

        //txtSupplierDet.Text = Supp_Det;

        Load_Data_Empty_ItemCode();

    }


    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        //modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();
    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtWarehouseName.Text = Convert.ToString(e.CommandName);


        Load_Data_Empty_ZoneName();
        Load_Data_Empty_BinName();
    }

    protected void btnZoneName_Click(object sender, EventArgs e)
    {
        //modalPop_ZoneName.Show();
    }

    private void Load_Data_Empty_ZoneName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtZoneName.Items.Clear();
        query = "Select ZoneName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And WarehouseCode='" + txtWarehouseName.SelectedValue + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtZoneName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["ZoneName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtZoneName.DataTextField = "ZoneName";

        txtZoneName.DataBind();
    }

    protected void GridViewClick_ZoneName(object sender, CommandEventArgs e)
    {
        txtZoneName.Text = Convert.ToString(e.CommandArgument);

        Load_Data_Empty_BinName();
    }

    protected void btnBinName_Click(object sender, EventArgs e)
    {
       // modalPop_BinName.Show();
    }

    private void Load_Data_Empty_SuppName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        ddlSupplier.Items.Clear();
        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        ddlSupplier.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["SuppCode"] = "-Select-";
        dr["SuppName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        ddlSupplier.DataTextField = "SuppName";
        ddlSupplier.DataValueField = "SuppCode";
        ddlSupplier.DataBind();
    }
    private void Load_Data_Empty_BinName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtBinName.Items.Clear();
        query = "Select BinName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And WarehouseCode='" + txtWarehouseName.SelectedValue + "'"; //And ZoneName='" + txtZoneName.SelectedItem.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtBinName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["BinName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtBinName.DataTextField = "BinName";

        txtBinName.DataBind();
    }

    protected void GridViewClick_BinName(object sender, CommandEventArgs e)
    {
        txtBinName.Text = Convert.ToString(e.CommandArgument);
    }

    private void Load_Data_Empty_ItemCode()
    {
       
       
        string query = "";
        
        DataTable DT = new DataTable();
        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");
        DT.Columns.Add("UOMCode");
        DT.Columns.Add("OutQty");
        DT.Columns.Add("Balance");

        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();

        query = "Select ItemCode,ItemName from GatePass_Out_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And GP_Out_No='" + ddlGPOutNo.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);



        //if (DT1.Rows.Count != 0)
        //{
        //    int k = 0;
        //    for (int i = 0; i < DT1.Rows.Count; i++)
        //    {
        //        query = "select Sum(INQty) as INQty from GatePass_IN_Main_Sub where ItemCode='" + DT1.Rows[i]["ItemCode"].ToString() + "' And GP_Out_No='" + ddlGPOutNo.SelectedItem.Text + "'";
        //        DT2 = objdata.RptEmployeeMultipleDetails(query);
        //        if (DT2.Rows[0]["INQty"].ToString() != "")
        //        {
        //            if (Convert.ToDecimal(DT2.Rows[0]["INQty"]) < Convert.ToDecimal(DT1.Rows[i]["OutQty"]))
        //            {
        //                bal = Convert.ToDecimal(DT1.Rows[i]["OutQty"]) - Convert.ToDecimal(DT2.Rows[0]["INQty"]);
        //            }
        //            else if (Convert.ToDecimal(DT2.Rows[0]["INQty"]) == Convert.ToDecimal(DT1.Rows[i]["OutQty"]))
        //            {
        //                bal = 0;
        //            }

        //        }
        //        else
        //        {
        //            bal = Convert.ToDecimal(DT1.Rows[i]["OutQty"]);
        //        }

        //        DT.Rows.Add();
        //        DT.Rows[i]["ItemCode"] = DT1.Rows[i]["ItemCode"].ToString();
        //        DT.Rows[i]["ItemName"] = DT1.Rows[i]["ItemName"].ToString();
        //        DT.Rows[i]["UOMCode"] = DT1.Rows[i]["UOMCode"].ToString();
        //        DT.Rows[i]["OutQty"] = DT1.Rows[i]["OutQty"].ToString();
        //        DT.Rows[i]["Balance"] = bal.ToString();
        //    }
        //}



        ddlItemName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataValueField = "ItemCode";

        ddlItemName.DataBind();
    }

    //protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    //{
    //    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
    //    string itemcode = commandArgs[0];
    //    string UOMCode = commandArgs[1];
    //    string OutQty = commandArgs[2];
    //    string Balance = commandArgs[3];

    //    txtItemCodeHide.Value = itemcode;
    //    txtItemName.Text = Convert.ToString(e.CommandName);
    //    txtItemUOMHide.Value = UOMCode;
    //    txtOutQty.Text = OutQty;
    //    txtBal.Text = Balance;

    //}

    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();
    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }
    protected void btnCostCenterName_Click(object sender, EventArgs e)
    {
        //modalPop_CostCenterName.Show();
    }
    private void Load_Data_Empty_CostCenterName()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtCostCenterName.Items.Clear();
        query = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCostCenterName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["CostcenterCode"] = "-Select-";
        dr["CostcenterName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCostCenterName.DataTextField = "CostcenterName";
        txtCostCenterName.DataValueField = "CostcenterCode";
        txtCostCenterName.DataBind();

    }

    protected void GridViewClick_CostCenterName(object sender, CommandEventArgs e)
    {
        //txtCostCenterCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtCostCenterName.Text = Convert.ToString(e.CommandName);
    }


    protected void txtWarehouseName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_ZoneName();
        Load_Data_Empty_BinName();
    }
    protected void txtZoneName_SelectedIndexChanged(object sender, EventArgs e)
    {

        Load_Data_Empty_BinName();
    }


    protected void txtRate_TextChanged(object sender, EventArgs e)
    {
        if (chkFreeServ.Checked != false)
        {
            if (txtINQty.Text != "")
            {
                txtValue.Text = (Convert.ToInt32(txtINQty.Text) * Convert.ToDouble(txtRate.Text)).ToString();
            }

            Total_Calculate();
        }
        else
        {
            Total_Calculate();
        }

    }

    protected void ddlGstType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TaxData();
    }

    private void Load_Data_GST()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        ddlGstType.Items.Clear();
        query = "Select *from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        ddlGstType.DataSource = Main_DT;
        //DataRow dr = Main_DT.NewRow();
        //dr["GST_Type"] = "-Select-";
        //dr["GST_Type"] = "-Select-";
        //Main_DT.Rows.InsertAt(dr, 0);
        ddlGstType.DataTextField = "GST_Type";
        ddlGstType.DataValueField = "GST_Type";
        ddlGstType.DataBind();

    }

    private void Total_Calculate()
    {
        string Qty_Val = "0";
        string Item_Rate = "0";
        string Item_Total = "0";
        string Discount_Percent = "0";
        string Discount_Amt = "0";
        string Tax_Per = "0";
        string Tax_Amt = "0";
        string CGST_Per = "0";
        string CGST_Amt = "0";
        string SGST_Per = "0";
        string SGST_Amt = "0";
        string IGST_Per = "0";
        string IGST_Amt = "0";
        string BDUTax_Per = "0";
        string BDUTax_Amt = "0";
        string Other_Charges = "0";
        string Final_Amount = "0";
        string PackingAmt = "0";
        if (txtINQty.Text != "") { Qty_Val = txtINQty.Text.ToString(); }
        if (txtRate.Text != "") { Item_Rate = txtRate.Text.ToString(); }


        Item_Total = (Convert.ToDecimal(Qty_Val) * Convert.ToDecimal(Item_Rate)).ToString();
        Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();
        if (Convert.ToDecimal(Item_Total) != 0)
        {
            if (Convert.ToDecimal(txtDiscAmt.Text.ToString()) != 0) { Discount_Amt = txtDiscAmt.Text.ToString(); }
            //if (Convert.ToDecimal(txtTax.Text.ToString()) != 0) { Tax_Per = txtTax.Text.ToString(); }
            //if (Convert.ToDecimal(txtBDUTax.Text.ToString()) != 0) { BDUTax_Per = txtBDUTax.Text.ToString(); }
            //if (Convert.ToDecimal(txtOtherCharge.Text.ToString()) != 0) { Other_Charges = txtOtherCharge.Text.ToString(); }
            if (Convert.ToDecimal(txtCGSTPer.Text.ToString()) != 0) { CGST_Per = txtCGSTPer.Text.ToString(); }
            if (Convert.ToDecimal(txtSGSTPer.Text.ToString()) != 0) { SGST_Per = txtSGSTPer.Text.ToString(); }
            if (Convert.ToDecimal(txtIGSTPer.Text.ToString()) != 0) { IGST_Per = txtIGSTPer.Text.ToString(); }

            if (Convert.ToDecimal(txtPackAmt.Text.ToString()) != 0) { PackingAmt = txtPackAmt.Text.ToString(); }


            //Discount Amt Calculate
            if (Convert.ToDecimal(Discount_Amt) != 0)
            {
                //Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                //Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                //Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

                Discount_Amt = txtDiscAmt.Text.ToString();

            }
            else
            {
                Discount_Amt = "0.00";
            }
            if (Convert.ToDecimal(BDUTax_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                BDUTax_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(BDUTax_Per)).ToString();
                BDUTax_Amt = (Convert.ToDecimal(BDUTax_Amt) / Convert.ToDecimal(100)).ToString();
                BDUTax_Amt = (Math.Round(Convert.ToDecimal(BDUTax_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                BDUTax_Amt = "0.00";
            }

            if (Convert.ToDecimal(CGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                CGST_Amt = "0.00";
            }

            if (Convert.ToDecimal(SGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                SGST_Amt = "0.00";
            }

            if (Convert.ToDecimal(IGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                IGST_Amt = "0.00";
            }


            //Tax Percentage Calculate
            if (Convert.ToDecimal(Tax_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                Item_Discount_Amt = (Convert.ToDecimal(Item_Discount_Amt) + Convert.ToDecimal(BDUTax_Amt)).ToString();
                Tax_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(Tax_Per)).ToString();
                Tax_Amt = (Convert.ToDecimal(Tax_Amt) / Convert.ToDecimal(100)).ToString();
                Tax_Amt = (Math.Round(Convert.ToDecimal(Tax_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                Tax_Amt = "0.00";
            }


            //Other Charges
            //if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

            //Final Amt
            Final_Amount = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
            //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
            Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
            Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 2, MidpointRounding.AwayFromZero)).ToString();

            txtValue.Text = Item_Total;
            txtDiscAmt.Text = Discount_Amt;
            //txtTaxAmt.Text = Tax_Amt;
            //txtBDUAmount.Text = BDUTax_Amt;
            txtCGSTAmt.Text = CGST_Amt;
            txtSGSTAmt.Text = SGST_Amt;
            txtIGSTAmt.Text = IGST_Amt;
            txtTotAmt.Text = Final_Amount;
            //txtNetAmt.Text = Final_Amount;

        }
    }

    private void Load_TaxData()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GST_Type='" + ddlGstType.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtCGSTPer.Text = DT.Rows[0]["CGST_Percent"].ToString();
            txtSGSTPer.Text = DT.Rows[0]["SGST_Percent"].ToString();
            txtIGSTPer.Text = DT.Rows[0]["IGST_Percent"].ToString();
        }
        else
        {
            txtCGSTPer.Text = "0";
            txtSGSTPer.Text = "0";
            txtIGSTPer.Text = "0";
        }

        Total_Calculate();
    }

    protected void txtDiscPer_TextChanged(object sender, EventArgs e)
    {
        //Total_Calculate();
    }

    protected void txtPackAmt_TextChanged(object sender, EventArgs e)
    {
        Total_Calculate();
    }

    protected void Load_Supplier()
    {
        string sQry = "";
        DataTable DT = new DataTable();
        sQry = "Select Supp_Det,GP_Out_Date,DeptCode,DeptName,CostCenterCode,CostCenterName,WarehouseCode,WarehouseName from GatePass_Out_Main where ";
        sQry=sQry + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_Out_No ='" + ddlGPOutNo.SelectedItem.Text + "'";// and Supp_Code='" + ddlGPOutNo.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(sQry);

        //ddlSupplier.SelectedValue = DT.Rows[0]["Supp_Code"].ToString();
        //ddlSupplier.SelectedItem.Text = DT.Rows[0]["Supp_Name"].ToString();
        txtSupplierDet.Text = DT.Rows[0]["Supp_Det"].ToString();
        txtGPOutDate.Text = DT.Rows[0]["GP_Out_Date"].ToString();

        txtDepartmentName.SelectedValue= DT.Rows[0]["DeptCode"].ToString();
        txtDepartmentName.SelectedItem.Text = DT.Rows[0]["DeptName"].ToString();

        txtCostCenterName.SelectedValue = DT.Rows[0]["CostCenterCode"].ToString();
        txtCostCenterName.SelectedItem.Text = DT.Rows[0]["CostCenterName"].ToString();

        txtWarehouseName.SelectedValue = DT.Rows[0]["WarehouseCode"].ToString();
        txtWarehouseName.SelectedItem.Text = DT.Rows[0]["WarehouseName"].ToString();

        Load_Data_Empty_BinName();
    }

    protected void ddlGPOutNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Supplier();
        Load_Data_Empty_ItemCode();

    }

    protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();

        decimal bal = 0;
        string sQry = "";

        if (chkFreeServ.Checked == false)
        {

            sQry = "Select UOMCode,OutQty,Value from GatePass_Out_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
            sQry = sQry + "And ItemName='" + ddlItemName.SelectedItem.Text + "' And GP_Out_No='" + ddlGPOutNo.SelectedItem.Text + "'";
            DT1 = objdata.RptEmployeeMultipleDetails(sQry);


            if (DT1.Rows.Count != 0)
            {
                int k = 0;
                for (int i = 0; i < DT1.Rows.Count; i++)
                {
                    sQry = "select Sum(INQty) as INQty from GatePass_IN_Main_Sub where ItemCode='" + ddlItemName.SelectedValue + "' And GP_Out_No='" + ddlGPOutNo.SelectedItem.Text + "'";
                    DT2 = objdata.RptEmployeeMultipleDetails(sQry);
                    if (DT2.Rows[0]["INQty"].ToString() != "")
                    {
                        if (Convert.ToDecimal(DT2.Rows[0]["INQty"]) < Convert.ToDecimal(DT1.Rows[i]["OutQty"]))
                        {
                            bal = Convert.ToDecimal(DT1.Rows[i]["OutQty"]) - Convert.ToDecimal(DT2.Rows[0]["INQty"]);
                        }
                        else if (Convert.ToDecimal(DT2.Rows[0]["INQty"]) == Convert.ToDecimal(DT1.Rows[i]["OutQty"]))
                        {
                            bal = 0;
                        }

                    }
                    else
                    {
                        bal = Convert.ToDecimal(DT1.Rows[i]["OutQty"]);
                    }

                    DT.Rows.Add();
                    txtItemCodeHide.Value = ddlItemName.SelectedValue;
                    txtItemUOMHide.Value = DT1.Rows[i]["UOMCode"].ToString();
                    txtOutQty.Text = DT1.Rows[i]["OutQty"].ToString();
                    txtBal.Text = bal.ToString();
                    //txtRate.Text = DT1.Rows[i]["Value"].ToString();

                    txtUom.Text = txtItemUOMHide.Value;

                    txtINQty.Text = "0.0";
                    txtScarbQty.Text = "0.0";
                    txtRate.Text = "0.0";
                    txtValue.Text = "0.0";
                    
                }
            }
        }
        else
        {
            sQry = "Select UOMCode,OutQty from GatePass_Out_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
            sQry = sQry + "And ItemName='" + ddlItemName.SelectedItem.Text + "' And GP_Out_No='" + ddlGPOutNo.SelectedItem.Text + "'";
            DT1 = objdata.RptEmployeeMultipleDetails(sQry);

            if (DT1.Rows.Count != 0)
            {
                int k = 0;
                for (int i = 0; i < DT1.Rows.Count; i++)
                {
                    sQry = "select Sum(INQty) as INQty from GatePass_IN_Main_Sub where ItemCode='" + ddlItemName.SelectedValue + "' And GP_Out_No='" + ddlGPOutNo.SelectedItem.Text + "'";
                    DT2 = objdata.RptEmployeeMultipleDetails(sQry);
                    if (DT2.Rows[0]["INQty"].ToString() != "")
                    {
                        if (Convert.ToDecimal(DT2.Rows[0]["INQty"]) < Convert.ToDecimal(DT1.Rows[i]["OutQty"]))
                        {
                            bal = Convert.ToDecimal(DT1.Rows[i]["OutQty"]) - Convert.ToDecimal(DT2.Rows[0]["INQty"]);
                        }
                        else if (Convert.ToDecimal(DT2.Rows[0]["INQty"]) == Convert.ToDecimal(DT1.Rows[i]["OutQty"]))
                        {
                            bal = 0;
                        }

                    }
                    else
                    {
                        bal = Convert.ToDecimal(DT1.Rows[i]["OutQty"]);
                    }

                    DT.Rows.Add();
                    //txtItemCodeHide.Value = ddlItemName.SelectedValue;
                    //txtItemUOMHide.Value = DT1.Rows[i]["UOMCode"].ToString();
                    //txtOutQty.Text = DT1.Rows[i]["OutQty"].ToString();
                    txtBal.Text = bal.ToString();
                    //txtRate.Text = DT1.Rows[i]["Value"].ToString();
                }
            }

            txtItemCodeHide.Value = ddlItemName.SelectedValue;
            txtItemUOMHide.Value = DT1.Rows[0]["UOMCode"].ToString();
            txtOutQty.Text = DT1.Rows[0]["OutQty"].ToString();

            txtINQty.Text = "0.0";
            txtScarbQty.Text = "0.0";
            txtRate.Text = "0.0";
            txtValue.Text = "0.0";
        }

    }

    protected void Final_Calculate()
    {
        string PackGstAmt = "0";
        string otherAmt = "0";
        double NetAmt = 0.0;
        if (txtPckingGST.Text != "0") { PackGstAmt = txtPckingGST.Text; }
        if (txtOtherAmt.Text != "0") { otherAmt = txtOtherAmt.Text; }

        if (Convert.ToDecimal(PackGstAmt) != 0)
        {
            PackGstAmt = txtPckingGST.Text;
        }
        else
        {
            PackGstAmt = "0.0";
        }

        if (Convert.ToDecimal(otherAmt) != 0)
        {
            otherAmt = txtOtherAmt.Text;
        }
        else
        {
            otherAmt = "0.0";
        }

        NetAmt = Convert.ToDouble(PackGstAmt) + (Convert.ToDouble(otherAmt) + Convert.ToDouble(txtSubTotAmt.Text));

        txtNetAmt.Text = NetAmt.ToString();
    }

    protected void txtDiscAmt_TextChanged(object sender, EventArgs e)
    {
        Total_Calculate();
    }

    protected void txtPckingGST_TextChanged(object sender, EventArgs e)
    {
        Final_Calculate();
    }

    protected void txtOtherAmt_TextChanged(object sender, EventArgs e)
    {
        Final_Calculate();
    }

    protected void txtINQty_TextChanged(object sender, EventArgs e)
    {
        Total_Calculate();
    }

    protected void ddlSupplier_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_GatePassOut();
    }
}
