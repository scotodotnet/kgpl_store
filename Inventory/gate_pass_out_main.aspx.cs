﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Inventory_gate_pass_out_main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    bool Errflag = false;
    string RptName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Gate Pass Out";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Inventory"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Load_Data_Enquiry_Grid();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Gate Pass OUT");
        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Gate Pass OUT..');", true);
        }
        else
        {
            Session.Remove("GP_Out_No");
            Response.Redirect("gate_pass_out.aspx");
        }
    }

    protected void GridPrintClick(object sender, CommandEventArgs e)
    {
        string PartyName = "-Select-";
        string ButtonName = "GP OUT SLIP";
        string WareHouse = "-Select-";
        string DeptName = "-Select-";
        string GPOutNo = e.CommandName.ToString();
        string GPOutIssBy = "-Select-";
        string ItemName = "-Select-";
        string FromDate = "";
        string ToDate = "";


        RptName = "GatePass OUT Details Report";

        //RptName = "Gate Pass Out Details";
        //ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?RptName=" + RptName + "&InvoiceNo=" + e.CommandName.ToString(), "_blank", "");
        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?PartyName=" + PartyName + "&WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&GPOutNo=" + GPOutNo + "&GPOutIssBy=" + GPOutIssBy + "&ItemName=" + ItemName + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");

    }
    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {

        string SSQL = "";
        bool ErrFlag = false;

        DataTable dtGatePassOut = new DataTable();

        //GatePass_IN_Main
        SSQL = "select GP_Out_Status from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + "And GP_Out_No='" + e.CommandName.ToString() + "'";

        dtGatePassOut = objdata.RptEmployeeMultipleDetails(SSQL);
        string status = dtGatePassOut.Rows[0]["GP_Out_Status"].ToString();

        if (status == "" || status == "0")
        {
            string Enquiry_No_Str = e.CommandName.ToString();
            Session.Remove("GP_Out_No");
            Session["GP_Out_No"] = Enquiry_No_Str;
            Response.Redirect("gate_pass_out.aspx");
        }
        else if (status == "2")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Gate Pass Out Already Rejected..');", true);
        }
        else if (status == "3")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Gate Pass Out put in pending..');", true);
        }
        else if (status == "1")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approved Gate Pass Out Cant Edit..');", true);
        }

       

    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Gate Pass OUT");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New Gate Pass OUT..');", true);
        }
        //User Rights Check End

        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        query = "Select * from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_Out_No='" + e.CommandName.ToString() + "' And GP_Out_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Delete Gate Pass OUT Details Already Approved..');", true);
        }
        //Check With Already Approved End

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_Out_No='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //Delete Main Table
                query = "Delete from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_Out_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                //Delete Main Sub Table
                query = "Delete from GatePass_Out_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And GP_Out_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GatePass Out Details Deleted Successfully');", true);
                Load_Data_Enquiry_Grid();
            }
        }
    }

    private void Load_Data_Enquiry_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select GP_Out_No,GP_Out_Date,Supp_Name,Takenby,Issuedby,Preferedby,WarehouseName ";
        query = query + "From GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ";
        query = query + "FinYearCode = '" + SessionFinYearCode + "' Order By CONVERT(DATETIME,GP_Out_Date, 103) Desc ";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }
}
