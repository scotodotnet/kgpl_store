﻿<%@ Page Language="C#"  MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="NonStock_Scrab.aspx.cs" Inherits="NonStock_Scrab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>


<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>

<!--Ajax popup End-->




<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Non Stock Scrab</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Non Stock Scrab</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Transaction No<span class="mandatory">*</span></label>
					              <asp:Label ID="txtIssueNo" runat="server" class="form-control"></asp:Label>
					         
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Date<span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					      
                                
					        <div class="form-group col-md-4">
					        <label for="exampleInputName">Item Name<span class="mandatory">*</span></label>
					         <asp:TextBox ID="txtitemname" class="form-control" runat="server"></asp:TextBox>
					      
					        
					        </div>
					           <div class="form-group col-md-4">
					            <label for="exampleInputName">Quantity</label>
					             <asp:TextBox ID="txtquantity" class="form-control" runat="server"></asp:TextBox>
					     </div>
					      
					          
					    </div>
					</div>
					
					<div class="col-md-12">
					    <div class="row">
					 
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Rate<span class="mandatory">*</span></label>
					             <asp:TextBox ID="txtrate" class="form-control" runat="server"></asp:TextBox>
					          
					           
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Description</label>
					             <asp:TextBox ID="txtdescription" class="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
                        
					        </div>
					       
					    </div>
					</div>
				
					
					
					
					
				
					<div class="clearfix"></div>				    
				
				  
					      
					    
					   
						
					
					
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                        <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                       
                        <asp:LinkButton ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                        <asp:LinkButton ID="btnBackEnquiry"  class="btn btn-success" runat="server" Text="Back" OnClick="btnBackRequest_Click"/>
                    </div>
                    <!-- Button end -->
                                      
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 
</asp:Content>

