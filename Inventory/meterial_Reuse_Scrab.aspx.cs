﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;



public partial class Inventory_meterial_Reuse_Scrab : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionScrabNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Material Issue";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Inventory"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh_Material_Request_Item();
            Employee_Name_Load();
            Load_Data_Empty_CostCenterName();
            Load_Data_Empty_Dept();
            Load_Data_Empty_WareHouse();

            if (Session["Mat_Trans_No"] == null)
            {
                SessionScrabNo = "";
            }
            else
            {
                SessionScrabNo = Session["Mat_Trans_No"].ToString();
                txtIssueNo.Text = SessionScrabNo;
                btnSearch_Click(sender, e);
            }

           // RdpIssueType_SelectedIndexChanged(sender, e);
        }


        Load_OLD_data();
        //Load_Data_Empty_ZoneName();
        //Load_Data_Empty_BinName();
        //Load_Data_Empty_MatRequestNo();
            Load_Data_Empty_ItemCode();
    }
    private void Initial_Data_Referesh_Material_Request_Item()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));

        dt.Columns.Add(new DataColumn("DeptCode", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseCode", typeof(string)));
        dt.Columns.Add(new DataColumn("WarehouseName", typeof(string)));

        dt.Columns.Add(new DataColumn("RequestQty", typeof(string)));
        dt.Columns.Add(new DataColumn("RequestDate", typeof(string)));
        dt.Columns.Add(new DataColumn("IssueQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Value", typeof(string)));
        dt.Columns.Add(new DataColumn("StockQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ZoneName", typeof(string)));
        dt.Columns.Add(new DataColumn("BinName", typeof(string)));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSourceS;
    }
    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }
    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    protected void txtDepartmentName_SelectedIndexChanged(object sender, EventArgs e)
    {
       
    }
    private void Load_Data_Empty_ItemCode()
    {
        string query = "";

        string IssueType = "";
        DataTable DT = new DataTable();
        query = "Select SC.ItemCode,SC.ItemName,IM.PurchaseUOM as UOMCode from Stock_Current_All SC inner join MstItemMaster IM on SC.ItemCode=IM.ItemCode where SC.Ccode='" + SessionCcode + "' And SC.Lcode='" + SessionLcode + "' And IM.Ccode='" + SessionCcode + "' And IM.Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater_ItemCode.DataSource = DT;
            Repeater_ItemCode.DataBind();
            Repeater_ItemCode.Visible = true;
            Repeater_ItemCode_MatReq.Visible = false;


    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();

        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string itemcode = commandArgs[0];
        string UOMCode = commandArgs[1];

        txtItemCodeHide.Value = itemcode;
        txtItemName.Text = Convert.ToString(e.CommandName);
        txtItemUOMHide.Value = UOMCode;
        string IssueType = "";


    
            //btnWareHouse.Enabled = false;
            //btnDept.Enabled = false;

            query = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName,Stock_Qty from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCodeHide.Value + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
   
        if (DT.Rows.Count != 0)
        {
            txtDepartmentName.SelectedValue = DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = DT.Rows[0]["DeptName"].ToString();
            txtWarehouseName.SelectedValue = DT.Rows[0]["WarehouseCode"].ToString();
            txtWarehouseName.SelectedItem.Text = DT.Rows[0]["WarehouseName"].ToString();
            txtZoneName.Text = DT.Rows[0]["ZoneName"].ToString();
            txtBinName.Text = DT.Rows[0]["BinName"].ToString();

        
        }
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    protected void txtWarehouseName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select ZoneName,BinName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And WarehouseCode='" + txtWarehouseName.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtZoneName.Text = DT.Rows[0]["ZoneName"].ToString();
            txtBinName.Text = DT.Rows[0]["BinName"].ToString();
        }
        else
        {
            txtZoneName.Text = "";
            txtBinName.Text = "";
        }
    }
    protected void txtIssueQty_TextChanged(object sender, EventArgs e)
    {

    }
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (txtIssueQty.Text == "0.0" || txtIssueQty.Text == "0" || txtIssueQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Issue Qty...');", true);
        }

        if (txtValue.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Value...');", true);
        }

           //Check Warehouse Name
        query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseName.SelectedValue + "' And WarehouseName='" + txtWarehouseName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Warehouse Name...');", true);
        }

        //Check Department Name
        query = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + txtDepartmentName.SelectedValue + "' And DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Department Name...');", true);
        }
        //check with Zone Name And Bin Name
        query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseName.SelectedValue + "' And ZoneName='" + txtZoneName.Text + "' And BinName='" + txtBinName.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Zone And Bin Details..');", true);
        }

        //check with Item Code And Item Name
        
            query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCodeHide.Value + "' And ItemShortName='" + txtItemName.Text + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
      

        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
        }

     



        if (!ErrFlag)
        {
            //UOM Code get
            string UOM_Code_Str = txtItemUOMHide.Value.ToString();

            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('this Item Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["ItemCode"] = txtItemCodeHide.Value;
                    dr["ItemName"] = txtItemName.Text;
                    dr["UOMCode"] = UOM_Code_Str;
                    dr["DeptCode"] = txtDepartmentName.SelectedValue;
                    dr["DeptName"] = txtDepartmentName.SelectedItem.Text;
                    dr["WarehouseCode"] = txtWarehouseName.SelectedValue;
                    dr["WarehouseName"] = txtWarehouseName.SelectedItem.Text;
                    dr["StockQty"] = txtIssueQty.Text;
                    dr["Value"] = txtValue.Text;
                    
                    dr["ZoneName"] = txtZoneName.Text;
                    dr["BinName"] = txtBinName.Text;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    txtItemCodeHide.Value = ""; txtItemName.Text = ""; txtItemUOMHide.Value = ""; 
                    txtIssueQty.Text = ""; txtValue.Text = ""; txtZoneName.Text = "";
                    txtBinName.Text = "";  txtDepartmentName.SelectedValue = "-Select-"; 
                    txtWarehouseName.SelectedValue = "-Select-";
                 
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["ItemCode"] = txtItemCodeHide.Value;
                dr["ItemName"] = txtItemName.Text;
                dr["UOMCode"] = UOM_Code_Str;
                dr["DeptCode"] = txtDepartmentName.SelectedValue;
                dr["DeptName"] = txtDepartmentName.SelectedItem.Text;
                dr["WarehouseCode"] = txtWarehouseName.SelectedValue;
                dr["WarehouseName"] = txtWarehouseName.SelectedItem.Text;

                dr["StockQty"] = txtIssueQty.Text;
                dr["Value"] = txtValue.Text;
             
                dr["ZoneName"] = txtZoneName.Text;
                dr["BinName"] = txtBinName.Text;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                txtItemCodeHide.Value = ""; txtItemName.Text = ""; txtItemUOMHide.Value = ""; 
                txtIssueQty.Text = ""; txtValue.Text = ""; txtZoneName.Text = ""; txtBinName.Text = "";
                txtDepartmentName.SelectedValue = "-Select-"; txtWarehouseName.SelectedValue = "-Select-";
               
            }
        }
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }

        //Check Cost Center Name
        query = "Select * from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CostcenterCode='" + txtCostCenterName.SelectedValue + "' And CostcenterName='" + txtCostCenterName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Cost Center Name...');", true);
        }

        //Check Taken by And Issued by
        if (txtGivenBy.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Given By...');", true);
        }
        if (txtIssuedBy.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Issued by...');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Scrab Reuse");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Scrab Reuse Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "8", "2", "Scrab Reuse");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Scrab Reuse Issue..');", true);
            }
        }      //User Rights Check End


        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Scrab Reuse", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtIssueNo.Text = Auto_Transaction_No;
                }
            }
        }
        if (!ErrFlag)
        {
            query = "Select * from Scrap_Meterial_ReUse_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Trans_No='" + txtIssueNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Scrap_Meterial_ReUse_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Trans_No='" + txtIssueNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Scrap_Meterial_ReUse_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Trans_No='" + txtIssueNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into Scrap_Meterial_ReUse_Main(Ccode,Lcode,FinYearCode,FinYearVal,Mat_Trans_No,Ledger_No,Mat_Issue_Date,CostCenterCode,CostCenterName,Givenby,Issuedby,Remarks,UserID,UserName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtIssueNo.Text + "','" + txtLedgerNo.Text + "','" + txtDate.Text + "','" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "',";
            query = query + " '" + txtGivenBy.Value + "','" + txtIssuedBy.Value + "','" + txtRemarks.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            //Meterial_Issue_Main_sub
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Scrap_Meterial_ReUse_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Mat_Trans_No,Mat_Issue_Date,ItemCode,ItemName,UOMCode,DeptCode,DeptName,Qty,Value,WarehouseCode,WarehouseName,ZoneName,BinName,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtIssueNo.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["DeptCode"].ToString() + "','" + dt.Rows[i]["DeptName"].ToString() + "','" + dt.Rows[i]["StockQty"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["WarehouseCode"].ToString() + "','" + dt.Rows[i]["WarehouseName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["ZoneName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
                
            }
            Stock_Add();
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Material Issue Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Material Issue Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Mat_Issue_No"] = txtIssueNo.Text;
            btnSave.Text = "Update";
        }


    }

    private void Stock_Add()
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();

        query = "Select * from Reuse_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtIssueNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            query = "Delete from Reuse_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtIssueNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }


        DateTime transDate = Convert.ToDateTime(txtDate.Text);

        //Insert Stock_Transaction_Ledger
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            
            //Insert Into Stock_Ledger_All
            query = "Insert Into Reuse_Stock_Ledger_All(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
            query = query + " Trans_Type,DeptCode,DeptName,CostCenterCode,CostCenterName,ItemCode,ItemName,Add_Qty,Add_Value,Minus_Qty,Minus_Value,WarehouseCode,WarehouseName,";
            query = query + " ZoneName,BinName,Supp_Code,Supp_Name,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
            query = query + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtIssueNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtDate.Text + "',";
            query = query + " 'PURCHASE OLD','" + dt.Rows[i]["DeptCode"].ToString() + "','" + dt.Rows[i]["DeptName"].ToString() + "','" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "',";
            query = query + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
            query = query + " '" + dt.Rows[i]["StockQty"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "','0.0','0.0','" + dt.Rows[i]["WarehouseCode"].ToString() + "',";
            query = query + " '" + dt.Rows[i]["WarehouseName"].ToString() + "','" + dt.Rows[i]["ZoneName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "',";
            query = query + " '','','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);
                       
        }




    }
    protected void RbtOld_New_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }
    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("meterial_Reuse_Scrab_Main.aspx");
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {

    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();

    }
    private void Load_Data_Empty_Dept()
    {


        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }
    private void Employee_Name_Load()
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        txtIssuedBy.Items.Clear();
        txtGivenBy.Items.Clear();
        query = "Select * from MstEmployee where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by EmpName Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtIssuedBy.Items.Add("-Select-");
        txtGivenBy.Items.Add("-Select-");
        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {
            txtIssuedBy.Items.Add(Main_DT.Rows[i]["EmpName"].ToString());
            txtGivenBy.Items.Add(Main_DT.Rows[i]["EmpName"].ToString());
        }
    }
    private void Load_Data_Empty_CostCenterName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtCostCenterName.Items.Clear();
        query = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCostCenterName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["CostcenterCode"] = "-Select-";
        dr["CostcenterName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCostCenterName.DataTextField = "CostcenterName";
        txtCostCenterName.DataValueField = "CostcenterCode";
        txtCostCenterName.DataBind();


    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Scrap_Meterial_ReUse_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Trans_No='" + txtIssueNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["Mat_Issue_Date"].ToString();
            // txtWarehouseCodeHide.Value = Main_DT.Rows[0]["WarehouseCode"].ToString();
            // txtWarehouseName.Text = Main_DT.Rows[0]["WarehouseName"].ToString();
            // txtDeptCodeHide.Value = Main_DT.Rows[0]["DeptCode"].ToString();
            // txtDepartmentName.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtCostCenterName.SelectedValue = Main_DT.Rows[0]["CostCenterCode"].ToString();
            txtCostCenterName.SelectedItem.Text = Main_DT.Rows[0]["CostCenterName"].ToString();
            txtGivenBy.Value = Main_DT.Rows[0]["Givenby"].ToString();
            txtIssuedBy.Value = Main_DT.Rows[0]["Issuedby"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();
            //RdpIssueType.SelectedValue = Main_DT.Rows[0]["IssueType"].ToString();
            //if (RdpIssueType.SelectedValue == "1")
            //{
            //    txtMaterialRequestNo.Enabled = true; txtMaterialRequestDate.Enabled = true;
            //    txtRequestQty.Enabled = true; txtRequestDate.Enabled = true;
            //    txtMaterialRequestNo.Text = Main_DT.Rows[0]["Mat_Req_No"].ToString();
            //    txtMaterialRequestDate.Text = Main_DT.Rows[0]["Mat_Req_Date"].ToString();
            //}
            //else
            //{
            //    txtMaterialRequestNo.Enabled = false; txtMaterialRequestDate.Enabled = false;
            //    txtRequestQty.Enabled = false; txtRequestDate.Enabled = false;
            //    txtMaterialRequestNo.Text = ""; txtMaterialRequestDate.Text = "";
            //    txtItemCodeHide.Value = ""; txtItemName.Text = ""; txtItemUOMHide.Value = "";
            //    txtRequestQty.Text = ""; txtRequestDate.Text = "";
            //}
            //Pur_Order_Receipt_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select ItemCode,ItemName,UOMCode,DeptCode,DeptName,Qty as StockQty,Value,WarehouseCode,WarehouseName,ZoneName,BinName from Scrap_Meterial_ReUse_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Mat_Trans_No='" + txtIssueNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            if (dt.Rows.Count != 0)
            {
                txtItemName.Text = dt.Rows[0]["ItemName"].ToString();
                txtDepartmentName.SelectedValue = dt.Rows[0]["DeptCode"].ToString();
                txtDepartmentName.SelectedItem.Text = dt.Rows[0]["DeptName"].ToString();
                txtWarehouseName.SelectedValue = dt.Rows[0]["WarehouseCode"].ToString();
                txtWarehouseName.SelectedItem.Text = dt.Rows[0]["WarehouseName"].ToString();
                txtIssueQty.Text = dt.Rows[0]["StockQty"].ToString();
                txtValue.Text = dt.Rows[0]["Value"].ToString();
                txtZoneName.Text = dt.Rows[0]["ZoneName"].ToString();
                txtBinName.Text = dt.Rows[0]["BinName"].ToString();
            }
          
            

            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            //Clear_All_Field();
        }
    }
}
