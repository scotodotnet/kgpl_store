﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;



public partial class Reports_Report_StdPurchase_Order_Det : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string DeptName = ""; string SupQtnNo = ""; string StdPurOrdNo = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        Page.Title = "ERP Stores Module :: Standard Purchase Order Details";

        if (!IsPostBack)
        {
            Load_Data_Empty_Dept();
            Load_Data_Empty_SupQtnNo();
            Load_Data_Empty_StdPurOrdNo();

        }

        Load_Data_Empty_ItemCode();
        
        Load_Data_Empty_Supp1();
        

    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDepartmentName.SelectedValue = "-Select-";
       // txtDeptCodeHide.Value = "";
        txtSupplierName.Text = "";
        txtSuppCodehide.Value = "";
        txtStdPurOrdNo.SelectedValue = "-Select-";
        //txtStdPurOrdDateHide.Value = "";
        txtSPONo.SelectedValue = "-Select-";
        //txtSPODateHide.Value = "";
        txtItemName.Text = "";
        txtItemCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
       RptName = "Standard Purchase Order Details Report";

       if (txtStdPurOrdNo.SelectedItem.Text != "-Select-")
       {
           StdPurOrdNo = txtStdPurOrdNo.SelectedItem.Text;
       }
       else
       {
           StdPurOrdNo = "";
       }
       if (txtSPONo.SelectedItem.Text != "-Select-")
       {
           SupQtnNo = txtSPONo.SelectedItem.Text;
       }
       else
       {
           SupQtnNo = "";
       }
       if (txtDepartmentName.SelectedItem.Text != "-Select-")
       {
           DeptName = txtDepartmentName.SelectedItem.Text;
       }
       else
       {
           DeptName = "";
       }



       ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + txtSupplierName.Text + "&StdPurOrdNo=" + StdPurOrdNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnInvcFormat_Click(object sender, EventArgs e)
    {
        RptName = "Standard Purchase Order Details Invoice Format";


        if (txtStdPurOrdNo.SelectedItem.Text != "-Select-")
        {
            StdPurOrdNo = txtStdPurOrdNo.SelectedItem.Text;
        }
        else
        {
            StdPurOrdNo = "";
        }
        if (txtSPONo.SelectedItem.Text != "-Select-")
        {
            SupQtnNo = txtSPONo.SelectedItem.Text;
        }
        else
        {
            SupQtnNo = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + txtSupplierName.Text + "&StdPurOrdNo=" + StdPurOrdNo + "&SupQtNo=" + SupQtnNo + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        if (txtRequestStatus.SelectedItem.Text != "-select-")
        {
            RptName = "Standard Purchase Order List";

            ResponseHelper.Redirect("ReportDisplay.aspx?Request_Str=" + txtRequestStatus.SelectedItem.Text + "&FromDate=" + txtFrmDate.Text + "&ToDate=" + txtTDate.Text + "&RptName=" + RptName, "_blank", "");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Status');", true);
        }
    }
    protected void btn_Clear_Click(object sender, EventArgs e)
    {
        txtRequestStatus.SelectedValue = "0";
        txtFrmDate.Text = "";
        txtTDate.Text = "";
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();


    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }
    protected void btnSupQtnNo_Click(object sender, EventArgs e)
    {
        //modalPop_SupQtnNo.Show();
    }
    private void Load_Data_Empty_SupQtnNo()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtSPONo.Items.Clear();
        query = "Select QuotNo,QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";

        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtSPONo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["QuotNo"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtSPONo.DataTextField = "QuotNo";

        txtSPONo.DataBind();



        //string query = "";
        //DataTable DT = new DataTable();


        //query = "Select QuotNo,QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";

        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_SupQtnNo.DataSource = DT;
        //Repeater_SupQtnNo.DataBind();
        //Repeater_SupQtnNo.Visible = true;

    }

    protected void GridViewClick_SupQtnNo(object sender, CommandEventArgs e)
    {
        txtSPONo.Text = Convert.ToString(e.CommandArgument);
        //txtSPODateHide.Value = Convert.ToString(e.CommandName);


    }


    protected void btnSupp1_Click(object sender, EventArgs e)
    {
        modalPop_Supp1.Show();
    }
    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier1.DataSource = DT;
        Repeater_Supplier1.DataBind();
        Repeater_Supplier1.Visible = true;

    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
        txtSupplierName.Text = Convert.ToString(e.CommandName);


    }


    protected void btnStdPurOrdNo_Click(object sender, EventArgs e)
    {
        //modalPop_StdPurOrdNo.Show();
    }
    private void Load_Data_Empty_StdPurOrdNo()
    {


        string query = "";
        DataTable Main_DT = new DataTable();

        txtStdPurOrdNo.Items.Clear();
        query = "Select Std_PO_No,Std_PO_Date from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And PO_Status='1'"; 
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtStdPurOrdNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Std_PO_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtStdPurOrdNo.DataTextField = "Std_PO_No";

        txtStdPurOrdNo.DataBind();



        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Std_PO_No,Std_PO_Date from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And PO_Status='1'"; 
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_StdPurOrdNo.DataSource = DT;
        //Repeater_StdPurOrdNo.DataBind();
        //Repeater_StdPurOrdNo.Visible = true;

    }

    protected void GridViewClick_StdPurOrdNo(object sender, CommandEventArgs e)
    {
        txtStdPurOrdNo.Text = Convert.ToString(e.CommandArgument);
        //txtStdPurOrdDateHide.Value = Convert.ToString(e.CommandName);

    }

}
