﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Report_Stock_Adjustment.aspx.cs" Inherits="Reports_Report_Stock_Adjustment" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Supplier1').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Supplier1').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#AdjustmentBy').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#AdjustmentBy').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#TransNo').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#TransNo').dataTable();               
            }
        });
    };
</script>

<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
    
  $find('AdjustmentBy_Close').hide();
 
  $find('Item_Close').hide();
  $find('TransNo_Close').hide();
     }
   } 
 </script>
<!--Ajax popup End-->



<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Stock Adjustment Entry</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">        
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Stock Adjustment Entry</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Adjustment By<span class="mandatory">*</span></label>
					            
					      <asp:DropDownList ID="txtAdjustmentBy" runat="server" class="js-states form-control">
                          </asp:DropDownList>
					            
					            
					            <%--<asp:TextBox ID="txtAdjustmentBy" MaxLength="20" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					             <asp:Button ID="btnAdjustmentBy" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnAdjustmentBy_Click"/>--%>
					            <%--<cc1:ModalPopupExtender ID="modalPop_AdjustmentBy"  runat="server" PopupControlID="Panel1" TargetControlID="btnAdjustmentBy"
                       CancelControlID="BtnClear_AdjustmentBy" BackgroundCssClass="modalBackground" BehaviorID="AdjustmentBy_Close">
                    </cc1:ModalPopupExtender>--%>
                    
                    <%--<asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Employee Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_AdjustmentBy" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="AdjustmentBy" class="display table">
                         <thead >
                         <tr>
                         <th>EmpCode</th>
                         <th>EmpName</th>
                         
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("EmpCode")%></td>
                          <td><%# Eval("EmpName")%></td>
                         
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditAdjustmentBy" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_AdjustmentBy" CommandArgument='<%# Eval("EmpName")%>' CommandName='<%# Eval("EmpCode")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_AdjustmentBy" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					             <%--<asp:HiddenField ID="txtEmpID" runat="server" />--%>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Name</label>
					            <asp:TextBox ID="txtItemName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnItemCode" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnItemCode_Click"/>
                                    
                                    <cc1:ModalPopupExtender ID="modalPop_ItemCode"  runat="server" PopupControlID="Panel7" TargetControlID="btnItemCode"
                       CancelControlID="BtnItemClose" BackgroundCssClass="modalBackground" BehaviorID="Item_Close">
                    </cc1:ModalPopupExtender>
                    
                                    <asp:Panel ID="Panel7" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Item Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					   
					    
					    <asp:Repeater ID="Repeater_ItemCode" runat="server" EnableViewState="false" Visible="false">
					    <HeaderTemplate>
                         <table id="ItemTable" class="display table">
                         <thead >
                         <tr>
                         <th>ItemCode</th>
                         <th>ItemName</th>
                         <th>Stock_Qty</th> 
                         <th>Stock_Value</th>
                         
                       
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ItemCode")%></td>
                          <td><%# Eval("ItemName")%></td>
                           <td><%# Eval("Stock_Qty")%></td>
                           <td><%# Eval("Stock_Value")%></td>
                        
                     
                          <td>
                          <asp:LinkButton ID="btnEdit_ItemCode" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ItemCode" CommandArgument='<%# Eval("ItemCode")+","+ Eval("Stock_Qty")+","+ Eval("Stock_Value")%>' CommandName='<%# Eval("ItemName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					    
					    
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnItemClose" class="btn btn-rounded" runat="server" Text="Close" />
					     </div>
					     </asp:Panel>
					            
					            
					            
					            <asp:HiddenField ID="txtItemCodeHide" runat="server" />
					            <asp:HiddenField ID="txtWarehouseCodeHide" runat="server" />
					            <asp:HiddenField ID="txtWarehouseNameHide" runat="server" />
					            <asp:HiddenField ID="txtZoneNameHide" runat="server" />
					            <asp:HiddenField ID="txtBinNameHide" runat="server" />
					           
					            <asp:RequiredFieldValidator ControlToValidate="txtItemName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        
					   <div class="form-group col-md-4">
					            <label for="exampleInputName">Trans No</label>
					            
					            <asp:DropDownList ID="txtTransNo" runat="server" class="js-states form-control">
                          </asp:DropDownList>
					            
					           <%-- <asp:TextBox ID="txtTransNo" class="form-control" runat="server" style="float: left;width: 85%;"></asp:TextBox>
					            <asp:Button ID="btnTransNo" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnTransNo_Click"/>--%>
                    <%--<cc1:ModalPopupExtender ID="modalPop_TransNo"  runat="server" PopupControlID="Panel3" TargetControlID="btnTransNo"
                       CancelControlID="BtnClear_TransNo" BackgroundCssClass="modalBackground" BehaviorID="TransNo_Close">
                    </cc1:ModalPopupExtender>--%>
                   
                    <%--<asp:Panel ID="Panel3" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Stock Adjustment Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_TransNo" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="TransNo" class="display table">
                         <thead >
                         <tr>
                         <th>Adjustment_No</th>
                         <th>Adjustmen_Date</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("Adjustment_No")%></td>
                          <td><%# Eval("Adjustmen_Date")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditTransNo" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_TransNo" CommandArgument='<%# Eval("Adjustment_No")%>' CommandName='<%# Eval("Adjustmen_Date")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_TransNo" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            <%--<asp:HiddenField ID="txtTransDateHide" runat="server" />--%>
					        </div>
					        
					        </div>
					        </div>
					        
					        
					       <div class="col-md-12">
					    <div class="row"> 
					    
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">From Date</label>
					            <asp:TextBox ID="txtFromDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderFromDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtFromDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">To Date</label>
					            <asp:TextBox ID="txtToDate" class="form-control date-picker" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderToDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtToDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					    </div>
					</div>
					
					<div class="clearfix"></div>
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnReports" class="btn btn-success"  runat="server" 
                            Text="Report" onclick="btnReports_Click"/>
                        <asp:Button ID="btnClear" class="btn btn-success" runat="server" Text="Clear" 
                            onclick="btnClear_Click"/>
                    </div>
                    <!-- Button end -->
                      <%--<div class="form-group row">
                          
                          <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
                              AutoDataBind="True" DisplayPage="False" DisplayToolbar="False" 
                              EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False" 
                              GroupTreeImagesFolderUrl="" Height="800px" ToolbarImagesFolderUrl="" 
                              ToolPanelView="None" ToolPanelWidth="200px" Width="200px" />
                          <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
                              <Report FileName="D:\ND\ERP_Stores\Source_Code\crystal\PurchaseEnquiryReport.rpt">
                              </Report>
                          </CR:CrystalReportSource>
                      </div>--%>                
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    <!-- Dashboard start -->
            <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
            <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->



</asp:Content>

