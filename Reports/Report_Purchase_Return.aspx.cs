﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_Purchase_Return : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;



    string WareHouse = ""; string DeptName = ""; string PurchaseReturnNo = "";

    string RptName = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        Page.Title = "ERP Stores Module :: Purchase Return Details Report";

        if (!IsPostBack)
        {
            Load_Data_Empty_WareHouse();
            Load_Data_Empty_Dept();

            Load_Data_Empty_PurchaseReturnNo();
        }
        Load_Data_Empty_Supp1();
        Load_Data_Empty_ItemCode();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtWarehouseName.SelectedValue = "-Select-";
        //txtWarehouseCodeHide.Value = "";
        txtDepartmentName.SelectedValue = "-Select-";
        //txtDeptCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtItemName.Text = "";
        txtItemCodeHide.Value = "";
        txtSupplierName.Text = "";
        txtSuppCodehide.Value = "";
        txtPurchaseReturnNo.SelectedValue = "-Select-";
        //txtPurchaseReturnNoHide.Value = "";
    }
       
    protected void btnReport_Click(object sender, EventArgs e)
    {
        
        RptName = "Purchase Return Report";

        if (txtPurchaseReturnNo.SelectedItem.Text != "-Select-")
        {
            PurchaseReturnNo = txtPurchaseReturnNo.SelectedItem.Text;
        }
        else
        {
            PurchaseReturnNo = "";
        }
        
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }


        ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&PurRetNo=" + PurchaseReturnNo + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName + "&WareHouse=" + WareHouse + "&DepartName=" + txtDepartmentName.Text + "&SupplierName=" + txtSupplierName.Text, "_blank", "");

    }

    protected void btnInvFormat_Click(object sender, EventArgs e)
    {
        RptName = "Purchase Return Invoice Format Report";

        if (txtPurchaseReturnNo.SelectedItem.Text != "-Select-")
        {
            PurchaseReturnNo = txtPurchaseReturnNo.SelectedItem.Text;
        }
        else
        {
            PurchaseReturnNo = "";
        }

        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }



        ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&PurRetNo=" + PurchaseReturnNo + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName + "&WareHouse=" + WareHouse + "&DepartName=" + txtDepartmentName.Text + "&SupplierName=" + txtSupplierName.Text, "_blank", "");

    }


    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        //modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();

    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtWarehouseName.Text = Convert.ToString(e.CommandName);

    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnSupp1_Click(object sender, EventArgs e)
    {
        modalPop_Supp1.Show();
    }
    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier1.DataSource = DT;
        Repeater_Supplier1.DataBind();
        Repeater_Supplier1.Visible = true;

    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
        txtSupplierName.Text = Convert.ToString(e.CommandName);


    }


    protected void btnPurchaseReturnNo_Click(object sender, EventArgs e)
    {
       // modalPop_PurchaseReturnNo.Show();
    }
    private void Load_Data_Empty_PurchaseReturnNo()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtPurchaseReturnNo.Items.Clear();
        query = "Select Pur_Return_No,Pur_Return_Date from Purc_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And PO_Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtPurchaseReturnNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Pur_Return_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtPurchaseReturnNo.DataTextField = "Pur_Return_No";

        txtPurchaseReturnNo.DataBind();



        //string query = "";
        //DataTable DT = new DataTable();
        //query = "Select Pur_Return_No,Pur_Return_Date from Purc_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And PO_Status='1'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_PurchaseReturnNo.DataSource = DT;
        //Repeater_PurchaseReturnNo.DataBind();
        //Repeater_PurchaseReturnNo.Visible = true;

    }

    protected void GridViewClick_PurchaseReturnNo(object sender, CommandEventArgs e)
    {
        txtPurchaseReturnNo.Text = Convert.ToString(e.CommandArgument);
        //txtPurchaseReturnNoHide.Value = Convert.ToString(e.CommandName);
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

}
