﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_MonthlyDeptWise_Purchase_Issue : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();


    string RptName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "ERP Stores Module :: Monthly DepartmentWise Purchase and Issue Details";
    }
    protected void btnReports_Click(object sender, EventArgs e)
    {
        RptName = "Monthly Department wise Purchase and Issue Report";

        ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
    }
}
