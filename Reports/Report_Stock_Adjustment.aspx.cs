﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_Stock_Adjustment : System.Web.UI.Page
{

    string SSQL = "";
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    DataTable PurEnqrDT = new DataTable();
    DateTime frmDate;
    DateTime ToDate;
    DataTable AutoDataTable = new DataTable();
    DataSet ds = new DataSet();
    string RptName = "";
    string AdjustmentBy = ""; string TransNo = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "ERP Stores Module :: Stock Adjustment Entry Details";

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Load_Data_Empty_AdjustmentBy();
            Load_Data_Empty_TransNo();
        }
        Load_Data_Empty_ItemCode();
        
    }

    protected void btnAdjustmentBy_Click(object sender, EventArgs e)
    {
        //modalPop_AdjustmentBy.Show();
    }
    private void Load_Data_Empty_AdjustmentBy()
    {


        string query = "";
        DataTable Main_DT = new DataTable();

        txtAdjustmentBy.Items.Clear();
        query = "Select EmpCode,EmpName from MstEmployee where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And ActiveMode='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtAdjustmentBy.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["EmpCode"] = "-Select-";
        dr["EmpName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtAdjustmentBy.DataTextField = "EmpName";
        txtAdjustmentBy.DataValueField = "EmpCode";
        txtAdjustmentBy.DataBind();


        //string query = "";
        //DataTable DT = new DataTable();
        //query = "Select EmpCode,EmpName from MstEmployee where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        //query = query + " And ActiveMode='1'";
        //DT = objdata.RptEmployeeMultipleDetails(query);


        //Repeater_AdjustmentBy.DataSource = DT;
        //Repeater_AdjustmentBy.DataBind();
        //Repeater_AdjustmentBy.Visible = true;
    }

    protected void GridViewClick_AdjustmentBy(object sender, CommandEventArgs e)
    {
        txtAdjustmentBy.Text = Convert.ToString(e.CommandArgument);
        //txtEmpID.Value = Convert.ToString(e.CommandName);

    }
    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();
        query = "Select ItemCode,ItemName,Stock_Qty,Stock_Value from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

        DT = objdata.RptEmployeeMultipleDetails(query);


        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string itemcode = commandArgs[0];
        string StckQty = commandArgs[1];
        string StckValue = commandArgs[2];

        string query = "";
        DataTable dt = new DataTable();

        query = "Select *from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' And ItemCode='" + itemcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);


        txtItemCodeHide.Value = itemcode;
        txtItemName.Text = Convert.ToString(e.CommandName);
      
        txtWarehouseCodeHide.Value = dt.Rows[0]["WarehouseCode"].ToString();
        txtWarehouseNameHide.Value = dt.Rows[0]["WarehouseName"].ToString();
        txtZoneNameHide.Value = dt.Rows[0]["ZoneName"].ToString();
        txtBinNameHide.Value = dt.Rows[0]["BinName"].ToString();

    }

    protected void btnTransNo_Click(object sender, EventArgs e)
    {
        //modalPop_TransNo.Show();
    }
    private void Load_Data_Empty_TransNo()
    {


        string query = "";
        DataTable Main_DT = new DataTable();

        txtTransNo.Items.Clear();
        query = "Select Adjustment_No,Adjustmen_Date from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";

        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtTransNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Adjustment_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtTransNo.DataTextField = "Adjustment_No";

        txtTransNo.DataBind();

        //string query = "";
        //DataTable DT = new DataTable();



        //query = "Select Adjustment_No,Adjustmen_Date from Adjustment_Stock_Entry_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";

        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_TransNo.DataSource = DT;
        //Repeater_TransNo.DataBind();
        //Repeater_TransNo.Visible = true;

    }

    protected void GridViewClick_TransNo(object sender, CommandEventArgs e)
    {
        txtTransNo.Text = Convert.ToString(e.CommandArgument);
        //txtTransDateHide.Value = Convert.ToString(e.CommandName);


    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtAdjustmentBy.SelectedValue = "-Select-";
        txtTransNo.SelectedValue = "-Select-";
        txtItemName.Text = "";
        txtItemCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
       
    }
    protected void btnReports_Click(object sender, EventArgs e)
    {
        RptName = "Stock Adjustment Report";

        if (txtAdjustmentBy.SelectedItem.Text != "-Select-")
        {
            AdjustmentBy = txtAdjustmentBy.SelectedItem.Text;
        }
        else
        {
            AdjustmentBy = "";
        }
        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }


        ResponseHelper.Redirect("ReportDisplay.aspx?AdjustmentBy=" + AdjustmentBy + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&AdjustmentNo=" + TransNo + "&RptName=" + RptName, "_blank", "");
    }
}
