﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Reports_Report_Reuse : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Closing Stock Statement";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_RptStock"));
            li.Attributes.Add("class", "droplink active open");

        }
    }
    protected void btnReports_Click(object sender, EventArgs e)
    {
        string[] finyear = SessionFinYearVal.Split('_');

        string FromDate_Chk = "01/04/" + finyear[0];
        string ToDate_Chk = "31/03/" + finyear[1];

        string fromDate = txtFromDate.Text;
        string toDate = txtToDate.Text;

           
                RptName = "Scrab Reuse";

                ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
        
        
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {

    }
}
