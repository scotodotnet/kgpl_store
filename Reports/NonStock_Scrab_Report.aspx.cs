﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class NonStock_Scrab_Reportaspx : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
     string FromDate = ""; string ToDate = "";
      DateTime frmDate; DateTime toDate;
    string RptName = "";
    DataTable dt = new DataTable();
    DataTable AutoDataTable=new DataTable();
    string SQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        
           

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Non-Stock Scrab Report";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_RptStock"));
            li.Attributes.Add("class", "droplink active open");
           
        }
    
        
    }

    protected void btnReports_Click(object sender, EventArgs e)
    {
        string[] finyear = SessionFinYearVal.Split('_');

        string FromDate_Chk = "01/04/" + finyear[0];
        string ToDate_Chk = "31/03/" + finyear[1];

        string fromDate = txtFromDate.Text;
        string toDate = txtToDate.Text;


        RptName = "Non Stock Scrab";

        ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
        
        //  AutoDataTable.Columns.Add("S.No");
        //AutoDataTable.Columns.Add("Date");
        //AutoDataTable.Columns.Add("Item_Name");
        //AutoDataTable.Columns.Add("Quantity");
        //AutoDataTable.Columns.Add("Rate");
        //  if (FromDate != "")
        //{
        //    frmDate = Convert.ToDateTime(FromDate);
        //}
        //if (ToDate != "")
        //{
        //    toDate = Convert.ToDateTime(ToDate);
        //}
        //if (FromDate != "" && ToDate != "")
        //{

        //    SQL = "select * From Non_Stock_Scrab where ";
        //    SQL = SQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
        //    SQL = SQL + " And CONVERT(DATETIME,Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
        //    SQL = SQL + " And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        //    dt = objdata.RptEmployeeMultipleDetails(SQL);
        //}
        //else if (FromDate !="" && ToDate =="")

        //{
        //     SQL = "select * From Non_Stock_Scrab where ";
        //    SQL = SQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
        //    SQL = SQL + " And CONVERT(DATETIME,Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
        //    dt = objdata.RptEmployeeMultipleDetails(SQL);
        //}
        //else if (FromDate == "" & ToDate == "")
        //{
        //    SQL = "select * From Non_Stock_Scrab where ";
        //    SQL = SQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
        //    dt = objdata.RptEmployeeMultipleDetails(SQL);
        //}


    }
    protected void btnClear_Click(object sender, EventArgs e)
    {

    }
}
