﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Report_GatePass_Non-Returnable.aspx.cs" Inherits="Reports_Report_GatePass_Non_Returnable" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

    <script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

    <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#WareHouse').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#WareHouse').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Dept').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Dept').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#GPOutNo').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#GPOutNo').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Issuedby').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Issuedby').dataTable();               
            }
        });
    };
</script>


<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
   $find('WareHouse_Close').hide();
   $find('Dept_Close').hide();
 $find('GPOutNo_Close').hide();
  $find('Issuedby_Close').hide();
   $find('Item_Close').hide();
      }
   } 
 </script>
<!--Ajax popup End-->



<%--Warehouse Name Select List Script Start--%>    
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_Warehouse_Name();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Warehouse_Name);
        });
        function initializer_Warehouse_Name() {
            $("#<%=txtWarehouseName.ClientID %>").autocomplete({                 
                source: function (request, response) {                        
                      $.ajax({ 
                          url: '<%=ResolveUrl("../List_Service.asmx/GetWarehouse_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) {
                                  return {
                                      label: item.split('-')[0] + "-" + item.split('-')[1],
                                      val: item.split('-')[1],
                                      text_val:item.split('-')[0],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtWarehouseName.ClientID %>").val(i.item.val);
                      //$("#<%=txtWarehouseName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtWarehouseName.ClientID %>").val(i.item.text_val);
                      $("#<%=txtWarehouseCodeHide.ClientID %>").val(i.item.val);
                   return false;
                  }
              });
          }
      </script>--%>
<%--Warehouse Name Select List Script End--%>

<%--Department Name Select List Script Start--%>    
   <%-- <script type="text/javascript">
         $(document).ready(function () {
            initializer_Dept_Code();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Dept_Code);
        });
        function initializer_Dept_Code() {
            $("#<%=txtDepartmentName.ClientID %>").autocomplete({                 
                source: function (request, response) {                        
                      $.ajax({ 
                          url: '<%=ResolveUrl("../List_Service.asmx/GetDeptFull_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) {
                                  return {
                                      label: item.split('|')[0] + "|" + item.split('|')[1],
                                      val: item.split('|')[1],
                                      text_val:item.split('|')[0],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtDepartmentName.ClientID %>").val(i.item.val);
                      //$("#<%=txtDepartmentName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtDepartmentName.ClientID %>").val(i.item.text_val);
                      $("#<%=txtDeptCodeHide.ClientID %>").val(i.item.val);
                   return false;
                  }
              });
          }
      </script>--%>
<%--Department Name Select List Script End--%>

<%--Gate Pass OUT Non-Returnable No Select List Script Saart--%>    
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_GPOutNo_PO();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_GPOutNo_PO);
        });
        function initializer_GPOutNo_PO() {
            $("#<%=txtGPOutNo.ClientID %>").autocomplete({                 
                source: function (request, response) {
                      $.ajax({
                          url: '<%=ResolveUrl("../List_Service.asmx/GetGPOutNo_NONReturn_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) { 
                                  return {                                  
                                      label: item.split('|')[0],
                                      val: item.split('|')[0],
                                      PE_Date:item.split('|')[1],
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtGPOutNo.ClientID %>").val(i.item.val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtGPOutNo.ClientID %>").val(i.item.val);
                      $("#<%=txtGPOutDateHide.ClientID %>").val(i.item.PE_Date);
                   return false;
                  }
              }); 
          }
      </script>--%>
<%--Gate Pass OUT Non-Returnable No Select List Script End --%>

<%--Gate Pass Issued By Select List Script Start--%>    
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_GPOutIssuedBy();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_GPOutIssuedBy);
        });
        function initializer_GPOutIssuedBy() {
            $("#<%=txtIssuedby.ClientID %>").autocomplete({                 
                source: function (request, response) {                        
                      $.ajax({ 
                          url: '<%=ResolveUrl("../List_Service.asmx/GetGPOutNonRetIssuedBy_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) {
                                  return {
                                      label: item.split('-')[0],
                                      val: item.split('-')[0],
                                      text_val:item.split('-')[0],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtIssuedby.ClientID %>").val(i.item.val);
                      //$("#<%=txtIssuedby.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtIssuedby.ClientID %>").val(i.item.val);
                      return false;
                  }
              });
          }
      </script>--%>
<%--Gate Pass Issued By Select List Script End--%>

<%--Item Select List Script Saart--%>    
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_Item();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Item);
        });
        function initializer_Item() {
            $("#<%=txtItemName.ClientID %>").autocomplete({                 
                source: function (request, response) {
                      $.ajax({
                          url: '<%=ResolveUrl("../List_Service.asmx/GetItemCodeSelect_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) { 
                                  return {                                  
                                      label: item.split('-')[0] + "-" + item.split('-')[1],
                                      val: item.split('-')[1],
                                      text_val:item.split('-')[0],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtItemName.ClientID %>").val(i.item.val);
                      //$("#<%=txtItemName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtItemName.ClientID %>").val(i.item.text_val);
                      $("#<%=txtItemCodeHide.ClientID %>").val(i.item.val);
                   return false;
                  }
              }); 
          }
      </script>--%>
<%--Item Select List Script End--%>


    <div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">GatePass Non-Returnable Details</li></h4> 
    </ol>
</div>
    <div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">        
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">GatePass Non-Returnable Details</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Warehouse</label>
					            
					            <asp:DropDownList ID="txtWarehouseName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					            
					            <%--<asp:TextBox ID="txtWarehouseName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnWareHouse" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnWareHouse_Click"/>--%>
					            <%--<cc1:ModalPopupExtender ID="modalPop_WareHouse"  runat="server" PopupControlID="Panel2" TargetControlID="btnWareHouse"
                       CancelControlID="BtnClear_WareHouse" BackgroundCssClass="modalBackground" BehaviorID="WareHouse_Close">
                    </cc1:ModalPopupExtender>--%>
                    
                    <%--<asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            WareHouse Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_WareHouse" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="WareHouse" class="display table">
                         <thead >
                         <tr>
                         <th>WarehouseCode</th>
                         <th>WarehouseName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("WarehouseCode")%></td>
                          <td><%# Eval("WarehouseName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditWareHouse" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_WareHouse" CommandArgument='<%# Eval("WarehouseCode")%>' CommandName='<%# Eval("WarehouseName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_WareHouse" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            <%--<asp:HiddenField ID="txtWarehouseCodeHide" runat="server" />--%>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Department</label>
					            
					            <asp:DropDownList ID="txtDepartmentName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					            
					            <%--<asp:TextBox ID="txtDepartmentName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnDept" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnDept_Click"/>--%>
                    <%--<cc1:ModalPopupExtender ID="modalPop_Dept"  runat="server" PopupControlID="Panel1" TargetControlID="btnDept"
                       CancelControlID="BtnClear_Dept" BackgroundCssClass="modalBackground" BehaviorID="Dept_Close">
                    </cc1:ModalPopupExtender>--%>
                   
                    <%--<asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Department Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_Dept" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="Dept" class="display table">
                         <thead >
                         <tr>
                         <th>DeptCode</th>
                         <th>DeptName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("DeptCode")%></td>
                          <td><%# Eval("DeptName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_Dept" CommandArgument='<%# Eval("DeptCode")%>' CommandName='<%# Eval("DeptName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_Dept" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            <%--<asp:HiddenField ID="txtDeptCodeHide" runat="server" />--%>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">GP Non-Ret No</label>
					            
					      <asp:DropDownList ID="txtGPOutNo" runat="server" class="js-states form-control">
                          </asp:DropDownList>
					            
					            
					            <%--<asp:TextBox ID="txtGPOutNo" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnGPOutNo" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnGPOutNo_Click"/>--%>
                    <%--<cc1:ModalPopupExtender ID="modalPop_GPOutNo"  runat="server" PopupControlID="Panel3" TargetControlID="btnGPOutNo"
                       CancelControlID="BtnClear_GPOutNo" BackgroundCssClass="modalBackground" BehaviorID="GPOutNo_Close">
                    </cc1:ModalPopupExtender>--%>
                   
                    <%--<asp:Panel ID="Panel3" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Gate Pass Out Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_GPOutNo" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="GPOutNo" class="display table">
                         <thead >
                         <tr>
                         <th>GP_Out_No</th>
                         <th>GP_Out_Date</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("GP_Out_No")%></td>
                          <td><%# Eval("GP_Out_Date")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditGPOutNo" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_GPOutNo" CommandArgument='<%# Eval("GP_Out_No")%>' CommandName='<%# Eval("GP_Out_Date")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_GPOutNo" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            <%--<asp:HiddenField ID="txtGPOutDateHide" runat="server" />--%>
					        </div>
					        </div>
					        </div>
					        
					        
					        <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">IssuedBy</label>
					            
					      <asp:DropDownList ID="txtIssuedby" runat="server" class="js-states form-control">
                          </asp:DropDownList>
					            
					            <%--<asp:TextBox ID="txtIssuedby" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnIssuedby" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnIssuedby_Click"/>--%>
                    <%--<cc1:ModalPopupExtender ID="modalPop_Issuedby"  runat="server" PopupControlID="Panel4" TargetControlID="btnIssuedby"
                       CancelControlID="BtnClear_Issuedby" BackgroundCssClass="modalBackground" BehaviorID="Issuedby_Close">
                    </cc1:ModalPopupExtender>--%>
                   
                    <%--<asp:Panel ID="Panel4" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                             Issuedby Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_Issuedby" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="Issuedby" class="display table">
                         <thead >
                         <tr>
                         <th>Issuedby</th>
                        
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("Issuedby")%></td>
                         
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditGPOIssuedby" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_Issuedby" CommandArgument='<%# Eval("Issuedby")%>' CommandName='Edit'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_Issuedby" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					           
					        </div>
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Name</label>
					            <asp:DropDownList ID="ddlItemName" runat="server" class="js-states form-control"></asp:DropDownList>
					             
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">From Date</label>
					            <asp:TextBox ID="txtFromDate" MaxLength="20" class="form-control date-picker" runat="server" AutoComplete="off" ></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderFromDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtFromDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">To Date</label>
					            <asp:TextBox ID="txtToDate" class="form-control date-picker" runat="server" AutoComplete="off" ></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderToDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtToDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					    </div>
					</div>
					
					<div class="clearfix"></div>
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnReports" class="btn btn-success"  runat="server" 
                            Text="Report" onclick="btnReports_Click"  />
                        
                        <asp:Button ID="btnClear" class="btn btn-success" runat="server" Text="Clear" onclick="btnClear_Click" 
                             />
                    </div>
                    <!-- Button end -->
                                      
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 
</asp:Content>