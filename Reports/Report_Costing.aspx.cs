﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_Costing : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string CostCenterName = ""; string DeptName = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Costing Details";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Inventory"));
            li.Attributes.Add("class", "droplink active open");

        }

        if (!IsPostBack)
        {
            Load_Data_Empty_Dept();
            Load_Data_Empty_CostCenterName();
        }
        Load_Data_Empty_ItemCode();
        
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }


    protected void btnCostCenterName_Click(object sender, EventArgs e)
    {
        //modalPop_CostCenterName.Show();
    }
    private void Load_Data_Empty_CostCenterName()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtCostCenterName.Items.Clear();
        query = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCostCenterName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["CostcenterCode"] = "-Select-";
        dr["CostcenterName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCostCenterName.DataTextField = "CostcenterName";
        txtCostCenterName.DataValueField = "CostcenterCode";
        txtCostCenterName.DataBind();






        //string query = "";
        //DataTable DT = new DataTable();


        //query = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_CostCenterName.DataSource = DT;
        //Repeater_CostCenterName.DataBind();
        //Repeater_CostCenterName.Visible = true;

    }

    protected void GridViewClick_CostCenterName(object sender, CommandEventArgs e)
    {
        //txtCostCenterCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtCostCenterName.Text = Convert.ToString(e.CommandName);


    }


    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtRequestStatus.SelectedValue = "0";
        txtDepartmentName.SelectedValue = "-Select-";
        //txtDeptCodeHide.Value = "";
        //txtCostCenterCodeHide.Value = "";
        txtCostCenterName.SelectedValue = "-Select-";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtItemCodeHide.Value = "";
        txtItemName.Text = "";

        Load_Data_Empty_Dept();
        Load_Data_Empty_CostCenterName();
        Load_Data_Empty_ItemCode();
       

    }
    protected void btnReports_Click(object sender, EventArgs e)
    {

        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtCostCenterName.SelectedItem.Text != "-Select-")
        {
            CostCenterName = txtCostCenterName.SelectedItem.Text;
        }
        else
        {
            CostCenterName = "";
        }

        if (txtRequestStatus.SelectedItem.Text != "-select-")
        {
            if (txtRequestStatus.SelectedItem.Text == "Department Wise")
            {
                
                RptName = "Department Wise Purchase and Issue Cost Compare Report";

                ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&CostCenterName=" + CostCenterName + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
            }
            else if (txtRequestStatus.SelectedItem.Text == "Financial Year")
            {
                RptName = "Department Wise Current Fin Year And Prev Fin Year Purchase And Issue Cost Report";

                ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&CostCenterName=" + CostCenterName + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
            }
            else if (txtRequestStatus.SelectedItem.Text == "Electrical Department")
            {
                RptName = "Electrical Department Item Purchase and Issue Cost Report";

                ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&CostCenterName=" + CostCenterName + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
            }
            else if (txtRequestStatus.SelectedItem.Text == "Lubricant Item")
            {
                RptName = "Lubricant Purchase and Issue Cost Details Report";

                ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&CostCenterName=" + CostCenterName + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
            }
            else if (txtRequestStatus.SelectedItem.Text == "Cost Center Wise")
            {
                RptName = "CostCenter Wise Purchase and Issue Cost Report";

                ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&CostCenterName=" + CostCenterName + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
            }
            

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Status');", true);
        }
        
    }



}
