﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Report_Blanket_PurOrd_Det.aspx.cs" Inherits="Reports_Report_Blanket_PurOrd_Det" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

    <script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

    <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Dept').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Dept').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable1').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable1').dataTable();               
            }
        });
    };
</script>



<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Supplier1').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Supplier1').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#BlnkPurOrdNo').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#BlnkPurOrdNo').dataTable();               
            }
        });
    };
</script>


<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
  
   $find('Dept_Close').hide();
   $find('Supp1_Close').hide();
   $find('Item_Close').hide();
   $find('BlnkPurOrdNo_Close').hide();
   $find('Item_Close1').hide();
      }
   } 
 </script>
<!--Ajax popup End-->





<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Blanket Purchase Order Details</li></h4> 
    </ol>
</div>

<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">        
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Blanket Purchase Order Details</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        
					       <div class="form-group col-md-4">
					            <label for="exampleInputName">Department</label>
					            
					            <asp:DropDownList ID="txtDepartmentName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					            
					            
					            <%--<asp:TextBox ID="txtDepartmentName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					             <asp:Button ID="btnDept" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnDept_Click"/>--%>
                    <%--<cc1:ModalPopupExtender ID="modalPop_Dept"  runat="server" PopupControlID="Panel1" TargetControlID="btnDept"
                       CancelControlID="BtnClear_Dept" BackgroundCssClass="modalBackground" BehaviorID="Dept_Close">
                    </cc1:ModalPopupExtender>--%>
                   
                   <%-- <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Department Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_Dept" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="Dept" class="display table">
                         <thead >
                         <tr>
                         <th>DeptCode</th>
                         <th>DeptName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("DeptCode")%></td>
                          <td><%# Eval("DeptName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_Dept" CommandArgument='<%# Eval("DeptCode")%>' CommandName='<%# Eval("DeptName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_Dept" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            <%--<asp:HiddenField ID="txtDeptCodeHide" runat="server" />--%>
					        </div>
					        
					      <div class="form-group col-md-4">
					            <label for="exampleInputName">Supplier</label>
					            <asp:TextBox ID="txtSupplierName" class="form-control" runat="server" style="float: left;width: 81%;"></asp:TextBox>
					            <asp:Button ID="btnSupp1" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnSupp1_Click"/>
					            
					            <cc1:ModalPopupExtender ID="modalPop_Supp1"  runat="server" PopupControlID="Panl1" TargetControlID="btnSupp1"
                       CancelControlID="BtnClear_Supp1" BackgroundCssClass="modalBackground" BehaviorID="Supp1_Close">
                    </cc1:ModalPopupExtender>
                              
                                <asp:Panel ID="Panl1" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Supplier Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_Supplier1" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="Supplier1" class="display table">
                         <thead >
                         <tr>
                         <th>SuppCode</th>
                         <th>SuppName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("SuppCode")%></td>
                          <td><%# Eval("SuppName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick" CommandArgument='<%# Eval("SuppCode")%>' CommandName='<%# Eval("SuppName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_Supp1" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>
					            
					            <asp:HiddenField ID="txtSuppCodehide" runat="server" />
					        </div>
					        
					      <div class="form-group col-md-4">
					            <label for="exampleInputName">Blanket.PO.No</label>
					            
					            <asp:DropDownList ID="txtBPONo" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					            
					            
					           <%-- <asp:TextBox ID="txtBPONo" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnBlnkPurOrdNo" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnBlnkPurOrdNo_Click"/>--%>
					            
					            <%--<cc1:ModalPopupExtender ID="modalPop_BlnkPurOrdNo"  runat="server" PopupControlID="Panel2" TargetControlID="btnBlnkPurOrdNo"
                       CancelControlID="BtnClear_BlnkPurOrdNo" BackgroundCssClass="modalBackground" BehaviorID="BlnkPurOrdNo_Close">
                    </cc1:ModalPopupExtender>--%>
                              
                                <%--<asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Blanket Purchase Order Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_BlnkPurOrdNo" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="BlnkPurOrdNo" class="display table">
                         <thead >
                         <tr>
                         <th>Blanket_PO_No</th>
                         <th>Blanket_PO_Date</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("Blanket_PO_No")%></td>
                          <td><%# Eval("Blanket_PO_Date")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditBlnkPurOrdNo" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_BlnkPurOrdNo" CommandArgument='<%# Eval("Blanket_PO_No")%>' CommandName='<%# Eval("Blanket_PO_Date")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_BlnkPurOrdNo" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            <%--<asp:HiddenField ID="txtBPONoDateHide" runat="server" />--%>
					        </div>
					       
					       </div>
					       </div>
					       
					  <div class="col-md-12">
					    <div class="row">  
					      <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Name</label>
					            <asp:TextBox ID="txtItemName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnItemCode" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnItemCode_Click"/>
                                
                    <cc1:ModalPopupExtender ID="modalPop_ItemCode"  runat="server" PopupControlID="Panel5" TargetControlID="btnItemCode"
                       CancelControlID="BtnItemClose" BackgroundCssClass="modalBackground" BehaviorID="Item_Close">
                    </cc1:ModalPopupExtender>
                   
                    <asp:Panel ID="Panel5" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Item Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_ItemCode" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="ItemTable" class="display table">
                         <thead >
                         <tr>
                         <th>ItemCode</th>
                         <th>ItemName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ItemCode")%></td>
                          <td><%# Eval("ItemName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEdit_ItemCode" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ItemCode" CommandArgument='<%# Eval("ItemCode")%>' CommandName='<%# Eval("ItemName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnItemClose" class="btn btn-rounded" runat="server" Text="Close" />
					     </div>
					     </asp:Panel>
					            
					            <asp:HiddenField ID="txtItemCodeHide" runat="server" />
					        </div>
					      			        
					      <div class="form-group col-md-2">
					            <label for="exampleInputName">From Date</label>
					            <asp:TextBox ID="txtFromDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderFromDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtFromDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					      <div class="form-group col-md-2">
					            <label for="exampleInputName">To Date</label>
					            <asp:TextBox ID="txtToDate" class="form-control date-picker" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderToDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtToDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					    </div>
					</div>
					
					<div class="clearfix"></div>
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnReports" class="btn btn-success"  runat="server" 
                            Text="Report" onclick="btnReports_Click"    />
                        <asp:Button ID="btnInvcFormat" class="btn btn-success"  runat="server" 
                            Text="Invoice Format" onclick="btnInvcFormat_Click" />
                        <asp:Button ID="btnClear" class="btn btn-success" runat="server" Text="Clear" onclick="btnClear_Click" 
                             />
                    </div>
                    <!-- Button end -->
                     
                     <div class="clearfix"></div>
					<div class="form-group row"></div>	
					   <br />
					      <br />
                     <div class="col-md-12">
					    <div class="row">
					     <div class="form-group row">							 
						
					
						<label for="input-Default" class="col-sm-1 control-label">Status</label>
						<div class="col-sm-2">
                            <asp:DropDownList ID="txtRequestStatus" runat="server" class="js-states form-control" AutoPostBack="true">
                            
                                    <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Rejected List"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Pending List"></asp:ListItem>
                                    
                            </asp:DropDownList>
                            
						</div>
						
						<label for="input-Default" class="col-sm-1 control-label">FromDate</label>
						<div class="col-sm-2">
						<asp:TextBox ID="txtFrmDate" MaxLength="20" class="form-control date-picker" runat="server" AutoComplete="off"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtFrmDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
                         </div>
                         
                         <label for="input-Default" class="col-sm-1 control-label">ToDate</label>
						<div class="col-sm-2">
						<asp:TextBox ID="txtTDate" MaxLength="20" class="form-control date-picker" runat="server" AutoComplete="off"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
                         </div>
                      
						<div class="form-group col-md-1">
					         
					            <asp:Button ID="btnView" Width="50" Height="30" class="btn-success"  
                                    runat="server" Text="View" onclick="btnView_Click"/>
					        </div>
					        <div class="form-group col-md-1">
					         
					            <asp:Button ID="btn_Clear" Width="50" Height="30" class="btn-success"  
                                    runat="server" Text="Clear" onclick="btn_Clear_Click"/>
					        </div>
					        
                     </div>
                     
                     </div>
                     </div>
                     
                     
                      <div class="clearfix"></div>				    
				    <div class="timeline-options">
				        <h4 class="panel-title">Blanket Scheduled Details</h4>
				    </div>
				    <div class="col-md-12">
					    <div class="row"> 
					  <div class="form-group col-md-4">   
					    <label for="exampleInputName">Item Name</label>
					            <asp:TextBox ID="txtItemName1" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnItemCode1" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnItemCode1_Click"/>
                                
                    <cc1:ModalPopupExtender ID="modalPop_ItemCode1"  runat="server" PopupControlID="Panel3" TargetControlID="btnItemCode1"
                       CancelControlID="BtnItemClose1" BackgroundCssClass="modalBackground" BehaviorID="Item_Close1">
                    </cc1:ModalPopupExtender>
                   
                    <asp:Panel ID="Panel3" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Item Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_ItemCode1" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="ItemTable1" class="display table">
                         <thead >
                         <tr>
                         <th>ItemCode</th>
                         <th>ItemName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ItemCode")%></td>
                          <td><%# Eval("ItemName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEdit_ItemCode1" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ItemCode1" CommandArgument='<%# Eval("ItemCode")%>' CommandName='<%# Eval("ItemName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnItemClose1" class="btn btn-rounded" runat="server" Text="Close" />
					     </div>
					     </asp:Panel>
					            
					            <asp:HiddenField ID="txtItemCodeHide1" runat="server" />
					        </div>
					     <div class="form-group col-md-3">
					     </br>
					     
					     <asp:Button ID="btnScheduleReport" class="btn btn-success"  runat="server" 
                            Text="ScheduleReport" onclick="btnScheduleReport_Click"    />
					     
					     </div>
					      </div>
					 </div>
				    
				    
                     
                                      
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
             <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
		    <div class="col-md-2"></div>
		   
		  
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->


</asp:Content>

