﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_GatePass_Non_Returnable : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string GPOutNo = ""; string Issuedby = ""; string DeptName = "";
    string WareHouse = "";

    string ItemName = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Page.Title = "ERP Stores Module :: GatePass Non-Returnable Report";
        if (!IsPostBack)
        {
            Load_Data_Empty_WareHouse();
            Load_Data_Empty_Dept();
            Load_Data_Empty_GPOutNo();
            Load_Data_Empty_Issuedby();
        }
        
        Load_Data_Empty_ItemCode();
        
        
       
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtWarehouseName.SelectedValue = "-Select-";
       // txtWarehouseCodeHide.Value = "";
        txtDepartmentName.SelectedValue = "-Select-";
        //txtDeptCodeHide.Value = "";
        txtGPOutNo.SelectedValue = "-Select-";
        //txtGPOutDateHide.Value = "";
        txtIssuedby.SelectedValue = "-Select-";
        ddlItemName.SelectedItem.Text = "-Select-";
        //txtItemCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }
    protected void btnReports_Click(object sender, EventArgs e)
    {
        RptName = "GatePass NonReturnable Details Report";

        if (txtGPOutNo.SelectedItem.Text != "-Select-")
        {
            GPOutNo = txtGPOutNo.SelectedItem.Text;
        }
        else
        {
            GPOutNo = "";
        }
        if (txtIssuedby.SelectedItem.Text != "-Select-")
        {
            Issuedby = txtIssuedby.SelectedItem.Text;
        }
        else
        {
            Issuedby = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }

        if (ddlItemName.SelectedItem.Text != "-Select-")
        {
            ItemName = ddlItemName.SelectedItem.Text;
        }
        else
        {
            ItemName = "";
        }


        if (txtFromDate.Text != "" || txtToDate.Text != "")
        {
            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if(FDate<TDate)
            {
                ResponseHelper.Redirect("ReportDisplay.aspx?WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&GPOutNo=" + GPOutNo + "&GPOutIssBy=" + Issuedby + "&ItemName=" + ItemName + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Give Details Correctly..');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Give FromDate and Todate..');", true);
        }
        
    }


    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        //modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();
    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtWarehouseName.Text = Convert.ToString(e.CommandName);

    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();


    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        //modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlItemName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";

        DT.Rows.InsertAt(dr,0);

        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataValueField = "ItemCode";

        ddlItemName.DataBind();
    }

    //protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    //{
    //    txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
    //    ddlItemName.SelectedItem.Text = Convert.ToString(e.CommandName);
    //}

    protected void btnGPOutNo_Click(object sender, EventArgs e)
    {
        //modalPop_GPOutNo.Show();
    }
    private void Load_Data_Empty_GPOutNo()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtGPOutNo.Items.Clear();
        query = "Select GP_Out_No,GP_Out_Date from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And GP_Type='2' And GP_Out_Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtGPOutNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["GP_Out_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtGPOutNo.DataTextField = "GP_Out_No";

        txtGPOutNo.DataBind();



        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select GP_Out_No,GP_Out_Date from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And GP_Type='2' And GP_Out_Status='1'";
        //DT = objdata.RptEmployeeMultipleDetails(query);
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_GPOutNo.DataSource = DT;
        //Repeater_GPOutNo.DataBind();
        //Repeater_GPOutNo.Visible = true;

    }

    protected void GridViewClick_GPOutNo(object sender, CommandEventArgs e)
    {
        txtGPOutNo.Text = Convert.ToString(e.CommandArgument);
        //txtGPOutDateHide.Value = Convert.ToString(e.CommandName);
    }

    protected void btnIssuedby_Click(object sender, EventArgs e)
    {
        //modalPop_Issuedby.Show();
    }
    private void Load_Data_Empty_Issuedby()
    {



        string query = "";
        DataTable Main_DT = new DataTable();

        txtIssuedby.Items.Clear();
        query = "Select Issuedby from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And GP_Type='2' And GP_Out_Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtIssuedby.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Issuedby"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtIssuedby.DataTextField = "Issuedby";

        txtIssuedby.DataBind();



        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Issuedby from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And GP_Type='2' And GP_Out_Status='1'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_Issuedby.DataSource = DT;
        //Repeater_Issuedby.DataBind();
        //Repeater_Issuedby.Visible = true;

    }

    protected void GridViewClick_Issuedby(object sender, CommandEventArgs e)
    {
        txtIssuedby.Text = Convert.ToString(e.CommandArgument);

    }


}
