﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Reports_Report_Supplier_Payment : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string RptName = ""; string TransNo = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        Page.Title = "ERP Stores Module :: Supplier Payment Details";
        if (!IsPostBack)
        {
            Load_Data_Empty_GenPurOrdNo();
        }
        Load_Data_Empty_Supp1();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtSupplierName.Text = "";
        txtSuppCodehide.Value = "";
        txtTransNo.SelectedValue = "-Select-";
        //txtTrans_Date.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }

    protected void btnSupplierBalance_Click(object sender, EventArgs e)
    {
        RptName = "Supplier Payment Details Report";
        string Button_Name = "All Supplier Balance";

        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?SupplierName=" + txtSupplierName.Text + "&SuppCode=" + txtSuppCodehide.Value + "&PayTransNo=" + TransNo + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + Button_Name + "&RptName=" + RptName, "_blank", "");
    }
    protected void btnReceipt_Click(object sender, EventArgs e)
    {

        RptName = "Supplier Payment Details Report";
        string Button_Name = "Supplier Payment Receipt";

        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }


        ResponseHelper.Redirect("ReportDisplay.aspx?SupplierName=" + txtSupplierName.Text + "&SuppCode=" + txtSuppCodehide.Value + "&PayTransNo=" + TransNo + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + Button_Name + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnMonthly_Payments_Click(object sender, EventArgs e)
    {

        RptName = "ALL Supplier Monthly Payment Details Report";

        ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnSupplier_Ledger_Rpt_Click(object sender, EventArgs e)
    {
        bool Error = false;
        if (txtSupplierName.Text == "")
        {
            Error = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Supplier Name...');", true);
        }
        if (!Error)
        {
            RptName = "Single Supplier Ledger Report Details Report";

            ResponseHelper.Redirect("ReportDisplay.aspx?SupplierName=" + txtSupplierName.Text + "&SuppCode=" + txtSuppCodehide.Value + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
        }
    }

    protected void btnspecificSupplier_Click(object sender, EventArgs e)
    {

        RptName = "Supplier Payment Details Report";
        string Button_Name = "Singel Supplier Payment";


        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?SupplierName=" + txtSupplierName.Text + "&SuppCode=" + txtSuppCodehide.Value + "&PayTransNo=" + TransNo + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + Button_Name + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnSupp1_Click(object sender, EventArgs e)
    {
        modalPop_Supp1.Show();
    }
    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier1.DataSource = DT;
        Repeater_Supplier1.DataBind();
        Repeater_Supplier1.Visible = true;

    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
        txtSupplierName.Text = Convert.ToString(e.CommandName);


    }

    protected void btnTransNo_Click(object sender, EventArgs e)
    {
        //modalPop_GenPurOrdNo.Show();
    }
    private void Load_Data_Empty_GenPurOrdNo()
    {
        string query = "";
        DataTable Main_DT = new DataTable();


        txtTransNo.Items.Clear();
        query = "Select Trans_No,Trans_Date,Supp_Name from Unplanned_Receipt_Supp_Payment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtTransNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Trans_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtTransNo.DataTextField = "Trans_No";

        txtTransNo.DataBind();


        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Trans_No,Trans_Date,Supp_Name from Unplanned_Receipt_Supp_Payment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_GenPurOrdNo.DataSource = DT;
        //Repeater_GenPurOrdNo.DataBind();
        //Repeater_GenPurOrdNo.Visible = true;

    }

    protected void GridViewClick_GenPurOrdNo(object sender, CommandEventArgs e)
    {
        txtTransNo.Text = Convert.ToString(e.CommandArgument);
        //txtTrans_Date.Value = Convert.ToString(e.CommandName);

    }
}
