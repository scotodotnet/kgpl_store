﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_General_Receipt : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string ButtonName = "";
    string WareHouse = ""; string UPRecNo = "";
    string CostCenterName = "";
    string ItemName = "";
    string Supplier = "";



    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        Page.Title = "ERP Stores Module :: UnPlanned Receipt Details Report";

        if (!IsPostBack)
        { 
            Load_Tax_Percentage();
            Load_Data_Empty_WareHouse();
            Load_Data_Empty_UPRecNo();
            Load_Data_Empty_CostCenterName();
            Load_Data_Empty_Dept();
            Load_Data_Empty_Supp1();
            Load_Data_Empty_ItemCode();
        }
        
        
        
    }

    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();
    }

    private void Load_Tax_Percentage()
    {
        DataTable DT_Tax = new DataTable();
        string query = "Select Distinct TaxPer from Unplanned_Receipt_Main_Sub where Ccode='" + SessionCcode + "'";
        query = query + " And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "'";
        query = query + " And TaxPer <> '0.0' And TaxPer <> '0'";
        DT_Tax = objdata.RptEmployeeMultipleDetails(query);
        txtTaxPercent.Items.Clear();
        
        if (DT_Tax.Rows.Count != 0)
        {
            for (int i = 0; i < DT_Tax.Rows.Count; i++)
            {
                txtTaxPercent.Items.Add(DT_Tax.Rows[i]["TaxPer"].ToString());
            }
        }

    }

    private void Load_Data_Empty_CostCenterName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtCostCenterName.Items.Clear();
        query = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCostCenterName.DataSource = Main_DT;

        DataRow dr = Main_DT.NewRow();
        dr["CostcenterCode"] = "-Select-";
        dr["CostcenterName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCostCenterName.DataTextField = "CostcenterName";
        txtCostCenterName.DataValueField = "CostcenterCode";
        txtCostCenterName.DataBind();


    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtWarehouseName.SelectedValue = "-Select-";
        //txtWarehouseCodeHide.Value = "";
        ddlSupplier.SelectedItem.Text = "";
        //txtSuppCodehide.Value = "";
        ddlItemName.SelectedItem.Text = "";
        //txtItemCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtUPRNo.SelectedValue = "-Select-";
        txtDepartmentName.SelectedValue = "-Select-";
       // txtUPRDateHide.Value = "";
        Load_Tax_Percentage();
        txtCostCenterName.SelectedValue = "-Select-";
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        ButtonName = "Details Report";
        RptName = "General Receipt Details Report";

        if (txtFromDate.Text.ToString() != "" || txtToDate.Text.ToString() != "")
        {
            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if(FDate<TDate)
            {
                ResponseHelper.Redirect("ReportDisplay.aspx?CostCenterName=" + txtCostCenterName.SelectedItem.Text + "&WareHouse=" + txtWarehouseName.SelectedItem.Text + "&SupplierName=" + ddlSupplier.SelectedItem.Text + "&UPRNumber=" + txtUPRNo.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter From Date And To Date..');", true);
        }

        
   
    }

    protected void btnSlip_Click(object sender, EventArgs e)
    {
        ButtonName = "Issue Slip";

        RptName = "General Receipt Details Report";

        if (txtUPRNo.SelectedItem.Text !="-Select-")
        {
            ResponseHelper.Redirect("ReportDisplay.aspx?CostCenterName=" + txtCostCenterName.SelectedItem.Text + "&WareHouse=" + txtWarehouseName.SelectedItem.Text + "&SupplierName=" + ddlSupplier.SelectedItem.Text + "&UPRNumber=" + txtUPRNo.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Select UnPlaned Receipt No..');", true);
        }

    }

    protected void btnItemWiseReport_Click(object sender, EventArgs e)
    {
        RptName = "General Receipt Purchase Item Report";

        if (txtFromDate.Text != "" || txtToDate.Text != "")
        {
            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if (FDate < TDate)
            {
                ResponseHelper.Redirect("ReportDisplay.aspx?CostCenterName=" + txtCostCenterName.SelectedItem.Text + "&WareHouse=" + txtWarehouseName.SelectedItem.Text + "&SupplierName=" + ddlSupplier.SelectedItem.Text + "&UPRNumber=" + txtUPRNo.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter FromDate And ToDate..');", true);
        }
    }

    protected void btnDeptWiseReport_Click(object sender, EventArgs e)
    {
        ButtonName = "Department Purchase Item Report";

        RptName = "General Receipt Department Purchase";

        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;

        }
        else
        {
            WareHouse = "";
        }

        if (txtUPRNo.SelectedItem.Text != "-Select-")
        {
            UPRecNo = txtUPRNo.SelectedItem.Text;

        }
        else
        {
            UPRecNo = "";
        }

        if (ddlSupplier.SelectedItem.Text != "-Select-")
        {
            Supplier = ddlSupplier.SelectedItem.Text;

        }
        else
        {
            Supplier = "";
        }

        if (txtCostCenterName.SelectedItem.Text != "-Select-")
        {
            CostCenterName = txtCostCenterName.SelectedItem.Text;

        }
        else
        {
            CostCenterName = "";
        }

        if (ddlItemName.SelectedItem.Text != "-Select-")
        {
            ItemName = ddlItemName.SelectedItem.Text;

        }
        else
        {
            ItemName = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?CostCenterName=" + CostCenterName + "&WareHouse=" + WareHouse + "&SupplierName=" + Supplier + "&UPRNumber=" + UPRecNo + "&ItemName=" + ItemName + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");

    }

    protected void BtnTaxReport_Click(object sender, EventArgs e)
    {
        ButtonName = "Tax Purchase Item Report";

        RptName = "General Receipt Tax Purchase";

        if (txtFromDate.Text != "" || txtToDate.Text != "")
        {
            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if (FDate < TDate)
            {
                ResponseHelper.Redirect("ReportDisplay.aspx?CostCenterName=" + txtCostCenterName.SelectedItem.Text + "&WareHouse=" + txtWarehouseName.SelectedItem.Text + "&SupplierName=" + ddlSupplier.SelectedItem.Text + "&UPRNumber=" + txtUPRNo.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&Tax_Percent=" + txtTaxPercent.Text + "&RptName=" + RptName, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter FromDate And ToDate..');", true);
        }
    }

    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        //modalPop_WareHouse.Show();
    }

    private void Load_Data_Empty_WareHouse()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();
    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtWarehouseName.Text = Convert.ToString(e.CommandName);

    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        //modalPop_ItemCode.Show();
    }

    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        //DT.Columns.Add("ItemCode");
        //DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlItemName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataValueField = "ItemCode";

        ddlItemName.DataBind();

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        //txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        //ddlItemName.SelectedItem.Text = Convert.ToString(e.CommandName);
    }

    protected void btnSupp1_Click(object sender, EventArgs e)
    {
        //modalPop_Supp1.Show();
    }

    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        //DT.Columns.Add("SuppCode");
        //DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlSupplier.DataSource = DT;
        DataRow dr = DT.NewRow();

        dr["SuppCode"] = "-Select-";
        dr["SuppName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlSupplier.DataTextField = "SuppName";
        ddlSupplier.DataValueField = "SuppCode";

        ddlSupplier.DataBind();
        //Repeater_Supplier1.Visible = true;

    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        //txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
        //ddlSupplier.SelectedItem.Text = Convert.ToString(e.CommandName);


    }

    protected void btnUPRecNo_Click(object sender, EventArgs e)
    {
        //modalPop_UPRecNo.Show();
    }

    private void Load_Data_Empty_UPRecNo()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        
        txtUPRNo.Items.Clear();
        query = "Select UnPlan_Recp_No,UnPlan_Recp_Date from Unplanned_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And UnPlan_PO_Receipt_Status='1' Order by UnPlan_Recp_No Asc"; 
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtUPRNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["UnPlan_Recp_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtUPRNo.DataTextField = "UnPlan_Recp_No";
        
        txtUPRNo.DataBind();



        //string query = "";
        //DataTable DT = new DataTable();
        //query = "Select UnPlan_Recp_No,UnPlan_Recp_Date from Unplanned_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And UnPlan_PO_Receipt_Status='1'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_UPRecNo.DataSource = DT;
        //Repeater_UPRecNo.DataBind();
        //Repeater_UPRecNo.Visible = true;

    }

    protected void GridViewClick_UPRecNo(object sender, CommandEventArgs e)
    {
        txtUPRNo.Text = Convert.ToString(e.CommandArgument);
        //txtUPRDateHide.Value = Convert.ToString(e.CommandName);
    }

    protected void btnApp_Cancel_Click(object sender, EventArgs e)
    {
        bool Errflag = false;
        if (txtUPRNo.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('UnPlan Receipt No Not Select!........');", true);
            Errflag = true;
        }
        if (!Errflag)
        {
            DataTable da_MT_Val = new DataTable(); 
            string SSQL = "";
            DataTable da_Update = new DataTable();
            SSQL = "update dbo.Unplanned_Receipt_Main set UnPlan_PO_Receipt_Status='0' where UnPlan_Recp_No='" + txtUPRNo.SelectedItem.Text + "' ";
            SSQL = SSQL + " and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            da_Update = objdata.RptEmployeeMultipleDetails(SSQL);

            //Stock Process

            SSQL = "delete from Stock_Transaction_Ledger where Trans_No='" + txtUPRNo.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            da_Update = objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "delete from Stock_Ledger_All where Trans_No='" + txtUPRNo.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            da_Update = objdata.RptEmployeeMultipleDetails(SSQL);


            SSQL = "select * from Unplanned_Receipt_Main_Sub where UnPlan_Recp_No='" + txtUPRNo.SelectedItem.Text + "' ";
            SSQL = SSQL + " and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            da_MT_Val = objdata.RptEmployeeMultipleDetails(SSQL);
            if (da_MT_Val.Rows.Count != 0)
            {
                for (int i = 0; i < da_MT_Val.Rows.Count; i++)
                {
                    //Get Stock Qty
                    string query = "";
                    DataTable dt_check = new DataTable();
                    string cls_Stock_Qty = "";
                    string cls_Stock_Value = "";
                    query = "Select isnull((sum(Add_Qty)-sum(Minus_Qty)),0) as Stock_Qty,isnull((sum(Add_Value)-sum(Minus_Value)),0) as Stock_Value";
                    query = query + " from Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                    query = query + " And ItemCode='" + da_MT_Val.Rows[i]["itemCode"].ToString() + "'";
                    dt_check = objdata.RptEmployeeMultipleDetails(query);
                    if (dt_check.Rows.Count != 0)
                    {
                        cls_Stock_Qty = dt_check.Rows[0]["Stock_Qty"].ToString();
                        cls_Stock_Value = dt_check.Rows[0]["Stock_Value"].ToString();
                    }
                    else
                    {
                        cls_Stock_Qty = "0";
                        cls_Stock_Value = "0";
                    }

                    //Check Current Stock item
                    query = "Select * from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    query = query + " And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + da_MT_Val.Rows[i]["itemCode"].ToString() + "'";
                    dt_check = objdata.RptEmployeeMultipleDetails(query);
                    if (dt_check.Rows.Count != 0)
                    {
                        query = "Delete from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        query = query + " And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + da_MT_Val.Rows[i]["itemCode"].ToString() + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                    }
                    //Insert Current Stock
                    query = "Insert Into Stock_Current_All(Ccode,Lcode,FinYearCode,FinYearVal,ItemCode,ItemName,Stock_Qty,Stock_Value,DeptCode,DeptName,";
                    query = query + " WarehouseCode,WarehouseName,ZoneName,BinName,UserID,UserName) Values('" + SessionCcode + "',";
                    query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + da_MT_Val.Rows[i]["itemCode"].ToString() + "',";
                    query = query + " '" + da_MT_Val.Rows[i]["itemName"].ToString() + "','" + cls_Stock_Qty + "','" + cls_Stock_Value + "',";
                    query = query + " '" + da_MT_Val.Rows[i]["DeptCode"].ToString() + "','" + da_MT_Val.Rows[i]["DeptName"].ToString() + "',";
                    query = query + " '" + da_MT_Val.Rows[i]["Warehousecode"].ToString() + "','" + da_MT_Val.Rows[i]["warehouseName"].ToString() + "',";
                    query = query + " '" + da_MT_Val.Rows[i]["ZoneName"].ToString() + "','" + da_MT_Val.Rows[i]["BinName"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(query);
                }
            }


            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert(' Updated Successfully......');", true);
        }

    }

    protected void btnSamplereport_Click(object sender, EventArgs e)
    {
       

        RptName = "Sample Purchase Report";


        if (txtCostCenterName.SelectedItem.Text != "-Select-")
        {
            CostCenterName = txtWarehouseName.SelectedItem.Text;

        }
        else
        {
            CostCenterName = "";
        }

        if (txtUPRNo.SelectedItem.Text != "-Select-")
        {
            UPRecNo = txtUPRNo.SelectedItem.Text;

        }
        else
        {
            UPRecNo = "";
        }

        string ItemName = ddlItemName.SelectedItem.Text;
        ItemName = ItemName.Replace("\"", "Doublequit");


        ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + txtDepartmentName.SelectedItem.Text + "&WareHouse=" + txtWarehouseName.SelectedItem.Text + "&CostCenterName=" + txtCostCenterName.SelectedItem.Text + "&SupplierName=" + ddlSupplier.SelectedItem.Text + "&UPRNumber=" + UPRecNo + "&ItemName=" + ItemName + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
    }
    protected void BtnGSTReport_Click(object sender, EventArgs e)
    {
        ButtonName = "GST Purchase Item Report";

        RptName = "General Receipt GST Purchase";

        if (txtFromDate.Text != "" || txtToDate.Text != "")
        {
            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if (FDate < TDate)
            {
                ResponseHelper.Redirect("ReportDisplay.aspx?WareHouse=" + txtWarehouseName.SelectedItem.Text + "&CostCenterName=" + txtCostCenterName.SelectedItem.Text + "&SupplierName=" + ddlSupplier.SelectedItem.Text + "&UPRNumber=" + txtUPRNo.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter FromDate And ToDate..');", true);
        }


        

    }

    protected void btnCmbinRpt_Click(object sender, EventArgs e)
    {

        RptName = "Combain Purchase Report";
       
        if (txtFromDate.Text != "" || txtToDate.Text != "")
        {
            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if (FDate < TDate)
            {

                ResponseHelper.Redirect("ReportDisplay.aspx?WareHouse=" + txtWarehouseName.SelectedItem.Text + "&CostCenterName=" + txtCostCenterName.SelectedItem.Text + "&SupplierName=" + ddlSupplier.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&DeptName=" + txtDepartmentName.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter From Date And To Date..');", true);
        }
    }
}
