﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;



public partial class Reports_Report_Material_Issue_Return : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string ButtonName = "";
    string MatIssRetNo = ""; string Retby = ""; string DeptName = "";
    string WareHouse = "";
    string ItemName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

       Page.Title = "ERP Stores Module :: Material Issue Return Report";
       if (!IsPostBack)
       {
           Load_Data_Empty_WareHouse();
           Load_Data_Empty_Dept();
           Load_Data_Empty_MatIssRetNo();
           Load_Data_Empty_ReturnedBy();
       }
        
        Load_Data_Empty_ItemCode();
        
        
    }
    protected void btnSlipReports_Click(object sender, EventArgs e)
    {
        RptName = "Material Issue Return Report";
        ButtonName = "Issue Return Slip";

        if (txtMatIssRetNo.SelectedItem.Text != "-Select-")
        {
            MatIssRetNo = txtMatIssRetNo.SelectedItem.Text;
        }
        else
        {
            MatIssRetNo = "";
        }
        if (txtRetby.SelectedItem.Text != "-Select-")
        {
            Retby = txtRetby.SelectedItem.Text;
        }
        else
        {
            Retby = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }

        if (ddlItemName.SelectedItem.Text != "-Select-")
        {
            ItemName = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            ItemName = "";
        }


        ResponseHelper.Redirect("ReportDisplay.aspx?WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&MatIssRetNo=" + MatIssRetNo + "&MatRetBy=" + Retby + "&ItemName=" + ItemName + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
    }
    protected void btnDetReports_Click(object sender, EventArgs e)
    {
        RptName = "Material Issue Return Report";
        ButtonName = "Issue Return Details";

        if (txtMatIssRetNo.SelectedItem.Text != "-Select-")
        {
            MatIssRetNo = txtMatIssRetNo.SelectedItem.Text;
        }
        else
        {
            MatIssRetNo = "";
        }
        if (txtRetby.SelectedItem.Text != "-Select-")
        {
            Retby = txtRetby.SelectedItem.Text;
        }
        else
        {
            Retby = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }

        if (ddlItemName.SelectedItem.Text != "-Select-")
        {
            ItemName = ddlItemName.SelectedItem.Text;
        }
        else
        {
            ItemName = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?ItemName=" + ItemName + "&WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&MatIssRetNo=" + MatIssRetNo + "&MatRetBy=" + Retby + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtWarehouseName.SelectedValue = "-Select-";

        txtDepartmentName.SelectedValue = "-Select-";
        //txtDeptCodeHide.Value = "";
        txtMatIssRetNo.SelectedValue = "-Select-";
        //txtMatIssRetDateHide.Value = "";
        txtRetby.SelectedValue = "-Select-";
        ddlItemName.SelectedItem.Text = "-Select-";
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }


    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        //modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();

    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtWarehouseName.Text = Convert.ToString(e.CommandName);

    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        //modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlItemName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataValueField = "ItemCode";

        ddlItemName.DataBind();
    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        //txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        //ddlItemName.SelectedItem.Text = Convert.ToString(e.CommandName);
    }

    protected void btnMatIssRetNo_Click(object sender, EventArgs e)
    {
        //modalPop_MatIssRetNo.Show();
    }
    private void Load_Data_Empty_MatIssRetNo()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtMatIssRetNo.Items.Clear();
        query = "Select Mat_Issue_Return_No,Mat_Issue_Return_Date from Meterial_Issue_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And Mat_Issue_Return_Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtMatIssRetNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Mat_Issue_Return_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtMatIssRetNo.DataTextField = "Mat_Issue_Return_No";

        txtMatIssRetNo.DataBind();

        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Mat_Issue_Return_No,Mat_Issue_Return_Date from Meterial_Issue_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And Mat_Issue_Return_Status='1'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_MatIssRetNo.DataSource = DT;
        //Repeater_MatIssRetNo.DataBind();
        //Repeater_MatIssRetNo.Visible = true;

    }

    protected void GridViewClick_MatIssRetNo(object sender, CommandEventArgs e)
    {
        txtMatIssRetNo.Text = Convert.ToString(e.CommandArgument);
        //txtMatIssRetDateHide.Value = Convert.ToString(e.CommandName);
    }

    protected void btnReturnedBy_Click(object sender, EventArgs e)
    {
        //modalPop_ReturnedBy.Show();
    }
    private void Load_Data_Empty_ReturnedBy()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtRetby.Items.Clear();
        query = "Select Returnby from Meterial_Issue_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And Mat_Issue_Return_Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtRetby.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Returnby"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtRetby.DataTextField = "Returnby";

        txtRetby.DataBind();
        

    }

    protected void GridViewClick_ReturnedBy(object sender, CommandEventArgs e)
    {
        txtRetby.Text = Convert.ToString(e.CommandArgument);

    }

}
