﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;



public partial class Reports_Report_Material_Issue_Slip : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string ButtonName = "";
    string MatIssNo = ""; string Issuedby = ""; string DeptName = "";
    string WareHouse = "";
    string CostCenter = "";
    string ItemName = "";
    string OtherUnit = "";
    string ItemType = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string MatIssNo = ""; string Issuedby = ""; string DeptName = "";
        string WareHouse = "";
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        Page.Title = "ERP Stores Module :: Material Issue Report";

        if (!IsPostBack)
        {
            Load_Data_Empty_Dept();
            Load_Data_Empty_MatIssNo();
            Load_Data_Empty_Issuedby();
            Load_Data_Empty_WareHouse();
            Load_Data_Empty_CostCenterName();
            Load_Data_Empty_ItemCode();
            Load_Data_Empty_OtherUnit();
            Load_Data_Empty_ItemType();
        }
    }

    private void Load_Data_Empty_OtherUnit()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        ddlOtherUnit.Items.Clear();
        query = "Select OtherUnitCode,OtherUnitName from mstOtherUnit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        ddlOtherUnit.DataSource = Main_DT;

        DataRow dr = Main_DT.NewRow();
        dr["OtherUnitCode"] = "-Select-";
        dr["OtherUnitName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        ddlOtherUnit.DataTextField = "OtherUnitName";
        ddlOtherUnit.DataValueField = "OtherUnitCode";
        ddlOtherUnit.DataBind();
    }

    private void Load_Data_Empty_ItemType()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        ddlItemType.Items.Clear();
        query = "Select ItemTypeCode,ItemType from MstItemType where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        ddlItemType.DataSource = Main_DT;

        DataRow dr = Main_DT.NewRow();
        dr["ItemTypeCode"] = "-Select-";
        dr["ItemType"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        ddlItemType.DataTextField = "ItemType";
        ddlItemType.DataValueField = "ItemTypeCode";
        ddlItemType.DataBind();
    }

    private void Load_Data_Empty_CostCenterName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtCostCenterName.Items.Clear();
        query = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCostCenterName.DataSource = Main_DT;
   
        DataRow dr = Main_DT.NewRow();
        dr["CostcenterCode"] = "-Select-";
        dr["CostcenterName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCostCenterName.DataTextField = "CostcenterName";
        txtCostCenterName.DataValueField = "CostcenterCode";
        txtCostCenterName.DataBind();
       

    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtWarehouseName.SelectedValue = "-Select-";
        //txtWarehouseCodeHide.Value = "";
        txtDepartmentName.SelectedValue = "-Select-";
        //txtDeptCodeHide.Value = "";
        txtMatIssNo.SelectedValue = null;
        //txtMatIssDateHide.Value = "";
        txtIssuedby.SelectedValue = null;
        ddlItemName.SelectedItem.Text = "-Select-";
        //txtItemCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }
    protected void btnSlipReports_Click(object sender, EventArgs e)
    {
        RptName = "Material Issue Report";
        ButtonName = "Issue Slip";
        
        if (txtMatIssNo.SelectedItem.Text != "-Select-")
        {
            MatIssNo = txtMatIssNo.SelectedItem.Text;
        }
        else
        {
            MatIssNo="";
        }
        if (txtIssuedby.SelectedItem.Text != "-Select-")
        {
            Issuedby = txtIssuedby.SelectedItem.Text;
        }
        else
        {
            Issuedby = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }
        if (txtCostCenterName.SelectedItem.Text != "-Select-")
        {
            CostCenter = txtCostCenterName.SelectedItem.Text; 
        }
        else
        {
            CostCenter = "";
        }

        if (ddlItemName.SelectedItem.Text != "-Select-")
        {
            ItemName = txtCostCenterName.SelectedItem.Text;
        }
        else
        {
            ItemName = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?CostCenter=" + CostCenter + "&WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&MatIssNo=" + MatIssNo + "&MatIssBy=" + Issuedby + "&ItemName=" + ItemName + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnDetReports_Click(object sender, EventArgs e)
    {
        RptName = "Material Issue Report";
        ButtonName = "Details Report";
        
        if (txtMatIssNo.SelectedItem.Text != "-Select-")
        {
            MatIssNo = txtMatIssNo.SelectedItem.Text;
        }
        else
        {
            MatIssNo = "";
        }
        if (txtIssuedby.SelectedItem.Text != "-Select-")
        {
            Issuedby = txtIssuedby.SelectedItem.Text;
        }
        else
        {
            Issuedby = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }
        if (txtCostCenterName.SelectedItem.Text != "-Select-")
        {
            CostCenter = txtCostCenterName.SelectedItem.Text;
        }
        else
        {
            CostCenter = "";
        }

        if (ddlItemName.SelectedItem.Text != "-Select-")
        {
            ItemName = ddlItemName.SelectedItem.Text;
        }
        else
        {
            ItemName = "";
        }

        if(ddlItemType.SelectedItem.Text != "-Select-")
        {
            ItemType = ddlItemType.SelectedItem.Text;
        }
        else
        {
            ItemType = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?CostCenter=" + CostCenter + "&WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&MatIssNo=" + MatIssNo + "&MatIssBy=" + Issuedby + "&ItemName=" + ItemName + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName + "&ItemType=" + ItemType, "_blank", "");
    }

    protected void btnItemWiseReport_Click(object sender, EventArgs e)
    {
        RptName = "Material Issue Item Report";
        ButtonName = "Issue Item";
        if (txtMatIssNo.SelectedItem.Text != "-Select-")
        {
            MatIssNo = txtMatIssNo.SelectedItem.Text;
        }
        else
        {
            MatIssNo="";
        }
        if (txtIssuedby.SelectedItem.Text != "-Select-")
        {
            Issuedby = txtIssuedby.SelectedItem.Text;
        }
        else
        {
            Issuedby = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }
        if (txtCostCenterName.SelectedItem.Text != "-Select-")
        {
            CostCenter = txtCostCenterName.SelectedItem.Text;
        }
        else
        {
            CostCenter = "";
        }

        if (ddlItemType.SelectedItem.Text != "-Select-")
        {
            ItemType = ddlItemType.SelectedItem.Text;
        }
        else
        {
            ItemType = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?CostCenter=" + CostCenter + "&WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&MatIssNo=" + MatIssNo + "&MatIssBy=" + Issuedby + "&ItemName=" + ddlItemName.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName + "&ItemType=" + ItemType, "_blank", "");
    }

    protected void btnDeptWiseReport_Click(object sender, EventArgs e)
    {
        RptName = "Material Issue Department Report";
        ButtonName = "Issue Department";
        if (txtMatIssNo.SelectedItem.Text != "-Select-")
        {
            MatIssNo = txtMatIssNo.SelectedItem.Text;
        }
        else
        {
            MatIssNo="";
        }
        if (txtIssuedby.SelectedItem.Text != "-Select-")
        {
            Issuedby = txtIssuedby.SelectedItem.Text;
        }
        else
        {
            Issuedby = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }
        if (txtCostCenterName.SelectedItem.Text != "-Select-")
        {
            CostCenter = txtCostCenterName.SelectedItem.Text;
        }
        else
        {
            CostCenter = "";
        }

        if (ddlItemType.SelectedItem.Text != "-Select-")
        {
            ItemType = ddlItemType.SelectedItem.Text;
        }
        else
        {
            ItemType = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?CostCenter=" + CostCenter + "&WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&MatIssNo=" + MatIssNo + "&MatIssBy=" + Issuedby + "&ItemName=" + ddlItemName.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName + "&ItemType=" + ItemType, "_blank", "");
    }

    protected void btnCostCenterWiseReport_Click(object sender, EventArgs e)
    {
        RptName = "Material Issue Costcenter Report";
        ButtonName = "Issue Costcenter";
        if (txtMatIssNo.SelectedItem.Text != "-Select-")
        {
            MatIssNo = txtMatIssNo.SelectedItem.Text;
        }
        else
        {
            MatIssNo="";
        }
        if (txtIssuedby.SelectedItem.Text != "-Select-")
        {
            Issuedby = txtIssuedby.SelectedItem.Text;
        }
        else
        {
            Issuedby = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }
        if (txtCostCenterName.SelectedItem.Text != "-Select-")
        {
            CostCenter = txtCostCenterName.SelectedItem.Text;
        }
        else
        {
            CostCenter = "";
        }

        if (ddlItemType.SelectedItem.Text != "-Select-")
        {
            ItemType = ddlItemType.SelectedItem.Text;
        }
        else
        {
            ItemType = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?CostCenter=" + CostCenter + "&WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&MatIssNo=" + MatIssNo + "&MatIssBy=" + Issuedby + "&ItemName=" + ddlItemName.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName + "&ItemType=" + ItemType, "_blank", "");
    }

    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        //modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();
    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        //txtWarehouseName.Text = Convert.ToString(e.CommandName);
       
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();
    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnMatIssNo_Click(object sender, EventArgs e)
    {
        //modalPop_MatIssNo.Show();
    }
    private void Load_Data_Empty_MatIssNo()
    {

        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Mat_Issue_No,Mat_Issue_Date from Meterial_Issue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And Mat_Issue_Status='1'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_MatIssNo.DataSource = DT;
        //Repeater_MatIssNo.DataBind();
        //Repeater_MatIssNo.Visible = true;

        
        string query = "";
        DataTable Main_DT = new DataTable();

        txtMatIssNo.Items.Clear();
        query = "Select Mat_Issue_No,Mat_Issue_Date from Meterial_Issue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And Mat_Issue_Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtMatIssNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Mat_Issue_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtMatIssNo.DataTextField = "Mat_Issue_No";

        txtMatIssNo.DataBind();

    }

    protected void GridViewClick_MatIssNo(object sender, CommandEventArgs e)
    {
        txtMatIssNo.Text = Convert.ToString(e.CommandArgument);
        //txtMatIssDateHide.Value = Convert.ToString(e.CommandName);
    }

    protected void btnIssuedby_Click(object sender, EventArgs e)
    {
        //modalPop_Issuedby.Show();
    }
    private void Load_Data_Empty_Issuedby()
    {

        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Issuedby from Meterial_Issue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And Mat_Issue_Status='1'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_Issuedby.DataSource = DT;
        //Repeater_Issuedby.DataBind();
        //Repeater_Issuedby.Visible = true;


        string query = "";
        DataTable Main_DT = new DataTable();

        txtIssuedby.Items.Clear();
        query = "Select Issuedby from Meterial_Issue_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And Mat_Issue_Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtIssuedby.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Issuedby"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtIssuedby.DataTextField = "Issuedby";

        txtIssuedby.DataBind();

    }

    protected void GridViewClick_Issuedby(object sender, CommandEventArgs e)
    {
        txtIssuedby.Text = Convert.ToString(e.CommandArgument);
        
    }

    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlItemName.DataSource = DT;

        DataRow dr= DT.NewRow();

        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";

        DT.Rows.InsertAt(dr,0);

        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataValueField = "ItemCode";

        ddlItemName.DataBind();
    }

    protected void btnApp_Cancel_Click(object sender, EventArgs e)
    {
        bool Errflag = false;
        if (txtMatIssNo.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(),"Script","SaveMsgAlert('Material Issue No Not Select!........');", true);
            Errflag = true;
        }
        if (!Errflag)
        {
            DataTable da_MT_Val = new DataTable(); 
            string SSQL = "";
            DataTable da_Update = new DataTable(); 
            SSQL = "update dbo.Meterial_Issue_Main set Mat_Issue_Status='0' where Mat_Issue_No='" + txtMatIssNo.SelectedItem.Text + "' ";
            SSQL = SSQL + " and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            da_Update = objdata.RptEmployeeMultipleDetails(SSQL);

            //Stock Process

            SSQL = "delete from Stock_Transaction_Ledger where Trans_No='" + txtMatIssNo.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            da_Update = objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "delete from Stock_Ledger_All where Trans_No='" + txtMatIssNo.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            da_Update = objdata.RptEmployeeMultipleDetails(SSQL);


            SSQL = "select * from Meterial_Issue_Main_Sub where Mat_Issue_No='" + txtMatIssNo.SelectedItem.Text + "' ";
            SSQL = SSQL + " and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            da_MT_Val = objdata.RptEmployeeMultipleDetails(SSQL);
            if (da_MT_Val.Rows.Count != 0)
            {
                for (int i = 0; i < da_MT_Val.Rows.Count; i++)
                {
                    //Get Stock Qty
                    string query = "";
                    DataTable dt_check = new DataTable();
                    string cls_Stock_Qty = "";
                    string cls_Stock_Value = "";
                    query = "Select isnull((sum(Add_Qty)-sum(Minus_Qty)),0) as Stock_Qty,isnull((sum(Add_Value)-sum(Minus_Value)),0) as Stock_Value";
                    query = query + " from Stock_Ledger_All where Ccode='" + SessionCcode  + "' And Lcode='" + SessionLcode  + "' And FinYearCode='" + SessionFinYearCode  + "'";
                    query = query + " And ItemCode='" + da_MT_Val.Rows[i]["itemCode"].ToString()   + "'";
                    dt_check = objdata.RptEmployeeMultipleDetails(query);
                    if (dt_check.Rows.Count != 0)
                    {
                        cls_Stock_Qty = dt_check.Rows[0]["Stock_Qty"].ToString();
                        cls_Stock_Value = dt_check.Rows[0]["Stock_Value"].ToString();
                    }
                    else
                    {
                        cls_Stock_Qty = "0";
                        cls_Stock_Value = "0";
                    }

                    //Check Current Stock item
                    query = "Select * from Stock_Current_All where Ccode='" + SessionCcode  + "' And Lcode='" + SessionLcode + "'";
                    query = query + " And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + da_MT_Val.Rows[i]["itemCode"].ToString() + "'";
                    dt_check = objdata.RptEmployeeMultipleDetails(query);
                    if (dt_check.Rows.Count != 0)
                    {
                        query = "Delete from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        query = query + " And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + da_MT_Val.Rows[i]["itemCode"].ToString() + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                    }
                    //Insert Current Stock
                    query = "Insert Into Stock_Current_All(Ccode,Lcode,FinYearCode,FinYearVal,ItemCode,ItemName,Stock_Qty,Stock_Value,DeptCode,DeptName,";
                    query = query + " WarehouseCode,WarehouseName,ZoneName,BinName,UserID,UserName) Values('" + SessionCcode + "',";
                    query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + da_MT_Val.Rows[i]["itemCode"].ToString() + "',";
                    query = query + " '" + da_MT_Val.Rows[i]["itemName"].ToString() + "','" + cls_Stock_Qty + "','" + cls_Stock_Value + "',";
                    query = query + " '" + da_MT_Val.Rows[i]["DeptCode"].ToString() + "','" + da_MT_Val.Rows[i]["DeptName"].ToString() + "',";
                    query = query + " '" + da_MT_Val.Rows[i]["Warehousecode"].ToString() + "','" + da_MT_Val.Rows[i]["warehouseName"].ToString() + "',";
                    query = query + " '" + da_MT_Val.Rows[i]["ZoneName"].ToString() + "','" + da_MT_Val.Rows[i]["BinName"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(query);
                }
            }

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert(' Updated Successfully......');", true);
        }
        
    }
    protected void btnSamplereport_Click(object sender, EventArgs e)
    {

        RptName = "Sample Material Issue Report";


        if (txtMatIssNo.SelectedItem.Text != "-Select-")
        {
            MatIssNo = txtMatIssNo.SelectedItem.Text;
        }
        else
        {
            MatIssNo = "";
        }
        if (txtIssuedby.SelectedItem.Text != "-Select-")
        {
            Issuedby = txtIssuedby.SelectedItem.Text;
        }
        else
        {
            Issuedby = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }
        if (txtCostCenterName.SelectedItem.Text != "-Select-")
        {
            CostCenter = txtCostCenterName.SelectedItem.Text;
        }
        else
        {
            CostCenter = "";
        }

        if (ddlItemName.SelectedItem.Text != "-Select-")
        {
            ItemName = ddlItemName.SelectedValue;
        }
        else
        {
            ItemName = "";
        }


        if (txtFromDate.Text != "" || txtToDate.Text != "")
        {
            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if (FDate < TDate)
            {
                ResponseHelper.Redirect("ReportDisplay.aspx?CostCenter=" + CostCenter + "&WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&MatIssNo=" + MatIssNo + "&MatIssBy=" + Issuedby + "&ItemName=" + ItemName + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Correct Date..');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select FromDate And ToDate..');", true);
        }

        
    }
    protected void btnSummary_Click(object sender, EventArgs e)
    {
        RptName = "Summary Material Issue Report";


        if (txtMatIssNo.SelectedItem.Text != "-Select-")
        {
            MatIssNo = txtMatIssNo.SelectedItem.Text;
        }
        else
        {
            MatIssNo = "";
        }
        if (txtIssuedby.SelectedItem.Text != "-Select-")
        {
            Issuedby = txtIssuedby.SelectedItem.Text;
        }
        else
        {
            Issuedby = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }
        if (txtCostCenterName.SelectedItem.Text != "")
        {
            CostCenter = txtCostCenterName.SelectedItem.Text;
        }
        else
        {
            CostCenter = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?CostCenter=" + CostCenter + "&WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&MatIssNo=" + MatIssNo + "&MatIssBy=" + Issuedby + "&ItemName=" + ddlItemName.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnOtherUnitRpt_Click(object sender, EventArgs e)
    {
        RptName = "OtherUnit Report Details";


        if (txtMatIssNo.SelectedItem.Text != "-Select-")
        {
            MatIssNo = txtMatIssNo.SelectedItem.Text;
        }
        else
        {
            MatIssNo = "";
        }

        if (txtIssuedby.SelectedItem.Text != "-Select-")
        {
            Issuedby = txtIssuedby.SelectedItem.Text;
        }
        else
        {
            Issuedby = "";
        }

        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }

        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }

        if (txtCostCenterName.SelectedItem.Text != "-Select-")
        {
            CostCenter = txtCostCenterName.SelectedItem.Text;
        }
        else
        {
            CostCenter = "";
        }

        if (ddlItemName.SelectedItem.Text != "-Select-")
        {
            ItemName = ddlItemName.SelectedValue;
        }
        else
        {
            ItemName = "";
        }

        if (ddlOtherUnit.SelectedItem.Text != "-Select-")
        {
            OtherUnit = ddlOtherUnit.SelectedItem.Text;
        }
        else
        {
            OtherUnit = "";
        }

        if (txtFromDate.Text != "" || txtToDate.Text != "")
        {
            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if (FDate < TDate)
            {
                ResponseHelper.Redirect("ReportDisplay.aspx?CostCenter=" + CostCenter + "&WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&MatIssNo=" + MatIssNo + "&MatIssBy=" + Issuedby + "&ItemName=" + ItemName + "&OtherUnit=" + OtherUnit + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Correct Date..');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select FromDate And ToDate..');", true);
        }
    }

    protected void btnItemType_Click(object sender, EventArgs e)
    {
        RptName = "ItemType Report Details";


        if (txtMatIssNo.SelectedItem.Text != "-Select-")
        {
            MatIssNo = txtMatIssNo.SelectedItem.Text;
        }
        else
        {
            MatIssNo = "";
        }

        if (txtIssuedby.SelectedItem.Text != "-Select-")
        {
            Issuedby = txtIssuedby.SelectedItem.Text;
        }
        else
        {
            Issuedby = "";
        }

        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }

        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }

        if (txtCostCenterName.SelectedItem.Text != "-Select-")
        {
            CostCenter = txtCostCenterName.SelectedItem.Text;
        }
        else
        {
            CostCenter = "";
        }

        if (ddlItemName.SelectedItem.Text != "-Select-")
        {
            ItemName = ddlItemName.SelectedValue;
        }
        else
        {
            ItemName = "";
        }

        if (ddlItemType.SelectedItem.Text != "-Select-")
        {
            ItemType = ddlItemType.SelectedItem.Text;
        }
        else
        {
            ItemType = "";
        }

        if (txtFromDate.Text != "" || txtToDate.Text != "")
        {
            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if (FDate < TDate)
            {
                ResponseHelper.Redirect("ReportDisplay.aspx?CostCenter=" + CostCenter + "&WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&MatIssNo=" + MatIssNo + "&MatIssBy=" + Issuedby + "&ItemName=" + ItemName + "&OtherUnit=" + OtherUnit + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName + "&ItemType=" + ItemType, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Correct Date..');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select FromDate And ToDate..');", true);
        }
    }
}
