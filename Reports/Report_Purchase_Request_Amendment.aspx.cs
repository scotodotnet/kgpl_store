﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_Purchase_Request_Amendment : System.Web.UI.Page
{
    string SSQL = "";
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    
    string RptName = "";
    string PurReqNo = ""; string AmendNo = ""; string Amendby = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Page.Title = "ERP Stores Module :: Purchase Request Amendment Report";

        if (!IsPostBack)
        {
            Load_Data_Empty_PurReqNo();
            Load_Data_Empty_AmendNo();
            Load_Data_Empty_Amendby();
        }
        Load_Data_Empty_ItemCode();
      
       
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtWarehouseName.Text = "";
        txtWarehouseCodeHide.Value = "";
        txtPurRqNo.SelectedValue = "-Select-";
        //txtPurRqDateHide.Value = "";
        txtAmendby.SelectedValue = "-Select-";
        txtItemName.Text = "";
        txtItemCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtAMNo.SelectedValue = "-Select-";
        //txtAMDateHide.Value = "";

    }

    protected void btnReports_Click(object sender, EventArgs e)
    {
        RptName = "Purchase Request Amendment Report";

        if (txtPurRqNo.SelectedItem.Text != "-Select-")
        {
            PurReqNo = txtPurRqNo.SelectedItem.Text;
        }
        else
        {
            PurReqNo = "";
        }
        if (txtAMNo.SelectedItem.Text != "-Select-")
        {
            AmendNo = txtAMNo.SelectedItem.Text;
        }
        else
        {
            AmendNo = "";
        }
        if (txtAmendby.SelectedItem.Text != "-Select-")
        {
            Amendby = txtAmendby.SelectedItem.Text;
        }
        else
        {
            Amendby = "";
        }



        ResponseHelper.Redirect("ReportDisplay.aspx?PurRqNo=" + PurReqNo + "&AmendBy=" + Amendby + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&AMNo=" + AmendNo + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }

    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnPurReqNo_Click(object sender, EventArgs e)
    {
       // modalPop_PurReqNo.Show();
    }

    private void Load_Data_Empty_PurReqNo()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtPurRqNo.Items.Clear();
        query = "Select Pur_Request_No,Pur_Request_Date from Pur_Request_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
       
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtPurRqNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Pur_Request_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtPurRqNo.DataTextField = "Pur_Request_No";

        txtPurRqNo.DataBind();



        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Pur_Request_No,Pur_Request_Date from Pur_Request_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
       
        //DT = objdata.RptEmployeeMultipleDetails(query);


        //Repeater_PurReqNo.DataSource = DT;
        //Repeater_PurReqNo.DataBind();
        //Repeater_PurReqNo.Visible = true;

    }

    protected void GridViewClick_PurReqNo(object sender, CommandEventArgs e)
    {
        txtPurRqNo.Text = Convert.ToString(e.CommandArgument);
      // txtPurRqDateHide.Value = Convert.ToString(e.CommandName);
    }

    protected void btnAmendNo_Click(object sender, EventArgs e)
    {
        //modalPop_AmendNo.Show();
    }

    private void Load_Data_Empty_AmendNo()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtAMNo.Items.Clear();
        query = "Select Amend_No,Amend_Date from Pur_Request_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
     
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtAMNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["Amend_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtAMNo.DataTextField = "Amend_No";
        txtAMNo.DataBind();



        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Amend_No,Amend_Date from Pur_Request_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
     
        //DT = objdata.RptEmployeeMultipleDetails(query);


        //Repeater_AmendNo.DataSource = DT;
        //Repeater_AmendNo.DataBind();
        //Repeater_AmendNo.Visible = true;

    }

    protected void GridViewClick_AmendNo(object sender, CommandEventArgs e)
    {
        txtAMNo.Text = Convert.ToString(e.CommandArgument);
        //txtAMDateHide.Value = Convert.ToString(e.CommandName);
    }

    protected void btnAmendby_Click(object sender, EventArgs e)
    {
        //modalPop_Amendby.Show();
    }

    private void Load_Data_Empty_Amendby()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtAmendby.Items.Clear();
        query = "Select Amendby from Pur_Request_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
     
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtAmendby.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["Amendby"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtAmendby.DataTextField = "Amendby";
        txtAmendby.DataBind();



        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Amendby from Pur_Request_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
     
        //DT = objdata.RptEmployeeMultipleDetails(query);


        //Repeater_Amendby.DataSource = DT;
        //Repeater_Amendby.DataBind();
        //Repeater_Amendby.Visible = true;

    }

    protected void GridViewClick_Amendby(object sender, CommandEventArgs e)
    {
        txtAmendby.Text = Convert.ToString(e.CommandArgument);
        
    }


}
