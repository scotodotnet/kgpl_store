﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class Reports_Report_Purchase_Order_ReceiptDetails : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionBlanketPOOrderNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SSQL;
    DateTime FromDate;
    DateTime ToDate;
    DataTable AutoDataTable = new DataTable();
    DataSet ds = new DataSet();
    string RptName = "";
    string ButtonName = "";
    string WareHouse = ""; string PORecNo = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

       
        Page.Title = "ERP Stores Module :: Purchase Order Receipt Details Report";

        if (!IsPostBack)
        {
            Load_Data_Empty_WareHouse();
            Load_Data_Empty_PORecNo();
            Load_Data_Empty_Supp1();
            Load_Data_Empty_ItemCode();
            Load_Data_Empty_Dept();
        }
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        ButtonName = "Details Report";
        RptName = "Purchase Order Receipt Details Report";


        if (txtFromDate.Text != "" || txtToDate.Text != "")
        {
            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if (FDate < TDate)
            {
                ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDeptName.SelectedItem.Text + "&WareHouse=" + ddlWarehouseName.SelectedItem.Text + "&SupplierName=" + ddlSuppName.SelectedItem.Text + "&PORNumber=" + txtPORNo.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedValue + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Correct Date..');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select FromDate And ToDate..');", true);
        }

    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlWarehouseName.SelectedValue = "-Select-";
        //txtWarehouseCodeHide.Value = "";
        ddlDeptName.SelectedItem.Text = "-Select-";
        ddlSuppName.SelectedItem.Text = "-Select-";
        ddlItemName.SelectedItem.Text = "-Select-";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtPORNo.SelectedValue = "-Select-";
        //txtPORDateHide.Value = "";
    }


    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        ddlWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        ddlWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        ddlWarehouseName.DataTextField = "WarehouseName";
        ddlWarehouseName.DataValueField = "WarehouseCode";
        ddlWarehouseName.DataBind();

    }

    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        ddlDeptName.Items.Clear();
        query = "select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        ddlDeptName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        ddlDeptName.DataTextField = "DeptName";
        ddlDeptName.DataValueField = "DeptCode";
        ddlDeptName.DataBind();

    }

    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        ddlItemName.Items.Clear();
      
        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlItemName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataValueField = "ItemCode";

        ddlItemName.DataBind();
        //Repeater_ItemCode.Visible = true;

    }
    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();
        DataRow dr;

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlSuppName.DataSource = DT;
        dr = DT.NewRow();

        dr["SuppCode"] = "-Select-";
        dr["SuppName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlSuppName.DataValueField = "SuppCode";
        ddlSuppName.DataTextField = "SuppName";

        ddlSuppName.DataBind();
        //Repeater_Supplier1.Visible = true;

    }

    private void Load_Data_Empty_PORecNo()
    {


        string query = "";
        DataTable Main_DT = new DataTable();

        txtPORNo.Items.Clear();
        query = "Select PO_Receipt_No,PO_Receipt_Date from Pur_Order_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And PO_Receipt_Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtPORNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();


        ddlItemName.DataSource = Main_DT;

        dr = Main_DT.NewRow();

        dr["PO_Receipt_Date"] = "-Select-";
        dr["PO_Receipt_No"] = "-Select-";

        Main_DT.Rows.InsertAt(dr, 0);

        txtPORNo.DataValueField = "PO_Receipt_Date";
        txtPORNo.DataTextField = "PO_Receipt_No";

        //txtPORNo.DataTextField = "PO_Receipt_No";

        txtPORNo.DataBind();



        //string query = "";
        //DataTable DT = new DataTable();
        //query = "Select PO_Receipt_No,PO_Receipt_Date from Pur_Order_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And PO_Receipt_Status='1'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_PORecNo.DataSource = DT;
        //Repeater_PORecNo.DataBind();
        //Repeater_PORecNo.Visible = true;

    }


    protected void btnDeptWise_Click(object sender, EventArgs e)
    {
        ButtonName = "Dept Wise Report";
        RptName = "Purchase Order Receipt Details Report";


        if (txtFromDate.Text != "" || txtToDate.Text != "")
        {
            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if (FDate < TDate)
            {
                ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDeptName.SelectedItem.Text + "&WareHouse=" + ddlWarehouseName.SelectedItem.Text + "&SupplierName=" + ddlSuppName.SelectedItem.Text + "&PORNumber=" + txtPORNo.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedValue + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Correct Date..');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select FromDate And ToDate..');", true);
        }

    }

    protected void btnSlip_Click(object sender, EventArgs e)
    {
        ButtonName = "Pur Ord Slip Report";

        RptName = "Purchase Order Receipt Details Report";

        if (txtPORNo.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Purchase No..');", true);
        }
        else
        {
            ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDeptName.SelectedItem.Text + "&WareHouse=" + ddlWarehouseName.SelectedItem.Text + "&SupplierName=" + ddlSuppName.SelectedItem.Text + "&PORNumber=" + txtPORNo.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedValue + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
        }
    }

    protected void btnItemWise_Click(object sender, EventArgs e)
    {
        ButtonName = "Item Wise Report";
        RptName = "Purchase Order Receipt Details Report";


        if (txtFromDate.Text != "" || txtToDate.Text != "")
        {
            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if (FDate < TDate)
            {
                ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDeptName.SelectedItem.Text + "&WareHouse=" + ddlWarehouseName.SelectedItem.Text + "&SupplierName=" + ddlSuppName.SelectedItem.Text + "&PORNumber=" + txtPORNo.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedValue + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Correct Date..');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select FromDate And ToDate..');", true);
        }
    }
}
