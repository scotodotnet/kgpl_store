<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Report_Purchase_Enquiry.aspx.cs" Inherits="Report_Report_Purchase_Enquiry" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=12.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Supplier1').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Supplier1').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#PEnquryNo').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#PEnquryNo').dataTable();               
            }
        });
    };
</script>


<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
  
   $find('Supp1_Close').hide();
   $find('Item_Close').hide();
 $find('PEnquryNo_Close').hide();
   
   
      }
   } 
 </script>
<!--Ajax popup End-->


<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Purchase Enquiry</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">        
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Purchase Enquiry</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Warehouse</label>
					            <asp:TextBox ID="txtWarehouseName" class="form-control" runat="server" Enabled="false"></asp:TextBox>
					            <asp:HiddenField ID="txtWarehouseCodeHide" runat="server" />
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Department</label>
					            <asp:TextBox ID="txtDepartmentName" class="form-control" runat="server" Enabled="false"></asp:TextBox>
					            <asp:HiddenField ID="txtDeptCodeHide" runat="server" />
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Supplier</label>
					            <asp:TextBox ID="txtSupplierName" class="form-control" runat="server" style="float: left;width: 81%;"></asp:TextBox>
					             <asp:Button ID="btnSupp1" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnSupp1_Click"/>
					            
					            <cc1:ModalPopupExtender ID="modalPop_Supp1"  runat="server" PopupControlID="Panl1" TargetControlID="btnSupp1"
                       CancelControlID="BtnClear_Supp1" BackgroundCssClass="modalBackground" BehaviorID="Supp1_Close">
                    </cc1:ModalPopupExtender>
                              
                                <asp:Panel ID="Panl1" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Supplier Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_Supplier1" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="Supplier1" class="display table">
                         <thead >
                         <tr>
                         <th>SuppCode</th>
                         <th>SuppName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("SuppCode")%></td>
                          <td><%# Eval("SuppName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick" CommandArgument='<%# Eval("SuppCode")%>' CommandName='<%# Eval("SuppName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_Supp1" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>
					            
					            
					            <asp:HiddenField ID="txtSuppCodehide" runat="server" />
					        </div>
					        
					        </div>
					        </div>
					        
					        
					       <div class="col-md-12">
					    <div class="row"> 
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Name</label>
					            <asp:TextBox ID="txtItemName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnItemCode" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnItemCode_Click"/>
                                
                                 <cc1:ModalPopupExtender ID="modalPop_ItemCode"  runat="server" PopupControlID="Panel5" TargetControlID="btnItemCode"
                       CancelControlID="BtnItemClose" BackgroundCssClass="modalBackground" BehaviorID="Item_Close">
                    </cc1:ModalPopupExtender>
                               
                                 <asp:Panel ID="Panel5" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Item Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_ItemCode" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="ItemTable" class="display table">
                         <thead >
                         <tr>
                         <th>ItemCode</th>
                         <th>ItemName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ItemCode")%></td>
                          <td><%# Eval("ItemName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEdit_ItemCode" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ItemCode" CommandArgument='<%# Eval("ItemCode")%>' CommandName='<%# Eval("ItemName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnItemClose" class="btn btn-rounded" runat="server" Text="Close" />
					     </div>
					     </asp:Panel>
					            
					            
					            <asp:HiddenField ID="txtItemCodeHide" runat="server" />
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">From Date</label>
					            <asp:TextBox ID="txtFromDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderFromDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtFromDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">To Date</label>
					            <asp:TextBox ID="txtToDate" class="form-control date-picker" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderToDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtToDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">PE No</label>
					            
					      <asp:DropDownList ID="txtPENo" runat="server" class="js-states form-control">
                          </asp:DropDownList>
					            
					            <%--<asp:TextBox ID="txtPENo" class="form-control" runat="server" style="float: left;width: 85%;"></asp:TextBox>
					            <asp:Button ID="btnPEnquryNo" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnPEnquryNo_Click"/>--%>
                    <%--<cc1:ModalPopupExtender ID="modalPop_PEnquryNo"  runat="server" PopupControlID="Panel3" TargetControlID="btnPEnquryNo"
                       CancelControlID="BtnClear_PEnquryNo" BackgroundCssClass="modalBackground" BehaviorID="PEnquryNo_Close">
                    </cc1:ModalPopupExtender>--%>
                   
                    <%--<asp:Panel ID="Panel3" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Purchase Enquiry Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_PEnquryNo" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="PEnquryNo" class="display table">
                         <thead >
                         <tr>
                         <th>Pur_EnquiryNo</th>
                         <th>Enquiry_Date</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("Pur_EnquiryNo")%></td>
                          <td><%# Eval("Enquiry_Date")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditPEnquryNo" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_PEnquryNo" CommandArgument='<%# Eval("Pur_EnquiryNo")%>' CommandName='<%# Eval("Enquiry_Date")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_PEnquryNo" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            <%--<asp:HiddenField ID="txtPEDateHide" runat="server" />--%>
					        </div>
					    </div>
					</div>
					
					<div class="clearfix"></div>
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnReports" class="btn btn-success"  runat="server" 
                            Text="Report" onclick="btnReports_Click"/>
                        <asp:Button ID="btnClear" class="btn btn-success" runat="server" Text="Clear" 
                            onclick="btnClear_Click"/>
                    </div>
                    <!-- Button end -->
                      <%--<div class="form-group row">
                          
                          <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
                              AutoDataBind="True" DisplayPage="False" DisplayToolbar="False" 
                              EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False" 
                              GroupTreeImagesFolderUrl="" Height="800px" ToolbarImagesFolderUrl="" 
                              ToolPanelView="None" ToolPanelWidth="200px" Width="200px" />
                          <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
                              <Report FileName="D:\ND\ERP_Stores\Source_Code\crystal\PurchaseEnquiryReport.rpt">
                              </Report>
                          </CR:CrystalReportSource>
                      </div>--%>                
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
</asp:Content>
