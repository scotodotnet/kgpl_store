﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_Spares_Re_Sales : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string CostCenterName = ""; string DeptName = "";
    string WareHouse = ""; string TransNo = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Spares Re-Sales Details";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_RptInventory"));
            li.Attributes.Add("class", "droplink active open");

        }

        if (!IsPostBack)
        {
            Load_Data_Empty_Dept();
            Load_Data_Empty_CostCenterName();
            Load_Data_Empty_WareHouse();
            Load_Data_Empty_SparesReSales();
        }
        Load_Data_Empty_ItemCode();
    }


    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }


    protected void btnCostCenterName_Click(object sender, EventArgs e)
    {
        //modalPop_CostCenterName.Show();
    }
    private void Load_Data_Empty_CostCenterName()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtCostCenterName.Items.Clear();
        query = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCostCenterName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["CostcenterCode"] = "-Select-";
        dr["CostcenterName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCostCenterName.DataTextField = "CostcenterName";
        txtCostCenterName.DataValueField = "CostcenterCode";
        txtCostCenterName.DataBind();

    }

    protected void GridViewClick_CostCenterName(object sender, CommandEventArgs e)
    {
        //txtCostCenterCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtCostCenterName.Text = Convert.ToString(e.CommandName);


    }


    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        //modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();


    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtWarehouseName.Text = Convert.ToString(e.CommandName);

    }



    protected void btnSparesReSales_Click(object sender, EventArgs e)
    {
        //modalPop_SparesReSales.Show();
    }
    private void Load_Data_Empty_SparesReSales()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtTransNo.Items.Clear();
        query = "Select Spares_ReSales_No,Spares_ReSales_Date from Spares_ReSales_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtTransNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Spares_ReSales_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtTransNo.DataTextField = "Spares_ReSales_No";

        txtTransNo.DataBind();
       
        
        
        //string query = "";
        //DataTable DT = new DataTable();


        //query = "Select Spares_ReSales_No,Spares_ReSales_Date from Spares_ReSales_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status='1'";

        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_SparesReSales.DataSource = DT;
        //Repeater_SparesReSales.DataBind();
        //Repeater_SparesReSales.Visible = true;

    }

    protected void GridViewClick_SparesReSales(object sender, CommandEventArgs e)
    {
       // txtSparesReSalesDate.Value = Convert.ToString(e.CommandName);
        txtTransNo.Text = Convert.ToString(e.CommandArgument);

    }




    protected void btnClear_Click(object sender, EventArgs e)
    {

        txtDepartmentName.SelectedValue = "-Select-";
        //txtDeptCodeHide.Value = "";
        //txtCostCenterCodeHide.Value = "";
        txtCostCenterName.SelectedValue = "-Select-";
        //txtWarehouseCodeHide.Value = "";
        txtWarehouseName.SelectedValue = "-Select-";
        //txtSparesReSalesDate.Value = "";
        txtTransNo.SelectedValue = "-Select-";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtItemCodeHide.Value = "";
        txtItemName.Text = "";

        Load_Data_Empty_Dept();
        Load_Data_Empty_CostCenterName();
        Load_Data_Empty_WareHouse();
        Load_Data_Empty_SparesReSales();
        Load_Data_Empty_ItemCode();


    }
    protected void btnReports_Click(object sender, EventArgs e)
    {
        RptName = "Spares Re-Sales Report";

        if (txtCostCenterName.SelectedItem.Text != "-Select-")
        {
            CostCenterName = txtCostCenterName.SelectedItem.Text;
        }
        else
        {
            CostCenterName = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }
        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            TransNo = txtTransNo.SelectedItem.Text;
        }
        else
        {
            TransNo = "";
        }
        
        ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&CostCenterName=" + CostCenterName + "&WareHouseName=" + WareHouse + "&SparesReSalesNo=" + TransNo + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");

    }


}
