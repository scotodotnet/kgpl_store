﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_Unit_To_Unit_Material_Stock_Transfer_Details : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        Page.Title = "ERP Stores Module :: Stock Transfer Details Report";


        Load_Data_Empty_WareHouse();
        Load_Data_Empty_StkTranNo();
        Load_Data_Empty_ItemCode();
    }


    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtWarehouseName.Text = "";
        txtWarehouseCodeHide.Value = "";
        txtStkTranNo.Text = "";
        txtStkTranDateHide.Value = "";
        txtItemName.Text = "";
        txtItemCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }
    protected void btnReports_Click(object sender, EventArgs e)
    {
        RptName = "Unit To Unit Stock Transfer Details Report";

        ResponseHelper.Redirect("ReportDisplay.aspx?WareHouse=" + txtWarehouseName.Text + "&StckTranNo=" + txtStkTranNo.Text + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable DT = new DataTable();


        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_WareHouse.DataSource = DT;
        Repeater_WareHouse.DataBind();
        Repeater_WareHouse.Visible = true;

    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtWarehouseName.Text = Convert.ToString(e.CommandName);

    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }




    protected void btnStkTranNo_Click(object sender, EventArgs e)
    {
        modalPop_StkTranNo.Show();
    }
    private void Load_Data_Empty_StkTranNo()
    {

        string query = "";
        DataTable DT = new DataTable();
        query = "Select Unit_Stock_Transfer_No,Unit_Stock_Transfer_Date from Unit_To_Unit_Meterial_Stock_Transfer_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And Mat_Stock_Transfer_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_StkTranNo.DataSource = DT;
        Repeater_StkTranNo.DataBind();
        Repeater_StkTranNo.Visible = true;

    }

    protected void GridViewClick_StkTranNo(object sender, CommandEventArgs e)
    {
        txtStkTranNo.Text = Convert.ToString(e.CommandArgument);
        txtStkTranDateHide.Value = Convert.ToString(e.CommandName);
    }

}
