﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;



public partial class Reports_Report_GatePassIn_Details : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string GPInNo = ""; string GPOutNo = ""; 
    string WareHouse = "";
    Boolean Errflag = true;

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "ERP Stores Module :: GatePass IN Details Report";


        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Load_Data_Empty_WareHouse();
            Load_Data_Empty_GPInNo();
            Load_Data_Empty_GPOutNo();
            Load_Data_Empty_ItemCode();
            Load_Data_Empty_Supp1();
            Load_Data_Empty_Dept();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtWarehouseName.SelectedValue = "-Select-";
        //txtWarehouseCodeHide.Value = "";
        ddlSupplier.SelectedValue = "-Select-";
        txtSuppCodehide.Value = "";
        txtGPInNo.SelectedValue = "-Select-";
        //txtGPInDateHide.Value = "";
        txtGPOutNo.SelectedValue = "-Select-";
        //txtGPOutDateHide.Value = "";
        ddlItemName.SelectedValue = "-Select-";

        ddlDeptName.SelectedItem.Text = "-Select-";
        txtItemCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }

    protected void btnReports_Click(object sender, EventArgs e)
    {

        DateTime FDate;
        DateTime TDate;
        RptName = "GatePass IN Details Report";
        string ButtonName = "GatePass_Det";
        //if (txtGPInNo.SelectedItem.Text != "-Select-")
        //{
        //    GPInNo = txtGPInNo.SelectedItem.Text;
        //}
        //else
        //{
        //    GPInNo = "";
        //}
        //if (txtGPOutNo.SelectedItem.Text != "-Select-")
        //{
        //    GPOutNo = txtGPOutNo.SelectedItem.Text;
        //}
        //else
        //{
        //    GPOutNo = "";
        //}

        //if (txtWarehouseName.SelectedItem.Text != "-Select-")
        //{
        //    WareHouse = txtWarehouseName.SelectedItem.Text;
        //}
        //else
        //{
        //    WareHouse = "";
        //}

        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {
            FDate = Convert.ToDateTime(txtFromDate.Text);
            TDate = Convert.ToDateTime(txtToDate.Text);
            if (FDate > TDate)
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Correct Date');", true);
            }
            else
            {
                if (Errflag == true)
                {
                    ResponseHelper.Redirect("ReportDisplay.aspx?WareHouse=" + txtWarehouseName.SelectedItem.Text + "&SupplierName=" + ddlSupplier.SelectedItem.Text + "&GPINNo=" + txtGPInNo.SelectedItem.Text + "&GPOutNo=" + txtGPOutNo.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName + "&BtnName=" + ButtonName + "&Dept=" + ddlDeptName.SelectedItem.Text, "_blank", "");
                }
            }
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Date');", true);
        }
    }
    protected void btnSlip_Click(object sender, EventArgs e)
    {
        string ButtonName = "GP IN SLIP";

        RptName = "GatePass IN Details Report";

        DateTime FDate;
        DateTime TDate;  

        //if (txtFromDate.Text != "" && txtToDate.Text != "")
        //{
        //    FDate = Convert.ToDateTime(txtFromDate.Text);
        //    TDate = Convert.ToDateTime(txtToDate.Text);
        //    if (FDate > TDate)
        //    {
        //        Errflag = false;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Correct Date');", true);
        //    }
        //    else
        //    {
        //        if (Errflag == true)
        //        {
        ResponseHelper.Redirect("ReportDisplay.aspx?WareHouse=" + txtWarehouseName.SelectedItem.Text + "&Dept="+  ddlDeptName.SelectedItem.Text +"&SupplierName=" + ddlItemName.SelectedItem.Text + "&GPINNo=" + txtGPInNo.SelectedItem.Text + "&GPOutNo=" + txtGPOutNo.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&BtnName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
        //        }
        //    }
        //}
        //else
        //{
        //    Errflag = false;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Date');", true);
        //}
    }

    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        //modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();
    }

    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        ddlDeptName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        ddlDeptName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        ddlDeptName.DataTextField = "DeptName";
        ddlDeptName.DataValueField = "DeptCode";
        ddlDeptName.DataBind();
    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtWarehouseName.Text = Convert.ToString(e.CommandName);

    }


    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        //modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlItemName.DataSource = DT;

        DataRow dr = DT.NewRow();
        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);

        ddlItemName.DataValueField = "ItemCode";
        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataBind();
    }

     private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlSupplier.DataSource = DT;

        DataRow dr = DT.NewRow();
        dr["SuppCode"] = "-Select-";
        dr["SuppName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);

        ddlSupplier.DataValueField = "SuppCode";
        ddlSupplier.DataTextField = "SuppName";
        ddlSupplier.DataBind();

    }

    protected void btnGPInNo_Click(object sender, EventArgs e)
    {
        //modalPop_GPInNo.Show();
    }
    private void Load_Data_Empty_GPInNo()
    {


        string query = "";
        DataTable Main_DT = new DataTable();

        txtGPInNo.Items.Clear();
        query = "Select GP_IN_No,GP_IN_Date from GatePass_IN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And GP_IN_Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtGPInNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["GP_IN_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtGPInNo.DataTextField = "GP_IN_No";

        txtGPInNo.DataBind();


        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select GP_IN_No,GP_IN_Date from GatePass_IN_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And GP_IN_Status='1'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_GPInNo.DataSource = DT;
        //Repeater_GPInNo.DataBind();
        //Repeater_GPInNo.Visible = true;

    }

    protected void GridViewClick_GPInNo(object sender, CommandEventArgs e)
    {
        txtGPInNo.Text = Convert.ToString(e.CommandArgument);
       // txtGPInDateHide.Value = Convert.ToString(e.CommandName);
    }


    protected void btnGPOutNo_Click(object sender, EventArgs e)
    {
        //modalPop_GPOutNo.Show();
    }
    private void Load_Data_Empty_GPOutNo()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtGPOutNo.Items.Clear();
        query = "Select GP_Out_No,GP_Out_Date from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And GP_Type='1' And GP_Out_Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtGPOutNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["GP_Out_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtGPOutNo.DataTextField = "GP_Out_No";

        txtGPOutNo.DataBind();



        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select GP_Out_No,GP_Out_Date from GatePass_Out_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And GP_Type='1' And GP_Out_Status='1'";
        //DT = objdata.RptEmployeeMultipleDetails(query);
       

        //Repeater_GPOutNo.DataSource = DT;
        //Repeater_GPOutNo.DataBind();
        //Repeater_GPOutNo.Visible = true;

    }

    protected void GridViewClick_GPOutNo(object sender, CommandEventArgs e)
    {
        txtGPOutNo.Text = Convert.ToString(e.CommandArgument);
        //txtGPOutDateHide.Value = Convert.ToString(e.CommandName);
    }



    protected void btnScrab_Click(object sender, EventArgs e)
    {

        RptName = "GatePass IN Details Report";
        string ButtonName = "GatePassIn_Scrab";

        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {
            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);
            if (FDate > TDate)
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Correct Date');", true);
            }
            else
            {
                if (Errflag == true)
                {
                    ResponseHelper.Redirect("ReportDisplay.aspx?WareHouse=" + txtWarehouseName.SelectedItem.Text + "&SupplierName=" + ddlSupplier.SelectedItem.Text + "&GPINNo=" + txtGPInNo.SelectedItem.Text + "&GPOutNo=" + txtGPOutNo.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName + "&BtnName=" + ButtonName + "&Dept=" + ddlDeptName.SelectedItem.Text, "_blank", "");
                }
            }
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Date');", true);
        }
    }

    protected void btnFreeService_Click(object sender, EventArgs e)
    {
        RptName = "GatePass IN Free Service";
      
        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {
            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);
            if (FDate > TDate)
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Correct Date');", true);
            }
            else
            {
                if (Errflag == true)
                {
                    ResponseHelper.Redirect("ReportDisplay.aspx?WareHouse=" + txtWarehouseName.SelectedItem.Text + "&SupplierName=" + ddlSupplier.SelectedItem.Text + "&GPINNo=" + txtGPInNo.SelectedItem.Text + "&GPOutNo=" + txtGPOutNo.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName + "&Dept=" + ddlDeptName.SelectedItem.Text, "_blank", "");
                }
            }
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Date');", true);
        }
    }
}
