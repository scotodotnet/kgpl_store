﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_Closing_Statement : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";


    protected void Page_Load(object sender, EventArgs e)
    {

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Closing Stock Statement";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_RptStock"));
            li.Attributes.Add("class", "droplink active open");

        }


    }


    protected void btnReports_Click(object sender, EventArgs e)
    {

        string[] finyear = SessionFinYearVal.Split('_');

        string FromDate_Chk = "01/04/" + finyear[0];
        string ToDate_Chk = "31/03/" + finyear[1];

        string fromDate = txtFromDate.Text;
        string toDate = txtToDate.Text;

        if ((Convert.ToDateTime(fromDate) >= Convert.ToDateTime(FromDate_Chk)) && (Convert.ToDateTime(toDate) <= Convert.ToDateTime(ToDate_Chk)))
        {

            if (ChkWithDet.Checked == true)
            {
                RptName = "Closing Stock Statement";

                ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
            }
            else if (ChkWithDet.Checked == false)
            {

                RptName = "Closing Stock Statement without Details";

                ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with given Date...');", true);
        }


    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtProdInKGs.Text = "0.00"; txtSpareCostPerKGs.Text = "0.00";
        txtPackCostPerKGs.Text = "0.00"; txtFuelsPerKGs.Text = "0.00";
        txtAlterErection.Text = "0.00"; txtMachinery.Text = "0.00";
        txtCivil.Text = "0.00"; txtFromDate.Text = "";
        txtToDate.Text = "";
    }

    protected void btnSaveDet_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Input Details 
       
        if (txtProdInKGs.Text=="")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter Production In KGs..');", true);
        }
        else if (txtSpareCostPerKGs.Text=="")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter Spare Cost Per KGs..');", true);
        }
        else if (txtPackCostPerKGs.Text=="")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter Packing Cost Per KGs..');", true);
        }
        else if (txtFuelsPerKGs.Text=="")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter Fuels Per KGs..');", true);
        }
        else if (txtAlterErection.Text=="")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter Additional Alter/Erection Work..');", true);
        }
        else if (txtMachinery.Text=="")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter Machinery Cost..');", true);
        }
        else if (txtCivil.Text=="")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter Civil Cost..');", true);
        }
        else if (txtFromDate.Text=="" || txtToDate.Text=="")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Date..');", true);
        }

        if (!ErrFlag)
        {
            query = "Insert Into Closing_Stock_Statement_Details(Ccode,Lcode,FinYearCode,FinYearVal,ProdInKGs,SpareCostPerKGs,PackCostPerKGs,FuelsPerKGs,AlterErection,Machinery,Civil,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtProdInKGs.Text + "','" + txtSpareCostPerKGs.Text + "','" + txtPackCostPerKGs.Text + "','" + txtFuelsPerKGs.Text + "',";
            query = query + " '" + txtAlterErection.Text + "','" + txtMachinery.Text + "','" + txtCivil.Text + "','" + txtFromDate.Text + "','" + txtToDate.Text + "',";
            query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

        }
        if (!ErrFlag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Details Saved Successfully..');", true);
        }
        
    }

}
