﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Report_Report_Purchase_Enquiry : System.Web.UI.Page
{

    string SSQL = "";
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    DataTable PurEnqrDT = new DataTable();
    DateTime frmDate;
    DateTime ToDate;
    DataTable AutoDataTable = new DataTable();
    DataSet ds = new DataSet();
    string RptName = ""; string PEnquryNo = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "ERP Stores Module :: Purchase Enquiry Details";

        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Load_Data_Empty_ItemCode();
        Load_Data_Empty_Supp1();

        if (!IsPostBack)
        {
            Load_Data_Empty_PEnquryNo();
        }


    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDepartmentName.Text = "";
        txtDeptCodeHide.Value = "";
        txtItemName.Text = "";
        txtItemCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtSupplierName.Text = "";
        txtSuppCodehide.Value = "";
        txtPENo.SelectedValue = "-Select-";
        //txtPEDateHide.Value = "";
    }
    protected void btnReports_Click(object sender, EventArgs e)
    {
       RptName="PurchaseEnquiryReport";

       if (txtPENo.SelectedItem.Text != "-Select-")
       {
           PEnquryNo = txtPENo.SelectedItem.Text;
       }
       else
       {
           PEnquryNo = "";
       }


       ResponseHelper.Redirect("ReportDisplay.aspx?SupplierName=" + txtSupplierName.Text + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&PENo=" + PEnquryNo + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnSupp1_Click(object sender, EventArgs e)
    {
        modalPop_Supp1.Show();
    }
    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier1.DataSource = DT;
        Repeater_Supplier1.DataBind();
        Repeater_Supplier1.Visible = true;

    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
        txtSupplierName.Text = Convert.ToString(e.CommandName);


    }


    protected void btnPEnquryNo_Click(object sender, EventArgs e)
    {
        //modalPop_PEnquryNo.Show();
    }
    private void Load_Data_Empty_PEnquryNo()
    {



        string query = "";
        DataTable Main_DT = new DataTable();


        txtPENo.Items.Clear();
        query = "Select Pur_EnquiryNo,Enquiry_Date from Pur_Enq_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtPENo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Pur_EnquiryNo"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtPENo.DataTextField = "Pur_EnquiryNo";

        txtPENo.DataBind();

        //string query = "";
        //DataTable DT = new DataTable();

      

        //query = "Select Pur_EnquiryNo,Enquiry_Date from Pur_Enq_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_PEnquryNo.DataSource = DT;
        //Repeater_PEnquryNo.DataBind();
        //Repeater_PEnquryNo.Visible = true;

    }

    protected void GridViewClick_PEnquryNo(object sender, CommandEventArgs e)
    {
       txtPENo.Text = Convert.ToString(e.CommandArgument);
       //txtPEDateHide.Value = Convert.ToString(e.CommandName);


    }

}
