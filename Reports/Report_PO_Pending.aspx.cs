﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_PO_Pending : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();


    string RptName = "";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnReports_Click(object sender, EventArgs e)
    {
        if (RdpReceiptType.SelectedValue == "1")
        {

            RptName = "Standard Purchase Order Pending Details Report";

            ResponseHelper.Redirect("ReportDisplay.aspx?SupplierName=" + txtSupplierName.Text + "&RptName=" + RptName, "_blank", "");
        }
        else if (RdpReceiptType.SelectedValue == "2")
        {

            RptName = "Blanket Purchase Order Pending Details Report";

            ResponseHelper.Redirect("ReportDisplay.aspx?SupplierName=" + txtSupplierName.Text + "&RptName=" + RptName, "_blank", "");
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        RdpReceiptType.SelectedValue = "1";
        txtSupplierName.Text = "";
        txtSuppCodehide.Value = "";
    }
}
