﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Report_Supplier_Payment.aspx.cs" Inherits="Reports_Report_Supplier_Payment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>



<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Supplier1').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Supplier1').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#GenPurOrdNo').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#GenPurOrdNo').dataTable();               
            }
        });
    };
</script>


<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
  
   $find('Supp1_Close').hide();
   $find('GenPurOrdNo_Close').hide();
  
      }
   } 
 </script>
<!--Ajax popup End-->



 <div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Supplier Payment Details</li></h4> 
    </ol>
</div>
    
 <div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">        
             <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Supplier Payment Details</h4>
				</div>
				</div>
			<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Supplier</label>
					            <asp:TextBox ID="txtSupplierName" class="form-control" runat="server" style="float: left;width: 81%;"></asp:TextBox>
					            <asp:Button ID="btnSupp1" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnSupp1_Click"/>
					            
					            <cc1:ModalPopupExtender ID="modalPop_Supp1"  runat="server" PopupControlID="Panl1" TargetControlID="btnSupp1"
                       CancelControlID="BtnClear_Supp1" BackgroundCssClass="modalBackground" BehaviorID="Supp1_Close">
                    </cc1:ModalPopupExtender>
                              
                                <asp:Panel ID="Panl1" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Supplier Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_Supplier1" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="Supplier1" class="display table">
                         <thead >
                         <tr>
                         <th>SuppCode</th>
                         <th>SuppName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("SuppCode")%></td>
                          <td><%# Eval("SuppName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick" CommandArgument='<%# Eval("SuppCode")%>' CommandName='<%# Eval("SuppName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_Supp1" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>
					            
					            
					            <asp:HiddenField ID="txtSuppCodehide" runat="server" />
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Payment Trans. No</label>
					            
					      <asp:DropDownList ID="txtTransNo" runat="server" class="js-states form-control">
                          </asp:DropDownList>
					            
					            <%--<asp:TextBox ID="txtTransNo" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnTransNo" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnTransNo_Click"/>--%>
					            
					            <%--<cc1:ModalPopupExtender ID="modalPop_GenPurOrdNo"  runat="server" PopupControlID="Panel2" TargetControlID="btnTransNo"
                       CancelControlID="BtnClear_GenPurOrdNo" BackgroundCssClass="modalBackground" BehaviorID="GenPurOrdNo_Close">
                    </cc1:ModalPopupExtender>--%>
                              
                                <%--<asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Supplier Payment Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_GenPurOrdNo" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="GenPurOrdNo" class="display table">
                         <thead >
                         <tr>
                         <th>Trans No</th>
                         <th>Trans Date</th>
                         <th>Supplier</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("Trans_No")%></td>
                          <td><%# Eval("Trans_Date")%></td>
                          <td><%# Eval("Supp_Name")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditGenPurOrdNo" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_GenPurOrdNo" CommandArgument='<%# Eval("Trans_No")%>' CommandName='<%# Eval("Trans_Date")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_GenPurOrdNo" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            <%--<asp:HiddenField ID="txtTrans_Date" runat="server" />--%>
					        </div>
					        
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">From Date</label>
					            <asp:TextBox ID="txtFromDate" MaxLength="20" class="form-control date-picker" 
                                    runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderFromDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtFromDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">To Date</label>
					            <asp:TextBox ID="txtToDate" class="form-control date-picker" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderToDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtToDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					    </div>
					</div>
					        
					        
					   <div class="col-md-12">
					    <div class="row">
					        
					        
					    </div>
					</div>
					
					<div class="clearfix"></div>
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="col-md-12">
					    <div class="row">
                            <asp:Button ID="btnSupplierBalance" class="btn btn-success"  runat="server" 
                                Text="All Supplier Balance" onclick="btnSupplierBalance_Click"/>
                            <asp:Button ID="btnspecificSupplier" class="btn btn-success"  runat="server" 
                                Text="Single Supplier" onclick="btnspecificSupplier_Click" />
                            <asp:Button ID="btnReceipt" class="btn btn-success"  runat="server" 
                                Text="Receipt Format" onclick="btnReceipt_Click" />
                            
                            <asp:Button ID="btnClear" class="btn btn-success" runat="server" 
                                Text="Clear" onclick="btnClear_Click"  />
                        </div>
                    </div>
                    <div class="col-md-12">
					    <div class="row">
					    <br />
                            <asp:Button ID="btnMonthly_Payments" class="btn btn-success"  runat="server" 
                                Text="Monthly Payments" onclick="btnMonthly_Payments_Click" Width="220"/>
                            <asp:Button ID="btnSupplier_Ledger_Rpt" class="btn btn-success"  runat="server" 
                                Text="Ledger Report" onclick="btnSupplier_Ledger_Rpt_Click" Width="188"/>
                        </div>
                    </div>
                    <!-- Button end -->
                      
                <div class="clearfix"></div>
                                                    
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
             <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
	<div class="col-md-2"></div>
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 
</asp:Content>

