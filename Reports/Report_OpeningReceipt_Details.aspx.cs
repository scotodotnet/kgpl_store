﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_OpeningReceipt_Details : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string WareHouse = ""; string OpnRecNo = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

       
       Page.Title = "ERP Stores Module :: Opening Receipt Details";

       if (!IsPostBack)
       {
           Load_Data_Empty_WareHouse();
           Load_Data_Empty_OpnRecNo();
       }
        Load_Data_Empty_ItemCode();
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtWarehouseName.SelectedValue = "-Select-";
        //txtWarehouseCodeHide.Value = "";
        txtOpnRecNo.SelectedValue = "-Select-";
        //txtOpnRecNoDateHide.Value = "";
        txtItemName.Text = "";
        txtItemCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }
    protected void btnReports_Click(object sender, EventArgs e)
    {
        RptName = "Opening Receipt Details Report";

        if (txtOpnRecNo.SelectedItem.Text != "-Select-")
        {
            OpnRecNo = txtOpnRecNo.SelectedItem.Text;
        }
        else
        {
            OpnRecNo = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }

        ResponseHelper.Redirect("ReportDisplay.aspx?WareHouse=" + WareHouse + "&OpnRecNo=" + OpnRecNo + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        //modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();

    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtWarehouseName.Text = Convert.ToString(e.CommandName);

    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }

    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnOpnRecNo_Click(object sender, EventArgs e)
    {
        //modalPop_OpnRecNo.Show();
    }

    private void Load_Data_Empty_OpnRecNo()
    {


        string query = "";
        DataTable Main_DT = new DataTable();

        txtOpnRecNo.Items.Clear();
        query = "Select Open_Recp_No,Open_Recp_Date from Opening_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And OP_Receipt_Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtOpnRecNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Open_Recp_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtOpnRecNo.DataTextField = "Open_Recp_No";

        txtOpnRecNo.DataBind();

        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Open_Recp_No,Open_Recp_Date from Opening_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And OP_Receipt_Status='1'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_OpnRecNo.DataSource = DT;
        //Repeater_OpnRecNo.DataBind();
        //Repeater_OpnRecNo.Visible = true;

    }

    protected void GridViewClick_OpnRecNo(object sender, CommandEventArgs e)
    {
        txtOpnRecNo.Text = Convert.ToString(e.CommandArgument);
        //txtOpnRecNoDateHide.Value = Convert.ToString(e.CommandName);
    }

}
