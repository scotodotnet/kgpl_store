﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_Blanket_PurOrd_Det : System.Web.UI.Page
{
    
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;


    string RptName = ""; string DeptName = ""; string BlnkPurOrdNo = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        Page.Title = "ERP Stores Module :: Blanket Purchase Order Details";
        if (!IsPostBack)
        {
            Load_Data_Empty_Dept();
            Load_Data_Empty_BlnkPurOrdNo();
        }
       
        Load_Data_Empty_Supp1();
        
        Load_Data_Empty_ItemCode();
        
        Load_Data_Empty_ItemCode1();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDepartmentName.SelectedValue = "-Select-";
        //txtDeptCodeHide.Value = "";
        txtSupplierName.Text = "";
        txtSuppCodehide.Value = "";
        txtBPONo.SelectedValue = "-Select-";
        //txtBPONoDateHide.Value = "";
        txtItemName.Text = "";
        txtItemCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }

protected void btnReports_Click(object sender, EventArgs e)
    {
        RptName = "Blanket Purchase Order Details Report";

        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtBPONo.SelectedItem.Text != "-Select-")
        {
            BlnkPurOrdNo = txtBPONo.SelectedItem.Text;
        }
        else
        {
            BlnkPurOrdNo = "";
        }


        ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + txtSupplierName.Text + "&BlanketPONo=" + BlnkPurOrdNo + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
    }

protected void  btnInvcFormat_Click(object sender, EventArgs e)
{
    RptName = "Blanket Purchase Order Details Invoice Format";

    if (txtDepartmentName.SelectedItem.Text != "-Select-")
    {
        DeptName = txtDepartmentName.SelectedItem.Text;
    }
    else
    {
        DeptName = "";
    }
    if (txtBPONo.SelectedItem.Text != "-Select-")
    {
        BlnkPurOrdNo = txtBPONo.SelectedItem.Text;
    }
    else
    {
        BlnkPurOrdNo = "";
    }


    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + txtSupplierName.Text + "&BlanketPONo=" + BlnkPurOrdNo + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
}

protected void btnView_Click(object sender, EventArgs e)
{
    if (txtRequestStatus.SelectedItem.Text != "-select-")
    {
        RptName = "Blanket Purchase Order List";

        ResponseHelper.Redirect("ReportDisplay.aspx?Request_Str=" + txtRequestStatus.SelectedItem.Text + "&FromDate=" + txtFrmDate.Text + "&ToDate=" + txtTDate.Text + "&RptName=" + RptName, "_blank", "");
    }
    else
    {
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Status');", true);
    }
}

protected void btn_Clear_Click(object sender, EventArgs e)
{
    txtRequestStatus.SelectedValue = "0";
    txtFrmDate.Text="";
    txtTDate.Text="";
}


protected void btnDept_Click(object sender, EventArgs e)
{
    //modalPop_Dept.Show();
}
private void Load_Data_Empty_Dept()
{

    string query = "";
    DataTable Main_DT = new DataTable();

    txtDepartmentName.Items.Clear();
    query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    Main_DT = objdata.RptEmployeeMultipleDetails(query);
    txtDepartmentName.DataSource = Main_DT;
    DataRow dr = Main_DT.NewRow();
    dr["DeptCode"] = "-Select-";
    dr["DeptName"] = "-Select-";
    Main_DT.Rows.InsertAt(dr, 0);
    txtDepartmentName.DataTextField = "DeptName";
    txtDepartmentName.DataValueField = "DeptCode";
    txtDepartmentName.DataBind();

}

protected void GridViewClick_Dept(object sender, CommandEventArgs e)
{
    //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
    txtDepartmentName.Text = Convert.ToString(e.CommandName);
}

protected void btnItemCode_Click(object sender, EventArgs e)
{
    modalPop_ItemCode.Show();
}
private void Load_Data_Empty_ItemCode()
{

    string query = "";
    DataTable DT = new DataTable();

    DT.Columns.Add("ItemCode");
    DT.Columns.Add("ItemName");

    query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    DT = objdata.RptEmployeeMultipleDetails(query);

    Repeater_ItemCode.DataSource = DT;
    Repeater_ItemCode.DataBind();
    Repeater_ItemCode.Visible = true;

}

protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
{
    txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
    txtItemName.Text = Convert.ToString(e.CommandName);
}


protected void btnSupp1_Click(object sender, EventArgs e)
{
    modalPop_Supp1.Show();
}
private void Load_Data_Empty_Supp1()
{

    string query = "";
    DataTable DT = new DataTable();

    DT.Columns.Add("SuppCode");
    DT.Columns.Add("SuppName");

    query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    DT = objdata.RptEmployeeMultipleDetails(query);

    Repeater_Supplier1.DataSource = DT;
    Repeater_Supplier1.DataBind();
    Repeater_Supplier1.Visible = true;

}

protected void GridViewClick(object sender, CommandEventArgs e)
{
    txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
    txtSupplierName.Text = Convert.ToString(e.CommandName);


}


protected void btnBlnkPurOrdNo_Click(object sender, EventArgs e)
{
    //modalPop_BlnkPurOrdNo.Show();
}
private void Load_Data_Empty_BlnkPurOrdNo()
{

    string query = "";
    DataTable Main_DT = new DataTable();

    txtBPONo.Items.Clear();
    query = "Select Blanket_PO_No,Blanket_PO_Date from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
    query = query + " And Blanket_PO_Status='1'";
    Main_DT = objdata.RptEmployeeMultipleDetails(query);
    txtBPONo.DataSource = Main_DT;
    DataRow dr = Main_DT.NewRow();

    dr["Blanket_PO_No"] = "-Select-";
    Main_DT.Rows.InsertAt(dr, 0);
    txtBPONo.DataTextField = "Blanket_PO_No";

    txtBPONo.DataBind();


    //string query = "";
    //DataTable DT = new DataTable();

    //query = "Select Blanket_PO_No,Blanket_PO_Date from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
    //query = query + " And Blanket_PO_Status='1'";
    //DT = objdata.RptEmployeeMultipleDetails(query);

    //Repeater_BlnkPurOrdNo.DataSource = DT;
    //Repeater_BlnkPurOrdNo.DataBind();
    //Repeater_BlnkPurOrdNo.Visible = true;

}

protected void GridViewClick_BlnkPurOrdNo(object sender, CommandEventArgs e)
{
    txtBPONo.Text = Convert.ToString(e.CommandArgument);
    //txtBPONoDateHide.Value = Convert.ToString(e.CommandName);

}

protected void btnScheduleReport_Click(object sender, EventArgs e)
{
    RptName = "Blanket Purchase Order Schedule Report";

    ResponseHelper.Redirect("ReportDisplay.aspx?ItemName=" + txtItemName1.Text + "&RptName=" + RptName, "_blank", "");
}


protected void btnItemCode1_Click(object sender, EventArgs e)
{
    modalPop_ItemCode1.Show();
}
private void Load_Data_Empty_ItemCode1()
{

    string query = "";
    DataTable DT = new DataTable();

    DT.Columns.Add("ItemCode");
    DT.Columns.Add("ItemName");

    query = "Select ItemCode,ItemName from Blanket_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Schedule='True'";
    DT = objdata.RptEmployeeMultipleDetails(query);

    Repeater_ItemCode1.DataSource = DT;
    Repeater_ItemCode1.DataBind();
    Repeater_ItemCode1.Visible = true;

}

protected void GridViewClick_ItemCode1(object sender, CommandEventArgs e)
{
    txtItemCodeHide1.Value = Convert.ToString(e.CommandArgument);
    txtItemName1.Text = Convert.ToString(e.CommandName);
}


}
