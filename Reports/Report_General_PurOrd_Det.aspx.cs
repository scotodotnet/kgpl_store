﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_General_PurOrd_Det : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string GenPurOrdNo = ""; string DeptName = "";
    string ItemName = ""; string Supplier = "";
    


    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        Page.Title = "ERP Stores Module :: General Purchase Order Details";
        if (!IsPostBack)
        {
            Load_Data_Empty_Dept();

            Load_Data_Empty_GenPurOrdNo();
        }
     
        Load_Data_Empty_ItemCode();
        Load_Data_Empty_Supp1();
       
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDepartmentName.SelectedValue = "-Select-";
        //txtDeptCodeHide.Value = "";
        ddlSupplier.SelectedItem.Text = "-Select-";
        //txtSuppCodehide.Value = "";
        txtGenPurOrdNo.SelectedValue = "-Select-";
        //txtGenPurOrdDateHide.Value = "";
        ddlItemName.SelectedItem.Text = "";
        //txtItemCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        RptName = "General Purchase Order Details Report";

        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtGenPurOrdNo.SelectedItem.Text != "-Select-")
        {
            GenPurOrdNo = txtGenPurOrdNo.SelectedItem.Text;
        }
        else
        {
            GenPurOrdNo = "";
        }

        if (ddlSupplier.SelectedItem.Text != "-Select-")
        {
            Supplier = ddlSupplier.SelectedItem.Text;
        }
        else
        {
            Supplier = "";
        }

        if (ddlItemName.SelectedItem.Text != "-Select-")
        {
            ItemName = ddlItemName.SelectedItem.Text;
        }
        else
        {
            ItemName = "";
        }


        if (txtFromDate.Text != "" || txtToDate.Text != "")
        {
            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if(FDate<TDate)
            {
                ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + Supplier + "&GenPurOrdNo=" + GenPurOrdNo + "&SupQtNo=" + txtSPONo.Text + "&ItemName=" + ItemName + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }
            
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter FromDate And ToDate..');", true);
        }

        
    }
    protected void btnInvFormat_Click(object sender, EventArgs e)
    {
        string SupplierName = "";
        string ItemName = "";

        RptName = "General Purchase Order Invoice Format";
        //RptName = "General Purchase Order Pre-Print Format";
        string RateType = "With Rate";
        if (RdpPORateType.SelectedValue == "1")
        {
            RateType = "With Rate";
        }
        else
        {
            RateType = "Without Rate";
        }

        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtGenPurOrdNo.SelectedItem.Text != "-Select-")
        {
            GenPurOrdNo = txtGenPurOrdNo.SelectedItem.Text;
        }
        else
        {
            GenPurOrdNo = "";
        }

        if (ddlSupplier.SelectedItem.Text != "-Select-")
        {
            SupplierName = ddlSupplier.SelectedItem.Text;
        }
        else
        {
            SupplierName = "";
        }

        if (ddlItemName.SelectedItem.Text != "-Select-")
        {
            ItemName = ddlItemName.SelectedItem.Text;
        }
        else
        {
            ItemName = "";
        }


        ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&SupplierName=" + SupplierName + "&GenPurOrdNo=" + GenPurOrdNo + "&SupQtNo=" + txtSPONo.Text + "&ItemName=" + ItemName + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RateType=" + RateType + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        
        if (txtRequestStatus.SelectedItem.Text != "-select-")
        {
            RptName = "General Purchase Order List";

            ResponseHelper.Redirect("ReportDisplay.aspx?Request_Str=" + txtRequestStatus.SelectedItem.Text + "&FromDate=" + txtFrmDate.Text + "&ToDate=" + txtTDate.Text + "&RptName=" + RptName, "_blank", "");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Status');", true);
        }
    }
    protected void btn_Clear_Click(object sender, EventArgs e)
    {
        txtRequestStatus.SelectedValue = "0";
        txtFrmDate.Text = "";
        txtTDate.Text = "";
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
       // modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        //modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlItemName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataValueField = "ItemCode";

        ddlItemName.DataBind();

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        //txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        //ddlItemName.SelectedItem.Text = Convert.ToString(e.CommandName);
    }

    protected void btnSupp1_Click(object sender, EventArgs e)
    {
        //modalPop_Supp1.Show();
    }
    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlSupplier.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["SuppCode"] = "-Select-";
        dr["SuppName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlSupplier.DataTextField = "SuppName";
        ddlSupplier.DataValueField = "SuppCode";
        ddlSupplier.DataBind();
    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        //txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
        //ddlSupplier.SelectedItem.Text = Convert.ToString(e.CommandName);


    }

    protected void btnGenPurOrdNo_Click(object sender, EventArgs e)
    {
       // modalPop_GenPurOrdNo.Show();
    }
    private void Load_Data_Empty_GenPurOrdNo()
    {



        string query = "";
        DataTable Main_DT = new DataTable();

        txtGenPurOrdNo.Items.Clear();
        query = "Select Gen_PO_No,Gen_PO_Date from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And PO_Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtGenPurOrdNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
       
        dr["Gen_PO_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtGenPurOrdNo.DataTextField = "Gen_PO_No";
        
        txtGenPurOrdNo.DataBind();




        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Gen_PO_No,Gen_PO_Date from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And PO_Status='1'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_GenPurOrdNo.DataSource = DT;
        //Repeater_GenPurOrdNo.DataBind();
        //Repeater_GenPurOrdNo.Visible = true;

    }

    protected void GridViewClick_GenPurOrdNo(object sender, CommandEventArgs e)
    {
        txtGenPurOrdNo.Text = Convert.ToString(e.CommandArgument);
        //txtGenPurOrdDateHide.Value = Convert.ToString(e.CommandName);

    }


}
