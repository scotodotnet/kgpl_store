﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class Reports_Supplier_Quotation_Crystalpage : System.Web.UI.Page
{
    
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionBlanketPOOrderNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SSQL;
    string SupplierName;
    string ItemName;
    string FDate;
    string TDate;
    DateTime FromDate;
    DateTime ToDate;
    string QNumber;
    string ReportName;
    DataTable AutoDataTable = new DataTable();
    DataSet ds = new DataSet();



    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        //if (!IsPostBack)
        //{
        //    Page.Title = "ERP Stores Module :: Purchase Enquiry";
        //    HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
        //    li.Attributes.Add("class", "droplink active open");
        //}

        SupplierName = Request.QueryString["SupplierName"].ToString();
        ItemName = Request.QueryString["ItemName"].ToString();
        FDate = Request.QueryString["FDate"].ToString();
        TDate = Request.QueryString["TDate"].ToString();
        ReportName = Request.QueryString["ReportName"].ToString();
        QNumber = Request.QueryString["QNumber"].ToString();


        if (ReportName.ToString() == "SUPPLIER QUOTATION REPORT")
        {

            if (FDate.ToString() != "")
            {
                FromDate = Convert.ToDateTime(FDate.ToString());
            }
            if (TDate != "")
            {
                ToDate = Convert.ToDateTime(TDate.ToString());
            }
            DataTable SupplierDetails = new DataTable();

            if (SupplierName.ToString() != "" && FDate.ToString() != "" && ToDate.ToString() != "")
            {
                SSQL = "select main.QuotNo,main.QuotDate,main.SuppCode,main.SuppName,main.SuppQutNo,";
                SSQL = SSQL + " main.SuppQutDate,main.RefDocNo, main.RefDocDate , main.PaymentTerms,";
                SSQL = SSQL + " main.PaymentMode,main.DeliveryDet,sub.ItemCode,sub.ItemName,sub.UOMCode,sub.Rate FROM Supp_Qut_Main main";
                SSQL = SSQL + " INNER JOIN Supp_Qut_Main_Sub sub ON main.QuotNo = sub.QuotNo where main.QuotNo = '" + QNumber.ToString() + "' and";
                SSQL = SSQL + " sub.QuotNo = '" + QNumber.ToString() + "' and sub.Ccode = '" + SessionCcode + "' and sub.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and main.Ccode = '" + SessionCcode + "' and main.Lcode='" + SessionLcode + "' and main.Supp_Qtn_Status='1'";

                if (SupplierName.ToString() != "")
                {
                    SSQL = SSQL + " and main.SuppName = '" + SupplierName.ToString() + "'";
                }
                if (ItemName.ToString() != "")
                {
                    SSQL = SSQL + " and sub.ItemName = '" + ItemName.ToString() + "'";
                }


                SSQL = SSQL + " and CONVERT(DATETIME, main.QuotDate, 103 ) >= CONVERT(DATETIME,'" + FromDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " and CONVERT(DATETIME, main.QuotDate, 103 ) <= CONVERT(DATETIME,'" + ToDate.ToString("dd/MM/yyyy") + "',103)";
                SupplierDetails = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (FDate.ToString() != "" && TDate.ToString() != "" && QNumber.ToString() == "")
            {
                SSQL = "select main.QuotNo,main.QuotDate,main.SuppCode,main.SuppName,main.SuppQutNo,";
                SSQL = SSQL + " main.SuppQutDate,main.RefDocNo, main.RefDocDate , main.PaymentTerms,";
                SSQL = SSQL + " main.PaymentMode,main.DeliveryDet,sub.ItemCode,sub.ItemName,sub.UOMCode,sub.Rate FROM Supp_Qut_Main main";
                SSQL = SSQL + " INNER JOIN Supp_Qut_Main_Sub sub ON main.QuotDate = sub.QuotDate where";
                SSQL = SSQL + " sub.Ccode = '" + SessionCcode + "' and sub.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and main.Ccode = '" + SessionCcode + "' and main.Lcode='" + SessionLcode + "' and main.Supp_Qtn_Status='1'";


                if (SupplierName.ToString() != "")
                {
                    SSQL = SSQL + " and main.SuppName = '" + SupplierName.ToString() + "'";
                }
                if (ItemName.ToString() != "")
                {
                    SSQL = SSQL + " and sub.ItemName = '" + ItemName.ToString() + "'";
                }

                SSQL = SSQL + " and  CONVERT(DATETIME, main.QuotDate, 103 ) >= CONVERT(DATETIME,'" + FromDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " and CONVERT(DATETIME, main.QuotDate, 103 ) <= CONVERT(DATETIME,'" + ToDate.ToString("dd/MM/yyyy") + "',103)";

                SupplierDetails = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (FDate.ToString() == "" && TDate.ToString() == "" && QNumber.ToString() != "")
            {
                SSQL = "select main.QuotNo,main.QuotDate,main.SuppCode,main.SuppName,main.SuppQutNo,";
                SSQL = SSQL + " main.SuppQutDate,main.RefDocNo, main.RefDocDate, main.PaymentTerms,";
                SSQL = SSQL + " main.PaymentMode,main.DeliveryDet,sub.ItemCode,sub.ItemName,sub.UOMCode,sub.Rate FROM Supp_Qut_Main main";
                SSQL = SSQL + " INNER JOIN Supp_Qut_Main_Sub sub ON main.QuotNo = sub.QuotNo where main.QuotNo = '" + QNumber.ToString() + "' and";
                SSQL = SSQL + " sub.QuotNo = '" + QNumber.ToString() + "' and sub.Ccode = '" + SessionCcode + "' and sub.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and main.Ccode = '" + SessionCcode + "' and main.Lcode='" + SessionLcode + "' and main.Supp_Qtn_Status='1'";

                if (SupplierName.ToString() != "")
                {
                    SSQL = SSQL + " and main.SuppName = '" + SupplierName.ToString() + "'";
                }
                if (ItemName.ToString() != "")
                {
                    SSQL = SSQL + " and sub.ItemName = '" + ItemName.ToString() + "'";
                }

                SupplierDetails = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FDate.ToString() == "" && TDate.ToString() == "" && QNumber.ToString() == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Fill The Details');", true);
            }

            if (SupplierDetails.Rows.Count > 0)
            {


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,District,Taluk,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDataTable.Columns.Add("CompanyName");
                AutoDataTable.Columns.Add("LocationName");
                AutoDataTable.Columns.Add("CompanyCode");
                AutoDataTable.Columns.Add("LocationCode");
                AutoDataTable.Columns.Add("Address1");
                AutoDataTable.Columns.Add("Address2");
                AutoDataTable.Columns.Add("QuotationNo");
                AutoDataTable.Columns.Add("QuotationDate");
                AutoDataTable.Columns.Add("SupplierCode");
                AutoDataTable.Columns.Add("SupplierName");
                AutoDataTable.Columns.Add("SupplierQuNo");
                AutoDataTable.Columns.Add("SupplierQuDate");
                AutoDataTable.Columns.Add("ReferenceDocNo");
                AutoDataTable.Columns.Add("ReferenceDocDate");
                AutoDataTable.Columns.Add("PaymentTerms");
                AutoDataTable.Columns.Add("Paymentmode");
                AutoDataTable.Columns.Add("DeliveryDate");

                AutoDataTable.Columns.Add("ItemCode");
                AutoDataTable.Columns.Add("ItemName");
                AutoDataTable.Columns.Add("UomCode");
                AutoDataTable.Columns.Add("Rate");


                for (int i = 0; i < SupplierDetails.Rows.Count; i++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDataTable.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDataTable.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDataTable.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDataTable.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();


                    AutoDataTable.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
                    AutoDataTable.Rows[i]["QuotationNo"] = SupplierDetails.Rows[i]["QuotNo"];
                    AutoDataTable.Rows[i]["QuotationDate"] = SupplierDetails.Rows[i]["QuotDate"];
                    AutoDataTable.Rows[i]["SupplierCode"] = SupplierDetails.Rows[i]["SuppCode"];
                    AutoDataTable.Rows[i]["SupplierName"] = SupplierDetails.Rows[i]["SuppName"];

                    AutoDataTable.Rows[i]["SupplierQuNo"] = SupplierDetails.Rows[i]["SuppQutNo"];
                    AutoDataTable.Rows[i]["SupplierQuDate"] = SupplierDetails.Rows[i]["SuppQutDate"];
                    AutoDataTable.Rows[i]["ReferenceDocNo"] = SupplierDetails.Rows[i]["RefDocNo"];
                    AutoDataTable.Rows[i]["ReferenceDocDate"] = SupplierDetails.Rows[i]["RefDocDate"];
                    AutoDataTable.Rows[i]["PaymentTerms"] = SupplierDetails.Rows[i]["PaymentTerms"];

                    AutoDataTable.Rows[i]["Paymentmode"] = SupplierDetails.Rows[i]["PaymentMode"];
                    AutoDataTable.Rows[i]["DeliveryDate"] = SupplierDetails.Rows[i]["DeliveryDet"];
                    AutoDataTable.Rows[i]["ItemCode"] = SupplierDetails.Rows[i]["ItemCode"];
                    AutoDataTable.Rows[i]["ItemName"] = SupplierDetails.Rows[i]["ItemName"];
                    AutoDataTable.Rows[i]["UomCode"] = SupplierDetails.Rows[i]["UOMCode"];
                    AutoDataTable.Rows[i]["Rate"] = SupplierDetails.Rows[i]["Rate"];

                }

                ds.Tables.Add(AutoDataTable);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("~/Crystal/SupplierQuotationReport.rpt"));
                report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                CrystalReportViewer1.ReportSource = report;

               // CrystalReportViewer1.RefreshReport();

                //ExportOptions CrExportOptions;
                //DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                //PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                //CrDiskFileDestinationOptions.DiskFileName = "E:\\Report\\SupplierQuotationReport.pdf";
                //CrExportOptions = report.ExportOptions;
                //{
                //    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                //    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                //    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                //    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                //}
                //report.Export();

            }



        }






    }
}
