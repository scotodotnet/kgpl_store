﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;



using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_StockDetails : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    bool ErrFlag = false;



    string RptName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        Page.Title = "ERP Stores Module :: Stock Details Report";
        if (!IsPostBack)
        {
            Load_Data_Empty_CostCenterName();
            Load_Data_Empty_Dept();
            Load_Data_Empty_WareHouse();

            Load_Data_Empty_ItemCode();
        }



    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlWarehouse.SelectedItem.Text = "-Select-";

        ddlItemName.SelectedItem.Text = "-Select-";

        txtFromDate.Text = ""; txtToDate.Text = "";
    }
    private void Load_Data_Empty_CostCenterName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtCostCenterName.Items.Clear();
        query = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCostCenterName.DataSource = Main_DT;

        DataRow dr = Main_DT.NewRow();
        dr["CostcenterCode"] = "-Select-";
        dr["CostcenterName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCostCenterName.DataTextField = "CostcenterName";
        txtCostCenterName.DataValueField = "CostcenterCode";
        txtCostCenterName.DataBind();


    }

    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();
    }
    protected void btnReports_Click(object sender, EventArgs e)
    {
        RptName = "Stock Details Report";

        ResponseHelper.Redirect("ReportDisplay.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&DeptName=" + txtDepartmentName.SelectedItem.Text + "&CostCenterName=" + txtCostCenterName.SelectedItem.Text + "&WareHouse=" + ddlWarehouse.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&RptName=" + RptName, "_blank", "");
    }
    protected void btnCriticalStck_Click(object sender, EventArgs e)
    {
        RptName = "Critical Stock Details Report";

        ResponseHelper.Redirect("ReportDisplay.aspx?WareHouse=" + ddlWarehouse.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&DeptName=" + txtDepartmentName.SelectedItem.Text + "&CostCenterName=" + txtCostCenterName.SelectedItem.Text + "&RptName=" + RptName, "_blank", "");
    }
    protected void btnNonMvbleItem_Click(object sender, EventArgs e)
    {
        RptName = "Non Movable Item Details Report";

        ResponseHelper.Redirect("ReportDisplay.aspx?WareHouse=" + ddlWarehouse.SelectedItem.Text + "&DeptName=" + txtDepartmentName.SelectedItem.Text + "&CostCenterName=" + txtCostCenterName.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        //modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable DT = new DataTable();


        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlWarehouse.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlWarehouse.DataTextField = "WarehouseName";
        ddlWarehouse.DataValueField = "WarehouseCode";


        ddlWarehouse.DataBind();


    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        //ddlWarehouse.SelectedItem.Text = Convert.ToString(e.CommandName);

    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        //modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlItemName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataValueField = "ItemCode";

        ddlItemName.DataBind();
    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        //txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        //    ddlItemName.SelectedItem.Text = Convert.ToString(e.CommandName);
    }


    protected void btnSamplereport_Click(object sender, EventArgs e)
    {
        RptName = "Sample Stock Details Report";

        ResponseHelper.Redirect("ReportDisplay.aspx?CostName=" + txtCostCenterName.SelectedItem.Text + " &DeptName=" + txtDepartmentName.SelectedItem.Text + " &FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&WareHouse=" + ddlWarehouse.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&RptName=" + RptName, "_blank", "");
    }
    protected void btnStockReports_Click(object sender, EventArgs e)
    {
        RptName = "Sample Stock and Issue Details Report";
        ResponseHelper.Redirect("ReportDisplay.aspx?CostName=" + txtCostCenterName.SelectedItem.Text + " &FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&WareHouse=" + ddlWarehouse.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&RptName=" + RptName, "_blank", "");
    }
    protected void btnSummary_Click(object sender, EventArgs e)
    {
        RptName = "Summary Stock Details Report";
        ResponseHelper.Redirect("ReportDisplay.aspx?CostName=" + txtCostCenterName.SelectedItem.Text + " &FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&WareHouse=" + ddlWarehouse.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnOldStockReport_Click(object sender, EventArgs e)
    {
        RptName = "Old Stock Details Report";
        ResponseHelper.Redirect("ReportDisplay.aspx?CostName=" + txtCostCenterName.SelectedItem.Text + " &DeptName=" + txtDepartmentName.SelectedItem.Text + " &FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&WareHouse=" + ddlWarehouse.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnDeptWise_Click(object sender, EventArgs e)
    {
        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {

            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if (FDate < TDate)
            {
                RptName = "Dept Stock Details";
                ResponseHelper.Redirect("ReportDisplay.aspx?CostName=" + txtCostCenterName.SelectedItem.Text + " &DeptName=" + txtDepartmentName.SelectedItem.Text + " &FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&WareHouse=" + ddlWarehouse.SelectedItem.Text + "&ItemName=" + ddlItemName.SelectedItem.Text + "&RptName=" + RptName, "_blank", "");
            }

            else
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Select Correct Date..');", true);
            }
        }
        else
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Select Date..');", true);
        }
    }
    protected void btnDeptSummaryExcl_Click(object sender, EventArgs e)
    {
        DataGrid grid = new DataGrid();

        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {

            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if (FDate <= TDate)
            {
                DataTable da_check_Dept = new DataTable();
                DataTable da_Check_ItemName = new DataTable();
                DataTable AutoDataTable = new DataTable();
                string SSQL = "";


                SSQL = "delete from Print_stock_details ";
                da_Check_ItemName = objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "select A.DeptName ,";
                SSQL = SSQL + "sum(A.Open_Qty) as [Open_Qty],sum(A.Open_Rate) as [Open_Rate] ,";
                SSQL = SSQL + "sum(A.Open_Qty* A.Open_Rate) as [Open_Value],";
                SSQL = SSQL + "sum(A.Pur_Qty) as [Pur_Qty] ,sum(A.Pur_Rate) as [Pur_Rate],";
                SSQL = SSQL + "sum(A.Pur_Qty* A.Pur_Rate)as [Pur_Value],";
                SSQL = SSQL + "sum(A.Open_Qty + A.Pur_Qty) as [TotalQty] , sum(A.Open_Rate + A.Pur_Rate) as [TotalValue],";
                SSQL = SSQL + "sum(A.Iss_Qty) as [Iss_Qty] ,sum( A.Iss_Rate) as [Iss_Rate],";
                SSQL = SSQL + "sum(A.Iss_Qty * A.Iss_Rate) as [Iss_Value],";
                SSQL = SSQL + "sum((A.Open_Qty + A.Pur_Qty) - A.Iss_Qty) as [Stock_Qty],sum((A.Open_Rate + A.Pur_Rate) - A.Iss_Rate) as [Stock_Value] ";
                SSQL = SSQL + " From ( ";
                SSQL = SSQL + "select SLA.DeptName as[DeptName],(SUM(SLA.Add_Qty) - SUM(Minus_Qty)) as Open_Qty, (sum(SLA.Add_Value) - SUM(SLA.Minus_Value))Open_Rate,";
                SSQL = SSQL + "0 Pur_Qty, 0 as Pur_Rate, 0 Iss_Qty, 0 Iss_Rate From Stock_Ledger_All SLA ";
                SSQL = SSQL + "Where SLA.Ccode='" + SessionCcode + "' And SLA.Lcode='" + SessionLcode + "' And SLA.FinYearCode='" + SessionFinYearCode + "'";

                if (txtDepartmentName.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + "And SLA.DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
                }

                if (ddlWarehouse.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + "And SLA.WarehouseName='" + ddlWarehouse.SelectedItem.Text + "'";
                }

                SSQL = SSQL + " And CONVERT(varchar,SLA.Trans_Date, 103) < CONVERT(varchar, '" + txtFromDate.Text.ToString() + "', 103) Group By SLA.DeptName ";

                SSQL = SSQL + "Union All ";

                SSQL = SSQL + "select SLA.DeptName as[DeptName],0 Open_Rate ,0 Open_Rate,";
                SSQL = SSQL + "SUM(SLA.Add_Qty) Pur_Qty,sum(SLA.Add_Value)Pur_Rate,sum(SLA.Minus_Qty)Iss_Qty,SUM(SLA.Minus_Value)Iss_Rate From Stock_Ledger_All SLA ";
                SSQL = SSQL + "Where SLA.Ccode='" + SessionCcode + "' And SLA.Lcode='" + SessionLcode + "' And SLA.FinYearCode='" + SessionFinYearCode + "'";

                if (txtDepartmentName.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + "And SLA.DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
                }

                if (ddlWarehouse.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + "And SLA.WarehouseName='" + ddlWarehouse.SelectedItem.Text + "'";
                }

                SSQL = SSQL + " And CONVERT(varchar,SLA.Trans_Date, 103) >= CONVERT(varchar, '" + txtFromDate.Text.ToString() + "', 103) ";
                SSQL = SSQL + " And CONVERT(varchar,SLA.Trans_Date, 103) <= CONVERT(varchar, '" + txtToDate.Text.ToString() + "', 103) ";
                SSQL = SSQL + "Group By SLA.DeptName)A Group By A.DeptName ";

                da_check_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

                if(da_check_Dept.Rows.Count>0)
                {
                    grid.DataSource = da_check_Dept;
                    grid.DataBind();

                    string attachment = "attachment;filename=Machine_Wise_Report.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";
                    grid.HeaderStyle.Font.Bold = true;
                    System.IO.StringWriter stw = new System.IO.StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    grid.RenderControl(htextw);

                    Response.Write("<table>");

                    Response.Write("<tr Font-Bold='true' font-Size='30' align='center'>");
                    Response.Write("<a style=\"font-weight:bold\">Kin</a>");
                    Response.Write("--");
                    Response.Write("<a style=\"font-weight:bold\" > " + SessionLcode + "</a>");
                    Response.Write("</td>");
                    Response.Write("</tr>");

                    Response.Write("<tr Font-Bold='true' align='center'>");
                    Response.Write("<a style=\"font-weight:bold\">Stock Report  </a>");
                    Response.Write("  ");

                    Response.Write("</td>");
                    Response.Write("</tr>");

                    Response.Write("<tr Font-Bold='true' align='center'>");

                    //Response.Write("<a style=\"font-weight:bold\"> FROM  -" + FromDate + "</a>");
                    //Response.Write("&nbsp;&nbsp;&nbsp;");
                    //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
                    Response.Write("  ");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("</table>");
                    Response.Write(stw.ToString());
                    Response.End();
                    Response.Clear();

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No More Records');", true);
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Select Correct Date');", true);
            }

        }
        else
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Select Date..');", true);
        }
    }

    protected void btnDeptSummary_Click(object sender, EventArgs e)
    {
        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {

            DateTime FDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime TDate = Convert.ToDateTime(txtToDate.Text);

            if (FDate <= TDate)
            {
                RptName = "Dept Summary Details";
                ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + txtDepartmentName.SelectedItem.Text + " &FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&WareHouse=" + ddlWarehouse.SelectedItem.Text + "&RptName=" + RptName, "_blank", "");
            }
            else
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Select Correct Date..');", true);
            }
        }
        else
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Select Date..');", true);
        }

    }
}
