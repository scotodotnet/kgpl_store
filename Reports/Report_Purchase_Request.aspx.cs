﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_Purchase_Request : System.Web.UI.Page
{
   
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string DeptName = ""; string PurReqNo = ""; string RequestBy = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

         Page.Title = "ERP Stores Module :: Purchase Request Report";
         if (!IsPostBack)
         {
             Load_Data_Empty_Dept();
             Load_Data_Empty_PurReqNo();
             Load_Data_Empty_RequestBy();
         }

        Load_Data_Empty_ItemCode();
        
        
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtWarehouseName.Text = "";
        txtWarehouseCodeHide.Value = "";
        txtDepartmentName.SelectedValue = "-Select-";
        //txtDeptCodeHide.Value = "";
        txtPurReqNo.SelectedValue = "-Select-";
        //txtPurReqDateHide.Value = "";
        txtReqBy.SelectedValue = "-Select-";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtItemCodeHide.Value = "";
        txtItemName.Text = "";

    }

    protected void btnReports_Click(object sender, EventArgs e)
    {
        RptName = "Purchase Request Report";

        if (txtPurReqNo.SelectedItem.Text != "-Select-")
        {
            PurReqNo = txtPurReqNo.SelectedItem.Text;
        }
        else
        {
            PurReqNo = "";
        }
        if (txtReqBy.SelectedItem.Text != "-Select-")
        {
            RequestBy = txtReqBy.SelectedItem.Text;
        }
        else
        {
            RequestBy = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }


        ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + DeptName + "&PurReqNo=" + PurReqNo + "&Requestby=" + RequestBy + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnPurReqNo_Click(object sender, EventArgs e)
    {
        //modalPop_PurReqNo.Show();
    }
    private void Load_Data_Empty_PurReqNo()
    {


        string query = "";
        DataTable Main_DT = new DataTable();

        txtPurReqNo.Items.Clear();
        query = "Select Pur_Request_No,Pur_Request_Date from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
      
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtPurReqNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Pur_Request_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtPurReqNo.DataTextField = "Pur_Request_No";

        txtPurReqNo.DataBind();




        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Pur_Request_No,Pur_Request_Date from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
      
        //DT = objdata.RptEmployeeMultipleDetails(query);


        //Repeater_PurReqNo.DataSource = DT;
        //Repeater_PurReqNo.DataBind();
        //Repeater_PurReqNo.Visible = true;

    }

    protected void GridViewClick_PurReqNo(object sender, CommandEventArgs e)
    {
        txtPurReqNo.Text = Convert.ToString(e.CommandArgument);
       // txtPurReqDateHide.Value = Convert.ToString(e.CommandName);
    }
    protected void btnRequestBy_Click(object sender, EventArgs e)
    {
        //modalPop_RequestBy.Show();
    }
    private void Load_Data_Empty_RequestBy()
    {


        string query = "";
        DataTable Main_DT = new DataTable();

        txtReqBy.Items.Clear();
        query = "Select Requestby from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
       
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtReqBy.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Requestby"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtReqBy.DataTextField = "Requestby";

        txtReqBy.DataBind();



        
        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Requestby from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
       
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_RequestBy.DataSource = DT;
        //Repeater_RequestBy.DataBind();
        //Repeater_RequestBy.Visible = true;

    }

    protected void GridViewClick_RequestBy(object sender, CommandEventArgs e)
    {
        txtReqBy.Text = Convert.ToString(e.CommandArgument);

    }


    protected void btnView_Click(object sender, EventArgs e)
    {
        if (txtRequestStatus.SelectedItem.Text != "-select-")
        {
            RptName = "Purchase Request List";

            ResponseHelper.Redirect("ReportDisplay.aspx?Request_Str=" + txtRequestStatus.SelectedItem.Text + "&FromDate=" + txtFrmDate.Text + "&ToDate=" + txtTDate.Text + "&RptName=" + RptName, "_blank", "");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Status');", true);
        }
    }
    protected void btn_Clear_Click(object sender, EventArgs e)
    {
        txtRequestStatus.SelectedValue = "0";
        txtFrmDate.Text = "";
        txtTDate.Text = "";
    }


}
