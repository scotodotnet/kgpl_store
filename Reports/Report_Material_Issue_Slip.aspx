<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Report_Material_Issue_Slip.aspx.cs" Inherits="Reports_Report_Material_Issue_Slip" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#WareHouse').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#WareHouse').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Dept').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Dept').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#MatIssNo').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#MatIssNo').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Issuedby').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Issuedby').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable').dataTable();               
            }
        });
    };
</script>



<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
   $find('WareHouse_Close').hide();
   $find('Dept_Close').hide();
   $find('MatIssNo_Close').hide();
   $find('Issuedby_Close').hide();
   $find('Item_Close').hide();
      }
   } 
 </script>
<!--Ajax popup End-->


<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Material Issue Slip</li></h4> 
    </ol>
</div>

<div id="main-wrapper" class="container">
    <div class="row">
        <div class="col-md-12">        
            <div class="col-md-9">
			    <div class="panel panel-white">
			        <div class="panel panel-primary">
				        <div class="panel-heading clearfix">
					        <h4 class="panel-title">Material Issue Slip</h4>
				        </div>
				    </div>
				    <form class="form-horizontal">
				        <div class="panel-body">
					        <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Warehouse</label>
					            
					            <asp:DropDownList ID="txtWarehouseName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					            
					          
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Department</label>
					            <asp:DropDownList ID="txtDepartmentName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					            
					           
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Mat.Issue No</label>
					            
					            <asp:DropDownList ID="txtMatIssNo" runat="server" class="js-states form-control">
                          </asp:DropDownList>
					            
					          
					        </div>
					        </div>
					        </div>

                            <div class="col-md-12">
					            <div class="row">
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">IssuedBy</label>
					                    <asp:DropDownList ID="txtIssuedby" runat="server" class="js-states form-control"></asp:DropDownList>
					                </div>
					                <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Name</label>
					            <asp:DropDownList ID="ddlItemName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					        </div>
					                <div class="form-group col-md-4">
					            <label for="exampleInputName">Cost Center Name</label>
					            <asp:DropDownList ID="txtCostCenterName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					           
                                </div> 
					            </div>
                            </div>
					        
                            <div class="col-md-12">
					    <div class="row">
                            <div class="form-group col-md-4">
					            <label for="exampleInputName">OtherUnit Name</label>
					            <asp:DropDownList ID="ddlOtherUnit" runat="server" class="js-states form-control">
                                </asp:DropDownList>
                            </div> 
                            <div class="form-group col-md-4">
					            <label for="exampleInputName">ItemType</label>
					            <asp:DropDownList ID="ddlItemType" runat="server" class="js-states form-control">
                                </asp:DropDownList>   
                            </div> 

					     <div class="form-group col-md-2">
					            <label for="exampleInputName">From Date</label>
					            <asp:TextBox ID="txtFromDate" MaxLength="20" class="form-control date-picker" runat="server" autocomplete="off"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderFromDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtFromDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">To Date</label>
					            <asp:TextBox ID="txtToDate" class="form-control date-picker" runat="server" autocomplete="off"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderToDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtToDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        </div> 
					        </div> 
                            <div class="clearfix"></div>
					        <div class="form-group row"></div>	
                            <!-- Button start -->
                            <div class="txtcenter">
                                <asp:Button ID="btnSlipReports" class="btn-success"  runat="server" Text="Slip Report" onclick="btnSlipReports_Click" height="32" Width="120" />
                                <asp:Button ID="btnDetReports" class="btn-success"  runat="server" Text="Details Report" onclick="btnDetReports_Click" height="32" Width="120" />
                                <asp:Button ID="btnItemWiseReport" class="btn-success"  runat="server" Text="Issue Item" onclick="btnItemWiseReport_Click"  height="32" Width="120"/>
                                <asp:Button ID="btnDeptWiseReport" class="btn-success"  runat="server" Text="Department Wise" onclick="btnDeptWiseReport_Click"  height="32" Width="120"/>
                                <asp:Button ID="btnCostCenterWiseReport" class="btn-success"  runat="server" Text="CostCenter Wise" onclick="btnCostCenterWiseReport_Click"  height="32" Width="120"/>
                                <asp:Button ID="btnClear" class="btn-success" runat="server"    Text="Clear" onclick="btnClear_Click" height="32" Width="120" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group row"></div>
                            <div class="txtcenter">
                                <asp:Button ID="btnApp_Cancel" class="btn-success" runat="server" Text="Approval_Cancel"  height="32" Width="120" onclick="btnApp_Cancel_Click" />
                                <asp:Button ID="btnSamplereport" class="btn-success" runat="server" Text="SampleReport"  height="32" Width="120" onclick="btnSamplereport_Click"/>
                                <asp:Button ID="btnItemType" class="btn-success" runat="server" Text="ItemType Wise"  height="32" Width="120" OnClick="btnItemType_Click"/>
                                <asp:Button ID="btnOtherUnitRpt" class="btn-success" runat="server" Text="Other's Unit"  height="32" Width="120" OnClick="btnOtherUnitRpt_Click"/>
                                <asp:Button ID="btnSummary" class="btn-success" runat="server" Text="Summary"  height="32" Width="120" onclick="btnSummary_Click"   Visible="false"/>
                            </div>
                            <!-- Button end -->       
				        </div><!-- panel body end -->
				    </form>
			    </div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    <!-- Dashboard start -->
		    <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  

            <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
	
        </div><!-- col 12 end -->
    </div><!-- row end -->
</div><!-- main-wrapper end -->

</asp:Content>

