﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_SingleItem_History : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";


    protected void Page_Load(object sender, EventArgs e)
    {

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();


        Page.Title = "ERP Stores Module :: Single Item Full History Details Report";

        

        Load_Data_Empty_ItemCode();
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        
        txtItemName.Text = "";
        txtItemCodeHide.Value = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }
    protected void btnReports_Click(object sender, EventArgs e)
    {
        if (txtItemName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Select ItemName..');", true);
        }
        else
        {

            RptName = "Single Item History Full Details Report";

            ResponseHelper.Redirect("ReportDisplay.aspx?ItemName=" + txtItemCodeHide.Value + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&RptName=" + RptName, "_blank", "");
        }
    }


    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }

}
