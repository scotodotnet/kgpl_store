﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

using Altius.BusinessAccessLayer.BALDataAccess;



public partial class Reports_ReportDisplay : System.Web.UI.Page
{
    string SSQL = "";
    BALDataAccess objdata = new BALDataAccess();
    ReportDocument RD_Report = new ReportDocument();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    bool Errflag;

    DataTable PurEnqrDT = new DataTable();
    DataTable PurRqAmendDT = new DataTable();
    DataTable PurRqDT = new DataTable();
    DataTable MatIssDT = new DataTable();
    DataTable MatIssRetDT = new DataTable();
    DataTable MatReqDT = new DataTable();
    DataTable GPOutDT = new DataTable();
    DataTable GPINDT = new DataTable();
    DataTable StdPurDT = new DataTable();
    DataTable GenPurDT = new DataTable();
    DataTable OpenRecpDT = new DataTable();
    DataTable BlanketPODT = new DataTable();
    DataTable POReceiptDT = new DataTable();
    DataTable GenReceiptDT = new DataTable();
    DataTable StockTransDT = new DataTable();
    DataTable StockDetDT = new DataTable();
    DataTable CriticalStockDT = new DataTable();
    DataTable NonMovableItemDT = new DataTable();
    DataTable SingleItemDT = new DataTable();
    DataTable PurchaseReturn = new DataTable();
    DataTable BlnkPODT = new DataTable();
    DataTable ClosingStckDT = new DataTable();


    DateTime frmDate; DateTime toDate;

    DataTable AutoDataTable = new DataTable();
    DataTable AutoDTPurRqAmend = new DataTable();
    DataTable AutoDTPurRq = new DataTable();
    DataTable AutoDTMatIss = new DataTable();
    DataTable AutoDTMatRet = new DataTable();
    DataTable AutoDTMatReq = new DataTable();
    DataTable AutoDTGPOut = new DataTable();
    DataTable AutoDTGPIN = new DataTable();
    DataTable AutoDTStdPur = new DataTable();
    DataTable AutoDTGenPur = new DataTable();
    DataTable AutoDTOpnRecp = new DataTable();
    DataTable AutoDTBlanketPO = new DataTable();
    DataTable AutoDTPOReceipt = new DataTable();
    DataTable AutoDTGenReceipt = new DataTable();
    DataTable AutoDTStockTrans = new DataTable();
    DataTable AutoDTStockDetails = new DataTable();
    DataTable AutoDTCriticalStockDetails = new DataTable();
    DataTable AutoDTNonMvbleItem = new DataTable();
    DataTable AutoDTSingleItem = new DataTable();
    DataTable AutoDataTable1 = new DataTable();
    DataTable AutoDataTable2 = new DataTable();
    DataTable AutoDataTable_Pend = new DataTable();

    DataSet ds = new DataSet();

    string FromDate = ""; string ToDate = ""; string RptName = "";
    string PENo = ""; string MatIssRetNo = ""; string MatIssNo = "";
    string SupplierName = ""; string ItemName = ""; string UPRNumber = ""; string OtherUnit = ""; string ItemType = "";
    string Supplier_Code = ""; string Transaction_No = "";
    string AMNo = ""; string PurRqNo = ""; string PurReqNo = "";
    string PO_Rate_Type = "With Rate";
    string AmendBy = ""; string Requestby = ""; string MatIssBy = "";
    string DeptName = ""; string WareHouse = ""; string ButtonName = ""; string PartyName = "";
    string MatRetBy = ""; string BlanketPONo = ""; string PORNumber = "";
    string MatReqNo = ""; string GenPurOrdNo = ""; string OpnRecNo = "";
    string MatPreBy = ""; string StdPurOrdNo = ""; string SupQtNo = ""; string CostCenter = "";
    string GPOutNo = ""; string GPOutIssBy = ""; string GPINNo = ""; string CostCenterName = "";
    string StckTranNo = ""; string TransType = ""; string PurRetNo = ""; string Request_Str = "";
    static decimal closingstock; string AdjustmentBy = ""; string AdjustmentNo = ""; string DamageEntryNo = "";
    string SparesReSalesNo = ""; string GensetNo = ""; string ScrabEntryNo = ""; string SENo = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        lblUploadSuccessfully.Text = "";
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        RptName = Request.QueryString["RptName"].ToString();

        if (RptName == "PurchaseEnquiryReport")
        {
            SupplierName = Request.QueryString["SupplierName"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            PENo = Request.QueryString["PENo"].ToString();

            PurchaseEnquiryReport();

        }

        else if (RptName == "Old Stock Details Report")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            CostCenter = Request.QueryString["CostName"].ToString();
            Old_Stock_Reports();
        }

        else if (RptName == "Dept Stock Details")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            CostCenter = Request.QueryString["CostName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            //CostCenter = Request.QueryString["CostName"].ToString();
            Dept_Stock_Details();
        }

        else if (RptName == "Dept Summary Details")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Dept_Summary_Details();
        }

        else if (RptName == "Sample Stock Details Report")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            CostCenter = Request.QueryString["CostName"].ToString();
            Sample_Stock_Details_Reports();
        }
        else if (RptName == "Sample Stock and Issue Details Report")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            // CostCenter = Request.QueryString["CostCenter"].ToString();
            Sample_Stock_Issue_Details_Reports();

        }
        else if (RptName == "Purchase Request Amendment Report")
        {
            PurRqNo = Request.QueryString["PurRqNo"].ToString();
            AmendBy = Request.QueryString["AmendBy"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            AMNo = Request.QueryString["AMNo"].ToString();

            Purchase_Request_Amendment_Report();
        }
        else if (RptName == "Purchase Request Report")
        {
            DeptName = Request.QueryString["DeptName"].ToString();
            PurReqNo = Request.QueryString["PurReqNo"].ToString();
            Requestby = Request.QueryString["Requestby"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Purchase_Request_Report();
        }
        else if (RptName == "Summary Stock Details Report")
        {
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            WareHouse = Request.QueryString["WareHouse"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            // CostCenter = Request.QueryString["CostCenter"].ToString();
            Summary_Stock_Details_Reports();
        }
        else if (RptName == "Sample Material Issue Report")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            MatIssNo = Request.QueryString["MatIssNo"].ToString();
            MatIssBy = Request.QueryString["MatIssBy"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            ItemName = ItemName.Replace("Doublequit", "\"");

            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            ButtonName = Request.QueryString["ButtonName"].ToString();
            CostCenter = Request.QueryString["CostCenter"].ToString();
            Sample_Material_Issue_Report();

        }

        else if (RptName == "OtherUnit Report Details")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            MatIssNo = Request.QueryString["MatIssNo"].ToString();
            MatIssBy = Request.QueryString["MatIssBy"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            ItemName = ItemName.Replace("Doublequit", "\"");

            OtherUnit = Request.QueryString["OtherUnit"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            ButtonName = Request.QueryString["ButtonName"].ToString();
            CostCenter = Request.QueryString["CostCenter"].ToString();
            OtherUnit_Report_Details();

        }

        else if (RptName == "ItemType Report Details")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            MatIssNo = Request.QueryString["MatIssNo"].ToString();
            MatIssBy = Request.QueryString["MatIssBy"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            ItemName = ItemName.Replace("Doublequit", "\"");

            ItemType = Request.QueryString["ItemType"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            ButtonName = Request.QueryString["ButtonName"].ToString();
            CostCenter = Request.QueryString["CostCenter"].ToString();
            ItemType_Report_Details();
        }

        else if (RptName == "Service Entry")
        {
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Service_Entry();
        }
        else if (RptName == "Non Stock Scrab")
        {
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            NonStock_Scrab_Report();
        }

        else if (RptName == "Material Issue Report" || RptName == "Material Issue Item Report" || RptName == "Material Issue Department Report" || RptName == "Material Issue Costcenter Report")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            MatIssNo = Request.QueryString["MatIssNo"].ToString();
            MatIssBy = Request.QueryString["MatIssBy"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            ItemType = Request.QueryString["ItemType"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            ButtonName = Request.QueryString["ButtonName"].ToString();
            CostCenter = Request.QueryString["CostCenter"].ToString();
            Material_Issue_Report();
        }
        else if (RptName == "Material Issue Return Report")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            MatIssRetNo = Request.QueryString["MatIssRetNo"].ToString();
            MatRetBy = Request.QueryString["MatRetBy"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            ButtonName = Request.QueryString["ButtonName"].ToString();

            Material_Issue_Return_Report();
        }
        else if (RptName == "Material Request Report")
        {

            WareHouse = Request.QueryString["WareHouse"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            MatReqNo = Request.QueryString["MatReqNo"].ToString();
            MatPreBy = Request.QueryString["MatPreBy"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            ButtonName = Request.QueryString["ButtonName"].ToString();

            Material_Request_Report();

        }
        else if (RptName == "GatePass OUT Details Report")
        {
            PartyName = Request.QueryString["PartyName"].ToString();
            WareHouse = Request.QueryString["WareHouse"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            GPOutNo = Request.QueryString["GPOutNo"].ToString();
            GPOutIssBy = Request.QueryString["GPOutIssBy"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            ButtonName = Request.QueryString["ButtonName"].ToString();

            GatePass_OUT_Details_Report();

        }

        else if (RptName == "GatePass OUT Pending Details Report")
        {
            PartyName = Request.QueryString["PartyName"].ToString();
            WareHouse = Request.QueryString["WareHouse"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            GPOutNo = Request.QueryString["GPOutNo"].ToString();
            GPOutIssBy = Request.QueryString["GPOutIssBy"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            ButtonName = Request.QueryString["ButtonName"].ToString();

            GatePass_OUT_Pending_Details_Report();

        }

        else if (RptName == "GatePass NonReturnable Details Report")
        {

            WareHouse = Request.QueryString["WareHouse"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            GPOutNo = Request.QueryString["GPOutNo"].ToString();
            GPOutIssBy = Request.QueryString["GPOutIssBy"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            GatePass_NonReturnable_Details_Report();

        }

        else if (RptName == "ServiceEntry Details Report")
        {
            SupplierName = Request.QueryString["SupplierName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            DeptName = Request.QueryString["Dept"].ToString();
            ButtonName = Request.QueryString["BtnName"].ToString();

            ServiceEntry_Details_Report();
            

        }

        else if (RptName == "GatePass IN Details Report")
        {

            WareHouse = Request.QueryString["WareHouse"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            GPINNo = Request.QueryString["GPINNo"].ToString();
            GPOutNo = Request.QueryString["GPOutNo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            DeptName = Request.QueryString["Dept"].ToString();
            ButtonName = Request.QueryString["BtnName"].ToString();

            GatePass_IN_Details_Report();

        }

        else if (RptName == "GatePass IN Free Service")
        {

            WareHouse = Request.QueryString["WareHouse"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            GPINNo = Request.QueryString["GPINNo"].ToString();
            GPOutNo = Request.QueryString["GPOutNo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            DeptName = Request.QueryString["Dept"].ToString();

            GatePass_IN_Free_Service();

        }

        else if (RptName == "GP Out Returnable Pending Report")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            GPINNo = Request.QueryString["GPINNo"].ToString();
            GPOutNo = Request.QueryString["GPOutNo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            GP_Out_Returnable_Pending_Report();

        }
        else if (RptName == "Standard Purchase Order Details Report")
        {

            DeptName = Request.QueryString["DeptName"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            StdPurOrdNo = Request.QueryString["StdPurOrdNo"].ToString();
            SupQtNo = Request.QueryString["SupQtNo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Standard_Purchase_Order_Details_Report();
        }
        else if (RptName == "General Purchase Order Details Report")
        {

            DeptName = Request.QueryString["DeptName"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            GenPurOrdNo = Request.QueryString["GenPurOrdNo"].ToString();
            SupQtNo = Request.QueryString["SupQtNo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            General_Purchase_Order_Details_Report();
        }
        else if (RptName == "Opening Receipt Details Report")
        {

            WareHouse = Request.QueryString["WareHouse"].ToString();
            OpnRecNo = Request.QueryString["OpnRecNo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Opening_Receipt_Details_Report();
        }
        else if (RptName == "Blanket Purchase Order Details Report")
        {

            DeptName = Request.QueryString["DeptName"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            BlanketPONo = Request.QueryString["BlanketPONo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Blanket_Purchase_Order_Details();
        }
        else if (RptName == "Purchase Order Receipt Details Report")

        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            PORNumber = Request.QueryString["PORNumber"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            ButtonName = Request.QueryString["ButtonName"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();

            Purchase_Order_Receipt_Details_Report();
        }

        else if (RptName == "Combain Purchase Report")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            //PORNumber = Request.QueryString["PORNumber"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            //ButtonName = Request.QueryString["ButtonName"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();

            Purchase_Combain_Receipt_Details_Report();
        }

        else if (RptName == "Sample Purchase Report")
        {

            SupplierName = Request.QueryString["SupplierName"].ToString();
            UPRNumber = Request.QueryString["UPRNumber"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            ItemName = ItemName.Replace("Doublequit", "\"");
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            ButtonName = Request.QueryString["ButtonName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            WareHouse = Request.QueryString["WareHouse"].ToString();
            Sample_Purchase_Report();
        }
        else if (RptName == "General Receipt Details Report" || RptName == "General Receipt Purchase Item Report" || RptName == "General Receipt Department Purchase" || RptName == "General Receipt Tax Purchase" || RptName == "General Receipt GST Purchase")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            UPRNumber = Request.QueryString["UPRNumber"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            ButtonName = Request.QueryString["ButtonName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();

            General_Receipt_Details_Report();
        }
        else if (RptName == "Standard Purchase Order Details Invoice Format")
        {

            DeptName = Request.QueryString["DeptName"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            StdPurOrdNo = Request.QueryString["StdPurOrdNo"].ToString();
            SupQtNo = Request.QueryString["SupQtNo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Standard_Purchase_Order_Invoice_Format();
        }
        else if (RptName == "Blanket Purchase Order Details Invoice Format")
        {

            DeptName = Request.QueryString["DeptName"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            BlanketPONo = Request.QueryString["BlanketPONo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Blanket_Purchase_Order_Invoice_Format();
        }

        else if (RptName == "General Purchase Order Pre-Print Format")
        {
            DeptName = Request.QueryString["DeptName"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            GenPurOrdNo = Request.QueryString["GenPurOrdNo"].ToString();
            SupQtNo = Request.QueryString["SupQtNo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            PO_Rate_Type = Request.QueryString["RateType"].ToString();

            General_Purchase_Order_Pre_Print_Invoice_Format();
        }
        else if (RptName == "General Purchase Order Invoice Format")
        {

            DeptName = Request.QueryString["DeptName"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            GenPurOrdNo = Request.QueryString["GenPurOrdNo"].ToString();
            SupQtNo = Request.QueryString["SupQtNo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            PO_Rate_Type = Request.QueryString["RateType"].ToString();

            General_Purchase_Order_Invoice_Format();
        }
        else if (RptName == "Stock Transfer Details Report")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            StckTranNo = Request.QueryString["StckTranNo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Stock_Transfer_Details_Report();
        }

        else if (RptName == "Stock Details Report")
        {
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            WareHouse = Request.QueryString["WareHouse"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();

            Stock_Details_Report();
        }
        else if (RptName == "Critical Stock Details Report")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();

            Critical_Stock_Details_Report();
        }
        else if (RptName == "Non Movable Item Details Report")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();

            NonMovable_Item_Details_Report();
        }
        else if (RptName == "Single Item History Full Details Report")
        {

            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Single_Item_History_Full_Details_Report();

        }

        else if (RptName == "Purchase Return Report")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            PurRetNo = Request.QueryString["PurRetNo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Purchase_Return_Details_Report();
        }
        else if (RptName == "Purchase Return Invoice Format Report")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            PurRetNo = Request.QueryString["PurRetNo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Purchase_Return_Details_Invoice_Format();
        }
        else if (RptName == "Monthly Department wise Purchase and Issue Report")
        {

            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Monthly_DeptWise_Purchase_Issue();
        }
        else if (RptName == "Blanket Purchase Order List")
        {
            Request_Str = Request.QueryString["Request_Str"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Blanket_Purchase_Order_List();

        }
        else if (RptName == "General Purchase Order List")
        {
            Request_Str = Request.QueryString["Request_Str"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            General_Purchase_Order_List();

        }
        else if (RptName == "Standard Purchase Order List")
        {
            Request_Str = Request.QueryString["Request_Str"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Standard_Purchase_Order_List();

        }
        else if (RptName == "Standard Purchase Order Pending Details Report")
        {

            SupplierName = Request.QueryString["SupplierName"].ToString();

            Standard_PO_Pending_Report();

        }
        else if (RptName == "Blanket Purchase Order Pending Details Report")
        {

            SupplierName = Request.QueryString["SupplierName"].ToString();

            Blanket_PO_Pending_Report();

        }
        else if (RptName == "Blanket Purchase Order Schedule Report")
        {

            ItemName = Request.QueryString["ItemName"].ToString();

            Blanket_PO_Schedule_Report();

        }

        else if (RptName == "Purchase Request List")
        {
            Request_Str = Request.QueryString["Request_Str"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Purchase_Request_List();

        }
        else if (RptName == "Stock Adjustment Report")
        {

            AdjustmentBy = Request.QueryString["AdjustmentBy"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            AdjustmentNo = Request.QueryString["AdjustmentNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Stock_Adjustment_Report();
        }
        else if (RptName == "Top Moving Issued Item Details Report")
        {

            DeptName = Request.QueryString["DeptName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Top_Moving_Issued_Item_Report();
        }
        else if (RptName == "Maximum Purchase Item Details Report")
        {
            SupplierName = Request.QueryString["SupplierName"].ToString();
            DeptName = Request.QueryString["DeptName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Maximum_Purchase_Item_Details_Report();
        }
        else if (RptName == "Department Wise Purchase and Issue Cost Compare Report")
        {

            DeptName = Request.QueryString["DeptName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            DepartmentWise_Purchase_Issue_Cost_Details_Report();
        }
        else if (RptName == "Department Wise Current Fin Year And Prev Fin Year Purchase And Issue Cost Report")
        {

            DeptName = Request.QueryString["DeptName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            DepartmentWise_Current_Prev_FinYear_Purchase_Issue_Cost_Details_Report();
        }
        else if (RptName == "Electrical Department Item Purchase and Issue Cost Report")
        {

            DeptName = Request.QueryString["DeptName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Electrical_Department_Item_Purchase_Issue_Cost_Report();
        }
        else if (RptName == "CostCenter Wise Purchase and Issue Cost Report")
        {

            DeptName = Request.QueryString["DeptName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();


            CostCenterWise_Purchase_Issue_Cost_Report();
        }
        else if (RptName == "Lubricant Purchase and Issue Cost Details Report")
        {

            DeptName = Request.QueryString["DeptName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();


            Lubricant_Purchase_Issue_Cost_Report();
        }
        else if (RptName == "Damage Material Stock Report")
        {
            DeptName = Request.QueryString["DeptName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();
            WareHouse = Request.QueryString["WareHouseName"].ToString();
            DamageEntryNo = Request.QueryString["DamageEntryNo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Damage_Material_Stock_Report();

        }

        else if (RptName == "Spares Re-Sales Report")
        {
            DeptName = Request.QueryString["DeptName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();
            WareHouse = Request.QueryString["WareHouseName"].ToString();
            SparesReSalesNo = Request.QueryString["SparesReSalesNo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Spares_ReSales_Report();

        }

        else if (RptName == "Unit To Unit Stock Transfer Details Report")
        {
            WareHouse = Request.QueryString["WareHouse"].ToString();
            StckTranNo = Request.QueryString["StckTranNo"].ToString();
            ItemName = Request.QueryString["ItemName"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Unit_Stock_Transfer_Details_Report();
        }
        else if (RptName == "Genset Expense Details Report")
        {
            DeptName = Request.QueryString["DeptName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();
            WareHouse = Request.QueryString["WareHouseName"].ToString();
            GensetNo = Request.QueryString["GensetNo"].ToString();

            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Genset_Expense_Details_Report();

        }
        else if (RptName == "Scrab Item And Cost Report" || RptName == "Scrab Entry No Wise Report" || RptName == "Scrab Item Wise Report")
        {
            DeptName = Request.QueryString["DeptName"].ToString();
            CostCenterName = Request.QueryString["CostCenterName"].ToString();
            WareHouse = Request.QueryString["WareHouseName"].ToString();
            ScrabEntryNo = Request.QueryString["ScrabEntryNo"].ToString();

            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Scrab_Item_Cost_Report();

        }

        else if (RptName == "Supplier Payment Details Report")
        {
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            Supplier_Code = Request.QueryString["SuppCode"].ToString();
            Transaction_No = Request.QueryString["PayTransNo"].ToString();
            ButtonName = Request.QueryString["ButtonName"].ToString();

            Supplier_Payment_Details_Report_Load();


        }
        else if (RptName == "Supplier Advance Details Report")
        {
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            SupplierName = Request.QueryString["SupplierName"].ToString();
            Supplier_Code = Request.QueryString["SuppCode"].ToString();
            Transaction_No = Request.QueryString["AdvTransNo"].ToString();
            ButtonName = Request.QueryString["ButtonName"].ToString();
            Supplier_Advance_Details_Report_Load();


        }
        else if (RptName == "Scrab Reuse")
        {
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            ScrabReuse();
        }
        else if (RptName == "Closing Stock Statement" || RptName == "Closing Stock Statement without Details")
        {

            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            ClosingStock();

        }

        else if (RptName == "ALL Supplier Monthly Payment Details Report")
        {
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            ALL_Supplier_Monthly_Payment_Details_Report_Load();


        }

        else if (RptName == "Single Supplier Ledger Report Details Report")
        {
            SupplierName = Request.QueryString["SupplierName"].ToString();
            Supplier_Code = Request.QueryString["SuppCode"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            Single_Supplier_Payment_Ledger_Details_Report_Load();


        }
        else if (RptName == "Gate Pass In Details")
        {
            GPINNo = Request.QueryString["InvoiceNo"].ToString();
            Gate_Pass_In_New();
        }

        else if (RptName == "Service Entry Details")
        {
            SENo = Request.QueryString["InvoiceNo"].ToString();
            Service_Entry_Details();
        }


    }


    //Blanket_PO_Schedule_Report
    public void Blanket_PO_Schedule_Report()
    {
        DataTable BPOSchDT = new DataTable();

        DateTime currDat = DateTime.Now;
        DateTime lastDat = currDat.AddDays(10);


        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("CompanyCode");
        AutoDataTable.Columns.Add("LocationCode");
        AutoDataTable.Columns.Add("Address1");
        AutoDataTable.Columns.Add("Address2");
        AutoDataTable.Columns.Add("BlanketPONo");
        AutoDataTable.Columns.Add("BlanketPODate");
        AutoDataTable.Columns.Add("ItemCode");
        AutoDataTable.Columns.Add("ItemName");
        AutoDataTable.Columns.Add("Qty");
        AutoDataTable.Columns.Add("DeliveryDate");


        SSQL = "select BPM.Blanket_PO_No,BPM.Blanket_PO_Date,";
        SSQL = SSQL + "BPM.ItemCode,BPM.ItemName,BPS.Qty,BPS.DeliveryDate";
        SSQL = SSQL + " from Blanket_Purchase_Order_Main_Sub BPM inner join Blanket_Purchase_Order_Schedule BPS";
        SSQL = SSQL + " on BPM.Blanket_PO_No=BPS.Blanket_PO_No where BPM.Ccode='" + SessionCcode + "' And BPM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And BPM.FinYearCode='" + SessionFinYearCode + "' And BPM.Schedule='True' And BPM.ItemName='" + ItemName + "'";
        SSQL = SSQL + " And CONVERT(DATETIME,BPS.DeliveryDate, 103)>=CONVERT(DATETIME,'" + currDat.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,BPS.DeliveryDate, 103)<=CONVERT(DATETIME,'" + lastDat.ToString("dd/MM/yyyy") + "',103)";

        BPOSchDT = objdata.RptEmployeeMultipleDetails(SSQL);


        if (BPOSchDT.Rows.Count > 0)
        {

            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

            for (int i = 0; i < BPOSchDT.Rows.Count; i++)
            {

                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();

                AutoDataTable.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                AutoDataTable.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                AutoDataTable.Rows[i]["CompanyCode"] = SessionCcode;
                AutoDataTable.Rows[i]["LocationCode"] = SessionLcode;
                AutoDataTable.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                AutoDataTable.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                AutoDataTable.Rows[i]["BlanketPONo"] = BPOSchDT.Rows[i]["Blanket_PO_No"];
                AutoDataTable.Rows[i]["BlanketPODate"] = BPOSchDT.Rows[i]["Blanket_PO_Date"];
                AutoDataTable.Rows[i]["ItemCode"] = BPOSchDT.Rows[i]["ItemCode"];
                AutoDataTable.Rows[i]["ItemName"] = BPOSchDT.Rows[i]["ItemName"];
                AutoDataTable.Rows[i]["Qty"] = BPOSchDT.Rows[i]["Qty"];
                AutoDataTable.Rows[i]["DeliveryDate"] = BPOSchDT.Rows[i]["DeliveryDate"];

            }

            //ds.Tables.Add(AutoDataTable);
            //ReportDocument report = new ReportDocument();
            //report.Load(Server.MapPath("~/crystal/BlanketPOSchedule.rpt"));
            //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            //CrystalReportViewer1.ReportSource = report;


            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDataTable);
            RD_Report.Load(Server.MapPath("~/crystal/BlanketPOSchedule.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            //Company Details Add

            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();



        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }



    }
    //Blanket_PO_Schedule_Report


    //Standard_PO_Pending_Report()
    public void Standard_PO_Pending_Report()
    {
        DataTable PORecDT = new DataTable();
        DataTable PORecSumDT = new DataTable();
        string PendingQty = "";

        AutoDataTable.Columns.Add("Std_PO_No");
        AutoDataTable.Columns.Add("Std_PO_Date");
        AutoDataTable.Columns.Add("Supp_Name");
        AutoDataTable.Columns.Add("DeliveryDate");
        AutoDataTable.Columns.Add("DeliveryAt");
        AutoDataTable.Columns.Add("ItemCode");
        AutoDataTable.Columns.Add("ItemName");
        AutoDataTable.Columns.Add("OrderQty");
        AutoDataTable.Columns.Add("ReceivedQty");
        AutoDataTable.Columns.Add("PendingQty");


        if (SupplierName != "")
        {
            SSQL = "select SPM.Std_PO_No,SPM.Std_PO_Date,SPM.Supp_Name,";
            SSQL = SSQL + "SPM.DeliveryDate,SPM.DeliveryAt,SPS.ItemCode";
            SSQL = SSQL + " from Std_Purchase_Order_Main SPM inner join Std_Purchase_Order_Main_Sub SPS";
            SSQL = SSQL + " on SPM.Std_PO_No=SPS.Std_PO_No where SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And SPM.FinYearCode='" + SessionFinYearCode + "' And SPM.PO_Status='1' And SPM.Supp_Name='" + SupplierName + "'";

            StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (StdPurDT.Rows.Count != 0)
            {
                for (int i = 0; i < StdPurDT.Rows.Count; i++)
                {
                    SSQL = "select ItemCode,ItemName,OrderQty from Pur_Order_Receipt_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Order_No='" + StdPurDT.Rows[i]["Std_PO_No"].ToString() + "' And ItemCode='" + StdPurDT.Rows[i]["ItemCode"] + "'";
                    PORecDT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "select Sum(ReceivedQty) as ReceivedQty from Pur_Order_Receipt_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Order_No='" + StdPurDT.Rows[i]["Std_PO_No"].ToString() + "' And ItemCode='" + StdPurDT.Rows[i]["ItemCode"] + "'";

                    PORecSumDT = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (PORecDT.Rows.Count != 0)
                    {
                        //for (int j = 0; j < PORecDT.Rows.Count; j++)
                        //{
                        string OrderQty = PORecDT.Rows[0]["OrderQty"].ToString();
                        string RecvQty = PORecSumDT.Rows[0]["ReceivedQty"].ToString();
                        if (Convert.ToDecimal(OrderQty) > Convert.ToDecimal(RecvQty))
                        {
                            PendingQty = (Convert.ToDecimal(OrderQty) - Convert.ToDecimal(RecvQty)).ToString();

                        }
                        else if (Convert.ToDecimal(OrderQty) == Convert.ToDecimal(RecvQty))
                        {
                            PendingQty = "0";
                        }

                        AutoDataTable.NewRow();
                        AutoDataTable.Rows.Add();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Std_PO_No"] = StdPurDT.Rows[i]["Std_PO_No"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Std_PO_Date"] = StdPurDT.Rows[i]["Std_PO_Date"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Supp_Name"] = StdPurDT.Rows[i]["Supp_Name"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["DeliveryDate"] = StdPurDT.Rows[i]["DeliveryDate"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["DeliveryAt"] = StdPurDT.Rows[i]["DeliveryAt"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["ItemCode"] = PORecDT.Rows[0]["ItemCode"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["ItemName"] = PORecDT.Rows[0]["ItemName"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["OrderQty"] = PORecDT.Rows[0]["OrderQty"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["ReceivedQty"] = PORecSumDT.Rows[0]["ReceivedQty"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["PendingQty"] = PendingQty;


                        //}
                    }


                }
            }
        }
        else
        {
            SSQL = "select SPM.Std_PO_No,SPM.Std_PO_Date,SPM.Supp_Name,";
            SSQL = SSQL + "SPM.DeliveryDate,SPM.DeliveryAt,SPS.ItemCode";
            SSQL = SSQL + " from Std_Purchase_Order_Main SPM inner join Std_Purchase_Order_Main_Sub SPS";
            SSQL = SSQL + " on SPM.Std_PO_No=SPS.Std_PO_No where SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And SPM.FinYearCode='" + SessionFinYearCode + "' And SPM.PO_Status='1'";

            StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (StdPurDT.Rows.Count != 0)
            {
                for (int i = 0; i < StdPurDT.Rows.Count; i++)
                {
                    SSQL = "select ItemCode,ItemName,OrderQty from Pur_Order_Receipt_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Order_No='" + StdPurDT.Rows[i]["Std_PO_No"].ToString() + "' And ItemCode='" + StdPurDT.Rows[i]["ItemCode"] + "'";
                    PORecDT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "select Sum(ReceivedQty) as ReceivedQty from Pur_Order_Receipt_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Order_No='" + StdPurDT.Rows[i]["Std_PO_No"].ToString() + "' And ItemCode='" + StdPurDT.Rows[i]["ItemCode"] + "'";

                    PORecSumDT = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (PORecDT.Rows.Count != 0)
                    {
                        //for (int j = 0; j < PORecDT.Rows.Count; j++)
                        //{
                        string OrderQty = PORecDT.Rows[0]["OrderQty"].ToString();
                        string RecvQty = PORecSumDT.Rows[0]["ReceivedQty"].ToString();
                        if (Convert.ToDecimal(OrderQty) > Convert.ToDecimal(RecvQty))
                        {
                            PendingQty = (Convert.ToDecimal(OrderQty) - Convert.ToDecimal(RecvQty)).ToString();

                        }
                        else if (Convert.ToDecimal(OrderQty) == Convert.ToDecimal(RecvQty))
                        {
                            PendingQty = "0";
                        }

                        AutoDataTable.NewRow();
                        AutoDataTable.Rows.Add();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Std_PO_No"] = StdPurDT.Rows[i]["Std_PO_No"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Std_PO_Date"] = StdPurDT.Rows[i]["Std_PO_Date"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Supp_Name"] = StdPurDT.Rows[i]["Supp_Name"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["DeliveryDate"] = StdPurDT.Rows[i]["DeliveryDate"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["DeliveryAt"] = StdPurDT.Rows[i]["DeliveryAt"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["ItemCode"] = PORecDT.Rows[0]["ItemCode"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["ItemName"] = PORecDT.Rows[0]["ItemName"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["OrderQty"] = PORecDT.Rows[0]["OrderQty"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["ReceivedQty"] = PORecSumDT.Rows[0]["ReceivedQty"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["PendingQty"] = PendingQty;


                        //}
                    }
                }
            }
        }


        if (AutoDataTable.Rows.Count > 0)
        {


            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


            AutoDataTable_Pend.Columns.Add("CompanyName");
            AutoDataTable_Pend.Columns.Add("LocationName");
            AutoDataTable_Pend.Columns.Add("CompanyCode");
            AutoDataTable_Pend.Columns.Add("LocationCode");
            AutoDataTable_Pend.Columns.Add("Address1");
            AutoDataTable_Pend.Columns.Add("Address2");
            AutoDataTable_Pend.Columns.Add("StdPONo");
            AutoDataTable_Pend.Columns.Add("StdPODate");
            AutoDataTable_Pend.Columns.Add("SuppName");
            AutoDataTable_Pend.Columns.Add("DeliveryDate");
            AutoDataTable_Pend.Columns.Add("DeliveryAt");
            AutoDataTable_Pend.Columns.Add("ItemCode");
            AutoDataTable_Pend.Columns.Add("ItemName");
            AutoDataTable_Pend.Columns.Add("OrderQty");
            AutoDataTable_Pend.Columns.Add("ReceivedQty");
            AutoDataTable_Pend.Columns.Add("PendingQty");



            for (int i = 0; i < AutoDataTable.Rows.Count; i++)
            {
                if (AutoDataTable.Rows[i]["PendingQty"].ToString() != "0")
                {
                    AutoDataTable_Pend.NewRow();
                    AutoDataTable_Pend.Rows.Add();

                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["CompanyCode"] = SessionCcode;
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["LocationCode"] = SessionLcode;
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["StdPONo"] = AutoDataTable.Rows[i]["Std_PO_No"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["StdPODate"] = AutoDataTable.Rows[i]["Std_PO_Date"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["SuppName"] = AutoDataTable.Rows[i]["Supp_Name"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["DeliveryDate"] = AutoDataTable.Rows[i]["DeliveryDate"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["DeliveryAt"] = AutoDataTable.Rows[i]["DeliveryAt"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["ItemCode"] = AutoDataTable.Rows[i]["ItemCode"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["ItemName"] = AutoDataTable.Rows[i]["ItemName"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["OrderQty"] = AutoDataTable.Rows[i]["OrderQty"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["ReceivedQty"] = AutoDataTable.Rows[i]["ReceivedQty"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["PendingQty"] = AutoDataTable.Rows[i]["PendingQty"];
                }
            }

            //ds.Tables.Add(AutoDataTable_Pend);
            //ReportDocument report = new ReportDocument();
            //report.Load(Server.MapPath("~/crystal/StandardPOPending.rpt"));
            //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            //CrystalReportViewer1.ReportSource = report;


            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDataTable_Pend);
            RD_Report.Load(Server.MapPath("~/crystal/StandardPOPending.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            //Company Details Add
            //DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();



        }

        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (AutoDataTable_Pend.Rows.Count == 0)
        {
            Errflag = false;
        }

        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }


    }
    //Standard_PO_Pending_Report()


    //Blanket_PO_Pending_Report()
    public void Blanket_PO_Pending_Report()
    {
        DataTable PORecDT = new DataTable();
        DataTable PORecSumDT = new DataTable();
        string PendingQty = "";

        AutoDataTable.Columns.Add("Blanket_PO_No");
        AutoDataTable.Columns.Add("Blanket_PO_Date");
        AutoDataTable.Columns.Add("Supp_Name");
        AutoDataTable.Columns.Add("DeliveryDate");
        AutoDataTable.Columns.Add("DeliveryAt");
        AutoDataTable.Columns.Add("ItemCode");
        AutoDataTable.Columns.Add("ItemName");
        AutoDataTable.Columns.Add("OrderQty");
        AutoDataTable.Columns.Add("ReceivedQty");
        AutoDataTable.Columns.Add("PendingQty");


        if (SupplierName != "")
        {
            SSQL = "select BPM.Blanket_PO_No,BPM.Blanket_PO_Date,BPM.Supp_Name,";
            SSQL = SSQL + "BPM.DeliveryAt,BPS.ItemCode";
            SSQL = SSQL + " from Blanket_Purchase_Order_Main BPM inner join Blanket_Purchase_Order_Main_Sub BPS";
            SSQL = SSQL + " on BPM.Blanket_PO_No=BPS.Blanket_PO_No where BPM.Ccode='" + SessionCcode + "' And BPM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And BPM.FinYearCode='" + SessionFinYearCode + "' And BPM.Blanket_PO_Status='1' And BPM.Supp_Name='" + SupplierName + "'";

            StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (StdPurDT.Rows.Count != 0)
            {
                for (int i = 0; i < StdPurDT.Rows.Count; i++)
                {
                    SSQL = "select ItemCode,ItemName,OrderQty from Pur_Order_Receipt_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Order_No='" + StdPurDT.Rows[i]["Blanket_PO_No"].ToString() + "' And ItemCode='" + StdPurDT.Rows[i]["ItemCode"] + "'";
                    PORecDT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "select Sum(ReceivedQty) as ReceivedQty from Pur_Order_Receipt_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Order_No='" + StdPurDT.Rows[i]["Blanket_PO_No"].ToString() + "' And ItemCode='" + StdPurDT.Rows[i]["ItemCode"] + "'";

                    PORecSumDT = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (PORecDT.Rows.Count != 0)
                    {
                        //for (int j = 0; j < PORecDT.Rows.Count; j++)
                        //{
                        string OrderQty = PORecDT.Rows[0]["OrderQty"].ToString();
                        string RecvQty = PORecSumDT.Rows[0]["ReceivedQty"].ToString();
                        if (Convert.ToDecimal(OrderQty) > Convert.ToDecimal(RecvQty))
                        {
                            PendingQty = (Convert.ToDecimal(OrderQty) - Convert.ToDecimal(RecvQty)).ToString();

                        }
                        else if (Convert.ToDecimal(OrderQty) == Convert.ToDecimal(RecvQty))
                        {
                            PendingQty = "0";
                        }

                        AutoDataTable.NewRow();
                        AutoDataTable.Rows.Add();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Blanket_PO_No"] = StdPurDT.Rows[i]["Blanket_PO_No"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Blanket_PO_Date"] = StdPurDT.Rows[i]["Blanket_PO_Date"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Supp_Name"] = StdPurDT.Rows[i]["Supp_Name"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["DeliveryAt"] = StdPurDT.Rows[i]["DeliveryAt"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["ItemCode"] = PORecDT.Rows[0]["ItemCode"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["ItemName"] = PORecDT.Rows[0]["ItemName"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["OrderQty"] = PORecDT.Rows[0]["OrderQty"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["ReceivedQty"] = PORecSumDT.Rows[0]["ReceivedQty"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["PendingQty"] = PendingQty;


                        //}
                    }


                }
            }
        }
        else
        {
            SSQL = "select BPM.Blanket_PO_No,BPM.Blanket_PO_Date,BPM.Supp_Name,";
            SSQL = SSQL + "BPM.DeliveryAt,BPS.ItemCode";
            SSQL = SSQL + " from Blanket_Purchase_Order_Main BPM inner join Blanket_Purchase_Order_Main_Sub BPS";
            SSQL = SSQL + " on BPM.Blanket_PO_No=BPS.Blanket_PO_No where BPM.Ccode='" + SessionCcode + "' And BPM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And BPM.FinYearCode='" + SessionFinYearCode + "' And BPM.Blanket_PO_Status='1'";

            StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (StdPurDT.Rows.Count != 0)
            {
                for (int i = 0; i < StdPurDT.Rows.Count; i++)
                {
                    SSQL = "select ItemCode,ItemName,OrderQty from Pur_Order_Receipt_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Order_No='" + StdPurDT.Rows[i]["Blanket_PO_No"].ToString() + "' And ItemCode='" + StdPurDT.Rows[i]["ItemCode"] + "'";
                    PORecDT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "select Sum(ReceivedQty) as ReceivedQty from Pur_Order_Receipt_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Pur_Order_No='" + StdPurDT.Rows[i]["Blanket_PO_No"].ToString() + "' And ItemCode='" + StdPurDT.Rows[i]["ItemCode"] + "'";

                    PORecSumDT = objdata.RptEmployeeMultipleDetails(SSQL);


                    if (PORecDT.Rows.Count != 0)
                    {
                        //for (int j = 0; j < PORecDT.Rows.Count; j++)
                        //{
                        string OrderQty = PORecDT.Rows[0]["OrderQty"].ToString();
                        string RecvQty = PORecSumDT.Rows[0]["ReceivedQty"].ToString();
                        if (Convert.ToDecimal(OrderQty) > Convert.ToDecimal(RecvQty))
                        {
                            PendingQty = (Convert.ToDecimal(OrderQty) - Convert.ToDecimal(RecvQty)).ToString();

                        }
                        else if (Convert.ToDecimal(OrderQty) == Convert.ToDecimal(RecvQty))
                        {
                            PendingQty = "0";
                        }

                        AutoDataTable.NewRow();
                        AutoDataTable.Rows.Add();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Blanket_PO_No"] = StdPurDT.Rows[i]["Blanket_PO_No"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Blanket_PO_Date"] = StdPurDT.Rows[i]["Blanket_PO_Date"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["Supp_Name"] = StdPurDT.Rows[i]["Supp_Name"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["DeliveryAt"] = StdPurDT.Rows[i]["DeliveryAt"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["ItemCode"] = PORecDT.Rows[0]["ItemCode"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["ItemName"] = PORecDT.Rows[0]["ItemName"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["OrderQty"] = PORecDT.Rows[0]["OrderQty"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["ReceivedQty"] = PORecSumDT.Rows[0]["ReceivedQty"].ToString();
                        AutoDataTable.Rows[AutoDataTable.Rows.Count - 1]["PendingQty"] = PendingQty;


                        //}
                    }
                }
            }
        }


        if (AutoDataTable.Rows.Count > 0)
        {


            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


            AutoDataTable_Pend.Columns.Add("CompanyName");
            AutoDataTable_Pend.Columns.Add("LocationName");
            AutoDataTable_Pend.Columns.Add("CompanyCode");
            AutoDataTable_Pend.Columns.Add("LocationCode");
            AutoDataTable_Pend.Columns.Add("Address1");
            AutoDataTable_Pend.Columns.Add("Address2");
            AutoDataTable_Pend.Columns.Add("BlanketPONo");
            AutoDataTable_Pend.Columns.Add("BlanketPODate");
            AutoDataTable_Pend.Columns.Add("SuppName");
            AutoDataTable_Pend.Columns.Add("DeliveryDate");
            AutoDataTable_Pend.Columns.Add("DeliveryAt");
            AutoDataTable_Pend.Columns.Add("ItemCode");
            AutoDataTable_Pend.Columns.Add("ItemName");
            AutoDataTable_Pend.Columns.Add("OrderQty");
            AutoDataTable_Pend.Columns.Add("ReceivedQty");
            AutoDataTable_Pend.Columns.Add("PendingQty");



            for (int i = 0; i < AutoDataTable.Rows.Count; i++)
            {
                if (AutoDataTable.Rows[i]["PendingQty"].ToString() != "0")
                {
                    AutoDataTable_Pend.NewRow();
                    AutoDataTable_Pend.Rows.Add();

                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["CompanyCode"] = SessionCcode;
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["LocationCode"] = SessionLcode;
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["BlanketPONo"] = AutoDataTable.Rows[i]["Blanket_PO_No"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["BlanketPODate"] = AutoDataTable.Rows[i]["Blanket_PO_Date"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["SuppName"] = AutoDataTable.Rows[i]["Supp_Name"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["DeliveryAt"] = AutoDataTable.Rows[i]["DeliveryAt"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["ItemCode"] = AutoDataTable.Rows[i]["ItemCode"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["ItemName"] = AutoDataTable.Rows[i]["ItemName"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["OrderQty"] = AutoDataTable.Rows[i]["OrderQty"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["ReceivedQty"] = AutoDataTable.Rows[i]["ReceivedQty"];
                    AutoDataTable_Pend.Rows[AutoDataTable_Pend.Rows.Count - 1]["PendingQty"] = AutoDataTable.Rows[i]["PendingQty"];
                }
            }

            //ds.Tables.Add(AutoDataTable_Pend);
            //ReportDocument report = new ReportDocument();
            //report.Load(Server.MapPath("~/crystal/BlanketPOPending.rpt"));
            //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            //CrystalReportViewer1.ReportSource = report;


            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDataTable_Pend);
            RD_Report.Load(Server.MapPath("~/crystal/BlanketPOPending.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            //Company Details Add

            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();



        }

        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (AutoDataTable_Pend.Rows.Count == 0)
        {
            Errflag = false;
        }

        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }
    //Blanket_PO_Pending_Report()


    //Blanket_Purchase_Order_List()
    public void Blanket_Purchase_Order_List()
    {
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }
        if (Request_Str == "Rejected List")
        {
            SSQL = "Select Blanket_PO_No,Blanket_PO_Date,Approvedby,Supp_Name,TotalOrderQty,TotalAmt from Blanket_Purchase_Order_Main where (Blanket_PO_Status = '2' or Blanket_PO_Status = '6')";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Blanket_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Blanket_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            BlnkPODT = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else if (Request_Str == "Pending List")
        {
            SSQL = "Select Blanket_PO_No,Blanket_PO_Date,Approvedby,Supp_Name,TotalOrderQty,TotalAmt from Blanket_Purchase_Order_Main where (Blanket_PO_Status = '3' or Blanket_PO_Status = '5' or Blanket_PO_Status = '0' or Blanket_PO_Status is null)";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Blanket_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Blanket_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            BlnkPODT = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Give Details Correctly..');", true);
        }
        if (BlnkPODT.Rows.Count > 0)
        {


            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


            AutoDataTable.Columns.Add("CompanyName");
            AutoDataTable.Columns.Add("LocationName");
            AutoDataTable.Columns.Add("CompanyCode");
            AutoDataTable.Columns.Add("LocationCode");
            AutoDataTable.Columns.Add("Address1");
            AutoDataTable.Columns.Add("Address2");
            AutoDataTable.Columns.Add("BlanketPONo");
            AutoDataTable.Columns.Add("BlanketPODate");
            AutoDataTable.Columns.Add("Approvedby");
            AutoDataTable.Columns.Add("SupplierName");
            AutoDataTable.Columns.Add("TotalOrderQty");
            AutoDataTable.Columns.Add("TotalAmt");
            AutoDataTable.Columns.Add("Status");



            for (int i = 0; i < BlnkPODT.Rows.Count; i++)
            {
                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();

                AutoDataTable.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                AutoDataTable.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                AutoDataTable.Rows[i]["CompanyCode"] = SessionCcode;
                AutoDataTable.Rows[i]["LocationCode"] = SessionLcode;
                AutoDataTable.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                AutoDataTable.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                AutoDataTable.Rows[i]["BlanketPONo"] = BlnkPODT.Rows[i]["Blanket_PO_No"];
                AutoDataTable.Rows[i]["BlanketPODate"] = BlnkPODT.Rows[i]["Blanket_PO_Date"];
                AutoDataTable.Rows[i]["Approvedby"] = BlnkPODT.Rows[i]["Approvedby"];
                AutoDataTable.Rows[i]["SupplierName"] = BlnkPODT.Rows[i]["Supp_Name"];
                AutoDataTable.Rows[i]["TotalOrderQty"] = BlnkPODT.Rows[i]["TotalOrderQty"];
                AutoDataTable.Rows[i]["TotalAmt"] = BlnkPODT.Rows[i]["TotalAmt"];
                AutoDataTable.Rows[i]["Status"] = Request_Str;

            }

            //ds.Tables.Add(AutoDataTable);
            //ReportDocument report = new ReportDocument();
            //report.Load(Server.MapPath("~/crystal/BlankerPOStatus.rpt"));
            //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            //CrystalReportViewer1.ReportSource = report;



            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDataTable);
            RD_Report.Load(Server.MapPath("~/crystal/BlankerPOStatus.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            //Company Details Add

            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();



        }

        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }




    }
    //Blanket_Purchase_Order_List()


    //General_Purchase_Order_List()
    public void General_Purchase_Order_List()
    {
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }
        if (Request_Str == "Rejected List")
        {
            SSQL = "Select Gen_PO_No,Gen_PO_Date,Approvedby,Supp_Name,TotalOrderQty,NetAmount from General_Purchase_Order_Main where (PO_Status = '2')";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Gen_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Gen_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            BlnkPODT = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else if (Request_Str == "Pending List")
        {
            SSQL = "Select Gen_PO_No,Gen_PO_Date,Approvedby,Supp_Name,TotalOrderQty,NetAmount from General_Purchase_Order_Main where (PO_Status = '3' or PO_Status = '0' or PO_Status is null)";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Gen_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Gen_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            BlnkPODT = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Give Details Correctly..');", true);
        }
        if (BlnkPODT.Rows.Count > 0)
        {


            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


            AutoDataTable.Columns.Add("CompanyName");
            AutoDataTable.Columns.Add("LocationName");
            AutoDataTable.Columns.Add("CompanyCode");
            AutoDataTable.Columns.Add("LocationCode");
            AutoDataTable.Columns.Add("Address1");
            AutoDataTable.Columns.Add("Address2");
            AutoDataTable.Columns.Add("GenPONo");
            AutoDataTable.Columns.Add("GenPODate");
            AutoDataTable.Columns.Add("Approvedby");
            AutoDataTable.Columns.Add("SupplierName");
            AutoDataTable.Columns.Add("TotalOrderQty");
            AutoDataTable.Columns.Add("TotalAmt");
            AutoDataTable.Columns.Add("Status");



            for (int i = 0; i < BlnkPODT.Rows.Count; i++)
            {
                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();

                AutoDataTable.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                AutoDataTable.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                AutoDataTable.Rows[i]["CompanyCode"] = SessionCcode;
                AutoDataTable.Rows[i]["LocationCode"] = SessionLcode;
                AutoDataTable.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                AutoDataTable.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                AutoDataTable.Rows[i]["GenPONo"] = BlnkPODT.Rows[i]["Gen_PO_No"];
                AutoDataTable.Rows[i]["GenPODate"] = BlnkPODT.Rows[i]["Gen_PO_Date"];
                AutoDataTable.Rows[i]["Approvedby"] = BlnkPODT.Rows[i]["Approvedby"];
                AutoDataTable.Rows[i]["SupplierName"] = BlnkPODT.Rows[i]["Supp_Name"];
                AutoDataTable.Rows[i]["TotalOrderQty"] = BlnkPODT.Rows[i]["TotalOrderQty"];
                AutoDataTable.Rows[i]["TotalAmt"] = BlnkPODT.Rows[i]["NetAmount"];
                AutoDataTable.Rows[i]["Status"] = Request_Str;

            }

            //ds.Tables.Add(AutoDataTable);
            //ReportDocument report = new ReportDocument();
            //report.Load(Server.MapPath("~/crystal/GeneralPOStatus.rpt"));
            //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            //CrystalReportViewer1.ReportSource = report;


            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDataTable);
            RD_Report.Load(Server.MapPath("~/crystal/GeneralPOStatus.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            //Company Details Add

            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();


        }

        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }




    }
    //General_Purchase_Order_List()


    //Standard_Purchase_Order_List()
    public void Standard_Purchase_Order_List()
    {
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }
        if (Request_Str == "Rejected List")
        {
            SSQL = "Select Std_PO_No,Std_PO_Date,Approvedby,Supp_Name,TotalQuantity,NetAmount from Std_Purchase_Order_Main where (PO_Status = '2' or PO_Status = '6')";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Std_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Std_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            BlnkPODT = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else if (Request_Str == "Pending List")
        {
            SSQL = "Select Std_PO_No,Std_PO_Date,Approvedby,Supp_Name,TotalQuantity,NetAmount from Std_Purchase_Order_Main where (PO_Status = '3' or PO_Status = '5' or PO_Status = '0' or PO_Status is null)";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Std_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Std_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            BlnkPODT = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Give Details Correctly..');", true);
        }
        if (BlnkPODT.Rows.Count > 0)
        {


            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


            AutoDataTable.Columns.Add("CompanyName");
            AutoDataTable.Columns.Add("LocationName");
            AutoDataTable.Columns.Add("CompanyCode");
            AutoDataTable.Columns.Add("LocationCode");
            AutoDataTable.Columns.Add("Address1");
            AutoDataTable.Columns.Add("Address2");
            AutoDataTable.Columns.Add("StdPONo");
            AutoDataTable.Columns.Add("StdPODate");
            AutoDataTable.Columns.Add("Approvedby");
            AutoDataTable.Columns.Add("SupplierName");
            AutoDataTable.Columns.Add("TotalOrderQty");
            AutoDataTable.Columns.Add("TotalAmt");
            AutoDataTable.Columns.Add("Status");



            for (int i = 0; i < BlnkPODT.Rows.Count; i++)
            {
                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();

                AutoDataTable.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                AutoDataTable.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                AutoDataTable.Rows[i]["CompanyCode"] = SessionCcode;
                AutoDataTable.Rows[i]["LocationCode"] = SessionLcode;
                AutoDataTable.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                AutoDataTable.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                AutoDataTable.Rows[i]["StdPONo"] = BlnkPODT.Rows[i]["Std_PO_No"];
                AutoDataTable.Rows[i]["StdPODate"] = BlnkPODT.Rows[i]["Std_PO_Date"];
                AutoDataTable.Rows[i]["Approvedby"] = BlnkPODT.Rows[i]["Approvedby"];
                AutoDataTable.Rows[i]["SupplierName"] = BlnkPODT.Rows[i]["Supp_Name"];
                AutoDataTable.Rows[i]["TotalOrderQty"] = BlnkPODT.Rows[i]["TotalQuantity"];
                AutoDataTable.Rows[i]["TotalAmt"] = BlnkPODT.Rows[i]["NetAmount"];
                AutoDataTable.Rows[i]["Status"] = Request_Str;

            }

            //ds.Tables.Add(AutoDataTable);
            //ReportDocument report = new ReportDocument();
            //report.Load(Server.MapPath("~/crystal/StandardPOStatus.rpt"));
            //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            //CrystalReportViewer1.ReportSource = report;



            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDataTable);
            RD_Report.Load(Server.MapPath("~/crystal/StandardPOStatus.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            //Company Details Add
            //DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }

        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }




    }
    //Standard_Purchase_Order_List()



    //PurchaseEnquiryReport Code Start
    public void PurchaseEnquiryReport()
    {


        if (RptName == "PurchaseEnquiryReport")
        {

            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }
            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            if (FromDate != "" && ToDate != "" && PENo == "")
            {

                SSQL = "select PM.Pur_EnquiryNo,PM.Enquiry_Date,PM.SupName1,PM.SupName2,PM.SupName3,";
                SSQL = SSQL + "PM.Supname4,PM.SupName5,PM.Description,PM.Note,PM.Others,PS.ItemCode,PS.ItemName,PS.UOMCode,PS.Rate from Pur_Enq_Main PM inner join Pur_Enq_Main_Sub PS ";
                SSQL = SSQL + "on PM.Enquiry_Date=PS.Enquiry_Date where CONVERT(DATETIME,PM.Enquiry_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,PM.Enquiry_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (SupplierName != "")
                {
                    SSQL = SSQL + "And (PM.SupName1='" + SupplierName + "' or PM.SupName2='" + SupplierName + "' or PM.SupName3='" + SupplierName + "'";
                    SSQL = SSQL + "or PM.SupName4='" + SupplierName + "' or PM.SupName5='" + SupplierName + "')";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And PS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And PM.Ccode='" + SessionCcode + "' And PM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And PM.FinYearCode='" + SessionFinYearCode + "' And PS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And PS.Lcode='" + SessionLcode + "' And PS.FinYearCode='" + SessionFinYearCode + "'";

                PurEnqrDT = objdata.RptEmployeeMultipleDetails(SSQL);
                Errflag = true;

            }

            else if (PENo != "" && FromDate != "" && ToDate != "")
            {
                SSQL = "select PM.Pur_EnquiryNo,PM.Enquiry_Date,PM.SupName1,PM.SupName2,PM.SupName3,";
                SSQL = SSQL + "PM.Supname4,PM.SupName5,PM.Description,PM.Note,PM.Others,PS.ItemCode,PS.ItemName,PS.UOMCode,PS.Rate from Pur_Enq_Main PM inner join Pur_Enq_Main_Sub PS ";
                SSQL = SSQL + "on PM.Pur_EnquiryNo=PS.Pur_EnquiryNo where PM.Pur_EnquiryNo='" + PENo + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,PM.Enquiry_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,PM.Enquiry_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (SupplierName != "")
                {
                    SSQL = SSQL + "And (PM.SupName1='" + SupplierName + "' or PM.SupName2='" + SupplierName + "' or PM.SupName3='" + SupplierName + "'";
                    SSQL = SSQL + "or PM.SupName4='" + SupplierName + "' or PM.SupName5='" + SupplierName + "')";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And PS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And PM.Ccode='" + SessionCcode + "' And PM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And PM.FinYearCode='" + SessionFinYearCode + "' And PS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And PS.Lcode='" + SessionLcode + "' And PS.FinYearCode='" + SessionFinYearCode + "'";

                PurEnqrDT = objdata.RptEmployeeMultipleDetails(SSQL);

                Errflag = true;

            }
            else if (PENo != "")
            {
                SSQL = "select PM.Pur_EnquiryNo,PM.Enquiry_Date,PM.SupName1,PM.SupName2,PM.SupName3,";
                SSQL = SSQL + "PM.Supname4,PM.SupName5,PM.Description,PM.Note,PM.Others,PS.ItemCode,PS.ItemName,PS.UOMCode,PS.Rate from Pur_Enq_Main PM inner join Pur_Enq_Main_Sub PS ";
                SSQL = SSQL + "on PM.Pur_EnquiryNo=PS.Pur_EnquiryNo where PM.Pur_EnquiryNo='" + PENo + "'";

                if (SupplierName != "")
                {
                    SSQL = SSQL + "And (PM.SupName1='" + SupplierName + "' or PM.SupName2='" + SupplierName + "' or PM.SupName3='" + SupplierName + "'";
                    SSQL = SSQL + "or PM.SupName4='" + SupplierName + "' or PM.SupName5='" + SupplierName + "')";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And PS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And PM.Ccode='" + SessionCcode + "' And PM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And PM.FinYearCode='" + SessionFinYearCode + "' And PS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And PS.Lcode='" + SessionLcode + "' And PS.FinYearCode='" + SessionFinYearCode + "'";

                PurEnqrDT = objdata.RptEmployeeMultipleDetails(SSQL);
                Errflag = true;

            }

            else if (SupplierName != "")
            {
                SSQL = "select PM.Pur_EnquiryNo,PM.Enquiry_Date,PM.SupName1,PM.SupName2,PM.SupName3,";
                SSQL = SSQL + "PM.Supname4,PM.SupName5,PM.Description,PM.Note,PM.Others,PS.ItemCode,PS.ItemName,PS.UOMCode,PS.Rate from Pur_Enq_Main PM inner join Pur_Enq_Main_Sub PS ";
                SSQL = SSQL + "on PM.Pur_EnquiryNo=PS.Pur_EnquiryNo where (PM.SupName1='" + SupplierName + "' or PM.SupName2='" + SupplierName + "' or PM.SupName3='" + SupplierName + "'";
                SSQL = SSQL + " or PM.SupName4='" + SupplierName + "' or PM.SupName5='" + SupplierName + "')";

                if (PENo != "")
                {
                    SSQL = SSQL + " And PM.Pur_EnquiryNo='" + PENo + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And PS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And PM.Ccode='" + SessionCcode + "' And PM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And PM.FinYearCode='" + SessionFinYearCode + "' And PS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And PS.Lcode='" + SessionLcode + "' And PS.FinYearCode='" + SessionFinYearCode + "'";

                PurEnqrDT = objdata.RptEmployeeMultipleDetails(SSQL);

                Errflag = true;

            }
            else if (ItemName != "")
            {
                SSQL = "select PM.Pur_EnquiryNo,PM.Enquiry_Date,PM.SupName1,PM.SupName2,PM.SupName3,";
                SSQL = SSQL + "PM.Supname4,PM.SupName5,PM.Description,PM.Note,PM.Others,PS.ItemCode,PS.ItemName,PS.UOMCode,PS.Rate from Pur_Enq_Main PM inner join Pur_Enq_Main_Sub PS ";
                SSQL = SSQL + "on PM.Pur_EnquiryNo=PS.Pur_EnquiryNo where PS.ItemName='" + ItemName + "'";

                if (SupplierName != "")
                {
                    SSQL = SSQL + "And (PM.SupName1='" + SupplierName + "' or PM.SupName2='" + SupplierName + "' or PM.SupName3='" + SupplierName + "'";
                    SSQL = SSQL + "or PM.SupName4='" + SupplierName + "' or PM.SupName5='" + SupplierName + "')";
                }
                if (PENo != "")
                {
                    SSQL = SSQL + " And PM.Pur_EnquiryNo='" + PENo + "'";
                }


                SSQL = SSQL + " And PM.Ccode='" + SessionCcode + "' And PM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And PM.FinYearCode='" + SessionFinYearCode + "' And PS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And PS.Lcode='" + SessionLcode + "' And PS.FinYearCode='" + SessionFinYearCode + "'";

                PurEnqrDT = objdata.RptEmployeeMultipleDetails(SSQL);
                Errflag = true;

            }


            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Purchase Enquiry(PE) Details..');", true);
            }

            if (PurEnqrDT.Rows.Count > 0)
            {


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDataTable.Columns.Add("CompanyName");
                AutoDataTable.Columns.Add("LocationName");
                AutoDataTable.Columns.Add("CompanyCode");
                AutoDataTable.Columns.Add("LocationCode");
                AutoDataTable.Columns.Add("Address1");
                AutoDataTable.Columns.Add("Address2");
                AutoDataTable.Columns.Add("PurEnqryNo");
                AutoDataTable.Columns.Add("PurEnqryDate");
                AutoDataTable.Columns.Add("SupplierName1");
                AutoDataTable.Columns.Add("SupplierName2");
                AutoDataTable.Columns.Add("SupplierName3");
                AutoDataTable.Columns.Add("SupplierName4");
                AutoDataTable.Columns.Add("SupplierName5");
                AutoDataTable.Columns.Add("Description");
                AutoDataTable.Columns.Add("Note");
                AutoDataTable.Columns.Add("Others");



                AutoDataTable.Columns.Add("ItemCode");
                AutoDataTable.Columns.Add("ItemName");
                AutoDataTable.Columns.Add("UomCode");
                AutoDataTable.Columns.Add("Rate");


                for (int i = 0; i < PurEnqrDT.Rows.Count; i++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDataTable.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDataTable.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDataTable.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDataTable.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDataTable.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                    AutoDataTable.Rows[i]["PurEnqryNo"] = PurEnqrDT.Rows[i]["Pur_EnquiryNo"];
                    AutoDataTable.Rows[i]["PurEnqryDate"] = PurEnqrDT.Rows[i]["Enquiry_Date"];
                    AutoDataTable.Rows[i]["SupplierName1"] = PurEnqrDT.Rows[i]["SupName1"];
                    AutoDataTable.Rows[i]["SupplierName2"] = PurEnqrDT.Rows[i]["SupName2"];
                    AutoDataTable.Rows[i]["SupplierName3"] = PurEnqrDT.Rows[i]["SupName3"];
                    AutoDataTable.Rows[i]["SupplierName4"] = PurEnqrDT.Rows[i]["SupName4"];
                    AutoDataTable.Rows[i]["SupplierName5"] = PurEnqrDT.Rows[i]["SupName5"];
                    AutoDataTable.Rows[i]["Description"] = PurEnqrDT.Rows[i]["Description"];
                    AutoDataTable.Rows[i]["Note"] = PurEnqrDT.Rows[i]["Note"];
                    AutoDataTable.Rows[i]["Others"] = PurEnqrDT.Rows[i]["Others"];


                    AutoDataTable.Rows[i]["ItemCode"] = PurEnqrDT.Rows[i]["ItemCode"];
                    AutoDataTable.Rows[i]["ItemName"] = PurEnqrDT.Rows[i]["ItemName"];
                    AutoDataTable.Rows[i]["UomCode"] = PurEnqrDT.Rows[i]["UOMCode"];
                    AutoDataTable.Rows[i]["Rate"] = PurEnqrDT.Rows[i]["Rate"];

                }

                //ds.Tables.Add(AutoDataTable);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/PurchaseEnquiryReport.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;



                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDataTable);
                RD_Report.Load(Server.MapPath("~/crystal/PurchaseEnquiryReport.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                //DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }

    }
    //Purchase Enquiry Report Code End


    //Purchase Request Amendment Report Code Start
    public void Purchase_Request_Amendment_Report()
    {


        if (RptName == "Purchase Request Amendment Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }
            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }
            if (FromDate != "" && ToDate != "" && PurRqNo == "" && AMNo == "")
            {

                SSQL = "select AM.Pur_Request_No,AM.Pur_Request_Date,AM.Amend_No,AM.Amend_Date,AM.Amendby,";
                SSQL = SSQL + "AM.Approvedby,AM.Others,AS1.ItemCode,AS1.ItemName,AS1.UOMCode,AS1.ReuiredQty,AS1.ReuiredDate,AS1.AmendQty,AS1.AmendDate from Pur_Request_Amend_Main AM inner join Pur_Request_Amend_Main_Sub AS1 ";
                SSQL = SSQL + "on AM.Amend_Date=AS1.Amend_Date where CONVERT(DATETIME,AM.Amend_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,AM.Amend_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (ItemName != "")
                {
                    SSQL = SSQL + " And AS1.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And AM.Ccode='" + SessionCcode + "' And AM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And AM.FinYearCode='" + SessionFinYearCode + "' And AS1.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And AS1.Lcode='" + SessionLcode + "' And AS1.FinYearCode='" + SessionFinYearCode + "'";

                PurRqAmendDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FromDate != "" && ToDate != "" && PurRqNo != "")
            {

                SSQL = "select AM.Pur_Request_No,AM.Pur_Request_Date,AM.Amend_No,AM.Amend_Date,AM.Amendby,";
                SSQL = SSQL + "AM.Approvedby,AM.Others,AS1.ItemCode,AS1.ItemName,AS1.UOMCode,AS1.ReuiredQty,AS1.ReuiredDate,AS1.AmendQty from Pur_Request_Amend_Main AM inner join Pur_Request_Amend_Main_Sub AS1 ";
                SSQL = SSQL + "on PM.Pur_Request_No=AS1.Pur_Request_No where AM.Pur_Request_No='" + PurRqNo + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,AM.Amend_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,AM.Amend_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                if (AMNo != "")
                {
                    SSQL = SSQL + " And AM.Amend_No='" + AMNo + "'";
                }

                if (AmendBy != "")
                {
                    SSQL = SSQL + " And AM.Amendby='" + AmendBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And AS1.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And AM.Ccode='" + SessionCcode + "' And AM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And AM.FinYearCode='" + SessionFinYearCode + "' And AS1.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And AS1.Lcode='" + SessionLcode + "' And AS1.FinYearCode='" + SessionFinYearCode + "'";

                PurRqAmendDT = objdata.RptEmployeeMultipleDetails(SSQL);


            }
            else if (PurRqNo != "")
            {

                SSQL = "select AM.Pur_Request_No,AM.Pur_Request_Date,AM.Amend_No,AM.Amend_Date,AM.Amendby,";
                SSQL = SSQL + "AM.Approvedby,AM.Others,AS1.ItemCode,AS1.ItemName,AS1.UOMCode,AS1.ReuiredQty,AS1.ReuiredDate,AS1.AmendQty from Pur_Request_Amend_Main AM inner join Pur_Request_Amend_Main_Sub AS1 ";
                SSQL = SSQL + "on AM.Pur_Request_No=AS1.Pur_Request_No where AM.Pur_Request_No='" + PurRqNo + "'";

                if (AmendBy != "")
                {
                    SSQL = SSQL + " And AM.Amendby='" + AmendBy + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And AS1.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And AM.Ccode='" + SessionCcode + "' And AM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And AM.FinYearCode='" + SessionFinYearCode + "' And AS1.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And AS1.Lcode='" + SessionLcode + "' And AS1.FinYearCode='" + SessionFinYearCode + "'";

                PurRqAmendDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (AMNo != "")
            {
                SSQL = "select AM.Pur_Request_No,AM.Pur_Request_Date,AM.Amend_No,AM.Amend_Date,AM.Amendby,";
                SSQL = SSQL + "AM.Approvedby,AM.Others,AS1.ItemCode,AS1.ItemName,AS1.UOMCode,AS1.ReuiredQty,AS1.ReuiredDate,AS1.AmendQty from Pur_Request_Amend_Main AM inner join Pur_Request_Amend_Main_Sub AS1 ";
                SSQL = SSQL + "on AM.Amend_No=AS1.Amend_No where AM.Amend_No='" + AMNo + "'";

                if (AmendBy != "")
                {
                    SSQL = SSQL + " And AM.Amendby='" + AmendBy + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And AS1.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And AM.Ccode='" + SessionCcode + "' And AM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And AM.FinYearCode='" + SessionFinYearCode + "' And AS1.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And AS1.Lcode='" + SessionLcode + "' And AS1.FinYearCode='" + SessionFinYearCode + "'";

                PurRqAmendDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (AmendBy != "")
            {
                SSQL = "select AM.Pur_Request_No,AM.Pur_Request_Date,AM.Amend_No,AM.Amend_Date,AM.Amendby,";
                SSQL = SSQL + "AM.Approvedby,AM.Others,AS1.ItemCode,AS1.ItemName,AS1.UOMCode,AS1.ReuiredQty,AS1.ReuiredDate,AS1.AmendQty from Pur_Request_Amend_Main AM inner join Pur_Request_Amend_Main_Sub AS1 ";
                SSQL = SSQL + "on AM.Amend_No=AS1.Amend_No where AM.Amendby='" + AmendBy + "'";

                if (AMNo != "")
                {
                    SSQL = SSQL + " And AM.Amend_No='" + AMNo + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And AS1.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And AM.Ccode='" + SessionCcode + "' And AM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And AM.FinYearCode='" + SessionFinYearCode + "' And AS1.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And AS1.Lcode='" + SessionLcode + "' And AS1.FinYearCode='" + SessionFinYearCode + "'";

                PurRqAmendDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (ItemName != "")
            {
                SSQL = "select AM.Pur_Request_No,AM.Pur_Request_Date,AM.Amend_No,AM.Amend_Date,AM.Amendby,";
                SSQL = SSQL + "AM.Approvedby,AM.Others,AS1.ItemCode,AS1.ItemName,AS1.UOMCode,AS1.ReuiredQty,AS1.ReuiredDate,AS1.AmendQty from Pur_Request_Amend_Main AM inner join Pur_Request_Amend_Main_Sub AS1 ";
                SSQL = SSQL + "on AM.Amend_No=AS1.Amend_No where AS1.ItemName='" + ItemName + "'";

                if (AMNo != "")
                {
                    SSQL = SSQL + " And AM.Amend_No='" + AMNo + "'";
                }
                if (AmendBy != "")
                {
                    SSQL = SSQL + " And AM.Amendby='" + AmendBy + "'";
                }


                SSQL = SSQL + " And AM.Ccode='" + SessionCcode + "' And AM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And AM.FinYearCode='" + SessionFinYearCode + "' And AS1.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And AS1.Lcode='" + SessionLcode + "' And AS1.FinYearCode='" + SessionFinYearCode + "'";

                PurRqAmendDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (PurRqAmendDT.Rows.Count > 0)
            {


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTPurRqAmend.Columns.Add("CompanyName");
                AutoDTPurRqAmend.Columns.Add("LocationName");
                AutoDTPurRqAmend.Columns.Add("CompanyCode");
                AutoDTPurRqAmend.Columns.Add("LocationCode");
                AutoDTPurRqAmend.Columns.Add("Address1");
                AutoDTPurRqAmend.Columns.Add("Address2");
                AutoDTPurRqAmend.Columns.Add("PurRequestNo");
                AutoDTPurRqAmend.Columns.Add("PurRequestDate");
                AutoDTPurRqAmend.Columns.Add("AmendNo");
                AutoDTPurRqAmend.Columns.Add("AmendDate");
                AutoDTPurRqAmend.Columns.Add("Amendby");
                AutoDTPurRqAmend.Columns.Add("Approvedby");
                AutoDTPurRqAmend.Columns.Add("Others");


                AutoDTPurRqAmend.Columns.Add("ItemCode");
                AutoDTPurRqAmend.Columns.Add("ItemName");
                AutoDTPurRqAmend.Columns.Add("UomCode");
                AutoDTPurRqAmend.Columns.Add("RequiredQty");
                AutoDTPurRqAmend.Columns.Add("RequiredDate");
                AutoDTPurRqAmend.Columns.Add("AmendQty");



                for (int i = 0; i < PurRqAmendDT.Rows.Count; i++)
                {
                    AutoDTPurRqAmend.NewRow();
                    AutoDTPurRqAmend.Rows.Add();

                    AutoDTPurRqAmend.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTPurRqAmend.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTPurRqAmend.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTPurRqAmend.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTPurRqAmend.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTPurRqAmend.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                    AutoDTPurRqAmend.Rows[i]["PurRequestNo"] = PurRqAmendDT.Rows[i]["Pur_Request_No"];
                    AutoDTPurRqAmend.Rows[i]["PurRequestDate"] = PurRqAmendDT.Rows[i]["Pur_Request_Date"];
                    AutoDTPurRqAmend.Rows[i]["AmendNo"] = PurRqAmendDT.Rows[i]["Amend_No"];
                    AutoDTPurRqAmend.Rows[i]["AmendDate"] = PurRqAmendDT.Rows[i]["Amend_Date"];
                    AutoDTPurRqAmend.Rows[i]["Amendby"] = PurRqAmendDT.Rows[i]["Amendby"];
                    AutoDTPurRqAmend.Rows[i]["Approvedby"] = PurRqAmendDT.Rows[i]["Approvedby"];
                    AutoDTPurRqAmend.Rows[i]["Others"] = PurRqAmendDT.Rows[i]["Others"];



                    AutoDTPurRqAmend.Rows[i]["ItemCode"] = PurRqAmendDT.Rows[i]["ItemCode"];
                    AutoDTPurRqAmend.Rows[i]["ItemName"] = PurRqAmendDT.Rows[i]["ItemName"];
                    AutoDTPurRqAmend.Rows[i]["UomCode"] = PurRqAmendDT.Rows[i]["UOMCode"];
                    AutoDTPurRqAmend.Rows[i]["RequiredQty"] = PurRqAmendDT.Rows[i]["ReuiredQty"];
                    AutoDTPurRqAmend.Rows[i]["RequiredDate"] = PurRqAmendDT.Rows[i]["ReuiredDate"];
                    AutoDTPurRqAmend.Rows[i]["AmendQty"] = PurRqAmendDT.Rows[i]["AmendQty"];

                }

                //ds.Tables.Add(AutoDTPurRqAmend);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/PurchaseRequestAmendmentReport.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;


                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDTPurRqAmend);
                RD_Report.Load(Server.MapPath("~/crystal/PurchaseRequestAmendmentReport.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                //DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();



            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }

            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }


    }
    //Purchase Request Amendment Report Code End


    //Purchase Request Report Code Start
    public void Purchase_Request_Report()
    {

        if (RptName == "Purchase Request Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }
            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }
            if (FromDate != "" && ToDate != "" && PurReqNo == "")
            {

                SSQL = "select PM.Pur_Request_No,PM.Pur_Request_Date,PM.DeptName,PM.CostCenter,PM.CostElement,";
                SSQL = SSQL + "PM.Requestby,PM.Approvedby,PM.Others,PS.ItemCode,PS.ItemName,PS.UOMCode,PS.ReuiredQty,PS.ReuiredDate from Pur_Request_Main PM inner join Pur_Request_Main_Sub PS ";
                SSQL = SSQL + "on PM.Pur_Request_Date=PS.Pur_Request_Date where CONVERT(DATETIME,PM.Pur_Request_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,PM.Pur_Request_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                if (DeptName != "")
                {
                    SSQL = SSQL + " And PM.DeptName='" + DeptName + "'";
                }
                if (Requestby != "")
                {
                    SSQL = SSQL + " And PM.Requestby='" + Requestby + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And PS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And PM.Ccode='" + SessionCcode + "' And PM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And PM.FinYearCode='" + SessionFinYearCode + "' And PS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And PS.Lcode='" + SessionLcode + "' And PS.FinYearCode='" + SessionFinYearCode + "'";

                PurRqDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FromDate != "" && ToDate != "" && PurReqNo != "")
            {
                SSQL = "select PM.Pur_Request_No,PM.Pur_Request_Date,PM.DeptName,PM.CostCenter,PM.CostElement,";
                SSQL = SSQL + "PM.Requestby,PM.Approvedby,PM.Others,PS.ItemCode,PS.ItemName,PS.UOMCode,PS.ReuiredQty,PS.ReuiredDate from Pur_Request_Main PM inner join Pur_Request_Main_Sub PS ";
                SSQL = SSQL + "on PM.Pur_Request_No=PS.Pur_Request_No where PM.Pur_Request_No='" + PurReqNo + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,PM.Pur_Request_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,PM.Pur_Request_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                if (DeptName != "")
                {
                    SSQL = SSQL + " And PM.DeptName='" + DeptName + "'";
                }
                if (Requestby != "")
                {
                    SSQL = SSQL + " And PM.Requestby='" + Requestby + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And PS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And PM.Ccode='" + SessionCcode + "' And PM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And PM.FinYearCode='" + SessionFinYearCode + "' And PS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And PS.Lcode='" + SessionLcode + "' And PS.FinYearCode='" + SessionFinYearCode + "'";

                PurRqDT = objdata.RptEmployeeMultipleDetails(SSQL);


            }
            else if (PurReqNo != "")
            {

                SSQL = "select PM.Pur_Request_No,PM.Pur_Request_Date,PM.DeptName,PM.CostCenter,PM.CostElement,";
                SSQL = SSQL + "PM.Requestby,PM.Approvedby,PM.Others,PS.ItemCode,PS.ItemName,PS.UOMCode,PS.ReuiredQty,PS.ReuiredDate from Pur_Request_Main PM inner join Pur_Request_Main_Sub PS ";
                SSQL = SSQL + "on PM.Pur_Request_No=PS.Pur_Request_No where PM.Pur_Request_No='" + PurReqNo + "'";

                if (DeptName != "")
                {
                    SSQL = SSQL + " And PM.DeptName='" + DeptName + "'";
                }
                if (Requestby != "")
                {
                    SSQL = SSQL + " And PM.Requestby='" + Requestby + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And PS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And PM.Ccode='" + SessionCcode + "' And PM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And PM.FinYearCode='" + SessionFinYearCode + "' And PS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And PS.Lcode='" + SessionLcode + "' And PS.FinYearCode='" + SessionFinYearCode + "'";

                PurRqDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (DeptName != "")
            {

                SSQL = "select PM.Pur_Request_No,PM.Pur_Request_Date,PM.DeptName,PM.CostCenter,PM.CostElement,";
                SSQL = SSQL + "PM.Requestby,PM.Approvedby,PM.Others,PS.ItemCode,PS.ItemName,PS.UOMCode,PS.ReuiredQty,PS.ReuiredDate from Pur_Request_Main PM inner join Pur_Request_Main_Sub PS ";
                SSQL = SSQL + "on PM.Pur_Request_No=PS.Pur_Request_No where PM.DeptName='" + DeptName + "'";

                if (PurReqNo != "")
                {
                    SSQL = SSQL + " And PM.Pur_Request_No='" + PurReqNo + "'";
                }
                if (Requestby != "")
                {
                    SSQL = SSQL + " And PM.Requestby='" + Requestby + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And PS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And PM.Ccode='" + SessionCcode + "' And PM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And PM.FinYearCode='" + SessionFinYearCode + "' And PS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And PS.Lcode='" + SessionLcode + "' And PS.FinYearCode='" + SessionFinYearCode + "'";

                PurRqDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (Requestby != "")
            {

                SSQL = "select PM.Pur_Request_No,PM.Pur_Request_Date,PM.DeptName,PM.CostCenter,PM.CostElement,";
                SSQL = SSQL + "PM.Requestby,PM.Approvedby,PM.Others,PS.ItemCode,PS.ItemName,PS.UOMCode,PS.ReuiredQty,PS.ReuiredDate from Pur_Request_Main PM inner join Pur_Request_Main_Sub PS ";
                SSQL = SSQL + "on PM.Pur_Request_No=PS.Pur_Request_No where PM.Requestby='" + Requestby + "'";

                if (PurReqNo != "")
                {
                    SSQL = SSQL + " And PM.Pur_Request_No='" + PurReqNo + "'";
                }
                if (DeptName != "")
                {
                    SSQL = SSQL + " And PM.DeptName='" + DeptName + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And PS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And PM.Ccode='" + SessionCcode + "' And PM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And PM.FinYearCode='" + SessionFinYearCode + "' And PS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And PS.Lcode='" + SessionLcode + "' And PS.FinYearCode='" + SessionFinYearCode + "'";

                PurRqDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (ItemName != "")
            {

                SSQL = "select PM.Pur_Request_No,PM.Pur_Request_Date,PM.DeptName,PM.CostCenter,PM.CostElement,";
                SSQL = SSQL + "PM.Requestby,PM.Approvedby,PM.Others,PS.ItemCode,PS.ItemName,PS.UOMCode,PS.ReuiredQty,PS.ReuiredDate from Pur_Request_Main PM inner join Pur_Request_Main_Sub PS ";
                SSQL = SSQL + "on PM.Pur_Request_No=PS.Pur_Request_No where PS.ItemName='" + ItemName + "'";

                if (PurReqNo != "")
                {
                    SSQL = SSQL + " And PM.Pur_Request_No='" + PurReqNo + "'";
                }
                if (DeptName != "")
                {
                    SSQL = SSQL + " And PM.DeptName='" + DeptName + "'";
                }
                if (Requestby != "")
                {
                    SSQL = SSQL + " And PM.Requestby='" + Requestby + "'";
                }


                SSQL = SSQL + " And PM.Ccode='" + SessionCcode + "' And PM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And PM.FinYearCode='" + SessionFinYearCode + "' And PS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And PS.Lcode='" + SessionLcode + "' And PS.FinYearCode='" + SessionFinYearCode + "'";

                PurRqDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (PurRqDT.Rows.Count > 0)
            {


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTPurRq.Columns.Add("CompanyName");
                AutoDTPurRq.Columns.Add("LocationName");
                AutoDTPurRq.Columns.Add("CompanyCode");
                AutoDTPurRq.Columns.Add("LocationCode");
                AutoDTPurRq.Columns.Add("Address1");
                AutoDTPurRq.Columns.Add("Address2");
                AutoDTPurRq.Columns.Add("PurRequestNo");
                AutoDTPurRq.Columns.Add("PurRequestDate");
                AutoDTPurRq.Columns.Add("DeptName");
                AutoDTPurRq.Columns.Add("CostCenter");
                AutoDTPurRq.Columns.Add("CostElement");
                AutoDTPurRq.Columns.Add("Requestby");
                AutoDTPurRq.Columns.Add("Approvedby");
                AutoDTPurRq.Columns.Add("Others");


                AutoDTPurRq.Columns.Add("ItemCode");
                AutoDTPurRq.Columns.Add("ItemName");
                AutoDTPurRq.Columns.Add("UomCode");
                AutoDTPurRq.Columns.Add("RequiredQty");
                AutoDTPurRq.Columns.Add("RequiredDate");



                for (int i = 0; i < PurRqDT.Rows.Count; i++)
                {
                    AutoDTPurRq.NewRow();
                    AutoDTPurRq.Rows.Add();

                    AutoDTPurRq.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTPurRq.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTPurRq.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTPurRq.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTPurRq.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTPurRq.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                    AutoDTPurRq.Rows[i]["PurRequestNo"] = PurRqDT.Rows[i]["Pur_Request_No"];
                    AutoDTPurRq.Rows[i]["PurRequestDate"] = PurRqDT.Rows[i]["Pur_Request_Date"];
                    AutoDTPurRq.Rows[i]["DeptName"] = PurRqDT.Rows[i]["DeptName"];
                    AutoDTPurRq.Rows[i]["CostCenter"] = PurRqDT.Rows[i]["CostCenter"];
                    AutoDTPurRq.Rows[i]["CostElement"] = PurRqDT.Rows[i]["CostElement"];
                    AutoDTPurRq.Rows[i]["Requestby"] = PurRqDT.Rows[i]["Requestby"];
                    AutoDTPurRq.Rows[i]["Approvedby"] = PurRqDT.Rows[i]["Approvedby"];
                    AutoDTPurRq.Rows[i]["Others"] = PurRqDT.Rows[i]["Others"];



                    AutoDTPurRq.Rows[i]["ItemCode"] = PurRqDT.Rows[i]["ItemCode"];
                    AutoDTPurRq.Rows[i]["ItemName"] = PurRqDT.Rows[i]["ItemName"];
                    AutoDTPurRq.Rows[i]["UomCode"] = PurRqDT.Rows[i]["UOMCode"];
                    AutoDTPurRq.Rows[i]["RequiredQty"] = PurRqDT.Rows[i]["ReuiredQty"];
                    AutoDTPurRq.Rows[i]["RequiredDate"] = PurRqDT.Rows[i]["ReuiredDate"];


                }

                //ds.Tables.Add(AutoDTPurRq);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/PurchaseRequestReport.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                //CrystalReportViewer1.ReportSource = report;

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDTPurRq);
                RD_Report.Load(Server.MapPath("~/crystal/PurchaseRequestReport.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                //DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();



            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }

    }
    //Purchase Request Report Code End

    public void Summary_Material_Issue_Report()
    {
        string Heading = " ";

        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select MI.Mat_Issue_No,MI.Mat_Issue_Date,MI.CostCenterName,";
        SSQL = SSQL + "MI.Takenby,MI.Issuedby,MI.Remarks,MI.Mat_Req_No,MI.Mat_Req_Date,MS.ItemCode,MS.ItemName,MS.UOMCode,MS.DeptName,";
        SSQL = SSQL + "MS.RequestQty,MS.RequestDate,MS.IssueQty,MS.Value,MS.StockQty,MS.WarehouseName,MS.ZoneName,MS.BinName from Meterial_Issue_Main MI inner join Meterial_Issue_Main_Sub MS ";
        SSQL = SSQL + "on MI.Mat_Issue_No=MS.Mat_Issue_No where MI.Ccode='" + SessionCcode + "' And MI.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And MI.FinYearCode='" + SessionFinYearCode + "' And MS.Ccode='" + SessionCcode + "'";
        SSQL = SSQL + " And MS.Lcode='" + SessionLcode + "' And MS.FinYearCode='" + SessionFinYearCode + "' And MI.Mat_Issue_Status='1'";

        if (FromDate != "" && ToDate != "")
        {
            string DateFormat = FromDate + "   ToDate :" + ToDate;
            SSQL = SSQL + " And CONVERT(datetime,MI.Mat_Issue_Date, 103)>=CONVERT(datetime,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(datetime,MI.Mat_Issue_Date, 103)<=CONVERT(datetime,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            Heading = "FromDate : " + DateFormat;
        }
        if (WareHouse != "")
        {
            SSQL = SSQL + "And MS.WarehouseName='" + WareHouse + "'";
            Heading = "WarehouseName : " + WareHouse;
        }
        if (DeptName != "")
        {
            SSQL = SSQL + " And MS.DeptName='" + DeptName + "'";
            Heading = "DeptName : " + DeptName;
        }
        if (MatIssBy != "")
        {
            SSQL = SSQL + " And MI.Issuedby='" + MatIssBy + "'";
            Heading = "MatIssBy : " + MatIssBy;
        }
        if (ItemName != "")
        {
            SSQL = SSQL + " And MS.ItemName='" + ItemName + "'";
            Heading = "ItemName : " + ItemName;
        }
        if (MatIssNo != "")
        {
            SSQL = SSQL + " And MI.Mat_Issue_No='" + MatIssNo + "'";
            Heading = "MatIssNo = " + MatIssNo;
        }
        if (CostCenter != "-Select-")
        {
            SSQL = SSQL + " And MS.CostCenterName='" + CostCenter + "'";
            Heading = "CostCenter : " + CostCenter;
        }

        SSQL = SSQL + " group by  MI.CostCenterName,MI.Mat_Issue_No,MI.Mat_Issue_Date,MI.Takenby,MI.Issuedby,MI.Remarks, ";
        SSQL = SSQL + " MI.Mat_Req_No,MI.Mat_Req_Date,MS.ItemCode,MS.ItemName,MS.UOMCode,MS.DeptName,MS.RequestQty,";
        SSQL = SSQL + " MS.RequestDate,MS.IssueQty,MS.Value,MS.StockQty,MS.WarehouseName,MS.ZoneName,MS.BinName";
        SSQL = SSQL + " order by  Mat_Issue_Date";

        MatIssDT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (MatIssDT.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


            AutoDTMatIss.Columns.Add("CompanyName");
            AutoDTMatIss.Columns.Add("LocationName");
            AutoDTMatIss.Columns.Add("CompanyCode");
            AutoDTMatIss.Columns.Add("LocationCode");
            AutoDTMatIss.Columns.Add("Address1");
            AutoDTMatIss.Columns.Add("Address2");
            AutoDTMatIss.Columns.Add("MatIssueNo");
            AutoDTMatIss.Columns.Add("MatIssueDate");
            AutoDTMatIss.Columns.Add("WarehouseName");
            AutoDTMatIss.Columns.Add("DeptName");
            AutoDTMatIss.Columns.Add("CostCenterName");
            AutoDTMatIss.Columns.Add("Takenby");
            AutoDTMatIss.Columns.Add("Issuedby");
            AutoDTMatIss.Columns.Add("IssueType");
            AutoDTMatIss.Columns.Add("MatReqNo");
            AutoDTMatIss.Columns.Add("MatReqDate");
            AutoDTMatIss.Columns.Add("ItemCode");
            AutoDTMatIss.Columns.Add("ItemName");
            AutoDTMatIss.Columns.Add("UomCode");
            AutoDTMatIss.Columns.Add("RequestQty");
            AutoDTMatIss.Columns.Add("RequestDate");
            AutoDTMatIss.Columns.Add("IssueQty");
            AutoDTMatIss.Columns.Add("StockQty");
            AutoDTMatIss.Columns.Add("Value");
            AutoDTMatIss.Columns.Add("ZoneName");
            AutoDTMatIss.Columns.Add("BinName");


            for (int i = 0; i < MatIssDT.Rows.Count; i++)
            {
                AutoDTMatIss.NewRow();
                AutoDTMatIss.Rows.Add();

                AutoDTMatIss.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                AutoDTMatIss.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                AutoDTMatIss.Rows[i]["CompanyCode"] = SessionCcode;
                AutoDTMatIss.Rows[i]["LocationCode"] = SessionLcode;
                AutoDTMatIss.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                AutoDTMatIss.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
                AutoDTMatIss.Rows[i]["MatIssueNo"] = MatIssDT.Rows[i]["Mat_Issue_No"];
                AutoDTMatIss.Rows[i]["MatIssueDate"] = MatIssDT.Rows[i]["Mat_Issue_Date"];
                AutoDTMatIss.Rows[i]["ItemCode"] = MatIssDT.Rows[i]["ItemCode"];
                AutoDTMatIss.Rows[i]["ItemName"] = MatIssDT.Rows[i]["ItemName"];
                AutoDTMatIss.Rows[i]["UomCode"] = MatIssDT.Rows[i]["UOMCode"];
                AutoDTMatIss.Rows[i]["RequestQty"] = MatIssDT.Rows[i]["RequestQty"];
                AutoDTMatIss.Rows[i]["RequestDate"] = MatIssDT.Rows[i]["RequestDate"];
                AutoDTMatIss.Rows[i]["IssueQty"] = MatIssDT.Rows[i]["IssueQty"];
                AutoDTMatIss.Rows[i]["StockQty"] = MatIssDT.Rows[i]["StockQty"];
                AutoDTMatIss.Rows[i]["Value"] = MatIssDT.Rows[i]["Value"];
                AutoDTMatIss.Rows[i]["ZoneName"] = MatIssDT.Rows[i]["ZoneName"];
                AutoDTMatIss.Rows[i]["BinName"] = MatIssDT.Rows[i]["BinName"];
                AutoDTMatIss.Rows[i]["CostCenterName"] = MatIssDT.Rows[i]["CostCenterName"];


            }

            ds.Tables.Add(AutoDTMatIss);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/Sample_MaterialIssueSlip_Sorting.rpt"));
            report.DataDefinition.FormulaFields["ReportHeading"].Text = "'" + Heading + "'";
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;



        }

        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }

    public void Sample_Material_Issue_Report()
    {
        string Heading = " ";

        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select MI.Mat_Issue_No,MI.Mat_Issue_Date,MS.CostCenterName,";
        SSQL = SSQL + "MI.Takenby,MI.Issuedby,MI.Remarks,MI.Mat_Req_No,MI.Mat_Req_Date,MS.ItemCode,MS.ItemName,MS.UOMCode,MS.DeptName,";
        SSQL = SSQL + "MS.RequestQty,MS.RequestDate,MS.IssueQty,MS.Value,MS.StockQty,MS.WarehouseName,MS.ZoneName,MS.BinName from Meterial_Issue_Main MI inner join Meterial_Issue_Main_Sub MS ";
        SSQL = SSQL + "on MI.Mat_Issue_No=MS.Mat_Issue_No where MI.Ccode='" + SessionCcode + "' And MI.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And MI.FinYearCode='" + SessionFinYearCode + "' And MS.Ccode='" + SessionCcode + "'";
        SSQL = SSQL + " And MS.Lcode='" + SessionLcode + "' And MS.FinYearCode='" + SessionFinYearCode + "' And MI.Mat_Issue_Status='1'";

        if (FromDate != "" && ToDate != "")
        {
            string DateFormat = FromDate + "   ToDate :" + ToDate;
            SSQL = SSQL + " And CONVERT(datetime,MI.Mat_Issue_Date, 103)>=CONVERT(datetime,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(datetime,MI.Mat_Issue_Date, 103)<=CONVERT(datetime,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            Heading = "FromDate : " + DateFormat;
        }
        if (WareHouse != "")
        {
            SSQL = SSQL + "And MS.WarehouseName='" + WareHouse + "'";
            Heading = "WarehouseName : " + WareHouse;
        }
        if (DeptName != "")
        {
            SSQL = SSQL + " And MS.DeptName='" + DeptName + "'";
            Heading = "DeptName : " + DeptName;
        }
        if (MatIssBy != "")
        {
            SSQL = SSQL + " And MI.Issuedby='" + MatIssBy + "'";
            Heading = "MatIssBy : " + MatIssBy;
        }
        if (ItemName != "")
        {
            SSQL = SSQL + " And MS.ItemCode='" + ItemName + "'";
            //Heading = "ItemName : " + ItemName;
        }
        if (MatIssNo != "")
        {
            SSQL = SSQL + " And MI.Mat_Issue_No='" + MatIssNo + "'";
            Heading = "MatIssNo = " + MatIssNo;
        }
        if (CostCenter != "")
        {
            SSQL = SSQL + " And MS.CostCenterName='" + CostCenter + "'";
            Heading = "CostCenter : " + CostCenter;
        }

        //SSQL = SSQL + " group by  MS.CostCenterName,MI.Mat_Issue_No,MI.Mat_Issue_Date,MI.Takenby,MI.Issuedby,MI.Remarks, ";
        //SSQL = SSQL + " MI.Mat_Req_No,MI.Mat_Req_Date,MS.ItemCode,MS.ItemName,MS.UOMCode,MS.DeptName,MS.RequestQty,";
        //SSQL = SSQL + " MS.RequestDate,MS.IssueQty,MS.Value,MS.StockQty,MS.WarehouseName,MS.ZoneName,MS.BinName";
        //SSQL = SSQL + " order by  Mat_Issue_Date";

        MatIssDT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (MatIssDT.Rows.Count > 0)
        {
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


            AutoDTMatIss.Columns.Add("CompanyName");
            AutoDTMatIss.Columns.Add("LocationName");
            AutoDTMatIss.Columns.Add("CompanyCode");
            AutoDTMatIss.Columns.Add("LocationCode");
            AutoDTMatIss.Columns.Add("Address1");
            AutoDTMatIss.Columns.Add("Address2");
            AutoDTMatIss.Columns.Add("MatIssueNo");
            AutoDTMatIss.Columns.Add("MatIssueDate");
            AutoDTMatIss.Columns.Add("WarehouseName");
            AutoDTMatIss.Columns.Add("DeptName");
            AutoDTMatIss.Columns.Add("CostCenterName");
            AutoDTMatIss.Columns.Add("Takenby");
            AutoDTMatIss.Columns.Add("Issuedby");
            AutoDTMatIss.Columns.Add("IssueType");
            AutoDTMatIss.Columns.Add("MatReqNo");
            AutoDTMatIss.Columns.Add("MatReqDate");
            AutoDTMatIss.Columns.Add("ItemCode");
            AutoDTMatIss.Columns.Add("ItemName");
            AutoDTMatIss.Columns.Add("UomCode");
            AutoDTMatIss.Columns.Add("RequestQty");
            AutoDTMatIss.Columns.Add("RequestDate");
            AutoDTMatIss.Columns.Add("IssueQty");
            AutoDTMatIss.Columns.Add("StockQty");
            AutoDTMatIss.Columns.Add("Value");
            AutoDTMatIss.Columns.Add("ZoneName");
            AutoDTMatIss.Columns.Add("BinName");


            for (int i = 0; i < MatIssDT.Rows.Count; i++)
            {
                AutoDTMatIss.NewRow();
                AutoDTMatIss.Rows.Add();

                AutoDTMatIss.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                AutoDTMatIss.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                AutoDTMatIss.Rows[i]["CompanyCode"] = SessionCcode;
                AutoDTMatIss.Rows[i]["LocationCode"] = SessionLcode;
                AutoDTMatIss.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                AutoDTMatIss.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
                AutoDTMatIss.Rows[i]["MatIssueNo"] = MatIssDT.Rows[i]["Mat_Issue_No"];
                AutoDTMatIss.Rows[i]["MatIssueDate"] = MatIssDT.Rows[i]["Mat_Issue_Date"];
                AutoDTMatIss.Rows[i]["ItemCode"] = MatIssDT.Rows[i]["ItemCode"];
                AutoDTMatIss.Rows[i]["ItemName"] = MatIssDT.Rows[i]["ItemName"];
                AutoDTMatIss.Rows[i]["UomCode"] = MatIssDT.Rows[i]["UOMCode"];
                AutoDTMatIss.Rows[i]["RequestQty"] = MatIssDT.Rows[i]["RequestQty"];
                AutoDTMatIss.Rows[i]["RequestDate"] = MatIssDT.Rows[i]["RequestDate"];
                AutoDTMatIss.Rows[i]["IssueQty"] = MatIssDT.Rows[i]["IssueQty"];
                AutoDTMatIss.Rows[i]["StockQty"] = MatIssDT.Rows[i]["StockQty"];
                AutoDTMatIss.Rows[i]["Value"] = MatIssDT.Rows[i]["Value"];
                AutoDTMatIss.Rows[i]["ZoneName"] = MatIssDT.Rows[i]["ZoneName"];
                AutoDTMatIss.Rows[i]["BinName"] = MatIssDT.Rows[i]["BinName"];
                AutoDTMatIss.Rows[i]["CostCenterName"] = MatIssDT.Rows[i]["CostCenterName"];


            }

            ds.Tables.Add(AutoDTMatIss);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/Sample_MaterialIssueSlip_Sorting.rpt"));
            report.DataDefinition.FormulaFields["ReportHeading"].Text = "'" + Heading + "'";
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;



        }

        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }


    }

    public void OtherUnit_Report_Details()
    {
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select CMP.CName,MI.Mat_Issue_No,MI.Mat_Issue_Date,MS.CostCenterName,MI.Takenby,MI.Issuedby,MI.Remarks,MS.ItemCode,MS.ItemName,";
        SSQL = SSQL + "MS.UOMCode,MS.DeptName,MS.IssueQty,MS.Value,MS.StockQty,MS.WarehouseName,MS.ZoneName,MS.BinName,MI.OtherUnitName from Meterial_Issue_Main MI ";
        SSQL = SSQL + "inner join Meterial_Issue_Main_Sub MS on MI.Mat_Issue_No=MS.Mat_Issue_No Inner join AdminRights CMP on CMP.Ccode=MI.Ccode ";
        SSQL = SSQL + " where MI.Ccode='" + SessionCcode + "' And MI.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And MI.FinYearCode='" + SessionFinYearCode + "' And MS.Ccode='" + SessionCcode + "' And MI.OtherUnit_Status='1'";
        SSQL = SSQL + " And MS.Lcode='" + SessionLcode + "' And MS.FinYearCode='" + SessionFinYearCode + "' And MI.Mat_Issue_Status='1'";

        if (FromDate != "" && ToDate != "")
        {
            string DateFormat = FromDate + "   ToDate :" + ToDate;
            SSQL = SSQL + " And CONVERT(datetime,MI.Mat_Issue_Date, 103)>=CONVERT(datetime,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(datetime,MI.Mat_Issue_Date, 103)<=CONVERT(datetime,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

        }
        if (WareHouse != "")
        {
            SSQL = SSQL + "And MS.WarehouseName='" + WareHouse + "'";

        }
        if (DeptName != "")
        {
            SSQL = SSQL + " And MS.DeptName='" + DeptName + "'";

        }
        if (MatIssBy != "")
        {
            SSQL = SSQL + " And MI.Issuedby='" + MatIssBy + "'";

        }
        if (ItemName != "")
        {
            SSQL = SSQL + " And MS.ItemCode='" + ItemName + "'";

        }
        if (MatIssNo != "")
        {
            SSQL = SSQL + " And MI.Mat_Issue_No='" + MatIssNo + "'";

        }
        if (CostCenter != "")
        {
            SSQL = SSQL + " And MS.CostCenterName='" + CostCenter + "'";

        }

        if (CostCenter != "")
        {
            SSQL = SSQL + " And MS.CostCenterName='" + CostCenter + "'";

        }

        if (OtherUnit != "")
        {
            SSQL = SSQL + " And MI.OtherUnitName='" + OtherUnit + "'";

        }

        MatIssDT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (MatIssDT.Rows.Count > 0)
        {
            ds.Tables.Add(MatIssDT);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/MaterialIssueOtherUnit.rpt"));
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.DataDefinition.FormulaFields["FDate"].Text = "'" + FromDate + "'";
            report.DataDefinition.FormulaFields["TDate"].Text = "'" + ToDate + "'";
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }

    public void ItemType_Report_Details()
    {
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select CMP.Cname,MI.Mat_Issue_No,MI.Mat_Issue_Date,MS.CostCenterName,MI.Takenby,MI.Issuedby,MI.Remarks,MS.ItemCode,MS.ItemName,";
        SSQL = SSQL + "MS.UOMCode,MS.DeptName,MS.IssueQty,MS.Value,MS.StockQty,MS.WarehouseName,MS.ZoneName,MS.BinName,Itm.ItemType from Meterial_Issue_Main MI ";
        SSQL = SSQL + "Inner join Meterial_Issue_Main_Sub MS on MI.Mat_Issue_No=MS.Mat_Issue_No ";
        SSQL = SSQL + "Inner join MstItemMaster Itm on Itm.ItemCode=MS.ItemCode Inner join AdminRights CMP on CMP.Ccode=MI.Ccode  ";
        SSQL = SSQL + "Where MI.Ccode='" + SessionCcode + "' And MI.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + "And MI.FinYearCode='" + SessionFinYearCode + "' And MS.Ccode='" + SessionCcode + "' ";
        SSQL = SSQL + "And MS.Lcode='" + SessionLcode + "' And MS.FinYearCode='" + SessionFinYearCode + "' And MI.Mat_Issue_Status='1'";

        if (FromDate != "" && ToDate != "")
        {
            string DateFormat = FromDate + "   ToDate :" + ToDate;
            SSQL = SSQL + " And CONVERT(datetime,MI.Mat_Issue_Date, 103)>=CONVERT(datetime,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(datetime,MI.Mat_Issue_Date, 103)<=CONVERT(datetime,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

        }
        if (WareHouse != "")
        {
            SSQL = SSQL + "And MS.WarehouseName='" + WareHouse + "'";

        }
        if (DeptName != "")
        {
            SSQL = SSQL + " And MS.DeptName='" + DeptName + "'";

        }
        if (MatIssBy != "")
        {
            SSQL = SSQL + " And MI.Issuedby='" + MatIssBy + "'";

        }
        if (ItemName != "")
        {
            SSQL = SSQL + " And MS.ItemCode='" + ItemName + "'";

        }
        if (MatIssNo != "")
        {
            SSQL = SSQL + " And MI.Mat_Issue_No='" + MatIssNo + "'";

        }
        if (CostCenter != "")
        {
            SSQL = SSQL + " And MS.CostCenterName='" + CostCenter + "'";

        }

        if (ItemType != "")
        {
            SSQL = SSQL + " And Itm.ItemType='" + ItemType + "'";

        }

        MatIssDT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (MatIssDT.Rows.Count > 0)
        {
            ds.Tables.Add(MatIssDT);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/MaterialIssueItemType.rpt"));
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.DataDefinition.FormulaFields["FDate"].Text = "'" + FromDate + "'";
            report.DataDefinition.FormulaFields["TDate"].Text = "'" + ToDate + "'";
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }


    //Material Issue Report Code Start
    public void Material_Issue_Report()
    {

        if (RptName == "Material Issue Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }
            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            SSQL = "select MI.Mat_Issue_No,MI.Mat_Issue_Date,MI.CostCenterName,";
            SSQL = SSQL + "MI.Takenby,MI.Issuedby,MI.Remarks,MI.Mat_Req_No,MI.Mat_Req_Date,MS.ItemCode,MS.ItemName,MS.UOMCode,Itm.ItemType,MS.DeptName,";
            SSQL = SSQL + "MS.RequestQty,MS.RequestDate,MS.IssueQty,MS.Value,MS.StockQty,MS.WarehouseName,MS.ZoneName,MS.BinName from Meterial_Issue_Main MI ";
            SSQL = SSQL + "Inner join Meterial_Issue_Main_Sub MS on MI.Mat_Issue_No=MS.Mat_Issue_No ";
            SSQL = SSQL + "inner join MstItemMaster Itm on Itm.ItemCode=MS.ItemCode ";
            SSQL = SSQL + "Where MI.Ccode='" + SessionCcode + "' And MI.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And MI.FinYearCode='" + SessionFinYearCode + "' And MS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And MS.Lcode='" + SessionLcode + "' And MS.FinYearCode='" + SessionFinYearCode + "' "; // And MI.Mat_Issue_Status='1'";

            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,MI.Mat_Issue_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,MI.Mat_Issue_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (WareHouse != "")
            {
                SSQL = SSQL + "And MS.WarehouseName='" + WareHouse + "'";
            }
            if (DeptName != "")
            {
                SSQL = SSQL + " And MS.DeptName='" + DeptName + "'";
            }
            if (MatIssBy != "")
            {
                SSQL = SSQL + " And MI.Issuedby='" + MatIssBy + "'";
            }
            if (ItemName != "")
            {
                SSQL = SSQL + " And MS.ItemName='" + ItemName + "'";
            }
            if (MatIssNo != "")
            {
                SSQL = SSQL + " And MI.Mat_Issue_No='" + MatIssNo + "'";
            }
            if (CostCenter != "")
            {
                SSQL = SSQL + " And MS.CostCenterName='" + CostCenterName + "'";
            }
            if (ItemType != "")
            {
                SSQL = SSQL + " And ITM.ItemType='" + ItemType + "'";
            }
            MatIssDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (MatIssDT.Rows.Count > 0)
            {
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State,Phone,GSTNo From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTMatIss.Columns.Add("CompanyName");
                AutoDTMatIss.Columns.Add("LocationName");
                AutoDTMatIss.Columns.Add("CompanyCode");
                AutoDTMatIss.Columns.Add("LocationCode");
                AutoDTMatIss.Columns.Add("Address1");
                AutoDTMatIss.Columns.Add("Address2");
                AutoDTMatIss.Columns.Add("Mobile");
                AutoDTMatIss.Columns.Add("GSTNo");
                AutoDTMatIss.Columns.Add("MatIssueNo");
                AutoDTMatIss.Columns.Add("MatIssueDate");
                AutoDTMatIss.Columns.Add("WarehouseName");
                AutoDTMatIss.Columns.Add("DeptName");
                AutoDTMatIss.Columns.Add("ItemType");
                AutoDTMatIss.Columns.Add("CostCenterName");
                AutoDTMatIss.Columns.Add("Takenby");
                AutoDTMatIss.Columns.Add("Issuedby");
                AutoDTMatIss.Columns.Add("IssueType");
                AutoDTMatIss.Columns.Add("MatReqNo");
                AutoDTMatIss.Columns.Add("MatReqDate");


                AutoDTMatIss.Columns.Add("ItemCode");
                AutoDTMatIss.Columns.Add("ItemName");
                AutoDTMatIss.Columns.Add("UomCode");
                AutoDTMatIss.Columns.Add("RequestQty");
                AutoDTMatIss.Columns.Add("RequestDate");
                AutoDTMatIss.Columns.Add("IssueQty");
                AutoDTMatIss.Columns.Add("StockQty");
                AutoDTMatIss.Columns.Add("Value");
                AutoDTMatIss.Columns.Add("ZoneName");
                AutoDTMatIss.Columns.Add("BinName");


                for (int i = 0; i < MatIssDT.Rows.Count; i++)
                {
                    AutoDTMatIss.NewRow();
                    AutoDTMatIss.Rows.Add();

                    AutoDTMatIss.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTMatIss.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTMatIss.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTMatIss.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTMatIss.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString();
                    AutoDTMatIss.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Address2"].ToString() + "," + dtdCompanyAddress.Rows[0]["PinCode"].ToString();
                    AutoDTMatIss.Rows[i]["Mobile"] = dtdCompanyAddress.Rows[0]["Phone"].ToString(); ;
                    AutoDTMatIss.Rows[i]["GSTNo"] = dtdCompanyAddress.Rows[0]["GSTNo"].ToString(); ;

                    AutoDTMatIss.Rows[i]["MatIssueNo"] = MatIssDT.Rows[i]["Mat_Issue_No"];
                    AutoDTMatIss.Rows[i]["MatIssueDate"] = MatIssDT.Rows[i]["Mat_Issue_Date"];
                    AutoDTMatIss.Rows[i]["WarehouseName"] = MatIssDT.Rows[i]["WarehouseName"];
                    AutoDTMatIss.Rows[i]["DeptName"] = MatIssDT.Rows[i]["DeptName"];
                    AutoDTMatIss.Rows[i]["ItemType"] = MatIssDT.Rows[i]["ItemType"];
                    AutoDTMatIss.Rows[i]["CostCenterName"] = MatIssDT.Rows[i]["CostCenterName"];
                    AutoDTMatIss.Rows[i]["Takenby"] = MatIssDT.Rows[i]["Takenby"];
                    AutoDTMatIss.Rows[i]["Issuedby"] = MatIssDT.Rows[i]["Issuedby"];
                    AutoDTMatIss.Rows[i]["IssueType"] = MatIssDT.Rows[i]["Remarks"];
                    AutoDTMatIss.Rows[i]["MatReqNo"] = MatIssDT.Rows[i]["Mat_Req_No"];
                    AutoDTMatIss.Rows[i]["MatReqDate"] = MatIssDT.Rows[i]["Mat_Req_Date"];

                    AutoDTMatIss.Rows[i]["ItemCode"] = MatIssDT.Rows[i]["ItemCode"];
                    AutoDTMatIss.Rows[i]["ItemName"] = MatIssDT.Rows[i]["ItemName"];
                    AutoDTMatIss.Rows[i]["UomCode"] = MatIssDT.Rows[i]["UOMCode"];
                    AutoDTMatIss.Rows[i]["RequestQty"] = MatIssDT.Rows[i]["RequestQty"];
                    AutoDTMatIss.Rows[i]["RequestDate"] = MatIssDT.Rows[i]["RequestDate"];
                    AutoDTMatIss.Rows[i]["IssueQty"] = MatIssDT.Rows[i]["IssueQty"];
                    AutoDTMatIss.Rows[i]["StockQty"] = MatIssDT.Rows[i]["StockQty"];
                    AutoDTMatIss.Rows[i]["Value"] = MatIssDT.Rows[i]["Value"];
                    AutoDTMatIss.Rows[i]["ZoneName"] = MatIssDT.Rows[i]["ZoneName"];
                    AutoDTMatIss.Rows[i]["BinName"] = MatIssDT.Rows[i]["BinName"];
                }

                if (ButtonName == "Issue Slip")
                {
                    if (AutoDTMatIss.Rows.Count < 10)
                    {
                        int Row_Count = AutoDTMatIss.Rows.Count;
                        for (int i = Row_Count; i < 10; i++)
                        {
                            AutoDTMatIss.NewRow();
                            AutoDTMatIss.Rows.Add();
                            AutoDTMatIss.Rows[i]["MatIssueNo"] = AutoDTMatIss.Rows[0]["MatIssueNo"];
                        }
                    }
                    ds.Tables.Add(AutoDTMatIss);
                    ReportDocument report = new ReportDocument();
                    report.Load(Server.MapPath("~/crystal/MaterialIssueSlip_Two_Copy.rpt"));
                    report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                    CrystalReportViewer1.ReportSource = report;
                }

                if (ButtonName == "Details Report")
                {
                    ds.Tables.Add(AutoDTMatIss);
                    ReportDocument report = new ReportDocument();
                    report.Load(Server.MapPath("~/crystal/MaterialIssueDetailsReport.rpt"));
                    report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                    CrystalReportViewer1.ReportSource = report;
                }
            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }

        //Material Issue Item Report
        if (RptName == "Material Issue Item Report")
        {
            if (FromDate != "") { frmDate = Convert.ToDateTime(FromDate); }
            if (ToDate != "") { toDate = Convert.ToDateTime(ToDate); }
            SSQL = "select MS.DeptName,Itm.ItemType,MS.ItemName,MS.UOMCode,sum(MS.IssueQty) as IssueQty,sum(MS.Value) as Value ";
            SSQL = SSQL + "From Meterial_Issue_Main MI Inner join Meterial_Issue_Main_Sub MS on MI.Mat_Issue_No = MS.Mat_Issue_No ";
            SSQL = SSQL + "Inner join MstItemMaster Itm on Itm.ItemCode=MS.ItemCode ";
            SSQL = SSQL + "where MI.Ccode='" + SessionCcode + "' And MI.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And MI.FinYearCode='" + SessionFinYearCode + "' And MS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And MS.Lcode='" + SessionLcode + "' And MS.FinYearCode='" + SessionFinYearCode + "' And MI.Mat_Issue_Status='1'";

            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,MI.Mat_Issue_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,MI.Mat_Issue_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (WareHouse != "")
            {
                SSQL = SSQL + "And MS.WarehouseName='" + WareHouse + "'";
            }
            if (DeptName != "")
            {
                SSQL = SSQL + " And MS.DeptName='" + DeptName + "'";
            }
            if (MatIssBy != "")
            {
                SSQL = SSQL + " And MI.Issuedby='" + MatIssBy + "'";
            }
            if (CostCenterName != "")
            {
                SSQL = SSQL + " And MS.CostCenterName='" + CostCenterName + "'";
            }

            if (ItemType != "")
            {
                SSQL = SSQL + " And Itm.ItemType='" + ItemType + "'";
            }


            SSQL = SSQL + " Group by MS.DeptName,Itm.ItemType,MS.ItemName,MS.UOMCode";
            SSQL = SSQL + " Order by MS.DeptName,Itm.ItemType,MS.ItemName Asc";

            MatIssDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (MatIssDT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(MatIssDT);
                RD_Report.Load(Server.MapPath("~/crystal/MaterialIssue_ItemAll.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }

        //Material Issue Department Report
        if (RptName == "Material Issue Department Report")
        {
            if (FromDate != "") { frmDate = Convert.ToDateTime(FromDate); }
            if (ToDate != "") { toDate = Convert.ToDateTime(ToDate); }
            SSQL = "select MS.DeptName,sum(MS.IssueQty) as IssueQty,sum(MS.Value) as Value";
            SSQL = SSQL + " from Meterial_Issue_Main MI inner join Meterial_Issue_Main_Sub MS ";
            SSQL = SSQL + "on MI.Mat_Issue_No=MS.Mat_Issue_No where MI.Ccode='" + SessionCcode + "' And MI.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And MI.FinYearCode='" + SessionFinYearCode + "' And MS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And MS.Lcode='" + SessionLcode + "' And MS.FinYearCode='" + SessionFinYearCode + "' And MI.Mat_Issue_Status='1'";

            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,MI.Mat_Issue_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,MI.Mat_Issue_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (WareHouse != "")
            {
                SSQL = SSQL + "And MS.WarehouseName='" + WareHouse + "'";
            }
            if (DeptName != "")
            {
                SSQL = SSQL + " And MS.DeptName='" + DeptName + "'";
            }
            if (MatIssBy != "")
            {
                SSQL = SSQL + " And MI.Issuedby='" + MatIssBy + "'";
            }
            if (CostCenter != "")
            {
                SSQL = SSQL + " And MS.CostCenterName='" + CostCenter + "'";
            }

            SSQL = SSQL + " Group by MS.DeptName";
            SSQL = SSQL + " Order by MS.DeptName Asc";

            MatIssDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (MatIssDT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(MatIssDT);
                RD_Report.Load(Server.MapPath("~/crystal/MaterialIssue_Department_Cost.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }

        //Material Issue Costcenter Report
        if (RptName == "Material Issue Costcenter Report")
        {
            if (FromDate != "") { frmDate = Convert.ToDateTime(FromDate); }
            if (ToDate != "") { toDate = Convert.ToDateTime(ToDate); }
            SSQL = "select MS.CostCenterName as DeptName,sum(MS.IssueQty) as IssueQty,sum(MS.Value) as Value";
            SSQL = SSQL + " from Meterial_Issue_Main MI inner join Meterial_Issue_Main_Sub MS ";
            SSQL = SSQL + "on MI.Mat_Issue_No=MS.Mat_Issue_No where MI.Ccode='" + SessionCcode + "' And MI.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And MI.FinYearCode='" + SessionFinYearCode + "' And MS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And MS.Lcode='" + SessionLcode + "' And MS.FinYearCode='" + SessionFinYearCode + "' And MI.Mat_Issue_Status='1'";

            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,MI.Mat_Issue_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,MI.Mat_Issue_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (WareHouse != "")
            {
                SSQL = SSQL + "And MS.WarehouseName='" + WareHouse + "'";
            }
            if (DeptName != "")
            {
                SSQL = SSQL + " And MS.DeptName='" + DeptName + "'";
            }
            if (MatIssBy != "")
            {
                SSQL = SSQL + " And MI.Issuedby='" + MatIssBy + "'";
            }
            if (CostCenter != "")
            {
                SSQL = SSQL + " And MS.CostCenterName='" + CostCenter + "'";
            }

            SSQL = SSQL + " Group by MS.CostCenterName";
            SSQL = SSQL + " Order by MS.CostCenterName Asc";

            MatIssDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (MatIssDT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(MatIssDT);
                RD_Report.Load(Server.MapPath("~/crystal/MaterialIssue_Costcenter_Cost.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }


    }
    //Material Issue Report Code End

    //Material Issue Return Code Start
    public void Material_Issue_Return_Report()
    {


        if (RptName == "Material Issue Return Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }
            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }
            if (FromDate != "" && ToDate != "" && MatIssRetNo == "")
            {

                SSQL = "select MRM.Mat_Issue_Return_No,MRM.Mat_Issue_Return_Date,MRM.WarehouseName,MRM.DeptName,MRM.Mat_Issue_No,";
                SSQL = SSQL + "MRM.Mat_Issue_Date,MRM.CostCenterName,MRM.Returnby,MRM.Receivedby,MRM.Remarks,MRS.ItemCode,MRS.ItemName,MRS.UOMCode,";
                SSQL = SSQL + "MRS.ReturnQty,MRS.Value,MRS.ZoneName,MRS.BinName from Meterial_Issue_Return_Main MRM inner join Meterial_Issue_Return_Main_Sub MRS ";
                SSQL = SSQL + "on MRM.Mat_Issue_Return_Date=MRS.Mat_Issue_Return_Date where CONVERT(DATETIME,MRM.Mat_Issue_Return_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,MRS.Mat_Issue_Return_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (WareHouse != "")
                {
                    SSQL = SSQL + "And MRM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And MRM.DeptName='" + DeptName + "'";
                }

                if (MatRetBy != "")
                {
                    SSQL = SSQL + " And MRM.Returnby='" + MatRetBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MRS.ItemName='" + ItemName + "'";
                }
                if (CostCenter != "-Select-")
                {
                    SSQL = SSQL + " And MS.CostCenterName='" + CostCenter + "'";
                }

                SSQL = SSQL + " And MRM.Ccode='" + SessionCcode + "' And MRM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MRM.FinYearCode='" + SessionFinYearCode + "' And MRS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MRS.Lcode='" + SessionLcode + "' And MRS.FinYearCode='" + SessionFinYearCode + "' And MRM.Mat_Issue_Return_Status='1'";

                MatIssRetDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FromDate != "" && ToDate != "" && MatIssRetNo != "")
            {

                SSQL = "select MRM.Mat_Issue_Return_No,MRM.Mat_Issue_Return_Date,MRM.WarehouseName,MRM.DeptName,MRM.Mat_Issue_No,";
                SSQL = SSQL + "MRM.Mat_Issue_Date,MRM.CostCenterName,MRM.Returnby,MRM.Receivedby,MRM.Remarks,MRS.ItemCode,MRS.ItemName,MRS.UOMCode,";
                SSQL = SSQL + "MRS.ReturnQty,MRS.Value,MRS.ZoneName,MRS.BinName from Meterial_Issue_Return_Main MRM inner join Meterial_Issue_Return_Main_Sub MRS ";
                SSQL = SSQL + "on MRM.Mat_Issue_Return_No=MRS.Mat_Issue_Return_No where MRM.Mat_Issue_Return_No='" + MatIssRetNo + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,MRM.Mat_Issue_Return_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,MRS.Mat_Issue_Return_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";



                if (WareHouse != "")
                {
                    SSQL = SSQL + "And MRM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And MRM.DeptName='" + DeptName + "'";
                }

                if (MatRetBy != "")
                {
                    SSQL = SSQL + " And MRM.Returnby='" + MatRetBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MRS.ItemName='" + ItemName + "'";
                }

                if (CostCenter != "-Select-")
                {
                    SSQL = SSQL + " And MS.CostCenterName='" + CostCenter + "'";
                }
                SSQL = SSQL + " And MRM.Ccode='" + SessionCcode + "' And MRM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MRM.FinYearCode='" + SessionFinYearCode + "' And MRS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MRS.Lcode='" + SessionLcode + "' And MRS.FinYearCode='" + SessionFinYearCode + "' And MRM.Mat_Issue_Return_Status='1'";

                MatIssRetDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (MatIssRetNo != "")
            {
                SSQL = "select MRM.Mat_Issue_Return_No,MRM.Mat_Issue_Return_Date,MRM.WarehouseName,MRM.DeptName,MRM.Mat_Issue_No,";
                SSQL = SSQL + "MRM.Mat_Issue_Date,MRM.CostCenterName,MRM.Returnby,MRM.Receivedby,MRM.Remarks,MRS.ItemCode,MRS.ItemName,MRS.UOMCode,";
                SSQL = SSQL + "MRS.ReturnQty,MRS.Value,MRS.ZoneName,MRS.BinName from Meterial_Issue_Return_Main MRM inner join Meterial_Issue_Return_Main_Sub MRS ";
                SSQL = SSQL + "on MRM.Mat_Issue_Return_No=MRS.Mat_Issue_Return_No where MRM.Mat_Issue_Return_No='" + MatIssRetNo + "'";


                if (WareHouse != "")
                {
                    SSQL = SSQL + "And MRM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And MRM.DeptName='" + DeptName + "'";
                }

                if (MatRetBy != "")
                {
                    SSQL = SSQL + " And MRM.Returnby='" + MatRetBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MRS.ItemName='" + ItemName + "'";
                }
                if (CostCenter != "-Select-")
                {
                    SSQL = SSQL + " And MS.CostCenterName='" + CostCenter + "'";
                }

                SSQL = SSQL + " And MRM.Ccode='" + SessionCcode + "' And MRM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MRM.FinYearCode='" + SessionFinYearCode + "' And MRS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MRS.Lcode='" + SessionLcode + "' And MRS.FinYearCode='" + SessionFinYearCode + "' And MRM.Mat_Issue_Return_Status='1'";

                MatIssRetDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (WareHouse != "")
            {
                SSQL = "select MRM.Mat_Issue_Return_No,MRM.Mat_Issue_Return_Date,MRM.WarehouseName,MRM.DeptName,MRM.Mat_Issue_No,";
                SSQL = SSQL + "MRM.Mat_Issue_Date,MRM.CostCenterName,MRM.Returnby,MRM.Receivedby,MRM.Remarks,MRS.ItemCode,MRS.ItemName,MRS.UOMCode,";
                SSQL = SSQL + "MRS.ReturnQty,MRS.Value,MRS.ZoneName,MRS.BinName from Meterial_Issue_Return_Main MRM inner join Meterial_Issue_Return_Main_Sub MRS ";
                SSQL = SSQL + "on MRM.Mat_Issue_Return_No=MRS.Mat_Issue_Return_No where MRM.WarehouseName='" + WareHouse + "'";


                if (MatIssRetNo != "")
                {
                    SSQL = SSQL + "And MRM.Mat_Issue_Return_No='" + MatIssRetNo + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And MRM.DeptName='" + DeptName + "'";
                }

                if (MatRetBy != "")
                {
                    SSQL = SSQL + " And MRM.Returnby='" + MatRetBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MRS.ItemName='" + ItemName + "'";
                }
                if (CostCenter != "-Select-")
                {
                    SSQL = SSQL + " And MS.CostCenterName='" + CostCenter + "'";
                }

                SSQL = SSQL + " And MRM.Ccode='" + SessionCcode + "' And MRM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MRM.FinYearCode='" + SessionFinYearCode + "' And MRS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MRS.Lcode='" + SessionLcode + "' And MRS.FinYearCode='" + SessionFinYearCode + "' And MRM.Mat_Issue_Return_Status='1'";

                MatIssRetDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (DeptName != "")
            {
                SSQL = "select MRM.Mat_Issue_Return_No,MRM.Mat_Issue_Return_Date,MRM.WarehouseName,MRM.DeptName,MRM.Mat_Issue_No,";
                SSQL = SSQL + "MRM.Mat_Issue_Date,MRM.CostCenterName,MRM.Returnby,MRM.Receivedby,MRM.Remarks,MRS.ItemCode,MRS.ItemName,MRS.UOMCode,";
                SSQL = SSQL + "MRS.ReturnQty,MRS.Value,MRS.ZoneName,MRS.BinName from Meterial_Issue_Return_Main MRM inner join Meterial_Issue_Return_Main_Sub MRS ";
                SSQL = SSQL + "on MRM.Mat_Issue_Return_No=MRS.Mat_Issue_Return_No where MRM.DeptName='" + DeptName + "'";


                if (MatIssRetNo != "")
                {
                    SSQL = SSQL + "And MRM.Mat_Issue_Return_No='" + MatIssRetNo + "'";
                }

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And MRM.WarehouseName='" + WareHouse + "'";
                }

                if (MatRetBy != "")
                {
                    SSQL = SSQL + " And MRM.Returnby='" + MatRetBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MRS.ItemName='" + ItemName + "'";
                }

                if (CostCenter != "-Select-")
                {
                    SSQL = SSQL + " And MS.CostCenterName='" + CostCenter + "'";
                }
                SSQL = SSQL + " And MRM.Ccode='" + SessionCcode + "' And MRM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MRM.FinYearCode='" + SessionFinYearCode + "' And MRS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MRS.Lcode='" + SessionLcode + "' And MRS.FinYearCode='" + SessionFinYearCode + "' And MRM.Mat_Issue_Return_Status='1'";

                MatIssRetDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (MatRetBy != "")
            {
                SSQL = "select MRM.Mat_Issue_Return_No,MRM.Mat_Issue_Return_Date,MRM.WarehouseName,MRM.DeptName,MRM.Mat_Issue_No,";
                SSQL = SSQL + "MRM.Mat_Issue_Date,MRM.CostCenterName,MRM.Returnby,MRM.Receivedby,MRM.Remarks,MRS.ItemCode,MRS.ItemName,MRS.UOMCode,";
                SSQL = SSQL + "MRS.ReturnQty,MRS.Value,MRS.ZoneName,MRS.BinName from Meterial_Issue_Return_Main MRM inner join Meterial_Issue_Return_Main_Sub MRS ";
                SSQL = SSQL + "on MRM.Mat_Issue_Return_No=MRS.Mat_Issue_Return_No where MRM.Returnby='" + MatRetBy + "'";


                if (MatIssRetNo != "")
                {
                    SSQL = SSQL + "And MRM.Mat_Issue_Return_No='" + MatIssRetNo + "'";
                }

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And MRM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And MRM.DeptName='" + DeptName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MRS.ItemName='" + ItemName + "'";
                }
                if (CostCenter != "-Select-")
                {
                    SSQL = SSQL + " And MS.CostCenterName='" + CostCenter + "'";
                }

                SSQL = SSQL + " And MRM.Ccode='" + SessionCcode + "' And MRM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MRM.FinYearCode='" + SessionFinYearCode + "' And MRS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MRS.Lcode='" + SessionLcode + "' And MRS.FinYearCode='" + SessionFinYearCode + "' And MRM.Mat_Issue_Return_Status='1'";

                MatIssRetDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (ItemName != "")
            {
                SSQL = "select MRM.Mat_Issue_Return_No,MRM.Mat_Issue_Return_Date,MRM.WarehouseName,MRM.DeptName,MRM.Mat_Issue_No,";
                SSQL = SSQL + "MRM.Mat_Issue_Date,MRM.CostCenterName,MRM.Returnby,MRM.Receivedby,MRM.Remarks,MRS.ItemCode,MRS.ItemName,MRS.UOMCode,";
                SSQL = SSQL + "MRS.ReturnQty,MRS.Value,MRS.ZoneName,MRS.BinName from Meterial_Issue_Return_Main MRM inner join Meterial_Issue_Return_Main_Sub MRS ";
                SSQL = SSQL + "on MRM.Mat_Issue_Return_No=MRS.Mat_Issue_Return_No where MRS.ItemName='" + ItemName + "'";


                if (MatIssRetNo != "")
                {
                    SSQL = SSQL + "And MRM.Mat_Issue_Return_No='" + MatIssRetNo + "'";
                }

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And MRM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And MRM.DeptName='" + DeptName + "'";
                }

                if (MatRetBy != "")
                {
                    SSQL = SSQL + " And MRM.Returnby='" + MatRetBy + "'";
                }
                if (CostCenter != "-Select-")
                {
                    SSQL = SSQL + " And MS.CostCenterName='" + CostCenter + "'";
                }
                SSQL = SSQL + " And MRM.Ccode='" + SessionCcode + "' And MRM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MRM.FinYearCode='" + SessionFinYearCode + "' And MRS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MRS.Lcode='" + SessionLcode + "' And MRS.FinYearCode='" + SessionFinYearCode + "' And MRM.Mat_Issue_Return_Status='1'";

                MatIssRetDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (MatIssRetDT.Rows.Count > 0)
            {


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTMatRet.Columns.Add("CompanyName");
                AutoDTMatRet.Columns.Add("LocationName");
                AutoDTMatRet.Columns.Add("CompanyCode");
                AutoDTMatRet.Columns.Add("LocationCode");
                AutoDTMatRet.Columns.Add("Address1");
                AutoDTMatRet.Columns.Add("Address2");
                AutoDTMatRet.Columns.Add("MatIssueReturnNo");
                AutoDTMatRet.Columns.Add("MatIssueReturnDate");
                AutoDTMatRet.Columns.Add("WarehouseName");
                AutoDTMatRet.Columns.Add("DeptName");
                AutoDTMatRet.Columns.Add("MatIssueNo");
                AutoDTMatRet.Columns.Add("MatIssueDate");
                AutoDTMatRet.Columns.Add("CostCenterName");
                AutoDTMatRet.Columns.Add("Returnby");
                AutoDTMatRet.Columns.Add("Receivedby");
                AutoDTMatRet.Columns.Add("Remarks");


                AutoDTMatRet.Columns.Add("ItemCode");
                AutoDTMatRet.Columns.Add("ItemName");
                AutoDTMatRet.Columns.Add("UomCode");
                AutoDTMatRet.Columns.Add("ReturnQty");
                AutoDTMatRet.Columns.Add("Value");
                AutoDTMatRet.Columns.Add("ZoneName");
                AutoDTMatRet.Columns.Add("BinName");


                for (int i = 0; i < MatIssRetDT.Rows.Count; i++)
                {
                    AutoDTMatRet.NewRow();
                    AutoDTMatRet.Rows.Add();

                    AutoDTMatRet.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTMatRet.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTMatRet.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTMatRet.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTMatRet.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTMatRet.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                    AutoDTMatRet.Rows[i]["MatIssueReturnNo"] = MatIssRetDT.Rows[i]["Mat_Issue_Return_No"];
                    AutoDTMatRet.Rows[i]["MatIssueReturnDate"] = MatIssRetDT.Rows[i]["Mat_Issue_Return_Date"];
                    AutoDTMatRet.Rows[i]["WarehouseName"] = MatIssRetDT.Rows[i]["WarehouseName"];
                    AutoDTMatRet.Rows[i]["DeptName"] = MatIssRetDT.Rows[i]["DeptName"];
                    AutoDTMatRet.Rows[i]["MatIssueNo"] = MatIssRetDT.Rows[i]["Mat_Issue_No"];
                    AutoDTMatRet.Rows[i]["MatIssueDate"] = MatIssRetDT.Rows[i]["Mat_Issue_Date"];
                    AutoDTMatRet.Rows[i]["CostCenterName"] = MatIssRetDT.Rows[i]["CostCenterName"];
                    AutoDTMatRet.Rows[i]["Returnby"] = MatIssRetDT.Rows[i]["Returnby"];
                    AutoDTMatRet.Rows[i]["Receivedby"] = MatIssRetDT.Rows[i]["Receivedby"];
                    AutoDTMatRet.Rows[i]["Remarks"] = MatIssRetDT.Rows[i]["Remarks"];


                    AutoDTMatRet.Rows[i]["ItemCode"] = MatIssRetDT.Rows[i]["ItemCode"];
                    AutoDTMatRet.Rows[i]["ItemName"] = MatIssRetDT.Rows[i]["ItemName"];
                    AutoDTMatRet.Rows[i]["UomCode"] = MatIssRetDT.Rows[i]["UOMCode"];
                    AutoDTMatRet.Rows[i]["ReturnQty"] = MatIssRetDT.Rows[i]["ReturnQty"];
                    AutoDTMatRet.Rows[i]["Value"] = MatIssRetDT.Rows[i]["Value"];
                    AutoDTMatRet.Rows[i]["ZoneName"] = MatIssRetDT.Rows[i]["ZoneName"];
                    AutoDTMatRet.Rows[i]["BinName"] = MatIssRetDT.Rows[i]["BinName"];



                }

                if (ButtonName == "Issue Return Slip")
                {
                    ds.Tables.Add(AutoDTMatRet);
                    ReportDocument report = new ReportDocument();
                    report.Load(Server.MapPath("~/crystal/MaterialIssueReturnSlip.rpt"));
                    report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                    CrystalReportViewer1.ReportSource = report;
                }

                if (ButtonName == "Issue Return Details")
                {
                    ds.Tables.Add(AutoDTMatRet);
                    ReportDocument report = new ReportDocument();
                    report.Load(Server.MapPath("~/crystal/MaterialIssueReturnDetails.rpt"));
                    report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                    CrystalReportViewer1.ReportSource = report;
                }
            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }


    }
    //Material Issue Return Code End

    //Material Request Code Start
    public void Material_Request_Report()
    {

        if (RptName == "Material Request Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            if (FromDate != "" && ToDate != "" && MatReqNo == "")
            {

                SSQL = "select MRM.Mat_Req_No,MRM.Mat_Req_Date,MRM.WarehouseName,MRM.DeptName,";
                SSQL = SSQL + "MRM.CostCenterName,MRM.Preparedby,MRM.Others,MRS.ItemCode,MRS.ItemName,MRS.UOMCode,";
                SSQL = SSQL + "MRS.ReuiredQty,MRS.ReuiredDate from Meterial_Request_Main MRM inner join Meterial_Request_Main_Sub MRS ";
                SSQL = SSQL + "on MRM.Mat_Req_No=MRS.Mat_Req_No where CONVERT(DATETIME,MRM.Mat_Req_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,MRS.Mat_Req_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (WareHouse != "")
                {
                    SSQL = SSQL + "And MRM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And MRM.DeptName='" + DeptName + "'";
                }

                if (MatPreBy != "")
                {
                    SSQL = SSQL + " And MRM.Preparedby='" + MatPreBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MRS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And MRM.Ccode='" + SessionCcode + "' And MRM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MRM.FinYearCode='" + SessionFinYearCode + "' And MRS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MRS.Lcode='" + SessionLcode + "' And MRS.FinYearCode='" + SessionFinYearCode + "' And MRM.Mat_Req_Status='1'";

                MatReqDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FromDate != "" && ToDate != "" && MatReqNo != "")
            {

                SSQL = "select MRM.Mat_Req_No,MRM.Mat_Req_Date,MRM.WarehouseName,MRM.DeptName,";
                SSQL = SSQL + "MRM.CostCenterName,MRM.Preparedby,MRM.Others,MRS.ItemCode,MRS.ItemName,MRS.UOMCode,";
                SSQL = SSQL + "MRS.ReuiredQty,MRS.ReuiredDate from Meterial_Request_Main MRM inner join Meterial_Request_Main_Sub MRS ";
                SSQL = SSQL + "on MRM.Mat_Req_No=MRS.Mat_Req_No where MRM.Mat_Req_No='" + MatReqNo + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,MRM.Mat_Req_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,MRS.Mat_Req_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (WareHouse != "")
                {
                    SSQL = SSQL + "And MRM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And MRM.DeptName='" + DeptName + "'";
                }

                if (MatPreBy != "")
                {
                    SSQL = SSQL + " And MRM.Preparedby='" + MatPreBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MRS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And MRM.Ccode='" + SessionCcode + "' And MRM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MRM.FinYearCode='" + SessionFinYearCode + "' And MRS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MRS.Lcode='" + SessionLcode + "' And MRS.FinYearCode='" + SessionFinYearCode + "' And MRM.Mat_Req_Status='1'";

                MatReqDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (MatReqNo != "")
            {

                SSQL = "select MRM.Mat_Req_No,MRM.Mat_Req_Date,MRM.WarehouseName,MRM.DeptName,";
                SSQL = SSQL + "MRM.CostCenterName,MRM.Preparedby,MRM.Others,MRS.ItemCode,MRS.ItemName,MRS.UOMCode,";
                SSQL = SSQL + "MRS.ReuiredQty,MRS.ReuiredDate from Meterial_Request_Main MRM inner join Meterial_Request_Main_Sub MRS ";
                SSQL = SSQL + "on MRM.Mat_Req_No=MRS.Mat_Req_No where MRM.Mat_Req_No='" + MatReqNo + "'";

                if (WareHouse != "")
                {
                    SSQL = SSQL + "And MRM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And MRM.DeptName='" + DeptName + "'";
                }

                if (MatPreBy != "")
                {
                    SSQL = SSQL + " And MRM.Preparedby='" + MatPreBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MRS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And MRM.Ccode='" + SessionCcode + "' And MRM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MRM.FinYearCode='" + SessionFinYearCode + "' And MRS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MRS.Lcode='" + SessionLcode + "' And MRS.FinYearCode='" + SessionFinYearCode + "' And MRM.Mat_Req_Status='1'";

                MatReqDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (WareHouse != "")
            {

                SSQL = "select MRM.Mat_Req_No,MRM.Mat_Req_Date,MRM.WarehouseName,MRM.DeptName,";
                SSQL = SSQL + "MRM.CostCenterName,MRM.Preparedby,MRM.Others,MRS.ItemCode,MRS.ItemName,MRS.UOMCode,";
                SSQL = SSQL + "MRS.ReuiredQty,MRS.ReuiredDate from Meterial_Request_Main MRM inner join Meterial_Request_Main_Sub MRS ";
                SSQL = SSQL + "on MRM.Mat_Req_No=MRS.Mat_Req_No where MRM.WarehouseName='" + WareHouse + "'";

                if (MatReqNo != "")
                {
                    SSQL = SSQL + "And MRM.Mat_Req_No='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And MRM.DeptName='" + DeptName + "'";
                }

                if (MatPreBy != "")
                {
                    SSQL = SSQL + " And MRM.Preparedby='" + MatPreBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MRS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And MRM.Ccode='" + SessionCcode + "' And MRM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MRM.FinYearCode='" + SessionFinYearCode + "' And MRS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MRS.Lcode='" + SessionLcode + "' And MRS.FinYearCode='" + SessionFinYearCode + "' And MRM.Mat_Req_Status='1'";

                MatReqDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (DeptName != "")
            {

                SSQL = "select MRM.Mat_Req_No,MRM.Mat_Req_Date,MRM.WarehouseName,MRM.DeptName,";
                SSQL = SSQL + "MRM.CostCenterName,MRM.Preparedby,MRM.Others,MRS.ItemCode,MRS.ItemName,MRS.UOMCode,";
                SSQL = SSQL + "MRS.ReuiredQty,MRS.ReuiredDate from Meterial_Request_Main MRM inner join Meterial_Request_Main_Sub MRS ";
                SSQL = SSQL + "on MRM.Mat_Req_No=MRS.Mat_Req_No where MRM.DeptName='" + DeptName + "'";

                if (MatReqNo != "")
                {
                    SSQL = SSQL + "And MRM.Mat_Req_No='" + WareHouse + "'";
                }

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And MRM.WarehouseName='" + WareHouse + "'";
                }

                if (MatPreBy != "")
                {
                    SSQL = SSQL + " And MRM.Preparedby='" + MatPreBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MRS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And MRM.Ccode='" + SessionCcode + "' And MRM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MRM.FinYearCode='" + SessionFinYearCode + "' And MRS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MRS.Lcode='" + SessionLcode + "' And MRS.FinYearCode='" + SessionFinYearCode + "' And MRM.Mat_Req_Status='1'";

                MatReqDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (MatPreBy != "")
            {

                SSQL = "select MRM.Mat_Req_No,MRM.Mat_Req_Date,MRM.WarehouseName,MRM.DeptName,";
                SSQL = SSQL + "MRM.CostCenterName,MRM.Preparedby,MRM.Others,MRS.ItemCode,MRS.ItemName,MRS.UOMCode,";
                SSQL = SSQL + "MRS.ReuiredQty,MRS.ReuiredDate from Meterial_Request_Main MRM inner join Meterial_Request_Main_Sub MRS ";
                SSQL = SSQL + "on MRM.Mat_Req_No=MRS.Mat_Req_No where MRM.Preparedby='" + MatPreBy + "'";

                if (MatReqNo != "")
                {
                    SSQL = SSQL + "And MRM.Mat_Req_No='" + WareHouse + "'";
                }

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And MRM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And MRM.DeptName='" + DeptName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MRS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And MRM.Ccode='" + SessionCcode + "' And MRM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MRM.FinYearCode='" + SessionFinYearCode + "' And MRS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MRS.Lcode='" + SessionLcode + "' And MRS.FinYearCode='" + SessionFinYearCode + "' And MRM.Mat_Req_Status='1'";

                MatReqDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }

            else if (ItemName != "")
            {

                SSQL = "select MRM.Mat_Req_No,MRM.Mat_Req_Date,MRM.WarehouseName,MRM.DeptName,";
                SSQL = SSQL + "MRM.CostCenterName,MRM.Preparedby,MRM.Others,MRS.ItemCode,MRS.ItemName,MRS.UOMCode,";
                SSQL = SSQL + "MRS.ReuiredQty,MRS.ReuiredDate from Meterial_Request_Main MRM inner join Meterial_Request_Main_Sub MRS ";
                SSQL = SSQL + "on MRM.Mat_Req_No=MRS.Mat_Req_No where MRS.ItemName='" + ItemName + "'";

                if (MatReqNo != "")
                {
                    SSQL = SSQL + "And MRM.Mat_Req_No='" + WareHouse + "'";
                }

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And MRM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And MRM.DeptName='" + DeptName + "'";
                }

                if (MatPreBy != "")
                {
                    SSQL = SSQL + " And MRM.Preparedby='" + MatPreBy + "'";
                }


                SSQL = SSQL + " And MRM.Ccode='" + SessionCcode + "' And MRM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MRM.FinYearCode='" + SessionFinYearCode + "' And MRS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MRS.Lcode='" + SessionLcode + "' And MRS.FinYearCode='" + SessionFinYearCode + "' And MRM.Mat_Req_Status='1'";

                MatReqDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (MatReqDT.Rows.Count > 0)
            {


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTMatReq.Columns.Add("CompanyName");
                AutoDTMatReq.Columns.Add("LocationName");
                AutoDTMatReq.Columns.Add("CompanyCode");
                AutoDTMatReq.Columns.Add("LocationCode");
                AutoDTMatReq.Columns.Add("Address1");
                AutoDTMatReq.Columns.Add("Address2");
                AutoDTMatReq.Columns.Add("MatReqNo");
                AutoDTMatReq.Columns.Add("MatReqDate");
                AutoDTMatReq.Columns.Add("WarehouseName");
                AutoDTMatReq.Columns.Add("DeptName");
                AutoDTMatReq.Columns.Add("CostCenterName");
                AutoDTMatReq.Columns.Add("Preparedby");
                AutoDTMatReq.Columns.Add("Others");


                AutoDTMatReq.Columns.Add("ItemCode");
                AutoDTMatReq.Columns.Add("ItemName");
                AutoDTMatReq.Columns.Add("UomCode");
                AutoDTMatReq.Columns.Add("RequiredQty");
                AutoDTMatReq.Columns.Add("RequiredDate");



                for (int i = 0; i < MatReqDT.Rows.Count; i++)
                {
                    AutoDTMatReq.NewRow();
                    AutoDTMatReq.Rows.Add();

                    AutoDTMatReq.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTMatReq.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTMatReq.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTMatReq.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTMatReq.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTMatReq.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                    AutoDTMatReq.Rows[i]["MatReqNo"] = MatReqDT.Rows[i]["Mat_Req_No"];
                    AutoDTMatReq.Rows[i]["MatReqDate"] = MatReqDT.Rows[i]["Mat_Req_Date"];
                    AutoDTMatReq.Rows[i]["WarehouseName"] = MatReqDT.Rows[i]["WarehouseName"];
                    AutoDTMatReq.Rows[i]["DeptName"] = MatReqDT.Rows[i]["DeptName"];
                    AutoDTMatReq.Rows[i]["CostCenterName"] = MatReqDT.Rows[i]["CostCenterName"];
                    AutoDTMatReq.Rows[i]["Preparedby"] = MatReqDT.Rows[i]["Preparedby"];
                    AutoDTMatReq.Rows[i]["Others"] = MatReqDT.Rows[i]["Others"];


                    AutoDTMatReq.Rows[i]["ItemCode"] = MatReqDT.Rows[i]["ItemCode"];
                    AutoDTMatReq.Rows[i]["ItemName"] = MatReqDT.Rows[i]["ItemName"];
                    AutoDTMatReq.Rows[i]["UomCode"] = MatReqDT.Rows[i]["UOMCode"];
                    AutoDTMatReq.Rows[i]["RequiredQty"] = MatReqDT.Rows[i]["ReuiredQty"];
                    AutoDTMatReq.Rows[i]["RequiredDate"] = MatReqDT.Rows[i]["ReuiredDate"];




                }

                if (ButtonName == "Request Slip")
                {
                    ds.Tables.Add(AutoDTMatReq);
                    ReportDocument report = new ReportDocument();
                    report.Load(Server.MapPath("~/crystal/MaterialRequestSlipReport.rpt"));
                    report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                    CrystalReportViewer1.ReportSource = report;
                }

                if (ButtonName == "Request Details")
                {
                    //ds.Tables.Add(AutoDTMatReq);
                    //ReportDocument report = new ReportDocument();
                    //report.Load(Server.MapPath("~/crystal/MaterialRequestDetailsReport.rpt"));
                    //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                    //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                    //CrystalReportViewer1.ReportSource = report;


                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(AutoDTMatReq);
                    RD_Report.Load(Server.MapPath("~/crystal/MaterialRequestDetailsReport.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);

                    //Company Details Add
                    //DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }
                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();

                }
            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }

    }


    //GatePass OUT Pending Details
    public void GatePass_OUT_Pending_Details_Report()
    {
        if (RptName == "GatePass OUT Pending Details Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            if (FromDate != "" && ToDate != "" && GPOutNo == "-Select-")
            {

                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,GOM.StockType,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.Remarks[ZoneName],GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No=GOS.GP_Out_No where CONVERT(DATETIME,GOM.GP_Out_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GOM.GP_Out_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                if (PartyName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Supp_Name='" + PartyName + "'";
                }

                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (GPOutIssBy != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GOM.OpStatus='0' And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FromDate != "" && ToDate != "" && GPOutNo != "-Select-")
            {


                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,GOM.StockType,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No=GOS.GP_Out_No where GOM.GP_Out_No='" + GPOutNo + "' And ";
                SSQL = SSQL + "CONVERT(DATETIME,GOM.GP_Out_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GOM.GP_Out_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (PartyName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Supp_Name='" + PartyName + "'";
                }


                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (GPOutIssBy != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (GPOutNo != "-Select-")
            {
                //SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                //SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,GOM.StockType,";
                //SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                //SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                //SSQL = SSQL + "on GOM.GP_Out_No = GOS.GP_Out_No where GOM.GP_Out_No='" + GPOutNo + "'";

                SSQL = "select CMP.Cname [CompanyName],CMP.Location[LocationName],cmp.Ccode[CompanyCode],CMP.LCode [LocationCode],CMP.Address1[CmpAddr],";
                SSQL = SSQL + "CMP.Address2+'-'+CMP.Pincode [CmpDst],CMP.Phone[CmpMob],CMP.GSTNO [CmpGST],CMP.ImagePath[ImagePath],SUP.SuppName [SupName],SUP.Address1[SupAddr1],";
                SSQL = SSQL + "SUP.Address2[SuppAdd2],SUP.Pincode[SupPin],SUP.State[SupState],SUP.City[SuppCity],SUP.MobileNo [SupMob],SUP.CstNo[SupGst],GOM.GP_Out_No[GPOutNo],";
                SSQL = SSQL + "GOM.GP_Out_Date[GPOutDate],GOM.GP_Type[GPType],GOM.DeliveryDate[DeliveryDate],GOM.Takenby[Takenby],GOM.Issuedby[Issuedby],";
                SSQL = SSQL + "GOM.Preferedby[Preferedby],GOM.DeptName[DeptName],GOM.StockType[StockType],GOM.WarehouseName[WarehouseName],GOM.Others[Others],";
                SSQL = SSQL + "GOS.ItemCode[ItemCode],GOS.ItemName[ItemName],GOS.UOMCode[UomCode],GOS.OutQty[OutQty],GOS.Value[Value],";
                SSQL = SSQL + "GOS.ZoneName[ZoneName],GOS.BinName[BinName] from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS on GOM.GP_Out_No = GOS.GP_Out_No ";
                SSQL = SSQL + "inner join AdminRights CMP on CMP.Ccode=GOM.Ccode inner join MstSupplier SUP on SUP.SuppCode=GOM.Supp_Code and SUP.Ccode=GOM.Ccode ";
                SSQL = SSQL + " Where GOM.GP_Out_No='" + GPOutNo + "'";


                if (PartyName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Supp_Code='" + PartyName + "'";
                }


                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (GPOutIssBy != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }


            else if (WareHouse != "-Select-")
            {
                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,GOM.StockType,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No = GOS.GP_Out_No where GOM.WarehouseName='" + WareHouse + "'";

                if (GPOutNo != "-Select-")
                {
                    SSQL = SSQL + " And GOM.GP_Out_No='" + GPOutNo + "'";
                }

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (GPOutIssBy != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (DeptName != "-Select-")
            {
                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,GOM.StockType,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No = GOS.GP_Out_No where GOM.DeptName='" + DeptName + "'";

                if (GPOutNo != "-Select-")
                {
                    SSQL = SSQL + " And GOM.GP_Out_No='" + GPOutNo + "'";
                }

                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (GPOutIssBy != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }

                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (GPOutIssBy != "-Select-")
            {
                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,GOM.StockType,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No = GOS.GP_Out_No where GOM.Issuedby='" + GPOutIssBy + "'";

                if (GPOutNo != "-Select-")
                {
                    SSQL = SSQL + " And GOM.GP_Out_No='" + GPOutNo + "'";
                }

                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }
                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (ItemName != "-Select-")
            {
                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,GOM.StockType,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No = GOS.GP_Out_No where GOS.ItemName='" + ItemName + "'";

                if (GPOutNo != "-Select-")
                {
                    SSQL = SSQL + " And GOM.GP_Out_No='" + GPOutNo + "'";
                }

                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (GPOutIssBy != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }
                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (GPOutDT.Rows.Count > 0)
            {
                if (ButtonName == "GP OUT SLIP")
                {

                    DataTable DtTst = new DataTable();
                    DtTst = GPOutDT;
                    for (int i = DtTst.Rows.Count; i < 4; i++)
                    {
                        DtTst.NewRow();
                        DtTst.Rows.Add();

                        DtTst.Rows[i]["GPOutNo"] = DtTst.Rows[0]["GPOutNo"].ToString();
                        DtTst.Rows[i]["GPOutDate"] = DtTst.Rows[0]["GPOutDate"].ToString();
                        DtTst.Rows[i]["SupName"] = DtTst.Rows[0]["SupName"].ToString();
                        DtTst.Rows[i]["GPType"] = DtTst.Rows[0]["GPType"].ToString();
                        DtTst.Rows[i]["Others"] = DtTst.Rows[0]["Others"].ToString();
                    }


                    ds.Tables.Add(DtTst);
                    ReportDocument report = new ReportDocument();

                    //if (StockType == "1")
                    //{
                    //    report.Load(Server.MapPath("~/crystal/GatePassOutSlipNoStock_Two_Copy.rpt"));
                    //    //report.Load(Server.MapPath("~/crystal/GatePassOutSlip_Two_Copy.rpt"));
                    //}
                    //else
                    //{
                    //    report.Load(Server.MapPath("~/crystal/GatePassOutSlipNoStock_Two_Copy.rpt"));
                    //}


                    if (DtTst.Rows[0]["CompanyName"].ToString() == "Kumaran Gin & Pressing Private Limited")
                    {
                        report.Load(Server.MapPath("~/crystal/GatePassOutSlipNoStock_Two_Unit1.rpt"));
                    }
                    else
                    {
                        report.Load(Server.MapPath("~/crystal/GatePassOutSlipNoStock_Two_Unit2.rpt"));
                    }
                    report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = report;
                }
                else
                {
                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

                    AutoDTGPOut.Columns.Add("CompanyName");
                    AutoDTGPOut.Columns.Add("LocationName");
                    AutoDTGPOut.Columns.Add("CompanyCode");
                    AutoDTGPOut.Columns.Add("LocationCode");
                    AutoDTGPOut.Columns.Add("Address1");
                    AutoDTGPOut.Columns.Add("Address2");
                    AutoDTGPOut.Columns.Add("GPOutNo");
                    AutoDTGPOut.Columns.Add("GPOutDate");
                    AutoDTGPOut.Columns.Add("GPType");
                    AutoDTGPOut.Columns.Add("SuppName");
                    AutoDTGPOut.Columns.Add("SuppDet");
                    AutoDTGPOut.Columns.Add("DeliveryDate");
                    AutoDTGPOut.Columns.Add("Takenby");
                    AutoDTGPOut.Columns.Add("Issuedby");
                    AutoDTGPOut.Columns.Add("Preferedby");
                    AutoDTGPOut.Columns.Add("DeptName");
                    AutoDTGPOut.Columns.Add("WarehouseName");
                    AutoDTGPOut.Columns.Add("Others");
                    AutoDTGPOut.Columns.Add("ItemCode");
                    AutoDTGPOut.Columns.Add("ItemName");
                    AutoDTGPOut.Columns.Add("UomCode");
                    AutoDTGPOut.Columns.Add("OutQty");
                    AutoDTGPOut.Columns.Add("Value");
                    AutoDTGPOut.Columns.Add("ZoneName");
                    AutoDTGPOut.Columns.Add("BinName");


                    for (int i = 0; i < GPOutDT.Rows.Count; i++)
                    {
                        AutoDTGPOut.NewRow();
                        AutoDTGPOut.Rows.Add();

                        AutoDTGPOut.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                        AutoDTGPOut.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                        AutoDTGPOut.Rows[i]["CompanyCode"] = SessionCcode;
                        AutoDTGPOut.Rows[i]["LocationCode"] = SessionLcode;
                        AutoDTGPOut.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                        AutoDTGPOut.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                        AutoDTGPOut.Rows[i]["GPOutNo"] = GPOutDT.Rows[i]["GP_Out_No"];
                        AutoDTGPOut.Rows[i]["GPOutDate"] = GPOutDT.Rows[i]["GP_Out_Date"];
                        if (GPOutDT.Rows[i]["GP_Type"].ToString() == "1")
                        {
                            AutoDTGPOut.Rows[i]["GPType"] = "Returnable";
                        }
                        else
                        {
                            AutoDTGPOut.Rows[i]["GPType"] = "Non-Returnable";
                        }

                        AutoDTGPOut.Rows[i]["SuppName"] = GPOutDT.Rows[i]["Supp_Name"];
                        AutoDTGPOut.Rows[i]["SuppDet"] = GPOutDT.Rows[i]["Supp_Det"];
                        AutoDTGPOut.Rows[i]["DeliveryDate"] = GPOutDT.Rows[i]["DeliveryDate"];
                        AutoDTGPOut.Rows[i]["Takenby"] = GPOutDT.Rows[i]["Takenby"];
                        AutoDTGPOut.Rows[i]["Issuedby"] = GPOutDT.Rows[i]["Issuedby"];
                        AutoDTGPOut.Rows[i]["Preferedby"] = GPOutDT.Rows[i]["Preferedby"];
                        AutoDTGPOut.Rows[i]["DeptName"] = GPOutDT.Rows[i]["DeptName"];
                        AutoDTGPOut.Rows[i]["WarehouseName"] = GPOutDT.Rows[i]["WarehouseName"];
                        AutoDTGPOut.Rows[i]["Others"] = GPOutDT.Rows[i]["Others"];


                        AutoDTGPOut.Rows[i]["ItemCode"] = GPOutDT.Rows[i]["ItemCode"];
                        AutoDTGPOut.Rows[i]["ItemName"] = GPOutDT.Rows[i]["ItemName"];
                        AutoDTGPOut.Rows[i]["UomCode"] = GPOutDT.Rows[i]["UOMCode"];
                        AutoDTGPOut.Rows[i]["OutQty"] = GPOutDT.Rows[i]["OutQty"];
                        AutoDTGPOut.Rows[i]["Value"] = GPOutDT.Rows[i]["Value"];
                        AutoDTGPOut.Rows[i]["ZoneName"] = GPOutDT.Rows[i]["ZoneName"];
                        AutoDTGPOut.Rows[i]["BinName"] = GPOutDT.Rows[i]["BinName"];


                    }


                    ds.Tables.Add(AutoDTGPOut);
                    ReportDocument report = new ReportDocument();
                    report.Load(Server.MapPath("~/crystal/GatePassOutPendingDetailsReport.rpt"));
                    report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = report;
                }

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }

    }

    //Gate Pass Out Details Code start
    public void GatePass_OUT_Details_Report()
    {
        if (RptName == "GatePass OUT Details Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            if (FromDate != "" && ToDate != "" && GPOutNo == "-Select-")
            {

                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,GOM.StockType,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No=GOS.GP_Out_No where CONVERT(DATETIME,GOM.GP_Out_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GOM.GP_Out_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                if (PartyName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Supp_Name='" + PartyName + "'";
                }

                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (GPOutIssBy != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FromDate != "" && ToDate != "" && GPOutNo != "-Select-")
            {


                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,GOM.StockType,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No=GOS.GP_Out_No where GOM.GP_Out_No='" + GPOutNo + "' And ";
                SSQL = SSQL + "CONVERT(DATETIME,GOM.GP_Out_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GOM.GP_Out_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (PartyName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Supp_Name='" + PartyName + "'";
                }


                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (GPOutIssBy != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (GPOutNo != "-Select-")
            {
                //SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                //SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,GOM.StockType,";
                //SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                //SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                //SSQL = SSQL + "on GOM.GP_Out_No = GOS.GP_Out_No where GOM.GP_Out_No='" + GPOutNo + "'";

                SSQL = "select CMP.Cname [CompanyName],CMP.Location[LocationName],cmp.Ccode[CompanyCode],CMP.LCode [LocationCode],CMP.Address1[CmpAddr],";
                SSQL = SSQL + "CMP.Address2+'-'+CMP.Pincode [CmpDst],CMP.Phone[CmpMob],CMP.GSTNO [CmpGST],CMP.ImagePath[ImagePath],SUP.SuppName [SupName],SUP.Address1[SupAddr1],";
                SSQL = SSQL + "SUP.Address2[SuppAdd2],SUP.Pincode[SupPin],SUP.State[SupState],SUP.City[SuppCity],SUP.MobileNo [SupMob],SUP.CstNo[SupGst],GOM.GP_Out_No[GPOutNo],";
                SSQL = SSQL + "GOM.GP_Out_Date[GPOutDate],GOM.GP_Type[GPType],GOM.DeliveryDate[DeliveryDate],GOM.Takenby[Takenby],GOM.Issuedby[Issuedby],";
                SSQL = SSQL + "GOM.Preferedby[Preferedby],GOM.DeptName[DeptName],GOM.StockType[StockType],GOM.WarehouseName[WarehouseName],GOM.Others[Others],";
                SSQL = SSQL + "GOS.ItemCode[ItemCode],GOS.ItemName[ItemName],GOS.UOMCode[UomCode],GOS.OutQty[OutQty],GOS.Value[Value],";
                SSQL = SSQL + "GOS.ZoneName[ZoneName],GOS.BinName[BinName] from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS on GOM.GP_Out_No = GOS.GP_Out_No ";
                SSQL = SSQL + "inner join AdminRights CMP on CMP.Ccode=GOM.Ccode inner join MstSupplier SUP on SUP.SuppCode=GOM.Supp_Code and SUP.Ccode=GOM.Ccode ";
                SSQL = SSQL + " Where GOM.GP_Out_No='" + GPOutNo + "'";


                if (PartyName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Supp_Code='" + PartyName + "'";
                }


                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (GPOutIssBy != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }


            else if (WareHouse != "-Select-")
            {
                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,GOM.StockType,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No = GOS.GP_Out_No where GOM.WarehouseName='" + WareHouse + "'";

                if (GPOutNo != "-Select-")
                {
                    SSQL = SSQL + " And GOM.GP_Out_No='" + GPOutNo + "'";
                }

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (GPOutIssBy != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (DeptName != "-Select-")
            {
                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,GOM.StockType,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No = GOS.GP_Out_No where GOM.DeptName='" + DeptName + "'";

                if (GPOutNo != "-Select-")
                {
                    SSQL = SSQL + " And GOM.GP_Out_No='" + GPOutNo + "'";
                }

                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (GPOutIssBy != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (GPOutIssBy != "-Select-")
            {
                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,GOM.StockType,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No = GOS.GP_Out_No where GOM.Issuedby='" + GPOutIssBy + "'";

                if (GPOutNo != "-Select-")
                {
                    SSQL = SSQL + " And GOM.GP_Out_No='" + GPOutNo + "'";
                }

                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }
                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (ItemName != "-Select-")
            {
                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,GOM.StockType,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No = GOS.GP_Out_No where GOS.ItemName='" + ItemName + "'";

                if (GPOutNo != "-Select-")
                {
                    SSQL = SSQL + " And GOM.GP_Out_No='" + GPOutNo + "'";
                }

                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (GPOutIssBy != "-Select-")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }
                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (GPOutDT.Rows.Count > 0)
            {
                if (ButtonName == "GP OUT SLIP")
                {

                    DataTable DtTst = new DataTable();
                    DtTst = GPOutDT;
                    for (int i = DtTst.Rows.Count; i < 4; i++)
                    {
                        DtTst.NewRow();
                        DtTst.Rows.Add();

                        DtTst.Rows[i]["GPOutNo"] = DtTst.Rows[0]["GPOutNo"].ToString();
                        DtTst.Rows[i]["GPOutDate"] = DtTst.Rows[0]["GPOutDate"].ToString();
                        DtTst.Rows[i]["SupName"] = DtTst.Rows[0]["SupName"].ToString();
                        DtTst.Rows[i]["GPType"] = DtTst.Rows[0]["GPType"].ToString();
                        DtTst.Rows[i]["Others"] = DtTst.Rows[0]["Others"].ToString();
                    }


                    ds.Tables.Add(DtTst);
                    ReportDocument report = new ReportDocument();

                    //if (StockType == "1")
                    //{
                    //    report.Load(Server.MapPath("~/crystal/GatePassOutSlipNoStock_Two_Copy.rpt"));
                    //    //report.Load(Server.MapPath("~/crystal/GatePassOutSlip_Two_Copy.rpt"));
                    //}
                    //else
                    //{
                    //    report.Load(Server.MapPath("~/crystal/GatePassOutSlipNoStock_Two_Copy.rpt"));
                    //}


                    if (DtTst.Rows[0]["CompanyName"].ToString() == "Kumaran Gin & Pressing Private Limited")
                    {
                        report.Load(Server.MapPath("~/crystal/GatePassOutSlipNoStock_Two_Unit1.rpt"));
                    }
                    else
                    {
                        report.Load(Server.MapPath("~/crystal/GatePassOutSlipNoStock_Two_Unit2.rpt"));
                    }
                    report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = report;
                }
                else
                {
                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

                    AutoDTGPOut.Columns.Add("CompanyName");
                    AutoDTGPOut.Columns.Add("LocationName");
                    AutoDTGPOut.Columns.Add("CompanyCode");
                    AutoDTGPOut.Columns.Add("LocationCode");
                    AutoDTGPOut.Columns.Add("Address1");
                    AutoDTGPOut.Columns.Add("Address2");
                    AutoDTGPOut.Columns.Add("GPOutNo");
                    AutoDTGPOut.Columns.Add("GPOutDate");
                    AutoDTGPOut.Columns.Add("GPType");
                    AutoDTGPOut.Columns.Add("SuppName");
                    AutoDTGPOut.Columns.Add("SuppDet");
                    AutoDTGPOut.Columns.Add("DeliveryDate");
                    AutoDTGPOut.Columns.Add("Takenby");
                    AutoDTGPOut.Columns.Add("Issuedby");
                    AutoDTGPOut.Columns.Add("Preferedby");
                    AutoDTGPOut.Columns.Add("DeptName");
                    AutoDTGPOut.Columns.Add("WarehouseName");
                    AutoDTGPOut.Columns.Add("Others");
                    AutoDTGPOut.Columns.Add("ItemCode");
                    AutoDTGPOut.Columns.Add("ItemName");
                    AutoDTGPOut.Columns.Add("UomCode");
                    AutoDTGPOut.Columns.Add("OutQty");
                    AutoDTGPOut.Columns.Add("Value");
                    AutoDTGPOut.Columns.Add("ZoneName");
                    AutoDTGPOut.Columns.Add("BinName");


                    for (int i = 0; i < GPOutDT.Rows.Count; i++)
                    {
                        AutoDTGPOut.NewRow();
                        AutoDTGPOut.Rows.Add();

                        AutoDTGPOut.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                        AutoDTGPOut.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                        AutoDTGPOut.Rows[i]["CompanyCode"] = SessionCcode;
                        AutoDTGPOut.Rows[i]["LocationCode"] = SessionLcode;
                        AutoDTGPOut.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                        AutoDTGPOut.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                        AutoDTGPOut.Rows[i]["GPOutNo"] = GPOutDT.Rows[i]["GP_Out_No"];
                        AutoDTGPOut.Rows[i]["GPOutDate"] = GPOutDT.Rows[i]["GP_Out_Date"];
                        if (GPOutDT.Rows[i]["GP_Type"].ToString() == "1")
                        {
                            AutoDTGPOut.Rows[i]["GPType"] = "Returnable";
                        }
                        else
                        {
                            AutoDTGPOut.Rows[i]["GPType"] = "Non-Returnable";
                        }

                        AutoDTGPOut.Rows[i]["SuppName"] = GPOutDT.Rows[i]["Supp_Name"];
                        AutoDTGPOut.Rows[i]["SuppDet"] = GPOutDT.Rows[i]["Supp_Det"];
                        AutoDTGPOut.Rows[i]["DeliveryDate"] = GPOutDT.Rows[i]["DeliveryDate"];
                        AutoDTGPOut.Rows[i]["Takenby"] = GPOutDT.Rows[i]["Takenby"];
                        AutoDTGPOut.Rows[i]["Issuedby"] = GPOutDT.Rows[i]["Issuedby"];
                        AutoDTGPOut.Rows[i]["Preferedby"] = GPOutDT.Rows[i]["Preferedby"];
                        AutoDTGPOut.Rows[i]["DeptName"] = GPOutDT.Rows[i]["DeptName"];
                        AutoDTGPOut.Rows[i]["WarehouseName"] = GPOutDT.Rows[i]["WarehouseName"];
                        AutoDTGPOut.Rows[i]["Others"] = GPOutDT.Rows[i]["Others"];


                        AutoDTGPOut.Rows[i]["ItemCode"] = GPOutDT.Rows[i]["ItemCode"];
                        AutoDTGPOut.Rows[i]["ItemName"] = GPOutDT.Rows[i]["ItemName"];
                        AutoDTGPOut.Rows[i]["UomCode"] = GPOutDT.Rows[i]["UOMCode"];
                        AutoDTGPOut.Rows[i]["OutQty"] = GPOutDT.Rows[i]["OutQty"];
                        AutoDTGPOut.Rows[i]["Value"] = GPOutDT.Rows[i]["Value"];
                        AutoDTGPOut.Rows[i]["ZoneName"] = GPOutDT.Rows[i]["ZoneName"];
                        AutoDTGPOut.Rows[i]["BinName"] = GPOutDT.Rows[i]["BinName"];


                    }


                    ds.Tables.Add(AutoDTGPOut);
                    ReportDocument report = new ReportDocument();
                    report.Load(Server.MapPath("~/crystal/GatePassOutDetailsReport.rpt"));
                    report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                    CrystalReportViewer1.ReportSource = report;
                }

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }

    }
    //Gate Pass Out Details Code End

    //Gate Pass Non-Returnable Code start
    public void GatePass_NonReturnable_Details_Report()
    {
        if (RptName == "GatePass NonReturnable Details Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            if (FromDate != "" && ToDate != "" && GPOutNo == "")
            {

                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No=GOS.GP_Out_No where GOM.GP_Type='2' And CONVERT(DATETIME,GOM.GP_Out_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GOM.GP_Out_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (WareHouse != "")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (GPOutIssBy != "")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "' And GOM.GP_Out_Status='1'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FromDate != "" && ToDate != "" && GPOutNo != "")
            {


                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No=GOS.GP_Out_No where GOM.GP_Type='2' And GOM.GP_Out_No='" + GPOutNo + "' And ";
                SSQL = SSQL + "CONVERT(DATETIME,GOM.GP_Out_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GOM.GP_Out_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (WareHouse != "")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (GPOutIssBy != "")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "' And GOM.GP_Out_Status='1'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (GPOutNo != "")
            {
                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No = GOS.GP_Out_No where GOM.GP_Type='2' And GOM.GP_Out_No='" + GPOutNo + "'";

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (GPOutIssBy != "")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "' And GOM.GP_Out_Status='1'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }


            else if (WareHouse != "")
            {
                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No = GOS.GP_Out_No where GOM.GP_Type='2' And GOM.WarehouseName='" + WareHouse + "'";

                if (GPOutNo != "")
                {
                    SSQL = SSQL + " And GOM.GP_Out_No='" + GPOutNo + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (GPOutIssBy != "")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "' And GOM.GP_Out_Status='1'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (DeptName != "")
            {
                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No = GOS.GP_Out_No where GOM.GP_Type='2' And GOM.DeptName='" + DeptName + "'";

                if (GPOutNo != "")
                {
                    SSQL = SSQL + " And GOM.GP_Out_No='" + GPOutNo + "'";
                }

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (GPOutIssBy != "")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "' And GOM.GP_Out_Status='1'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (GPOutIssBy != "")
            {
                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No = GOS.GP_Out_No where GOM.GP_Type='2' And GOM.Issuedby='" + GPOutIssBy + "'";

                if (GPOutNo != "")
                {
                    SSQL = SSQL + " And GOM.GP_Out_No='" + GPOutNo + "'";
                }

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And GOS.ItemName='" + ItemName + "'";
                }
                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "' And GOM.GP_Out_Status='1'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (ItemName != "")
            {
                SSQL = "select GOM.GP_Out_No,GOM.GP_Out_Date,GOM.GP_Type,GOM.Supp_Name,";
                SSQL = SSQL + "GOM.Supp_Det,GOM.DeliveryDate,GOM.Takenby,GOM.Issuedby,GOM.Preferedby,GOM.DeptName,";
                SSQL = SSQL + "GOM.WarehouseName,GOM.Others,GOS.ItemCode,GOS.ItemName,GOS.UOMCode,GOS.OutQty,GOS.Value,";
                SSQL = SSQL + "GOS.ZoneName,GOS.BinName from GatePass_Out_Main GOM inner join GatePass_Out_Main_Sub GOS ";
                SSQL = SSQL + "on GOM.GP_Out_No = GOS.GP_Out_No where GOM.GP_Type='2' And GOS.ItemName='" + ItemName + "'";

                if (GPOutNo != "")
                {
                    SSQL = SSQL + " And GOM.GP_Out_No='" + GPOutNo + "'";
                }

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And GOM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And GOM.DeptName='" + DeptName + "'";
                }

                if (GPOutIssBy != "")
                {
                    SSQL = SSQL + " And GOM.Issuedby='" + GPOutIssBy + "'";
                }
                SSQL = SSQL + " And GOM.Ccode='" + SessionCcode + "' And GOM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GOM.FinYearCode='" + SessionFinYearCode + "' And GOS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GOS.Lcode='" + SessionLcode + "' And GOS.FinYearCode='" + SessionFinYearCode + "' And GOM.GP_Out_Status='1'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (GPOutDT.Rows.Count > 0)
            {


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTGPOut.Columns.Add("CompanyName");
                AutoDTGPOut.Columns.Add("LocationName");
                AutoDTGPOut.Columns.Add("CompanyCode");
                AutoDTGPOut.Columns.Add("LocationCode");
                AutoDTGPOut.Columns.Add("Address1");
                AutoDTGPOut.Columns.Add("Address2");
                AutoDTGPOut.Columns.Add("GPOutNo");
                AutoDTGPOut.Columns.Add("GPOutDate");
                AutoDTGPOut.Columns.Add("GPType");
                AutoDTGPOut.Columns.Add("SuppName");
                AutoDTGPOut.Columns.Add("SuppDet");
                AutoDTGPOut.Columns.Add("DeliveryDate");
                AutoDTGPOut.Columns.Add("Takenby");
                AutoDTGPOut.Columns.Add("Issuedby");
                AutoDTGPOut.Columns.Add("Preferedby");
                AutoDTGPOut.Columns.Add("DeptName");
                AutoDTGPOut.Columns.Add("WarehouseName");
                AutoDTGPOut.Columns.Add("Others");

                AutoDTGPOut.Columns.Add("ItemCode");
                AutoDTGPOut.Columns.Add("ItemName");
                AutoDTGPOut.Columns.Add("UomCode");
                AutoDTGPOut.Columns.Add("OutQty");
                AutoDTGPOut.Columns.Add("Value");
                AutoDTGPOut.Columns.Add("ZoneName");
                AutoDTGPOut.Columns.Add("BinName");



                for (int i = 0; i < GPOutDT.Rows.Count; i++)
                {
                    AutoDTGPOut.NewRow();
                    AutoDTGPOut.Rows.Add();

                    AutoDTGPOut.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTGPOut.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTGPOut.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTGPOut.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTGPOut.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTGPOut.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDTGPOut.Rows[i]["GPOutNo"] = GPOutDT.Rows[i]["GP_Out_No"];
                    AutoDTGPOut.Rows[i]["GPOutDate"] = GPOutDT.Rows[i]["GP_Out_Date"];
                    if (GPOutDT.Rows[i]["GP_Type"].ToString() == "1")
                    {
                        AutoDTGPOut.Rows[i]["GPType"] = "Returnable";
                    }
                    else
                    {
                        AutoDTGPOut.Rows[i]["GPType"] = "Non-Returnable";
                    }

                    AutoDTGPOut.Rows[i]["SuppName"] = GPOutDT.Rows[i]["Supp_Name"];
                    AutoDTGPOut.Rows[i]["SuppDet"] = GPOutDT.Rows[i]["Supp_Det"];
                    AutoDTGPOut.Rows[i]["DeliveryDate"] = GPOutDT.Rows[i]["DeliveryDate"];
                    AutoDTGPOut.Rows[i]["Takenby"] = GPOutDT.Rows[i]["Takenby"];
                    AutoDTGPOut.Rows[i]["Issuedby"] = GPOutDT.Rows[i]["Issuedby"];
                    AutoDTGPOut.Rows[i]["Preferedby"] = GPOutDT.Rows[i]["Preferedby"];
                    AutoDTGPOut.Rows[i]["DeptName"] = GPOutDT.Rows[i]["DeptName"];
                    AutoDTGPOut.Rows[i]["WarehouseName"] = GPOutDT.Rows[i]["WarehouseName"];
                    AutoDTGPOut.Rows[i]["Others"] = GPOutDT.Rows[i]["Others"];


                    AutoDTGPOut.Rows[i]["ItemCode"] = GPOutDT.Rows[i]["ItemCode"];
                    AutoDTGPOut.Rows[i]["ItemName"] = GPOutDT.Rows[i]["ItemName"];
                    AutoDTGPOut.Rows[i]["UomCode"] = GPOutDT.Rows[i]["UOMCode"];
                    AutoDTGPOut.Rows[i]["OutQty"] = GPOutDT.Rows[i]["OutQty"];
                    AutoDTGPOut.Rows[i]["Value"] = GPOutDT.Rows[i]["Value"];
                    AutoDTGPOut.Rows[i]["ZoneName"] = GPOutDT.Rows[i]["ZoneName"];
                    AutoDTGPOut.Rows[i]["BinName"] = GPOutDT.Rows[i]["BinName"];


                }


                ds.Tables.Add(AutoDTGPOut);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("~/crystal/GatePassNonReturnableDetailsReport.rpt"));
                report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = report;

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }

    }

    //Gate Pass Non-Returnable Code End

    //Service Entry Details
    public void ServiceEntry_Details_Report()
    {

        if (RptName == "ServiceEntry Details Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            if (FromDate != "" && ToDate != "")
            {
                SSQL = "Select SEM.SE_NO[SENo],SEM.SE_Date[SEDate] ,SEM.InvNo[InvNo],SEM.InvDate[InvDate],SEM.StockType[StkType],";
                SSQL = SSQL + " SEM.Supp_Name[SupName],SEM.ServicerName[ServicerName],SEM.MobileNo[Mobile],SEM.Others[Others],";
                SSQL = SSQL + " SEM.DeptName[DeptName],SES.ItemName[ItemName],SES.UOM[UOM],";
                SSQL = SSQL + " SES.Qty[Qty], SES.Rate[Rate],SES.Value[BeforGst],SES.DiscAmt[DiscAmt],SES.CGSTPer[CGSTPer],";
                SSQL = SSQL + " SES.CGSTAmt[CGSTAmt],SES.SGSTPer[SGSTPer], SES.SGSTAmt[SGSTAmt],SES.IGSTPer[IGSTPer],";
                SSQL = SSQL + " SES.IGSTAmt[IGSTAmt],SES.TotAmt[TotAmt] From Service_Entry_Main SEM ";
                SSQL = SSQL + " Inner join Service_Entry_Main_Sub SES on SEM.SE_NO=SES.SE_NO ";
                SSQL = SSQL + " Where CONVERT(DATETIME,SEM.SE_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,SEM.SE_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                if (SupplierName != "-Select-")
                {
                    SSQL = SSQL + " And SEM.Supp_Name='" + SupplierName + "'";
                }

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And SEM.DeptName='" + DeptName + "'";
                }
                 
                SSQL = SSQL + " And SEM.Ccode='" + SessionCcode + "' And SEM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And SEM.FinYearCode='" + SessionFinYearCode + "' And SES.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And SES.Lcode='" + SessionLcode + "' And SES.FinYearCode='" + SessionFinYearCode + "'";

                GPOutDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (GPOutDT.Rows.Count > 0)
            {
                if (ButtonName == "GP OUT SLIP")
                {

                    
                }
                else
                {
                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' ";
                    SSQL = SSQL + " and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

                    AutoDTGPOut.Columns.Add("CompanyName");
                    AutoDTGPOut.Columns.Add("LocationName");
                    AutoDTGPOut.Columns.Add("CompanyCode");
                    AutoDTGPOut.Columns.Add("LocationCode");
                    AutoDTGPOut.Columns.Add("Address1");
                    AutoDTGPOut.Columns.Add("Address2");
                    AutoDTGPOut.Columns.Add("SENo");
                    AutoDTGPOut.Columns.Add("SEDate");
                    AutoDTGPOut.Columns.Add("InvNo");
                    AutoDTGPOut.Columns.Add("InvDate");
                    AutoDTGPOut.Columns.Add("StkType");
                    AutoDTGPOut.Columns.Add("SuppName");
                    AutoDTGPOut.Columns.Add("DeptName");
                    AutoDTGPOut.Columns.Add("Servicer");
                    AutoDTGPOut.Columns.Add("Mobile");
                    AutoDTGPOut.Columns.Add("Others");
                    AutoDTGPOut.Columns.Add("ItemName");
                    AutoDTGPOut.Columns.Add("Uom");
                    AutoDTGPOut.Columns.Add("Qty");
                    AutoDTGPOut.Columns.Add("Rate");
                    AutoDTGPOut.Columns.Add("Value");
                    AutoDTGPOut.Columns.Add("Disc");
                    AutoDTGPOut.Columns.Add("CGST");
                    AutoDTGPOut.Columns.Add("SGST");
                    AutoDTGPOut.Columns.Add("IGST");
                    AutoDTGPOut.Columns.Add("Total");


                    for (int i = 0; i < GPOutDT.Rows.Count; i++)
                    {
                        AutoDTGPOut.NewRow();
                        AutoDTGPOut.Rows.Add();

                        AutoDTGPOut.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                        AutoDTGPOut.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                        AutoDTGPOut.Rows[i]["CompanyCode"] = SessionCcode;
                        AutoDTGPOut.Rows[i]["LocationCode"] = SessionLcode;
                        AutoDTGPOut.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                        AutoDTGPOut.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                        AutoDTGPOut.Rows[i]["SENo"] = GPOutDT.Rows[i]["SENo"];
                        AutoDTGPOut.Rows[i]["SEDate"] = GPOutDT.Rows[i]["SEDate"];

                        AutoDTGPOut.Rows[i]["InvNo"] = GPOutDT.Rows[i]["InvNo"];
                        AutoDTGPOut.Rows[i]["InvDate"] = GPOutDT.Rows[i]["InvDate"];

                        if (GPOutDT.Rows[i]["StkType"].ToString() == "1")
                        {
                            AutoDTGPOut.Rows[i]["StkType"] = "Stock";
                        }
                        else
                        {
                            AutoDTGPOut.Rows[i]["StkType"] = "Non-Stock";
                        }

                        AutoDTGPOut.Rows[i]["SuppName"] = GPOutDT.Rows[i]["SupName"];
                        AutoDTGPOut.Rows[i]["Servicer"] = GPOutDT.Rows[i]["ServicerName"];
                        AutoDTGPOut.Rows[i]["Mobile"] = GPOutDT.Rows[i]["Mobile"];
                        AutoDTGPOut.Rows[i]["DeptName"] = GPOutDT.Rows[i]["DeptName"];
                        AutoDTGPOut.Rows[i]["Others"] = GPOutDT.Rows[i]["Others"];


                        AutoDTGPOut.Rows[i]["ItemName"] = GPOutDT.Rows[i]["ItemName"];
                        AutoDTGPOut.Rows[i]["Uom"] = GPOutDT.Rows[i]["UOM"];
                        AutoDTGPOut.Rows[i]["Qty"] = GPOutDT.Rows[i]["Qty"];
                        AutoDTGPOut.Rows[i]["Rate"] = GPOutDT.Rows[i]["Rate"];
                        AutoDTGPOut.Rows[i]["Value"] = GPOutDT.Rows[i]["BeforGst"];
                        AutoDTGPOut.Rows[i]["Disc"] = GPOutDT.Rows[i]["DiscAmt"];
                        AutoDTGPOut.Rows[i]["CGST"] = GPOutDT.Rows[i]["CGSTAmt"];
                        AutoDTGPOut.Rows[i]["SGST"] = GPOutDT.Rows[i]["SGSTAmt"];
                        AutoDTGPOut.Rows[i]["IGST"] = GPOutDT.Rows[i]["IGSTAmt"];
                        AutoDTGPOut.Rows[i]["Total"] = GPOutDT.Rows[i]["TotAmt"];
                    }

                    ds.Tables.Add(AutoDTGPOut);
                    ReportDocument report = new ReportDocument();
                    report.Load(Server.MapPath("~/crystal/Service_Entry_DetailsReport.rpt"));
                    report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = report;
                }

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }

    
    //Service Entry Details End

    //Gate Pass IN Details Code Start
    public void GatePass_IN_Details_Report()
    {

        if (RptName == "GatePass IN Details Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            if (FromDate != "" && ToDate != "" && GPINNo == "-Select-" && GPOutNo == "-Select-")
            {


                SSQL = "select GIM.GP_IN_No[GPInNo],GIM.GP_IN_Date[GPInDate] ,GIM.GP_Out_No[GPOutNo],GIM.GP_Out_Date[GPOutDATE],GIM.StockType[StkType],GIM.Supp_Name[SupName],";
                SSQL = SSQL + "GIM.Others[Others],GIM.WarehouseName[WarehouseName],GIM.DeptName[DeptName],GIS.ItemCode[ItemCode],GIS.ItemName[ItemName],'Kg'[UOM],";
                SSQL = SSQL + "GIS.OutQty[OutQty],GIS.INQty[INQty],GIS.SCRABQTY[SCRABQTY],GIS.Rate[Rate],GIS.Value[BeforGst],GIS.PackingAmt[PackingAmt],GIS.DiscAmt[DiscAmt],GIS.CGSTPer[CGSTPer],";
                SSQL = SSQL + "GIS.CGSTAmt[CGSTAmt] ,GIS.SGSTPer[SGSTPer],GIS.SGSTAmt[SGSTAmt],GIS.IGSTPer[IGSTPer],GIS.IGSTAmt[IGSTAmt] ,GIS.TotAmt[TotAmt],";
                SSQL = SSQL + "GIS.ZoneName[ZoneName],GIS.BinName[BinName] From GatePass_IN_Main GIM ";

                //SSQL = "select GIM.GP_IN_No,GIM.GP_IN_Date,GIM.GP_Out_No,GIM.GP_Out_Date,GIM.StockType,";
                //SSQL = SSQL + "GIM.Supp_Name,GIM.Supp_Det,GIM.Others,GIM.WarehouseName,GIS.ItemCode,GIS.ItemName,";
                //SSQL = SSQL + "GIS.UOMCode,GIS.OutQty,GIS.INQty,GIS.Value,GIS.ZoneName,GIS.BinName";
                SSQL = SSQL + "Inner join GatePass_IN_Main_Sub GIS ";
                SSQL = SSQL + "on GIM.GP_IN_No=GIS.GP_IN_No where CONVERT(DATETIME,GIM.GP_IN_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GIM.GP_IN_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GIM.WarehouseName='" + WareHouse + "'";
                }

                if (SupplierName != "-Select-")
                {
                    SSQL = SSQL + " And GIM.Supp_Name='" + SupplierName + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GIS.ItemName='" + ItemName + "'";
                }

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And GIM.DeptName='" + DeptName + "'";
                }


                SSQL = SSQL + " And GIM.Ccode='" + SessionCcode + "' And GIM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GIM.FinYearCode='" + SessionFinYearCode + "' And GIS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GIS.Lcode='" + SessionLcode + "' And GIS.FinYearCode='" + SessionFinYearCode + "' And GIM.GP_IN_Status='1'";

                GPINDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FromDate != "" && ToDate != "" && GPINNo != "-Select-")
            {

                //SSQL = "select GIM.GP_IN_No,GIM.GP_IN_Date,GIM.GP_Out_No,GIM.GP_Out_Date,GIM.StockType,";
                //SSQL = SSQL + "GIM.Supp_Name,GIM.Supp_Det,GIM.Others,GIM.WarehouseName,GIS.ItemCode,GIS.ItemName,";
                //SSQL = SSQL + "GIS.UOMCode,GIS.OutQty,GIS.INQty,GIS.Value,GIS.ZoneName,GIS.BinName";
                //SSQL = SSQL + " from GatePass_IN_Main GIM inner join GatePass_IN_Main_Sub GIS ";
                //SSQL = SSQL + "on GIM.GP_IN_No=GIS.GP_IN_No where ";


                SSQL = "select GIM.GP_IN_No[GPInNo],GIM.GP_IN_Date[GPInDate] ,GIM.GP_Out_No[GPOutNo],GIM.GP_Out_Date[GPOutDATE],GIM.StockType[StkType],GIM.Supp_Name[SupName],";
                SSQL = SSQL + "GIM.Others[Others],GIM.WarehouseName[WarehouseName],GIM.DeptName[DeptName],GIS.ItemCode[ItemCode],GIS.ItemName[ItemName],'Kg'[UOM],";
                SSQL = SSQL + "GIS.OutQty[OutQty],GIS.INQty[INQty],GIS.SCRABQTY[SCRABQTY],GIS.Rate[Rate],GIS.Value[BeforGst],GIS.PackingAmt[PackingAmt],GIS.DiscAmt[DiscAmt],GIS.CGSTPer[CGSTPer],";
                SSQL = SSQL + "GIS.CGSTAmt[CGSTAmt] ,GIS.SGSTPer[SGSTPer],GIS.SGSTAmt[SGSTAmt],GIS.IGSTPer[IGSTPer],GIS.IGSTAmt[IGSTAmt] ,GIS.TotAmt[TotAmt],";
                SSQL = SSQL + "GIS.ZoneName[ZoneName],GIS.BinName[BinName] From GatePass_IN_Main GIM ";
                SSQL = SSQL + "Inner Join GatePass_IN_Main_Sub GIS on GIM.GP_IN_No = GIS.GP_IN_No Where ";

                if (GPINNo != "-Select-")
                {
                    SSQL = SSQL + "GIM.GP_IN_No='" + GPINNo + "' And ";
                }

                SSQL = SSQL + "CONVERT(DATETIME,GIM.GP_IN_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GIM.GP_IN_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GIM.WarehouseName='" + WareHouse + "'";
                }
                if (SupplierName != "-Select-")
                {
                    SSQL = SSQL + " And GIM.Supp_Name='" + SupplierName + "'";
                }
                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GIS.ItemName='" + ItemName + "'";
                }

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And GIM.DeptName='" + DeptName + "'";
                }

                SSQL = SSQL + " And GIM.Ccode='" + SessionCcode + "' And GIM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GIM.FinYearCode='" + SessionFinYearCode + "' And GIS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GIS.Lcode='" + SessionLcode + "' And GIS.FinYearCode='" + SessionFinYearCode + "' And GIM.GP_IN_Status='1'";

                GPINDT = objdata.RptEmployeeMultipleDetails(SSQL);


            }
            else if (GPINNo != "-Select-")
            {

                //SSQL = "select GIM.GP_IN_No,GIM.GP_IN_Date,GIM.GP_Out_No,GIM.GP_Out_Date,GIM.StockType,";
                //SSQL = SSQL + "GIM.Supp_Name,GIM.Supp_Det,GIM.Others,GIM.WarehouseName,GIS.ItemCode,GIS.ItemName,";
                //SSQL = SSQL + "GIS.UOMCode,GIS.OutQty,GIS.INQty,GIS.Value,GIS.ZoneName,GIS.BinName";
                //SSQL = SSQL + " from GatePass_IN_Main GIM 

                SSQL = "select GIM.GP_IN_No[GPInNo],GIM.GP_IN_Date[GPInDate] ,GIM.GP_Out_No[GPOutNo],GIM.GP_Out_Date[GPOutDATE],GIM.StockType[StkType],GIM.Supp_Name[SupName],";
                SSQL = SSQL + "GIM.Others[Others],GIM.WarehouseName[WarehouseName],GIM.DeptName[DeptName],GIS.ItemCode[ItemCode],GIS.ItemName[ItemName],'Kg'[UOM],";
                SSQL = SSQL + "GIS.OutQty[OutQty],GIS.INQty[INQty],GIS.SCRABQTY[SCRABQTY],GIS.Rate[Rate],GIS.Value[BeforGst],GIS.PackingAmt[PackingAmt],GIS.DiscAmt[DiscAmt],GIS.CGSTPer[CGSTPer],";
                SSQL = SSQL + "GIS.CGSTAmt[CGSTAmt] ,GIS.SGSTPer[SGSTPer],GIS.SGSTAmt[SGSTAmt],GIS.IGSTPer[IGSTPer],GIS.IGSTAmt[IGSTAmt] ,GIS.TotAmt[TotAmt],";
                SSQL = SSQL + "GIS.ZoneName[ZoneName],GIS.BinName[BinName] From GatePass_IN_Main GIM ";

                SSQL = SSQL + " inner join GatePass_IN_Main_Sub GIS On GIM.GP_IN_No=GIS.GP_IN_No where GIM.GP_IN_No='" + GPINNo + "'";

                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GIM.WarehouseName='" + WareHouse + "'";
                }
                if (SupplierName != "-Select-")
                {
                    SSQL = SSQL + " And GIM.Supp_Name='" + SupplierName + "'";
                }
                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GIS.ItemName='" + ItemName + "'";
                }

                SSQL = SSQL + " And GIM.Ccode='" + SessionCcode + "' And GIM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GIM.FinYearCode='" + SessionFinYearCode + "' And GIS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GIS.Lcode='" + SessionLcode + "' And GIS.FinYearCode='" + SessionFinYearCode + "' And GIM.GP_IN_Status='1'";

                GPINDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }


            else if (WareHouse != "-Select-")
            {
                //SSQL = "select GIM.GP_IN_No,GIM.GP_IN_Date,GIM.GP_Out_No,GIM.GP_Out_Date,GIM.StockType,";
                //SSQL = SSQL + "GIM.Supp_Name,GIM.Supp_Det,GIM.Others,GIM.WarehouseName,GIS.ItemCode,GIS.ItemName,";
                //SSQL = SSQL + "GIS.UOMCode,GIS.OutQty,GIS.INQty,GIS.Value,GIS.ZoneName,GIS.BinName";

                SSQL = "select GIM.GP_IN_No[GPInNo],GIM.GP_IN_Date[GPInDate] ,GIM.GP_Out_No[GPOutNo],GIM.GP_Out_Date[GPOutDATE],GIM.StockType[StkType],GIM.Supp_Name[SupName],";
                SSQL = SSQL + "GIM.Others[Others],GIM.WarehouseName[WarehouseName],GIM.DeptName[DeptName],GIS.ItemCode[ItemCode],GIS.ItemName[ItemName],'Kg'[UOM],";
                SSQL = SSQL + "GIS.OutQty[OutQty],GIS.INQty[INQty],GIS.SCRABQTY[SCRABQTY],GIS.Rate[Rate],GIS.Value[BeforGst],GIS.PackingAmt[PackingAmt],GIS.DiscAmt[DiscAmt],GIS.CGSTPer[CGSTPer],";
                SSQL = SSQL + "GIS.CGSTAmt[CGSTAmt] ,GIS.SGSTPer[SGSTPer],GIS.SGSTAmt[SGSTAmt],GIS.IGSTPer[IGSTPer],GIS.IGSTAmt[IGSTAmt] ,GIS.TotAmt[TotAmt],";
                SSQL = SSQL + "GIS.ZoneName[ZoneName],GIS.BinName[BinName] From GatePass_IN_Main GIM inner join GatePass_IN_Main_Sub GIS ";
                SSQL = SSQL + "on GIM.GP_IN_No=GIS.GP_IN_No where GIM.WarehouseName='" + WareHouse + "'";

                if (GPOutNo != "-Select-")
                {
                    SSQL = SSQL + " And GIM.GP_Out_No='" + GPOutNo + "'";
                }
                if (SupplierName != "-Select-")
                {
                    SSQL = SSQL + " And GIM.Supp_Name='" + SupplierName + "'";
                }
                if (GPINNo != "-Select-")
                {
                    SSQL = SSQL + " And GIM.GP_IN_No='" + GPINNo + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GIS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GIM.Ccode='" + SessionCcode + "' And GIM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GIM.FinYearCode='" + SessionFinYearCode + "' And GIS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GIS.Lcode='" + SessionLcode + "' And GIS.FinYearCode='" + SessionFinYearCode + "' And GIM.GP_IN_Status='1'";

                GPINDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (GPOutNo != "-Select-")
            {
                //SSQL = "select GIM.GP_IN_No,GIM.GP_IN_Date,GIM.GP_Out_No,GIM.GP_Out_Date,GIM.StockType,";
                //SSQL = SSQL + "GIM.Supp_Name,GIM.Supp_Det,GIM.Others,GIM.WarehouseName,GIS.ItemCode,GIS.ItemName,";
                //SSQL = SSQL + "GIS.UOMCode,GIS.OutQty,GIS.INQty,GIS.Value,GIS.ZoneName,GIS.BinName";

                SSQL = "select GIM.GP_IN_No[GPInNo],GIM.GP_IN_Date[GPInDate] ,GIM.GP_Out_No[GPOutNo],GIM.GP_Out_Date[GPOutDATE],GIM.StockType[StkType],GIM.Supp_Name[SupName],";
                SSQL = SSQL + "GIM.Others[Others],GIM.WarehouseName[WarehouseName],GIM.DeptName[DeptName],GIS.ItemCode[ItemCode],GIS.ItemName[ItemName],'Kg'[UOM],";
                SSQL = SSQL + "GIS.OutQty[OutQty],GIS.INQty[INQty],GIS.SCRABQTY[SCRABQTY],GIS.Rate[Rate],GIS.Value[BeforGst],GIS.PackingAmt[PackingAmt],GIS.DiscAmt[DiscAmt],GIS.CGSTPer[CGSTPer],";
                SSQL = SSQL + "GIS.CGSTAmt[CGSTAmt] ,GIS.SGSTPer[SGSTPer],GIS.SGSTAmt[SGSTAmt],GIS.IGSTPer[IGSTPer],GIS.IGSTAmt[IGSTAmt] ,GIS.TotAmt[TotAmt],";
                SSQL = SSQL + "GIS.ZoneName[ZoneName],GIS.BinName[BinName] From GatePass_IN_Main GIM inner join GatePass_IN_Main_Sub GIS ";
                SSQL = SSQL + "on GIM.GP_IN_No=GIS.GP_IN_No where GIM.GP_Out_No='" + GPOutNo + "'";

                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GIM.WarehouseName='" + WareHouse + "'";
                }

                if (GPINNo != "-Select-")
                {
                    SSQL = SSQL + " And GIM.GP_IN_No='" + GPINNo + "'";
                }
                if (SupplierName != "-Select-")
                {
                    SSQL = SSQL + " And GIM.Supp_Name='" + SupplierName + "'";
                }
                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GIS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GIM.Ccode='" + SessionCcode + "' And GIM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GIM.FinYearCode='" + SessionFinYearCode + "' And GIS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GIS.Lcode='" + SessionLcode + "' And GIS.FinYearCode='" + SessionFinYearCode + "' And GIM.GP_IN_Status='1'";

                GPINDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (SupplierName != "-Select-")
            {

                SSQL = "select GIM.GP_IN_No,GIM.GP_IN_Date,GIM.GP_Out_No,GIM.GP_Out_Date,GIM.StockType,";
                SSQL = SSQL + "GIM.Supp_Name,GIM.Supp_Det,GIM.Others,GIM.WarehouseName,GIS.ItemCode,GIS.ItemName,";
                SSQL = SSQL + "GIS.UOMCode,GIS.OutQty,GIS.INQty,GIS.Value,GIS.ZoneName,GIS.BinName";
                SSQL = SSQL + " from GatePass_IN_Main GIM inner join GatePass_IN_Main_Sub GIS ";
                SSQL = SSQL + "on GIM.GP_IN_No=GIS.GP_IN_No where GIM.Supp_Name='" + SupplierName + "'";

                if (GPOutNo != "-Select-")
                {
                    SSQL = SSQL + " And GIM.GP_Out_No='" + GPOutNo + "'";
                }
                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GIM.WarehouseName='" + WareHouse + "'";
                }
                if (GPINNo != "-Select-")
                {
                    SSQL = SSQL + " And GIM.GP_IN_No='" + GPINNo + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And GIS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GIM.Ccode='" + SessionCcode + "' And GIM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GIM.FinYearCode='" + SessionFinYearCode + "' And GIS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GIS.Lcode='" + SessionLcode + "' And GIS.FinYearCode='" + SessionFinYearCode + "' And GIM.GP_IN_Status='1'";

                GPINDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }


            else if (ItemName != "-Select-")
            {
                SSQL = "select GIM.GP_IN_No,GIM.GP_IN_Date,GIM.GP_Out_No,GIM.GP_Out_Date,GIM.StockType,";
                SSQL = SSQL + "GIM.Supp_Name,GIM.Supp_Det,GIM.Others,GIM.WarehouseName,GIS.ItemCode,GIS.ItemName,";
                SSQL = SSQL + "GIS.UOMCode,GIS.OutQty,GIS.INQty,GIS.Value,GIS.ZoneName,GIS.BinName";
                SSQL = SSQL + " from GatePass_IN_Main GIM inner join GatePass_IN_Main_Sub GIS ";
                SSQL = SSQL + "on GIM.GP_IN_No=GIS.GP_IN_No where GIS.ItemName='" + ItemName + "'";

                if (GPOutNo != "-Select-")
                {
                    SSQL = SSQL + " And GIM.GP_Out_No='" + GPOutNo + "'";
                }
                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And GIM.WarehouseName='" + WareHouse + "'";
                }
                if (GPINNo != "-Select-")
                {
                    SSQL = SSQL + " And GIM.GP_IN_No='" + GPINNo + "'";
                }

                if (SupplierName != "-Select-")
                {
                    SSQL = SSQL + "And GIM.Supp_Name='" + SupplierName + "'";
                }


                SSQL = SSQL + " And GIM.Ccode='" + SessionCcode + "' And GIM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GIM.FinYearCode='" + SessionFinYearCode + "' And GIS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GIS.Lcode='" + SessionLcode + "' And GIS.FinYearCode='" + SessionFinYearCode + "' And GIM.GP_IN_Status='1'";

                GPINDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (GPINDT.Rows.Count > 0)
            {


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTGPIN.Columns.Add("FDate");
                AutoDTGPIN.Columns.Add("TDate");

                AutoDTGPIN.Columns.Add("CompanyName");
                AutoDTGPIN.Columns.Add("LocationName");
                AutoDTGPIN.Columns.Add("CompanyCode");
                AutoDTGPIN.Columns.Add("LocationCode");
                AutoDTGPIN.Columns.Add("Address1");
                AutoDTGPIN.Columns.Add("Address2");
                AutoDTGPIN.Columns.Add("GPINNo");
                AutoDTGPIN.Columns.Add("GPINDate");
                AutoDTGPIN.Columns.Add("GPOutNo");
                AutoDTGPIN.Columns.Add("GPOutDate");
                AutoDTGPIN.Columns.Add("SuppName");
                //AutoDTGPIN.Columns.Add("SuppDet");
                AutoDTGPIN.Columns.Add("Others");
                AutoDTGPIN.Columns.Add("WarehouseName");
                AutoDTGPIN.Columns.Add("DeptName");

                AutoDTGPIN.Columns.Add("ItemCode");
                AutoDTGPIN.Columns.Add("ItemName");
                AutoDTGPIN.Columns.Add("Uom");
                AutoDTGPIN.Columns.Add("OutQty");
                AutoDTGPIN.Columns.Add("INQty");
                AutoDTGPIN.Columns.Add("SCRABQty");
                AutoDTGPIN.Columns.Add("Rate");
                AutoDTGPIN.Columns.Add("BeforGst");
                AutoDTGPIN.Columns.Add("PackingAmt");
                AutoDTGPIN.Columns.Add("DiscAmt");
                AutoDTGPIN.Columns.Add("CGSTPer");
                AutoDTGPIN.Columns.Add("CGSTAmt");
                AutoDTGPIN.Columns.Add("SGSTPer");
                AutoDTGPIN.Columns.Add("SGSTAmt");
                AutoDTGPIN.Columns.Add("IGSTPer");
                AutoDTGPIN.Columns.Add("IGSTAmt");
                AutoDTGPIN.Columns.Add("TotAmt");
                AutoDTGPIN.Columns.Add("ZoneName");
                AutoDTGPIN.Columns.Add("BinName");

                string StockType = "";

                for (int i = 0; i < GPINDT.Rows.Count; i++)
                {
                    AutoDTGPIN.NewRow();
                    AutoDTGPIN.Rows.Add();

                    AutoDTGPIN.Rows[i]["FDate"] = frmDate.ToString("dd-MM-yyyy");
                    AutoDTGPIN.Rows[i]["TDate"] = toDate.ToString("dd-MM-yyyy");

                    AutoDTGPIN.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTGPIN.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTGPIN.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTGPIN.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTGPIN.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTGPIN.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                    //StockType = GPINDT.Rows[i]["StkType"].ToString();

                    AutoDTGPIN.Rows[i]["GPINNo"] = GPINDT.Rows[i]["GPInNo"];
                    AutoDTGPIN.Rows[i]["GPINDate"] = GPINDT.Rows[i]["GPInDate"];
                    AutoDTGPIN.Rows[i]["GPOutNo"] = GPINDT.Rows[i]["GPOutNo"];
                    AutoDTGPIN.Rows[i]["GPOutDate"] = GPINDT.Rows[i]["GPOutDATE"];
                    AutoDTGPIN.Rows[i]["SuppName"] = GPINDT.Rows[i]["SupName"];
                    //AutoDTGPIN.Rows[i]["SuppDet"] = GPINDT.Rows[i]["Supp_Det"];
                    AutoDTGPIN.Rows[i]["Others"] = GPINDT.Rows[i]["Others"];
                    AutoDTGPIN.Rows[i]["WarehouseName"] = GPINDT.Rows[i]["WarehouseName"];
                    AutoDTGPIN.Rows[i]["DeptName"] = GPINDT.Rows[i]["DeptName"];

                    AutoDTGPIN.Rows[i]["ItemCode"] = GPINDT.Rows[i]["ItemCode"];
                    AutoDTGPIN.Rows[i]["ItemName"] = GPINDT.Rows[i]["ItemName"];
                    AutoDTGPIN.Rows[i]["UOM"] = GPINDT.Rows[i]["UOM"];
                    AutoDTGPIN.Rows[i]["OutQty"] = GPINDT.Rows[i]["OutQty"];
                    AutoDTGPIN.Rows[i]["INQty"] = GPINDT.Rows[i]["INQty"];
                    AutoDTGPIN.Rows[i]["SCRABQty"] = GPINDT.Rows[i]["SCRABQty"];
                    AutoDTGPIN.Rows[i]["Rate"] = GPINDT.Rows[i]["Rate"];

                    AutoDTGPIN.Rows[i]["BeforGst"] = GPINDT.Rows[i]["BeforGst"];
                    AutoDTGPIN.Rows[i]["PackingAmt"] = GPINDT.Rows[i]["PackingAmt"];
                    AutoDTGPIN.Rows[i]["DiscAmt"] = GPINDT.Rows[i]["DiscAmt"];
                    AutoDTGPIN.Rows[i]["CGSTPer"] = GPINDT.Rows[i]["CGSTPer"];
                    AutoDTGPIN.Rows[i]["CGSTAmt"] = GPINDT.Rows[i]["CGSTAmt"];
                    AutoDTGPIN.Rows[i]["SGSTPer"] = GPINDT.Rows[i]["SGSTPer"];
                    AutoDTGPIN.Rows[i]["SGSTAmt"] = GPINDT.Rows[i]["SGSTAmt"];
                    AutoDTGPIN.Rows[i]["IGSTPer"] = GPINDT.Rows[i]["IGSTPer"];
                    AutoDTGPIN.Rows[i]["IGSTAmt"] = GPINDT.Rows[i]["IGSTAmt"];
                    AutoDTGPIN.Rows[i]["TotAmt"] = GPINDT.Rows[i]["TotAmt"];

                    AutoDTGPIN.Rows[i]["ZoneName"] = GPINDT.Rows[i]["ZoneName"];
                    AutoDTGPIN.Rows[i]["BinName"] = GPINDT.Rows[i]["BinName"];
                }

                if (ButtonName == "GP IN SLIP")
                {
                    if (AutoDTGPIN.Rows.Count < 5)
                    {
                        int Row_Count = AutoDTGPIN.Rows.Count;
                        for (int i = Row_Count; i < 4; i++)
                        {
                            AutoDTGPIN.NewRow();
                            AutoDTGPIN.Rows.Add();
                            AutoDTGPIN.Rows[i]["GPINNo"] = AutoDTGPIN.Rows[0]["GPINNo"];
                        }
                    }
                    ds.Tables.Add(AutoDTGPIN);
                    ReportDocument report = new ReportDocument();
                    if (StockType == "1")
                    {
                        report.Load(Server.MapPath("~/crystal/GatePassINSlip_Two_Copy.rpt"));
                    }
                    else
                    {
                        report.Load(Server.MapPath("~/crystal/GatePassINSlipNoStock_Two_Copy.rpt"));
                    }
                    //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                    report.SetDataSource(ds.Tables[0]);

                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                    CrystalReportViewer1.ReportSource = report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();

                    //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                    //CrystalReportViewer1.ReportSource = report;
                }
                else if (ButtonName == "GatePassIn_Scrab")
                {
                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(AutoDTGPIN);

                    RD_Report.Load(Server.MapPath("~/crystal/GatePassINScrabDetRpt.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);


                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }
                else
                {
                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(AutoDTGPIN);


                    RD_Report.Load(Server.MapPath("~/crystal/GatePassINDetailsReport_New.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);

                    //Company Details Add

                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }
                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();

                }

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }

    }
    //Gate Pass IN Details Code End
    public void Service_Entry()
    {
        string SQL = "";
        DataTable dt = new DataTable();
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("Date");
        AutoDataTable.Columns.Add("Item_Name");
        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("Quantity");
        AutoDataTable.Columns.Add("Rate");
        AutoDataTable.Columns.Add("Cost");
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }
        if (FromDate != "" && ToDate != "")
        {

            SQL = "select * From Service_Entry ";
            SQL = SQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            SQL = SQL + " And CONVERT(DATETIME,Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SQL = SQL + " And CONVERT(DATETIME,Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SQL = SQL + " order by Date asc";
            dt = objdata.RptEmployeeMultipleDetails(SQL);
        }
        else if (FromDate != "" && ToDate == "")
        {
            SQL = "select * From Service_Entry ";
            SQL = SQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            //CONVERT(DATETIME,SpareReturn_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SpareReturn_Date, 103)<=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)
            SQL = SQL + "And CONVERT(DATETIME,Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)And CONVERT(DATETIME,Date, 103)<=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SQL = SQL + " order by Date asc";
            dt = objdata.RptEmployeeMultipleDetails(SQL);
        }
        else if (FromDate == "" & ToDate == "")
        {
            SQL = "select * From Service_Entry ";
            SQL = SQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            SQL = SQL + " order by Date asc";
            dt = objdata.RptEmployeeMultipleDetails(SQL);
        }

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();
                AutoDataTable.Rows[i]["CompanyName"] = dt.Rows[i]["Ccode"].ToString();
                AutoDataTable.Rows[i]["LocationName"] = dt.Rows[i]["Lcode"].ToString();

                AutoDataTable.Rows[i]["Date"] = dt.Rows[i]["Date"].ToString();
                AutoDataTable.Rows[i]["Item_Name"] = dt.Rows[i]["Item_Name"].ToString();
                AutoDataTable.Rows[i]["DeptName"] = dt.Rows[i]["Dept"].ToString();
                AutoDataTable.Rows[i]["Quantity"] = dt.Rows[i]["Quantity"].ToString();
                AutoDataTable.Rows[i]["Rate"] = dt.Rows[i]["Rate"].ToString();
                AutoDataTable.Rows[i]["Cost"] = dt.Rows[i]["CostCenter"].ToString();


            }
        }

        DataSet ds1 = new DataSet();
        ds1.Tables.Add(AutoDataTable);
        RD_Report.Load(Server.MapPath("~/crystal/ServiceEntry.RPT"));
        //  RD_Report.DataDefinition.FormulaFields["ReportHeading"].Text = "'" + Heading + "'";
        RD_Report.SetDataSource(ds1.Tables[0]);
        RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
        CrystalReportViewer1.ReportSource = RD_Report;
        CrystalReportViewer1.RefreshReport();
        CrystalReportViewer1.DataBind();

    }
    public void NonStock_Scrab_Report()
    {
        string SQL = "";
        DataTable dt = new DataTable();
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("Date");
        AutoDataTable.Columns.Add("Item_Name");
        AutoDataTable.Columns.Add("Quantity");
        AutoDataTable.Columns.Add("Rate");
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }
        if (FromDate != "" && ToDate != "")
        {

            SQL = "select * From Non_Stock_Scrab  ";
            SQL = SQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            SQL = SQL + " And CONVERT(DATETIME,Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SQL = SQL + " And CONVERT(DATETIME,Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            dt = objdata.RptEmployeeMultipleDetails(SQL);
        }
        else if (FromDate != "" && ToDate == "")
        {
            SQL = "select * From Non_Stock_Scrab  ";
            SQL = SQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            //CONVERT(DATETIME,SpareReturn_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SpareReturn_Date, 103)<=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)
            SQL = SQL + "And CONVERT(DATETIME,Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)And CONVERT(DATETIME,Date, 103)<=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            dt = objdata.RptEmployeeMultipleDetails(SQL);
        }
        else if (FromDate == "" & ToDate == "")
        {
            SQL = "select * From Non_Stock_Scrab  ";
            SQL = SQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            dt = objdata.RptEmployeeMultipleDetails(SQL);
        }

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();
                AutoDataTable.Rows[i]["CompanyName"] = dt.Rows[i]["Ccode"].ToString();
                AutoDataTable.Rows[i]["LocationName"] = dt.Rows[i]["Lcode"].ToString();

                AutoDataTable.Rows[i]["Date"] = dt.Rows[i]["Date"].ToString();
                AutoDataTable.Rows[i]["Item_Name"] = dt.Rows[i]["Item_Name"].ToString();
                AutoDataTable.Rows[i]["Quantity"] = dt.Rows[i]["Quantity"].ToString();
                AutoDataTable.Rows[i]["Rate"] = dt.Rows[i]["Rate"].ToString();

            }
        }




        DataSet ds1 = new DataSet();
        ds1.Tables.Add(AutoDataTable);
        RD_Report.Load(Server.MapPath("~/crystal/NonStockScrab.RPT"));
        //  RD_Report.DataDefinition.FormulaFields["ReportHeading"].Text = "'" + Heading + "'";
        RD_Report.SetDataSource(ds1.Tables[0]);
        RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
        CrystalReportViewer1.ReportSource = RD_Report;
        CrystalReportViewer1.RefreshReport();
        CrystalReportViewer1.DataBind();


    }

    //Gate Pass Out Returnable Pending Details Code Start
    public void GP_Out_Returnable_Pending_Report()
    {

        if (RptName == "GP Out Returnable Pending Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            if (FromDate != "" && ToDate != "" && GPINNo == "" && GPOutNo == "")
            {

                SSQL = "select GIM.GP_IN_No,GIM.GP_IN_Date,GIM.GP_Out_No,GIM.GP_Out_Date,";
                SSQL = SSQL + "GIM.Supp_Name,GIM.Supp_Det,GIM.Others,GIM.WarehouseName,GIS.ItemCode,GIS.ItemName,";
                SSQL = SSQL + "GIS.UOMCode,GIS.OutQty,GIS.INQty,GIS.Value,GIS.ZoneName,GIS.BinName";
                SSQL = SSQL + " from GatePass_IN_Main GIM inner join GatePass_IN_Main_Sub GIS ";
                SSQL = SSQL + "on GIM.GP_IN_No=GIS.GP_IN_No where CONVERT(DATETIME,GIM.GP_IN_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GIM.GP_IN_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (WareHouse != "")
                {
                    SSQL = SSQL + "And GIM.WarehouseName='" + WareHouse + "'";
                }

                if (SupplierName != "")
                {
                    SSQL = SSQL + "And GIM.Supp_Name='" + SupplierName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And GIS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GIM.Ccode='" + SessionCcode + "' And GIM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GIM.FinYearCode='" + SessionFinYearCode + "' And GIS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GIS.Lcode='" + SessionLcode + "' And GIS.FinYearCode='" + SessionFinYearCode + "' And GIS.INQty < GIS.OutQty And GIM.GP_IN_Status='1'";

                GPINDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FromDate != "" && ToDate != "" && GPINNo != "")
            {

                SSQL = "select GIM.GP_IN_No,GIM.GP_IN_Date,GIM.GP_Out_No,GIM.GP_Out_Date,";
                SSQL = SSQL + "GIM.Supp_Name,GIM.Supp_Det,GIM.Others,GIM.WarehouseName,GIS.ItemCode,GIS.ItemName,";
                SSQL = SSQL + "GIS.UOMCode,GIS.OutQty,GIS.INQty,GIS.Value,GIS.ZoneName,GIS.BinName";
                SSQL = SSQL + " from GatePass_IN_Main GIM inner join GatePass_IN_Main_Sub GIS ";
                SSQL = SSQL + "on GIM.GP_IN_No=GIS.GP_IN_No where GIM.GP_IN_No='" + GPINNo + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,GIM.GP_IN_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GIM.GP_IN_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                if (WareHouse != "")
                {
                    SSQL = SSQL + "And GIM.WarehouseName='" + WareHouse + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + "And GIM.Supp_Name='" + SupplierName + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And GIS.ItemName='" + ItemName + "'";
                }

                SSQL = SSQL + " And GIM.Ccode='" + SessionCcode + "' And GIM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GIM.FinYearCode='" + SessionFinYearCode + "' And GIS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GIS.Lcode='" + SessionLcode + "' And GIS.FinYearCode='" + SessionFinYearCode + "' And GIS.INQty < GIS.OutQty And GIM.GP_IN_Status='1'";

                GPINDT = objdata.RptEmployeeMultipleDetails(SSQL);


            }
            else if (GPINNo != "")
            {
                SSQL = "select GIM.GP_IN_No,GIM.GP_IN_Date,GIM.GP_Out_No,GIM.GP_Out_Date,";
                SSQL = SSQL + "GIM.Supp_Name,GIM.Supp_Det,GIM.Others,GIM.WarehouseName,GIS.ItemCode,GIS.ItemName,";
                SSQL = SSQL + "GIS.UOMCode,GIS.OutQty,GIS.INQty,GIS.Value,GIS.ZoneName,GIS.BinName";
                SSQL = SSQL + " from GatePass_IN_Main GIM inner join GatePass_IN_Main_Sub GIS ";
                SSQL = SSQL + "on GIM.GP_IN_No=GIS.GP_IN_No where GIM.GP_IN_No='" + GPINNo + "'";

                if (WareHouse != "")
                {
                    SSQL = SSQL + "And GIM.WarehouseName='" + WareHouse + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + "And GIM.Supp_Name='" + SupplierName + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And GIS.ItemName='" + ItemName + "'";
                }

                SSQL = SSQL + " And GIM.Ccode='" + SessionCcode + "' And GIM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GIM.FinYearCode='" + SessionFinYearCode + "' And GIS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GIS.Lcode='" + SessionLcode + "' And GIS.FinYearCode='" + SessionFinYearCode + "' And GIS.INQty < GIS.OutQty And GIM.GP_IN_Status='1'";

                GPINDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }


            else if (WareHouse != "")
            {
                SSQL = "select GIM.GP_IN_No,GIM.GP_IN_Date,GIM.GP_Out_No,GIM.GP_Out_Date,";
                SSQL = SSQL + "GIM.Supp_Name,GIM.Supp_Det,GIM.Others,GIM.WarehouseName,GIS.ItemCode,GIS.ItemName,";
                SSQL = SSQL + "GIS.UOMCode,GIS.OutQty,GIS.INQty,GIS.Value,GIS.ZoneName,GIS.BinName";
                SSQL = SSQL + " from GatePass_IN_Main GIM inner join GatePass_IN_Main_Sub GIS ";
                SSQL = SSQL + "on GIM.GP_IN_No=GIS.GP_IN_No where GIM.WarehouseName='" + WareHouse + "'";

                if (GPOutNo != "")
                {
                    SSQL = SSQL + " And GIM.GP_Out_No='" + GPOutNo + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + " And GIM.Supp_Name='" + SupplierName + "'";
                }
                if (GPINNo != "")
                {
                    SSQL = SSQL + " And GIM.GP_IN_No='" + GPINNo + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And GIS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GIM.Ccode='" + SessionCcode + "' And GIM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GIM.FinYearCode='" + SessionFinYearCode + "' And GIS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GIS.Lcode='" + SessionLcode + "' And GIS.FinYearCode='" + SessionFinYearCode + "' And GIS.INQty < GIS.OutQty And GIM.GP_IN_Status='1'";

                GPINDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (GPOutNo != "")
            {
                SSQL = "select GIM.GP_IN_No,GIM.GP_IN_Date,GIM.GP_Out_No,GIM.GP_Out_Date,";
                SSQL = SSQL + "GIM.Supp_Name,GIM.Supp_Det,GIM.Others,GIM.WarehouseName,GIS.ItemCode,GIS.ItemName,";
                SSQL = SSQL + "GIS.UOMCode,GIS.OutQty,GIS.INQty,GIS.Value,GIS.ZoneName,GIS.BinName";
                SSQL = SSQL + " from GatePass_IN_Main GIM inner join GatePass_IN_Main_Sub GIS ";
                SSQL = SSQL + "on GIM.GP_IN_No=GIS.GP_IN_No where GIM.GP_Out_No='" + GPOutNo + "'";

                if (WareHouse != "")
                {
                    SSQL = SSQL + "And GIM.WarehouseName='" + WareHouse + "'";
                }

                if (GPINNo != "")
                {
                    SSQL = SSQL + " And GIM.GP_IN_No='" + GPINNo + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + "And GIM.Supp_Name='" + SupplierName + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And GIS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GIM.Ccode='" + SessionCcode + "' And GIM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GIM.FinYearCode='" + SessionFinYearCode + "' And GIS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GIS.Lcode='" + SessionLcode + "' And GIS.FinYearCode='" + SessionFinYearCode + "' And GIS.INQty < GIS.OutQty And GIM.GP_IN_Status='1'";

                GPINDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (SupplierName != "")
            {

                SSQL = "select GIM.GP_IN_No,GIM.GP_IN_Date,GIM.GP_Out_No,GIM.GP_Out_Date,";
                SSQL = SSQL + "GIM.Supp_Name,GIM.Supp_Det,GIM.Others,GIM.WarehouseName,GIS.ItemCode,GIS.ItemName,";
                SSQL = SSQL + "GIS.UOMCode,GIS.OutQty,GIS.INQty,GIS.Value,GIS.ZoneName,GIS.BinName";
                SSQL = SSQL + " from GatePass_IN_Main GIM inner join GatePass_IN_Main_Sub GIS ";
                SSQL = SSQL + "on GIM.GP_IN_No=GIS.GP_IN_No where GIM.Supp_Name='" + SupplierName + "'";

                if (GPOutNo != "")
                {
                    SSQL = SSQL + " And GIM.GP_Out_No='" + GPOutNo + "'";
                }
                if (WareHouse != "")
                {
                    SSQL = SSQL + " And GIM.WarehouseName='" + WareHouse + "'";
                }
                if (GPINNo != "")
                {
                    SSQL = SSQL + " And GIM.GP_IN_No='" + GPINNo + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And GIS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GIM.Ccode='" + SessionCcode + "' And GIM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GIM.FinYearCode='" + SessionFinYearCode + "' And GIS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GIS.Lcode='" + SessionLcode + "' And GIS.FinYearCode='" + SessionFinYearCode + "' And GIS.INQty < GIS.OutQty And GIM.GP_IN_Status='1'";

                GPINDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }


            else if (ItemName != "")
            {
                SSQL = "select GIM.GP_IN_No,GIM.GP_IN_Date,GIM.GP_Out_No,GIM.GP_Out_Date,";
                SSQL = SSQL + "GIM.Supp_Name,GIM.Supp_Det,GIM.Others,GIM.WarehouseName,GIS.ItemCode,GIS.ItemName,";
                SSQL = SSQL + "GIS.UOMCode,GIS.OutQty,GIS.INQty,GIS.Value,GIS.ZoneName,GIS.BinName";
                SSQL = SSQL + " from GatePass_IN_Main GIM inner join GatePass_IN_Main_Sub GIS ";
                SSQL = SSQL + "on GIM.GP_IN_No=GIS.GP_IN_No where GIS.ItemName='" + ItemName + "'";

                if (GPOutNo != "")
                {
                    SSQL = SSQL + " And GIM.GP_Out_No='" + GPOutNo + "'";
                }
                if (WareHouse != "")
                {
                    SSQL = SSQL + " And GIM.WarehouseName='" + WareHouse + "'";
                }
                if (GPINNo != "")
                {
                    SSQL = SSQL + " And GIM.GP_IN_No='" + GPINNo + "'";
                }

                if (SupplierName != "")
                {
                    SSQL = SSQL + "And GIM.Supp_Name='" + SupplierName + "'";
                }


                SSQL = SSQL + " And GIM.Ccode='" + SessionCcode + "' And GIM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GIM.FinYearCode='" + SessionFinYearCode + "' And GIS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GIS.Lcode='" + SessionLcode + "' And GIS.FinYearCode='" + SessionFinYearCode + "' And GIS.INQty < GIS.OutQty And GIM.GP_IN_Status='1'";

                GPINDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (GPINDT.Rows.Count > 0)
            {


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



                AutoDTGPIN.Columns.Add("CompanyName");
                AutoDTGPIN.Columns.Add("LocationName");
                AutoDTGPIN.Columns.Add("CompanyCode");
                AutoDTGPIN.Columns.Add("LocationCode");
                AutoDTGPIN.Columns.Add("Address1");
                AutoDTGPIN.Columns.Add("Address2");
                AutoDTGPIN.Columns.Add("GPINNo");
                AutoDTGPIN.Columns.Add("GPINDate");
                AutoDTGPIN.Columns.Add("GPOutNo");
                AutoDTGPIN.Columns.Add("GPOutDate");
                AutoDTGPIN.Columns.Add("SuppName");
                AutoDTGPIN.Columns.Add("SuppDet");
                AutoDTGPIN.Columns.Add("Others");
                AutoDTGPIN.Columns.Add("WarehouseName");

                AutoDTGPIN.Columns.Add("ItemCode");
                AutoDTGPIN.Columns.Add("ItemName");
                AutoDTGPIN.Columns.Add("UomCode");
                AutoDTGPIN.Columns.Add("OutQty");
                AutoDTGPIN.Columns.Add("INQty");
                AutoDTGPIN.Columns.Add("Value");
                AutoDTGPIN.Columns.Add("ZoneName");
                AutoDTGPIN.Columns.Add("BinName");



                for (int i = 0; i < GPINDT.Rows.Count; i++)
                {
                    AutoDTGPIN.NewRow();
                    AutoDTGPIN.Rows.Add();

                    AutoDTGPIN.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTGPIN.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTGPIN.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTGPIN.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTGPIN.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTGPIN.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDTGPIN.Rows[i]["GPINNo"] = GPINDT.Rows[i]["GP_IN_No"];
                    AutoDTGPIN.Rows[i]["GPINDate"] = GPINDT.Rows[i]["GP_IN_Date"];
                    AutoDTGPIN.Rows[i]["GPOutNo"] = GPINDT.Rows[i]["GP_Out_No"];
                    AutoDTGPIN.Rows[i]["GPOutDate"] = GPINDT.Rows[i]["GP_Out_Date"];
                    AutoDTGPIN.Rows[i]["SuppName"] = GPINDT.Rows[i]["Supp_Name"];
                    AutoDTGPIN.Rows[i]["SuppDet"] = GPINDT.Rows[i]["Supp_Det"];
                    AutoDTGPIN.Rows[i]["Others"] = GPINDT.Rows[i]["Others"];
                    AutoDTGPIN.Rows[i]["WarehouseName"] = GPINDT.Rows[i]["WarehouseName"];



                    AutoDTGPIN.Rows[i]["ItemCode"] = GPINDT.Rows[i]["ItemCode"];
                    AutoDTGPIN.Rows[i]["ItemName"] = GPINDT.Rows[i]["ItemName"];
                    AutoDTGPIN.Rows[i]["UomCode"] = GPINDT.Rows[i]["UOMCode"];
                    AutoDTGPIN.Rows[i]["OutQty"] = GPINDT.Rows[i]["OutQty"];
                    AutoDTGPIN.Rows[i]["INQty"] = GPINDT.Rows[i]["INQty"];
                    AutoDTGPIN.Rows[i]["Value"] = GPINDT.Rows[i]["Value"];
                    AutoDTGPIN.Rows[i]["ZoneName"] = GPINDT.Rows[i]["ZoneName"];
                    AutoDTGPIN.Rows[i]["BinName"] = GPINDT.Rows[i]["BinName"];


                }

                ds.Tables.Add(AutoDTGPIN);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("~/crystal/GatePassOutReturnablePendingReport.rpt"));
                report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = report;

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }


    }
    //Gate Pass Out Returnable Pending Details Code End


    //Standard Purchase Order Details Report Code Start
    public void Standard_Purchase_Order_Details_Report()
    {

        if (RptName == "Standard Purchase Order Details Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            if (FromDate != "" && ToDate != "" && StdPurOrdNo == "")
            {

                SSQL = "select SPM.Std_PO_No,SPM.Std_PO_Date,SPM.Supp_Name,SPM.Supp_Qtn_No,SPM.Pur_Request_No,SPM.Pur_Request_Date,";
                SSQL = SSQL + "SPM.Supp_Qtn_Date,SPM.DeliveryMode,SPM.DeliveryDate,SPM.DeliveryAt,";
                SSQL = SSQL + "SPM.PaymentMode,SPM.DeptName,SPM.PaymentTerms,SPM.Description,";
                SSQL = SSQL + "SPM.Note,SPM.Others,SPM.TotalAmt,SPM.Discount,SPM.TaxPer,SPM.TaxAmount,SPM.OtherCharge,SPM.NetAmount,";
                SSQL = SSQL + "SPS.ItemCode,SPS.ItemName,SPS.UOMCode,SPS.ReuiredQty,";
                SSQL = SSQL + "SPS.OrderQty,SPS.ReuiredDate,SPS.Rate,";
                SSQL = SSQL + "SPS.Remarks,SPS.LineTotal";
                SSQL = SSQL + " from Std_Purchase_Order_Main SPM inner join Std_Purchase_Order_Main_Sub SPS ";
                SSQL = SSQL + "on SPM.Std_PO_No=SPS.Std_PO_No where CONVERT(DATETIME,SPM.Std_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SPM.Std_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (DeptName != "")
                {
                    SSQL = SSQL + " And SPM.DeptName='" + DeptName + "'";
                }

                if (SupplierName != "")
                {
                    SSQL = SSQL + " And SPM.Supp_Name='" + SupplierName + "'";
                }
                if (SupQtNo != "")
                {
                    SSQL = SSQL + " And SPM.Supp_Qtn_No='" + SupQtNo + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And SPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And SPM.FinYearCode='" + SessionFinYearCode + "' And SPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "' And SPM.PO_Status='1'";

                StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FromDate != "" && ToDate != "" && StdPurOrdNo != "")
            {

                SSQL = "select SPM.Std_PO_No,SPM.Std_PO_Date,SPM.Supp_Name,SPM.Supp_Qtn_No,SPM.Pur_Request_No,SPM.Pur_Request_Date,";
                SSQL = SSQL + "SPM.Supp_Qtn_Date,SPM.DeliveryMode,SPM.DeliveryDate,SPM.DeliveryAt,";
                SSQL = SSQL + "SPM.PaymentMode,SPM.DeptName,SPM.PaymentTerms,SPM.Description,";
                SSQL = SSQL + "SPM.Note,SPM.Others,SPM.TotalAmt,SPM.Discount,SPM.TaxPer,SPM.TaxAmount,SPM.OtherCharge,SPM.NetAmount,";
                SSQL = SSQL + "SPS.ItemCode,SPS.ItemName,SPS.UOMCode,SPS.ReuiredQty,";
                SSQL = SSQL + "SPS.OrderQty,SPS.ReuiredDate,SPS.Rate,";
                SSQL = SSQL + "SPS.Remarks,SPS.LineTotal";
                SSQL = SSQL + " from Std_Purchase_Order_Main SPM inner join Std_Purchase_Order_Main_Sub SPS ";
                SSQL = SSQL + "on SPM.Std_PO_No=SPS.Std_PO_No where SPM.Std_PO_No='" + StdPurOrdNo + "' CONVERT(DATETIME,SPM.Std_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SPM.Std_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (DeptName != "")
                {
                    SSQL = SSQL + " And SPM.DeptName='" + DeptName + "'";
                }

                if (SupplierName != "")
                {
                    SSQL = SSQL + " And SPM.Supp_Name='" + SupplierName + "'";
                }
                if (SupQtNo != "")
                {
                    SSQL = SSQL + " And SPM.Supp_Qtn_No='" + SupQtNo + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And SPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And SPM.FinYearCode='" + SessionFinYearCode + "' And SPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "' And SPM.PO_Status='1'";

                StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (StdPurOrdNo != "")
            {
                SSQL = "select SPM.Std_PO_No,SPM.Std_PO_Date,SPM.Supp_Name,SPM.Supp_Qtn_No,SPM.Pur_Request_No,SPM.Pur_Request_Date,";
                SSQL = SSQL + "SPM.Supp_Qtn_Date,SPM.DeliveryMode,SPM.DeliveryDate,SPM.DeliveryAt,";
                SSQL = SSQL + "SPM.PaymentMode,SPM.DeptName,SPM.PaymentTerms,SPM.Description,";
                SSQL = SSQL + "SPM.Note,SPM.Others,SPM.TotalAmt,SPM.Discount,SPM.TaxPer,SPM.TaxAmount,SPM.OtherCharge,SPM.NetAmount,";
                SSQL = SSQL + "SPS.ItemCode,SPS.ItemName,SPS.UOMCode,SPS.ReuiredQty,";
                SSQL = SSQL + "SPS.OrderQty,SPS.ReuiredDate,SPS.Rate,";
                SSQL = SSQL + "SPS.Remarks,SPS.LineTotal";
                SSQL = SSQL + " from Std_Purchase_Order_Main SPM inner join Std_Purchase_Order_Main_Sub SPS ";
                SSQL = SSQL + "on SPM.Std_PO_No=SPS.Std_PO_No where SPM.Std_PO_No='" + StdPurOrdNo + "'";


                if (DeptName != "")
                {
                    SSQL = SSQL + " And SPM.DeptName='" + DeptName + "'";
                }

                if (SupplierName != "")
                {
                    SSQL = SSQL + " And SPM.Supp_Name='" + SupplierName + "'";
                }
                if (SupQtNo != "")
                {
                    SSQL = SSQL + " And SPM.Supp_Qtn_No='" + SupQtNo + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And SPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And SPM.FinYearCode='" + SessionFinYearCode + "' And SPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "' And SPM.PO_Status='1'";

                StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }


            else if (DeptName != "")
            {

                SSQL = "select SPM.Std_PO_No,SPM.Std_PO_Date,SPM.Supp_Name,SPM.Supp_Qtn_No,SPM.Pur_Request_No,SPM.Pur_Request_Date,";
                SSQL = SSQL + "SPM.Supp_Qtn_Date,SPM.DeliveryMode,SPM.DeliveryDate,SPM.DeliveryAt,";
                SSQL = SSQL + "SPM.PaymentMode,SPM.DeptName,SPM.PaymentTerms,SPM.Description,";
                SSQL = SSQL + "SPM.Note,SPM.Others,SPM.TotalAmt,SPM.Discount,SPM.TaxPer,SPM.TaxAmount,SPM.OtherCharge,SPM.NetAmount,";
                SSQL = SSQL + "SPS.ItemCode,SPS.ItemName,SPS.UOMCode,SPS.ReuiredQty,";
                SSQL = SSQL + "SPS.OrderQty,SPS.ReuiredDate,SPS.Rate,";
                SSQL = SSQL + "SPS.Remarks,SPS.LineTotal";
                SSQL = SSQL + " from Std_Purchase_Order_Main SPM inner join Std_Purchase_Order_Main_Sub SPS ";
                SSQL = SSQL + "on SPM.Std_PO_No=SPS.Std_PO_No where SPM.DeptName='" + DeptName + "'";


                if (StdPurOrdNo != "")
                {
                    SSQL = SSQL + " And SPM.Std_PO_No='" + StdPurOrdNo + "'";
                }

                if (SupplierName != "")
                {
                    SSQL = SSQL + " And SPM.Supp_Name='" + SupplierName + "'";
                }
                if (SupQtNo != "")
                {
                    SSQL = SSQL + " And SPM.Supp_Qtn_No='" + SupQtNo + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And SPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And SPM.FinYearCode='" + SessionFinYearCode + "' And SPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "' And SPM.PO_Status='1'";

                StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (SupQtNo != "")
            {

                SSQL = "select SPM.Std_PO_No,SPM.Std_PO_Date,SPM.Supp_Name,SPM.Supp_Qtn_No,SPM.Pur_Request_No,SPM.Pur_Request_Date,";
                SSQL = SSQL + "SPM.Supp_Qtn_Date,SPM.DeliveryMode,SPM.DeliveryDate,SPM.DeliveryAt,";
                SSQL = SSQL + "SPM.PaymentMode,SPM.DeptName,SPM.PaymentTerms,SPM.Description,";
                SSQL = SSQL + "SPM.Note,SPM.Others,SPM.TotalAmt,SPM.Discount,SPM.TaxPer,SPM.TaxAmount,SPM.OtherCharge,SPM.NetAmount,";
                SSQL = SSQL + "SPS.ItemCode,SPS.ItemName,SPS.UOMCode,SPS.ReuiredQty,";
                SSQL = SSQL + "SPS.OrderQty,SPS.ReuiredDate,SPS.Rate,";
                SSQL = SSQL + "SPS.Remarks,SPS.LineTotal";
                SSQL = SSQL + " from Std_Purchase_Order_Main SPM inner join Std_Purchase_Order_Main_Sub SPS ";
                SSQL = SSQL + "on SPM.Std_PO_No=SPS.Std_PO_No where SPM.Supp_Qtn_No='" + SupQtNo + "'";


                if (StdPurOrdNo != "")
                {
                    SSQL = SSQL + " And SPM.Std_PO_No='" + StdPurOrdNo + "'";
                }

                if (SupplierName != "")
                {
                    SSQL = SSQL + " And SPM.Supp_Name='" + SupplierName + "'";
                }
                if (DeptName != "")
                {
                    SSQL = SSQL + " And SPM.DeptName='" + DeptName + "'";
                }
                if (ItemName != "")
                {
                    SSQL = SSQL + " And SPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And SPM.FinYearCode='" + SessionFinYearCode + "' And SPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "' And SPM.PO_Status='1'";

                StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (ItemName != "")
            {

                SSQL = "select SPM.Std_PO_No,SPM.Std_PO_Date,SPM.Supp_Name,SPM.Supp_Qtn_No,SPM.Pur_Request_No,SPM.Pur_Request_Date,";
                SSQL = SSQL + "SPM.Supp_Qtn_Date,SPM.DeliveryMode,SPM.DeliveryDate,SPM.DeliveryAt,";
                SSQL = SSQL + "SPM.PaymentMode,SPM.DeptName,SPM.PaymentTerms,SPM.Description,";
                SSQL = SSQL + "SPM.Note,SPM.Others,SPM.TotalAmt,SPM.Discount,SPM.TaxPer,SPM.TaxAmount,SPM.OtherCharge,SPM.NetAmount,";
                SSQL = SSQL + "SPS.ItemCode,SPS.ItemName,SPS.UOMCode,SPS.ReuiredQty,";
                SSQL = SSQL + "SPS.OrderQty,SPS.ReuiredDate,SPS.Rate,";
                SSQL = SSQL + "SPS.Remarks,SPS.LineTotal";
                SSQL = SSQL + " from Std_Purchase_Order_Main SPM inner join Std_Purchase_Order_Main_Sub SPS ";
                SSQL = SSQL + "on SPM.Std_PO_No=SPS.Std_PO_No where SPS.ItemName='" + ItemName + "'";


                if (StdPurOrdNo != "")
                {
                    SSQL = SSQL + " And SPM.Std_PO_No='" + StdPurOrdNo + "'";
                }

                if (SupplierName != "")
                {
                    SSQL = SSQL + " And SPM.Supp_Name='" + SupplierName + "'";
                }
                if (DeptName != "")
                {
                    SSQL = SSQL + " And SPM.DeptName='" + DeptName + "'";
                }
                if (SupQtNo != "")
                {
                    SSQL = SSQL + " And SPM.Supp_Qtn_No='" + SupQtNo + "'";
                }

                SSQL = SSQL + " And SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And SPM.FinYearCode='" + SessionFinYearCode + "' And SPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "' And SPM.PO_Status='1'";

                StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (StdPurDT.Rows.Count > 0)
            {


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTStdPur.Columns.Add("CompanyName");
                AutoDTStdPur.Columns.Add("LocationName");
                AutoDTStdPur.Columns.Add("CompanyCode");
                AutoDTStdPur.Columns.Add("LocationCode");
                AutoDTStdPur.Columns.Add("Address1");
                AutoDTStdPur.Columns.Add("Address2");
                AutoDTStdPur.Columns.Add("StdPONo");
                AutoDTStdPur.Columns.Add("StdPODate");
                AutoDTStdPur.Columns.Add("SuppName");
                AutoDTStdPur.Columns.Add("SuppQtnNo");
                AutoDTStdPur.Columns.Add("SuppQtnDate");
                AutoDTStdPur.Columns.Add("DeliveryMode");
                AutoDTStdPur.Columns.Add("DeliveryDate");
                AutoDTStdPur.Columns.Add("DeliveryAt");
                AutoDTStdPur.Columns.Add("PaymentMode");
                AutoDTStdPur.Columns.Add("DeptName");
                AutoDTStdPur.Columns.Add("PaymentTerms");
                AutoDTStdPur.Columns.Add("Description");
                AutoDTStdPur.Columns.Add("Note");
                AutoDTStdPur.Columns.Add("Others");
                AutoDTStdPur.Columns.Add("TotalAmt");
                AutoDTStdPur.Columns.Add("Discount");
                AutoDTStdPur.Columns.Add("TaxPer");
                AutoDTStdPur.Columns.Add("TaxAmount");
                AutoDTStdPur.Columns.Add("OtherCharge");
                AutoDTStdPur.Columns.Add("NetAmount");

                AutoDTStdPur.Columns.Add("PurRequestNo");
                AutoDTStdPur.Columns.Add("PurRequestDate");
                AutoDTStdPur.Columns.Add("ItemCode");
                AutoDTStdPur.Columns.Add("ItemName");
                AutoDTStdPur.Columns.Add("UomCode");
                AutoDTStdPur.Columns.Add("RequiredQty");
                AutoDTStdPur.Columns.Add("OrderQty");
                AutoDTStdPur.Columns.Add("RequiredDate");
                AutoDTStdPur.Columns.Add("Rate");
                AutoDTStdPur.Columns.Add("Remarks");
                AutoDTStdPur.Columns.Add("LineTotal");



                for (int i = 0; i < StdPurDT.Rows.Count; i++)
                {
                    AutoDTStdPur.NewRow();
                    AutoDTStdPur.Rows.Add();

                    AutoDTStdPur.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTStdPur.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTStdPur.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTStdPur.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTStdPur.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTStdPur.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDTStdPur.Rows[i]["StdPONo"] = StdPurDT.Rows[i]["Std_PO_No"];
                    AutoDTStdPur.Rows[i]["StdPODate"] = StdPurDT.Rows[i]["Std_PO_Date"];
                    AutoDTStdPur.Rows[i]["SuppName"] = StdPurDT.Rows[i]["Supp_Name"];
                    AutoDTStdPur.Rows[i]["SuppQtnNo"] = StdPurDT.Rows[i]["Supp_Qtn_No"];
                    AutoDTStdPur.Rows[i]["SuppQtnDate"] = StdPurDT.Rows[i]["Supp_Qtn_Date"];
                    AutoDTStdPur.Rows[i]["PurRequestNo"] = StdPurDT.Rows[i]["Pur_Request_No"];
                    AutoDTStdPur.Rows[i]["PurRequestDate"] = StdPurDT.Rows[i]["Pur_Request_Date"];
                    AutoDTStdPur.Rows[i]["DeliveryMode"] = StdPurDT.Rows[i]["DeliveryMode"];
                    AutoDTStdPur.Rows[i]["DeliveryDate"] = StdPurDT.Rows[i]["DeliveryDate"];
                    AutoDTStdPur.Rows[i]["DeliveryAt"] = StdPurDT.Rows[i]["DeliveryAt"];
                    AutoDTStdPur.Rows[i]["PaymentMode"] = StdPurDT.Rows[i]["PaymentMode"];
                    AutoDTStdPur.Rows[i]["DeptName"] = StdPurDT.Rows[i]["DeptName"];
                    AutoDTStdPur.Rows[i]["PaymentTerms"] = StdPurDT.Rows[i]["PaymentTerms"];
                    AutoDTStdPur.Rows[i]["Description"] = StdPurDT.Rows[i]["Description"];
                    AutoDTStdPur.Rows[i]["Note"] = StdPurDT.Rows[i]["Note"];
                    AutoDTStdPur.Rows[i]["Others"] = StdPurDT.Rows[i]["Others"];
                    AutoDTStdPur.Rows[i]["TotalAmt"] = StdPurDT.Rows[i]["TotalAmt"];
                    AutoDTStdPur.Rows[i]["Discount"] = StdPurDT.Rows[i]["Discount"];
                    AutoDTStdPur.Rows[i]["TaxPer"] = StdPurDT.Rows[i]["TaxPer"];
                    AutoDTStdPur.Rows[i]["TaxAmount"] = StdPurDT.Rows[i]["TaxAmount"];
                    AutoDTStdPur.Rows[i]["OtherCharge"] = StdPurDT.Rows[i]["OtherCharge"];
                    AutoDTStdPur.Rows[i]["NetAmount"] = StdPurDT.Rows[i]["NetAmount"];

                    AutoDTStdPur.Rows[i]["ItemCode"] = StdPurDT.Rows[i]["ItemCode"];
                    AutoDTStdPur.Rows[i]["ItemName"] = StdPurDT.Rows[i]["ItemName"];
                    AutoDTStdPur.Rows[i]["UomCode"] = StdPurDT.Rows[i]["UOMCode"];
                    AutoDTStdPur.Rows[i]["RequiredQty"] = StdPurDT.Rows[i]["ReuiredQty"];
                    AutoDTStdPur.Rows[i]["OrderQty"] = StdPurDT.Rows[i]["OrderQty"];
                    AutoDTStdPur.Rows[i]["RequiredDate"] = StdPurDT.Rows[i]["ReuiredDate"];
                    AutoDTStdPur.Rows[i]["Rate"] = StdPurDT.Rows[i]["Rate"];
                    AutoDTStdPur.Rows[i]["Remarks"] = StdPurDT.Rows[i]["Remarks"];
                    AutoDTStdPur.Rows[i]["LineTotal"] = StdPurDT.Rows[i]["LineTotal"];


                }

                //ds.Tables.Add(AutoDTStdPur);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/StandardPurchaseOrderDetails.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;


                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDTStdPur);
                RD_Report.Load(Server.MapPath("~/crystal/StandardPurchaseOrderDetails.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                //DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();


            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //Standard Purchase Order Details Report Code End


    //General Purchase Order Details Report Code Start
    public void General_Purchase_Order_Details_Report()
    {

        if (RptName == "General Purchase Order Details Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            if (FromDate != "" && ToDate != "" && GenPurOrdNo == "")
            {

                SSQL = "select GPM.Gen_PO_No,GPM.Gen_PO_Date,GPM.Supp_Name,GPM.Supp_Qtn_No,";
                SSQL = SSQL + "GPM.Supp_Qtn_Date,GPM.DeliveryMode,GPM.DeliveryDate,GPM.DeliveryAt,";
                SSQL = SSQL + "GPM.PaymentMode,GPM.DeptName,GPM.PaymentTerms,GPM.Description,";
                SSQL = SSQL + "GPM.Note,GPM.Others,GPM.TotalAmt,GPM.Discount,GPM.TaxPer,GPM.TaxAmount,GPM.OtherCharge,GPM.NetAmount,GPS.ItemCode,GPS.ItemName,GPS.UOMCode,";
                SSQL = SSQL + "GPS.OrderQty,GPS.Rate,GPS.Remarks,GPS.LineTotal";
                SSQL = SSQL + " from General_Purchase_Order_Main GPM inner join General_Purchase_Order_Main_Sub GPS ";
                SSQL = SSQL + "on GPM.Gen_PO_No=GPS.Gen_PO_No where CONVERT(DATETIME,GPM.Gen_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GPM.Gen_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And GPM.DeptName='" + DeptName + "'";
                }

                if (SupplierName != "-Select-")
                {
                    SSQL = SSQL + " And GPM.Supp_Name='" + SupplierName + "'";
                }

                if (ItemName != "-select-")
                {
                    SSQL = SSQL + " And GPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GPM.Ccode='" + SessionCcode + "' And GPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GPM.FinYearCode='" + SessionFinYearCode + "' And GPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GPS.Lcode='" + SessionLcode + "' And GPS.FinYearCode='" + SessionFinYearCode + "' And GPM.PO_Status='1'";

                GenPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FromDate != "" && ToDate != "" && GenPurOrdNo != "")
            {

                SSQL = "select GPM.Gen_PO_No,GPM.Gen_PO_Date,GPM.Supp_Name,GPM.Supp_Qtn_No,";
                SSQL = SSQL + "GPM.Supp_Qtn_Date,GPM.DeliveryMode,GPM.DeliveryDate,GPM.DeliveryAt,";
                SSQL = SSQL + "GPM.PaymentMode,GPM.DeptName,GPM.PaymentTerms,GPM.Description,";
                SSQL = SSQL + "GPM.Note,GPM.Others,GPM.TotalAmt,GPM.Discount,GPM.TaxPer,GPM.TaxAmount,GPM.OtherCharge,GPM.NetAmount,GPS.ItemCode,GPS.ItemName,GPS.UOMCode,";
                SSQL = SSQL + "GPS.OrderQty,GPS.Rate,GPS.Remarks,GPS.LineTotal";
                SSQL = SSQL + " from General_Purchase_Order_Main GPM inner join General_Purchase_Order_Main_Sub GPS ";
                SSQL = SSQL + "on GPM.Gen_PO_No=GPS.Gen_PO_No where GPM.Gen_PO_No='" + GenPurOrdNo + "' And CONVERT(DATETIME,GPM.Gen_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GPM.Gen_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (DeptName != "")
                {
                    SSQL = SSQL + " And GPM.DeptName='" + DeptName + "'";
                }

                if (SupplierName != "")
                {
                    SSQL = SSQL + " And GPM.Supp_Name='" + SupplierName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And GPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GPM.Ccode='" + SessionCcode + "' And GPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GPM.FinYearCode='" + SessionFinYearCode + "' And GPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GPS.Lcode='" + SessionLcode + "' And GPS.FinYearCode='" + SessionFinYearCode + "' And GPM.PO_Status='1'";

                GenPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (GenPurOrdNo != "")
            {

                SSQL = "select GPM.Gen_PO_No,GPM.Gen_PO_Date,GPM.Supp_Name,GPM.Supp_Qtn_No,";
                SSQL = SSQL + "GPM.Supp_Qtn_Date,GPM.DeliveryMode,GPM.DeliveryDate,GPM.DeliveryAt,";
                SSQL = SSQL + "GPM.PaymentMode,GPM.DeptName,GPM.PaymentTerms,GPM.Description,";
                SSQL = SSQL + "GPM.Note,GPM.Others,GPM.TotalAmt,GPM.Discount,GPM.TaxPer,GPM.TaxAmount,GPM.OtherCharge,GPM.NetAmount,GPS.ItemCode,GPS.ItemName,GPS.UOMCode,";
                SSQL = SSQL + "GPS.OrderQty,GPS.Rate,GPS.Remarks,GPS.LineTotal";
                SSQL = SSQL + " from General_Purchase_Order_Main GPM inner join General_Purchase_Order_Main_Sub GPS ";
                SSQL = SSQL + "on GPM.Gen_PO_No=GPS.Gen_PO_No where GPM.Gen_PO_No='" + GenPurOrdNo + "'";


                if (DeptName != "")
                {
                    SSQL = SSQL + " And GPM.DeptName='" + DeptName + "'";
                }

                if (SupplierName != "")
                {
                    SSQL = SSQL + " And GPM.Supp_Name='" + SupplierName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And GPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GPM.Ccode='" + SessionCcode + "' And GPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GPM.FinYearCode='" + SessionFinYearCode + "' And GPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GPS.Lcode='" + SessionLcode + "' And GPS.FinYearCode='" + SessionFinYearCode + "' And GPM.PO_Status='1'";

                GenPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (DeptName != "")
            {

                SSQL = "select GPM.Gen_PO_No,GPM.Gen_PO_Date,GPM.Supp_Name,GPM.Supp_Qtn_No,";
                SSQL = SSQL + "GPM.Supp_Qtn_Date,GPM.DeliveryMode,GPM.DeliveryDate,GPM.DeliveryAt,";
                SSQL = SSQL + "GPM.PaymentMode,GPM.DeptName,GPM.PaymentTerms,GPM.Description,";
                SSQL = SSQL + "GPM.Note,GPM.Others,GPM.TotalAmt,GPM.Discount,GPM.TaxPer,GPM.TaxAmount,GPM.OtherCharge,GPM.NetAmount,GPS.ItemCode,GPS.ItemName,GPS.UOMCode,";
                SSQL = SSQL + "GPS.OrderQty,GPS.Rate,GPS.Remarks,GPS.LineTotal";
                SSQL = SSQL + " from General_Purchase_Order_Main GPM inner join General_Purchase_Order_Main_Sub GPS ";
                SSQL = SSQL + "on GPM.Gen_PO_No=GPS.Gen_PO_No where GPM.DeptName='" + DeptName + "'";


                if (GenPurOrdNo != "")
                {
                    SSQL = SSQL + " And GPM.Gen_PO_No='" + GenPurOrdNo + "'";
                }

                if (SupplierName != "")
                {
                    SSQL = SSQL + " And GPM.Supp_Name='" + SupplierName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And GPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GPM.Ccode='" + SessionCcode + "' And GPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GPM.FinYearCode='" + SessionFinYearCode + "' And GPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GPS.Lcode='" + SessionLcode + "' And GPS.FinYearCode='" + SessionFinYearCode + "' And GPM.PO_Status='1'";

                GenPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (SupplierName != "")
            {

                SSQL = "select GPM.Gen_PO_No,GPM.Gen_PO_Date,GPM.Supp_Name,GPM.Supp_Qtn_No,";
                SSQL = SSQL + "GPM.Supp_Qtn_Date,GPM.DeliveryMode,GPM.DeliveryDate,GPM.DeliveryAt,";
                SSQL = SSQL + "GPM.PaymentMode,GPM.DeptName,GPM.PaymentTerms,GPM.Description,";
                SSQL = SSQL + "GPM.Note,GPM.Others,GPM.TotalAmt,GPM.Discount,GPM.TaxPer,GPM.TaxAmount,GPM.OtherCharge,GPM.NetAmount,GPS.ItemCode,GPS.ItemName,GPS.UOMCode,";
                SSQL = SSQL + "GPS.OrderQty,GPS.Rate,GPS.Remarks,GPS.LineTotal";
                SSQL = SSQL + " from General_Purchase_Order_Main GPM inner join General_Purchase_Order_Main_Sub GPS ";
                SSQL = SSQL + "on GPM.Gen_PO_No=GPS.Gen_PO_No where GPM.Supp_Name='" + SupplierName + "'";


                if (GenPurOrdNo != "")
                {
                    SSQL = SSQL + " And GPM.Gen_PO_No='" + GenPurOrdNo + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And GPM.DeptName='" + DeptName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And GPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And GPM.Ccode='" + SessionCcode + "' And GPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GPM.FinYearCode='" + SessionFinYearCode + "' And GPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GPS.Lcode='" + SessionLcode + "' And GPS.FinYearCode='" + SessionFinYearCode + "' And GPM.PO_Status='1'";

                GenPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (ItemName != "")
            {

                SSQL = "select GPM.Gen_PO_No,GPM.Gen_PO_Date,GPM.Supp_Name,GPM.Supp_Qtn_No,";
                SSQL = SSQL + "GPM.Supp_Qtn_Date,GPM.DeliveryMode,GPM.DeliveryDate,GPM.DeliveryAt,";
                SSQL = SSQL + "GPM.PaymentMode,GPM.DeptName,GPM.PaymentTerms,GPM.Description,";
                SSQL = SSQL + "GPM.Note,GPM.Others,GPM.TotalAmt,GPM.Discount,GPM.TaxPer,GPM.TaxAmount,GPM.OtherCharge,GPM.NetAmount,GPS.ItemCode,GPS.ItemName,GPS.UOMCode,";
                SSQL = SSQL + "GPS.OrderQty,GPS.Rate,GPS.Remarks,GPS.LineTotal";
                SSQL = SSQL + " from General_Purchase_Order_Main GPM inner join General_Purchase_Order_Main_Sub GPS ";
                SSQL = SSQL + "on GPM.Gen_PO_No=GPS.Gen_PO_No where GPS.ItemName='" + ItemName + "'";


                if (GenPurOrdNo != "")
                {
                    SSQL = SSQL + " And GPM.Gen_PO_No='" + GenPurOrdNo + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And GPM.DeptName='" + DeptName + "'";
                }

                if (SupplierName != "")
                {
                    SSQL = SSQL + " And GPM.Supp_Name='" + SupplierName + "'";
                }


                SSQL = SSQL + " And GPM.Ccode='" + SessionCcode + "' And GPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GPM.FinYearCode='" + SessionFinYearCode + "' And GPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GPS.Lcode='" + SessionLcode + "' And GPS.FinYearCode='" + SessionFinYearCode + "' And GPM.PO_Status='1'";

                GenPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (GenPurDT.Rows.Count > 0)
            {


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTGenPur.Columns.Add("CompanyName");
                AutoDTGenPur.Columns.Add("LocationName");
                AutoDTGenPur.Columns.Add("CompanyCode");
                AutoDTGenPur.Columns.Add("LocationCode");
                AutoDTGenPur.Columns.Add("Address1");
                AutoDTGenPur.Columns.Add("Address2");
                AutoDTGenPur.Columns.Add("GenPONo");
                AutoDTGenPur.Columns.Add("GenPODate");
                AutoDTGenPur.Columns.Add("SuppName");
                AutoDTGenPur.Columns.Add("SuppQtnNo");
                AutoDTGenPur.Columns.Add("SuppQtnDate");
                AutoDTGenPur.Columns.Add("DeliveryMode");
                AutoDTGenPur.Columns.Add("DeliveryDate");
                AutoDTGenPur.Columns.Add("DeliveryAt");
                AutoDTGenPur.Columns.Add("PaymentMode");
                AutoDTGenPur.Columns.Add("DeptName");
                AutoDTGenPur.Columns.Add("PaymentTerms");
                AutoDTGenPur.Columns.Add("Description");
                AutoDTGenPur.Columns.Add("Note");
                AutoDTGenPur.Columns.Add("Others");
                AutoDTGenPur.Columns.Add("TotalAmt");
                AutoDTGenPur.Columns.Add("Discount");
                AutoDTGenPur.Columns.Add("TaxPer");
                AutoDTGenPur.Columns.Add("TaxAmount");
                AutoDTGenPur.Columns.Add("OtherCharge");
                AutoDTGenPur.Columns.Add("NetAmount");


                AutoDTGenPur.Columns.Add("ItemCode");
                AutoDTGenPur.Columns.Add("ItemName");
                AutoDTGenPur.Columns.Add("UomCode");
                AutoDTGenPur.Columns.Add("OrderQty");
                AutoDTGenPur.Columns.Add("Rate");
                AutoDTGenPur.Columns.Add("Remarks");
                AutoDTGenPur.Columns.Add("LineTotal");



                for (int i = 0; i < GenPurDT.Rows.Count; i++)
                {
                    AutoDTGenPur.NewRow();
                    AutoDTGenPur.Rows.Add();

                    AutoDTGenPur.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTGenPur.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTGenPur.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTGenPur.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTGenPur.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTGenPur.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDTGenPur.Rows[i]["GenPONo"] = GenPurDT.Rows[i]["Gen_PO_No"];
                    AutoDTGenPur.Rows[i]["GenPODate"] = GenPurDT.Rows[i]["Gen_PO_Date"];
                    AutoDTGenPur.Rows[i]["SuppName"] = GenPurDT.Rows[i]["Supp_Name"];
                    AutoDTGenPur.Rows[i]["SuppQtnNo"] = GenPurDT.Rows[i]["Supp_Qtn_No"];
                    AutoDTGenPur.Rows[i]["SuppQtnDate"] = GenPurDT.Rows[i]["Supp_Qtn_Date"];
                    AutoDTGenPur.Rows[i]["DeliveryMode"] = GenPurDT.Rows[i]["DeliveryMode"];
                    AutoDTGenPur.Rows[i]["DeliveryDate"] = GenPurDT.Rows[i]["DeliveryDate"];
                    AutoDTGenPur.Rows[i]["DeliveryAt"] = GenPurDT.Rows[i]["DeliveryAt"];
                    AutoDTGenPur.Rows[i]["PaymentMode"] = GenPurDT.Rows[i]["PaymentMode"];
                    AutoDTGenPur.Rows[i]["DeptName"] = GenPurDT.Rows[i]["DeptName"];
                    AutoDTGenPur.Rows[i]["PaymentTerms"] = GenPurDT.Rows[i]["PaymentTerms"];
                    AutoDTGenPur.Rows[i]["Description"] = GenPurDT.Rows[i]["Description"];
                    AutoDTGenPur.Rows[i]["Note"] = GenPurDT.Rows[i]["Note"];
                    AutoDTGenPur.Rows[i]["Others"] = GenPurDT.Rows[i]["Others"];
                    AutoDTGenPur.Rows[i]["TotalAmt"] = GenPurDT.Rows[i]["TotalAmt"];
                    AutoDTGenPur.Rows[i]["Discount"] = GenPurDT.Rows[i]["Discount"];
                    AutoDTGenPur.Rows[i]["TaxPer"] = GenPurDT.Rows[i]["TaxPer"];
                    AutoDTGenPur.Rows[i]["TaxAmount"] = GenPurDT.Rows[i]["TaxAmount"];
                    AutoDTGenPur.Rows[i]["OtherCharge"] = GenPurDT.Rows[i]["OtherCharge"];
                    AutoDTGenPur.Rows[i]["NetAmount"] = GenPurDT.Rows[i]["NetAmount"];


                    AutoDTGenPur.Rows[i]["ItemCode"] = GenPurDT.Rows[i]["ItemCode"];
                    AutoDTGenPur.Rows[i]["ItemName"] = GenPurDT.Rows[i]["ItemName"];
                    AutoDTGenPur.Rows[i]["UomCode"] = GenPurDT.Rows[i]["UOMCode"];
                    AutoDTGenPur.Rows[i]["OrderQty"] = GenPurDT.Rows[i]["OrderQty"];
                    AutoDTGenPur.Rows[i]["Rate"] = GenPurDT.Rows[i]["Rate"];
                    AutoDTGenPur.Rows[i]["Remarks"] = GenPurDT.Rows[i]["Remarks"];
                    AutoDTGenPur.Rows[i]["LineTotal"] = GenPurDT.Rows[i]["LineTotal"];


                }

                //ds.Tables.Add(AutoDTGenPur);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/GeneralPurchaseOrderDetails.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDTGenPur);
                RD_Report.Load(Server.MapPath("~/crystal/GeneralPurchaseOrderDetails.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add

                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();


            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //General Purchase Order Details Report Code End

    //Opening Receipt Details Report Code Start
    public void Opening_Receipt_Details_Report()
    {
        if (RptName == "Opening Receipt Details Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            if (FromDate != "" && ToDate != "" && OpnRecNo == "")
            {

                SSQL = "select ORM.Open_Recp_No,ORM.Open_Recp_Date,ORM.WarehouseName,";
                SSQL = SSQL + "ORS.ItemCode,ORS.ItemName,ORS.ValuationType,";
                SSQL = SSQL + "ORS.UOMCode,ORS.ZoneName,ORS.BinName,";
                SSQL = SSQL + "ORS.OpenQty,ORS.OpenValue";
                SSQL = SSQL + " from Opening_Receipt_Main ORM inner join Opening_Receipt_Main_Sub ORS ";
                SSQL = SSQL + "on ORM.Open_Recp_No=ORS.Open_Recp_No where CONVERT(DATETIME,ORM.Open_Recp_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,ORM.Open_Recp_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (WareHouse != "")
                {
                    SSQL = SSQL + " And ORM.WarehouseName='" + WareHouse + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And ORS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And ORM.Ccode='" + SessionCcode + "' And ORM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And ORM.FinYearCode='" + SessionFinYearCode + "' And ORS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And ORS.Lcode='" + SessionLcode + "' And ORS.FinYearCode='" + SessionFinYearCode + "' And ORM.OP_Receipt_Status='1'";

                OpenRecpDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FromDate != "" && ToDate != "" && OpnRecNo != "")
            {

                SSQL = "select ORM.Open_Recp_No,ORM.Open_Recp_Date,ORM.WarehouseName,";
                SSQL = SSQL + "ORS.ItemCode,ORS.ItemName,ORS.ValuationType,";
                SSQL = SSQL + "ORS.UOMCode,ORS.ZoneName,ORS.BinName,";
                SSQL = SSQL + "ORS.OpenQty,ORS.OpenValue";
                SSQL = SSQL + " from Opening_Receipt_Main ORM inner join Opening_Receipt_Main_Sub ORS ";
                SSQL = SSQL + "on ORM.Open_Recp_No=ORS.Open_Recp_No where ORM.Open_Recp_No='" + OpnRecNo + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,ORM.Open_Recp_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,ORM.Open_Recp_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And ORM.WarehouseName='" + WareHouse + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And ORS.ItemName='" + ItemName + "'";
                }

                SSQL = SSQL + " And ORM.Ccode='" + SessionCcode + "' And ORM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And ORM.FinYearCode='" + SessionFinYearCode + "' And ORS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And ORS.Lcode='" + SessionLcode + "' And ORS.FinYearCode='" + SessionFinYearCode + "' And ORM.OP_Receipt_Status='1'";

                OpenRecpDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (OpnRecNo != "")
            {
                SSQL = "select ORM.Open_Recp_No,ORM.Open_Recp_Date,ORM.WarehouseName,";
                SSQL = SSQL + "ORS.ItemCode,ORS.ItemName,ORS.ValuationType,";
                SSQL = SSQL + "ORS.UOMCode,ORS.ZoneName,ORS.BinName,";
                SSQL = SSQL + "ORS.OpenQty,ORS.OpenValue";
                SSQL = SSQL + " from Opening_Receipt_Main ORM inner join Opening_Receipt_Main_Sub ORS ";
                SSQL = SSQL + "on ORM.Open_Recp_No=ORS.Open_Recp_No where ORM.Open_Recp_No='" + OpnRecNo + "'";

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And ORM.WarehouseName='" + WareHouse + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And ORS.ItemName='" + ItemName + "'";
                }

                SSQL = SSQL + " And ORM.Ccode='" + SessionCcode + "' And ORM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And ORM.FinYearCode='" + SessionFinYearCode + "' And ORS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And ORS.Lcode='" + SessionLcode + "' And ORS.FinYearCode='" + SessionFinYearCode + "' And ORM.OP_Receipt_Status='1'";

                OpenRecpDT = objdata.RptEmployeeMultipleDetails(SSQL);


            }
            else if (WareHouse != "")
            {

                SSQL = "select ORM.Open_Recp_No,ORM.Open_Recp_Date,ORM.WarehouseName,";
                SSQL = SSQL + "ORS.ItemCode,ORS.ItemName,ORS.ValuationType,";
                SSQL = SSQL + "ORS.UOMCode,ORS.ZoneName,ORS.BinName,";
                SSQL = SSQL + "ORS.OpenQty,ORS.OpenValue";
                SSQL = SSQL + " from Opening_Receipt_Main ORM inner join Opening_Receipt_Main_Sub ORS ";
                SSQL = SSQL + "on ORM.Open_Recp_No=ORS.Open_Recp_No where ORM.WarehouseName='" + WareHouse + "'";

                if (OpnRecNo != "")
                {
                    SSQL = SSQL + " And ORM.Open_Recp_No='" + OpnRecNo + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And ORS.ItemName='" + ItemName + "'";
                }

                SSQL = SSQL + " And ORM.Ccode='" + SessionCcode + "' And ORM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And ORM.FinYearCode='" + SessionFinYearCode + "' And ORS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And ORS.Lcode='" + SessionLcode + "' And ORS.FinYearCode='" + SessionFinYearCode + "' And ORM.OP_Receipt_Status='1'";

                OpenRecpDT = objdata.RptEmployeeMultipleDetails(SSQL);


            }
            else if (ItemName != "")
            {

                SSQL = "select ORM.Open_Recp_No,ORM.Open_Recp_Date,ORM.WarehouseName,";
                SSQL = SSQL + "ORS.ItemCode,ORS.ItemName,ORS.ValuationType,";
                SSQL = SSQL + "ORS.UOMCode,ORS.ZoneName,ORS.BinName,";
                SSQL = SSQL + "ORS.OpenQty,ORS.OpenValue";
                SSQL = SSQL + " from Opening_Receipt_Main ORM inner join Opening_Receipt_Main_Sub ORS ";
                SSQL = SSQL + "on ORM.Open_Recp_No=ORS.Open_Recp_No where ORS.ItemName='" + ItemName + "'";

                if (OpnRecNo != "")
                {
                    SSQL = SSQL + " And ORM.Open_Recp_No='" + OpnRecNo + "'";
                }

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And ORM.WarehouseName='" + WareHouse + "'";
                }

                SSQL = SSQL + " And ORM.Ccode='" + SessionCcode + "' And ORM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And ORM.FinYearCode='" + SessionFinYearCode + "' And ORS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And ORS.Lcode='" + SessionLcode + "' And ORS.FinYearCode='" + SessionFinYearCode + "' And ORM.OP_Receipt_Status='1'";

                OpenRecpDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (OpenRecpDT.Rows.Count > 0)
            {


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



                AutoDTOpnRecp.Columns.Add("CompanyName");
                AutoDTOpnRecp.Columns.Add("LocationName");
                AutoDTOpnRecp.Columns.Add("CompanyCode");
                AutoDTOpnRecp.Columns.Add("LocationCode");
                AutoDTOpnRecp.Columns.Add("Address1");
                AutoDTOpnRecp.Columns.Add("Address2");
                AutoDTOpnRecp.Columns.Add("OpenRecpNo");
                AutoDTOpnRecp.Columns.Add("OpenRecpDate");
                AutoDTOpnRecp.Columns.Add("WarehouseName");


                AutoDTOpnRecp.Columns.Add("ItemCode");
                AutoDTOpnRecp.Columns.Add("ItemName");
                AutoDTOpnRecp.Columns.Add("ValuationType");
                AutoDTOpnRecp.Columns.Add("UomCode");
                AutoDTOpnRecp.Columns.Add("ZoneName");
                AutoDTOpnRecp.Columns.Add("BinName");
                AutoDTOpnRecp.Columns.Add("OpenQty");
                AutoDTOpnRecp.Columns.Add("OpenValue");



                for (int i = 0; i < OpenRecpDT.Rows.Count; i++)
                {
                    AutoDTOpnRecp.NewRow();
                    AutoDTOpnRecp.Rows.Add();

                    AutoDTOpnRecp.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTOpnRecp.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTOpnRecp.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTOpnRecp.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTOpnRecp.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTOpnRecp.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDTOpnRecp.Rows[i]["OpenRecpNo"] = OpenRecpDT.Rows[i]["Open_Recp_No"];
                    AutoDTOpnRecp.Rows[i]["OpenRecpDate"] = OpenRecpDT.Rows[i]["Open_Recp_Date"];
                    AutoDTOpnRecp.Rows[i]["WarehouseName"] = OpenRecpDT.Rows[i]["WarehouseName"];


                    AutoDTOpnRecp.Rows[i]["ItemCode"] = OpenRecpDT.Rows[i]["ItemCode"];
                    AutoDTOpnRecp.Rows[i]["ItemName"] = OpenRecpDT.Rows[i]["ItemName"];
                    AutoDTOpnRecp.Rows[i]["ValuationType"] = OpenRecpDT.Rows[i]["ValuationType"];
                    AutoDTOpnRecp.Rows[i]["UomCode"] = OpenRecpDT.Rows[i]["UOMCode"];
                    AutoDTOpnRecp.Rows[i]["ZoneName"] = OpenRecpDT.Rows[i]["ZoneName"];
                    AutoDTOpnRecp.Rows[i]["BinName"] = OpenRecpDT.Rows[i]["BinName"];
                    AutoDTOpnRecp.Rows[i]["OpenQty"] = OpenRecpDT.Rows[i]["OpenQty"];
                    AutoDTOpnRecp.Rows[i]["OpenValue"] = OpenRecpDT.Rows[i]["OpenValue"];


                }

                ds.Tables.Add(AutoDTOpnRecp);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("~/crystal/OpeningReceiptDetails.rpt"));
                report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = report;

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //Opening Receipt Details Report Code End

    //Blanket Purchase Order Details Report Code Start
    public void Blanket_Purchase_Order_Details()
    {
        if (RptName == "Blanket Purchase Order Details Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            if (FromDate != "" && ToDate != "" && BlanketPONo == "")
            {

                SSQL = "select BPM.Blanket_PO_No,BPM.Blanket_PO_Date,BPM.Supp_Name,";
                SSQL = SSQL + "BPM.Supp_Qtn_No,BPM.Supp_Qtn_Date,BPM.DeliveryMode,";
                SSQL = SSQL + "BPM.DeliveryAt,BPM.PaymentMode,BPM.DeptName,";
                SSQL = SSQL + "BPM.PaymentTerms,BPS.Pur_Request_No,BPS.Pur_Request_Date,";
                SSQL = SSQL + "BPS.ItemCode,BPS.ItemName,BPS.UOMCode,";
                SSQL = SSQL + "BPS.ReuiredQty,BPS.OrderQty,BPS.Rate";
                SSQL = SSQL + " from Blanket_Purchase_Order_Main BPM inner join Blanket_Purchase_Order_Main_Sub BPS ";
                SSQL = SSQL + "on BPM.Blanket_PO_No=BPS.Blanket_PO_No where CONVERT(DATETIME,BPM.Blanket_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,BPM.Blanket_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (DeptName != "")
                {
                    SSQL = SSQL + " And BPM.DeptName='" + DeptName + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + " And BPM.Supp_Name='" + SupplierName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And BPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And BPM.Ccode='" + SessionCcode + "' And BPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And BPM.FinYearCode='" + SessionFinYearCode + "' And BPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And BPS.Lcode='" + SessionLcode + "' And BPS.FinYearCode='" + SessionFinYearCode + "' And BPM.Blanket_PO_Status='1'";

                BlanketPODT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FromDate != "" && ToDate != "" && BlanketPONo != "")
            {

                SSQL = "select BPM.Blanket_PO_No,BPM.Blanket_PO_Date,BPM.Supp_Name,";
                SSQL = SSQL + "BPM.Supp_Qtn_No,BPM.Supp_Qtn_Date,BPM.DeliveryMode,";
                SSQL = SSQL + "BPM.DeliveryAt,BPM.PaymentMode,BPM.DeptName,";
                SSQL = SSQL + "BPM.PaymentTerms,BPS.Pur_Request_No,BPS.Pur_Request_Date,";
                SSQL = SSQL + "BPS.ItemCode,BPS.ItemName,BPS.UOMCode,";
                SSQL = SSQL + "BPS.ReuiredQty,BPS.OrderQty,BPS.Rate";
                SSQL = SSQL + " from Blanket_Purchase_Order_Main BPM inner join Blanket_Purchase_Order_Main_Sub BPS ";
                SSQL = SSQL + "on BPM.Blanket_PO_No=BPS.Blanket_PO_No where BPM.Blanket_PO_No='" + BlanketPONo + "' And CONVERT(DATETIME,BPM.Blanket_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,BPM.Blanket_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (DeptName != "")
                {
                    SSQL = SSQL + " And BPM.DeptName='" + DeptName + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + " And BPM.Supp_Name='" + SupplierName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And BPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And BPM.Ccode='" + SessionCcode + "' And BPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And BPM.FinYearCode='" + SessionFinYearCode + "' And BPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And BPS.Lcode='" + SessionLcode + "' And BPS.FinYearCode='" + SessionFinYearCode + "' And BPM.Blanket_PO_Status='1'";

                BlanketPODT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (BlanketPONo != "")
            {
                SSQL = "select BPM.Blanket_PO_No,BPM.Blanket_PO_Date,BPM.Supp_Name,";
                SSQL = SSQL + "BPM.Supp_Qtn_No,BPM.Supp_Qtn_Date,BPM.DeliveryMode,";
                SSQL = SSQL + "BPM.DeliveryAt,BPM.PaymentMode,BPM.DeptName,";
                SSQL = SSQL + "BPM.PaymentTerms,BPS.Pur_Request_No,BPS.Pur_Request_Date,";
                SSQL = SSQL + "BPS.ItemCode,BPS.ItemName,BPS.UOMCode,";
                SSQL = SSQL + "BPS.ReuiredQty,BPS.OrderQty,BPS.Rate";
                SSQL = SSQL + " from Blanket_Purchase_Order_Main BPM inner join Blanket_Purchase_Order_Main_Sub BPS ";
                SSQL = SSQL + "on BPM.Blanket_PO_No=BPS.Blanket_PO_No where BPM.Blanket_PO_No='" + BlanketPONo + "'";


                if (DeptName != "")
                {
                    SSQL = SSQL + " And BPM.DeptName='" + DeptName + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + " And BPM.Supp_Name='" + SupplierName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And BPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And BPM.Ccode='" + SessionCcode + "' And BPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And BPM.FinYearCode='" + SessionFinYearCode + "' And BPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And BPS.Lcode='" + SessionLcode + "' And BPS.FinYearCode='" + SessionFinYearCode + "' And BPM.Blanket_PO_Status='1'";

                BlanketPODT = objdata.RptEmployeeMultipleDetails(SSQL);


            }
            else if (DeptName != "")
            {
                SSQL = "select BPM.Blanket_PO_No,BPM.Blanket_PO_Date,BPM.Supp_Name,";
                SSQL = SSQL + "BPM.Supp_Qtn_No,BPM.Supp_Qtn_Date,BPM.DeliveryMode,";
                SSQL = SSQL + "BPM.DeliveryAt,BPM.PaymentMode,BPM.DeptName,";
                SSQL = SSQL + "BPM.PaymentTerms,BPS.Pur_Request_No,BPS.Pur_Request_Date,";
                SSQL = SSQL + "BPS.ItemCode,BPS.ItemName,BPS.UOMCode,";
                SSQL = SSQL + "BPS.ReuiredQty,BPS.OrderQty,BPS.Rate";
                SSQL = SSQL + " from Blanket_Purchase_Order_Main BPM inner join Blanket_Purchase_Order_Main_Sub BPS ";
                SSQL = SSQL + "on BPM.Blanket_PO_No=BPS.Blanket_PO_No where BPM.DeptName='" + DeptName + "'";


                if (OpnRecNo != "")
                {
                    SSQL = SSQL + " And BPM.Blanket_PO_No='" + BlanketPONo + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + " And BPM.Supp_Name='" + SupplierName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And BPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And BPM.Ccode='" + SessionCcode + "' And BPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And BPM.FinYearCode='" + SessionFinYearCode + "' And BPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And BPS.Lcode='" + SessionLcode + "' And BPS.FinYearCode='" + SessionFinYearCode + "' And BPM.Blanket_PO_Status='1'";

                BlanketPODT = objdata.RptEmployeeMultipleDetails(SSQL);


            }
            else if (SupplierName != "")
            {
                SSQL = "select BPM.Blanket_PO_No,BPM.Blanket_PO_Date,BPM.Supp_Name,";
                SSQL = SSQL + "BPM.Supp_Qtn_No,BPM.Supp_Qtn_Date,BPM.DeliveryMode,";
                SSQL = SSQL + "BPM.DeliveryAt,BPM.PaymentMode,BPM.DeptName,";
                SSQL = SSQL + "BPM.PaymentTerms,BPS.Pur_Request_No,BPS.Pur_Request_Date,";
                SSQL = SSQL + "BPS.ItemCode,BPS.ItemName,BPS.UOMCode,";
                SSQL = SSQL + "BPS.ReuiredQty,BPS.OrderQty,BPS.Rate";
                SSQL = SSQL + " from Blanket_Purchase_Order_Main BPM inner join Blanket_Purchase_Order_Main_Sub BPS ";
                SSQL = SSQL + "on BPM.Blanket_PO_No=BPS.Blanket_PO_No where BPM.Supp_Name='" + SupplierName + "'";


                if (OpnRecNo != "")
                {
                    SSQL = SSQL + " And BPM.Blanket_PO_No='" + BlanketPONo + "'";
                }
                if (DeptName != "")
                {
                    SSQL = SSQL + " And BPM.DeptName='" + DeptName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And BPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And BPM.Ccode='" + SessionCcode + "' And BPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And BPM.FinYearCode='" + SessionFinYearCode + "' And BPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And BPS.Lcode='" + SessionLcode + "' And BPS.FinYearCode='" + SessionFinYearCode + "' And BPM.Blanket_PO_Status='1'";

                BlanketPODT = objdata.RptEmployeeMultipleDetails(SSQL);


            }
            else if (ItemName != "")
            {

                SSQL = "select BPM.Blanket_PO_No,BPM.Blanket_PO_Date,BPM.Supp_Name,";
                SSQL = SSQL + "BPM.Supp_Qtn_No,BPM.Supp_Qtn_Date,BPM.DeliveryMode,";
                SSQL = SSQL + "BPM.DeliveryAt,BPM.PaymentMode,BPM.DeptName,";
                SSQL = SSQL + "BPM.PaymentTerms,BPS.Pur_Request_No,BPS.Pur_Request_Date,";
                SSQL = SSQL + "BPS.ItemCode,BPS.ItemName,BPS.UOMCode,";
                SSQL = SSQL + "BPS.ReuiredQty,BPS.OrderQty,BPS.Rate";
                SSQL = SSQL + " from Blanket_Purchase_Order_Main BPM inner join Blanket_Purchase_Order_Main_Sub BPS ";
                SSQL = SSQL + "on BPM.Blanket_PO_No=BPS.Blanket_PO_No where BPS.ItemName='" + ItemName + "'";


                if (OpnRecNo != "")
                {
                    SSQL = SSQL + " And BPM.Blanket_PO_No='" + BlanketPONo + "'";
                }
                if (DeptName != "")
                {
                    SSQL = SSQL + " And BPM.DeptName='" + DeptName + "'";
                }

                if (SupplierName != "")
                {
                    SSQL = SSQL + " And BPM.Supp_Name='" + SupplierName + "'";
                }


                SSQL = SSQL + " And BPM.Ccode='" + SessionCcode + "' And BPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And BPM.FinYearCode='" + SessionFinYearCode + "' And BPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And BPS.Lcode='" + SessionLcode + "' And BPS.FinYearCode='" + SessionFinYearCode + "' And BPM.Blanket_PO_Status='1'";

                BlanketPODT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (BlanketPODT.Rows.Count > 0)
            {


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



                AutoDTBlanketPO.Columns.Add("CompanyName");
                AutoDTBlanketPO.Columns.Add("LocationName");
                AutoDTBlanketPO.Columns.Add("CompanyCode");
                AutoDTBlanketPO.Columns.Add("LocationCode");
                AutoDTBlanketPO.Columns.Add("Address1");
                AutoDTBlanketPO.Columns.Add("Address2");
                AutoDTBlanketPO.Columns.Add("BlanketPONo");
                AutoDTBlanketPO.Columns.Add("BlanketPODate");
                AutoDTBlanketPO.Columns.Add("SuppName");
                AutoDTBlanketPO.Columns.Add("SuppQtnNo");
                AutoDTBlanketPO.Columns.Add("SuppQtnDate");
                AutoDTBlanketPO.Columns.Add("DeliveryMode");
                AutoDTBlanketPO.Columns.Add("DeliveryAt");
                AutoDTBlanketPO.Columns.Add("PaymentMode");
                AutoDTBlanketPO.Columns.Add("DeptName");
                AutoDTBlanketPO.Columns.Add("PaymentTerms");

                AutoDTBlanketPO.Columns.Add("PurRequestNo");
                AutoDTBlanketPO.Columns.Add("PurRequestDate");
                AutoDTBlanketPO.Columns.Add("ItemCode");
                AutoDTBlanketPO.Columns.Add("ItemName");
                AutoDTBlanketPO.Columns.Add("UomCode");
                AutoDTBlanketPO.Columns.Add("RequiredQty");
                AutoDTBlanketPO.Columns.Add("OrderQty");
                AutoDTBlanketPO.Columns.Add("Rate");



                for (int i = 0; i < BlanketPODT.Rows.Count; i++)
                {
                    AutoDTBlanketPO.NewRow();
                    AutoDTBlanketPO.Rows.Add();

                    AutoDTBlanketPO.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTBlanketPO.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTBlanketPO.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTBlanketPO.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTBlanketPO.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTBlanketPO.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDTBlanketPO.Rows[i]["BlanketPONo"] = BlanketPODT.Rows[i]["Blanket_PO_No"];
                    AutoDTBlanketPO.Rows[i]["BlanketPODate"] = BlanketPODT.Rows[i]["Blanket_PO_Date"];
                    AutoDTBlanketPO.Rows[i]["SuppName"] = BlanketPODT.Rows[i]["Supp_Name"];
                    AutoDTBlanketPO.Rows[i]["SuppQtnNo"] = BlanketPODT.Rows[i]["Supp_Qtn_No"];
                    AutoDTBlanketPO.Rows[i]["SuppQtnDate"] = BlanketPODT.Rows[i]["Supp_Qtn_Date"];
                    AutoDTBlanketPO.Rows[i]["DeliveryMode"] = BlanketPODT.Rows[i]["DeliveryMode"];
                    AutoDTBlanketPO.Rows[i]["DeliveryAt"] = BlanketPODT.Rows[i]["DeliveryAt"];
                    AutoDTBlanketPO.Rows[i]["PaymentMode"] = BlanketPODT.Rows[i]["PaymentMode"];
                    AutoDTBlanketPO.Rows[i]["DeptName"] = BlanketPODT.Rows[i]["DeptName"];
                    AutoDTBlanketPO.Rows[i]["PaymentTerms"] = BlanketPODT.Rows[i]["PaymentTerms"];


                    AutoDTBlanketPO.Rows[i]["PurRequestNo"] = BlanketPODT.Rows[i]["Pur_Request_No"];
                    AutoDTBlanketPO.Rows[i]["PurRequestDate"] = BlanketPODT.Rows[i]["Pur_Request_Date"];
                    AutoDTBlanketPO.Rows[i]["ItemCode"] = BlanketPODT.Rows[i]["ItemCode"];
                    AutoDTBlanketPO.Rows[i]["ItemName"] = BlanketPODT.Rows[i]["ItemName"];
                    AutoDTBlanketPO.Rows[i]["UomCode"] = BlanketPODT.Rows[i]["UOMCode"];
                    AutoDTBlanketPO.Rows[i]["RequiredQty"] = BlanketPODT.Rows[i]["ReuiredQty"];
                    AutoDTBlanketPO.Rows[i]["OrderQty"] = BlanketPODT.Rows[i]["OrderQty"];
                    AutoDTBlanketPO.Rows[i]["Rate"] = BlanketPODT.Rows[i]["Rate"];



                }

                //ds.Tables.Add(AutoDTBlanketPO);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/BlanketPurchaseOrderDetails.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;



                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDTBlanketPO);
                RD_Report.Load(Server.MapPath("~/crystal/BlanketPurchaseOrderDetails.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add

                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //Blanket Purchase Order Details Report Code End

    //Purchase Order Receipt Details Report Code Start
    public void Purchase_Order_Receipt_Details_Report()
    {
        DataTable DT = new DataTable();


        if (ButtonName == "Details Report")
        {
            SSQL = "Select CMP.Cname As [CompanyName],PRM.PO_Receipt_No as [PORNO],PRM.PO_Receipt_Date as [PORDATE],PRM.InvNo as [INVNO],PRM.InvDate as [INVDATE],PRM.GPINNo as [DCNO],";
            SSQL = SSQL + "PRM.GPINDate as [DCDATE],SUP.SuppName as [SUPPNAME],PRM.DeptName as [DEPTNAME],PRM.WarehouseName as [WAREHOUSENAME],PRS.ZoneName as [ZONENAME],";
            SSQL = SSQL + "PRS.BinName as [BINNAME],PRM.Others as [REMARKS],PRS.Pur_Order_No as [ORDNO],PRS.Pur_Order_Date as [ORDDATE],PRS.ItemCode as [ITEMCODE],";
            SSQL = SSQL + "PRS.ItemName as [ITEMNAME],PRS.UOMCode as[UOM],PRS.OrderQty as [ORDQTY],PRS.ReceivedQty as [QTY],PRS.ItemRate as [RATE],PRS.ItemTotal as [VALUE],";
            SSQL = SSQL + "PRS.PackingAmt as [PACKINGAMT] ,PRS.Discount_Per as [DISCPER],PRS.Discount as [DISCAMT],PRS.TaxPer,PRS.TaxAmount,PRS.BDUTaxPer,PRS.BDUTaxAmount,";
            SSQL = SSQL + "PRS.OtherCharge,isnull((PRS.CGSTPer), 0) as [CGSTPER],isnull((PRS.CGSTAmount), 0) as [CGSTAmt],isnull((PRS.SGSTPer), 0) as [SGSTPER],";
            SSQL = SSQL + "isnull((PRS.SGSTAmount), 0) as [SGSTAmt],isnull((PRS.IGSTPer), 0) as [IGSTPER],isnull((PRS.IGSTAmount), 0) as [IGSTAmt],PRS.LineTotal as [ITEMTOTAL],";
            SSQL = SSQL + "PRM.TotalAmt as [SUBTOTAL],PRM.TotPackAmt as [TOTALPACKING],PRM.AddOrLess as [OTHERAMT],PRM.NetAmount as[NETAMT],";
            SSQL = SSQL + "'" + FromDate.ToString() + "' as [Address1],'" + ToDate.ToString() + "' as [Address2]  from Pur_Order_Receipt_Main PRM ";
            SSQL = SSQL + "Inner join Pur_Order_Receipt_Main_Sub PRS on PRM.PO_Receipt_No = PRS.PO_Receipt_No ";
            SSQL = SSQL + "Inner join MstSupplier SUP on SUP.SuppCode = PRM.Supp_Code ";
            SSQL = SSQL + "Inner join AdminRights CMP on CMP.Ccode = PRM.Ccode ";
            SSQL = SSQL + "Where PRM.Ccode='" + SessionCcode + "' And PRM.Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + "And PRM.FinYearCode='" + SessionFinYearCode + "' And PRS.Ccode='" + SessionCcode + "' ";
        }

        else if (ButtonName == "Dept Wise Report")
        {
            SSQL = "Select CMP.Cname As [CmpName],PRM.DeptName as [DEPTNAME],sum(PRS.ReceivedQty) as [QTY],sum(PRS.ItemTotal) as [VALUE],'" + FromDate.ToString() + "' as [FDate],";
            SSQL = SSQL + "'" + ToDate.ToString() + "' as [TDate] from Pur_Order_Receipt_Main PRM ";
            SSQL = SSQL + "Inner join Pur_Order_Receipt_Main_Sub PRS on PRM.PO_Receipt_No = PRS.PO_Receipt_No ";
            SSQL = SSQL + "Inner join MstSupplier SUP on SUP.SuppCode = PRM.Supp_Code ";
            SSQL = SSQL + "Inner join AdminRights CMP on CMP.Ccode = PRM.Ccode ";
            SSQL = SSQL + "Where PRM.Ccode='" + SessionCcode + "' And PRM.Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + "And PRM.FinYearCode='" + SessionFinYearCode + "' And PRS.Ccode='" + SessionCcode + "' ";
        }
        else if (ButtonName == "Pur Ord Slip Report")
        {

            // ,   PRM.Others as [REMARKS],
            SSQL = "select Cmp.Ccode As [CmpCode],Cmp.Cname As [CmpName],Cmp.lcode As [LocCode],Cmp.Location as [CmpLoc],";
            SSQL = SSQL + " Cmp.Address1 as [CmpAddr1],Cmp.Address2 as [CmpAddr2],Cmp.Pincode as [CmpPin],Cmp.Phone as [CmpMob],";
            SSQL = SSQL + " Cmp.GSTNO as [CmpGST],PRM.PO_Receipt_No as [UnPlanRecpNo],PRM.PO_Receipt_Date as [UnPlanRecpDate],";
            SSQL = SSQL + " SUP.SuppName as [SUPPNAME],PRM.InvNo as [INVNO],PRM.InvDate as [INVDATE],PRM.GPINNo as [DCNO],";
            SSQL = SSQL + " PRM.GPINDate as [DCDATE],PRM.Others[Description],PRM.WarehouseName as [WAREHOUSENAME],";
            SSQL = SSQL + " PRS.ItemCode as [ITEMCODE],PRS.ItemName as [ITEMNAME],SUP.CstNo[CSTNo], ";
            SSQL = SSQL + " SUP.Address1 as [SUPPADDR1],SUP.Address2 as [SUPPADDR2],SUP.City as [SUPCITY] ,";
            SSQL = SSQL + " PRS.Pur_Order_No as [PurOrderNo],PRS.Pur_Order_Date as [PurOrderDate], ";
            SSQL = SSQL + " PRS.UOMCode as[UomCode],PRM.DeptName as [DeptName] ,PRS.ZoneName as [ZONENAME],PRS.BinName as [BINNAME],";
            SSQL = SSQL + " Sup.Pincode as [SUPPIN],SUP.MobileNo as [SUPMOBI],PRS.OrderQty as [ORDQTY],PRS.ReceivedQty as [ReceivedQty],";
            SSQL = SSQL + " PRS.ItemRate as [ItemRate],PRS.ItemTotal as [ItemTotal],PRS.PackingAmt as [PACKINGAMT] ,PRS.Discount_Per as [Discount_Per],";
            SSQL = SSQL + " PRS.Discount as [Discount],PRS.BDUTaxPer,PRS.BDUTaxAmount,PRS.OtherCharge,PRS.LineTotal as [LineTotal],";
            SSQL = SSQL + " PRM.AddOrLess as [AddOrLess],PRM.NetAmount as[NetAmount],isnull((PRS.CGSTPer), 0) as [CGSTPER],";
            SSQL = SSQL + " isnull((PRS.CGSTAmount), 0) as [CGSTAmount],isnull((PRS.SGSTPer), 0) as [SGSTPER],";
            SSQL = SSQL + " isnull((PRS.SGSTAmount), 0) as [SGSTAmount],isnull((PRS.IGSTPer), 0) as [IGSTPER],";
            SSQL = SSQL + " isnull((PRS.IGSTAmount), 0) as [IGSTAmount],PRM.TotalAmt as [SUBTOTAL],PRM.TotPackAmt as [TaxAmount],PRM.TotOtherAmt[TaxPer] ";
            SSQL = SSQL + " From Pur_Order_Receipt_Main PRM Inner join Pur_Order_Receipt_Main_Sub PRS on PRM.PO_Receipt_No = PRS.PO_Receipt_No and PRM.Ccode=PRS.Ccode and PRM.Lcode=PRS.Lcode ";
            SSQL = SSQL + " Inner join MstSupplier SUP on SUP.SuppCode = PRM.Supp_Code and SUP.Ccode=PRM.Ccode and SUP.Lcode=PRM.Lcode ";
            SSQL = SSQL + " Inner join AdminRights Cmp on Cmp.Ccode = PRM.Ccode and Cmp.LCode=PRM.Lcode ";
            SSQL = SSQL + " Where PRM.Ccode='" + SessionCcode + "' And PRM.Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + " And PRM.FinYearCode='" + SessionFinYearCode + "' And PRS.Ccode='" + SessionCcode + "' ";

        }

        else if (ButtonName == "Item Wise Report")
        {
            SSQL = "select Cmp.Cname As [CmpName],PRS.ItemName as [ITEMNAME],PRS.ItemCode as [ItemCode],PRS.BinName as [BinName],";
            SSQL = SSQL + "sum(PRS.ReceivedQty) as [QTY],sum(PRS.LineTotal) as [VALUE],'" + FromDate.ToString() + "' as [FDate],";
            SSQL = SSQL + "'" + ToDate.ToString() + "' as [TDate] from Pur_Order_Receipt_Main PRM ";
            SSQL = SSQL + "Inner join Pur_Order_Receipt_Main_Sub PRS on PRM.PO_Receipt_No = PRS.PO_Receipt_No ";
            SSQL = SSQL + "Inner join AdminRights Cmp on Cmp.Ccode = PRM.Ccode ";
            SSQL = SSQL + "Where PRM.Ccode='" + SessionCcode + "' And PRM.Lcode='" + SessionLcode + "' ";
            SSQL = SSQL + "And PRM.FinYearCode='" + SessionFinYearCode + "' And PRS.Ccode='" + SessionCcode + "' ";
        }

        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,PRM.PO_Receipt_Date, 103)>=CONVERT(DATETIME,'" + FromDate.ToString() + "',103) And CONVERT(DATETIME,PRM.PO_Receipt_Date, 103)<=CONVERT(DATETIME,'" + ToDate.ToString() + "',103)";
        }

        if (PORNumber != "-Select-")
        {
            SSQL = SSQL + " And PRM.PO_Receipt_No='" + PORNumber + "'";
        }
        if (SupplierName != "-Select-")
        {
            SSQL = SSQL + " And PRM.Supp_Name='" + SupplierName + "'";
        }
        if (ItemName != "-Select-")
        {
            SSQL = SSQL + " And PRS.ItemCode='" + ItemName + "'";
        }
        if (DeptName != "-Select-")
        {
            SSQL = SSQL + " And PRM.DeptName='" + DeptName + "'";
        }
        if (WareHouse != "-Select-")
        {
            SSQL = SSQL + " And PRM.WarehouseName='" + WareHouse + "'";
        }

        if (ButtonName == "Dept Wise Report")
        {
            SSQL = SSQL + "  Group by CMP.CName,PRM.DeptName";
        }

        if (ButtonName == "Item Wise Report")
        {
            SSQL = SSQL + "  Group by CMP.CName,PRS.ItemName,PRS.BinName,PRS.ItemCode";
        }

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {

            ReportDocument report = new ReportDocument();

            if (ButtonName == "Details Report")
            {
                report.Load(Server.MapPath("~/crystal/PurchaseOrderReceiptDetails.rpt"));
            }

            else if (ButtonName == "Dept Wise Report")
            {
                report.Load(Server.MapPath("~/crystal/PurchaseOrderReceiptDeptWise.rpt"));
            }

            else if (ButtonName == "Item Wise Report")
            {
                report.Load(Server.MapPath("~/crystal/PurchaseOrderReceiptItemWise.rpt"));
            }

            else if (ButtonName == "Pur Ord Slip Report")
            {
                DataTable DtTst = new DataTable();
                DataRow dr;

                DtTst = DT;

                if (DtTst.Rows.Count < 9)
                {
                    int Row_Count = DtTst.Rows.Count;
                    for (int i = Row_Count; i < 9; i++)
                    {
                        DtTst.NewRow();
                        DtTst.Rows.Add();
                        DtTst.Rows[i]["UnPlanRecpNo"] = DtTst.Rows[0]["UnPlanRecpNo"].ToString();
                        DtTst.Rows[i]["CmpAddr1"] = DtTst.Rows[0]["CmpAddr1"].ToString();
                        DtTst.Rows[i]["CmpAddr2"] = DtTst.Rows[0]["CmpAddr2"].ToString();
                        DtTst.Rows[i]["CmpMob"] = DtTst.Rows[0]["CmpMob"].ToString();
                        DtTst.Rows[i]["CmpGST"] = DtTst.Rows[0]["CmpGST"].ToString();
                        DtTst.Rows[i]["CmpName"] = DtTst.Rows[0]["CmpName"].ToString();
                        DtTst.Rows[i]["CmpPin"] = DtTst.Rows[0]["CmpPin"].ToString();
                        //AutoDTGenReceipt.Rows[i]["Discount_Per"] = "0.0";
                        //AutoDTGenReceipt.Rows[i]["Discount"] = "0.0";
                        DtTst.Rows[i]["TaxPer"] = "0.0";
                        DtTst.Rows[i]["TaxAmount"] = "0.0";
                        DtTst.Rows[i]["BDUTaxPer"] = "0.0";
                        DtTst.Rows[i]["BDUTaxAmount"] = "0.0";
                        DtTst.Rows[i]["OtherCharge"] = "0.0";
                        DtTst.Rows[i]["LineTotal"] = DtTst.Rows[i]["LineTotal"];
                        //DtTst.Rows[i]["TotalAmt"] = AutoDTPOReceipt.Rows[0]["TotalAmt"];
                        DtTst.Rows[i]["AddOrLess"] = DtTst.Rows[0]["AddOrLess"];

                        DtTst.Rows[i]["Description"] = DtTst.Rows[0]["Description"];


                        //TotalPacking Amt
                        DtTst.Rows[i]["TaxAmount"] = DtTst.Rows[0]["TaxAmount"];
                        //******************************

                        //TotalOther Amt
                        DtTst.Rows[i]["TaxPer"] = DtTst.Rows[0]["TaxPer"];
                        //******************************

                        DtTst.Rows[i]["NetAmount"] = DtTst.Rows[0]["NetAmount"];
                    }

                }


                report.Load(Server.MapPath("~/crystal/PurchaseOrderReceiptSlipNew_Two_Copy.rpt"));
                DT = DtTst;
            }
            ds.Tables.Add(DT);
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    //Purchase Order Receipt Details Report Code End

    public void Purchase_Combain_Receipt_Details_Report()
    {
        DataTable DT = new DataTable();
        
        //
        SSQL = "Select A.ItemCode,A.ItemName,A.BinName,SUM(A.Qty)Qty,SUM(A.Value)Value,'" + FromDate.ToString() + "' as [FDate],'" + ToDate.ToString() + "' as [TDate] from ( ";

        // General Purchase Order
        SSQL = SSQL + "select PRS.ItemName as [ITEMNAME],PRS.ItemCode as [ItemCode],PRS.BinName as [BinName],";
        SSQL = SSQL + "sum(PRS.ReceivedQty) as [QTY],sum(PRS.LineTotal) as [VALUE] from Pur_Order_Receipt_Main PRM ";
        SSQL = SSQL + " Inner join Pur_Order_Receipt_Main_Sub PRS on PRM.PO_Receipt_No = PRS.PO_Receipt_No ";
        SSQL = SSQL + "Where PRM.Ccode='" + SessionCcode + "' And PRM.Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + "And PRM.FinYearCode='" + SessionFinYearCode + "' And PRS.Ccode='" + SessionCcode + "' ";

        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,PRM.PO_Receipt_Date, 103)>=CONVERT(DATETIME,'" + FromDate.ToString() + "',103) And CONVERT(DATETIME,PRM.PO_Receipt_Date, 103)<=CONVERT(DATETIME,'" + ToDate.ToString() + "',103)";
        }

        //if (PORNumber != "-Select-")
        //{
        //    SSQL = SSQL + " And PRM.PO_Receipt_No='" + PORNumber + "'";
        //}
        if (SupplierName != "-Select-")
        {
            SSQL = SSQL + " And PRM.Supp_Name='" + SupplierName + "'";
        }
        if (ItemName != "-Select-")
        {
            SSQL = SSQL + " And PRS.ItemCode='" + ItemName + "'";
        }
        if (DeptName != "-Select-")
        {
            SSQL = SSQL + " And PRM.DeptName='" + DeptName + "'";
        }
        if (WareHouse != "-Select-")
        {
            SSQL = SSQL + " And PRM.WarehouseName='" + WareHouse + "'";
        }

        SSQL = SSQL + " Group by ItemCode,ItemName,BinName ";

        SSQL = SSQL + " Union All ";

        // UnPlaned Purchase Recipts
        
        SSQL = SSQL + "select PRS.ItemName as [ITEMNAME],PRS.ItemCode as [ItemCode],PRS.BinName as [BinName],";
        SSQL = SSQL + "sum(PRS.ReceivedQty) as [QTY],sum(PRS.LineTotal) as [VALUE] from Unplanned_Receipt_Main PRM ";
        SSQL = SSQL + " Inner join Unplanned_Receipt_Main_Sub PRS on PRM.UnPlan_Recp_No = PRS.UnPlan_Recp_No ";
        SSQL = SSQL + "Where PRM.Ccode='" + SessionCcode + "' And PRM.Lcode='" + SessionLcode + "' ";
        SSQL = SSQL + "And PRM.FinYearCode='" + SessionFinYearCode + "' And PRS.Ccode='" + SessionCcode + "' ";

        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,PRM.UnPlan_Recp_Date, 103)>=CONVERT(DATETIME,'" + FromDate.ToString() + "',103) And CONVERT(DATETIME,PRM.UnPlan_Recp_Date, 103)<=CONVERT(DATETIME,'" + ToDate.ToString() + "',103)";
        }

        //if (PORNumber != "-Select-")
        //{
        //    SSQL = SSQL + " And PRM.PO_Receipt_No='" + PORNumber + "'";
        //}
        if (SupplierName != "-Select-")
        {
            SSQL = SSQL + " And PRM.Supp_Name='" + SupplierName + "'";
        }
        if (ItemName != "-Select-")
        {
            SSQL = SSQL + " And PRS.ItemCode='" + ItemName + "'";
        }
        if (DeptName != "-Select-")
        {
            SSQL = SSQL + " And PRM.DeptName='" + DeptName + "'";
        }
        if (WareHouse != "-Select-")
        {
            SSQL = SSQL + " And PRM.WarehouseName='" + WareHouse + "'";
        }

        SSQL = SSQL + " Group by ItemCode,ItemName,BinName ) A Group by A.ItemCode,A.ItemName,A.BinName ";

        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count > 0)
        {

            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("~/crystal/PurchaseOrderReceiptItemWise.rpt"));
            ds.Tables.Add(DT);
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }
    public void Sample_Purchase_Report()
    {
        string Heading = "ALL";

        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }

        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select UPM.UnPlan_Recp_No,UPM.UnPlan_Recp_Date,UPM.Supp_Name,UPM.InvNo,UPM.InvDate,";
        SSQL = SSQL + "UPM.DCNo,UPM.DCDate,UPM.Description,UPM.TotalAmt,UPM.AddOrLess,UPM.NetAmount,";
        SSQL = SSQL + "UPS.ItemCode,UPS.ItemName,UPS.ValuationType,UPS.UOMCode,UPS.DeptName,UPS.WarehouseName,";
        SSQL = SSQL + "UPS.ZoneName,UPS.BinName,UPS.ReceivedQty,UPS.PrevRate,UPS.ItemRate,";
        SSQL = SSQL + "UPS.ItemTotal,UPS.Discount_Per,UPS.Discount,UPS.TaxPer,UPS.TaxAmount,UPS.BDUTaxPer,UPS.BDUTaxAmount,UPS.OtherCharge,UPS.LineTotal";
        SSQL = SSQL + " from Unplanned_Receipt_Main UPM inner join Unplanned_Receipt_Main_Sub UPS ";
        SSQL = SSQL + "on UPM.UnPlan_Recp_No=UPS.UnPlan_Recp_No where UPM.Ccode='" + SessionCcode + "' And UPM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And UPM.FinYearCode='" + SessionFinYearCode + "' And UPS.Ccode='" + SessionCcode + "'";
        SSQL = SSQL + " And UPS.Lcode='" + SessionLcode + "' And UPS.FinYearCode='" + SessionFinYearCode + "' And UPM.UnPlan_PO_Receipt_Status='1' ";

        if (FromDate != "" && ToDate != "")
        {
            string DateFormat = FromDate + "   ToDate :" + ToDate;
            SSQL = SSQL + " And CONVERT(DATETIME,UPM.UnPlan_Recp_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,UPM.UnPlan_Recp_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            Heading = "FromDate : " + DateFormat;
        }
        if (WareHouse != "-Select-")
        {
            SSQL = SSQL + " And UPS.WarehouseName='" + WareHouse + "'";
            Heading = "WarehouseName : " + WareHouse;
        }
        if (SupplierName != "-Select-")
        {
            SSQL = SSQL + " And UPM.Supp_Name='" + SupplierName + "'";
            Heading = "SupplierName : " + SupplierName;
        }

        if (ItemName != "-Select-")
        {
            SSQL = SSQL + " And UPS.ItemName='" + ItemName + "'";
            Heading = "ItemName : " + ItemName;
        }

        if (UPRNumber != "")
        {
            SSQL = SSQL + " And UPM.UnPlan_Recp_No='" + UPRNumber + "'";
            Heading = "Recp_No : " + UPRNumber;
        }
        if (CostCenterName != "-Select-")
        {
            SSQL = SSQL + " And UPM.CostCenterName='" + CostCenterName + "'";
            Heading = "CostCenterName : " + CostCenterName;
        }
        if (DeptName != "-Select-")
        {
            SSQL = SSQL + " And UPS.DeptName='" + DeptName + "'";
            Heading = "DeptName : " + DeptName;
        }


        SSQL = SSQL + " order by  UnPlan_Recp_Date";

        GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (GenReceiptDT.Rows.Count > 0)
        {


            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

            AutoDTGenReceipt.Columns.Add("CompanyName");
            AutoDTGenReceipt.Columns.Add("LocationName");
            AutoDTGenReceipt.Columns.Add("CompanyCode");
            AutoDTGenReceipt.Columns.Add("LocationCode");
            AutoDTGenReceipt.Columns.Add("Address1");
            AutoDTGenReceipt.Columns.Add("Address2");
            AutoDTGenReceipt.Columns.Add("UnPlanRecpNo");
            AutoDTGenReceipt.Columns.Add("UnPlanRecpDate");
            AutoDTGenReceipt.Columns.Add("SuppName");
            AutoDTGenReceipt.Columns.Add("InvNo");
            AutoDTGenReceipt.Columns.Add("InvDate");
            AutoDTGenReceipt.Columns.Add("DCNo");
            AutoDTGenReceipt.Columns.Add("DCDate");
            AutoDTGenReceipt.Columns.Add("WarehouseName");
            AutoDTGenReceipt.Columns.Add("Description");

            AutoDTGenReceipt.Columns.Add("ItemCode");
            AutoDTGenReceipt.Columns.Add("ItemName");
            AutoDTGenReceipt.Columns.Add("ValuationType");
            AutoDTGenReceipt.Columns.Add("UomCode");
            AutoDTGenReceipt.Columns.Add("DeptName");
            AutoDTGenReceipt.Columns.Add("ZoneName");
            AutoDTGenReceipt.Columns.Add("BinName");
            AutoDTGenReceipt.Columns.Add("ReceivedQty");
            AutoDTGenReceipt.Columns.Add("PrevRate");
            AutoDTGenReceipt.Columns.Add("ItemRate");
            AutoDTGenReceipt.Columns.Add("ItemTotal");
            AutoDTGenReceipt.Columns.Add("Discount_Per");
            AutoDTGenReceipt.Columns.Add("Discount");
            AutoDTGenReceipt.Columns.Add("TaxPer");
            AutoDTGenReceipt.Columns.Add("TaxAmount");
            AutoDTGenReceipt.Columns.Add("BDUTaxPer");
            AutoDTGenReceipt.Columns.Add("BDUTaxAmount");
            AutoDTGenReceipt.Columns.Add("OtherCharge");
            AutoDTGenReceipt.Columns.Add("LineTotal");
            AutoDTGenReceipt.Columns.Add("TotalAmt");

            AutoDTGenReceipt.Columns.Add("AddOrLess");
            AutoDTGenReceipt.Columns.Add("NetAmount");


            string Receipt_No = "";
            for (int i = 0; i < GenReceiptDT.Rows.Count; i++)
            {
                AutoDTGenReceipt.NewRow();
                AutoDTGenReceipt.Rows.Add();

                AutoDTGenReceipt.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                AutoDTGenReceipt.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                AutoDTGenReceipt.Rows[i]["CompanyCode"] = SessionCcode;
                AutoDTGenReceipt.Rows[i]["LocationCode"] = SessionLcode;
                AutoDTGenReceipt.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                AutoDTGenReceipt.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                Receipt_No = GenReceiptDT.Rows[i]["UnPlan_Recp_No"].ToString();

                AutoDTGenReceipt.Rows[i]["UnPlanRecpNo"] = GenReceiptDT.Rows[i]["UnPlan_Recp_No"];
                AutoDTGenReceipt.Rows[i]["UnPlanRecpDate"] = GenReceiptDT.Rows[i]["UnPlan_Recp_Date"];
                AutoDTGenReceipt.Rows[i]["SuppName"] = GenReceiptDT.Rows[i]["Supp_Name"];
                AutoDTGenReceipt.Rows[i]["InvNo"] = GenReceiptDT.Rows[i]["InvNo"];
                AutoDTGenReceipt.Rows[i]["InvDate"] = GenReceiptDT.Rows[i]["InvDate"];
                AutoDTGenReceipt.Rows[i]["DCNo"] = GenReceiptDT.Rows[i]["DCNo"];
                AutoDTGenReceipt.Rows[i]["DCDate"] = GenReceiptDT.Rows[i]["DCDate"];
                AutoDTGenReceipt.Rows[i]["WarehouseName"] = GenReceiptDT.Rows[i]["WarehouseName"];
                AutoDTGenReceipt.Rows[i]["Description"] = GenReceiptDT.Rows[i]["Description"];


                AutoDTGenReceipt.Rows[i]["ItemCode"] = GenReceiptDT.Rows[i]["ItemCode"];
                AutoDTGenReceipt.Rows[i]["ItemName"] = GenReceiptDT.Rows[i]["ItemName"];
                AutoDTGenReceipt.Rows[i]["ValuationType"] = GenReceiptDT.Rows[i]["ValuationType"];
                AutoDTGenReceipt.Rows[i]["UomCode"] = GenReceiptDT.Rows[i]["UOMCode"];
                AutoDTGenReceipt.Rows[i]["DeptName"] = GenReceiptDT.Rows[i]["DeptName"];
                AutoDTGenReceipt.Rows[i]["ZoneName"] = GenReceiptDT.Rows[i]["ZoneName"];
                AutoDTGenReceipt.Rows[i]["BinName"] = GenReceiptDT.Rows[i]["BinName"];
                AutoDTGenReceipt.Rows[i]["ReceivedQty"] = GenReceiptDT.Rows[i]["ReceivedQty"];
                AutoDTGenReceipt.Rows[i]["PrevRate"] = GenReceiptDT.Rows[i]["PrevRate"];
                AutoDTGenReceipt.Rows[i]["ItemRate"] = GenReceiptDT.Rows[i]["ItemRate"];
                AutoDTGenReceipt.Rows[i]["ItemTotal"] = GenReceiptDT.Rows[i]["ItemTotal"];
                AutoDTGenReceipt.Rows[i]["Discount_Per"] = GenReceiptDT.Rows[i]["Discount_Per"];
                AutoDTGenReceipt.Rows[i]["Discount"] = GenReceiptDT.Rows[i]["Discount"];
                AutoDTGenReceipt.Rows[i]["TaxPer"] = GenReceiptDT.Rows[i]["TaxPer"];
                AutoDTGenReceipt.Rows[i]["TaxAmount"] = GenReceiptDT.Rows[i]["TaxAmount"];
                AutoDTGenReceipt.Rows[i]["BDUTaxPer"] = GenReceiptDT.Rows[i]["BDUTaxPer"];
                AutoDTGenReceipt.Rows[i]["BDUTaxAmount"] = GenReceiptDT.Rows[i]["BDUTaxAmount"];
                AutoDTGenReceipt.Rows[i]["OtherCharge"] = GenReceiptDT.Rows[i]["OtherCharge"];
                AutoDTGenReceipt.Rows[i]["LineTotal"] = GenReceiptDT.Rows[i]["LineTotal"];
                AutoDTGenReceipt.Rows[i]["TotalAmt"] = GenReceiptDT.Rows[i]["TotalAmt"];

                AutoDTGenReceipt.Rows[i]["AddOrLess"] = GenReceiptDT.Rows[i]["AddOrLess"];
                AutoDTGenReceipt.Rows[i]["NetAmount"] = GenReceiptDT.Rows[i]["NetAmount"];

            }


            //if (AutoDTGenReceipt.Rows.Count < 9)
            //{
            //    int Row_Count = AutoDTGenReceipt.Rows.Count;
            //    for (int i = Row_Count; i < 9; i++)
            //    {
            //        AutoDTGenReceipt.NewRow();
            //        AutoDTGenReceipt.Rows.Add();
            //        AutoDTGenReceipt.Rows[i]["UnPlanRecpNo"] = Receipt_No;
            //        AutoDTGenReceipt.Rows[i]["Discount_Per"] = "0.0";
            //        AutoDTGenReceipt.Rows[i]["Discount"] = "0.0";
            //        AutoDTGenReceipt.Rows[i]["TaxPer"] = "0.0";
            //        AutoDTGenReceipt.Rows[i]["TaxAmount"] = "0.0";
            //        AutoDTGenReceipt.Rows[i]["BDUTaxPer"] = "0.0";
            //        AutoDTGenReceipt.Rows[i]["BDUTaxAmount"] = "0.0";
            //        AutoDTGenReceipt.Rows[i]["OtherCharge"] = "0.0";
            //        //AutoDTGenReceipt.Rows[i]["LineTotal"] = GenReceiptDT.Rows[i]["LineTotal"];
            //        AutoDTGenReceipt.Rows[i]["TotalAmt"] = AutoDTGenReceipt.Rows[0]["TotalAmt"];
            //        AutoDTGenReceipt.Rows[i]["AddOrLess"] = AutoDTGenReceipt.Rows[0]["AddOrLess"];
            //        AutoDTGenReceipt.Rows[i]["NetAmount"] = AutoDTGenReceipt.Rows[0]["NetAmount"];
            //    }

            //}
            ds.Tables.Add(AutoDTGenReceipt);
            ReportDocument report = new ReportDocument();

            report.Load(Server.MapPath("~/crystal/SamplePurchaseDetails_Sorting.rpt"));
            report.DataDefinition.FormulaFields["ReportHeading"].Text = "'" + Heading + "'";
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }

        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }


    }

    //General Receipt Details Report Code Start
    public void General_Receipt_Details_Report()
    {
        if (RptName == "General Receipt Details Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            SSQL = "Select UPM.UnPlan_Recp_No,UPM.UnPlan_Recp_Date,UPM.Supp_Name,UPM.InvNo,UPM.InvDate,";
            SSQL = SSQL + " UPM.Description,UPM.TotalAmt,UPM.AddOrLess,UPM.NetAmount,UPM.GPINNo as DCNo,UPM.GPINDate as DCDate,";
            SSQL = SSQL + " UPS.ItemCode,UPS.ItemName,UPS.ValuationType,UPS.UOMCode,UPS.DeptName,UPS.WarehouseName,";
            SSQL = SSQL + " UPS.ZoneName,UPS.BinName,UPS.ReceivedQty,UPS.PrevRate,UPS.ItemRate,";
            SSQL = SSQL + " UPS.ItemTotal,UPS.Discount_Per,UPS.Discount,UPS.TaxPer,UPS.TaxAmount,UPS.BDUTaxPer,UPS.BDUTaxAmount,";
            SSQL = SSQL + " UPS.OtherCharge,UPS.LineTotal,isnull((UPS.CGSTPer),0) as CGSTPer,isnull((UPS.CGSTAmount),0) as CGSTAmount,";
            SSQL = SSQL + " Isnull((UPS.SGSTPer),0) as SGSTPer,isnull((UPS.SGSTAmount),0) as SGSTAmount,isnull((UPS.IGSTPer),0) as IGSTPer,";
            SSQL = SSQL + " Isnull((UPS.IGSTAmount),0) as IGSTAmount,UPM.OtherAmt,UPM.PackingAmt ";
            SSQL = SSQL + " From Unplanned_Receipt_Main UPM inner join Unplanned_Receipt_Main_Sub UPS ";
            SSQL = SSQL + " on UPM.UnPlan_Recp_No=UPS.UnPlan_Recp_No where UPM.Ccode='" + SessionCcode + "' And UPM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And UPM.FinYearCode='" + SessionFinYearCode + "' And UPS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And UPS.Lcode='" + SessionLcode + "' And UPS.FinYearCode='" + SessionFinYearCode + "' And UPM.UnPlan_PO_Receipt_Status='1'";

            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,UPM.UnPlan_Recp_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,UPM.UnPlan_Recp_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (WareHouse != "-Select-")
            {
                SSQL = SSQL + " And UPS.WarehouseName='" + WareHouse + "'";
            }
            if (SupplierName != "-Select-")
            {
                SSQL = SSQL + " And UPM.Supp_Name='" + SupplierName + "'";
            }

            if (ItemName != "-Select-")
            {
                SSQL = SSQL + " And UPS.ItemName='" + ItemName + "'";
            }

            if (UPRNumber != "-Select-")
            {
                SSQL = SSQL + " And UPM.UnPlan_Recp_No='" + UPRNumber + "'";
            }

            if (CostCenterName != "-Select-")
            {
                SSQL = SSQL + " And UPM.CostCenterName='" + CostCenterName + "'";
            }


            SSQL = SSQL + " order by  DCDate";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (GenReceiptDT.Rows.Count > 0)
            {


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State,Phone,GSTNo From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDTGenReceipt.Columns.Add("CmpName");
                AutoDTGenReceipt.Columns.Add("CmpLoc");
                AutoDTGenReceipt.Columns.Add("CmpCode");
                AutoDTGenReceipt.Columns.Add("LocCode");
                AutoDTGenReceipt.Columns.Add("CmpAddr1");
                AutoDTGenReceipt.Columns.Add("CmpAddr2");
                AutoDTGenReceipt.Columns.Add("CmpPin");
                AutoDTGenReceipt.Columns.Add("CmpMob");
                AutoDTGenReceipt.Columns.Add("CmpGST");
                AutoDTGenReceipt.Columns.Add("UnPlanRecpNo");
                AutoDTGenReceipt.Columns.Add("UnPlanRecpDate");
                AutoDTGenReceipt.Columns.Add("SuppName");
                AutoDTGenReceipt.Columns.Add("InvNo");
                AutoDTGenReceipt.Columns.Add("InvDate");
                AutoDTGenReceipt.Columns.Add("DCNo");
                AutoDTGenReceipt.Columns.Add("DCDate");
                AutoDTGenReceipt.Columns.Add("WarehouseName");
                AutoDTGenReceipt.Columns.Add("Description");

                AutoDTGenReceipt.Columns.Add("ItemCode");
                AutoDTGenReceipt.Columns.Add("ItemName");
                AutoDTGenReceipt.Columns.Add("ValuationType");
                AutoDTGenReceipt.Columns.Add("UomCode");
                AutoDTGenReceipt.Columns.Add("DeptName");
                AutoDTGenReceipt.Columns.Add("ZoneName");
                AutoDTGenReceipt.Columns.Add("BinName");
                AutoDTGenReceipt.Columns.Add("ReceivedQty");
                AutoDTGenReceipt.Columns.Add("PrevRate");
                AutoDTGenReceipt.Columns.Add("ItemRate");
                AutoDTGenReceipt.Columns.Add("ItemTotal");
                AutoDTGenReceipt.Columns.Add("Discount_Per");
                AutoDTGenReceipt.Columns.Add("Discount");
                AutoDTGenReceipt.Columns.Add("TaxPer");
                AutoDTGenReceipt.Columns.Add("TaxAmount");
                AutoDTGenReceipt.Columns.Add("BDUTaxPer");
                AutoDTGenReceipt.Columns.Add("BDUTaxAmount");
                AutoDTGenReceipt.Columns.Add("OtherCharge");
                AutoDTGenReceipt.Columns.Add("LineTotal");
                AutoDTGenReceipt.Columns.Add("TotalAmt");

                AutoDTGenReceipt.Columns.Add("OtherAmt");
                AutoDTGenReceipt.Columns.Add("PackingAmt");
                AutoDTGenReceipt.Columns.Add("AddOrLess");
                AutoDTGenReceipt.Columns.Add("NetAmount");

                AutoDTGenReceipt.Columns.Add("CGSTPer");
                AutoDTGenReceipt.Columns.Add("CGSTAmount");
                AutoDTGenReceipt.Columns.Add("SGSTPer");
                AutoDTGenReceipt.Columns.Add("SGSTAmount");
                AutoDTGenReceipt.Columns.Add("IGSTPer");
                AutoDTGenReceipt.Columns.Add("IGSTAmount");
                AutoDTGenReceipt.Columns.Add("SuppAddr1");
                AutoDTGenReceipt.Columns.Add("SuppAddr2");
                AutoDTGenReceipt.Columns.Add("SuppAddr3");
                AutoDTGenReceipt.Columns.Add("TinNo");
                AutoDTGenReceipt.Columns.Add("CSTNo");


                string Receipt_No = "";
                for (int i = 0; i < GenReceiptDT.Rows.Count; i++)
                {
                    AutoDTGenReceipt.NewRow();
                    AutoDTGenReceipt.Rows.Add();

                    AutoDTGenReceipt.Rows[i]["CmpName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTGenReceipt.Rows[i]["CmpLoc"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTGenReceipt.Rows[i]["CmpCode"] = SessionCcode;
                    AutoDTGenReceipt.Rows[i]["LocCode"] = SessionLcode;
                    AutoDTGenReceipt.Rows[i]["CmpAddr1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString();
                    AutoDTGenReceipt.Rows[i]["CmpAddr2"] = dtdCompanyAddress.Rows[0]["Address2"].ToString() + "," + dtdCompanyAddress.Rows[0]["Pincode"].ToString();
                    AutoDTGenReceipt.Rows[i]["CmpMob"] = "Cell : " + dtdCompanyAddress.Rows[0]["Phone"].ToString();
                    AutoDTGenReceipt.Rows[i]["CmpGST"] = "GST No. : " + dtdCompanyAddress.Rows[0]["GSTNo"].ToString();

                    Receipt_No = GenReceiptDT.Rows[i]["UnPlan_Recp_No"].ToString();

                    AutoDTGenReceipt.Rows[i]["UnPlanRecpNo"] = GenReceiptDT.Rows[i]["UnPlan_Recp_No"];
                    AutoDTGenReceipt.Rows[i]["UnPlanRecpDate"] = GenReceiptDT.Rows[i]["UnPlan_Recp_Date"];
                    AutoDTGenReceipt.Rows[i]["SuppName"] = GenReceiptDT.Rows[i]["Supp_Name"];


                    DataTable dtdCustomerAddress = new DataTable();
                    if (AutoDTGenReceipt.Rows[i]["SuppName"].ToString() != "")
                    {
                        SSQL = "";
                        SSQL = "Select Address1,Address2,City,Pincode,TinNo,CstNo from MstSupplier where SuppName='" + AutoDTGenReceipt.Rows[i]["SuppName"].ToString() + "' And Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                        dtdCustomerAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    }

                    if (dtdCustomerAddress.Rows.Count != 0)
                    {
                        AutoDTGenReceipt.Rows[i]["SuppAddr1"] = dtdCustomerAddress.Rows[0]["Address1"].ToString() + ",";
                        AutoDTGenReceipt.Rows[i]["SuppAddr2"] = dtdCustomerAddress.Rows[0]["Address2"].ToString() + ",";
                        AutoDTGenReceipt.Rows[i]["SuppAddr3"] = dtdCustomerAddress.Rows[0]["City"].ToString() + "," + dtdCustomerAddress.Rows[0]["Pincode"].ToString();
                        AutoDTGenReceipt.Rows[i]["TinNo"] = dtdCustomerAddress.Rows[0]["TinNo"].ToString();
                        AutoDTGenReceipt.Rows[i]["CSTNo"] = dtdCustomerAddress.Rows[0]["CstNo"].ToString();
                    }


                    AutoDTGenReceipt.Rows[i]["InvNo"] = GenReceiptDT.Rows[i]["InvNo"];
                    AutoDTGenReceipt.Rows[i]["InvDate"] = GenReceiptDT.Rows[i]["InvDate"];
                    AutoDTGenReceipt.Rows[i]["DCNo"] = GenReceiptDT.Rows[i]["DCNo"];
                    AutoDTGenReceipt.Rows[i]["DCDate"] = GenReceiptDT.Rows[i]["DCDate"];
                    AutoDTGenReceipt.Rows[i]["WarehouseName"] = GenReceiptDT.Rows[i]["WarehouseName"];
                    AutoDTGenReceipt.Rows[i]["Description"] = GenReceiptDT.Rows[i]["Description"];


                    AutoDTGenReceipt.Rows[i]["ItemCode"] = GenReceiptDT.Rows[i]["ItemCode"];
                    AutoDTGenReceipt.Rows[i]["ItemName"] = GenReceiptDT.Rows[i]["ItemName"];
                    AutoDTGenReceipt.Rows[i]["ValuationType"] = GenReceiptDT.Rows[i]["ValuationType"];
                    AutoDTGenReceipt.Rows[i]["UomCode"] = GenReceiptDT.Rows[i]["UOMCode"];
                    AutoDTGenReceipt.Rows[i]["DeptName"] = GenReceiptDT.Rows[i]["DeptName"];
                    AutoDTGenReceipt.Rows[i]["ZoneName"] = GenReceiptDT.Rows[i]["ZoneName"];
                    AutoDTGenReceipt.Rows[i]["BinName"] = GenReceiptDT.Rows[i]["BinName"];
                    AutoDTGenReceipt.Rows[i]["ReceivedQty"] = GenReceiptDT.Rows[i]["ReceivedQty"];
                    AutoDTGenReceipt.Rows[i]["PrevRate"] = GenReceiptDT.Rows[i]["PrevRate"];
                    AutoDTGenReceipt.Rows[i]["ItemRate"] = GenReceiptDT.Rows[i]["ItemRate"];
                    AutoDTGenReceipt.Rows[i]["ItemTotal"] = GenReceiptDT.Rows[i]["ItemTotal"];
                    AutoDTGenReceipt.Rows[i]["Discount_Per"] = GenReceiptDT.Rows[i]["Discount_Per"];
                    AutoDTGenReceipt.Rows[i]["Discount"] = GenReceiptDT.Rows[i]["Discount"];
                    AutoDTGenReceipt.Rows[i]["TaxPer"] = GenReceiptDT.Rows[i]["TaxPer"];
                    AutoDTGenReceipt.Rows[i]["TaxAmount"] = GenReceiptDT.Rows[i]["TaxAmount"];
                    AutoDTGenReceipt.Rows[i]["BDUTaxPer"] = GenReceiptDT.Rows[i]["BDUTaxPer"];
                    AutoDTGenReceipt.Rows[i]["BDUTaxAmount"] = GenReceiptDT.Rows[i]["BDUTaxAmount"];
                    AutoDTGenReceipt.Rows[i]["OtherCharge"] = GenReceiptDT.Rows[i]["OtherCharge"];
                    AutoDTGenReceipt.Rows[i]["LineTotal"] = GenReceiptDT.Rows[i]["LineTotal"];
                    AutoDTGenReceipt.Rows[i]["TotalAmt"] = GenReceiptDT.Rows[i]["TotalAmt"];

                    AutoDTGenReceipt.Rows[i]["OtherAmt"] = GenReceiptDT.Rows[i]["OtherAmt"];
                    AutoDTGenReceipt.Rows[i]["PackingAmt"] = GenReceiptDT.Rows[i]["PackingAmt"];
                    AutoDTGenReceipt.Rows[i]["AddOrLess"] = GenReceiptDT.Rows[i]["AddOrLess"];
                    AutoDTGenReceipt.Rows[i]["NetAmount"] = GenReceiptDT.Rows[i]["NetAmount"];

                    AutoDTGenReceipt.Rows[i]["CGSTPer"] = GenReceiptDT.Rows[i]["CGSTPer"];
                    AutoDTGenReceipt.Rows[i]["CGSTAmount"] = GenReceiptDT.Rows[i]["CGSTAmount"];
                    AutoDTGenReceipt.Rows[i]["SGSTPer"] = GenReceiptDT.Rows[i]["SGSTPer"];
                    AutoDTGenReceipt.Rows[i]["SGSTAmount"] = GenReceiptDT.Rows[i]["SGSTAmount"];
                    AutoDTGenReceipt.Rows[i]["IGSTPer"] = GenReceiptDT.Rows[i]["IGSTPer"];
                    AutoDTGenReceipt.Rows[i]["IGSTAmount"] = GenReceiptDT.Rows[i]["IGSTAmount"];

                }

                if (ButtonName == "Issue Slip")
                {
                    if (AutoDTGenReceipt.Rows.Count < 14)
                    {
                        int Row_Count = AutoDTGenReceipt.Rows.Count;
                        for (int i = Row_Count; i < 14; i++)
                        {
                            AutoDTGenReceipt.NewRow();
                            AutoDTGenReceipt.Rows.Add();
                            AutoDTGenReceipt.Rows[i]["UnPlanRecpNo"] = Receipt_No;
                            //AutoDTGenReceipt.Rows[i]["Discount_Per"] = "0.0";
                            //AutoDTGenReceipt.Rows[i]["Discount"] = "0.0";
                            AutoDTGenReceipt.Rows[i]["TaxPer"] = "0.0";
                            AutoDTGenReceipt.Rows[i]["TaxAmount"] = "0.0";
                            AutoDTGenReceipt.Rows[i]["BDUTaxPer"] = "0.0";
                            AutoDTGenReceipt.Rows[i]["BDUTaxAmount"] = "0.0";
                            AutoDTGenReceipt.Rows[i]["OtherCharge"] = "0.0";
                            //AutoDTGenReceipt.Rows[i]["LineTotal"] = GenReceiptDT.Rows[i]["LineTotal"];
                            AutoDTGenReceipt.Rows[i]["TotalAmt"] = AutoDTGenReceipt.Rows[0]["TotalAmt"];
                            AutoDTGenReceipt.Rows[i]["OtherAmt"] = AutoDTGenReceipt.Rows[0]["OtherAmt"];
                            AutoDTGenReceipt.Rows[i]["PackingAmt"] = AutoDTGenReceipt.Rows[0]["PackingAmt"];
                            AutoDTGenReceipt.Rows[i]["AddOrLess"] = AutoDTGenReceipt.Rows[0]["AddOrLess"];
                            AutoDTGenReceipt.Rows[i]["NetAmount"] = AutoDTGenReceipt.Rows[0]["NetAmount"];
                            AutoDTGenReceipt.Rows[i]["Description"] = GenReceiptDT.Rows[0]["Description"];
                        }

                    }
                    ds.Tables.Add(AutoDTGenReceipt);
                    ReportDocument report = new ReportDocument();
                    report.Load(Server.MapPath("~/crystal/GeneralReceiptSlip_Two_Copy.rpt"));
                    report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = report;
                }

                if (ButtonName == "Details Report")
                {
                    ds.Tables.Add(AutoDTGenReceipt);
                    ReportDocument report = new ReportDocument();
                    report.Load(Server.MapPath("~/crystal/GeneralReceiptDetails.rpt"));

                    report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                    CrystalReportViewer1.ReportSource = report;

                }


            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }

        //Purchase Item Report
        if (RptName == "General Receipt Purchase Item Report")
        {
            if (FromDate != "") { frmDate = Convert.ToDateTime(FromDate); }
            if (ToDate != "") { toDate = Convert.ToDateTime(ToDate); }

            //SSQL = "Select UPS.DeptName,UPS.ItemName,UPS.UOMCode,sum(UPS.ReceivedQty) as ReceivedQty,sum(UPS.Discount) as Discount,";
            //SSQL = SSQL + "sum(UPS.TaxAmount) as TaxAmount,Sum(UPS.BDUTaxAmount) as BDUTaxAmount,sum(UPS.OtherCharge) as OtherCharge,sum(UPS.LineTotal) as LineTotal,";
            //SSQL = SSQL + "(isnull(sum(UPS.CGSTPer),0)+ isnull(Sum(UPS.SGSTPer),0) + isnull(sum(UPS.IGSTPer),0)) as GSTPer,(isnull(sum(UPS.CGSTAmount),0) + isnull(sum(UPS.SGSTAmount),0) + isnull(sum(UPS.IGSTAmount),0)) as GSTAmount";
            //SSQL = SSQL + " from Unplanned_Receipt_Main UPM inner join Unplanned_Receipt_Main_Sub UPS ";
            //SSQL = SSQL + "on UPM.UnPlan_Recp_No=UPS.UnPlan_Recp_No where UPM.Ccode='" + SessionCcode + "' And UPM.Lcode='" + SessionLcode + "'";
            //SSQL = SSQL + " And UPM.FinYearCode='" + SessionFinYearCode + "' And UPS.Ccode='" + SessionCcode + "'";
            //SSQL = SSQL + " And UPS.Lcode='" + SessionLcode + "' And UPS.FinYearCode='" + SessionFinYearCode + "' And UPM.UnPlan_PO_Receipt_Status='1'";


            SSQL = "Select UPS.ItemCode [ItemCode],UPS.ItemName[ItemName],UPS.BinName[BinName],sum(UPS.ReceivedQty) as [Qty],sum(UPS.LineTotal) as [Value],";
            SSQL = SSQL + "'" + frmDate.ToString("dd-MM-yyyy")+"'[FDate],'" + toDate.ToString("dd-MM-yyyy") + "' [TDate] from Unplanned_Receipt_Main UPM ";
            SSQL = SSQL + " inner join Unplanned_Receipt_Main_Sub UPS on UPM.UnPlan_Recp_No=UPS.UnPlan_Recp_No where UPM.Ccode='" + SessionCcode + "' And ";
            SSQL = SSQL + " UPM.Lcode='" + SessionLcode + "' And UPM.FinYearCode='" + SessionFinYearCode + "' And UPS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And UPS.Lcode='" + SessionLcode + "' And UPS.FinYearCode='" + SessionFinYearCode + "' And UPM.UnPlan_PO_Receipt_Status='1'";


            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,UPM.UnPlan_Recp_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,UPM.UnPlan_Recp_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (WareHouse != "-Select-")
            {
                SSQL = SSQL + " And UPS.WarehouseName='" + WareHouse + "'";
            }
            if (SupplierName != "-Select-")
            {
                SSQL = SSQL + " And UPM.Supp_Name='" + SupplierName + "'";
            }

            if (ItemName != "-Select-")
            {
                SSQL = SSQL + " And UPS.ItemName='" + ItemName + "'";
            }

            if (UPRNumber != "-Select-")
            {
                SSQL = SSQL + " And UPM.UnPlan_Recp_No='" + UPRNumber + "'";
            }

            if (CostCenterName != "-Select-")
            {
                SSQL = SSQL + " And UPM.CostCenterName='" + CostCenterName + "'";

            }

            SSQL = SSQL + " Group by UPS.ItemCode,UPS.ItemName,UPS.BinName";
            SSQL = SSQL + " Order by UPS.ItemName Asc";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(GenReceiptDT);
                //RD_Report.Load(Server.MapPath("~/crystal/GeneralReceiptItemAll.rpt"));
                RD_Report.Load(Server.MapPath("~/crystal/PurchaseOrderReceiptItemWise.rpt"));

                RD_Report.SetDataSource(ds1.Tables[0]);

                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }

        //Department Purchase Report
        if (RptName == "General Receipt Department Purchase")
        {
            if (FromDate != "") { frmDate = Convert.ToDateTime(FromDate); }
            if (ToDate != "") { toDate = Convert.ToDateTime(ToDate); }
            SSQL = "Select UPS.DeptName,sum(UPS.ReceivedQty) as ReceivedQty,sum(UPS.Discount) as Discount,";
            SSQL = SSQL + "sum(UPS.TaxAmount) as TaxAmount,sum(UPS.OtherCharge) as OtherCharge,sum(UPS.LineTotal) as LineTotal";
            SSQL = SSQL + " from Unplanned_Receipt_Main UPM inner join Unplanned_Receipt_Main_Sub UPS ";
            SSQL = SSQL + "on UPM.UnPlan_Recp_No=UPS.UnPlan_Recp_No where UPM.Ccode='" + SessionCcode + "' And UPM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And UPM.FinYearCode='" + SessionFinYearCode + "' And UPS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And UPS.Lcode='" + SessionLcode + "' And UPS.FinYearCode='" + SessionFinYearCode + "' And UPM.UnPlan_PO_Receipt_Status='1'";

            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,UPM.UnPlan_Recp_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,UPM.UnPlan_Recp_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (WareHouse != "")
            {
                SSQL = SSQL + " And UPS.WarehouseName='" + WareHouse + "'";
            }
            if (SupplierName != "")
            {
                SSQL = SSQL + " And UPM.Supp_Name='" + SupplierName + "'";
            }
            if (CostCenterName != "")
            {
                SSQL = SSQL + " And UPM.CostCenterName='" + CostCenterName + "'";

            }

            if (ItemName != "")
            {
                SSQL = SSQL + " And UPS.ItemName='" + ItemName + "'";

            }

            SSQL = SSQL + " Group by UPS.DeptName";
            SSQL = SSQL + " Order by UPS.DeptName Asc";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(GenReceiptDT);
                RD_Report.Load(Server.MapPath("~/crystal/GeneralReceiptDepartment.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }

        //General Receipt Tax Purchase
        if (RptName == "General Receipt Tax Purchase")
        {
            string Tax_Percent = Request.QueryString["Tax_Percent"].ToString();

            if (FromDate != "") { frmDate = Convert.ToDateTime(FromDate); }

            if (ToDate != "") { toDate = Convert.ToDateTime(ToDate); }

            SSQL = "select UPM.UnPlan_Recp_No,UPM.UnPlan_Recp_Date,UPM.Supp_Name,UPM.InvNo,UPM.InvDate,";
            SSQL = SSQL + "UPM.DCNo,UPM.DCDate,UPM.Description,UPM.TotalAmt,";
            SSQL = SSQL + "UPS.ItemCode,UPS.ItemName,UPS.ValuationType,UPS.UOMCode,UPS.WarehouseName,";
            SSQL = SSQL + "UPS.ZoneName,UPS.BinName,UPS.ReceivedQty,UPS.ItemRate,";
            SSQL = SSQL + "UPS.ItemTotal,UPS.Discount_Per,UPS.Discount,UPS.TaxPer,UPS.TaxAmount,UPS.OtherCharge,UPS.LineTotal";
            SSQL = SSQL + " from Unplanned_Receipt_Main UPM inner join Unplanned_Receipt_Main_Sub UPS ";
            SSQL = SSQL + "on UPM.UnPlan_Recp_No=UPS.UnPlan_Recp_No where UPM.Ccode='" + SessionCcode + "' And UPM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And UPM.FinYearCode='" + SessionFinYearCode + "' And UPS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And UPS.Lcode='" + SessionLcode + "' And UPS.FinYearCode='" + SessionFinYearCode + "' And UPM.UnPlan_PO_Receipt_Status='1'";
            //SSQL = SSQL + " And UPS.TaxPer='" + Tax_Percent + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,UPM.UnPlan_Recp_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,UPM.UnPlan_Recp_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (WareHouse != "-Select-")
            {
                SSQL = SSQL + " And UPS.WarehouseName='" + WareHouse + "'";
            }
            if (SupplierName != "-Select-")
            {
                SSQL = SSQL + " And UPM.Supp_Name='" + SupplierName + "'";
            }

            if (ItemName != "-Select-")
            {
                SSQL = SSQL + " And UPS.ItemName='" + ItemName + "'";
            }

            if (UPRNumber != "-Select-")
            {
                SSQL = SSQL + " And UPM.UnPlan_Recp_No='" + UPRNumber + "'";
            }
            SSQL = SSQL + " Order by UPM.Supp_Name,UPM.UnPlan_Recp_No Asc";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (GenReceiptDT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(GenReceiptDT);
                RD_Report.Load(Server.MapPath("~/crystal/GeneralReceipt_TAX_Details.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }

                RD_Report.DataDefinition.FormulaFields["Tax_Percent_Dis"].Text = "'" + Tax_Percent + " %" + "'";

                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }

        //General Receipt GST Tax Purchase
        if (RptName == "General Receipt GST Purchase")
        {

            if (FromDate != "") { frmDate = Convert.ToDateTime(FromDate); }

            if (ToDate != "") { toDate = Convert.ToDateTime(ToDate); }

            SSQL = "select UPM.UnPlan_Recp_No,UPM.UnPlan_Recp_Date,UPM.Supp_Name,UPM.InvNo,UPM.InvDate,";
            SSQL = SSQL + "UPM.DCNo,UPM.DCDate,UPM.Description,UPM.TotalAmt,";
            SSQL = SSQL + "UPS.ItemCode,UPS.ItemName,UPS.ValuationType,UPS.UOMCode,UPS.WarehouseName,";
            SSQL = SSQL + "UPS.ZoneName,UPS.BinName,UPS.ReceivedQty,UPS.ItemRate,";
            SSQL = SSQL + "UPS.ItemTotal,UPS.Discount_Per,UPS.Discount,UPS.TaxPer,UPS.TaxAmount,UPS.OtherCharge,UPS.LineTotal,";
            SSQL = SSQL + "isnull((UPS.CGSTPer),0) as CGSTPer,isnull((UPS.CGSTAmount),0) as CGSTAmount,isnull((UPS.SGSTPer),0) as SGSTPer,isnull((UPS.SGSTAmount),0) as SGSTAmount,isnull((UPS.IGSTPer),0) as IGSTPer,isnull((UPS.IGSTAmount),0) as IGSTAmount";
            SSQL = SSQL + " from Unplanned_Receipt_Main UPM inner join Unplanned_Receipt_Main_Sub UPS ";
            SSQL = SSQL + "on UPM.UnPlan_Recp_No=UPS.UnPlan_Recp_No where UPM.Ccode='" + SessionCcode + "' And UPM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And UPM.FinYearCode='" + SessionFinYearCode + "' And UPS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And UPS.Lcode='" + SessionLcode + "' And UPS.FinYearCode='" + SessionFinYearCode + "' And UPM.UnPlan_PO_Receipt_Status='1'";

            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,UPM.UnPlan_Recp_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,UPM.UnPlan_Recp_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (WareHouse != "-Select-")
            {
                SSQL = SSQL + " And UPS.WarehouseName='" + WareHouse + "'";
            }
            if (SupplierName != "-Select-")
            {
                SSQL = SSQL + " And UPM.Supp_Name='" + SupplierName + "'";
            }

            if (ItemName != "-Select-")
            {
                SSQL = SSQL + " And UPS.ItemName='" + ItemName + "'";
            }

            if (UPRNumber != "-Select-")
            {
                SSQL = SSQL + " And UPM.UnPlan_Recp_No='" + UPRNumber + "'";
            }

            SSQL = SSQL + " Order by UPM.Supp_Name,UPM.UnPlan_Recp_No Asc";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(GenReceiptDT);
                RD_Report.Load(Server.MapPath("~/crystal/GeneralReceipt_GST_Details.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }



                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }
    }
    //General Receipt Details Report Code End


    //Standard Purchase Order Details Invoice Format Code Start
    public void Standard_Purchase_Order_Invoice_Format()
    {

        if (RptName == "Standard Purchase Order Details Invoice Format")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }



            SSQL = "select SPM.Std_PO_No,SPM.Std_PO_Date,SPM.Supp_Name,SPM.Supp_Qtn_No,SPM.Pur_Request_No,SPM.Pur_Request_Date,";
            SSQL = SSQL + "SPM.Supp_Qtn_Date,SPM.DeliveryMode,SPM.DeliveryDate,SPM.DeliveryAt,";
            SSQL = SSQL + "SPM.PaymentMode,SPM.DeptName,SPM.PaymentTerms,SPM.Description,";
            SSQL = SSQL + "SPM.Note,SPM.Others,SPM.AddOrLess,SPM.NetAmount,";
            SSQL = SSQL + "SPS.ItemCode,SPS.ItemName,SPS.UOMCode,SPS.ReuiredQty,";
            SSQL = SSQL + "SPS.OrderQty,SPS.ReuiredDate,SPS.Rate,";
            SSQL = SSQL + "SPS.ItemTotal,SPS.Discount_Per,SPS.Discount,SPS.TaxPer,SPS.TaxAmount,SPS.BDUTaxPer,SPS.BDUTaxAmount,";
            SSQL = SSQL + "SPS.OtherCharge,SPS.CGSTPer,SPS.CGSTAmount,SPS.SGSTPer,SPS.SGSTAmount,SPS.IGSTPer,SPS.IGSTAmount,";
            SSQL = SSQL + "SPS.Remarks,SPS.LineTotal";
            SSQL = SSQL + " from Std_Purchase_Order_Main SPM inner join Std_Purchase_Order_Main_Sub SPS ";
            SSQL = SSQL + "on SPM.Std_PO_No=SPS.Std_PO_No where  SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And SPM.FinYearCode='" + SessionFinYearCode + "' And SPS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "'"; //And SPM.PO_Status='1'";


            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " and CONVERT(DATETIME,SPM.Std_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SPM.Std_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (StdPurOrdNo != "")
            {
                SSQL = SSQL + " And SPM.Std_PO_No='" + StdPurOrdNo + "'";
            }
            if (DeptName != "")
            {
                SSQL = SSQL + " And SPM.DeptName='" + DeptName + "'";
            }

            if (SupplierName != "")
            {
                SSQL = SSQL + " And SPM.Supp_Name='" + SupplierName + "'";
            }
            if (SupQtNo != "")
            {
                SSQL = SSQL + " And SPM.Supp_Qtn_No='" + SupQtNo + "'";
            }

            if (ItemName != "")
            {
                SSQL = SSQL + " And SPS.ItemName='" + ItemName + "'";
            }
            StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);





            if (StdPurDT.Rows.Count > 0)
            {

                DataTable dtdSuppAddress = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State,GstNo,ImagePath From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTStdPur.Columns.Add("CompanyName");
                AutoDTStdPur.Columns.Add("LocationName");
                AutoDTStdPur.Columns.Add("CompanyCode");
                AutoDTStdPur.Columns.Add("LocationCode");
                AutoDTStdPur.Columns.Add("Address1");
                AutoDTStdPur.Columns.Add("Address2");
                AutoDTStdPur.Columns.Add("GstNo");
                AutoDTStdPur.Columns.Add("ImagePath");
                AutoDTStdPur.Columns.Add("StdPONo");
                AutoDTStdPur.Columns.Add("StdPODate");
                AutoDTStdPur.Columns.Add("SuppName");
                AutoDTStdPur.Columns.Add("SupAddr1");
                AutoDTStdPur.Columns.Add("SupAddr2");
                AutoDTStdPur.Columns.Add("SupAddr3");
                AutoDTStdPur.Columns.Add("TinNo");
                AutoDTStdPur.Columns.Add("CSTNo");
                AutoDTStdPur.Columns.Add("SuppQtnNo");
                AutoDTStdPur.Columns.Add("SuppQtnDate");
                AutoDTStdPur.Columns.Add("DeliveryMode");
                AutoDTStdPur.Columns.Add("DeliveryDate");
                AutoDTStdPur.Columns.Add("DeliveryAt");
                AutoDTStdPur.Columns.Add("PaymentMode");
                AutoDTStdPur.Columns.Add("DeptName");
                AutoDTStdPur.Columns.Add("PaymentTerms");
                AutoDTStdPur.Columns.Add("Description");
                AutoDTStdPur.Columns.Add("Note");
                AutoDTStdPur.Columns.Add("Others");
                AutoDTStdPur.Columns.Add("AddOrLess");
                AutoDTStdPur.Columns.Add("NetAmount");

                

                AutoDTStdPur.Columns.Add("PurRequestNo");
                AutoDTStdPur.Columns.Add("PurRequestDate");
                AutoDTStdPur.Columns.Add("ItemCode");
                AutoDTStdPur.Columns.Add("ItemName");
                AutoDTStdPur.Columns.Add("UomCode");
                AutoDTStdPur.Columns.Add("RequiredQty");
                AutoDTStdPur.Columns.Add("OrderQty");
                AutoDTStdPur.Columns.Add("RequiredDate");
                AutoDTStdPur.Columns.Add("Rate");
                AutoDTStdPur.Columns.Add("Remarks");
                AutoDTStdPur.Columns.Add("LineTotal");



                AutoDTStdPur.Columns.Add("ItemTotal");
                AutoDTStdPur.Columns.Add("Discount_Per");
                AutoDTStdPur.Columns.Add("Discount");
                AutoDTStdPur.Columns.Add("TaxPer");
                AutoDTStdPur.Columns.Add("TaxAmount");
                AutoDTStdPur.Columns.Add("BDUTaxPer");
                AutoDTStdPur.Columns.Add("BDUTaxAmount");
                AutoDTStdPur.Columns.Add("OtherCharge");
                AutoDTStdPur.Columns.Add("CGSTPer");
                AutoDTStdPur.Columns.Add("CGSTAmount");
                AutoDTStdPur.Columns.Add("SGSTPer");
                AutoDTStdPur.Columns.Add("SGSTAmount");
                AutoDTStdPur.Columns.Add("IGSTPer");
                AutoDTStdPur.Columns.Add("IGSTAmount");



                for (int i = 0; i < StdPurDT.Rows.Count; i++)
                {
                    AutoDTStdPur.NewRow();
                    AutoDTStdPur.Rows.Add();

                    AutoDTStdPur.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTStdPur.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTStdPur.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTStdPur.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTStdPur.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTStdPur.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
                    AutoDTStdPur.Rows[i]["GstNo"] = dtdCompanyAddress.Rows[0]["GstNo"].ToString();

                    //if (AutoDTStdPur.Rows[i]["CompanyName"].ToString()== "SRI ARAVIND SIVAKUMARAN MILLS")
                    //{
                    //    AutoDTStdPur.Rows[i]["ImagePath"] = dtdCompanyAddress.Rows[0]["ImagePath"].ToString() + "UNIT2.png";
                    //}
                    //else
                    //{
                    //    AutoDTStdPur.Rows[i]["ImagePath"] = dtdCompanyAddress.Rows[0]["ImagePath"].ToString() + "UNIT1.png";
                    //}

                    AutoDTStdPur.Rows[i]["ImagePath"] = dtdCompanyAddress.Rows[0]["ImagePath"].ToString();

                    AutoDTStdPur.Rows[i]["StdPONo"] = StdPurDT.Rows[i]["Std_PO_No"];
                    AutoDTStdPur.Rows[i]["StdPODate"] = StdPurDT.Rows[i]["Std_PO_Date"];
                    AutoDTStdPur.Rows[i]["SuppName"] = StdPurDT.Rows[i]["Supp_Name"];

                    if (AutoDTStdPur.Rows[i]["SuppName"].ToString() != "")
                    {
                        SSQL = "";
                        SSQL = "Select Address1,Address2,City,Pincode,TinNo,CstNo from MstSupplier where SuppName='" + AutoDTStdPur.Rows[i]["SuppName"].ToString() + "' And Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                        dtdSuppAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    }

                    AutoDTStdPur.Rows[i]["SupAddr1"] = dtdSuppAddress.Rows[0]["Address1"].ToString() + ",";
                    AutoDTStdPur.Rows[i]["SupAddr2"] = dtdSuppAddress.Rows[0]["Address2"].ToString() + ",";
                    AutoDTStdPur.Rows[i]["SupAddr3"] = dtdSuppAddress.Rows[0]["City"].ToString() + "," + dtdSuppAddress.Rows[0]["Pincode"].ToString();
                    AutoDTStdPur.Rows[i]["TinNo"] = dtdSuppAddress.Rows[0]["TinNo"].ToString();
                    AutoDTStdPur.Rows[i]["CSTNo"] = dtdSuppAddress.Rows[0]["CstNo"].ToString();

                    AutoDTStdPur.Rows[i]["SuppQtnNo"] = StdPurDT.Rows[i]["Supp_Qtn_No"];
                    AutoDTStdPur.Rows[i]["SuppQtnDate"] = StdPurDT.Rows[i]["Supp_Qtn_Date"];
                    AutoDTStdPur.Rows[i]["DeliveryMode"] = StdPurDT.Rows[i]["DeliveryMode"];
                    AutoDTStdPur.Rows[i]["DeliveryDate"] = StdPurDT.Rows[i]["DeliveryDate"];
                    AutoDTStdPur.Rows[i]["DeliveryAt"] = StdPurDT.Rows[i]["DeliveryAt"];
                    AutoDTStdPur.Rows[i]["PaymentMode"] = StdPurDT.Rows[i]["PaymentMode"];
                    AutoDTStdPur.Rows[i]["DeptName"] = StdPurDT.Rows[i]["DeptName"];
                    AutoDTStdPur.Rows[i]["PaymentTerms"] = StdPurDT.Rows[i]["PaymentTerms"];
                    AutoDTStdPur.Rows[i]["Description"] = StdPurDT.Rows[i]["Description"];
                    AutoDTStdPur.Rows[i]["Note"] = StdPurDT.Rows[i]["Note"];
                    AutoDTStdPur.Rows[i]["Others"] = StdPurDT.Rows[i]["Others"];
                    AutoDTStdPur.Rows[i]["AddOrLess"] = StdPurDT.Rows[i]["AddOrLess"];
                    AutoDTStdPur.Rows[i]["NetAmount"] = StdPurDT.Rows[i]["NetAmount"];


                    AutoDTStdPur.Rows[i]["PurRequestNo"] = StdPurDT.Rows[i]["Pur_Request_No"];
                    AutoDTStdPur.Rows[i]["PurRequestDate"] = StdPurDT.Rows[i]["Pur_Request_Date"];
                    AutoDTStdPur.Rows[i]["ItemCode"] = StdPurDT.Rows[i]["ItemCode"];
                    AutoDTStdPur.Rows[i]["ItemName"] = StdPurDT.Rows[i]["ItemName"];
                    AutoDTStdPur.Rows[i]["UomCode"] = StdPurDT.Rows[i]["UOMCode"];
                    AutoDTStdPur.Rows[i]["RequiredQty"] = StdPurDT.Rows[i]["ReuiredQty"];
                    AutoDTStdPur.Rows[i]["OrderQty"] = StdPurDT.Rows[i]["OrderQty"];
                    AutoDTStdPur.Rows[i]["RequiredDate"] = StdPurDT.Rows[i]["ReuiredDate"];
                    AutoDTStdPur.Rows[i]["Rate"] = StdPurDT.Rows[i]["Rate"];
                    AutoDTStdPur.Rows[i]["Remarks"] = StdPurDT.Rows[i]["Remarks"];
                    AutoDTStdPur.Rows[i]["LineTotal"] = StdPurDT.Rows[i]["LineTotal"];


                    AutoDTStdPur.Rows[i]["ItemTotal"] = StdPurDT.Rows[i]["ItemTotal"];
                    AutoDTStdPur.Rows[i]["Discount_Per"] = StdPurDT.Rows[i]["Discount_Per"];
                    AutoDTStdPur.Rows[i]["Discount"] = StdPurDT.Rows[i]["Discount"];
                    AutoDTStdPur.Rows[i]["OtherCharge"] = StdPurDT.Rows[i]["OtherCharge"];
                    AutoDTStdPur.Rows[i]["CGSTPer"] = StdPurDT.Rows[i]["CGSTPer"];
                    AutoDTStdPur.Rows[i]["CGSTAmount"] = StdPurDT.Rows[i]["CGSTAmount"];
                    AutoDTStdPur.Rows[i]["SGSTPer"] = StdPurDT.Rows[i]["SGSTPer"];
                    AutoDTStdPur.Rows[i]["SGSTAmount"] = StdPurDT.Rows[i]["SGSTAmount"];
                    AutoDTStdPur.Rows[i]["IGSTPer"] = StdPurDT.Rows[i]["IGSTPer"];
                    AutoDTStdPur.Rows[i]["IGSTAmount"] = StdPurDT.Rows[i]["IGSTAmount"];


                }


                if (AutoDTStdPur.Rows.Count < 5)
                {
                    int Row_Count = AutoDTStdPur.Rows.Count;
                    for (int i = Row_Count; i < Row_Count + 5; i++)
                    {
                        AutoDTStdPur.NewRow();
                        AutoDTStdPur.Rows.Add();
                        AutoDTStdPur.Rows[i]["StdPONo"] = AutoDTStdPur.Rows[0]["StdPONo"];

                        AutoDTStdPur.Rows[i]["StdPODate"] = AutoDTStdPur.Rows[0]["StdPODate"];
                        AutoDTStdPur.Rows[i]["SuppName"] = AutoDTStdPur.Rows[0]["SuppName"];


                        AutoDTStdPur.Rows[i]["SupAddr1"] = AutoDTStdPur.Rows[0]["SupAddr1"];
                        AutoDTStdPur.Rows[i]["SupAddr2"] = AutoDTStdPur.Rows[0]["SupAddr2"];
                        AutoDTStdPur.Rows[i]["SupAddr3"] = AutoDTStdPur.Rows[0]["SupAddr3"];
                        AutoDTStdPur.Rows[i]["TinNo"] = AutoDTStdPur.Rows[0]["TinNo"];
                        AutoDTStdPur.Rows[i]["CSTNo"] = AutoDTStdPur.Rows[0]["CSTNo"];

                        AutoDTStdPur.Rows[i]["SuppQtnNo"] = AutoDTStdPur.Rows[0]["SuppQtnNo"];
                        AutoDTStdPur.Rows[i]["SuppQtnDate"] = AutoDTStdPur.Rows[0]["SuppQtnDate"];
                        AutoDTStdPur.Rows[i]["DeliveryMode"] = AutoDTStdPur.Rows[0]["DeliveryMode"];
                        AutoDTStdPur.Rows[i]["DeliveryDate"] = AutoDTStdPur.Rows[0]["DeliveryDate"];
                        AutoDTStdPur.Rows[i]["DeliveryAt"] = AutoDTStdPur.Rows[0]["DeliveryAt"];
                        AutoDTStdPur.Rows[i]["PaymentMode"] = AutoDTStdPur.Rows[0]["PaymentMode"];
                        AutoDTStdPur.Rows[i]["DeptName"] = AutoDTStdPur.Rows[0]["DeptName"];
                        AutoDTStdPur.Rows[i]["PaymentTerms"] = AutoDTStdPur.Rows[0]["PaymentTerms"];
                        AutoDTStdPur.Rows[i]["Description"] = AutoDTStdPur.Rows[0]["Description"];
                        AutoDTStdPur.Rows[i]["Note"] = AutoDTStdPur.Rows[0]["Note"];
                        AutoDTStdPur.Rows[i]["Others"] = AutoDTStdPur.Rows[0]["Others"];
                        AutoDTStdPur.Rows[i]["AddOrLess"] = AutoDTStdPur.Rows[0]["AddOrLess"];
                        AutoDTStdPur.Rows[i]["NetAmount"] = AutoDTStdPur.Rows[0]["NetAmount"];


                    }
                }

                //ds.Tables.Add(AutoDTStdPur);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/StandardPurchaseOrderInvoiceFormat.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDTStdPur);
                RD_Report.Load(Server.MapPath("~/crystal/StandardPurchaseOrderInvoiceFormat.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                //DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();


            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);

            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //Standard Purchase Order Details Invoice Format Code End

    //Blanket Purchase Order Details Invoice Format Code Start
    public void Blanket_Purchase_Order_Invoice_Format()
    {
        if (RptName == "Blanket Purchase Order Details Invoice Format")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            if (FromDate != "" && ToDate != "" && BlanketPONo == "")
            {

                SSQL = "select BPM.Blanket_PO_No,BPM.Blanket_PO_Date,BPM.Supp_Name,";
                SSQL = SSQL + "BPM.Supp_Qtn_No,BPM.Supp_Qtn_Date,BPM.DeliveryMode,";
                SSQL = SSQL + "BPM.DeliveryAt,BPM.PaymentMode,BPM.DeptName,";
                SSQL = SSQL + "BPM.PaymentTerms,BPM.Description,BPM.Note,BPM.Others,";
                SSQL = SSQL + "BPS.Pur_Request_No,BPS.Pur_Request_Date,";
                SSQL = SSQL + "BPS.ItemCode,BPS.ItemName,BPS.UOMCode,";
                SSQL = SSQL + "BPS.ReuiredQty,BPS.OrderQty,BPS.Rate,BPSc.DeliveryDate";
                SSQL = SSQL + " from Blanket_Purchase_Order_Main BPM inner join Blanket_Purchase_Order_Main_Sub BPS ";
                SSQL = SSQL + "on BPM.Blanket_PO_No=BPS.Blanket_PO_No inner join Blanket_Purchase_Order_Schedule BPSc on BPS.Blanket_PO_No=BPSc.Blanket_PO_No where CONVERT(DATETIME,BPM.Blanket_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,BPM.Blanket_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (DeptName != "")
                {
                    SSQL = SSQL + " And BPM.DeptName='" + DeptName + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + " And BPM.Supp_Name='" + SupplierName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And BPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And BPM.Ccode='" + SessionCcode + "' And BPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And BPM.FinYearCode='" + SessionFinYearCode + "' And BPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And BPS.Lcode='" + SessionLcode + "' And BPS.FinYearCode='" + SessionFinYearCode + "' And BPM.Blanket_PO_Status='1'";

                BlanketPODT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FromDate != "" && ToDate != "" && BlanketPONo != "")
            {

                SSQL = "select BPM.Blanket_PO_No,BPM.Blanket_PO_Date,BPM.Supp_Name,";
                SSQL = SSQL + "BPM.Supp_Qtn_No,BPM.Supp_Qtn_Date,BPM.DeliveryMode,";
                SSQL = SSQL + "BPM.DeliveryAt,BPM.PaymentMode,BPM.DeptName,";
                SSQL = SSQL + "BPM.PaymentTerms,BPM.Description,BPM.Note,BPM.Others,";
                SSQL = SSQL + "BPS.Pur_Request_No,BPS.Pur_Request_Date,";
                SSQL = SSQL + "BPS.ItemCode,BPS.ItemName,BPS.UOMCode,";
                SSQL = SSQL + "BPS.ReuiredQty,BPS.OrderQty,BPS.Rate,BPSc.DeliveryDate";
                SSQL = SSQL + " from Blanket_Purchase_Order_Main BPM inner join Blanket_Purchase_Order_Main_Sub BPS ";
                SSQL = SSQL + "on BPM.Blanket_PO_No=BPS.Blanket_PO_No inner join Blanket_Purchase_Order_Schedule BPSc on BPS.Blanket_PO_No=BPSc.Blanket_PO_No where BPM.Blanket_PO_No='" + BlanketPONo + "' And CONVERT(DATETIME,BPM.Blanket_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,BPM.Blanket_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (DeptName != "")
                {
                    SSQL = SSQL + " And BPM.DeptName='" + DeptName + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + " And BPM.Supp_Name='" + SupplierName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And BPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And BPM.Ccode='" + SessionCcode + "' And BPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And BPM.FinYearCode='" + SessionFinYearCode + "' And BPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And BPS.Lcode='" + SessionLcode + "' And BPS.FinYearCode='" + SessionFinYearCode + "' And BPM.Blanket_PO_Status='1'";

                BlanketPODT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (BlanketPONo != "")
            {
                SSQL = "select BPM.Blanket_PO_No,BPM.Blanket_PO_Date,BPM.Supp_Name,";
                SSQL = SSQL + "BPM.Supp_Qtn_No,BPM.Supp_Qtn_Date,BPM.DeliveryMode,";
                SSQL = SSQL + "BPM.DeliveryAt,BPM.PaymentMode,BPM.DeptName,";
                SSQL = SSQL + "BPM.PaymentTerms,BPM.Description,BPM.Note,BPM.Others,";
                SSQL = SSQL + "BPS.Pur_Request_No,BPS.Pur_Request_Date,";
                SSQL = SSQL + "BPS.ItemCode,BPS.ItemName,BPS.UOMCode,";
                SSQL = SSQL + "BPS.ReuiredQty,BPS.OrderQty,BPS.Rate,BPSc.DeliveryDate";
                SSQL = SSQL + " from Blanket_Purchase_Order_Main BPM inner join Blanket_Purchase_Order_Main_Sub BPS ";
                SSQL = SSQL + "on BPM.Blanket_PO_No=BPS.Blanket_PO_No inner join Blanket_Purchase_Order_Schedule BPSc on BPS.Blanket_PO_No=BPSc.Blanket_PO_No where BPM.Blanket_PO_No='" + BlanketPONo + "'";


                if (DeptName != "")
                {
                    SSQL = SSQL + " And BPM.DeptName='" + DeptName + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + " And BPM.Supp_Name='" + SupplierName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And BPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And BPM.Ccode='" + SessionCcode + "' And BPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And BPM.FinYearCode='" + SessionFinYearCode + "' And BPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And BPS.Lcode='" + SessionLcode + "' And BPS.FinYearCode='" + SessionFinYearCode + "' And BPM.Blanket_PO_Status='1'";

                BlanketPODT = objdata.RptEmployeeMultipleDetails(SSQL);


            }
            else if (DeptName != "")
            {
                SSQL = "select BPM.Blanket_PO_No,BPM.Blanket_PO_Date,BPM.Supp_Name,";
                SSQL = SSQL + "BPM.Supp_Qtn_No,BPM.Supp_Qtn_Date,BPM.DeliveryMode,";
                SSQL = SSQL + "BPM.DeliveryAt,BPM.PaymentMode,BPM.DeptName,";
                SSQL = SSQL + "BPM.PaymentTerms,BPM.Description,BPM.Note,BPM.Others,";
                SSQL = SSQL + "BPS.Pur_Request_No,BPS.Pur_Request_Date,";
                SSQL = SSQL + "BPS.ItemCode,BPS.ItemName,BPS.UOMCode,";
                SSQL = SSQL + "BPS.ReuiredQty,BPS.OrderQty,BPS.Rate,BPSc.DeliveryDate";
                SSQL = SSQL + " from Blanket_Purchase_Order_Main BPM inner join Blanket_Purchase_Order_Main_Sub BPS ";
                SSQL = SSQL + "on BPM.Blanket_PO_No=BPS.Blanket_PO_No inner join Blanket_Purchase_Order_Schedule BPSc on BPS.Blanket_PO_No=BPSc.Blanket_PO_No where BPM.DeptName='" + DeptName + "'";


                if (OpnRecNo != "")
                {
                    SSQL = SSQL + " And BPM.Blanket_PO_No='" + BlanketPONo + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + " And BPM.Supp_Name='" + SupplierName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And BPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And BPM.Ccode='" + SessionCcode + "' And BPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And BPM.FinYearCode='" + SessionFinYearCode + "' And BPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And BPS.Lcode='" + SessionLcode + "' And BPS.FinYearCode='" + SessionFinYearCode + "' And BPM.Blanket_PO_Status='1'";

                BlanketPODT = objdata.RptEmployeeMultipleDetails(SSQL);


            }
            else if (SupplierName != "")
            {
                SSQL = "select BPM.Blanket_PO_No,BPM.Blanket_PO_Date,BPM.Supp_Name,";
                SSQL = SSQL + "BPM.Supp_Qtn_No,BPM.Supp_Qtn_Date,BPM.DeliveryMode,";
                SSQL = SSQL + "BPM.DeliveryAt,BPM.PaymentMode,BPM.DeptName,";
                SSQL = SSQL + "BPM.PaymentTerms,BPM.Description,BPM.Note,BPM.Others,";
                SSQL = SSQL + "BPS.Pur_Request_No,BPS.Pur_Request_Date,";
                SSQL = SSQL + "BPS.ItemCode,BPS.ItemName,BPS.UOMCode,";
                SSQL = SSQL + "BPS.ReuiredQty,BPS.OrderQty,BPS.Rate,BPSc.DeliveryDate";
                SSQL = SSQL + " from Blanket_Purchase_Order_Main BPM inner join Blanket_Purchase_Order_Main_Sub BPS ";
                SSQL = SSQL + "on BPM.Blanket_PO_No=BPS.Blanket_PO_No inner join Blanket_Purchase_Order_Schedule BPSc on BPS.Blanket_PO_No=BPSc.Blanket_PO_No where BPM.Supp_Name='" + SupplierName + "'";


                if (OpnRecNo != "")
                {
                    SSQL = SSQL + " And BPM.Blanket_PO_No='" + BlanketPONo + "'";
                }
                if (DeptName != "")
                {
                    SSQL = SSQL + " And BPM.DeptName='" + DeptName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And BPS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And BPM.Ccode='" + SessionCcode + "' And BPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And BPM.FinYearCode='" + SessionFinYearCode + "' And BPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And BPS.Lcode='" + SessionLcode + "' And BPS.FinYearCode='" + SessionFinYearCode + "' And BPM.Blanket_PO_Status='1'";

                BlanketPODT = objdata.RptEmployeeMultipleDetails(SSQL);


            }
            else if (ItemName != "")
            {

                SSQL = "select BPM.Blanket_PO_No,BPM.Blanket_PO_Date,BPM.Supp_Name,";
                SSQL = SSQL + "BPM.Supp_Qtn_No,BPM.Supp_Qtn_Date,BPM.DeliveryMode,";
                SSQL = SSQL + "BPM.DeliveryAt,BPM.PaymentMode,BPM.DeptName,";
                SSQL = SSQL + "BPM.PaymentTerms,BPM.Description,BPM.Note,BPM.Others,";
                SSQL = SSQL + "BPS.Pur_Request_No,BPS.Pur_Request_Date,";
                SSQL = SSQL + "BPS.ItemCode,BPS.ItemName,BPS.UOMCode,";
                SSQL = SSQL + "BPS.ReuiredQty,BPS.OrderQty,BPS.Rate,BPSc.DeliveryDate";
                SSQL = SSQL + " from Blanket_Purchase_Order_Main BPM inner join Blanket_Purchase_Order_Main_Sub BPS ";
                SSQL = SSQL + "on BPM.Blanket_PO_No=BPS.Blanket_PO_No inner join Blanket_Purchase_Order_Schedule BPSc on BPS.Blanket_PO_No=BPSc.Blanket_PO_No where BPS.ItemName='" + ItemName + "'";


                if (OpnRecNo != "")
                {
                    SSQL = SSQL + " And BPM.Blanket_PO_No='" + BlanketPONo + "'";
                }
                if (DeptName != "")
                {
                    SSQL = SSQL + " And BPM.DeptName='" + DeptName + "'";
                }

                if (SupplierName != "")
                {
                    SSQL = SSQL + " And BPM.Supp_Name='" + SupplierName + "'";
                }


                SSQL = SSQL + " And BPM.Ccode='" + SessionCcode + "' And BPM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And BPM.FinYearCode='" + SessionFinYearCode + "' And BPS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And BPS.Lcode='" + SessionLcode + "' And BPS.FinYearCode='" + SessionFinYearCode + "' And BPM.Blanket_PO_Status='1'";

                BlanketPODT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (BlanketPODT.Rows.Count > 0)
            {

                DataTable dtdSuppAddress = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);



                AutoDTBlanketPO.Columns.Add("CompanyName");
                AutoDTBlanketPO.Columns.Add("LocationName");
                AutoDTBlanketPO.Columns.Add("CompanyCode");
                AutoDTBlanketPO.Columns.Add("LocationCode");
                AutoDTBlanketPO.Columns.Add("Address1");
                AutoDTBlanketPO.Columns.Add("Address2");
                AutoDTBlanketPO.Columns.Add("BlanketPONo");
                AutoDTBlanketPO.Columns.Add("BlanketPODate");
                AutoDTBlanketPO.Columns.Add("SuppName");
                AutoDTBlanketPO.Columns.Add("SupAddr1");
                AutoDTBlanketPO.Columns.Add("SupAddr2");
                AutoDTBlanketPO.Columns.Add("SupAddr3");
                AutoDTBlanketPO.Columns.Add("TinNo");
                AutoDTBlanketPO.Columns.Add("CSTNo");
                AutoDTBlanketPO.Columns.Add("SuppQtnNo");
                AutoDTBlanketPO.Columns.Add("SuppQtnDate");
                AutoDTBlanketPO.Columns.Add("DeliveryMode");
                AutoDTBlanketPO.Columns.Add("DeliveryAt");
                AutoDTBlanketPO.Columns.Add("DeliveryDate");
                AutoDTBlanketPO.Columns.Add("PaymentMode");
                AutoDTBlanketPO.Columns.Add("DeptName");
                AutoDTBlanketPO.Columns.Add("PaymentTerms");
                AutoDTBlanketPO.Columns.Add("Description");
                AutoDTBlanketPO.Columns.Add("Note");
                AutoDTBlanketPO.Columns.Add("Others");

                AutoDTBlanketPO.Columns.Add("PurRequestNo");
                AutoDTBlanketPO.Columns.Add("PurRequestDate");
                AutoDTBlanketPO.Columns.Add("ItemCode");
                AutoDTBlanketPO.Columns.Add("ItemName");
                AutoDTBlanketPO.Columns.Add("UomCode");
                AutoDTBlanketPO.Columns.Add("RequiredQty");
                AutoDTBlanketPO.Columns.Add("OrderQty");
                AutoDTBlanketPO.Columns.Add("Rate");



                for (int i = 0; i < BlanketPODT.Rows.Count; i++)
                {
                    AutoDTBlanketPO.NewRow();
                    AutoDTBlanketPO.Rows.Add();

                    AutoDTBlanketPO.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTBlanketPO.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTBlanketPO.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTBlanketPO.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTBlanketPO.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTBlanketPO.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDTBlanketPO.Rows[i]["BlanketPONo"] = BlanketPODT.Rows[i]["Blanket_PO_No"];
                    AutoDTBlanketPO.Rows[i]["BlanketPODate"] = BlanketPODT.Rows[i]["Blanket_PO_Date"];
                    AutoDTBlanketPO.Rows[i]["SuppName"] = BlanketPODT.Rows[i]["Supp_Name"];


                    if (AutoDTBlanketPO.Rows[i]["SuppName"].ToString() != "")
                    {
                        SSQL = "";
                        SSQL = "Select Address1,Address2,City,Pincode,TinNo,CstNo from MstSupplier where SuppName='" + AutoDTBlanketPO.Rows[i]["SuppName"].ToString() + "' And Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                        dtdSuppAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    }

                    AutoDTBlanketPO.Rows[i]["SupAddr1"] = dtdSuppAddress.Rows[0]["Address1"].ToString() + ",";
                    AutoDTBlanketPO.Rows[i]["SupAddr2"] = dtdSuppAddress.Rows[0]["Address2"].ToString() + ",";
                    AutoDTBlanketPO.Rows[i]["SupAddr3"] = dtdSuppAddress.Rows[0]["City"].ToString() + "," + dtdSuppAddress.Rows[0]["Pincode"].ToString();
                    AutoDTBlanketPO.Rows[i]["TinNo"] = dtdSuppAddress.Rows[0]["TinNo"].ToString();
                    AutoDTBlanketPO.Rows[i]["CSTNo"] = dtdSuppAddress.Rows[0]["CstNo"].ToString();



                    AutoDTBlanketPO.Rows[i]["SuppQtnNo"] = BlanketPODT.Rows[i]["Supp_Qtn_No"];
                    AutoDTBlanketPO.Rows[i]["SuppQtnDate"] = BlanketPODT.Rows[i]["Supp_Qtn_Date"];
                    AutoDTBlanketPO.Rows[i]["DeliveryMode"] = BlanketPODT.Rows[i]["DeliveryMode"];
                    AutoDTBlanketPO.Rows[i]["DeliveryAt"] = BlanketPODT.Rows[i]["DeliveryAt"];
                    AutoDTBlanketPO.Rows[i]["DeliveryDate"] = BlanketPODT.Rows[i]["DeliveryDate"];
                    AutoDTBlanketPO.Rows[i]["PaymentMode"] = BlanketPODT.Rows[i]["PaymentMode"];
                    AutoDTBlanketPO.Rows[i]["DeptName"] = BlanketPODT.Rows[i]["DeptName"];
                    AutoDTBlanketPO.Rows[i]["PaymentTerms"] = BlanketPODT.Rows[i]["PaymentTerms"];
                    AutoDTBlanketPO.Rows[i]["Description"] = BlanketPODT.Rows[i]["Description"];
                    AutoDTBlanketPO.Rows[i]["Note"] = BlanketPODT.Rows[i]["Note"];
                    AutoDTBlanketPO.Rows[i]["Others"] = BlanketPODT.Rows[i]["Others"];


                    AutoDTBlanketPO.Rows[i]["PurRequestNo"] = BlanketPODT.Rows[i]["Pur_Request_No"];
                    AutoDTBlanketPO.Rows[i]["PurRequestDate"] = BlanketPODT.Rows[i]["Pur_Request_Date"];
                    AutoDTBlanketPO.Rows[i]["ItemCode"] = BlanketPODT.Rows[i]["ItemCode"];
                    AutoDTBlanketPO.Rows[i]["ItemName"] = BlanketPODT.Rows[i]["ItemName"];
                    AutoDTBlanketPO.Rows[i]["UomCode"] = BlanketPODT.Rows[i]["UOMCode"];
                    AutoDTBlanketPO.Rows[i]["RequiredQty"] = BlanketPODT.Rows[i]["ReuiredQty"];
                    AutoDTBlanketPO.Rows[i]["OrderQty"] = BlanketPODT.Rows[i]["OrderQty"];
                    AutoDTBlanketPO.Rows[i]["Rate"] = BlanketPODT.Rows[i]["Rate"];




                }

                //ds.Tables.Add(AutoDTBlanketPO);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/BlanketPurchaseOrderInvoiceFormat.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;



                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDTBlanketPO);
                RD_Report.Load(Server.MapPath("~/crystal/BlanketPurchaseOrderInvoiceFormat.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add

                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //Blanket Purchase Order Details Invoice Format Code End

    //General Purchase Order Invoice Format Code Start

    //public void General_Purchase_Order_Invoice_Format()
    //{

    //    if (RptName == "General Purchase Order Invoice Format")
    //    {
    //        if (FromDate != "")
    //        {
    //            frmDate = Convert.ToDateTime(FromDate);
    //        }

    //        if (ToDate != "")
    //        {
    //            toDate = Convert.ToDateTime(ToDate);
    //        }


    //        //SSQL = "select GPM.Gen_PO_No,GPM.Gen_PO_Date,GPM.Supp_Name,GPM.Supp_Qtn_No,";
    //        //SSQL = SSQL + "GPM.Supp_Qtn_Date,GPM.DeliveryMode,GPM.DeliveryDate,GPM.DeliveryAt,";
    //        //SSQL = SSQL + "GPM.PaymentMode,GPM.DeptName,GPM.PaymentTerms,GPM.Description,";
    //        //SSQL = SSQL + "GPM.Note,GPM.Others,GPM.AddOrLess,GPM.NetAmount,";
    //        //SSQL = SSQL + "GPS.ItemCode,GPS.ItemName,GPS.UOMCode,";
    //        //SSQL = SSQL + "GPS.OrderQty,GPS.Rate,";
    //        //SSQL = SSQL + "GPS.ItemTotal,GPS.Discount_Per,GPS.Discount,";
    //        //SSQL = SSQL + "GPS.OtherCharge,GPS.CGSTPer,GPS.CGSTAmount,GPS.SGSTPer,GPS.SGSTAmount,GPS.IGSTPer,GPS.IGSTAmount,";
    //        //SSQL = SSQL + "GPS.Remarks,GPS.LineTotal";
    //        //SSQL = SSQL + " from General_Purchase_Order_Main GPM inner join General_Purchase_Order_Main_Sub GPS ";
    //        //SSQL = SSQL + "on GPM.Gen_PO_No=GPS.Gen_PO_No";

    //        SSQL = "Select CMP.Cname [CompanyName],CMP.Address1[Address1],CMP.Address2+' - '+cmp.Pincode [Address2],CMP.Phone [Mobile],CMP.Email[Mail],CMP.ImagePath[ImgPath],";
    //        SSQL = SSQL + "SUP.SuppName[SuppName],SUP.Address1[SupAddr1],SUP.Address2[SupAddr2],SUP.City+' - '+sup.Pincode [SupAddr3],SUP.TinNo[TinNo],SUP.CstNo[CSTNo],";
    //        SSQL = SSQL + "GPM.Gen_PO_No[StdPONo],GPM.Gen_PO_Date[StdPODate],GPM.Supp_Qtn_No[SuppQtnNo],GPM.Supp_Qtn_Date[SuppQtnDate],GPM.DeliveryMode[DeliveryMode],";
    //        SSQL = SSQL + "GPM.DeliveryDate[DeliveryDate],GPM.DeliveryAt[DeliveryAt],GPM.PaymentMode[PaymentMode],GPM.DeptName[DeptName],GPM.PaymentTerms[PaymentTerms],";
    //        SSQL = SSQL + "GPM.Description[Description],GPM.Note[Note],GPM.Others[Others],GPM.AddOrLess[AddOrLess],GPM.NetAmount[NetAmount],GPS.ItemCode[ItemCode],";
    //        SSQL = SSQL + "GPS.ItemName[ItemName],GPS.UOMCode[UomCode],GPS.OrderQty[OrderQty],GPS.Rate[Rate],GPS.ItemTotal[ItemTotal],GPS.Discount_Per[Discount_Per],";
    //        SSQL = SSQL + "GPS.Discount[Discount],GPS.OtherCharge[OtherCharge],GPS.CGSTPer[CGSTPer],GPS.CGSTAmount[CGSTAmount],GPS.SGSTPer[SGSTPer],GPS.SGSTAmount[SGSTAmount],";
    //        SSQL = SSQL + "GPS.IGSTPer[IGSTPer],GPS.IGSTAmount[IGSTAmount],GPS.LineTotal[LineTotal],GPS.Remarks[Remarks] from General_Purchase_Order_Main GPM ";
    //        SSQL = SSQL + "Inner join General_Purchase_Order_Main_Sub GPS on GPM.Gen_PO_No=GPS.Gen_PO_No ";
    //        SSQL = SSQL + "Inner Join AdminRights CMP on CMP.Ccode=GPM.Ccode Inner Join MstSupplier SUP on SUP.SuppCode=GPM.Supp_Code";
    //        SSQL = SSQL + " Where GPM.Ccode='" + SessionCcode + "' And GPM.Lcode='" + SessionLcode + "'";
    //        SSQL = SSQL + " And GPM.FinYearCode='" + SessionFinYearCode + "' And GPS.Ccode='" + SessionCcode + "'";
    //        SSQL = SSQL + " And GPS.Lcode='" + SessionLcode + "' And GPS.FinYearCode='" + SessionFinYearCode + "' And GPM.PO_Status='1'";

    //        if (GenPurOrdNo != "")
    //        {
    //            SSQL = SSQL + " And GPM.Gen_PO_No='" + GenPurOrdNo + "'";
    //        }

    //        if (FromDate != "" && ToDate != "" && GenPurOrdNo == "")
    //        {

    //            SSQL = SSQL + " and CONVERT(DATETIME,GPM.Gen_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GPM.Gen_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
    //        }

    //        if (DeptName != "")
    //        {
    //            SSQL = SSQL + " And GPM.DeptName='" + DeptName + "'";
    //        }

    //        if (SupplierName != "")
    //        {
    //            SSQL = SSQL + " And GPM.Supp_Name='" + SupplierName + "'";
    //        }

    //        if (ItemName != "")
    //        {
    //            SSQL = SSQL + " And GPS.ItemName='" + ItemName + "'";
    //        }

    //        GenPurDT = objdata.RptEmployeeMultipleDetails(SSQL);


    //        //if (GenPurDT.Rows.Count > 0)
    //        //{

    //        //    DataTable dtdSuppAddress = new DataTable();
    //        //    DataTable dtdCompanyAddress = new DataTable();
    //        //    SSQL = "select Cname,location,Address1,Address2,Pincode,State,GSTNO From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
    //        //    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


    //        //    AutoDTGenPur.Columns.Add("CompanyName");
    //        //    AutoDTGenPur.Columns.Add("LocationName");
    //        //    AutoDTGenPur.Columns.Add("CompanyCode");
    //        //    AutoDTGenPur.Columns.Add("LocationCode");
    //        //    AutoDTGenPur.Columns.Add("Address1");
    //        //    AutoDTGenPur.Columns.Add("Address2");
    //        //    AutoDTGenPur.Columns.Add("GSTNO");
    //        //    AutoDTGenPur.Columns.Add("StdPONo");
    //        //    AutoDTGenPur.Columns.Add("StdPODate");
    //        //    AutoDTGenPur.Columns.Add("SuppName");
    //        //    AutoDTGenPur.Columns.Add("SupAddr1");
    //        //    AutoDTGenPur.Columns.Add("SupAddr2");
    //        //    AutoDTGenPur.Columns.Add("SupAddr3");
    //        //    AutoDTGenPur.Columns.Add("TinNo");
    //        //    AutoDTGenPur.Columns.Add("CSTNo");
    //        //    AutoDTGenPur.Columns.Add("SuppQtnNo");
    //        //    AutoDTGenPur.Columns.Add("SuppQtnDate");
    //        //    AutoDTGenPur.Columns.Add("DeliveryMode");
    //        //    AutoDTGenPur.Columns.Add("DeliveryDate");
    //        //    AutoDTGenPur.Columns.Add("DeliveryAt");
    //        //    AutoDTGenPur.Columns.Add("PaymentMode");
    //        //    AutoDTGenPur.Columns.Add("DeptName");
    //        //    AutoDTGenPur.Columns.Add("PaymentTerms");
    //        //    AutoDTGenPur.Columns.Add("Description");
    //        //    AutoDTGenPur.Columns.Add("Note");
    //        //    AutoDTGenPur.Columns.Add("Others");
    //        //    AutoDTGenPur.Columns.Add("TotalAmt");
    //        //    AutoDTGenPur.Columns.Add("AddOrLess");

    //        //    AutoDTGenPur.Columns.Add("NetAmount");


    //        //    AutoDTGenPur.Columns.Add("ItemCode");
    //        //    AutoDTGenPur.Columns.Add("ItemName");
    //        //    AutoDTGenPur.Columns.Add("UomCode");
    //        //    AutoDTGenPur.Columns.Add("OrderQty");
    //        //    AutoDTGenPur.Columns.Add("Rate");
    //        //    AutoDTGenPur.Columns.Add("Remarks");
    //        //    AutoDTGenPur.Columns.Add("LineTotal");


    //        //    AutoDTGenPur.Columns.Add("ItemTotal");
    //        //    AutoDTGenPur.Columns.Add("Discount_Per");
    //        //    AutoDTGenPur.Columns.Add("Discount");
    //        //    AutoDTGenPur.Columns.Add("OtherCharge");
    //        //    AutoDTGenPur.Columns.Add("CGSTPer");
    //        //    AutoDTGenPur.Columns.Add("CGSTAmount");
    //        //    AutoDTGenPur.Columns.Add("SGSTPer");
    //        //    AutoDTGenPur.Columns.Add("SGSTAmount");
    //        //    AutoDTGenPur.Columns.Add("IGSTPer");
    //        //    AutoDTGenPur.Columns.Add("IGSTAmount");

    //        //    for (int i = 0; i < GenPurDT.Rows.Count; i++)
    //        //    {
    //        //        AutoDTGenPur.NewRow();
    //        //        AutoDTGenPur.Rows.Add();

    //        //        AutoDTGenPur.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
    //        //        AutoDTGenPur.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
    //        //        AutoDTGenPur.Rows[i]["CompanyCode"] = SessionCcode;
    //        //        AutoDTGenPur.Rows[i]["LocationCode"] = SessionLcode;
    //        //        AutoDTGenPur.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
    //        //        AutoDTGenPur.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
    //        //        AutoDTGenPur.Rows[i]["GSTNO"] = dtdCompanyAddress.Rows[0]["GSTNO"].ToString();

    //        //        AutoDTGenPur.Rows[i]["StdPONo"] = GenPurDT.Rows[i]["Gen_PO_No"];
    //        //        AutoDTGenPur.Rows[i]["StdPODate"] = GenPurDT.Rows[i]["Gen_PO_Date"];
    //        //        AutoDTGenPur.Rows[i]["SuppName"] = GenPurDT.Rows[i]["Supp_Name"];

    //        //        if (AutoDTGenPur.Rows[i]["SuppName"].ToString() != "")
    //        //        {
    //        //            SSQL = "";
    //        //            SSQL = "Select Address1,Address2,City,Pincode,TinNo,CstNo from MstSupplier where SuppName='" + AutoDTGenPur.Rows[i]["SuppName"].ToString() + "' And Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
    //        //            dtdSuppAddress = objdata.RptEmployeeMultipleDetails(SSQL);
    //        //        }

    //        //        AutoDTGenPur.Rows[i]["SupAddr1"] = dtdSuppAddress.Rows[0]["Address1"].ToString() + ",";
    //        //        AutoDTGenPur.Rows[i]["SupAddr2"] = dtdSuppAddress.Rows[0]["Address2"].ToString() + ",";
    //        //        AutoDTGenPur.Rows[i]["SupAddr3"] = dtdSuppAddress.Rows[0]["City"].ToString() + "," + dtdSuppAddress.Rows[0]["Pincode"].ToString();
    //        //        AutoDTGenPur.Rows[i]["TinNo"] = dtdSuppAddress.Rows[0]["TinNo"].ToString();
    //        //        AutoDTGenPur.Rows[i]["CSTNo"] = dtdSuppAddress.Rows[0]["CstNo"].ToString();


    //        //        AutoDTGenPur.Rows[i]["SuppQtnNo"] = GenPurDT.Rows[i]["Supp_Qtn_No"];
    //        //        AutoDTGenPur.Rows[i]["SuppQtnDate"] = GenPurDT.Rows[i]["Supp_Qtn_Date"];
    //        //        AutoDTGenPur.Rows[i]["DeliveryMode"] = GenPurDT.Rows[i]["DeliveryMode"];
    //        //        AutoDTGenPur.Rows[i]["DeliveryDate"] = GenPurDT.Rows[i]["DeliveryDate"];
    //        //        AutoDTGenPur.Rows[i]["DeliveryAt"] = GenPurDT.Rows[i]["DeliveryAt"];
    //        //        AutoDTGenPur.Rows[i]["PaymentMode"] = GenPurDT.Rows[i]["PaymentMode"];
    //        //        AutoDTGenPur.Rows[i]["DeptName"] = GenPurDT.Rows[i]["DeptName"];
    //        //        AutoDTGenPur.Rows[i]["PaymentTerms"] = GenPurDT.Rows[i]["PaymentTerms"];
    //        //        AutoDTGenPur.Rows[i]["Description"] = GenPurDT.Rows[i]["Description"];
    //        //        AutoDTGenPur.Rows[i]["Note"] = GenPurDT.Rows[i]["Note"];
    //        //        AutoDTGenPur.Rows[i]["Others"] = GenPurDT.Rows[i]["Others"];
    //        //        //AutoDTGenPur.Rows[i]["TotalAmt"] = GenPurDT.Rows[i]["TotalAmt"];
    //        //        AutoDTGenPur.Rows[i]["AddOrLess"] = GenPurDT.Rows[i]["AddOrLess"];
    //        //        AutoDTGenPur.Rows[i]["NetAmount"] = GenPurDT.Rows[i]["NetAmount"];


    //        //        AutoDTGenPur.Rows[i]["ItemCode"] = GenPurDT.Rows[i]["ItemCode"];
    //        //        AutoDTGenPur.Rows[i]["ItemName"] = GenPurDT.Rows[i]["ItemName"];
    //        //        AutoDTGenPur.Rows[i]["UomCode"] = GenPurDT.Rows[i]["UOMCode"];
    //        //        AutoDTGenPur.Rows[i]["OrderQty"] = GenPurDT.Rows[i]["OrderQty"];
    //        //        AutoDTGenPur.Rows[i]["Rate"] = GenPurDT.Rows[i]["Rate"];
    //        //        AutoDTGenPur.Rows[i]["Remarks"] = GenPurDT.Rows[i]["Remarks"];
    //        //        AutoDTGenPur.Rows[i]["LineTotal"] = GenPurDT.Rows[i]["LineTotal"];

    //        //        AutoDTGenPur.Rows[i]["ItemTotal"] = GenPurDT.Rows[i]["ItemTotal"];
    //        //        AutoDTGenPur.Rows[i]["Discount_Per"] = GenPurDT.Rows[i]["Discount_Per"];
    //        //        AutoDTGenPur.Rows[i]["Discount"] = GenPurDT.Rows[i]["Discount"];
    //        //        AutoDTGenPur.Rows[i]["OtherCharge"] = GenPurDT.Rows[i]["OtherCharge"];
    //        //        AutoDTGenPur.Rows[i]["CGSTPer"] = GenPurDT.Rows[i]["CGSTPer"];
    //        //        AutoDTGenPur.Rows[i]["CGSTAmount"] = GenPurDT.Rows[i]["CGSTAmount"];
    //        //        AutoDTGenPur.Rows[i]["SGSTPer"] = GenPurDT.Rows[i]["SGSTPer"];
    //        //        AutoDTGenPur.Rows[i]["SGSTAmount"] = GenPurDT.Rows[i]["SGSTAmount"];
    //        //        AutoDTGenPur.Rows[i]["IGSTPer"] = GenPurDT.Rows[i]["IGSTPer"];
    //        //        AutoDTGenPur.Rows[i]["IGSTAmount"] = GenPurDT.Rows[i]["IGSTAmount"];

    //        //    }

    //        //ds.Tables.Add(AutoDTGenPur);
    //        //ReportDocument report = new ReportDocument();
    //        //report.Load(Server.MapPath("~/crystal/GeneralPurchaseOrderInvoiceFormat.rpt"));
    //        //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
    //        //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

    //        //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


    //        //CrystalReportViewer1.ReportSource = report;
    //        DataRow dr;

    //        //for (int i= AutoDTGenPur.Rows.Count;i<6;i++)
    //        //{
    //        //    dr = AutoDTGenPur.NewRow();
    //        //     dr["Note"] = GenPurDT.Rows[0]["Note"];
    //        //    dr["Others"] = GenPurDT.Rows[0]["Others"];
    //        //    //AutoDTGenPur.Rows[i]["TotalAmt"] = GenPurDT.Rows[i]["TotalAmt"];
    //        //    dr["AddOrLess"] = GenPurDT.Rows[0]["AddOrLess"];
    //        //    dr["NetAmount"] = GenPurDT.Rows[0]["NetAmount"];

    //        //    AutoDTGenPur.Rows.InsertAt(dr,i);
    //        //}


    //        DataSet ds1 = new DataSet();
    //        ds1.Tables.Add(GenPurDT);
    //        RD_Report.Load(Server.MapPath("~/crystal/GeneralPurchaseOrderInvoiceFormatNew.rpt"));
    //        RD_Report.SetDataSource(ds1.Tables[0]);

    //        //Company Details Add

    //        //SSQL = "select Cname,location,Address1,Address2,Pincode,State,GstNo,ImagePath From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
    //        //dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
    //        //if (dtdCompanyAddress.Rows.Count != 0)
    //        //{
    //        //    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
    //        //    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
    //        //    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";

    //        //    //if (dtdCompanyAddress.Rows[0]["Cname"].ToString() == "SRI ARAVIND SIVAKUMARAN MILLS")
    //        //    //{
    //        //    //    RD_Report.DataDefinition.FormulaFields["ImagePath"].Text = "'" + dtdCompanyAddress.Rows[0]["ImagePath"].ToString() + "UNIT2.png'";
    //        //    //}
    //        //    RD_Report.DataDefinition.FormulaFields["ImagePath"].Text = "'" + dtdCompanyAddress.Rows[0]["ImagePath"].ToString() + "'";
    //        //}
    //        RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


    //        CrystalReportViewer1.ReportSource = RD_Report;
    //        CrystalReportViewer1.RefreshReport();
    //        CrystalReportViewer1.DataBind();
    //    }

    //    else
    //    {
    //        Errflag = false;
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
    //    }
    //    if (!Errflag)
    //    {
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
    //        lblUploadSuccessfully.Text = "No Records Matched..";
    //    }

    //}
    public void General_Purchase_Order_Invoice_Format()
    {

        if (RptName == "General Purchase Order Invoice Format")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            SSQL = "Select CMP.Cname [CompanyName],CMP.Address1[Address1],CMP.Address2+' - '+cmp.Pincode [Address2],CMP.Phone [Mobile],CMP.Email[Mail],CMP.ImagePath[ImgPath],";
            SSQL = SSQL + "SUP.SuppName[SuppName],SUP.Address1[SupAddr1],SUP.Address2[SupAddr2],SUP.City+' - '+sup.Pincode [SupAddr3],SUP.TinNo[TinNo],SUP.CstNo[CSTNo],";
            SSQL = SSQL + "GPM.Gen_PO_No[StdPONo],GPM.Gen_PO_Date[StdPODate],GPM.Supp_Qtn_No[SuppQtnNo],GPM.Supp_Qtn_Date[SuppQtnDate],GPM.DeliveryMode[DeliveryMode],";
            SSQL = SSQL + "GPM.DeliveryDate[DeliveryDate],GPM.DeliveryAt[DeliveryAt],GPM.PaymentMode[PaymentMode],GPM.DeptName[DeptName],GPM.PaymentTerms[PaymentTerms],";
            SSQL = SSQL + "GPM.Description[Description],GPM.Note[Note],GPM.Others[Others],GPM.Discount[TotDisc],GPM.PackingAmt[PackingAmt],GPM.AddOrLess[AddOrLess],GPM.NetAmount[NetAmount],GPS.ItemCode[ItemCode],";
            SSQL = SSQL + "GPS.ItemName[ItemName],GPS.UOMCode[UomCode],GPS.OrderQty[OrderQty],GPS.Rate[Rate],GPS.ItemTotal[ItemTotal],GPS.Discount_Per[Discount_Per],";
            SSQL = SSQL + "GPS.Discount[Discount],GPS.OtherCharge[OtherCharge],GPS.CGSTPer[CGSTPer],GPS.CGSTAmount[CGSTAmount],GPS.SGSTPer[SGSTPer],GPS.SGSTAmount[SGSTAmount],";
            SSQL = SSQL + "GPS.IGSTPer[IGSTPer],GPS.IGSTAmount[IGSTAmount],GPS.LineTotal[LineTotal],GPS.Remarks[Remarks] from General_Purchase_Order_Main GPM ";
            SSQL = SSQL + "Inner join General_Purchase_Order_Main_Sub GPS on GPM.Gen_PO_No=GPS.Gen_PO_No ";
            SSQL = SSQL + "Inner Join AdminRights CMP on CMP.Ccode=GPM.Ccode Inner Join MstSupplier SUP on SUP.SuppCode=GPM.Supp_Code and SUP.Ccode=GPM.Ccode";
            SSQL = SSQL + " Where GPM.Ccode='" + SessionCcode + "' And GPM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And GPM.FinYearCode='" + SessionFinYearCode + "' And GPS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And GPS.Lcode='" + SessionLcode + "' And GPS.FinYearCode='" + SessionFinYearCode + "' ";// And GPM.PO_Status='1'";

            if (GenPurOrdNo != "")
            {
                SSQL = SSQL + " And GPM.Gen_PO_No='" + GenPurOrdNo + "'";
            }

            if (FromDate != "" && ToDate != "" && GenPurOrdNo == "")
            {

                SSQL = SSQL + " and CONVERT(DATETIME,GPM.Gen_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GPM.Gen_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }

            if (DeptName != "")
            {
                SSQL = SSQL + " And GPM.DeptName='" + DeptName + "'";
            }

            if (SupplierName != "")
            {
                SSQL = SSQL + " And GPM.Supp_Name='" + SupplierName + "'";
            }

            if (ItemName != "")
            {
                SSQL = SSQL + " And GPS.ItemName='" + ItemName + "'";
            }

            GenPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (GenPurDT.Rows.Count > 0)
            {

                DataTable DtTst = new DataTable();
                DtTst =GenPurDT;
                for (int i = DtTst.Rows.Count; i < 7; i++)
                {
                    DtTst.NewRow();
                    DtTst.Rows.Add();

                    DtTst.Rows[i]["StdPONo"] = DtTst.Rows[0]["StdPONo"].ToString();
                    DtTst.Rows[i]["StdPODate"] = DtTst.Rows[0]["StdPODate"].ToString();
                    DtTst.Rows[i]["TotDisc"] = DtTst.Rows[0]["TotDisc"].ToString();
                    DtTst.Rows[i]["PackingAmt"] = DtTst.Rows[0]["PackingAmt"].ToString();
                    DtTst.Rows[i]["AddOrLess"] = DtTst.Rows[0]["AddOrLess"].ToString();
                    DtTst.Rows[i]["NetAmount"] = DtTst.Rows[0]["NetAmount"].ToString();
                }
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(DtTst);

                if (DtTst.Rows[0]["CompanyName"].ToString() == "Kumaran Gin & Pressing Private Limited")
                {
                    RD_Report.Load(Server.MapPath("~/crystal/GeneralPurchaseOrderInvoiceFormatNewUnit1.rpt"));
                }
                else
                {
                    RD_Report.Load(Server.MapPath("~/crystal/GeneralPurchaseOrderInvoiceFormatNewUnit2.rpt"));
                }
                
                RD_Report.SetDataSource(ds1.Tables[0]);
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }

    }
    
    //General Purchase Order Invoice Format Code End

    //Stock Transfer Details Report Code Start
    public void Stock_Transfer_Details_Report()
    {

        if (RptName == "Stock Transfer Details Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            if (FromDate != "" && ToDate != "" && StckTranNo == "")
            {

                SSQL = "select MSM.Mat_Stock_Transfer_No,MSM.Mat_Stock_Transfer_Date,MSM.From_WarehouseName,MSM.To_WarehouseName,";
                SSQL = SSQL + "MSM.CostCenterName,MSM.Preparedby,MSM.Others,";
                SSQL = SSQL + "MSS.ItemCode,MSS.ItemName,MSS.UOMCode,MSS.ValueType,";
                SSQL = SSQL + "MSS.FromZone,MSS.ToZone,MSS.FromBin,MSS.ToBin,";
                SSQL = SSQL + "MSS.TransferQty,MSS.Value";
                SSQL = SSQL + " from Meterial_Stock_Transfer_Main MSM inner join Meterial_Stock_Transfer_Main_Sub MSS ";
                SSQL = SSQL + "on MSM.Mat_Stock_Transfer_No=MSS.Mat_Stock_Transfer_No where CONVERT(DATETIME,MSM.Mat_Stock_Transfer_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,MSM.Mat_Stock_Transfer_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (WareHouse != "")
                {
                    SSQL = SSQL + " And MSM.From_WarehouseName='" + WareHouse + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MSS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And MSM.Ccode='" + SessionCcode + "' And MSM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MSM.FinYearCode='" + SessionFinYearCode + "' And MSS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MSS.Lcode='" + SessionLcode + "' And MSS.FinYearCode='" + SessionFinYearCode + "' And MSM.Mat_Stock_Transfer_Status='1'";

                StockTransDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (FromDate != "" && ToDate != "" && StckTranNo != "")
            {

                SSQL = "select MSM.Mat_Stock_Transfer_No,MSM.Mat_Stock_Transfer_Date,MSM.From_WarehouseName,MSM.To_WarehouseName,";
                SSQL = SSQL + "MSM.CostCenterName,MSM.Preparedby,MSM.Others,";
                SSQL = SSQL + "MSS.ItemCode,MSS.ItemName,MSS.UOMCode,MSS.ValueType,";
                SSQL = SSQL + "MSS.FromZone,MSS.ToZone,MSS.FromBin,MSS.ToBin,";
                SSQL = SSQL + "MSS.TransferQty,MSS.Value";
                SSQL = SSQL + " from Meterial_Stock_Transfer_Main MSM inner join Meterial_Stock_Transfer_Main_Sub MSS ";
                SSQL = SSQL + "on MSM.Mat_Stock_Transfer_No=MSS.Mat_Stock_Transfer_No where MSM.Mat_Stock_Transfer_No='" + StckTranNo + "' And CONVERT(DATETIME,MSM.Mat_Stock_Transfer_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,MSM.Mat_Stock_Transfer_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                if (WareHouse != "")
                {
                    SSQL = SSQL + " And MSM.From_WarehouseName='" + WareHouse + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MSS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And MSM.Ccode='" + SessionCcode + "' And MSM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MSM.FinYearCode='" + SessionFinYearCode + "' And MSS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MSS.Lcode='" + SessionLcode + "' And MSS.FinYearCode='" + SessionFinYearCode + "' And MSM.Mat_Stock_Transfer_Status='1'";

                StockTransDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (StckTranNo != "")
            {

                SSQL = "select MSM.Mat_Stock_Transfer_No,MSM.Mat_Stock_Transfer_Date,MSM.From_WarehouseName,MSM.To_WarehouseName,";
                SSQL = SSQL + "MSM.CostCenterName,MSM.Preparedby,MSM.Others,";
                SSQL = SSQL + "MSS.ItemCode,MSS.ItemName,MSS.UOMCode,MSS.ValueType,";
                SSQL = SSQL + "MSS.FromZone,MSS.ToZone,MSS.FromBin,MSS.ToBin,";
                SSQL = SSQL + "MSS.TransferQty,MSS.Value";
                SSQL = SSQL + " from Meterial_Stock_Transfer_Main MSM inner join Meterial_Stock_Transfer_Main_Sub MSS ";
                SSQL = SSQL + "on MSM.Mat_Stock_Transfer_No=MSS.Mat_Stock_Transfer_No where MSM.Mat_Stock_Transfer_No='" + StckTranNo + "'";


                if (WareHouse != "")
                {
                    SSQL = SSQL + " And MSM.From_WarehouseName='" + WareHouse + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MSS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And MSM.Ccode='" + SessionCcode + "' And MSM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MSM.FinYearCode='" + SessionFinYearCode + "' And MSS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MSS.Lcode='" + SessionLcode + "' And MSS.FinYearCode='" + SessionFinYearCode + "' And MSM.Mat_Stock_Transfer_Status='1'";

                StockTransDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (WareHouse != "")
            {

                SSQL = "select MSM.Mat_Stock_Transfer_No,MSM.Mat_Stock_Transfer_Date,MSM.From_WarehouseName,MSM.To_WarehouseName,";
                SSQL = SSQL + "MSM.CostCenterName,MSM.Preparedby,MSM.Others,";
                SSQL = SSQL + "MSS.ItemCode,MSS.ItemName,MSS.UOMCode,MSS.ValueType,";
                SSQL = SSQL + "MSS.FromZone,MSS.ToZone,MSS.FromBin,MSS.ToBin,";
                SSQL = SSQL + "MSS.TransferQty,MSS.Value";
                SSQL = SSQL + " from Meterial_Stock_Transfer_Main MSM inner join Meterial_Stock_Transfer_Main_Sub MSS ";
                SSQL = SSQL + "on MSM.Mat_Stock_Transfer_No=MSS.Mat_Stock_Transfer_No where MSM.From_WarehouseName='" + WareHouse + "'";


                if (StckTranNo != "")
                {
                    SSQL = SSQL + " And MSM.Mat_Stock_Transfer_No='" + StckTranNo + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And MSS.ItemName='" + ItemName + "'";
                }


                SSQL = SSQL + " And MSM.Ccode='" + SessionCcode + "' And MSM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MSM.FinYearCode='" + SessionFinYearCode + "' And MSS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MSS.Lcode='" + SessionLcode + "' And MSS.FinYearCode='" + SessionFinYearCode + "' And MSM.Mat_Stock_Transfer_Status='1'";

                StockTransDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else if (ItemName != "")
            {

                SSQL = "select MSM.Mat_Stock_Transfer_No,MSM.Mat_Stock_Transfer_Date,MSM.From_WarehouseName,MSM.To_WarehouseName,";
                SSQL = SSQL + "MSM.CostCenterName,MSM.Preparedby,MSM.Others,";
                SSQL = SSQL + "MSS.ItemCode,MSS.ItemName,MSS.UOMCode,MSS.ValueType,";
                SSQL = SSQL + "MSS.FromZone,MSS.ToZone,MSS.FromBin,MSS.ToBin,";
                SSQL = SSQL + "MSS.TransferQty,MSS.Value";
                SSQL = SSQL + " from Meterial_Stock_Transfer_Main MSM inner join Meterial_Stock_Transfer_Main_Sub MSS ";
                SSQL = SSQL + "on MSM.Mat_Stock_Transfer_No=MSS.Mat_Stock_Transfer_No where MSS.ItemName='" + ItemName + "'";


                if (StckTranNo != "")
                {
                    SSQL = SSQL + " And MSM.Mat_Stock_Transfer_No='" + StckTranNo + "'";
                }

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And MSM.From_WarehouseName='" + WareHouse + "'";
                }


                SSQL = SSQL + " And MSM.Ccode='" + SessionCcode + "' And MSM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MSM.FinYearCode='" + SessionFinYearCode + "' And MSS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MSS.Lcode='" + SessionLcode + "' And MSS.FinYearCode='" + SessionFinYearCode + "' And MSM.Mat_Stock_Transfer_Status='1'";

                StockTransDT = objdata.RptEmployeeMultipleDetails(SSQL);


            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (StockTransDT.Rows.Count > 0)
            {

                DataTable dtdSuppAddress = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDTStockTrans.Columns.Add("CompanyName");
                AutoDTStockTrans.Columns.Add("LocationName");
                AutoDTStockTrans.Columns.Add("CompanyCode");
                AutoDTStockTrans.Columns.Add("LocationCode");
                AutoDTStockTrans.Columns.Add("Address1");
                AutoDTStockTrans.Columns.Add("Address2");

                AutoDTStockTrans.Columns.Add("MatStckTransNo");
                AutoDTStockTrans.Columns.Add("MatStckTransDate");
                AutoDTStockTrans.Columns.Add("FrmWarehouName");
                AutoDTStockTrans.Columns.Add("ToWarehouName");
                AutoDTStockTrans.Columns.Add("CstCentName");
                AutoDTStockTrans.Columns.Add("Preparedby");
                AutoDTStockTrans.Columns.Add("Others");

                AutoDTStockTrans.Columns.Add("ItemCode");
                AutoDTStockTrans.Columns.Add("ItemName");
                AutoDTStockTrans.Columns.Add("UomCode");
                AutoDTStockTrans.Columns.Add("ValueType");
                AutoDTStockTrans.Columns.Add("FromZone");
                AutoDTStockTrans.Columns.Add("ToZone");
                AutoDTStockTrans.Columns.Add("FromBin");
                AutoDTStockTrans.Columns.Add("ToBin");
                AutoDTStockTrans.Columns.Add("TransferQty");
                AutoDTStockTrans.Columns.Add("Value");



                for (int i = 0; i < StockTransDT.Rows.Count; i++)
                {
                    AutoDTStockTrans.NewRow();
                    AutoDTStockTrans.Rows.Add();

                    AutoDTStockTrans.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTStockTrans.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTStockTrans.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTStockTrans.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTStockTrans.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTStockTrans.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                    AutoDTStockTrans.Rows[i]["MatStckTransNo"] = StockTransDT.Rows[i]["Mat_Stock_Transfer_No"];
                    AutoDTStockTrans.Rows[i]["MatStckTransDate"] = StockTransDT.Rows[i]["Mat_Stock_Transfer_Date"];
                    AutoDTStockTrans.Rows[i]["FrmWarehouName"] = StockTransDT.Rows[i]["From_WarehouseName"];
                    AutoDTStockTrans.Rows[i]["ToWarehouName"] = StockTransDT.Rows[i]["To_WarehouseName"];
                    AutoDTStockTrans.Rows[i]["CstCentName"] = StockTransDT.Rows[i]["CostCenterName"];
                    AutoDTStockTrans.Rows[i]["Preparedby"] = StockTransDT.Rows[i]["Preparedby"];
                    AutoDTStockTrans.Rows[i]["Others"] = StockTransDT.Rows[i]["Others"];

                    AutoDTStockTrans.Rows[i]["ItemCode"] = StockTransDT.Rows[i]["ItemCode"];
                    AutoDTStockTrans.Rows[i]["ItemName"] = StockTransDT.Rows[i]["ItemName"];
                    AutoDTStockTrans.Rows[i]["UomCode"] = StockTransDT.Rows[i]["UOMCode"];
                    AutoDTStockTrans.Rows[i]["ValueType"] = StockTransDT.Rows[i]["ValueType"];
                    AutoDTStockTrans.Rows[i]["FromZone"] = StockTransDT.Rows[i]["FromZone"];
                    AutoDTStockTrans.Rows[i]["ToZone"] = StockTransDT.Rows[i]["ToZone"];
                    AutoDTStockTrans.Rows[i]["FromBin"] = StockTransDT.Rows[i]["FromBin"];
                    AutoDTStockTrans.Rows[i]["ToBin"] = StockTransDT.Rows[i]["ToBin"];
                    AutoDTStockTrans.Rows[i]["TransferQty"] = StockTransDT.Rows[i]["TransferQty"];
                    AutoDTStockTrans.Rows[i]["Value"] = StockTransDT.Rows[i]["Value"];


                }

                ds.Tables.Add(AutoDTStockTrans);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("~/crystal/StockTransferDetails.rpt"));
                report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = report;

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //Stock Transfer Details Report Code End

    //Stock Details Report Code Start
    //public void Stock_Details_Report()
    //{

    //    if (RptName == "Stock Details Report")
    //    {

    //        if (WareHouse != "")
    //        {

    //            SSQL = "select ItemCode,ItemName,Stock_Qty,Stock_Value,";
    //            SSQL = SSQL + "WarehouseName,ZoneName,BinName";
    //            SSQL = SSQL + " from Stock_Current_All where WarehouseName='" + WareHouse + "'";

    //            if (ItemName != "")
    //            {
    //                SSQL = SSQL + " And ItemName='" + ItemName + "'";
    //            }


    //            SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    //            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "'";

    //            StockDetDT = objdata.RptEmployeeMultipleDetails(SSQL);

    //        }
    //        else if (ItemName != "")
    //        {

    //            SSQL = "select ItemCode,ItemName,Stock_Qty,Stock_Value,";
    //            SSQL = SSQL + "WarehouseName,ZoneName,BinName";
    //            SSQL = SSQL + " from Stock_Current_All where ItemName='" + ItemName + "'";

    //            if (WareHouse != "")
    //            {
    //                SSQL = SSQL + " And WarehouseName='" + WareHouse + "'";
    //            }


    //            SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    //            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "'";

    //            StockDetDT = objdata.RptEmployeeMultipleDetails(SSQL);

    //        }

    //        else
    //        {
    //            SSQL = "select ItemCode,ItemName,Stock_Qty,Stock_Value,";
    //            SSQL = SSQL + "WarehouseName,ZoneName,BinName";
    //            SSQL = SSQL + " from Stock_Current_All where";
    //            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    //            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "'";

    //            StockDetDT = objdata.RptEmployeeMultipleDetails(SSQL);
    //        }

    //        if (StockDetDT.Rows.Count > 0)
    //        {

    //            DataTable dtdSuppAddress = new DataTable();
    //            DataTable dtdCompanyAddress = new DataTable();
    //            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
    //            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

    //            AutoDTStockDetails.Columns.Add("CompanyName");
    //            AutoDTStockDetails.Columns.Add("LocationName");
    //            AutoDTStockDetails.Columns.Add("CompanyCode");
    //            AutoDTStockDetails.Columns.Add("LocationCode");
    //            AutoDTStockDetails.Columns.Add("Address1");
    //            AutoDTStockDetails.Columns.Add("Address2");


    //            AutoDTStockDetails.Columns.Add("ItemCode");
    //            AutoDTStockDetails.Columns.Add("ItemName");
    //            AutoDTStockDetails.Columns.Add("StockQty");
    //            AutoDTStockDetails.Columns.Add("StockValue");
    //            AutoDTStockDetails.Columns.Add("WarehouseName");
    //            AutoDTStockDetails.Columns.Add("ZoneName");
    //            AutoDTStockDetails.Columns.Add("BinName");




    //            for (int i = 0; i < StockDetDT.Rows.Count; i++)
    //            {
    //                AutoDTStockDetails.NewRow();
    //                AutoDTStockDetails.Rows.Add();

    //                AutoDTStockDetails.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
    //                AutoDTStockDetails.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
    //                AutoDTStockDetails.Rows[i]["CompanyCode"] = SessionCcode;
    //                AutoDTStockDetails.Rows[i]["LocationCode"] = SessionLcode;
    //                AutoDTStockDetails.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
    //                AutoDTStockDetails.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


    //                AutoDTStockDetails.Rows[i]["ItemCode"] = StockDetDT.Rows[i]["ItemCode"];
    //                AutoDTStockDetails.Rows[i]["ItemName"] = StockDetDT.Rows[i]["ItemName"];
    //                AutoDTStockDetails.Rows[i]["StockQty"] = StockDetDT.Rows[i]["Stock_Qty"];
    //                AutoDTStockDetails.Rows[i]["StockValue"] = StockDetDT.Rows[i]["Stock_Value"];
    //                AutoDTStockDetails.Rows[i]["WarehouseName"] = StockDetDT.Rows[i]["WarehouseName"];
    //                AutoDTStockDetails.Rows[i]["ZoneName"] = StockDetDT.Rows[i]["ZoneName"];
    //                AutoDTStockDetails.Rows[i]["BinName"] = StockDetDT.Rows[i]["BinName"];


    //            }

    //            ds.Tables.Add(AutoDTStockDetails);
    //            ReportDocument report = new ReportDocument();
    //            report.Load(Server.MapPath("~/crystal/StockDetails.rpt"));
    //            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
    //            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

    //            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


    //            CrystalReportViewer1.ReportSource = report;

    //        }

    //        else
    //        {
    //            Errflag = false;
    //            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
    //        }
    //        if (!Errflag)
    //        {
    //            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
    //            lblUploadSuccessfully.Text = "No Records Matched..";
    //        }

    //    }
    //}
    //Stock Details Report Code End

    //Critical Stock Details Report Code Start
    public void Critical_Stock_Details_Report()
    {

        if (RptName == "Critical Stock Details Report")
        {

            if (WareHouse != "-Select-")
            {

                SSQL = "select SC.ItemCode,SC.ItemName,SC.Stock_Qty,SC.WarehouseName,";
                SSQL = SSQL + "SC.ZoneName,SC.BinName,IM.MinQtyLevel from Stock_Current_All SC ";
                SSQL = SSQL + "inner join MstItemMaster IM on SC.ItemCode=IM.ItemCode where SC.Stock_Qty < IM.MinQtyLevel And SC.WarehouseName='" + WareHouse + "'";
                SSQL = SSQL + " And SC.Ccode='" + SessionCcode + "' And SC.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And SC.FinYearCode='" + SessionFinYearCode + "' And IM.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And IM.Lcode='" + SessionLcode + "'";

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And SC.DeptName='" + DeptName + "'";
                }
                if (CostCenterName != "-Select-")
                {
                    SSQL = SSQL + " And IM.CostCenterName='" + CostCenterName + "'";
                }
                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And SC.WareHouseName='" + WareHouse + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And SC.ItemName='" + ItemName + "'";
                }

                CriticalStockDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }


            else
            {
                SSQL = "select SC.ItemCode,SC.ItemName,SC.Stock_Qty,SC.WarehouseName,";
                SSQL = SSQL + "SC.ZoneName,SC.BinName,IM.MinQtyLevel from Stock_Current_All SC ";
                SSQL = SSQL + "inner join MstItemMaster IM on SC.ItemCode=IM.ItemCode where SC.Stock_Qty < IM.MinQtyLevel";
                SSQL = SSQL + " And SC.Ccode='" + SessionCcode + "' And SC.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And SC.FinYearCode='" + SessionFinYearCode + "' And IM.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And IM.Lcode='" + SessionLcode + "'";

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And SC.DeptName='" + DeptName + "'";
                }
                if (CostCenterName != "-Select-")
                {
                    SSQL = SSQL + " And SC.CostCenterName='" + CostCenterName + "'";
                }
                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And SC.WareHouseName='" + WareHouse + "'";
                }

                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And SC.ItemName='" + ItemName + "'";
                }

                CriticalStockDT = objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (CriticalStockDT.Rows.Count > 0)
            {

                DataTable dtdSuppAddress = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDTCriticalStockDetails.Columns.Add("CompanyName");
                AutoDTCriticalStockDetails.Columns.Add("LocationName");
                AutoDTCriticalStockDetails.Columns.Add("CompanyCode");
                AutoDTCriticalStockDetails.Columns.Add("LocationCode");
                AutoDTCriticalStockDetails.Columns.Add("Address1");
                AutoDTCriticalStockDetails.Columns.Add("Address2");

                AutoDTCriticalStockDetails.Columns.Add("ItemCode");
                AutoDTCriticalStockDetails.Columns.Add("ItemName");
                AutoDTCriticalStockDetails.Columns.Add("StockQty");
                AutoDTCriticalStockDetails.Columns.Add("WarehouseName");
                AutoDTCriticalStockDetails.Columns.Add("ZoneName");
                AutoDTCriticalStockDetails.Columns.Add("BinName");
                AutoDTCriticalStockDetails.Columns.Add("MinQtyLevel");


                for (int i = 0; i < CriticalStockDT.Rows.Count; i++)
                {
                    AutoDTCriticalStockDetails.NewRow();
                    AutoDTCriticalStockDetails.Rows.Add();

                    AutoDTCriticalStockDetails.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTCriticalStockDetails.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTCriticalStockDetails.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTCriticalStockDetails.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTCriticalStockDetails.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTCriticalStockDetails.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDTCriticalStockDetails.Rows[i]["ItemCode"] = CriticalStockDT.Rows[i]["ItemCode"];
                    AutoDTCriticalStockDetails.Rows[i]["ItemName"] = CriticalStockDT.Rows[i]["ItemName"];
                    AutoDTCriticalStockDetails.Rows[i]["StockQty"] = CriticalStockDT.Rows[i]["Stock_Qty"];
                    AutoDTCriticalStockDetails.Rows[i]["WarehouseName"] = CriticalStockDT.Rows[i]["WarehouseName"];
                    AutoDTCriticalStockDetails.Rows[i]["ZoneName"] = CriticalStockDT.Rows[i]["ZoneName"];
                    AutoDTCriticalStockDetails.Rows[i]["BinName"] = CriticalStockDT.Rows[i]["BinName"];
                    AutoDTCriticalStockDetails.Rows[i]["MinQtyLevel"] = CriticalStockDT.Rows[i]["MinQtyLevel"];

                }

                ds.Tables.Add(AutoDTCriticalStockDetails);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("~/crystal/CriticalStockDetails.rpt"));
                report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = report;

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //Critical Stock Details Report Code End

    //Non Movable Item Details Report Code Start
    public void NonMovable_Item_Details_Report()
    {

        if (RptName == "Non Movable Item Details Report")
        {


            //SSQL = "select CS.ItemCode,CS.ItemName as ItemShortName,IM.PurchaseUOM,CS.Stock_Qty as ItemType, CS.Stock_Value as ValuationType";
            //SSQL = SSQL + " from Stock_Current_All CS left outer join Meterial_Issue_Main_Sub MI ";
            //SSQL = SSQL + " on CS.ItemCode=MI.ItemCode ";
            //SSQL = SSQL + " inner join MstItemMaster IM on IM.ItemCode=CS.ItemCode";
            //    SSQL = SSQL + " where MI.ItemCode is null";

            SSQL = "Select SCA.ItemCode as [ItemCode],SCA.ItemName as [ItemShortName],IM.PurchaseUOM as [PurchaseUOM], ";
            SSQL = SSQL + "SUM(SCA.Add_Qty)-SUM(sca.Minus_Qty) As [Qty] ,SUM(SCA.Add_Value)-SUM(sca.Minus_Value) as [Val]  from Stock_Ledger_All SCA ";
            SSQL = SSQL + "inner join MstItemMaster IM on IM.ItemCode=SCA.ItemCode ";
            SSQL = SSQL + "where sca.ItemName Not in ";
            SSQL = SSQL + "(select ItemName from Meterial_Issue_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";

            if (WareHouse != "-Select-")
            {
                SSQL = SSQL + " And WarehouseName='" + WareHouse + "'";
            }

            if (DeptName != "-Select-")
            {
                SSQL = SSQL + " And DeptName='" + DeptName + "'";
            }

            if (CostCenterName != "-Select-")
            {
                SSQL = SSQL + " And CostCenterName='" + CostCenterName + "'";
            }

            if (ItemName != "-Select-")
            {
                SSQL = SSQL + " And ItemName='" + ItemName + "'";
            }

            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "')";

            if (WareHouse != "-Select-")
            {
                SSQL = SSQL + " And SCA.WarehouseName='" + WareHouse + "'";
            }

            if (DeptName != "-Select-")
            {
                SSQL = SSQL + " And SCA.DeptName='" + DeptName + "'";
            }

            if (CostCenterName != "-Select-")
            {
                SSQL = SSQL + " And SCA.CostCenterName='" + CostCenterName + "'";
            }

            if (ItemName != "-Select-")
            {
                SSQL = SSQL + " And SCA.ItemName='" + ItemName + "'";
            }

            SSQL = SSQL + " And SCA.Ccode='" + SessionCcode + "' And SCA.Lcode='" + SessionLcode + "' And SCA.FinYearCode='" + SessionFinYearCode + "'";

            SSQL = SSQL += " Group by SCA.ItemName,SCA.ItemCode,IM.PurchaseUOM,SCA.ItemCode,IM.PurchaseUOM having SUM(SCA.Add_Qty)-SUM(sca.Minus_Qty)>0 ";


            NonMovableItemDT = objdata.RptEmployeeMultipleDetails(SSQL);


            if (NonMovableItemDT.Rows.Count > 0)
            {

                DataTable dtdSuppAddress = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDTNonMvbleItem.Columns.Add("CompanyName");
                AutoDTNonMvbleItem.Columns.Add("LocationName");
                AutoDTNonMvbleItem.Columns.Add("CompanyCode");
                AutoDTNonMvbleItem.Columns.Add("LocationCode");
                AutoDTNonMvbleItem.Columns.Add("Address1");
                AutoDTNonMvbleItem.Columns.Add("Address2");

                AutoDTNonMvbleItem.Columns.Add("ItemCode");
                AutoDTNonMvbleItem.Columns.Add("ItemName");
                AutoDTNonMvbleItem.Columns.Add("UomCode");
                AutoDTNonMvbleItem.Columns.Add("ItemType");
                AutoDTNonMvbleItem.Columns.Add("ValuationType");



                for (int i = 0; i < NonMovableItemDT.Rows.Count; i++)
                {
                    AutoDTNonMvbleItem.NewRow();
                    AutoDTNonMvbleItem.Rows.Add();

                    AutoDTNonMvbleItem.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTNonMvbleItem.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTNonMvbleItem.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTNonMvbleItem.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTNonMvbleItem.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTNonMvbleItem.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDTNonMvbleItem.Rows[i]["ItemCode"] = NonMovableItemDT.Rows[i]["ItemCode"];
                    AutoDTNonMvbleItem.Rows[i]["ItemName"] = NonMovableItemDT.Rows[i]["ItemShortName"];
                    AutoDTNonMvbleItem.Rows[i]["UomCode"] = NonMovableItemDT.Rows[i]["PurchaseUOM"];
                    AutoDTNonMvbleItem.Rows[i]["ItemType"] = NonMovableItemDT.Rows[i]["Qty"];
                    AutoDTNonMvbleItem.Rows[i]["ValuationType"] = NonMovableItemDT.Rows[i]["Val"];


                }

                ds.Tables.Add(AutoDTNonMvbleItem);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("~/crystal/NonmovableItemDetails.rpt"));
                report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = report;

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //Non Movable Item Details Report Code End

    //Single Item History Full Details Report Code Start 
    public void Single_Item_History_Full_Details_Report()
    {

        if (RptName == "Single Item History Full Details Report")
        {
            closingstock = 0;
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            if (FromDate != "" && ToDate != "" && ItemName != "")
            {

                SSQL = "select Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,";
                SSQL = SSQL + "ItemCode,ItemName,Open_Qty,PO_Qty,";
                SSQL = SSQL + "General_Qty,Issue_Qty,Issue_Return_Qty,GP_IN_Qty,";
                SSQL = SSQL + "GP_OUT_Qty,Transfer_Qty,WarehouseName,ZoneName,BinName,";
                SSQL = SSQL + "Supp_Name from Stock_Transaction_Ledger";
                SSQL = SSQL + " where ItemCode='" + ItemName + "' And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


                SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Trans_Type <> 'STOCK TRANSFER' Order by Trans_Date";


                SingleItemDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else if (ItemName != "")
            {

                SSQL = "select Trans_No,Trans_Date,Trans_Date_Str,Trans_Type,";
                SSQL = SSQL + "ItemCode,ItemName,Open_Qty,PO_Qty,";
                SSQL = SSQL + "General_Qty,Issue_Qty,Issue_Return_Qty,GP_IN_Qty,";
                SSQL = SSQL + "GP_OUT_Qty,Transfer_Qty,WarehouseName,ZoneName,BinName,";
                SSQL = SSQL + "Supp_Name from Stock_Transaction_Ledger";
                SSQL = SSQL + " where ItemCode='" + ItemName + "'";
                SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Trans_Type <> 'STOCK TRANSFER' Order by Trans_Date";


                SingleItemDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (SingleItemDT.Rows.Count > 0)
            {

                DataTable MaterialTknBy = new DataTable();
                DataTable dtdSuppAddress = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTSingleItem.Columns.Add("CompanyName");
                AutoDTSingleItem.Columns.Add("LocationName");
                AutoDTSingleItem.Columns.Add("CompanyCode");
                AutoDTSingleItem.Columns.Add("LocationCode");
                AutoDTSingleItem.Columns.Add("Address1");
                AutoDTSingleItem.Columns.Add("Address2");

                AutoDTSingleItem.Columns.Add("TransNo");
                AutoDTSingleItem.Columns.Add("TransDate");
                AutoDTSingleItem.Columns.Add("TransDateStr");
                AutoDTSingleItem.Columns.Add("TransType");
                AutoDTSingleItem.Columns.Add("ItemCode");
                AutoDTSingleItem.Columns.Add("ItemName");
                AutoDTSingleItem.Columns.Add("OpenQty");
                AutoDTSingleItem.Columns.Add("POQty");
                AutoDTSingleItem.Columns.Add("GeneralQty");
                AutoDTSingleItem.Columns.Add("IssueQty");
                AutoDTSingleItem.Columns.Add("IssueReturnQty");
                AutoDTSingleItem.Columns.Add("GPINQty");
                AutoDTSingleItem.Columns.Add("GPOUTQty");
                AutoDTSingleItem.Columns.Add("TransferQty");
                AutoDTSingleItem.Columns.Add("WarehouseName");
                AutoDTSingleItem.Columns.Add("ZoneName");
                AutoDTSingleItem.Columns.Add("BinName");
                AutoDTSingleItem.Columns.Add("SuppName");
                AutoDTSingleItem.Columns.Add("TakenBy");
                AutoDTSingleItem.Columns.Add("ClosingStock");


                for (int i = 0; i < SingleItemDT.Rows.Count; i++)
                {
                    AutoDTSingleItem.NewRow();
                    AutoDTSingleItem.Rows.Add();

                    AutoDTSingleItem.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTSingleItem.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTSingleItem.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTSingleItem.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTSingleItem.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTSingleItem.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                    AutoDTSingleItem.Rows[i]["TransNo"] = SingleItemDT.Rows[i]["Trans_No"];
                    AutoDTSingleItem.Rows[i]["TransDate"] = SingleItemDT.Rows[i]["Trans_Date"];
                    AutoDTSingleItem.Rows[i]["TransDateStr"] = SingleItemDT.Rows[i]["Trans_Date_Str"];
                    AutoDTSingleItem.Rows[i]["TransType"] = SingleItemDT.Rows[i]["Trans_Type"];
                    AutoDTSingleItem.Rows[i]["ItemCode"] = SingleItemDT.Rows[i]["ItemCode"];
                    AutoDTSingleItem.Rows[i]["ItemName"] = SingleItemDT.Rows[i]["ItemName"];

                    AutoDTSingleItem.Rows[i]["OpenQty"] = SingleItemDT.Rows[i]["Open_Qty"];
                    AutoDTSingleItem.Rows[i]["POQty"] = SingleItemDT.Rows[i]["PO_Qty"];
                    AutoDTSingleItem.Rows[i]["GeneralQty"] = SingleItemDT.Rows[i]["General_Qty"];
                    AutoDTSingleItem.Rows[i]["IssueQty"] = SingleItemDT.Rows[i]["Issue_Qty"];
                    AutoDTSingleItem.Rows[i]["IssueReturnQty"] = SingleItemDT.Rows[i]["Issue_Return_Qty"];
                    AutoDTSingleItem.Rows[i]["GPINQty"] = SingleItemDT.Rows[i]["GP_IN_Qty"];
                    AutoDTSingleItem.Rows[i]["GPOUTQty"] = SingleItemDT.Rows[i]["GP_OUT_Qty"];
                    AutoDTSingleItem.Rows[i]["TransferQty"] = SingleItemDT.Rows[i]["Transfer_Qty"];
                    AutoDTSingleItem.Rows[i]["WarehouseName"] = SingleItemDT.Rows[i]["WarehouseName"];
                    AutoDTSingleItem.Rows[i]["ZoneName"] = SingleItemDT.Rows[i]["ZoneName"];
                    AutoDTSingleItem.Rows[i]["BinName"] = SingleItemDT.Rows[i]["BinName"];
                    AutoDTSingleItem.Rows[i]["SuppName"] = SingleItemDT.Rows[i]["Supp_Name"];

                    if (AutoDTSingleItem.Rows[i]["TransType"].ToString() == "MATERIAL ISSUE")
                    {
                        SSQL = "";
                        SSQL = "Select Takenby from Meterial_Issue_Main where Mat_Issue_No='" + AutoDTSingleItem.Rows[i]["TransNo"].ToString() + "'";
                        MaterialTknBy = objdata.RptEmployeeMultipleDetails(SSQL);


                        if (MaterialTknBy.Rows.Count > 0)
                        {
                            AutoDTSingleItem.Rows[i]["TakenBy"] = MaterialTknBy.Rows[0]["Takenby"];
                        }
                        else
                        {
                            AutoDTSingleItem.Rows[i]["TakenBy"] = "";
                        }

                    }



                    closingstock = closingstock + Convert.ToDecimal(SingleItemDT.Rows[i]["Open_Qty"]) + Convert.ToDecimal(SingleItemDT.Rows[i]["PO_Qty"]) + Convert.ToDecimal(SingleItemDT.Rows[i]["General_Qty"]) + Convert.ToDecimal(SingleItemDT.Rows[i]["Issue_Return_Qty"]) + Convert.ToDecimal(SingleItemDT.Rows[i]["GP_IN_Qty"]) - Convert.ToDecimal(SingleItemDT.Rows[i]["Issue_Qty"]) - Convert.ToDecimal(SingleItemDT.Rows[i]["GP_OUT_Qty"]);
                    AutoDTSingleItem.Rows[i]["ClosingStock"] = closingstock;

                }

                ds.Tables.Add(AutoDTSingleItem);
                ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("~/crystal/SingleItemHistoryDetails.rpt"));
                report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = report;

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //Single Item History Full Details Report Code End

    //Purchase Return Details Report Code start
    public void Purchase_Return_Details_Report()
    {
        WareHouse = Request.QueryString["WareHouse"].ToString();
        DeptName = Request.QueryString["DeptName"].ToString();
        SupplierName = Request.QueryString["SupplierName"].ToString();
        PurRetNo = Request.QueryString["PurRetNo"].ToString();
        ItemName = Request.QueryString["ItemName"].ToString();
        FromDate = Request.QueryString["FromDate"].ToString();
        ToDate = Request.QueryString["ToDate"].ToString();



        if (RptName == "Purchase Return Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }
            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }
            if (FromDate != "" && ToDate != "" && PurRetNo == "")
            {
                SSQL = "select AM.Pur_Return_No,AM.Pur_Return_Date,AM.Supp_Name,AM.DCNo,AM.DCDate,AM.Others,";
                SSQL = SSQL + "AM.WarehouseName,AM.DeptName,AM.Note,AS1.ItemCode,AS1.ItemName,AS1.ReturnQty,AS1.Pur_Value,AS1.zoneName,AS1.BinName,AS1.Remarks,Total from Purc_Return_Main AM inner join Purc_Return_Main_Sub AS1 ";
                SSQL = SSQL + "on AM.Pur_Return_No=AS1.Pur_Return_No where CONVERT(DATETIME,AM.Pur_Return_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,AM.Pur_Return_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                if (ItemName != "")
                {
                    SSQL = SSQL + " And AS1.ItemName='" + ItemName + "'";
                }
                if (WareHouse != "")
                {
                    SSQL = SSQL + " And AM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And AM.DeptName='" + DeptName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And AS1.ItemName='" + ItemName + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + " And AM.Supp_Name='" + SupplierName + "'";
                }

                SSQL = SSQL + " And AM.Ccode='" + SessionCcode + "' And AM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And AM.FinYearCode='" + SessionFinYearCode + "' And AS1.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And AS1.Lcode='" + SessionLcode + "' And AS1.FinYearCode='" + SessionFinYearCode + "' And AM.PO_Status='1'";

                PurchaseReturn = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (FromDate != "" && ToDate != "" && PurRetNo != "")
            {
                SSQL = "select AM.Pur_Return_No,AM.Pur_Return_Date,AM.Supp_Name,AM.DCNo,AM.DCDate,AM.Others,";
                SSQL = SSQL + "AM.WarehouseName,AM.DeptName,AM.Note,AS1.ItemCode,AS1.ItemName,AS1.ReturnQty,AS1.Pur_Value,AS1.zoneName,AS1.BinName,AS1.Remarks,Total from Purc_Return_Main AM inner join Purc_Return_Main_Sub AS1 ";
                SSQL = SSQL + "on AM.Pur_Return_No=AS1.Pur_Return_No where CONVERT(DATETIME,AM.Pur_Return_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,AM.Pur_Return_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And AM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And AM.DeptName='" + DeptName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And AS1.ItemName='" + ItemName + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + " And AM.Supp_Name='" + SupplierName + "'";
                }

                SSQL = SSQL + " And AM.Ccode='" + SessionCcode + "' And AM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And AM.FinYearCode='" + SessionFinYearCode + "' And AS1.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And AS1.Lcode='" + SessionLcode + "' And AS1.FinYearCode='" + SessionFinYearCode + "' And AM.PO_Status='1'";
                SSQL = SSQL + " And AM.Pur_Return_No='" + PurRetNo.ToString() + "'";

                PurchaseReturn = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (PurRetNo != "")
            {
                SSQL = "select AM.Pur_Return_No,AM.Pur_Return_Date,AM.Supp_Name,AM.DCNo,AM.DCDate,AM.Others,";
                SSQL = SSQL + "AM.WarehouseName,AM.DeptName,AM.Note,AS1.ItemCode,AS1.ItemName,AS1.ReturnQty,AS1.Pur_Value,AS1.zoneName,AS1.BinName,AS1.Remarks,Total from Purc_Return_Main AM inner join Purc_Return_Main_Sub AS1 ";
                SSQL = SSQL + "on AM.Pur_Return_No=AS1.Pur_Return_No where AM.Pur_Return_No='" + PurRetNo.ToString() + "'";

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And AM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And AM.DeptName='" + DeptName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And AS1.ItemName='" + ItemName + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + " And AM.Supp_Name='" + SupplierName + "'";
                }

                SSQL = SSQL + " And AM.Ccode='" + SessionCcode + "' And AM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And AM.FinYearCode='" + SessionFinYearCode + "' And AS1.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And AS1.Lcode='" + SessionLcode + "' And AS1.FinYearCode='" + SessionFinYearCode + "' And AM.PO_Status='1'";


                PurchaseReturn = objdata.RptEmployeeMultipleDetails(SSQL);
            }



            else if (WareHouse != "" || DeptName != "" || ItemName != "" || SupplierName != "")
            {
                SSQL = "select AM.Pur_Return_No,AM.Pur_Return_Date,AM.Supp_Name,AM.DCNo,AM.DCDate,AM.Others,";
                SSQL = SSQL + "AM.WarehouseName,AM.DeptName,AM.Note,AS1.ItemCode,AS1.ItemName,AS1.ReturnQty,AS1.Pur_Value,AS1.zoneName,AS1.BinName,AS1.Remarks,Total from Purc_Return_Main AM inner join Purc_Return_Main_Sub AS1 ";
                SSQL = SSQL + "on AM.Pur_Return_No=AS1.Pur_Return_No where";
                if (WareHouse != "")
                {
                    SSQL = SSQL + " And AM.WarehouseName='" + WareHouse + "'";
                }

                else if (DeptName != "")
                {
                    SSQL = SSQL + " And AM.DeptName='" + DeptName + "'";
                }

                else if (ItemName != "")
                {
                    SSQL = SSQL + " And AS1.ItemName='" + ItemName + "'";
                }
                else if (SupplierName != "")
                {
                    SSQL = SSQL + " And AM.Supp_Name='" + SupplierName + "'";
                }

                SSQL = SSQL + " And AM.Ccode='" + SessionCcode + "' And AM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And AM.FinYearCode='" + SessionFinYearCode + "' And AS1.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And AS1.Lcode='" + SessionLcode + "' And AS1.FinYearCode='" + SessionFinYearCode + "' And AM.PO_Status='1'";

                PurchaseReturn = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (PurchaseReturn.Rows.Count > 0)
            {

                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDataTable.Columns.Add("CompanyName");
                AutoDataTable.Columns.Add("LocationName");
                AutoDataTable.Columns.Add("CompanyCode");
                AutoDataTable.Columns.Add("LocationCode");
                AutoDataTable.Columns.Add("Address1");
                AutoDataTable.Columns.Add("Address2");

                AutoDataTable.Columns.Add("PurReturnNo");
                AutoDataTable.Columns.Add("PurReturnDate");
                AutoDataTable.Columns.Add("SupplierName");
                AutoDataTable.Columns.Add("DocumentNo");
                AutoDataTable.Columns.Add("DocumentDate");
                AutoDataTable.Columns.Add("WarehouseName");
                AutoDataTable.Columns.Add("DeptName");
                AutoDataTable.Columns.Add("Others");

                AutoDataTable.Columns.Add("ItemCode");
                AutoDataTable.Columns.Add("ItemName");
                AutoDataTable.Columns.Add("ReturnQty");
                AutoDataTable.Columns.Add("Pur_Value");
                AutoDataTable.Columns.Add("zoneName");
                AutoDataTable.Columns.Add("BinName");
                AutoDataTable.Columns.Add("Remarks");
                AutoDataTable.Columns.Add("Total");


                for (int i = 0; i < PurchaseReturn.Rows.Count; i++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDataTable.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDataTable.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDataTable.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDataTable.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDataTable.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDataTable.Rows[i]["PurReturnNo"] = PurchaseReturn.Rows[i]["Pur_Return_No"];
                    AutoDataTable.Rows[i]["PurReturnDate"] = PurchaseReturn.Rows[i]["Pur_Return_Date"];
                    AutoDataTable.Rows[i]["SupplierName"] = PurchaseReturn.Rows[i]["Supp_Name"];
                    AutoDataTable.Rows[i]["DocumentNo"] = PurchaseReturn.Rows[i]["DCNo"];
                    AutoDataTable.Rows[i]["DocumentDate"] = PurchaseReturn.Rows[i]["DCDate"];
                    AutoDataTable.Rows[i]["WarehouseName"] = PurchaseReturn.Rows[i]["WarehouseName"];
                    AutoDataTable.Rows[i]["DeptName"] = PurchaseReturn.Rows[i]["DeptName"];
                    AutoDataTable.Rows[i]["Others"] = PurchaseReturn.Rows[i]["Others"];

                    AutoDataTable.Rows[i]["ItemCode"] = PurchaseReturn.Rows[i]["ItemCode"];
                    AutoDataTable.Rows[i]["ItemName"] = PurchaseReturn.Rows[i]["ItemName"];
                    AutoDataTable.Rows[i]["ReturnQty"] = PurchaseReturn.Rows[i]["ReturnQty"];
                    AutoDataTable.Rows[i]["Pur_Value"] = PurchaseReturn.Rows[i]["Pur_Value"];
                    AutoDataTable.Rows[i]["zoneName"] = PurchaseReturn.Rows[i]["zoneName"];
                    AutoDataTable.Rows[i]["BinName"] = PurchaseReturn.Rows[i]["BinName"];
                    AutoDataTable.Rows[i]["Remarks"] = PurchaseReturn.Rows[i]["Remarks"];
                    AutoDataTable.Rows[i]["Total"] = PurchaseReturn.Rows[i]["Total"];

                }

                //ds.Tables.Add(AutoDataTable);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/PurchaseReturnReport.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;


                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDataTable);
                RD_Report.Load(Server.MapPath("~/crystal/PurchaseReturnReport.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                //DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();


            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }
    }
    //Purchase Return Details Report Code End


    //Purchase Return Details Report Invoice Format Code start
    public void Purchase_Return_Details_Invoice_Format()
    {
        WareHouse = Request.QueryString["WareHouse"].ToString();
        DeptName = Request.QueryString["DeptName"].ToString();
        SupplierName = Request.QueryString["SupplierName"].ToString();
        PurRetNo = Request.QueryString["PurRetNo"].ToString();
        ItemName = Request.QueryString["ItemName"].ToString();
        FromDate = Request.QueryString["FromDate"].ToString();
        ToDate = Request.QueryString["ToDate"].ToString();



        if (RptName == "Purchase Return Invoice Format Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }
            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }
            if (FromDate != "" && ToDate != "" && PurRetNo == "")
            {
                SSQL = "select AM.Pur_Return_No,AM.Pur_Return_Date,AM.Supp_Name,AM.DCNo,AM.DCDate,AM.Others,";
                SSQL = SSQL + "AM.WarehouseName,AM.DeptName,AM.Note,AS1.ItemCode,AS1.ItemName,AS1.ReturnQty,AS1.Pur_Value,AS1.zoneName,AS1.BinName,AS1.Remarks,Total from Purc_Return_Main AM inner join Purc_Return_Main_Sub AS1 ";
                SSQL = SSQL + "on AM.Pur_Return_No=AS1.Pur_Return_No where CONVERT(DATETIME,AM.Pur_Return_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,AM.Pur_Return_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                if (ItemName != "")
                {
                    SSQL = SSQL + " And AS1.ItemName='" + ItemName + "'";
                }
                if (WareHouse != "")
                {
                    SSQL = SSQL + " And AM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And AM.DeptName='" + DeptName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And AS1.ItemName='" + ItemName + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + " And AM.Supp_Name='" + SupplierName + "'";
                }

                SSQL = SSQL + " And AM.Ccode='" + SessionCcode + "' And AM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And AM.FinYearCode='" + SessionFinYearCode + "' And AS1.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And AS1.Lcode='" + SessionLcode + "' And AS1.FinYearCode='" + SessionFinYearCode + "' And AM.PO_Status='1'";

                PurchaseReturn = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (FromDate != "" && ToDate != "" && PurRetNo != "")
            {
                SSQL = "select AM.Pur_Return_No,AM.Pur_Return_Date,AM.Supp_Name,AM.DCNo,AM.DCDate,AM.Others,";
                SSQL = SSQL + "AM.WarehouseName,AM.DeptName,AM.Note,AS1.ItemCode,AS1.ItemName,AS1.ReturnQty,AS1.Pur_Value,AS1.zoneName,AS1.BinName,AS1.Remarks,Total from Purc_Return_Main AM inner join Purc_Return_Main_Sub AS1 ";
                SSQL = SSQL + "on AM.Pur_Return_No=AS1.Pur_Return_No where CONVERT(DATETIME,AM.Pur_Return_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,AM.Pur_Return_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And AM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And AM.DeptName='" + DeptName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And AS1.ItemName='" + ItemName + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + " And AM.Supp_Name='" + SupplierName + "'";
                }

                SSQL = SSQL + " And AM.Ccode='" + SessionCcode + "' And AM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And AM.FinYearCode='" + SessionFinYearCode + "' And AS1.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And AS1.Lcode='" + SessionLcode + "' And AS1.FinYearCode='" + SessionFinYearCode + "' And AM.PO_Status='1'";
                SSQL = SSQL + " And AM.Pur_Return_No='" + PurRetNo.ToString() + "'";

                PurchaseReturn = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else if (PurRetNo != "")
            {
                SSQL = "select AM.Pur_Return_No,AM.Pur_Return_Date,AM.Supp_Name,AM.DCNo,AM.DCDate,AM.Others,";
                SSQL = SSQL + "AM.WarehouseName,AM.DeptName,AM.Note,AS1.ItemCode,AS1.ItemName,AS1.ReturnQty,AS1.Pur_Value,AS1.zoneName,AS1.BinName,AS1.Remarks,Total from Purc_Return_Main AM inner join Purc_Return_Main_Sub AS1 ";
                SSQL = SSQL + "on AM.Pur_Return_No=AS1.Pur_Return_No where AM.Pur_Return_No='" + PurRetNo.ToString() + "'";

                if (WareHouse != "")
                {
                    SSQL = SSQL + " And AM.WarehouseName='" + WareHouse + "'";
                }

                if (DeptName != "")
                {
                    SSQL = SSQL + " And AM.DeptName='" + DeptName + "'";
                }

                if (ItemName != "")
                {
                    SSQL = SSQL + " And AS1.ItemName='" + ItemName + "'";
                }
                if (SupplierName != "")
                {
                    SSQL = SSQL + " And AM.Supp_Name='" + SupplierName + "'";
                }

                SSQL = SSQL + " And AM.Ccode='" + SessionCcode + "' And AM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And AM.FinYearCode='" + SessionFinYearCode + "' And AS1.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And AS1.Lcode='" + SessionLcode + "' And AS1.FinYearCode='" + SessionFinYearCode + "' And AM.PO_Status='1'";


                PurchaseReturn = objdata.RptEmployeeMultipleDetails(SSQL);
            }



            else if (WareHouse != "" || DeptName != "" || ItemName != "" || SupplierName != "")
            {
                SSQL = "select AM.Pur_Return_No,AM.Pur_Return_Date,AM.Supp_Name,AM.DCNo,AM.DCDate,AM.Others,";
                SSQL = SSQL + "AM.WarehouseName,AM.DeptName,AM.Note,AS1.ItemCode,AS1.ItemName,AS1.ReturnQty,AS1.Pur_Value,AS1.zoneName,AS1.BinName,AS1.Remarks,Total from Purc_Return_Main AM inner join Purc_Return_Main_Sub AS1 ";
                SSQL = SSQL + "on AM.Pur_Return_No=AS1.Pur_Return_No where";
                if (WareHouse != "")
                {
                    SSQL = SSQL + " And AM.WarehouseName='" + WareHouse + "'";
                }

                else if (DeptName != "")
                {
                    SSQL = SSQL + " And AM.DeptName='" + DeptName + "'";
                }

                else if (ItemName != "")
                {
                    SSQL = SSQL + " And AS1.ItemName='" + ItemName + "'";
                }
                else if (SupplierName != "")
                {
                    SSQL = SSQL + " And AM.Supp_Name='" + SupplierName + "'";
                }

                SSQL = SSQL + " And AM.Ccode='" + SessionCcode + "' And AM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And AM.FinYearCode='" + SessionFinYearCode + "' And AS1.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And AS1.Lcode='" + SessionLcode + "' And AS1.FinYearCode='" + SessionFinYearCode + "' And AM.PO_Status='1'";

                PurchaseReturn = objdata.RptEmployeeMultipleDetails(SSQL);
            }
            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (PurchaseReturn.Rows.Count > 0)
            {
                DataTable dtdSuppAddress = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDataTable.Columns.Add("CompanyName");
                AutoDataTable.Columns.Add("LocationName");
                AutoDataTable.Columns.Add("CompanyCode");
                AutoDataTable.Columns.Add("LocationCode");
                AutoDataTable.Columns.Add("Address1");
                AutoDataTable.Columns.Add("Address2");

                AutoDataTable.Columns.Add("PurReturnNo");
                AutoDataTable.Columns.Add("PurReturnDate");
                AutoDataTable.Columns.Add("SupplierName");
                AutoDataTable.Columns.Add("SupAddr1");
                AutoDataTable.Columns.Add("SupAddr2");
                AutoDataTable.Columns.Add("SupAddr3");
                AutoDataTable.Columns.Add("TinNo");
                AutoDataTable.Columns.Add("CSTNo");
                AutoDataTable.Columns.Add("DocumentNo");
                AutoDataTable.Columns.Add("DocumentDate");
                AutoDataTable.Columns.Add("WarehouseName");
                AutoDataTable.Columns.Add("DeptName");
                AutoDataTable.Columns.Add("Others");
                AutoDataTable.Columns.Add("Note");

                AutoDataTable.Columns.Add("ItemCode");
                AutoDataTable.Columns.Add("ItemName");
                AutoDataTable.Columns.Add("ReturnQty");
                AutoDataTable.Columns.Add("Pur_Value");
                AutoDataTable.Columns.Add("zoneName");
                AutoDataTable.Columns.Add("BinName");
                AutoDataTable.Columns.Add("Remarks");
                AutoDataTable.Columns.Add("Total");


                for (int i = 0; i < PurchaseReturn.Rows.Count; i++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDataTable.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDataTable.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDataTable.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDataTable.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDataTable.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDataTable.Rows[i]["PurReturnNo"] = PurchaseReturn.Rows[i]["Pur_Return_No"];
                    AutoDataTable.Rows[i]["PurReturnDate"] = PurchaseReturn.Rows[i]["Pur_Return_Date"];
                    AutoDataTable.Rows[i]["SupplierName"] = PurchaseReturn.Rows[i]["Supp_Name"];


                    if (AutoDataTable.Rows[i]["SupplierName"].ToString() != "")
                    {
                        SSQL = "";
                        SSQL = "Select Address1,Address2,City,Pincode,TinNo,CstNo from MstSupplier where SuppName='" + AutoDataTable.Rows[i]["SupplierName"] + "' And Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                        dtdSuppAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    }

                    AutoDataTable.Rows[i]["SupAddr1"] = dtdSuppAddress.Rows[0]["Address1"].ToString() + ",";
                    AutoDataTable.Rows[i]["SupAddr2"] = dtdSuppAddress.Rows[0]["Address2"].ToString() + ",";
                    AutoDataTable.Rows[i]["SupAddr3"] = dtdSuppAddress.Rows[0]["City"].ToString() + "," + dtdSuppAddress.Rows[0]["Pincode"].ToString();
                    AutoDataTable.Rows[i]["TinNo"] = dtdSuppAddress.Rows[0]["TinNo"].ToString();
                    AutoDataTable.Rows[i]["CSTNo"] = dtdSuppAddress.Rows[0]["CstNo"].ToString();



                    AutoDataTable.Rows[i]["DocumentNo"] = PurchaseReturn.Rows[i]["DCNo"];
                    AutoDataTable.Rows[i]["DocumentDate"] = PurchaseReturn.Rows[i]["DCDate"];
                    AutoDataTable.Rows[i]["WarehouseName"] = PurchaseReturn.Rows[i]["WarehouseName"];
                    AutoDataTable.Rows[i]["DeptName"] = PurchaseReturn.Rows[i]["DeptName"];
                    AutoDataTable.Rows[i]["Others"] = PurchaseReturn.Rows[i]["Others"];
                    AutoDataTable.Rows[i]["Note"] = PurchaseReturn.Rows[i]["Note"];

                    AutoDataTable.Rows[i]["ItemCode"] = PurchaseReturn.Rows[i]["ItemCode"];
                    AutoDataTable.Rows[i]["ItemName"] = PurchaseReturn.Rows[i]["ItemName"];
                    AutoDataTable.Rows[i]["ReturnQty"] = PurchaseReturn.Rows[i]["ReturnQty"];
                    AutoDataTable.Rows[i]["Pur_Value"] = PurchaseReturn.Rows[i]["Pur_Value"];
                    AutoDataTable.Rows[i]["zoneName"] = PurchaseReturn.Rows[i]["zoneName"];
                    AutoDataTable.Rows[i]["BinName"] = PurchaseReturn.Rows[i]["BinName"];
                    AutoDataTable.Rows[i]["Remarks"] = PurchaseReturn.Rows[i]["Remarks"];
                    AutoDataTable.Rows[i]["Total"] = PurchaseReturn.Rows[i]["Total"];

                }

                //ds.Tables.Add(AutoDataTable);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/PurchaseReturnInvoiceFormat.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;


                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDataTable);
                RD_Report.Load(Server.MapPath("~/crystal/PurchaseReturnInvoiceFormat.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                //DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();


            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }
    }
    //Purchase Return Details Report Invoice Format Code End

    //Monthly Department wise Purchase and Issue Report Code start
    public void Monthly_DeptWise_Purchase_Issue()
    {
        DataTable BlanketPurDT = new DataTable();
        DataTable GeneralPurDT = new DataTable();
        DataTable StdPurDT = new DataTable();
        DataTable MaterialIssDT = new DataTable();
        if (RptName == "Monthly Department wise Purchase and Issue Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }
            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }
            if (FromDate != "" && ToDate != "")
            {
                SSQL = "select BM.DeptName,BS.OrderQty,BS.Rate";
                SSQL = SSQL + " from Blanket_Purchase_Order_Main BM inner join Blanket_Purchase_Order_Main_Sub BS ";
                SSQL = SSQL + "on BM.Blanket_PO_No=BS.Blanket_PO_No where CONVERT(DATETIME,BM.Blanket_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,BM.Blanket_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And BM.Ccode='" + SessionCcode + "' And BM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And BM.FinYearCode='" + SessionFinYearCode + "' And BS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And BS.Lcode='" + SessionLcode + "' And BS.FinYearCode='" + SessionFinYearCode + "' And BM.Blanket_PO_Status='1' order by BM.DeptName Asc";

                BlanketPurDT = objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "";

                SSQL = "select GM.DeptName,GS.OrderQty,GS.LineTotal";
                SSQL = SSQL + " from General_Purchase_Order_Main GM inner join General_Purchase_Order_Main_Sub GS ";
                SSQL = SSQL + "on GM.Gen_PO_No=GS.Gen_PO_No where CONVERT(DATETIME,GM.Gen_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GM.Gen_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And GM.Ccode='" + SessionCcode + "' And GM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And GM.FinYearCode='" + SessionFinYearCode + "' And GS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And GS.Lcode='" + SessionLcode + "' And GS.FinYearCode='" + SessionFinYearCode + "' And GM.PO_Status='1' order by GM.DeptName Asc";

                GeneralPurDT = objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "";

                SSQL = "select SM.DeptName,SS.OrderQty,SS.LineTotal";
                SSQL = SSQL + " from Std_Purchase_Order_Main SM inner join Std_Purchase_Order_Main_Sub SS ";
                SSQL = SSQL + "on SM.Std_PO_No=SS.Std_PO_No where CONVERT(DATETIME,SM.Std_PO_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SM.Std_PO_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And SM.Ccode='" + SessionCcode + "' And SM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And SM.FinYearCode='" + SessionFinYearCode + "' And SS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And SS.Lcode='" + SessionLcode + "' And SS.FinYearCode='" + SessionFinYearCode + "' And SM.PO_Status='1' order by SM.DeptName Asc";

                StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);
                SSQL = "";

                SSQL = "select MM.DeptName,MS.IssueQty,MS.Value";
                SSQL = SSQL + " from Meterial_Issue_Main MM inner join Meterial_Issue_Main_Sub MS ";
                SSQL = SSQL + "on MM.Mat_Issue_No=MS.Mat_Issue_No where CONVERT(DATETIME,MM.Mat_Issue_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,MM.Mat_Issue_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And MM.Ccode='" + SessionCcode + "' And MM.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And MM.FinYearCode='" + SessionFinYearCode + "' And MS.Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And MS.Lcode='" + SessionLcode + "' And MS.FinYearCode='" + SessionFinYearCode + "' And MM.Mat_Issue_Status='1' order by MM.DeptName Asc";

                MaterialIssDT = objdata.RptEmployeeMultipleDetails(SSQL);

            }

            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Enter Input Details Correctly..');", true);
            }

            if (BlanketPurDT.Rows.Count > 0)
            {
                DataTable dtdSuppAddress = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDataTable.Columns.Add("CompanyName");
                AutoDataTable.Columns.Add("LocationName");
                AutoDataTable.Columns.Add("CompanyCode");
                AutoDataTable.Columns.Add("LocationCode");
                AutoDataTable.Columns.Add("Address1");
                AutoDataTable.Columns.Add("Address2");

                AutoDataTable.Columns.Add("DeptName");
                AutoDataTable.Columns.Add("BlankPurQty");
                AutoDataTable.Columns.Add("BlankPurValue");
                AutoDataTable.Columns.Add("GenPurQty");
                AutoDataTable.Columns.Add("GenPurValue");
                AutoDataTable.Columns.Add("StdPurQty");
                AutoDataTable.Columns.Add("StdPurValue");
                AutoDataTable.Columns.Add("IssQty");
                AutoDataTable.Columns.Add("IssValue");

                AutoDataTable1.Columns.Add("CompanyName");
                AutoDataTable1.Columns.Add("LocationName");
                AutoDataTable1.Columns.Add("CompanyCode");
                AutoDataTable1.Columns.Add("LocationCode");
                AutoDataTable1.Columns.Add("Address1");
                AutoDataTable1.Columns.Add("Address2");

                AutoDataTable1.Columns.Add("DeptName");
                AutoDataTable1.Columns.Add("BlankPurQty");
                AutoDataTable1.Columns.Add("BlankPurValue");
                AutoDataTable1.Columns.Add("GenPurQty");
                AutoDataTable1.Columns.Add("GenPurValue");
                AutoDataTable1.Columns.Add("StdPurQty");
                AutoDataTable1.Columns.Add("StdPurValue");
                AutoDataTable1.Columns.Add("IssQty");
                AutoDataTable1.Columns.Add("IssValue");



                AutoDataTable2.Columns.Add("CompanyName");
                AutoDataTable2.Columns.Add("LocationName");
                AutoDataTable2.Columns.Add("CompanyCode");
                AutoDataTable2.Columns.Add("LocationCode");
                AutoDataTable2.Columns.Add("Address1");
                AutoDataTable2.Columns.Add("Address2");

                AutoDataTable2.Columns.Add("DeptName");
                AutoDataTable2.Columns.Add("BlankPurQty");
                AutoDataTable2.Columns.Add("BlankPurValue");
                AutoDataTable2.Columns.Add("GenPurQty");
                AutoDataTable2.Columns.Add("GenPurValue");
                AutoDataTable2.Columns.Add("StdPurQty");
                AutoDataTable2.Columns.Add("StdPurValue");
                AutoDataTable2.Columns.Add("IssQty");
                AutoDataTable2.Columns.Add("IssValue");




                for (int i = 0; i < BlanketPurDT.Rows.Count; i++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDataTable.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDataTable.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDataTable.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDataTable.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDataTable.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDataTable.Rows[i]["DeptName"] = BlanketPurDT.Rows[i]["DeptName"];
                    AutoDataTable.Rows[i]["BlankPurQty"] = BlanketPurDT.Rows[i]["OrderQty"];
                    decimal BlnkQty = Convert.ToDecimal(BlanketPurDT.Rows[i]["OrderQty"]);
                    decimal BlnkValue = BlnkQty * Convert.ToDecimal(BlanketPurDT.Rows[i]["Rate"]);
                    AutoDataTable.Rows[i]["BlankPurValue"] = BlnkValue;


                }
                int j = AutoDataTable.Rows.Count;
                for (int i = 0; i < GeneralPurDT.Rows.Count; i++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[j]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDataTable.Rows[j]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDataTable.Rows[j]["CompanyCode"] = SessionCcode;
                    AutoDataTable.Rows[j]["LocationCode"] = SessionLcode;
                    AutoDataTable.Rows[j]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDataTable.Rows[j]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDataTable.Rows[j]["DeptName"] = GeneralPurDT.Rows[i]["DeptName"];
                    AutoDataTable.Rows[j]["GenPurQty"] = GeneralPurDT.Rows[i]["OrderQty"];
                    AutoDataTable.Rows[j]["GenPurValue"] = GeneralPurDT.Rows[i]["LineTotal"];

                    j++;
                }
                int k = AutoDataTable.Rows.Count;
                for (int i = 0; i < StdPurDT.Rows.Count; i++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[k]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDataTable.Rows[k]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDataTable.Rows[k]["CompanyCode"] = SessionCcode;
                    AutoDataTable.Rows[k]["LocationCode"] = SessionLcode;
                    AutoDataTable.Rows[k]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDataTable.Rows[k]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDataTable.Rows[k]["DeptName"] = StdPurDT.Rows[i]["DeptName"];
                    AutoDataTable.Rows[k]["StdPurQty"] = StdPurDT.Rows[i]["OrderQty"];
                    AutoDataTable.Rows[k]["StdPurValue"] = StdPurDT.Rows[i]["LineTotal"];

                    k++;

                }
                int k1 = AutoDataTable.Rows.Count;
                for (int i = 0; i < MaterialIssDT.Rows.Count; i++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[k1]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDataTable.Rows[k1]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDataTable.Rows[k1]["CompanyCode"] = SessionCcode;
                    AutoDataTable.Rows[k1]["LocationCode"] = SessionLcode;
                    AutoDataTable.Rows[k1]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDataTable.Rows[k1]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDataTable.Rows[k1]["DeptName"] = MaterialIssDT.Rows[i]["DeptName"];
                    AutoDataTable.Rows[k1]["IssQty"] = MaterialIssDT.Rows[i]["IssueQty"];
                    AutoDataTable.Rows[k1]["IssValue"] = MaterialIssDT.Rows[i]["Value"];

                    k1++;

                }
                DataTable DeptDT = new DataTable();
                SSQL = "select DeptName from MstDepartment order by DeptName Asc";

                DeptDT = objdata.RptEmployeeMultipleDetails(SSQL);

                for (int i = 0; i < DeptDT.Rows.Count; i++)
                {
                    AutoDataTable1.NewRow();
                    AutoDataTable1.Rows.Add();

                    AutoDataTable1.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDataTable1.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDataTable1.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDataTable1.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDataTable1.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDataTable1.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDataTable1.Rows[i]["DeptName"] = DeptDT.Rows[i]["DeptName"];

                    for (int j1 = 0; j1 < AutoDataTable.Rows.Count; j1++)
                    {
                        if (DeptDT.Rows[i]["DeptName"].ToString() == AutoDataTable.Rows[j1]["DeptName"].ToString())
                        {
                            if (AutoDataTable.Rows[j1]["BlankPurQty"].ToString() != "")
                            {
                                AutoDataTable1.Rows[i]["BlankPurQty"] = AutoDataTable.Rows[j1]["BlankPurQty"];
                            }

                            if (AutoDataTable.Rows[j1]["BlankPurValue"].ToString() != "")
                            {
                                AutoDataTable1.Rows[i]["BlankPurValue"] = AutoDataTable.Rows[j1]["BlankPurValue"];
                            }
                            if (AutoDataTable.Rows[j1]["GenPurQty"].ToString() != "")
                            {
                                AutoDataTable1.Rows[i]["GenPurQty"] = AutoDataTable.Rows[j1]["GenPurQty"];
                            }

                            if (AutoDataTable.Rows[j1]["GenPurValue"].ToString() != "")
                            {
                                AutoDataTable1.Rows[i]["GenPurValue"] = AutoDataTable.Rows[j1]["GenPurValue"];
                            }
                            if (AutoDataTable.Rows[j1]["StdPurQty"].ToString() != "")
                            {
                                AutoDataTable1.Rows[i]["StdPurQty"] = AutoDataTable.Rows[j1]["StdPurQty"];
                            }

                            if (AutoDataTable.Rows[j1]["StdPurValue"].ToString() != "")
                            {
                                AutoDataTable1.Rows[i]["StdPurValue"] = AutoDataTable.Rows[j1]["StdPurValue"];
                            }
                            if (AutoDataTable.Rows[j1]["IssQty"].ToString() != "")
                            {
                                AutoDataTable1.Rows[i]["IssQty"] = AutoDataTable.Rows[j1]["IssQty"];
                            }

                            if (AutoDataTable.Rows[j1]["IssValue"].ToString() != "")
                            {
                                AutoDataTable1.Rows[i]["IssValue"] = AutoDataTable.Rows[j1]["IssValue"];
                            }
                        }
                    }
                }

                for (int i = 0; i < AutoDataTable1.Rows.Count; i++)
                {
                    AutoDataTable2.NewRow();
                    AutoDataTable2.Rows.Add();
                    AutoDataTable2.Rows[i]["CompanyName"] = AutoDataTable1.Rows[i]["CompanyName"];
                    AutoDataTable2.Rows[i]["LocationName"] = AutoDataTable1.Rows[i]["LocationName"];
                    AutoDataTable2.Rows[i]["CompanyCode"] = AutoDataTable1.Rows[i]["CompanyCode"];
                    AutoDataTable2.Rows[i]["LocationCode"] = AutoDataTable1.Rows[i]["LocationCode"];
                    AutoDataTable2.Rows[i]["Address1"] = AutoDataTable1.Rows[i]["Address1"];
                    AutoDataTable2.Rows[i]["Address2"] = AutoDataTable1.Rows[i]["Address2"];
                    AutoDataTable2.Rows[i]["DeptName"] = AutoDataTable1.Rows[i]["DeptName"];

                    if (AutoDataTable1.Rows[i]["BlankPurQty"].ToString() != "")
                    {
                        AutoDataTable2.Rows[i]["BlankPurQty"] = AutoDataTable1.Rows[i]["BlankPurQty"];
                    }
                    else
                    {
                        AutoDataTable2.Rows[i]["BlankPurQty"] = "0.00";
                    }
                    if (AutoDataTable1.Rows[i]["BlankPurValue"].ToString() != "")
                    {
                        AutoDataTable2.Rows[i]["BlankPurValue"] = AutoDataTable1.Rows[i]["BlankPurValue"];
                    }
                    else
                    {
                        AutoDataTable2.Rows[i]["BlankPurValue"] = "0.00";
                    }
                    if (AutoDataTable1.Rows[i]["GenPurQty"].ToString() != "")
                    {
                        AutoDataTable2.Rows[i]["GenPurQty"] = AutoDataTable1.Rows[i]["GenPurQty"];
                    }
                    else
                    {
                        AutoDataTable2.Rows[i]["GenPurQty"] = "0.00";
                    }
                    if (AutoDataTable1.Rows[i]["GenPurValue"].ToString() != "")
                    {
                        AutoDataTable2.Rows[i]["GenPurValue"] = AutoDataTable1.Rows[i]["GenPurValue"];
                    }
                    else
                    {
                        AutoDataTable2.Rows[i]["GenPurValue"] = "0.00";
                    }
                    if (AutoDataTable1.Rows[i]["StdPurQty"].ToString() != "")
                    {
                        AutoDataTable2.Rows[i]["StdPurQty"] = AutoDataTable1.Rows[i]["StdPurQty"];
                    }
                    else
                    {
                        AutoDataTable2.Rows[i]["StdPurQty"] = "0.00";
                    }
                    if (AutoDataTable1.Rows[i]["StdPurValue"].ToString() != "")
                    {
                        AutoDataTable2.Rows[i]["StdPurValue"] = AutoDataTable1.Rows[i]["StdPurValue"];
                    }
                    else
                    {
                        AutoDataTable2.Rows[i]["StdPurValue"] = "0.00";
                    }
                    if (AutoDataTable1.Rows[i]["IssQty"].ToString() != "")
                    {
                        AutoDataTable2.Rows[i]["IssQty"] = AutoDataTable1.Rows[i]["IssQty"];
                    }
                    else
                    {
                        AutoDataTable2.Rows[i]["IssQty"] = "0.00";
                    }
                    if (AutoDataTable1.Rows[i]["IssValue"].ToString() != "")
                    {
                        AutoDataTable2.Rows[i]["IssValue"] = AutoDataTable1.Rows[i]["IssValue"];
                    }
                    else
                    {
                        AutoDataTable2.Rows[i]["IssValue"] = "0.00";
                    }

                }




                //ds.Tables.Add(AutoDataTable2);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/DeptwisePurchaseIssue.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDataTable2);
                RD_Report.Load(Server.MapPath("~/crystal/DeptwisePurchaseIssue.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add

                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();


            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }
    }
    //Monthly Department wise Purchase and Issue Report Code End


    //Purchase_Request_List()
    public void Purchase_Request_List()
    {
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }
        if (Request_Str == "Rejected List")
        {
            SSQL = "Select Pur_Request_No,Pur_Request_Date,DeptName,Approvedby,TotalReqQty from Pur_Request_Main where Status = '2'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Pur_Request_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Pur_Request_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            BlnkPODT = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else if (Request_Str == "Pending List")
        {
            SSQL = "Select Pur_Request_No,Pur_Request_Date,DeptName,Approvedby,TotalReqQty from Pur_Request_Main where (Status = '3' or Status is null)";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Pur_Request_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Pur_Request_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            BlnkPODT = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to Give Details Correctly..');", true);
        }
        if (BlnkPODT.Rows.Count > 0)
        {


            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


            AutoDataTable.Columns.Add("CompanyName");
            AutoDataTable.Columns.Add("LocationName");
            AutoDataTable.Columns.Add("CompanyCode");
            AutoDataTable.Columns.Add("LocationCode");
            AutoDataTable.Columns.Add("Address1");
            AutoDataTable.Columns.Add("Address2");
            AutoDataTable.Columns.Add("Pur_Request_No");
            AutoDataTable.Columns.Add("Pur_Request_Date");
            AutoDataTable.Columns.Add("DeptName");
            AutoDataTable.Columns.Add("Approvedby");
            AutoDataTable.Columns.Add("TotalReqQty");
            AutoDataTable.Columns.Add("Status");



            for (int i = 0; i < BlnkPODT.Rows.Count; i++)
            {
                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();

                AutoDataTable.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                AutoDataTable.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                AutoDataTable.Rows[i]["CompanyCode"] = SessionCcode;
                AutoDataTable.Rows[i]["LocationCode"] = SessionLcode;
                AutoDataTable.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                AutoDataTable.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                AutoDataTable.Rows[i]["Pur_Request_No"] = BlnkPODT.Rows[i]["Pur_Request_No"];
                AutoDataTable.Rows[i]["Pur_Request_Date"] = BlnkPODT.Rows[i]["Pur_Request_Date"];
                AutoDataTable.Rows[i]["DeptName"] = BlnkPODT.Rows[i]["DeptName"];
                AutoDataTable.Rows[i]["Approvedby"] = BlnkPODT.Rows[i]["Approvedby"];
                AutoDataTable.Rows[i]["TotalReqQty"] = BlnkPODT.Rows[i]["TotalReqQty"];

                AutoDataTable.Rows[i]["Status"] = Request_Str;

            }

            //ds.Tables.Add(AutoDataTable);
            //ReportDocument report = new ReportDocument();
            //report.Load(Server.MapPath("~/crystal/PurchaseRequestStatus.rpt"));
            //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            //CrystalReportViewer1.ReportSource = report;


            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDataTable);
            RD_Report.Load(Server.MapPath("~/crystal/PurchaseRequestStatus.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            //Company Details Add
            //DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();




        }

        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }




    }
    //Purchase_Request_List()


    //Stock_Adjustment_Report Code Start
    public void Stock_Adjustment_Report()
    {

        if (RptName == "Stock Adjustment Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            SSQL = "select ASM.Adjustment_No,ASM.Adjustmen_Date,ASM.AdjustmenBy,ASM.Remarks,ASM.ApprovedBy,ASM.Status,";
            SSQL = SSQL + "ASS.ItemCode,ASS.ItemName,ASS.StockQty,ASS.StockValue,ASS.AdjustQty,ASS.AdjustValue,ASS.WarehouseCode,ASS.WarehouseName,ASS.ZoneName,ASS.BinName from Adjustment_Stock_Entry_Main ASM inner join Adjustment_Stock_Entry_Sub ASS";
            SSQL = SSQL + " on ASM.Adjustment_No=ASS.Adjustment_No where";
            SSQL = SSQL + " ASM.Ccode='" + SessionCcode + "' And ASM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And ASM.FinYearCode='" + SessionFinYearCode + "' And ASS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And ASS.Lcode='" + SessionLcode + "' And ASS.FinYearCode='" + SessionFinYearCode + "'";


            if (FromDate != "" && ToDate != "")
            {

                SSQL = SSQL + " And CONVERT(DATETIME,ASM.Adjustmen_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,ASM.Adjustmen_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }

            if (AdjustmentNo != "")
            {
                SSQL = SSQL + " And ASM.Adjustment_No='" + AdjustmentNo + "'";
            }


            if (ItemName != "")
            {
                SSQL = SSQL + " And ASS.ItemName='" + ItemName + "'";
            }



            StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (StdPurDT.Rows.Count > 0)
            {


                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTStdPur.Columns.Add("CompanyName");
                AutoDTStdPur.Columns.Add("LocationName");
                AutoDTStdPur.Columns.Add("CompanyCode");
                AutoDTStdPur.Columns.Add("LocationCode");
                AutoDTStdPur.Columns.Add("Address1");
                AutoDTStdPur.Columns.Add("Address2");
                AutoDTStdPur.Columns.Add("Adjustment_No");
                AutoDTStdPur.Columns.Add("Adjustmen_Date");
                AutoDTStdPur.Columns.Add("AdjustmenBy");
                AutoDTStdPur.Columns.Add("Remarks");
                AutoDTStdPur.Columns.Add("ApprovedBy");
                AutoDTStdPur.Columns.Add("Status");
                AutoDTStdPur.Columns.Add("ItemCode");
                AutoDTStdPur.Columns.Add("ItemName");
                AutoDTStdPur.Columns.Add("StockQty");
                AutoDTStdPur.Columns.Add("StockValue");
                AutoDTStdPur.Columns.Add("AdjustQty");
                AutoDTStdPur.Columns.Add("AdjustValue");
                AutoDTStdPur.Columns.Add("WarehouseCode");
                AutoDTStdPur.Columns.Add("WarehouseName");
                AutoDTStdPur.Columns.Add("ZoneName");
                AutoDTStdPur.Columns.Add("BinName");




                for (int i = 0; i < StdPurDT.Rows.Count; i++)
                {
                    AutoDTStdPur.NewRow();
                    AutoDTStdPur.Rows.Add();

                    AutoDTStdPur.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTStdPur.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTStdPur.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTStdPur.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTStdPur.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTStdPur.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDTStdPur.Rows[i]["Adjustment_No"] = StdPurDT.Rows[i]["Adjustment_No"];
                    AutoDTStdPur.Rows[i]["Adjustmen_Date"] = StdPurDT.Rows[i]["Adjustmen_Date"];
                    AutoDTStdPur.Rows[i]["AdjustmenBy"] = StdPurDT.Rows[i]["AdjustmenBy"];
                    AutoDTStdPur.Rows[i]["Remarks"] = StdPurDT.Rows[i]["Remarks"];
                    AutoDTStdPur.Rows[i]["ApprovedBy"] = StdPurDT.Rows[i]["ApprovedBy"];
                    AutoDTStdPur.Rows[i]["Status"] = StdPurDT.Rows[i]["Status"];
                    AutoDTStdPur.Rows[i]["ItemCode"] = StdPurDT.Rows[i]["ItemCode"];
                    AutoDTStdPur.Rows[i]["ItemName"] = StdPurDT.Rows[i]["ItemName"];
                    AutoDTStdPur.Rows[i]["StockQty"] = StdPurDT.Rows[i]["StockQty"];
                    AutoDTStdPur.Rows[i]["StockValue"] = StdPurDT.Rows[i]["StockValue"];
                    AutoDTStdPur.Rows[i]["AdjustQty"] = StdPurDT.Rows[i]["AdjustQty"];
                    AutoDTStdPur.Rows[i]["AdjustValue"] = StdPurDT.Rows[i]["AdjustValue"];
                    AutoDTStdPur.Rows[i]["WarehouseCode"] = StdPurDT.Rows[i]["WarehouseCode"];
                    AutoDTStdPur.Rows[i]["WarehouseName"] = StdPurDT.Rows[i]["WarehouseName"];
                    AutoDTStdPur.Rows[i]["ZoneName"] = StdPurDT.Rows[i]["ZoneName"];
                    AutoDTStdPur.Rows[i]["BinName"] = StdPurDT.Rows[i]["BinName"];


                }

                //ds.Tables.Add(AutoDTStdPur);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/StockAdjustmentDetails.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;


                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDTStdPur);
                RD_Report.Load(Server.MapPath("~/crystal/StockAdjustmentDetails.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                //DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();


            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //Stock_Adjustment_Report Code End



    //Top_Moving_Issued_Item_Report Code Start
    public void Top_Moving_Issued_Item_Report()
    {

        if (RptName == "Top Moving Issued Item Details Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            SSQL = "select MIS.ItemCode,sum(MIS.IssueQty) as IssueQty,sum(Value) as Value";
            SSQL = SSQL + " from Meterial_Issue_Main MIM inner join Meterial_Issue_Main_Sub MIS";
            SSQL = SSQL + " on MIM.Mat_Issue_No=MIS.Mat_Issue_No where";
            SSQL = SSQL + " MIM.Ccode='" + SessionCcode + "' And MIM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And MIM.FinYearCode='" + SessionFinYearCode + "' And MIS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And MIS.Lcode='" + SessionLcode + "' And MIS.FinYearCode='" + SessionFinYearCode + "' And  MIM.Mat_Issue_Status='1'";


            if (FromDate != "" && ToDate != "")
            {

                SSQL = SSQL + " And CONVERT(DATETIME,MIM.Mat_Issue_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,MIM.Mat_Issue_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }

            if (DeptName != "")
            {
                SSQL = SSQL + " And MIS.DeptName='" + DeptName + "'";
            }
            if (CostCenterName != "")
            {
                SSQL = SSQL + " And MIM.CostCenterName='" + CostCenterName + "'";
            }

            if (ItemName != "")
            {
                SSQL = SSQL + " And MIS.ItemName='" + ItemName + "'";
            }

            SSQL = SSQL + " group by MIS.ItemCode order by IssueQty Desc";

            StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (StdPurDT.Rows.Count > 0)
            {

                DataTable DT_Item = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTStdPur.Columns.Add("CompanyName");
                AutoDTStdPur.Columns.Add("LocationName");
                AutoDTStdPur.Columns.Add("CompanyCode");
                AutoDTStdPur.Columns.Add("LocationCode");
                AutoDTStdPur.Columns.Add("Address1");
                AutoDTStdPur.Columns.Add("Address2");
                AutoDTStdPur.Columns.Add("ItemCode");
                AutoDTStdPur.Columns.Add("ItemName");
                AutoDTStdPur.Columns.Add("IssueQty");
                AutoDTStdPur.Columns.Add("Value");




                for (int i = 0; i < StdPurDT.Rows.Count; i++)
                {
                    AutoDTStdPur.NewRow();
                    AutoDTStdPur.Rows.Add();

                    AutoDTStdPur.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTStdPur.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTStdPur.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTStdPur.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTStdPur.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTStdPur.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDTStdPur.Rows[i]["ItemCode"] = StdPurDT.Rows[i]["ItemCode"];

                    if (AutoDTStdPur.Rows[i]["ItemCode"].ToString() != "")
                    {
                        SSQL = "select ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ItemCode='" + AutoDTStdPur.Rows[i]["ItemCode"].ToString() + "'";
                        DT_Item = objdata.RptEmployeeMultipleDetails(SSQL);

                    }


                    AutoDTStdPur.Rows[i]["ItemName"] = DT_Item.Rows[0]["ItemName"];
                    AutoDTStdPur.Rows[i]["IssueQty"] = StdPurDT.Rows[i]["IssueQty"];
                    AutoDTStdPur.Rows[i]["Value"] = StdPurDT.Rows[i]["Value"];


                }

                //ds.Tables.Add(AutoDTStdPur);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/TopMovingIssuedItemDetails.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;


                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDTStdPur);
                RD_Report.Load(Server.MapPath("~/crystal/TopMovingIssuedItemDetails.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add
                //DataTable dtdCompanyAddress = new DataTable();

                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();


            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //Top_Moving_Issued_Item_Report Code End


    //Maximum_Purchase_Item_Details_Report Code Start
    public void Maximum_Purchase_Item_Details_Report()
    {
        DataTable DT_POReceipt = new DataTable();
        DataTable DT_UPReceipt = new DataTable();
        DataTable AutoDT = new DataTable();
        if (RptName == "Maximum Purchase Item Details Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            SSQL = "select PRS.ItemCode,sum(PRS.ReceivedQty) as ReceivedQty";
            SSQL = SSQL + " from Pur_Order_Receipt_Main PRM inner join Pur_Order_Receipt_Main_Sub PRS";
            SSQL = SSQL + " on PRM.PO_Receipt_No=PRS.PO_Receipt_No where";
            SSQL = SSQL + " PRM.Ccode='" + SessionCcode + "' And PRM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And PRM.FinYearCode='" + SessionFinYearCode + "' And PRS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And PRS.Lcode='" + SessionLcode + "' And PRS.FinYearCode='" + SessionFinYearCode + "' And  PRM.PO_Receipt_Status='1'";


            if (FromDate != "" && ToDate != "")
            {

                SSQL = SSQL + " And CONVERT(DATETIME,PRM.PO_Receipt_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,PRM.PO_Receipt_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }

            if (DeptName != "")
            {
                SSQL = SSQL + " And PRM.DeptName='" + DeptName + "'";
            }
            if (CostCenterName != "")
            {
                SSQL = SSQL + " And PRM.CostCenterName='" + CostCenterName + "'";
            }
            if (SupplierName != "")
            {
                SSQL = SSQL + " And PRM.Supp_Name='" + SupplierName + "'";
            }

            if (ItemName != "")
            {
                SSQL = SSQL + " And PRS.ItemName='" + ItemName + "'";
            }

            SSQL = SSQL + " group by PRS.ItemCode order by ReceivedQty Desc";

            DT_POReceipt = objdata.RptEmployeeMultipleDetails(SSQL);




            SSQL = "select URS.ItemCode,sum(URS.ReceivedQty) as ReceivedQty";
            SSQL = SSQL + " from Unplanned_Receipt_Main URM inner join Unplanned_Receipt_Main_Sub URS";
            SSQL = SSQL + " on URM.UnPlan_Recp_No=URS.UnPlan_Recp_No where";
            SSQL = SSQL + " URM.Ccode='" + SessionCcode + "' And URM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And URM.FinYearCode='" + SessionFinYearCode + "' And URS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And URS.Lcode='" + SessionLcode + "' And URS.FinYearCode='" + SessionFinYearCode + "' And  URM.UnPlan_PO_Receipt_Status='1'";


            if (FromDate != "" && ToDate != "")
            {

                SSQL = SSQL + " And CONVERT(DATETIME,URM.UnPlan_Recp_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,URM.UnPlan_Recp_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }

            if (DeptName != "")
            {
                SSQL = SSQL + " And URS.DeptName='" + DeptName + "'";
            }
            if (CostCenterName != "")
            {
                SSQL = SSQL + " And URM.CostCenterName='" + CostCenterName + "'";
            }
            if (SupplierName != "")
            {
                SSQL = SSQL + " And URM.Supp_Name='" + SupplierName + "'";
            }

            if (ItemName != "")
            {
                SSQL = SSQL + " And URS.ItemName='" + ItemName + "'";
            }

            SSQL = SSQL + " group by URS.ItemCode order by ReceivedQty Desc";

            DT_UPReceipt = objdata.RptEmployeeMultipleDetails(SSQL);


            if (DT_POReceipt.Rows.Count > 0 || DT_UPReceipt.Rows.Count > 0)
            {

                DataTable DT_Item = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTStdPur.Columns.Add("CompanyName");
                AutoDTStdPur.Columns.Add("LocationName");
                AutoDTStdPur.Columns.Add("CompanyCode");
                AutoDTStdPur.Columns.Add("LocationCode");
                AutoDTStdPur.Columns.Add("Address1");
                AutoDTStdPur.Columns.Add("Address2");
                AutoDTStdPur.Columns.Add("ItemCode");
                AutoDTStdPur.Columns.Add("ItemName");
                AutoDTStdPur.Columns.Add("ReceivedQty", typeof(Decimal));

                AutoDT.Columns.Add("CompanyName");
                AutoDT.Columns.Add("LocationName");
                AutoDT.Columns.Add("CompanyCode");
                AutoDT.Columns.Add("LocationCode");
                AutoDT.Columns.Add("Address1");
                AutoDT.Columns.Add("Address2");
                AutoDT.Columns.Add("ItemCode");
                AutoDT.Columns.Add("ItemName");
                AutoDT.Columns.Add("ReceivedQty", typeof(Decimal));



                for (int i = 0; i < DT_POReceipt.Rows.Count; i++)
                {
                    DataTable DT_CheckItem = new DataTable();

                    AutoDTStdPur.NewRow();
                    AutoDTStdPur.Rows.Add();

                    AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["CompanyCode"] = SessionCcode;
                    AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["LocationCode"] = SessionLcode;
                    AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                    AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemCode"] = DT_POReceipt.Rows[i]["ItemCode"].ToString();

                    if (AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemCode"].ToString() != "")
                    {
                        SSQL = "select ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ItemCode='" + AutoDTStdPur.Rows[i]["ItemCode"].ToString() + "'";
                        DT_Item = objdata.RptEmployeeMultipleDetails(SSQL);

                    }


                    AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemName"] = DT_Item.Rows[0]["ItemName"];


                    SSQL = "select URS.ItemCode,sum(URS.ReceivedQty) as ReceivedQty";
                    SSQL = SSQL + " from Unplanned_Receipt_Main URM inner join Unplanned_Receipt_Main_Sub URS";
                    SSQL = SSQL + " on URM.UnPlan_Recp_No=URS.UnPlan_Recp_No where";
                    SSQL = SSQL + " URM.Ccode='" + SessionCcode + "' And URM.Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And URM.FinYearCode='" + SessionFinYearCode + "' And URS.Ccode='" + SessionCcode + "'";
                    SSQL = SSQL + " And URS.Lcode='" + SessionLcode + "' And URS.FinYearCode='" + SessionFinYearCode + "' And  URM.UnPlan_PO_Receipt_Status='1' And URS.ItemCode='" + DT_POReceipt.Rows[i]["ItemCode"].ToString() + "'";
                    SSQL = SSQL + " group by URS.ItemCode order by ReceivedQty Desc";
                    DT_CheckItem = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (DT_CheckItem.Rows.Count != 0)
                    {

                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ReceivedQty"] = (Convert.ToDecimal(DT_POReceipt.Rows[i]["ReceivedQty"]) + Convert.ToDecimal(DT_CheckItem.Rows[0]["ReceivedQty"])).ToString();
                    }
                    else
                    {
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ReceivedQty"] = DT_POReceipt.Rows[i]["ReceivedQty"].ToString();
                    }

                }

                for (int j = 0; j < DT_UPReceipt.Rows.Count; j++)
                {
                    if (DT_UPReceipt.Rows[j]["ItemCode"].ToString() != AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemCode"].ToString())
                    {
                        AutoDTStdPur.NewRow();
                        AutoDTStdPur.Rows.Add();

                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["CompanyCode"] = SessionCcode;
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["LocationCode"] = SessionLcode;
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemCode"] = DT_UPReceipt.Rows[j]["ItemCode"].ToString();

                        if (DT_UPReceipt.Rows[j]["ItemCode"].ToString() != "")
                        {
                            SSQL = "select ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ItemCode='" + DT_UPReceipt.Rows[j]["ItemCode"].ToString() + "'";
                            DT_Item = objdata.RptEmployeeMultipleDetails(SSQL);

                        }


                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemName"] = DT_Item.Rows[0]["ItemName"];
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ReceivedQty"] = DT_UPReceipt.Rows[j]["ReceivedQty"].ToString();
                    }
                }
                //AutoDTStdPur.DefaultView.Sort = "ReceivedQty DESC";
                //AutoDT = AutoDTStdPur.DefaultView.ToTable();
                //ds.Tables.Add(AutoDT);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/MaximumPurchaseItemDetails.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;


                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDT);
                RD_Report.Load(Server.MapPath("~/crystal/MaximumPurchaseItemDetails.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                //DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //Maximum_Purchase_Item_Details_Report Code End


    //DepartmentWise_Purchase_Issue_Cost_Details_Report Code Start
    public void DepartmentWise_Purchase_Issue_Cost_Details_Report()
    {
        if (RptName == "Department Wise Purchase and Issue Cost Compare Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            SSQL = "select DeptCode,ItemCode,isnull((sum(PO_Qty)+sum(General_Qty)),0) as PO_Qty,isnull((sum(PO_Value)+sum(General_Value)),0) as PO_Value,sum(Issue_Qty) as Issue_Qty,sum(Issue_Value) as Issue_Value";
            SSQL = SSQL + " from Stock_Transaction_Ledger";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "'";


            if (FromDate != "" && ToDate != "")
            {

                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }

            if (DeptName != "")
            {
                SSQL = SSQL + " And DeptName='" + DeptName + "'";
            }
            if (CostCenterName != "")
            {
                SSQL = SSQL + " And CostCenterName='" + CostCenterName + "'";
            }

            if (ItemName != "")
            {
                SSQL = SSQL + " And ItemName='" + ItemName + "'";
            }

            SSQL = SSQL + " group by DeptCode,ItemCode";

            StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (StdPurDT.Rows.Count > 0)
            {

                DataTable DT_Item = new DataTable();
                DataTable DT_Dept = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTStdPur.Columns.Add("CompanyName");
                AutoDTStdPur.Columns.Add("LocationName");
                AutoDTStdPur.Columns.Add("CompanyCode");
                AutoDTStdPur.Columns.Add("LocationCode");
                AutoDTStdPur.Columns.Add("Address1");
                AutoDTStdPur.Columns.Add("Address2");
                AutoDTStdPur.Columns.Add("DeptCode");
                AutoDTStdPur.Columns.Add("DeptName");
                AutoDTStdPur.Columns.Add("ItemCode");
                AutoDTStdPur.Columns.Add("ItemName");
                AutoDTStdPur.Columns.Add("PO_Qty");
                AutoDTStdPur.Columns.Add("PO_Value");
                AutoDTStdPur.Columns.Add("Issue_Qty");
                AutoDTStdPur.Columns.Add("Issue_Value");




                for (int i = 0; i < StdPurDT.Rows.Count; i++)
                {
                    AutoDTStdPur.NewRow();
                    AutoDTStdPur.Rows.Add();

                    AutoDTStdPur.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTStdPur.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTStdPur.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTStdPur.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTStdPur.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTStdPur.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDTStdPur.Rows[i]["DeptCode"] = StdPurDT.Rows[i]["DeptCode"];

                    if (AutoDTStdPur.Rows[i]["DeptCode"].ToString() != "")
                    {
                        SSQL = "select DeptName from MstDepartment where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And DeptCode='" + AutoDTStdPur.Rows[i]["DeptCode"].ToString() + "'";
                        DT_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

                    }
                    AutoDTStdPur.Rows[i]["DeptName"] = DT_Dept.Rows[0]["DeptName"];



                    AutoDTStdPur.Rows[i]["ItemCode"] = StdPurDT.Rows[i]["ItemCode"];

                    if (AutoDTStdPur.Rows[i]["ItemCode"].ToString() != "")
                    {
                        SSQL = "select ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ItemCode='" + AutoDTStdPur.Rows[i]["ItemCode"].ToString() + "'";
                        DT_Item = objdata.RptEmployeeMultipleDetails(SSQL);

                    }


                    AutoDTStdPur.Rows[i]["ItemName"] = DT_Item.Rows[0]["ItemName"];
                    AutoDTStdPur.Rows[i]["PO_Qty"] = StdPurDT.Rows[i]["PO_Qty"];
                    AutoDTStdPur.Rows[i]["PO_Value"] = StdPurDT.Rows[i]["PO_Value"];
                    AutoDTStdPur.Rows[i]["Issue_Qty"] = StdPurDT.Rows[i]["Issue_Qty"];
                    AutoDTStdPur.Rows[i]["Issue_Value"] = StdPurDT.Rows[i]["Issue_Value"];


                }

                AutoDTStdPur.DefaultView.Sort = "DeptName ASC";
                AutoDTStdPur = AutoDTStdPur.DefaultView.ToTable();

                //ds.Tables.Add(AutoDTStdPur);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/DepartmentWisePurchase&IssueCostCompare.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;



                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDTStdPur);
                RD_Report.Load(Server.MapPath("~/crystal/DepartmentWisePurchase&IssueCostCompare.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add

                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //DepartmentWise_Purchase_Issue_Cost_Details_Report Code End

    //DepartmentWise_Current_Prev_FinYear_Purchase_Issue_Cost_Details_Report Code Start
    public void DepartmentWise_Current_Prev_FinYear_Purchase_Issue_Cost_Details_Report()
    {
        DataTable CurrentFinYear_DT = new DataTable();
        DataTable PreviousFinYear_DT = new DataTable();

        if (RptName == "Department Wise Current Fin Year And Prev Fin Year Purchase And Issue Cost Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            SSQL = "select DeptCode,ItemCode,isnull((sum(PO_Qty)+sum(General_Qty)),0) as PO_Qty,isnull((sum(PO_Value)+sum(General_Value)),0) as PO_Value,sum(Issue_Qty) as Issue_Qty,sum(Issue_Value) as Issue_Value";
            SSQL = SSQL + " from Stock_Transaction_Ledger";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "'";


            if (FromDate != "" && ToDate != "")
            {

                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }

            if (DeptName != "")
            {
                SSQL = SSQL + " And DeptName='" + DeptName + "'";
            }
            if (CostCenterName != "")
            {
                SSQL = SSQL + " And CostCenterName='" + CostCenterName + "'";
            }

            if (ItemName != "")
            {
                SSQL = SSQL + " And ItemName='" + ItemName + "'";
            }

            SSQL = SSQL + " group by DeptCode,ItemCode";

            CurrentFinYear_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            string PrevYearCode = (Convert.ToInt32(SessionFinYearCode) - 1).ToString();

            SSQL = "select DeptCode,ItemCode,sum(PO_Qty) as PO_Qty,sum(PO_Value) as PO_Value,sum(Issue_Qty) as Issue_Qty,sum(Issue_Value) as Issue_Value";
            SSQL = SSQL + " from Stock_Transaction_Ledger";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And FinYearCode='" + PrevYearCode + "'";


            if (FromDate != "" && ToDate != "")
            {

                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }

            if (DeptName != "")
            {
                SSQL = SSQL + " And DeptName='" + DeptName + "'";
            }
            if (CostCenterName != "")
            {
                SSQL = SSQL + " And CostCenterName='" + CostCenterName + "'";
            }

            if (ItemName != "")
            {
                SSQL = SSQL + " And ItemName='" + ItemName + "'";
            }

            SSQL = SSQL + " group by DeptCode,ItemCode";

            PreviousFinYear_DT = objdata.RptEmployeeMultipleDetails(SSQL);


            if (CurrentFinYear_DT.Rows.Count > 0 || PreviousFinYear_DT.Rows.Count > 0)
            {

                DataTable DT_Item = new DataTable();
                DataTable DT_Dept = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTStdPur.Columns.Add("CompanyName");
                AutoDTStdPur.Columns.Add("LocationName");
                AutoDTStdPur.Columns.Add("CompanyCode");
                AutoDTStdPur.Columns.Add("LocationCode");
                AutoDTStdPur.Columns.Add("Address1");
                AutoDTStdPur.Columns.Add("Address2");
                AutoDTStdPur.Columns.Add("DeptCode");
                AutoDTStdPur.Columns.Add("DeptName");
                AutoDTStdPur.Columns.Add("ItemCode");
                AutoDTStdPur.Columns.Add("ItemName");
                AutoDTStdPur.Columns.Add("Curr_PO_Qty");
                AutoDTStdPur.Columns.Add("Curr_PO_Value");
                AutoDTStdPur.Columns.Add("Curr_Issue_Qty");
                AutoDTStdPur.Columns.Add("Curr_Issue_Value");
                AutoDTStdPur.Columns.Add("Prev_PO_Qty");
                AutoDTStdPur.Columns.Add("Prev_PO_Value");
                AutoDTStdPur.Columns.Add("Prev_Issue_Qty");
                AutoDTStdPur.Columns.Add("Prev_Issue_Value");



                for (int i = 0; i < CurrentFinYear_DT.Rows.Count; i++)
                {
                    int count = 0;
                    for (int j = 0; j < PreviousFinYear_DT.Rows.Count; j++)
                    {
                        if (CurrentFinYear_DT.Rows[i]["DeptCode"].ToString() == PreviousFinYear_DT.Rows[j]["DeptCode"].ToString() && CurrentFinYear_DT.Rows[i]["ItemCode"].ToString() == PreviousFinYear_DT.Rows[j]["ItemCode"].ToString())
                        {
                            count = count + 1;
                            AutoDTStdPur.NewRow();
                            AutoDTStdPur.Rows.Add();

                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["CompanyCode"] = SessionCcode;
                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["LocationCode"] = SessionLcode;
                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["DeptCode"] = CurrentFinYear_DT.Rows[i]["DeptCode"];

                            if (AutoDTStdPur.Rows[i]["DeptCode"].ToString() != "")
                            {
                                SSQL = "select DeptName from MstDepartment where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And DeptCode='" + AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["DeptCode"].ToString() + "'";
                                DT_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

                            }
                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["DeptName"] = DT_Dept.Rows[0]["DeptName"];

                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemCode"] = CurrentFinYear_DT.Rows[i]["ItemCode"];

                            if (AutoDTStdPur.Rows[i]["ItemCode"].ToString() != "")
                            {
                                SSQL = "select ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ItemCode='" + AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemCode"].ToString() + "'";
                                DT_Item = objdata.RptEmployeeMultipleDetails(SSQL);

                            }
                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemName"] = DT_Item.Rows[0]["ItemName"];


                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Curr_PO_Qty"] = CurrentFinYear_DT.Rows[i]["PO_Qty"];
                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Curr_PO_Value"] = CurrentFinYear_DT.Rows[i]["PO_Value"];
                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Curr_Issue_Qty"] = CurrentFinYear_DT.Rows[i]["Issue_Qty"];
                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Curr_Issue_Value"] = CurrentFinYear_DT.Rows[i]["Issue_Value"];
                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Prev_PO_Qty"] = PreviousFinYear_DT.Rows[i]["PO_Qty"];
                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Prev_PO_Value"] = PreviousFinYear_DT.Rows[i]["PO_Value"];
                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Prev_Issue_Qty"] = PreviousFinYear_DT.Rows[i]["Issue_Qty"];
                            AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Prev_Issue_Value"] = PreviousFinYear_DT.Rows[i]["Issue_Value"];

                        }
                    }
                    if (count == 0)
                    {
                        AutoDTStdPur.NewRow();
                        AutoDTStdPur.Rows.Add();

                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["CompanyCode"] = SessionCcode;
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["LocationCode"] = SessionLcode;
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["DeptCode"] = CurrentFinYear_DT.Rows[i]["DeptCode"];

                        if (AutoDTStdPur.Rows[i]["DeptCode"].ToString() != "")
                        {
                            SSQL = "select DeptName from MstDepartment where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And DeptCode='" + AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["DeptCode"].ToString() + "'";
                            DT_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

                        }
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["DeptName"] = DT_Dept.Rows[0]["DeptName"];



                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemCode"] = CurrentFinYear_DT.Rows[i]["ItemCode"];

                        if (AutoDTStdPur.Rows[i]["ItemCode"].ToString() != "")
                        {
                            SSQL = "select ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ItemCode='" + AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemCode"].ToString() + "'";
                            DT_Item = objdata.RptEmployeeMultipleDetails(SSQL);

                        }
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemName"] = DT_Item.Rows[0]["ItemName"];

                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Curr_PO_Qty"] = CurrentFinYear_DT.Rows[i]["PO_Qty"];
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Curr_PO_Value"] = CurrentFinYear_DT.Rows[i]["PO_Value"];
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Curr_Issue_Qty"] = CurrentFinYear_DT.Rows[i]["Issue_Qty"];
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Curr_Issue_Value"] = CurrentFinYear_DT.Rows[i]["Issue_Value"];

                    }

                }


                for (int i = 0; i < PreviousFinYear_DT.Rows.Count; i++)
                {
                    if (AutoDTStdPur.Rows.Count > 0)
                    {
                        for (int j = 0; j < AutoDTStdPur.Rows.Count; j++)
                        {
                            if (PreviousFinYear_DT.Rows[i]["DeptCode"].ToString() != AutoDTStdPur.Rows[j]["DeptCode"].ToString() && PreviousFinYear_DT.Rows[i]["ItemCode"].ToString() != AutoDTStdPur.Rows[j]["ItemCode"].ToString())
                            {
                                AutoDTStdPur.NewRow();
                                AutoDTStdPur.Rows.Add();

                                AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                                AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                                AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["CompanyCode"] = SessionCcode;
                                AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["LocationCode"] = SessionLcode;
                                AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                                AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                                AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["DeptCode"] = PreviousFinYear_DT.Rows[i]["DeptCode"];

                                if (AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["DeptCode"].ToString() != "")
                                {
                                    SSQL = "select DeptName from MstDepartment where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And DeptCode='" + AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["DeptCode"].ToString() + "'";
                                    DT_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

                                }
                                AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["DeptName"] = DT_Dept.Rows[0]["DeptName"];



                                AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemCode"] = PreviousFinYear_DT.Rows[i]["ItemCode"];

                                if (AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemCode"].ToString() != "")
                                {
                                    SSQL = "select ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ItemCode='" + AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemCode"].ToString() + "'";
                                    DT_Item = objdata.RptEmployeeMultipleDetails(SSQL);

                                }


                                AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemName"] = DT_Item.Rows[0]["ItemName"];



                                AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Prev_PO_Qty"] = PreviousFinYear_DT.Rows[i]["PO_Qty"];
                                AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Prev_PO_Value"] = PreviousFinYear_DT.Rows[i]["PO_Value"];
                                AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Prev_Issue_Qty"] = PreviousFinYear_DT.Rows[i]["Issue_Qty"];
                                AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Prev_Issue_Value"] = PreviousFinYear_DT.Rows[i]["Issue_Value"];

                            }
                        }
                    }
                    else
                    {
                        AutoDTStdPur.NewRow();
                        AutoDTStdPur.Rows.Add();

                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["CompanyCode"] = SessionCcode;
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["LocationCode"] = SessionLcode;
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["DeptCode"] = PreviousFinYear_DT.Rows[i]["DeptCode"];

                        if (AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["DeptCode"].ToString() != "")
                        {
                            SSQL = "select DeptName from MstDepartment where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And DeptCode='" + AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["DeptCode"].ToString() + "'";
                            DT_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

                        }
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["DeptName"] = DT_Dept.Rows[0]["DeptName"];



                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemCode"] = PreviousFinYear_DT.Rows[i]["ItemCode"];

                        if (AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemCode"].ToString() != "")
                        {
                            SSQL = "select ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ItemCode='" + AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemCode"].ToString() + "'";
                            DT_Item = objdata.RptEmployeeMultipleDetails(SSQL);

                        }


                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["ItemName"] = DT_Item.Rows[0]["ItemName"];



                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Prev_PO_Qty"] = PreviousFinYear_DT.Rows[i]["PO_Qty"];
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Prev_PO_Value"] = PreviousFinYear_DT.Rows[i]["PO_Value"];
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Prev_Issue_Qty"] = PreviousFinYear_DT.Rows[i]["Issue_Qty"];
                        AutoDTStdPur.Rows[AutoDTStdPur.Rows.Count - 1]["Prev_Issue_Value"] = PreviousFinYear_DT.Rows[i]["Issue_Value"];

                    }

                }
                AutoDTStdPur.DefaultView.Sort = "DeptName ASC";
                AutoDTStdPur = AutoDTStdPur.DefaultView.ToTable();

                //ds.Tables.Add(AutoDTStdPur);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/DepartmentWiseCurrentFinYearAndPrevFinYearPurchaseAndIssueCost.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;


                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDTStdPur);
                RD_Report.Load(Server.MapPath("~/crystal/DepartmentWiseCurrentFinYearAndPrevFinYearPurchaseAndIssueCost.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add

                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();


            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //DepartmentWise_Current_Prev_FinYear_Purchase_Issue_Cost_Details_Report Code End


    //Electrical_Department_Item_Purchase_Issue_Cost_Report Code Start
    public void Electrical_Department_Item_Purchase_Issue_Cost_Report()
    {

        if (RptName == "Electrical Department Item Purchase and Issue Cost Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            SSQL = "select DeptCode,ItemCode,isnull((sum(PO_Qty)+sum(General_Qty)),0) as PO_Qty,isnull((sum(PO_Value)+sum(General_Value)),0) as PO_Value,sum(Issue_Qty) as Issue_Qty,sum(Issue_Value) as Issue_Value";
            SSQL = SSQL + " from Stock_Transaction_Ledger";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And DeptCode='ELECTRIC 001'";


            if (FromDate != "" && ToDate != "")
            {

                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }


            if (CostCenterName != "")
            {
                SSQL = SSQL + " And CostCenterName='" + CostCenterName + "'";
            }

            if (ItemName != "")
            {
                SSQL = SSQL + " And ItemName='" + ItemName + "'";
            }

            SSQL = SSQL + " group by DeptCode,ItemCode";

            StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (StdPurDT.Rows.Count > 0)
            {

                DataTable DT_Item = new DataTable();
                DataTable DT_Dept = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTStdPur.Columns.Add("CompanyName");
                AutoDTStdPur.Columns.Add("LocationName");
                AutoDTStdPur.Columns.Add("CompanyCode");
                AutoDTStdPur.Columns.Add("LocationCode");
                AutoDTStdPur.Columns.Add("Address1");
                AutoDTStdPur.Columns.Add("Address2");
                AutoDTStdPur.Columns.Add("DeptCode");
                AutoDTStdPur.Columns.Add("DeptName");
                AutoDTStdPur.Columns.Add("ItemCode");
                AutoDTStdPur.Columns.Add("ItemName");
                AutoDTStdPur.Columns.Add("PO_Qty");
                AutoDTStdPur.Columns.Add("PO_Value");
                AutoDTStdPur.Columns.Add("Issue_Qty");
                AutoDTStdPur.Columns.Add("Issue_Value");




                for (int i = 0; i < StdPurDT.Rows.Count; i++)
                {
                    AutoDTStdPur.NewRow();
                    AutoDTStdPur.Rows.Add();

                    AutoDTStdPur.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTStdPur.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTStdPur.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTStdPur.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTStdPur.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTStdPur.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDTStdPur.Rows[i]["DeptCode"] = StdPurDT.Rows[i]["DeptCode"];

                    if (AutoDTStdPur.Rows[i]["DeptCode"].ToString() != "")
                    {
                        SSQL = "select DeptName from MstDepartment where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And DeptCode='" + AutoDTStdPur.Rows[i]["DeptCode"].ToString() + "'";
                        DT_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

                    }
                    AutoDTStdPur.Rows[i]["DeptName"] = DT_Dept.Rows[0]["DeptName"];



                    AutoDTStdPur.Rows[i]["ItemCode"] = StdPurDT.Rows[i]["ItemCode"];

                    if (AutoDTStdPur.Rows[i]["ItemCode"].ToString() != "")
                    {
                        SSQL = "select ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ItemCode='" + AutoDTStdPur.Rows[i]["ItemCode"].ToString() + "'";
                        DT_Item = objdata.RptEmployeeMultipleDetails(SSQL);

                    }


                    AutoDTStdPur.Rows[i]["ItemName"] = DT_Item.Rows[0]["ItemName"];
                    AutoDTStdPur.Rows[i]["PO_Qty"] = StdPurDT.Rows[i]["PO_Qty"];
                    AutoDTStdPur.Rows[i]["PO_Value"] = StdPurDT.Rows[i]["PO_Value"];
                    AutoDTStdPur.Rows[i]["Issue_Qty"] = StdPurDT.Rows[i]["Issue_Qty"];
                    AutoDTStdPur.Rows[i]["Issue_Value"] = StdPurDT.Rows[i]["Issue_Value"];


                }

                //ds.Tables.Add(AutoDTStdPur);
                //ReportDocument report = new ReportDocument();

                //report.Load(Server.MapPath("~/crystal/ElectricalDepartmentItemPurchase&IssueCost.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;


                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDTStdPur);
                RD_Report.Load(Server.MapPath("~/crystal/ElectricalDepartmentItemPurchase&IssueCost.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add

                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //Electrical_Department_Item_Purchase_Issue_Cost_Report Code End

    //CostCenterWise_Purchase_Issue_Cost_Report Code Start
    public void CostCenterWise_Purchase_Issue_Cost_Report()
    {
        if (RptName == "CostCenter Wise Purchase and Issue Cost Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }

            SSQL = "select CostCenterCode,ItemCode,isnull((sum(PO_Qty)+sum(General_Qty)),0) as PO_Qty,isnull((sum(PO_Value)+sum(General_Value)),0) as PO_Value,sum(Issue_Qty) as Issue_Qty,sum(Issue_Value) as Issue_Value";
            SSQL = SSQL + " from Stock_Transaction_Ledger";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "'";


            if (FromDate != "" && ToDate != "")
            {

                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }


            if (CostCenterName != "")
            {
                SSQL = SSQL + " And CostCenterName='" + CostCenterName + "'";
            }

            if (ItemName != "")
            {
                SSQL = SSQL + " And ItemName='" + ItemName + "'";
            }

            SSQL = SSQL + " group by CostCenterCode,ItemCode";

            StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (StdPurDT.Rows.Count > 0)
            {

                DataTable DT_Item = new DataTable();
                DataTable DT_Dept = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTStdPur.Columns.Add("CompanyName");
                AutoDTStdPur.Columns.Add("LocationName");
                AutoDTStdPur.Columns.Add("CompanyCode");
                AutoDTStdPur.Columns.Add("LocationCode");
                AutoDTStdPur.Columns.Add("Address1");
                AutoDTStdPur.Columns.Add("Address2");
                AutoDTStdPur.Columns.Add("CostCenterCode");
                AutoDTStdPur.Columns.Add("CostCenterName");
                AutoDTStdPur.Columns.Add("ItemCode");
                AutoDTStdPur.Columns.Add("ItemName");
                AutoDTStdPur.Columns.Add("PO_Qty");
                AutoDTStdPur.Columns.Add("PO_Value");
                AutoDTStdPur.Columns.Add("Issue_Qty");
                AutoDTStdPur.Columns.Add("Issue_Value");




                for (int i = 0; i < StdPurDT.Rows.Count; i++)
                {
                    AutoDTStdPur.NewRow();
                    AutoDTStdPur.Rows.Add();

                    AutoDTStdPur.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTStdPur.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTStdPur.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTStdPur.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTStdPur.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTStdPur.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDTStdPur.Rows[i]["CostCenterCode"] = StdPurDT.Rows[i]["CostCenterCode"];

                    if (AutoDTStdPur.Rows[i]["CostCenterCode"].ToString() != "")
                    {
                        SSQL = "select CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And CostcenterCode='" + AutoDTStdPur.Rows[i]["CostCenterCode"].ToString() + "'";
                        DT_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

                    }
                    AutoDTStdPur.Rows[i]["CostCenterName"] = DT_Dept.Rows[0]["CostcenterName"];



                    AutoDTStdPur.Rows[i]["ItemCode"] = StdPurDT.Rows[i]["ItemCode"];

                    if (AutoDTStdPur.Rows[i]["ItemCode"].ToString() != "")
                    {
                        SSQL = "select ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ItemCode='" + AutoDTStdPur.Rows[i]["ItemCode"].ToString() + "'";
                        DT_Item = objdata.RptEmployeeMultipleDetails(SSQL);

                    }


                    AutoDTStdPur.Rows[i]["ItemName"] = DT_Item.Rows[0]["ItemName"];
                    AutoDTStdPur.Rows[i]["PO_Qty"] = StdPurDT.Rows[i]["PO_Qty"];
                    AutoDTStdPur.Rows[i]["PO_Value"] = StdPurDT.Rows[i]["PO_Value"];
                    AutoDTStdPur.Rows[i]["Issue_Qty"] = StdPurDT.Rows[i]["Issue_Qty"];
                    AutoDTStdPur.Rows[i]["Issue_Value"] = StdPurDT.Rows[i]["Issue_Value"];


                }

                //AutoDTStdPur.DefaultView.Sort = "CostCenterName ASC";
                //AutoDTStdPur = AutoDTStdPur.DefaultView.ToTable();

                //ds.Tables.Add(AutoDTStdPur);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/CostCenterWisePurchase&IssueCost.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;

                AutoDTStdPur.DefaultView.Sort = "CostCenterName ASC";
                AutoDTStdPur = AutoDTStdPur.DefaultView.ToTable();
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDTStdPur);
                RD_Report.Load(Server.MapPath("~/crystal/CostCenterWisePurchase&IssueCost.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add

                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();


            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //CostCenterWise_Purchase_Issue_Cost_Report Code End



    //Lubricant_Purchase_Issue_Cost_Report Code Start
    public void Lubricant_Purchase_Issue_Cost_Report()
    {
        if (RptName == "Lubricant Purchase and Issue Cost Details Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }


            SSQL = "select STL.DeptCode,STL.ItemCode,isnull((sum(STL.PO_Qty)+sum(STL.General_Qty)),0) as PO_Qty,isnull((sum(STL.PO_Value)+sum(STL.General_Value)),0) as PO_Value,sum(STL.Issue_Qty) as Issue_Qty,sum(STL.Issue_Value) as Issue_Value";
            SSQL = SSQL + " from Stock_Transaction_Ledger STL inner join MstItemMaster MI ";
            SSQL = SSQL + " on STL.ItemCode=MI.ItemCode where STL.Ccode='" + SessionCcode + "' And STL.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And STL.FinYearCode='" + SessionFinYearCode + "' And MI.ItemType='Lubricants'";


            if (FromDate != "" && ToDate != "")
            {

                SSQL = SSQL + " And CONVERT(DATETIME,STL.Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,STL.Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }

            if (DeptName != "")
            {
                SSQL = SSQL + " And STL.DeptName='" + DeptName + "'";
            }
            if (CostCenterName != "")
            {
                SSQL = SSQL + " And STL.CostCenterName='" + CostCenterName + "'";
            }

            if (ItemName != "")
            {
                SSQL = SSQL + " And STL.ItemName='" + ItemName + "'";
            }

            SSQL = SSQL + " group by STL.DeptCode,STL.ItemCode";

            StdPurDT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (StdPurDT.Rows.Count > 0)
            {

                DataTable DT_Item = new DataTable();
                DataTable DT_Dept = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDTStdPur.Columns.Add("CompanyName");
                AutoDTStdPur.Columns.Add("LocationName");
                AutoDTStdPur.Columns.Add("CompanyCode");
                AutoDTStdPur.Columns.Add("LocationCode");
                AutoDTStdPur.Columns.Add("Address1");
                AutoDTStdPur.Columns.Add("Address2");
                AutoDTStdPur.Columns.Add("DeptCode");
                AutoDTStdPur.Columns.Add("DeptName");
                AutoDTStdPur.Columns.Add("ItemCode");
                AutoDTStdPur.Columns.Add("ItemName");
                AutoDTStdPur.Columns.Add("PO_Qty");
                AutoDTStdPur.Columns.Add("PO_Value");
                AutoDTStdPur.Columns.Add("Issue_Qty");
                AutoDTStdPur.Columns.Add("Issue_Value");




                for (int i = 0; i < StdPurDT.Rows.Count; i++)
                {
                    AutoDTStdPur.NewRow();
                    AutoDTStdPur.Rows.Add();

                    AutoDTStdPur.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTStdPur.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTStdPur.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTStdPur.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTStdPur.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTStdPur.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();


                    AutoDTStdPur.Rows[i]["DeptCode"] = StdPurDT.Rows[i]["DeptCode"];

                    if (AutoDTStdPur.Rows[i]["DeptCode"].ToString() != "")
                    {
                        SSQL = "select DeptName from MstDepartment where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And DeptCode='" + AutoDTStdPur.Rows[i]["DeptCode"].ToString() + "'";
                        DT_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

                    }
                    AutoDTStdPur.Rows[i]["DeptName"] = DT_Dept.Rows[0]["DeptName"];



                    AutoDTStdPur.Rows[i]["ItemCode"] = StdPurDT.Rows[i]["ItemCode"];

                    if (AutoDTStdPur.Rows[i]["ItemCode"].ToString() != "")
                    {
                        SSQL = "select ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ItemCode='" + AutoDTStdPur.Rows[i]["ItemCode"].ToString() + "'";
                        DT_Item = objdata.RptEmployeeMultipleDetails(SSQL);

                    }


                    AutoDTStdPur.Rows[i]["ItemName"] = DT_Item.Rows[0]["ItemName"];
                    AutoDTStdPur.Rows[i]["PO_Qty"] = StdPurDT.Rows[i]["PO_Qty"];
                    AutoDTStdPur.Rows[i]["PO_Value"] = StdPurDT.Rows[i]["PO_Value"];
                    AutoDTStdPur.Rows[i]["Issue_Qty"] = StdPurDT.Rows[i]["Issue_Qty"];
                    AutoDTStdPur.Rows[i]["Issue_Value"] = StdPurDT.Rows[i]["Issue_Value"];


                }

                AutoDTStdPur.DefaultView.Sort = "DeptName ASC";
                AutoDTStdPur = AutoDTStdPur.DefaultView.ToTable();

                //ds.Tables.Add(AutoDTStdPur);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/LubricantPurchase&IssueCost.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;


                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDTStdPur);
                RD_Report.Load(Server.MapPath("~/crystal/LubricantPurchase&IssueCost.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                //DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();


            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //Lubricant_Purchase_Issue_Cost_Report Code End


    //Damage_Material_Stock_Report Code Start
    public void Damage_Material_Stock_Report()
    {
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select DEM.Damage_Entry_No,DEM.Damage_Entry_Date,DEM.DeptCode,DEM.DeptName,DEM.CostCenterCode,DEM.CostCenterName,DEM.WarehouseCode,DEM.WarehouseName,DEM.Damagedby,DEM.Verifiedby,DEM.Remarks,DEM.Note,DEM.Status,";
        SSQL = SSQL + "DES.ItemCode,DES.ItemName,DES.UOMCode,DES.Qty,DES.Rate,DES.ZoneName,DES.BinName from Damage_Entry_Main DEM inner join Damage_Entry_Main_Sub DES";
        SSQL = SSQL + " on DEM.Damage_Entry_No=DES.Damage_Entry_No where";
        SSQL = SSQL + " DEM.Ccode='" + SessionCcode + "' And DEM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And DEM.FinYearCode='" + SessionFinYearCode + "' And DES.Ccode='" + SessionCcode + "'";
        SSQL = SSQL + " And DES.Lcode='" + SessionLcode + "' And DES.FinYearCode='" + SessionFinYearCode + "'";


        if (FromDate != "" && ToDate != "")
        {

            SSQL = SSQL + " And CONVERT(DATETIME,DEM.Damage_Entry_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,DEM.Damage_Entry_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }

        if (AdjustmentNo != "")
        {
            SSQL = SSQL + " And DEM.Damage_Entry_No='" + DamageEntryNo + "'";
        }


        if (ItemName != "")
        {
            SSQL = SSQL + " And DES.ItemName='" + ItemName + "'";
        }

        BlnkPODT = objdata.RptEmployeeMultipleDetails(SSQL);



        if (BlnkPODT.Rows.Count > 0)
        {


            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


            AutoDataTable.Columns.Add("CompanyName");
            AutoDataTable.Columns.Add("LocationName");
            AutoDataTable.Columns.Add("CompanyCode");
            AutoDataTable.Columns.Add("LocationCode");
            AutoDataTable.Columns.Add("Address1");
            AutoDataTable.Columns.Add("Address2");
            AutoDataTable.Columns.Add("Damage_Entry_No");
            AutoDataTable.Columns.Add("Damage_Entry_Date");
            AutoDataTable.Columns.Add("DeptCode");
            AutoDataTable.Columns.Add("DeptName");
            AutoDataTable.Columns.Add("CostCenterCode");
            AutoDataTable.Columns.Add("CostCenterName");
            AutoDataTable.Columns.Add("WarehouseCode");
            AutoDataTable.Columns.Add("WarehouseName");
            AutoDataTable.Columns.Add("Damagedby");
            AutoDataTable.Columns.Add("Verifiedby");
            AutoDataTable.Columns.Add("Remarks");
            AutoDataTable.Columns.Add("Note");
            AutoDataTable.Columns.Add("Status");
            AutoDataTable.Columns.Add("ItemCode");
            AutoDataTable.Columns.Add("ItemName");
            AutoDataTable.Columns.Add("UOMCode");
            AutoDataTable.Columns.Add("Qty");
            AutoDataTable.Columns.Add("Rate");
            AutoDataTable.Columns.Add("ZoneName");
            AutoDataTable.Columns.Add("BinName");




            for (int i = 0; i < BlnkPODT.Rows.Count; i++)
            {
                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();

                AutoDataTable.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                AutoDataTable.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                AutoDataTable.Rows[i]["CompanyCode"] = SessionCcode;
                AutoDataTable.Rows[i]["LocationCode"] = SessionLcode;
                AutoDataTable.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                AutoDataTable.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                AutoDataTable.Rows[i]["Damage_Entry_No"] = BlnkPODT.Rows[i]["Damage_Entry_No"];
                AutoDataTable.Rows[i]["Damage_Entry_Date"] = BlnkPODT.Rows[i]["Damage_Entry_Date"];
                AutoDataTable.Rows[i]["DeptCode"] = BlnkPODT.Rows[i]["DeptCode"];
                AutoDataTable.Rows[i]["DeptName"] = BlnkPODT.Rows[i]["DeptName"];
                AutoDataTable.Rows[i]["CostCenterCode"] = BlnkPODT.Rows[i]["CostCenterCode"];
                AutoDataTable.Rows[i]["CostCenterName"] = BlnkPODT.Rows[i]["CostCenterName"];
                AutoDataTable.Rows[i]["WarehouseCode"] = BlnkPODT.Rows[i]["WarehouseCode"];
                AutoDataTable.Rows[i]["WarehouseName"] = BlnkPODT.Rows[i]["WarehouseName"];
                AutoDataTable.Rows[i]["Damagedby"] = BlnkPODT.Rows[i]["Damagedby"];
                AutoDataTable.Rows[i]["Verifiedby"] = BlnkPODT.Rows[i]["Verifiedby"];
                AutoDataTable.Rows[i]["Remarks"] = BlnkPODT.Rows[i]["Remarks"];
                AutoDataTable.Rows[i]["Note"] = BlnkPODT.Rows[i]["Note"];
                AutoDataTable.Rows[i]["Status"] = BlnkPODT.Rows[i]["Status"];
                AutoDataTable.Rows[i]["ItemCode"] = BlnkPODT.Rows[i]["ItemCode"];
                AutoDataTable.Rows[i]["ItemName"] = BlnkPODT.Rows[i]["ItemName"];
                AutoDataTable.Rows[i]["UOMCode"] = BlnkPODT.Rows[i]["UOMCode"];
                AutoDataTable.Rows[i]["Qty"] = BlnkPODT.Rows[i]["Qty"];
                AutoDataTable.Rows[i]["Rate"] = BlnkPODT.Rows[i]["Rate"];
                AutoDataTable.Rows[i]["ZoneName"] = BlnkPODT.Rows[i]["ZoneName"];
                AutoDataTable.Rows[i]["BinName"] = BlnkPODT.Rows[i]["BinName"];

            }

            //ds.Tables.Add(AutoDataTable);
            //ReportDocument report = new ReportDocument();
            //report.Load(Server.MapPath("~/crystal/DamageMaterialStockDetails.rpt"));
            //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            //CrystalReportViewer1.ReportSource = report;

            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDataTable);
            RD_Report.Load(Server.MapPath("~/crystal/DamageMaterialStockDetails.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            //Company Details Add

            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }

        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }




    }
    //Damage_Material_Stock_Report Code End



    //Spares_Re-Sales_Report Code Start
    public void Spares_ReSales_Report()
    {
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select SRM.Spares_ReSales_No,SRM.Spares_ReSales_Date,SRM.DeptCode,SRM.DeptName,SRM.CostCenterCode,SRM.CostCenterName,SRM.WarehouseCode,SRM.WarehouseName,SRM.PartyName,SRM.PartyDetails,SRM.Mobile_Code,SRM.MobileNo,SRM.Remarks,SRM.Note,SRM.Status,";
        SSQL = SSQL + "SRS.ItemCode,SRS.ItemName,SRS.UOMCode,SRS.Qty,SRS.Rate,SRS.ZoneName,SRS.BinName from Spares_ReSales_Main SRM inner join Spares_ReSales_Main_Sub SRS";
        SSQL = SSQL + " on SRM.Spares_ReSales_No=SRS.Spares_ReSales_No where";
        SSQL = SSQL + " SRM.Ccode='" + SessionCcode + "' And SRM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SRM.FinYearCode='" + SessionFinYearCode + "' And SRS.Ccode='" + SessionCcode + "'";
        SSQL = SSQL + " And SRS.Lcode='" + SessionLcode + "' And SRS.FinYearCode='" + SessionFinYearCode + "'";


        if (FromDate != "" && ToDate != "")
        {

            SSQL = SSQL + " And CONVERT(DATETIME,SRM.Spares_ReSales_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SRM.Spares_ReSales_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }

        if (AdjustmentNo != "")
        {
            SSQL = SSQL + " And SRM.Spares_ReSales_No='" + DamageEntryNo + "'";
        }


        if (ItemName != "")
        {
            SSQL = SSQL + " And SRS.ItemName='" + ItemName + "'";
        }

        BlnkPODT = objdata.RptEmployeeMultipleDetails(SSQL);



        if (BlnkPODT.Rows.Count > 0)
        {


            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


            AutoDataTable.Columns.Add("CompanyName");
            AutoDataTable.Columns.Add("LocationName");
            AutoDataTable.Columns.Add("CompanyCode");
            AutoDataTable.Columns.Add("LocationCode");
            AutoDataTable.Columns.Add("Address1");
            AutoDataTable.Columns.Add("Address2");
            AutoDataTable.Columns.Add("Spares_ReSales_No");
            AutoDataTable.Columns.Add("Spares_ReSales_Date");
            AutoDataTable.Columns.Add("DeptCode");
            AutoDataTable.Columns.Add("DeptName");
            AutoDataTable.Columns.Add("CostCenterCode");
            AutoDataTable.Columns.Add("CostCenterName");
            AutoDataTable.Columns.Add("WarehouseCode");
            AutoDataTable.Columns.Add("WarehouseName");
            AutoDataTable.Columns.Add("PartyName");
            AutoDataTable.Columns.Add("PartyDetails");
            AutoDataTable.Columns.Add("Mobile_Code");
            AutoDataTable.Columns.Add("MobileNo");
            AutoDataTable.Columns.Add("Remarks");
            AutoDataTable.Columns.Add("Note");
            AutoDataTable.Columns.Add("Status");
            AutoDataTable.Columns.Add("ItemCode");
            AutoDataTable.Columns.Add("ItemName");
            AutoDataTable.Columns.Add("UOMCode");
            AutoDataTable.Columns.Add("Qty");
            AutoDataTable.Columns.Add("Rate");
            AutoDataTable.Columns.Add("ZoneName");
            AutoDataTable.Columns.Add("BinName");




            for (int i = 0; i < BlnkPODT.Rows.Count; i++)
            {
                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();

                AutoDataTable.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                AutoDataTable.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                AutoDataTable.Rows[i]["CompanyCode"] = SessionCcode;
                AutoDataTable.Rows[i]["LocationCode"] = SessionLcode;
                AutoDataTable.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                AutoDataTable.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                AutoDataTable.Rows[i]["Spares_ReSales_No"] = BlnkPODT.Rows[i]["Spares_ReSales_No"];
                AutoDataTable.Rows[i]["Spares_ReSales_Date"] = BlnkPODT.Rows[i]["Spares_ReSales_Date"];
                AutoDataTable.Rows[i]["DeptCode"] = BlnkPODT.Rows[i]["DeptCode"];
                AutoDataTable.Rows[i]["DeptName"] = BlnkPODT.Rows[i]["DeptName"];
                AutoDataTable.Rows[i]["CostCenterCode"] = BlnkPODT.Rows[i]["CostCenterCode"];
                AutoDataTable.Rows[i]["CostCenterName"] = BlnkPODT.Rows[i]["CostCenterName"];
                AutoDataTable.Rows[i]["WarehouseCode"] = BlnkPODT.Rows[i]["WarehouseCode"];
                AutoDataTable.Rows[i]["WarehouseName"] = BlnkPODT.Rows[i]["WarehouseName"];
                AutoDataTable.Rows[i]["PartyName"] = BlnkPODT.Rows[i]["PartyName"];
                AutoDataTable.Rows[i]["PartyDetails"] = BlnkPODT.Rows[i]["PartyDetails"];
                AutoDataTable.Rows[i]["Mobile_Code"] = BlnkPODT.Rows[i]["Mobile_Code"];
                AutoDataTable.Rows[i]["MobileNo"] = BlnkPODT.Rows[i]["MobileNo"];
                AutoDataTable.Rows[i]["Remarks"] = BlnkPODT.Rows[i]["Remarks"];
                AutoDataTable.Rows[i]["Note"] = BlnkPODT.Rows[i]["Note"];
                AutoDataTable.Rows[i]["Status"] = BlnkPODT.Rows[i]["Status"];
                AutoDataTable.Rows[i]["ItemCode"] = BlnkPODT.Rows[i]["ItemCode"];
                AutoDataTable.Rows[i]["ItemName"] = BlnkPODT.Rows[i]["ItemName"];
                AutoDataTable.Rows[i]["UOMCode"] = BlnkPODT.Rows[i]["UOMCode"];
                AutoDataTable.Rows[i]["Qty"] = BlnkPODT.Rows[i]["Qty"];
                AutoDataTable.Rows[i]["Rate"] = BlnkPODT.Rows[i]["Rate"];
                AutoDataTable.Rows[i]["ZoneName"] = BlnkPODT.Rows[i]["ZoneName"];
                AutoDataTable.Rows[i]["BinName"] = BlnkPODT.Rows[i]["BinName"];

            }

            //ds.Tables.Add(AutoDataTable);
            //ReportDocument report = new ReportDocument();
            //report.Load(Server.MapPath("~/crystal/SparesReSalesDetails.rpt"));
            //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            //CrystalReportViewer1.ReportSource = report;


            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDataTable);
            RD_Report.Load(Server.MapPath("~/crystal/SparesReSalesDetails.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            //Company Details Add
            //DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();



        }

        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }




    }
    //Spares_Re-Sales_Report Code End



    //Unit To Unit Stock Transfer Details Report Code Start
    public void Unit_Stock_Transfer_Details_Report()
    {

        if (RptName == "Unit To Unit Stock Transfer Details Report")
        {
            if (FromDate != "")
            {
                frmDate = Convert.ToDateTime(FromDate);
            }

            if (ToDate != "")
            {
                toDate = Convert.ToDateTime(ToDate);
            }


            SSQL = "select MSM.Unit_Stock_Transfer_No,MSM.Unit_Stock_Transfer_Date,MSM.Transfer_Unit_LCode,MSM.Transfer_Unit_Location,MSM.From_WarehouseCode,MSM.From_WarehouseName,MSM.To_WarehouseCode,MSM.To_WarehouseName,";
            SSQL = SSQL + "MSM.DeptCode,MSM.DeptName,MSM.CostCenterCode,MSM.CostCenterName,MSM.Preparedby,MSM.Others,";
            SSQL = SSQL + "MSS.ItemCode,MSS.ItemName,MSS.UOMCode,MSS.ValueType,";
            SSQL = SSQL + "MSS.FromZone,MSS.ToZone,MSS.FromBin,MSS.ToBin,";
            SSQL = SSQL + "MSS.TransferQty,MSS.Value";
            SSQL = SSQL + " from Unit_To_Unit_Meterial_Stock_Transfer_Main MSM inner join Unit_To_Unit_Meterial_Stock_Transfer_Main_Sub MSS ";
            SSQL = SSQL + "on MSM.Unit_Stock_Transfer_No=MSS.Unit_Stock_Transfer_No where MSM.Ccode='" + SessionCcode + "' And MSM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And MSM.FinYearCode='" + SessionFinYearCode + "' And MSS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And MSS.Lcode='" + SessionLcode + "' And MSS.FinYearCode='" + SessionFinYearCode + "' And MSM.Mat_Stock_Transfer_Status='1'";


            if (FromDate != "" && ToDate != "")
            {

                SSQL = SSQL + " And CONVERT(DATETIME,MSM.Unit_Stock_Transfer_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,MSM.Unit_Stock_Transfer_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }

            if (StckTranNo != "")
            {

                SSQL = SSQL + " And MSM.Mat_Stock_Transfer_No='" + StckTranNo + "'";

            }


            if (WareHouse != "")
            {
                SSQL = SSQL + " And MSM.From_WarehouseName='" + WareHouse + "'";
            }

            if (ItemName != "")
            {
                SSQL = SSQL + " And MSS.ItemName='" + ItemName + "'";
            }



            StockTransDT = objdata.RptEmployeeMultipleDetails(SSQL);


            if (StockTransDT.Rows.Count > 0)
            {

                DataTable dtdSuppAddress = new DataTable();
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDTStockTrans.Columns.Add("CompanyName");
                AutoDTStockTrans.Columns.Add("LocationName");
                AutoDTStockTrans.Columns.Add("CompanyCode");
                AutoDTStockTrans.Columns.Add("LocationCode");
                AutoDTStockTrans.Columns.Add("Address1");
                AutoDTStockTrans.Columns.Add("Address2");

                AutoDTStockTrans.Columns.Add("Unit_Stock_Transfer_No");
                AutoDTStockTrans.Columns.Add("Unit_Stock_Transfer_Date");
                AutoDTStockTrans.Columns.Add("Transfer_Unit_LCode");
                AutoDTStockTrans.Columns.Add("Transfer_Unit_Location");
                AutoDTStockTrans.Columns.Add("From_WarehouseCode");
                AutoDTStockTrans.Columns.Add("From_WarehouseName");
                AutoDTStockTrans.Columns.Add("To_WarehouseCode");
                AutoDTStockTrans.Columns.Add("To_WarehouseName");
                AutoDTStockTrans.Columns.Add("DeptCode");
                AutoDTStockTrans.Columns.Add("DeptName");
                AutoDTStockTrans.Columns.Add("CostCenterCode");
                AutoDTStockTrans.Columns.Add("CostCenterName");
                AutoDTStockTrans.Columns.Add("Preparedby");
                AutoDTStockTrans.Columns.Add("Others");


                AutoDTStockTrans.Columns.Add("ItemCode");
                AutoDTStockTrans.Columns.Add("ItemName");
                AutoDTStockTrans.Columns.Add("UOMCode");
                AutoDTStockTrans.Columns.Add("ValueType");
                AutoDTStockTrans.Columns.Add("FromZone");
                AutoDTStockTrans.Columns.Add("ToZone");
                AutoDTStockTrans.Columns.Add("FromBin");
                AutoDTStockTrans.Columns.Add("ToBin");
                AutoDTStockTrans.Columns.Add("TransferQty");
                AutoDTStockTrans.Columns.Add("Value");



                for (int i = 0; i < StockTransDT.Rows.Count; i++)
                {
                    AutoDTStockTrans.NewRow();
                    AutoDTStockTrans.Rows.Add();

                    AutoDTStockTrans.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDTStockTrans.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDTStockTrans.Rows[i]["CompanyCode"] = SessionCcode;
                    AutoDTStockTrans.Rows[i]["LocationCode"] = SessionLcode;
                    AutoDTStockTrans.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDTStockTrans.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                    AutoDTStockTrans.Rows[i]["Unit_Stock_Transfer_No"] = StockTransDT.Rows[i]["Unit_Stock_Transfer_No"];
                    AutoDTStockTrans.Rows[i]["Unit_Stock_Transfer_Date"] = StockTransDT.Rows[i]["Unit_Stock_Transfer_Date"];
                    AutoDTStockTrans.Rows[i]["Transfer_Unit_LCode"] = StockTransDT.Rows[i]["Transfer_Unit_LCode"];
                    AutoDTStockTrans.Rows[i]["Transfer_Unit_Location"] = StockTransDT.Rows[i]["Transfer_Unit_Location"];
                    AutoDTStockTrans.Rows[i]["From_WarehouseCode"] = StockTransDT.Rows[i]["From_WarehouseCode"];
                    AutoDTStockTrans.Rows[i]["From_WarehouseName"] = StockTransDT.Rows[i]["From_WarehouseName"];
                    AutoDTStockTrans.Rows[i]["To_WarehouseCode"] = StockTransDT.Rows[i]["To_WarehouseCode"];
                    AutoDTStockTrans.Rows[i]["To_WarehouseName"] = StockTransDT.Rows[i]["To_WarehouseName"];
                    AutoDTStockTrans.Rows[i]["DeptCode"] = StockTransDT.Rows[i]["DeptCode"];
                    AutoDTStockTrans.Rows[i]["DeptName"] = StockTransDT.Rows[i]["DeptName"];
                    AutoDTStockTrans.Rows[i]["CostCenterCode"] = StockTransDT.Rows[i]["CostCenterCode"];
                    AutoDTStockTrans.Rows[i]["CostCenterName"] = StockTransDT.Rows[i]["CostCenterName"];
                    AutoDTStockTrans.Rows[i]["Preparedby"] = StockTransDT.Rows[i]["Preparedby"];
                    AutoDTStockTrans.Rows[i]["Others"] = StockTransDT.Rows[i]["Others"];

                    AutoDTStockTrans.Rows[i]["ItemCode"] = StockTransDT.Rows[i]["ItemCode"];
                    AutoDTStockTrans.Rows[i]["ItemName"] = StockTransDT.Rows[i]["ItemName"];
                    AutoDTStockTrans.Rows[i]["UomCode"] = StockTransDT.Rows[i]["UOMCode"];
                    AutoDTStockTrans.Rows[i]["ValueType"] = StockTransDT.Rows[i]["ValueType"];
                    AutoDTStockTrans.Rows[i]["FromZone"] = StockTransDT.Rows[i]["FromZone"];
                    AutoDTStockTrans.Rows[i]["ToZone"] = StockTransDT.Rows[i]["ToZone"];
                    AutoDTStockTrans.Rows[i]["FromBin"] = StockTransDT.Rows[i]["FromBin"];
                    AutoDTStockTrans.Rows[i]["ToBin"] = StockTransDT.Rows[i]["ToBin"];
                    AutoDTStockTrans.Rows[i]["TransferQty"] = StockTransDT.Rows[i]["TransferQty"];
                    AutoDTStockTrans.Rows[i]["Value"] = StockTransDT.Rows[i]["Value"];


                }

                //ds.Tables.Add(AutoDTStockTrans);
                //ReportDocument report = new ReportDocument();
                //report.Load(Server.MapPath("~/crystal/UnitToUnitStockTransferDetails.rpt"));
                //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                //CrystalReportViewer1.ReportSource = report;



                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDTStockTrans);
                RD_Report.Load(Server.MapPath("~/crystal/UnitToUnitStockTransferDetails.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add
                //DataTable dtdCompanyAddress = new DataTable();

                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }

            else
            {
                Errflag = false;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            }
            if (!Errflag)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }
    //Unit To Unit Stock Transfer Details Report Code End



    //Genset_Expense_Details_Report Code Start
    public void Genset_Expense_Details_Report()
    {
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select Genset_No,Genset_Date,DeptCode,DeptName,CostCenterCode,CostCenterName,WarehouseCode,WarehouseName,Remarks,Purpose,";
        SSQL = SSQL + "TotalHrs,DieselPetrol,Value from Genset_Main";
        SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "'";


        if (FromDate != "" && ToDate != "")
        {

            SSQL = SSQL + " And CONVERT(DATETIME,Genset_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Genset_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }

        if (GensetNo != "")
        {
            SSQL = SSQL + " And Genset_No='" + GensetNo + "'";
        }


        if (DeptName != "")
        {
            SSQL = SSQL + " And DeptName='" + DeptName + "'";
        }

        if (CostCenterName != "")
        {
            SSQL = SSQL + " And CostCenterName='" + CostCenterName + "'";
        }

        if (WareHouse != "")
        {
            SSQL = SSQL + " And WarehouseName='" + WareHouse + "'";
        }

        BlnkPODT = objdata.RptEmployeeMultipleDetails(SSQL);



        if (BlnkPODT.Rows.Count > 0)
        {


            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


            AutoDataTable.Columns.Add("CompanyName");
            AutoDataTable.Columns.Add("LocationName");
            AutoDataTable.Columns.Add("CompanyCode");
            AutoDataTable.Columns.Add("LocationCode");
            AutoDataTable.Columns.Add("Address1");
            AutoDataTable.Columns.Add("Address2");
            AutoDataTable.Columns.Add("Genset_No");
            AutoDataTable.Columns.Add("Genset_Date");
            AutoDataTable.Columns.Add("DeptCode");
            AutoDataTable.Columns.Add("DeptName");
            AutoDataTable.Columns.Add("CostCenterCode");
            AutoDataTable.Columns.Add("CostCenterName");
            AutoDataTable.Columns.Add("WarehouseCode");
            AutoDataTable.Columns.Add("WarehouseName");
            AutoDataTable.Columns.Add("Remarks");
            AutoDataTable.Columns.Add("Purpose");
            AutoDataTable.Columns.Add("TotalHrs");
            AutoDataTable.Columns.Add("DieselPetrol");
            AutoDataTable.Columns.Add("Value");


            for (int i = 0; i < BlnkPODT.Rows.Count; i++)
            {
                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();

                AutoDataTable.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                AutoDataTable.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                AutoDataTable.Rows[i]["CompanyCode"] = SessionCcode;
                AutoDataTable.Rows[i]["LocationCode"] = SessionLcode;
                AutoDataTable.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                AutoDataTable.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                AutoDataTable.Rows[i]["Genset_No"] = BlnkPODT.Rows[i]["Genset_No"];
                AutoDataTable.Rows[i]["Genset_Date"] = BlnkPODT.Rows[i]["Genset_Date"];
                AutoDataTable.Rows[i]["DeptCode"] = BlnkPODT.Rows[i]["DeptCode"];
                AutoDataTable.Rows[i]["DeptName"] = BlnkPODT.Rows[i]["DeptName"];
                AutoDataTable.Rows[i]["CostCenterCode"] = BlnkPODT.Rows[i]["CostCenterCode"];
                AutoDataTable.Rows[i]["CostCenterName"] = BlnkPODT.Rows[i]["CostCenterName"];
                AutoDataTable.Rows[i]["WarehouseCode"] = BlnkPODT.Rows[i]["WarehouseCode"];
                AutoDataTable.Rows[i]["WarehouseName"] = BlnkPODT.Rows[i]["WarehouseName"];
                AutoDataTable.Rows[i]["Remarks"] = BlnkPODT.Rows[i]["Remarks"];
                AutoDataTable.Rows[i]["Purpose"] = BlnkPODT.Rows[i]["Purpose"];
                AutoDataTable.Rows[i]["TotalHrs"] = BlnkPODT.Rows[i]["TotalHrs"];
                AutoDataTable.Rows[i]["DieselPetrol"] = BlnkPODT.Rows[i]["DieselPetrol"];
                AutoDataTable.Rows[i]["Value"] = BlnkPODT.Rows[i]["Value"];

            }

            //ds.Tables.Add(AutoDataTable);
            //ReportDocument report = new ReportDocument();
            //report.Load(Server.MapPath("~/crystal/GensetExpenseDetails.rpt"));
            //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            //CrystalReportViewer1.ReportSource = report;



            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDataTable);
            RD_Report.Load(Server.MapPath("~/crystal/GensetExpenseDetails.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);

            //Company Details Add
            //DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();

        }

        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }
    //Genset_Expense_Details_Report Code End



    //Scrab_Item_Cost_Report Code Start
    public void Scrab_Item_Cost_Report()
    {
        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select SEM.Scrab_Entry_No,SEM.Scrab_Entry_Date,SEM.DeptCode,SEM.DeptName,SEM.CostCenterCode,SEM.CostCenterName,SEM.WarehouseCode,SEM.WarehouseName,SEM.Remarks,SEM.Reason,SEM.VerifiedBy,SEM.ApprovedBy,SEM.Status,";
        SSQL = SSQL + "SES.ItemCode,SES.ItemName,SES.Qty,SES.Rate,SES.ZoneName,SES.BinName from Scrab_Entry_Main SEM inner join Scrab_Entry_Main_Sub SES";
        SSQL = SSQL + " on SEM.Scrab_Entry_No=SES.Scrab_Entry_No where";
        SSQL = SSQL + " SEM.Ccode='" + SessionCcode + "' And SEM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SEM.FinYearCode='" + SessionFinYearCode + "' And SES.Ccode='" + SessionCcode + "'";
        SSQL = SSQL + " And SES.Lcode='" + SessionLcode + "' And SES.FinYearCode='" + SessionFinYearCode + "'";


        if (FromDate != "" && ToDate != "")
        {

            SSQL = SSQL + " And CONVERT(DATETIME,SEM.Scrab_Entry_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SEM.Scrab_Entry_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }

        if (ScrabEntryNo != "")
        {
            SSQL = SSQL + " And SEM.Scrab_Entry_No='" + ScrabEntryNo + "'";
        }


        if (DeptName != "")
        {
            SSQL = SSQL + " And SEM.DeptName='" + DeptName + "'";
        }

        if (WareHouse != "")
        {
            SSQL = SSQL + " And SEM.WarehouseName='" + WareHouse + "'";
        }

        if (CostCenterName != "")
        {
            SSQL = SSQL + " And SEM.CostCenterName='" + CostCenterName + "'";
        }

        SSQL = SSQL + " Order By SEM.Scrab_Entry_Date Desc";

        BlnkPODT = objdata.RptEmployeeMultipleDetails(SSQL);



        if (BlnkPODT.Rows.Count > 0)
        {


            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


            AutoDataTable.Columns.Add("CompanyName");
            AutoDataTable.Columns.Add("LocationName");
            AutoDataTable.Columns.Add("CompanyCode");
            AutoDataTable.Columns.Add("LocationCode");
            AutoDataTable.Columns.Add("Address1");
            AutoDataTable.Columns.Add("Address2");
            AutoDataTable.Columns.Add("Scrab_Entry_No");
            AutoDataTable.Columns.Add("Scrab_Entry_Date");
            AutoDataTable.Columns.Add("DeptCode");
            AutoDataTable.Columns.Add("DeptName");
            AutoDataTable.Columns.Add("CostCenterCode");
            AutoDataTable.Columns.Add("CostCenterName");
            AutoDataTable.Columns.Add("WarehouseCode");
            AutoDataTable.Columns.Add("WarehouseName");
            AutoDataTable.Columns.Add("Remarks");
            AutoDataTable.Columns.Add("Reason");
            AutoDataTable.Columns.Add("VerifiedBy");
            AutoDataTable.Columns.Add("ApprovedBy");
            AutoDataTable.Columns.Add("Status");
            AutoDataTable.Columns.Add("ItemCode");
            AutoDataTable.Columns.Add("ItemName");
            AutoDataTable.Columns.Add("Qty");
            AutoDataTable.Columns.Add("Rate");
            AutoDataTable.Columns.Add("ZoneName");
            AutoDataTable.Columns.Add("BinName");

            for (int i = 0; i < BlnkPODT.Rows.Count; i++)
            {
                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();

                AutoDataTable.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                AutoDataTable.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                AutoDataTable.Rows[i]["CompanyCode"] = SessionCcode;
                AutoDataTable.Rows[i]["LocationCode"] = SessionLcode;
                AutoDataTable.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                AutoDataTable.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                AutoDataTable.Rows[i]["Scrab_Entry_No"] = BlnkPODT.Rows[i]["Scrab_Entry_No"];
                AutoDataTable.Rows[i]["Scrab_Entry_Date"] = BlnkPODT.Rows[i]["Scrab_Entry_Date"];
                AutoDataTable.Rows[i]["DeptCode"] = BlnkPODT.Rows[i]["DeptCode"];
                AutoDataTable.Rows[i]["DeptName"] = BlnkPODT.Rows[i]["DeptName"];
                AutoDataTable.Rows[i]["CostCenterCode"] = BlnkPODT.Rows[i]["CostCenterCode"];
                AutoDataTable.Rows[i]["CostCenterName"] = BlnkPODT.Rows[i]["CostCenterName"];
                AutoDataTable.Rows[i]["WarehouseCode"] = BlnkPODT.Rows[i]["WarehouseCode"];
                AutoDataTable.Rows[i]["WarehouseName"] = BlnkPODT.Rows[i]["WarehouseName"];
                AutoDataTable.Rows[i]["Remarks"] = BlnkPODT.Rows[i]["Remarks"];
                AutoDataTable.Rows[i]["Reason"] = BlnkPODT.Rows[i]["Reason"];
                AutoDataTable.Rows[i]["VerifiedBy"] = BlnkPODT.Rows[i]["VerifiedBy"];
                AutoDataTable.Rows[i]["ApprovedBy"] = BlnkPODT.Rows[i]["ApprovedBy"];
                AutoDataTable.Rows[i]["Status"] = BlnkPODT.Rows[i]["Status"];
                AutoDataTable.Rows[i]["ItemCode"] = BlnkPODT.Rows[i]["ItemCode"];
                AutoDataTable.Rows[i]["ItemName"] = BlnkPODT.Rows[i]["ItemName"];
                AutoDataTable.Rows[i]["Qty"] = BlnkPODT.Rows[i]["Qty"];
                AutoDataTable.Rows[i]["Rate"] = BlnkPODT.Rows[i]["Rate"];
                AutoDataTable.Rows[i]["ZoneName"] = BlnkPODT.Rows[i]["ZoneName"];
                AutoDataTable.Rows[i]["BinName"] = BlnkPODT.Rows[i]["BinName"];

            }

            //ds.Tables.Add(AutoDataTable);
            //ReportDocument report = new ReportDocument();
            //if(RptName == "Scrab Item And Cost Report")
            //{
            //report.Load(Server.MapPath("~/crystal/ScrabItemCostDetails.rpt"));
            //}
            //else if(RptName == "Scrab Entry No Wise Report")
            //{
            //    report.Load(Server.MapPath("~/crystal/ScrabEntryNoItemCostDetails.rpt"));
            //}
            //else if(RptName == "Scrab Item Wise Report")
            //{
            //    report.Load(Server.MapPath("~/crystal/ScrabItemWiseCostDetails.rpt"));
            //}

            //report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            //report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

            //CrystalReportViewer1.ReportSource = report;



            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDataTable);


            if (RptName == "Scrab Item And Cost Report")
            {
                RD_Report.Load(Server.MapPath("~/crystal/ScrabItemCostDetails.rpt"));
            }
            else if (RptName == "Scrab Entry No Wise Report")
            {
                RD_Report.Load(Server.MapPath("~/crystal/ScrabEntryNoItemCostDetails.rpt"));
            }
            else if (RptName == "Scrab Item Wise Report")
            {
                RD_Report.Load(Server.MapPath("~/crystal/ScrabItemWiseCostDetails.rpt"));
            }

            RD_Report.SetDataSource(ds1.Tables[0]);

            //Company Details Add
            //DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();



        }

        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }




    }
    //Scrab_Item_Cost_Report Code End

    //Supplier Payment Details Report()
    public void Supplier_Payment_Details_Report_Load()
    {
        //Supplier Payment Receipt 
        if (ButtonName == "Supplier Payment Receipt")
        {
            if (FromDate != "") { frmDate = Convert.ToDateTime(FromDate); }
            if (ToDate != "") { toDate = Convert.ToDateTime(ToDate); }
            SSQL = "Select SPM.Trans_No,SPM.Trans_Date,SPM.Supp_Name,SPM.Supp_Code,SPM.ChequeNo,SPM.ChequeDate,SPM.BankName,SPM.BranchName,SPM.Description,SPM.TotalBalance as NetAmount,SPS.InvNo,SPS.InvDate,SPS.TotalAmt,SPS.AdvAmt,SPS.CreditAmt,";
            SSQL = SSQL + " SPS.Discount,SPS.AddOrLess,SPS.NetBalAmt from Unplanned_Receipt_Supp_Payment_Main SPM inner join Unplanned_Receipt_Supp_Payment_Main_Sub SPS ";
            SSQL = SSQL + "on SPM.Trans_No=SPS.Trans_No where SPM.Ccode='" + SessionCcode + "' And SPM.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And SPM.FinYearCode='" + SessionFinYearCode + "' And SPS.Ccode='" + SessionCcode + "'";
            SSQL = SSQL + " And SPS.Lcode='" + SessionLcode + "' And SPS.FinYearCode='" + SessionFinYearCode + "'";

            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,SPM.Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,SPM.Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (SupplierName != "")
            {
                SSQL = SSQL + " And SPM.Supp_Name='" + SupplierName + "'";
            }
            if (Transaction_No != "") { SSQL = SSQL + " And SPM.Trans_No='" + Transaction_No + "' And SPS.Trans_No='" + Transaction_No + "'"; }
            SSQL = SSQL + " Order by SPM.Trans_No Asc";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(GenReceiptDT);
                RD_Report.Load(Server.MapPath("~/crystal/SupplierPaymentReceipt.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }

                //Get Supplier Address
                SSQL = "select * From MstSupplier where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And SuppCode='" + GenReceiptDT.Rows[0]["Supp_Code"].ToString() + "' And SuppName= '" + GenReceiptDT.Rows[0]["Supp_Name"].ToString() + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["Supp_Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Supp_Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Supp_City"].Text = "'" + dtdCompanyAddress.Rows[0]["City"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Supp_Pincode"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["MobileNo"].Text = "'" + dtdCompanyAddress.Rows[0]["MobileNo"].ToString() + "'";
                }

                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }

        //All Supplier Balance Report
        if (ButtonName == "All Supplier Balance")
        {
            SSQL = "Delete from Unplanned_Receipt_Supp_Bal_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select * from Unplanned_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);


            if (GenReceiptDT.Rows.Count != 0)
            {
                //Insert Dummy Table for Supplier Balance
                DataTable DT_Pay = new DataTable();
                for (int i = 0; i < GenReceiptDT.Rows.Count; i++)
                {
                    SSQL = "Select * from Unplanned_Receipt_Supp_Payment_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Supp_Code='" + GenReceiptDT.Rows[i]["Supp_Code"].ToString() + "'";
                    SSQL = SSQL + " And InvNo='" + GenReceiptDT.Rows[i]["InvNo"].ToString() + "'";
                    DT_Pay = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DT_Pay.Rows.Count == 0)
                    {
                        //Insert
                        SSQL = "Insert Into Unplanned_Receipt_Supp_Bal_Print(Ccode,Lcode,FinYearCode,FinYearVal,Supp_Code,Supp_Name,UnPlan_Recp_No,";
                        SSQL = SSQL + "UnPlan_Recp_Date,InvNo,InvDate,TotalAmt,UserID,UserName,AdvAmt,PaidAmt,PaidBill_Count,BalanceBill_Count,Bill_Status) Values('" + SessionCcode + "','" + SessionLcode + "',";
                        SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + GenReceiptDT.Rows[i]["Supp_Code"].ToString() + "',";
                        SSQL = SSQL + " '" + GenReceiptDT.Rows[i]["Supp_Name"].ToString() + "','" + GenReceiptDT.Rows[i]["UnPlan_Recp_No"].ToString() + "',";
                        SSQL = SSQL + " '" + GenReceiptDT.Rows[i]["UnPlan_Recp_Date"].ToString() + "','" + GenReceiptDT.Rows[i]["InvNo"].ToString() + "',";
                        SSQL = SSQL + " '" + GenReceiptDT.Rows[i]["InvDate"].ToString() + "','" + GenReceiptDT.Rows[i]["NetAmount"].ToString() + "',";
                        SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "','0.00','0.00','0','1','0')";
                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                    else
                    {
                        SSQL = "Select sum(TotalAmt) as TotalAmt,sum(CreditAmt) as CreditAmt,Sum(AdvAmt) as AdvAmt,sum(BalAmt) as BalAmt,sum(Discount) as Discount,sum(AddOrLess) as AddOrLess from Unplanned_Receipt_Supp_Payment_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Supp_Code='" + GenReceiptDT.Rows[i]["Supp_Code"].ToString() + "'";
                        SSQL = SSQL + " And InvNo='" + GenReceiptDT.Rows[i]["InvNo"].ToString() + "'";
                        DT_Pay = objdata.RptEmployeeMultipleDetails(SSQL);




                        SSQL = "Insert Into Unplanned_Receipt_Supp_Bal_Print(Ccode,Lcode,FinYearCode,FinYearVal,Supp_Code,Supp_Name,UnPlan_Recp_No,";
                        SSQL = SSQL + "UnPlan_Recp_Date,InvNo,InvDate,TotalAmt,UserID,UserName,AdvAmt,PaidAmt,PaidBill_Count,BalanceBill_Count,Bill_Status) Values('" + SessionCcode + "','" + SessionLcode + "',";
                        SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + GenReceiptDT.Rows[i]["Supp_Code"].ToString() + "',";
                        SSQL = SSQL + " '" + GenReceiptDT.Rows[i]["Supp_Name"].ToString() + "','" + GenReceiptDT.Rows[i]["UnPlan_Recp_No"].ToString() + "',";
                        SSQL = SSQL + " '" + GenReceiptDT.Rows[i]["UnPlan_Recp_Date"].ToString() + "','" + GenReceiptDT.Rows[i]["InvNo"].ToString() + "',";
                        SSQL = SSQL + " '" + GenReceiptDT.Rows[i]["InvDate"].ToString() + "','" + GenReceiptDT.Rows[i]["NetAmount"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "',";
                        SSQL = SSQL + " '" + DT_Pay.Rows[0]["AdvAmt"].ToString() + "','" + DT_Pay.Rows[0]["BalAmt"].ToString() + "','1','0','1')";
                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                }

                //Get All Supplier Balance
                SSQL = "Select Supp_Name,sum(TotalAmt) as TotalAmt,sum(BalanceBill_Count) as Bill_Count,sum(AdvAmt) as AdvAmt,sum(PaidAmt) as PaidAmt,sum(PaidBill_Count) as PaidBill_Count from Unplanned_Receipt_Supp_Bal_Print where";
                SSQL = SSQL + "  Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " group by Supp_Name order by Supp_Name Asc";
                GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (GenReceiptDT.Rows.Count != 0)
                {
                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(GenReceiptDT);
                    RD_Report.Load(Server.MapPath("~/crystal/SupplierPaymentBalance.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);
                    //Company Details Add
                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }
                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                    lblUploadSuccessfully.Text = "No Records Matched..";
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }


        //Single Supplier Balance Report
        if (ButtonName == "Singel Supplier Payment")
        {
            SSQL = "Delete from Unplanned_Receipt_Supp_Bal_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select * from Unplanned_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And Supp_Name='" + SupplierName + "' Order by UnPlan_Recp_No Asc";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {
                //Insert Dummy Table for Supplier Balance
                DataTable DT_Pay = new DataTable();
                for (int i = 0; i < GenReceiptDT.Rows.Count; i++)
                {
                    SSQL = "Select *from Unplanned_Receipt_Supp_Payment_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Supp_Code='" + GenReceiptDT.Rows[i]["Supp_Code"].ToString() + "'";
                    SSQL = SSQL + " And InvNo='" + GenReceiptDT.Rows[i]["InvNo"].ToString() + "'";
                    DT_Pay = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DT_Pay.Rows.Count == 0)
                    {
                        //Insert
                        SSQL = "Insert Into Unplanned_Receipt_Supp_Bal_Print(Ccode,Lcode,FinYearCode,FinYearVal,Supp_Code,Supp_Name,UnPlan_Recp_No,";
                        SSQL = SSQL + "UnPlan_Recp_Date,InvNo,InvDate,TotalAmt,UserID,UserName,AdvAmt,PaidAmt,PaidBill_Count,BalanceBill_Count,Bill_Status,Bill_Amount,CreditAmt,Discount,AddOrLess) Values('" + SessionCcode + "','" + SessionLcode + "',";
                        SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + GenReceiptDT.Rows[i]["Supp_Code"].ToString() + "',";
                        SSQL = SSQL + " '" + GenReceiptDT.Rows[i]["Supp_Name"].ToString() + "','" + GenReceiptDT.Rows[i]["UnPlan_Recp_No"].ToString() + "',";
                        SSQL = SSQL + " '" + GenReceiptDT.Rows[i]["UnPlan_Recp_Date"].ToString() + "','" + GenReceiptDT.Rows[i]["InvNo"].ToString() + "',";
                        SSQL = SSQL + " '" + GenReceiptDT.Rows[i]["InvDate"].ToString() + "','" + GenReceiptDT.Rows[i]["TotalAmt"].ToString() + "',";
                        SSQL = SSQL + " '" + SessionUserID + "','" + SessionUserName + "','0.00','0.00','0','1','0','" + GenReceiptDT.Rows[i]["NetAmount"].ToString() + "','0.00','0.00','0.00')";
                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                    else
                    {
                        SSQL = "Select sum(TotalAmt) as TotalAmt,sum(CreditAmt) as CreditAmt,Sum(AdvAmt) as AdvAmt,sum(BalAmt) as BalAmt,sum(Discount) as Discount,sum(AddOrLess) as AddOrLess from Unplanned_Receipt_Supp_Payment_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And Supp_Code='" + GenReceiptDT.Rows[i]["Supp_Code"].ToString() + "'";
                        SSQL = SSQL + " And InvNo='" + GenReceiptDT.Rows[i]["InvNo"].ToString() + "'";
                        DT_Pay = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (GenReceiptDT.Rows[i]["NetAmount"].ToString() == DT_Pay.Rows[0]["BalAmt"].ToString())
                        {

                            SSQL = "Insert Into Unplanned_Receipt_Supp_Bal_Print(Ccode,Lcode,FinYearCode,FinYearVal,Supp_Code,Supp_Name,UnPlan_Recp_No,";
                            SSQL = SSQL + "UnPlan_Recp_Date,InvNo,InvDate,TotalAmt,UserID,UserName,AdvAmt,PaidAmt,PaidBill_Count,BalanceBill_Count,Bill_Status,Bill_Amount,CreditAmt,Discount,AddOrLess) Values('" + SessionCcode + "','" + SessionLcode + "',";
                            SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + GenReceiptDT.Rows[i]["Supp_Code"].ToString() + "',";
                            SSQL = SSQL + " '" + GenReceiptDT.Rows[i]["Supp_Name"].ToString() + "','" + GenReceiptDT.Rows[i]["UnPlan_Recp_No"].ToString() + "',";
                            SSQL = SSQL + " '" + GenReceiptDT.Rows[i]["UnPlan_Recp_Date"].ToString() + "','" + GenReceiptDT.Rows[i]["InvNo"].ToString() + "',";
                            SSQL = SSQL + " '" + GenReceiptDT.Rows[i]["InvDate"].ToString() + "','0.00','" + SessionUserID + "','" + SessionUserName + "',";
                            SSQL = SSQL + " '" + DT_Pay.Rows[0]["AdvAmt"].ToString() + "','" + DT_Pay.Rows[0]["BalAmt"].ToString() + "','1','0','1','" + GenReceiptDT.Rows[i]["NetAmount"].ToString() + "','0.00','" + DT_Pay.Rows[0]["Discount"].ToString() + "','" + DT_Pay.Rows[0]["AddOrLess"].ToString() + "')";
                            objdata.RptEmployeeMultipleDetails(SSQL);
                        }
                        else
                        {
                            SSQL = "Insert Into Unplanned_Receipt_Supp_Bal_Print(Ccode,Lcode,FinYearCode,FinYearVal,Supp_Code,Supp_Name,UnPlan_Recp_No,";
                            SSQL = SSQL + "UnPlan_Recp_Date,InvNo,InvDate,TotalAmt,UserID,UserName,AdvAmt,PaidAmt,PaidBill_Count,BalanceBill_Count,Bill_Status,Bill_Amount,CreditAmt,Discount,AddOrLess) Values('" + SessionCcode + "','" + SessionLcode + "',";
                            SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + GenReceiptDT.Rows[i]["Supp_Code"].ToString() + "',";
                            SSQL = SSQL + " '" + GenReceiptDT.Rows[i]["Supp_Name"].ToString() + "','" + GenReceiptDT.Rows[i]["UnPlan_Recp_No"].ToString() + "',";
                            SSQL = SSQL + " '" + GenReceiptDT.Rows[i]["UnPlan_Recp_Date"].ToString() + "','" + GenReceiptDT.Rows[i]["InvNo"].ToString() + "',";
                            SSQL = SSQL + " '" + GenReceiptDT.Rows[i]["InvDate"].ToString() + "','0.00','" + SessionUserID + "','" + SessionUserName + "',";
                            SSQL = SSQL + " '" + DT_Pay.Rows[0]["AdvAmt"].ToString() + "','" + DT_Pay.Rows[0]["BalAmt"].ToString() + "','1','0','1','" + GenReceiptDT.Rows[i]["NetAmount"].ToString() + "','" + DT_Pay.Rows[0]["CreditAmt"].ToString() + "','" + DT_Pay.Rows[0]["Discount"].ToString() + "','" + DT_Pay.Rows[0]["AddOrLess"].ToString() + "')";
                            objdata.RptEmployeeMultipleDetails(SSQL);
                        }

                    }
                }

                //Get All Supplier Balance
                SSQL = "Select * from Unplanned_Receipt_Supp_Bal_Print where";
                SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                SSQL = SSQL + " And Supp_Name='" + SupplierName + "'";
                SSQL = SSQL + " order by UnPlan_Recp_No Asc";
                GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (GenReceiptDT.Rows.Count != 0)
                {
                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(GenReceiptDT);
                    RD_Report.Load(Server.MapPath("~/crystal/SupplierPaymentSingle.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);
                    //Company Details Add
                    DataTable dtdCompanyAddress = new DataTable();

                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }
                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                    lblUploadSuccessfully.Text = "No Records Matched..";
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }


    }

    //General Purchase Order Pre Printed Sheet Format
    public void General_Purchase_Order_Pre_Print_Invoice_Format()
    {

        SSQL = "Select POM.Gen_PO_No,POM.Gen_PO_Date,POM.Supp_Name,POM.Supp_Qtn_No,POM.Supp_Qtn_Date,";
        SSQL = SSQL + " POM.DeliveryMode,POM.DeliveryDate,POM.DeliveryAt,POM.PaymentMode,POM.DeptName,POM.PaymentTerms,";
        SSQL = SSQL + " POM.Description,POM.Note,POM.Others,POM.TotalAmt,POM.Discount_Per,POM.Discount,POM.TaxPer,";
        SSQL = SSQL + " POM.TaxAmount,POM.OtherCharge,POM.ExciseDuty,POM.NetAmount,POS.ItemName,POS.UOMCode,";
        SSQL = SSQL + " POS.OrderQty,POS.Rate,POS.Remarks,POS.LineTotal from General_Purchase_Order_Main POM inner join General_Purchase_Order_Main_Sub POS on POM.Ccode=POS.Ccode And POM.Lcode=POM.Lcode And POM.Gen_PO_No=POS.Gen_PO_No";
        SSQL = SSQL + " where POM.Ccode='" + SessionCcode + "' And POM.Lcode='" + SessionLcode + "' And POM.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And POS.Ccode='" + SessionCcode + "' And POS.Lcode='" + SessionLcode + "' And POS.FinYearCode='" + SessionFinYearCode + "'";
        SSQL = SSQL + " And POM.Gen_PO_No='" + GenPurOrdNo + "' And POS.Gen_PO_No='" + GenPurOrdNo + "'";

        GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (GenReceiptDT.Rows.Count != 0)
        {
            if (GenReceiptDT.Rows.Count < 12)
            {
                int Row_Count = GenReceiptDT.Rows.Count;
                for (int i = Row_Count; i < 12; i++)
                {
                    GenReceiptDT.NewRow();
                    GenReceiptDT.Rows.Add();
                    GenReceiptDT.Rows[i]["Gen_PO_No"] = GenReceiptDT.Rows[0]["Gen_PO_No"];
                    GenReceiptDT.Rows[i]["DeliveryMode"] = GenReceiptDT.Rows[0]["DeliveryMode"];
                    GenReceiptDT.Rows[i]["DeliveryDate"] = GenReceiptDT.Rows[0]["DeliveryDate"];
                    GenReceiptDT.Rows[i]["DeliveryAt"] = GenReceiptDT.Rows[0]["DeliveryAt"];

                    GenReceiptDT.Rows[i]["PaymentMode"] = GenReceiptDT.Rows[0]["PaymentMode"];
                    GenReceiptDT.Rows[i]["PaymentTerms"] = GenReceiptDT.Rows[0]["PaymentTerms"];
                    GenReceiptDT.Rows[i]["Description"] = GenReceiptDT.Rows[0]["Description"];
                    GenReceiptDT.Rows[i]["Discount_Per"] = GenReceiptDT.Rows[0]["Discount_Per"];
                    GenReceiptDT.Rows[i]["TaxPer"] = GenReceiptDT.Rows[0]["TaxPer"];
                    GenReceiptDT.Rows[i]["OtherCharge"] = GenReceiptDT.Rows[0]["OtherCharge"];
                    //GenReceiptDT.Rows[i]["Discount_Per"] = "0.0";
                    //GenReceiptDT.Rows[i]["Discount"] = "0.0";
                    //GenReceiptDT.Rows[i]["TaxPer"] = "0.0";
                    //GenReceiptDT.Rows[i]["TaxAmount"] = "0.0";
                    //GenReceiptDT.Rows[i]["OtherCharge"] = "0.0";
                }

            }


            DataSet ds1 = new DataSet();
            ds1.Tables.Add(GenReceiptDT);
            if (PO_Rate_Type == "Without Rate")
            {
                RD_Report.Load(Server.MapPath("~/crystal/GeneralPOPrePrintFormat.rpt"));
            }
            else
            {
                RD_Report.Load(Server.MapPath("~/crystal/GeneralPOPrePrintFormat_Rate.rpt"));
            }
            RD_Report.SetDataSource(ds1.Tables[0]);

            ////Company Details Add
            //DataTable dtdCompanyAddress = new DataTable();
            //SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            //dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            //if (dtdCompanyAddress.Rows.Count != 0)
            //{
            //    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
            //    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
            //    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            //}

            //Get Supplier Address
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select * From MstSupplier where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And SuppName= '" + GenReceiptDT.Rows[0]["Supp_Name"].ToString() + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["Supp_Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Supp_Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                //RD_Report.DataDefinition.FormulaFields["Supp_City"].Text = "'" + dtdCompanyAddress.Rows[0]["City"].ToString() + "'";
                //RD_Report.DataDefinition.FormulaFields["Supp_Pincode"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "'";
                //RD_Report.DataDefinition.FormulaFields["MobileNo"].Text = "'" + dtdCompanyAddress.Rows[0]["MobileNo"].ToString() + "'";
            }

            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    //Supplier Advance Details Report()
    public void Supplier_Advance_Details_Report_Load()
    {
        //Supplier Advance Receipt 
        if (ButtonName == "Supplier Advance Receipt")
        {
            if (FromDate != "") { frmDate = Convert.ToDateTime(FromDate); }
            if (ToDate != "") { toDate = Convert.ToDateTime(ToDate); }
            SSQL = "select *from Unplanned_Receipt_Supp_Advance_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            if (SupplierName != "")
            {
                SSQL = SSQL + " And Supp_Name='" + SupplierName + "'";
            }
            if (Transaction_No != "") { SSQL = SSQL + " And Trans_No='" + Transaction_No + "'"; }
            SSQL = SSQL + " Order by Trans_No Asc";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(GenReceiptDT);
                RD_Report.Load(Server.MapPath("~/crystal/SupplierAdvanceReceipt.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);

                //Company Details Add
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }

                //Get Supplier Address
                SSQL = "select * From MstSupplier where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And SuppCode='" + GenReceiptDT.Rows[0]["Supp_Code"].ToString() + "' And SuppName= '" + GenReceiptDT.Rows[0]["Supp_Name"].ToString() + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["Supp_Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Supp_Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Supp_City"].Text = "'" + dtdCompanyAddress.Rows[0]["City"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Supp_Pincode"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["MobileNo"].Text = "'" + dtdCompanyAddress.Rows[0]["MobileNo"].ToString() + "'";
                }

                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");


                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }
        }

        //All Supplier Advance Report
        if (ButtonName == "All Supplier Advance")
        {
            SSQL = "select Supp_Name,sum(AdvAmount) as AdvAmount from Unplanned_Receipt_Supp_Advance_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            SSQL = SSQL + " group by Supp_Name";
            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);


            if (GenReceiptDT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(GenReceiptDT);
                RD_Report.Load(Server.MapPath("~/crystal/SupplierAdvancePayment.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add
                DataTable dtdCompanyAddress = new DataTable();

                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }


        //Single Supplier Advance Report
        if (ButtonName == "Singel Supplier Advance")
        {
            SSQL = "select *from Unplanned_Receipt_Supp_Advance_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            SSQL = SSQL + " And Supp_Name='" + SupplierName + "'";
            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);


            if (GenReceiptDT.Rows.Count != 0)
            {
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(GenReceiptDT);
                RD_Report.Load(Server.MapPath("~/crystal/SupplierAdvanceSingle.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add
                DataTable dtdCompanyAddress = new DataTable();

                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
    }

    public void ClosingStock()
    {
        DataTable StockDT = new DataTable();
        DataTable Department_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable Purchase_DT = new DataTable();
        DataTable Issue_DT = new DataTable();

        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("Opening_Val");
        AutoDataTable.Columns.Add("PO_Val");
        AutoDataTable.Columns.Add("Issue_Val");


        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        string[] finyear = SessionFinYearVal.Split('_');
        string FromDate_Chk = "01/04/" + finyear[0];
        DateTime fromDate_Chk = Convert.ToDateTime(FromDate_Chk);


        string[] FromDate_ChkStr = FromDate.Split('/');


        if (FromDate == FromDate_Chk)
        {
            SSQL = "Delete  Closing_Stock_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select Distinct DeptName from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int i = 0; i < Department_DT.Rows.Count; i++)
            {

                SSQL = "Select sum(Open_Value) as Open_Value from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + "And DeptName='" + Department_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + "And Trans_Type='OPENING RECEIPT'";
                //SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";

                Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "select (sum(General_Value)+sum(PO_Value)) as PO_Val";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And DeptName='" + Department_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "select sum(Issue_Value) as Issue_Val";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And DeptName='" + Department_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);



                SSQL = "Insert Into Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,DeptName,Opening_Val,PO_Val,";
                SSQL = SSQL + "Issue_Val,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Department_DT.Rows[i]["DeptName"].ToString() + "',";
                if (Open_Stck.Rows.Count != 0)
                {
                    if (Open_Stck.Rows[0]["Open_Value"].ToString() != "")
                    {
                        SSQL = SSQL + " '" + Open_Stck.Rows[0]["Open_Value"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + " '0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + " '0.00',";
                }

                if (Purchase_DT.Rows.Count != 0)
                {
                    if (Purchase_DT.Rows[0]["PO_Val"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Purchase_DT.Rows[0]["PO_Val"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00',";
                }
                if (Issue_DT.Rows.Count != 0)
                {
                    if (Issue_DT.Rows[0]["Issue_Val"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Issue_DT.Rows[0]["Issue_Val"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00',";
                }


                SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

            }

            //Get Closing Stock
            SSQL = "Select * from Closing_Stock_Print where";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " order by DeptName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {

                if (RptName == "Closing Stock Statement")
                {

                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(GenReceiptDT);
                    RD_Report.Load(Server.MapPath("~/crystal/ClosingStockStatement.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);
                    //Company Details Add
                    DataTable dtdCompanyAddress = new DataTable();
                    DataTable Close_Det = new DataTable();

                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }


                    //Get Supplier Address
                    SSQL = "select * From Closing_Stock_Statement_Details where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    Close_Det = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (Close_Det.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["ProdInKGs"].Text = "'" + Close_Det.Rows[0]["ProdInKGs"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["SpareCostPerKGs"].Text = "'" + Close_Det.Rows[0]["SpareCostPerKGs"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["PackCostPerKGs"].Text = "'" + Close_Det.Rows[0]["PackCostPerKGs"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["FuelsPerKGs"].Text = "'" + Close_Det.Rows[0]["FuelsPerKGs"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["AlterErection"].Text = "'" + Close_Det.Rows[0]["AlterErection"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Machinery"].Text = "'" + Close_Det.Rows[0]["Machinery"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Civil"].Text = "'" + Close_Det.Rows[0]["Civil"].ToString() + "'";
                    }
                    else
                    {
                        RD_Report.DataDefinition.FormulaFields["ProdInKGs"].Text = "'0.00'";
                        RD_Report.DataDefinition.FormulaFields["SpareCostPerKGs"].Text = "'0.00'";
                        RD_Report.DataDefinition.FormulaFields["PackCostPerKGs"].Text = "'0.00'";
                        RD_Report.DataDefinition.FormulaFields["FuelsPerKGs"].Text = "'0.00'";
                        RD_Report.DataDefinition.FormulaFields["AlterErection"].Text = "'0.00'";
                        RD_Report.DataDefinition.FormulaFields["Machinery"].Text = "'0.00'";
                        RD_Report.DataDefinition.FormulaFields["Civil"].Text = "'0.00'";
                    }


                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }
                else if (RptName == "Closing Stock Statement without Details")
                {
                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(GenReceiptDT);
                    RD_Report.Load(Server.MapPath("~/crystal/ClosingStockStatementWithoutDetails.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);
                    //Company Details Add
                    DataTable dtdCompanyAddress = new DataTable();
                    DataTable Close_Det = new DataTable();

                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }
                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }




        }
        else
        {

            SSQL = "Delete  Closing_Stock_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select Distinct DeptName from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            for (int i = 0; i < Department_DT.Rows.Count; i++)
            {

                SSQL = "select ((sum(Open_Value)+sum(General_Value))-(sum(Issue_Value))) as Open_Value from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And DeptName='" + Department_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + frmDate.AddDays(-1).ToString("dd/MM/yyyy") + "',103)";

                Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);


                SSQL = "select sum(General_Value) as PO_Val";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And DeptName='" + Department_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "select sum(Issue_Value) as Issue_Val";
                SSQL = SSQL + " from Stock_Transaction_Ledger";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " And DeptName='" + Department_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);



                SSQL = "Insert Into Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,DeptName,Opening_Val,PO_Val,";
                SSQL = SSQL + "Issue_Val,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Department_DT.Rows[i]["DeptName"].ToString() + "',";
                if (Open_Stck.Rows.Count != 0)
                {
                    if (Open_Stck.Rows[0]["Open_Value"].ToString() != "")
                    {
                        SSQL = SSQL + " '" + Open_Stck.Rows[0]["Open_Value"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + " '0.00',";
                }

                if (Purchase_DT.Rows.Count != 0)
                {
                    if (Purchase_DT.Rows[0]["PO_Val"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Purchase_DT.Rows[0]["PO_Val"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00',";
                }
                if (Issue_DT.Rows.Count != 0)
                {
                    if (Issue_DT.Rows[0]["Issue_Val"].ToString() != "")
                    {
                        SSQL = SSQL + "'" + Issue_DT.Rows[0]["Issue_Val"] + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00',";
                    }
                }
                else
                {
                    SSQL = SSQL + "'0.00',";
                }


                SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);


            }



            //SSQL = "select DeptName,sum(Open_Value)+sum(General_Value)-sum(Issue_Value) as Opening_Val from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            //SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + frmDate.AddDays(-1).ToString("dd/MM/yyyy") + "',103)";
            //SSQL = SSQL + " group by DeptName order by DeptName";

            //ClosingStckDT = objdata.RptEmployeeMultipleDetails(SSQL);


            //SSQL = "select DeptName,sum(General_Value) as PO_Val,sum(Issue_Value) as Issue_Val";
            //SSQL = SSQL + " from Stock_Transaction_Ledger";
            //SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
            //SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            //SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            //SSQL = SSQL + " group by DeptName order by DeptName";

            //StockDT = objdata.RptEmployeeMultipleDetails(SSQL);

            //if (ClosingStckDT.Rows.Count != 0 && StockDT.Rows.Count != 0)
            //{
            //    SSQL = "Delete  Closing_Stock_Print";
            //    objdata.RptEmployeeMultipleDetails(SSQL);
            //    for (int i = 0; i < ClosingStckDT.Rows.Count; i++)
            //    {

            //        for (int j = 0; j < StockDT.Rows.Count; j++)
            //        {
            //            if (ClosingStckDT.Rows[i]["DeptName"].ToString() == StockDT.Rows[j]["DeptName"].ToString())
            //            {
            //                SSQL = "Insert Into Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,DeptName,Opening_Val,PO_Val,";
            //                SSQL = SSQL + "Issue_Val,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
            //                SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + ClosingStckDT.Rows[i]["DeptName"].ToString() + "',";
            //                SSQL = SSQL + " '" + ClosingStckDT.Rows[i]["Opening_Val"] + "','" + StockDT.Rows[j]["PO_Val"] + "','" + StockDT.Rows[j]["Issue_Val"] + "',";
            //                SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
            //                objdata.RptEmployeeMultipleDetails(SSQL);
            //            }
            //        }
            //    }
            //    DataTable DT_Pay = new DataTable();

            //    for (int i = 0; i < ClosingStckDT.Rows.Count; i++)
            //    {


            //        SSQL = "Select * from Closing_Stock_Print where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            //        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And DeptName='" + ClosingStckDT.Rows[i]["DeptName"].ToString() + "'";

            //        DT_Pay = objdata.RptEmployeeMultipleDetails(SSQL);
            //        if (DT_Pay.Rows.Count == 0)
            //        {

            //            SSQL = "Insert Into Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,DeptName,Opening_Val,PO_Val,";
            //            SSQL = SSQL + "Issue_Val,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
            //            SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + ClosingStckDT.Rows[i]["DeptName"].ToString() + "',";
            //            SSQL = SSQL + " '" + ClosingStckDT.Rows[i]["Opening_Val"] + "','0.00','0.00',";
            //            SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
            //            objdata.RptEmployeeMultipleDetails(SSQL);

            //        }

            //    }

            //    for (int i = 0; i < StockDT.Rows.Count; i++)
            //    {
            //        SSQL = "Select * from Closing_Stock_Print where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            //        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And DeptName='" + StockDT.Rows[i]["DeptName"].ToString() + "'";

            //        DT_Pay = objdata.RptEmployeeMultipleDetails(SSQL);
            //        if (DT_Pay.Rows.Count == 0)
            //        {

            //            SSQL = "Insert Into Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,DeptName,Opening_Val,PO_Val,";
            //            SSQL = SSQL + "Issue_Val,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
            //            SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + StockDT.Rows[i]["DeptName"].ToString() + "',";
            //            SSQL = SSQL + " '0.00','" + StockDT.Rows[i]["PO_Val"] + "','" + StockDT.Rows[i]["Issue_Val"] + "'";
            //            SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
            //            objdata.RptEmployeeMultipleDetails(SSQL);

            //        }

            //    }

            //}
            //else if (ClosingStckDT.Rows.Count != 0 && StockDT.Rows.Count == 0)
            //{
            //    SSQL = "Delete  Closing_Stock_Print";
            //    objdata.RptEmployeeMultipleDetails(SSQL);

            //    DataTable DT_Pay = new DataTable();

            //    for (int i = 0; i < ClosingStckDT.Rows.Count; i++)
            //    {


            //        SSQL = "Select * from Closing_Stock_Print where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            //        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And DeptName='" + ClosingStckDT.Rows[i]["DeptName"].ToString() + "'";

            //        DT_Pay = objdata.RptEmployeeMultipleDetails(SSQL);
            //        if (DT_Pay.Rows.Count == 0)
            //        {

            //            SSQL = "Insert Into Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,DeptName,Opening_Val,PO_Val,";
            //            SSQL = SSQL + "Issue_Val,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
            //            SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + ClosingStckDT.Rows[i]["DeptName"].ToString() + "',";
            //            SSQL = SSQL + " '" + ClosingStckDT.Rows[i]["Opening_Val"] + "','0.00','0.00',";
            //            SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
            //            objdata.RptEmployeeMultipleDetails(SSQL);

            //        }

            //    }


            //}
            //else if (ClosingStckDT.Rows.Count == 0 && StockDT.Rows.Count != 0)
            //{
            //    SSQL = "Delete  Closing_Stock_Print";
            //    objdata.RptEmployeeMultipleDetails(SSQL);
            //    DataTable DT_Pay = new DataTable();

            //    for (int i = 0; i < StockDT.Rows.Count; i++)
            //    {
            //        SSQL = "Select * from Closing_Stock_Print where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            //        SSQL = SSQL + " And FinYearCode='" + SessionFinYearCode + "' And DeptName='" + StockDT.Rows[i]["DeptName"].ToString() + "'";

            //        DT_Pay = objdata.RptEmployeeMultipleDetails(SSQL);
            //        if (DT_Pay.Rows.Count == 0)
            //        {

            //            SSQL = "Insert Into Closing_Stock_Print(Ccode,Lcode,FinYearCode,FinYearVal,DeptName,Opening_Val,PO_Val,";
            //            SSQL = SSQL + "Issue_Val,From_Date,To_Date,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
            //            SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + StockDT.Rows[i]["DeptName"].ToString() + "',";
            //            SSQL = SSQL + " '0.00','" + StockDT.Rows[i]["PO_Val"] + "','" + StockDT.Rows[i]["Issue_Val"] + "',";
            //            SSQL = SSQL + " '" + FromDate + "','" + ToDate + "','" + SessionUserID + "','" + SessionUserName + "')";
            //            objdata.RptEmployeeMultipleDetails(SSQL);

            //        }

            //    }
            //}

            //Get Closing Stock
            SSQL = "Select * from Closing_Stock_Print where";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " order by DeptName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {
                if (RptName == "Closing Stock Statement")
                {
                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(GenReceiptDT);
                    RD_Report.Load(Server.MapPath("~/crystal/ClosingStockStatement.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);
                    //Company Details Add
                    DataTable dtdCompanyAddress = new DataTable();
                    DataTable Close_Det = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }


                    //Get Supplier Address
                    SSQL = "select * From Closing_Stock_Statement_Details where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    Close_Det = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (Close_Det.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["ProdInKGs"].Text = "'" + Close_Det.Rows[0]["ProdInKGs"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["SpareCostPerKGs"].Text = "'" + Close_Det.Rows[0]["SpareCostPerKGs"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["PackCostPerKGs"].Text = "'" + Close_Det.Rows[0]["PackCostPerKGs"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["FuelsPerKGs"].Text = "'" + Close_Det.Rows[0]["FuelsPerKGs"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["AlterErection"].Text = "'" + Close_Det.Rows[0]["AlterErection"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Machinery"].Text = "'" + Close_Det.Rows[0]["Machinery"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Civil"].Text = "'" + Close_Det.Rows[0]["Civil"].ToString() + "'";
                    }
                    else
                    {
                        RD_Report.DataDefinition.FormulaFields["ProdInKGs"].Text = "'0.00'";
                        RD_Report.DataDefinition.FormulaFields["SpareCostPerKGs"].Text = "'0.00'";
                        RD_Report.DataDefinition.FormulaFields["PackCostPerKGs"].Text = "'0.00'";
                        RD_Report.DataDefinition.FormulaFields["FuelsPerKGs"].Text = "'0.00'";
                        RD_Report.DataDefinition.FormulaFields["AlterErection"].Text = "'0.00'";
                        RD_Report.DataDefinition.FormulaFields["Machinery"].Text = "'0.00'";
                        RD_Report.DataDefinition.FormulaFields["Civil"].Text = "'0.00'";
                    }


                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();

                }
                else if (RptName == "Closing Stock Statement without Details")
                {
                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(GenReceiptDT);
                    RD_Report.Load(Server.MapPath("~/crystal/ClosingStockStatementWithoutDetails.rpt"));
                    RD_Report.SetDataSource(ds1.Tables[0]);
                    //Company Details Add
                    DataTable dtdCompanyAddress = new DataTable();
                    DataTable Close_Det = new DataTable();

                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dtdCompanyAddress.Rows.Count != 0)
                    {
                        RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                        RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                    }
                    RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = RD_Report;
                    CrystalReportViewer1.RefreshReport();
                    CrystalReportViewer1.DataBind();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }


        }

    }


    public void ScrabReuse()
    {
        DataTable StockDT = new DataTable();
        DataTable Department_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable Purchase_DT = new DataTable();
        DataTable Issue_DT = new DataTable();

        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("Opening_Val");
        AutoDataTable.Columns.Add("PO_Val");
        AutoDataTable.Columns.Add("Issue_Val");


        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        string[] finyear = SessionFinYearVal.Split('_');
        string FromDate_Chk = "01/04/" + finyear[0];
        DateTime fromDate_Chk = Convert.ToDateTime(FromDate_Chk);


        string[] FromDate_ChkStr = FromDate.Split('/');

        //Get Closing Stock
        SSQL = "Select DeptName,ItemName, sum(Add_Qty) as OP_reuse_qty,SUM(Minus_Qty) as issue_Qty";
        SSQL = SSQL + " from Reuse_Stock_Ledger_All where";
        SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
        }

        SSQL = SSQL + " group by DeptName,ItemName ";

        GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);

        DataTable dtdCompanyAddress = new DataTable();
        DataTable Close_Det = new DataTable();
        DataSet ds1 = new DataSet();
        ds1.Tables.Add(GenReceiptDT);
        RD_Report.Load(Server.MapPath("~/crystal/Reuse Scrab.rpt"));
        RD_Report.SetDataSource(ds1.Tables[0]);

        SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dtdCompanyAddress.Rows.Count != 0)
        {
            RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
            RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "'";

            RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
        }

        if (GenReceiptDT.Rows.Count != 0)
        {


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();



        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }




    }

    public void Summary_Stock_Details_Reports()
    {
        DataTable StockDT = new DataTable();
        DataTable Department_DT = new DataTable();
        DataTable Item_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable Purchase_DT = new DataTable();
        DataTable Issue_DT = new DataTable();
        DataTable ZoneBin_DT = new DataTable();
        DataTable Item_UOM = new DataTable();
        bool Errflag_Fromdate = false;
        string Heading = "";
        DataTable da_check_Dept = new DataTable();
        DataTable da_Check_ItemName = new DataTable();

        SSQL = "delete from Print_stock_details ";
        da_Check_ItemName = objdata.RptEmployeeMultipleDetails(SSQL);

        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("CompanyCode");
        AutoDataTable.Columns.Add("LocationCode");
        AutoDataTable.Columns.Add("Address1");
        AutoDataTable.Columns.Add("Address2");
        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("ItemCode");
        AutoDataTable.Columns.Add("ItemName");
        AutoDataTable.Columns.Add("QTY");
        AutoDataTable.Columns.Add("Rate");
        AutoDataTable.Columns.Add("Amount");


        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        //string[] finyear = SessionFinYearVal.Split('_');
        //string FromDate_Chk = "01/04/" + finyear[0];
        //DateTime fromDate_Chk = Convert.ToDateTime(FromDate_Chk);


        string[] FromDate_ChkStr = FromDate.Split('/');

        SSQL = "select distinct DeptName from Stock_Transaction_Ledger ";
        SSQL = SSQL + " where (Trans_Type ='UNPLANNED RECEIPT' or Trans_Type ='OPENING RECEIPT'  or Trans_Type='MATERIAL ISSUE' or Trans_Type='GATE PASS OUT' )";
        SSQL = SSQL + " and  Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        SSQL = SSQL + "  And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' ";
        if (FromDate != "" && ToDate != "")
        {
            string DateFormat = FromDate + "   ToDate :" + ToDate;
            SSQL = SSQL + " And CONVERT(varchar(20),Trans_Date, 103)>=CONVERT(varchar(20),'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(varchar(20),Trans_Date, 103)<=CONVERT(varchar(20),'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            Heading = "FromDate : " + DateFormat;
            Errflag_Fromdate = true;
        }

        da_check_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

        if (da_check_Dept.Rows.Count != 0)
        {
            string DeptName_val = "";
            string DeptCode = "";
            for (int k = 0; k < da_check_Dept.Rows.Count; k++)
            {
                DeptName_val = da_check_Dept.Rows[k]["DeptName"].ToString();

                SSQL = "select distinct ItemName from Stock_Transaction_Ledger";
                SSQL = SSQL + " where (Trans_Type ='UNPLANNED RECEIPT' or Trans_Type ='OPENING RECEIPT'  or Trans_Type='MATERIAL ISSUE' or Trans_Type='GATE PASS OUT')";
                SSQL = SSQL + " and DeptName='" + DeptName_val + "'";
                SSQL = SSQL + " and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                SSQL = SSQL + "  And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' ";
                da_Check_ItemName = objdata.RptEmployeeMultipleDetails(SSQL);

                for (int j = 0; j < da_Check_ItemName.Rows.Count; j++)
                {
                    DataTable da_Val = new DataTable();
                    SSQL = "select Distinct ItemCode,ItemName  ,SUM(Add_Qty) - SUM(Minus_Qty)  as QTY, ";
                    SSQL = SSQL + " SUM(Add_Value)-SUM(Minus_Value) as Amount from Stock_Ledger_All ";
                    SSQL = SSQL + " where (Trans_Type ='UNPLANNED RECEIPT' or Trans_Type ='OPENING RECEIPT'  or Trans_Type='MATERIAL ISSUE' or Trans_Type='GATE PASS OUT')";
                    SSQL = SSQL + " and ItemName='" + da_Check_ItemName.Rows[j]["ItemName"].ToString() + "'";
                    SSQL = SSQL + " and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    SSQL = SSQL + "  And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' ";
                    SSQL = SSQL + " group by ItemCode,ItemName";
                    da_Val = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (da_Val.Rows.Count != 0)
                    {
                        DataTable da_chek_print = new DataTable();


                        SSQL = "select * from Print_stock_details where ItemName='" + da_Check_ItemName.Rows[j]["ItemName"].ToString() + "'";
                        da_chek_print = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (da_chek_print.Rows.Count == 0)
                        {
                            SSQL = "insert into Print_stock_details(Ccode,LCode,DeptName,ItemCode,ItemName,Qty,Amount)";
                            SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + DeptName_val + "',";
                            SSQL = SSQL + " '" + da_Val.Rows[0]["ItemCode"].ToString() + "','" + da_Val.Rows[0]["ItemName"].ToString() + "',";
                            SSQL = SSQL + " '" + da_Val.Rows[0]["QTY"].ToString() + "','" + da_Val.Rows[0]["Amount"].ToString() + "')";
                            da_chek_print = objdata.RptEmployeeMultipleDetails(SSQL);
                        }

                    }

                }
            }

            DataTable da_check_val = new DataTable();
            SSQL = "select DeptName,SUM(Qty) as Qty,SUM(Amount) as Amount from Print_stock_details group by DeptName order by DeptName asc";
            da_check_val = objdata.RptEmployeeMultipleDetails(SSQL);
            if (da_check_val.Rows.Count != 0)
            {
                for (int j = 0; j < da_check_val.Rows.Count; j++)
                {
                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();
                    AutoDataTable.Rows[j]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDataTable.Rows[j]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDataTable.Rows[j]["CompanyCode"] = SessionCcode;
                    AutoDataTable.Rows[j]["LocationCode"] = SessionLcode;
                    AutoDataTable.Rows[j]["DeptName"] = da_check_val.Rows[j]["DeptName"];
                    AutoDataTable.Rows[j]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDataTable.Rows[j]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
                    AutoDataTable.Rows[j]["QTY"] = da_check_val.Rows[j]["QTY"];
                    AutoDataTable.Rows[j]["Amount"] = da_check_val.Rows[j]["Amount"];
                }
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDataTable);
                RD_Report.Load(Server.MapPath("~/crystal/Summary_StockDetailsStatement.rpt"));

                RD_Report.SetDataSource(ds1.Tables[0]);
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }



        }
        else
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void Old_Stock_Reports()
    {
        DataTable StockDT = new DataTable();
        DataTable Department_DT = new DataTable();
        DataTable Item_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable Purchase_DT = new DataTable();
        DataTable Issue_DT = new DataTable();
        DataTable ZoneBin_DT = new DataTable();
        DataTable Item_UOM = new DataTable();
        bool Errflag_Fromdate = false;
        string Heading = "";
        DataTable da_check_Dept = new DataTable();
        DataTable da_Check_ItemName = new DataTable();

        SSQL = "delete from Print_stock_details ";
        da_Check_ItemName = objdata.RptEmployeeMultipleDetails(SSQL);

        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("CompanyCode");
        AutoDataTable.Columns.Add("LocationCode");
        AutoDataTable.Columns.Add("Address1");
        AutoDataTable.Columns.Add("Address2");
        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("ItemCode");
        AutoDataTable.Columns.Add("ItemName");
        AutoDataTable.Columns.Add("QTY");
        AutoDataTable.Columns.Add("Rate");
        AutoDataTable.Columns.Add("Amount");


        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        //string[] finyear = SessionFinYearVal.Split('_');
        //string FromDate_Chk = "01/04/" + finyear[0];
        //DateTime fromDate_Chk = Convert.ToDateTime(FromDate_Chk);


        string[] FromDate_ChkStr = FromDate.Split('/');

        SSQL = "select distinct DeptName from Reuse_Stock_Ledger_All ";
        SSQL = SSQL + " where (Trans_Type ='PURCHASE OLD' or Trans_Type ='OPENING REUSE')";
        SSQL = SSQL + " and  Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        SSQL = SSQL + "  And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' ";
        if (FromDate != "" && ToDate != "")
        {
            string DateFormat = FromDate + "   ToDate :" + ToDate;
            SSQL = SSQL + " And CONVERT(varchar(20),Trans_Date, 103)>=CONVERT(varchar(20),'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(varchar(20),Trans_Date, 103)<=CONVERT(varchar(20),'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            Heading = "FromDate : " + DateFormat;
            Errflag_Fromdate = true;
        }
        if (WareHouse != "-Select-")
        {
            SSQL = SSQL + " And WarehouseName='" + WareHouse + "'";
            Heading = "WarehouseName = " + WareHouse;
        }

        if (ItemName != "-Select-")
        {
            SSQL = SSQL + " And ItemName='" + ItemName + "'";
            Heading = "ItemName = " + ItemName;
        }

        if (CostCenter != "-Select- ")
        {
            SSQL = SSQL + " And CostCenterName='" + CostCenter + "'";
            Heading = "CostCenterName : " + CostCenter;
        }
        //if (DeptName != "-Select- ")
        //{
        //    SSQL = SSQL + " And DeptName='" + DeptName + "'";
        //    Heading = "DeptName : " + DeptName;
        //}


        da_check_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

        if (da_check_Dept.Rows.Count != 0)
        {
            string DeptName_val = "";
            string DeptCode = "";
            for (int k = 0; k < da_check_Dept.Rows.Count; k++)
            {
                DeptName_val = da_check_Dept.Rows[k]["DeptName"].ToString();

                if (DeptName_val == "OPEN END MACHINE")
                {
                    DeptName_val = "OPEN END MACHINE";
                }

                SSQL = "select distinct ItemName from Reuse_Stock_Ledger_All";
                SSQL = SSQL + " where (Trans_Type ='PURCHASE OLD' or Trans_Type ='OPENING REUSE')";
                SSQL = SSQL + " and DeptName='" + DeptName_val + "'";
                SSQL = SSQL + " and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                SSQL = SSQL + "  And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' ";
                da_Check_ItemName = objdata.RptEmployeeMultipleDetails(SSQL);

                for (int j = 0; j < da_Check_ItemName.Rows.Count; j++)
                {
                    DataTable da_Val = new DataTable();
                    string item_Name = da_Check_ItemName.Rows[j]["ItemName"].ToString();
                    if (item_Name == "GUIDE BLOCK BUSH (TOP ONLY)")
                    {
                        item_Name = "GUIDE BLOCK BUSH (TOP ONLY)";
                    }

                    SSQL = "select Distinct ItemCode,ItemName  ,SUM(Add_Qty) - SUM(Minus_Qty)  as QTY, ";
                    SSQL = SSQL + " SUM(Add_Value)-SUM(Minus_Value) as Amount from Reuse_Stock_Ledger_All ";
                    // SSQL = SSQL + " where (Trans_Type ='UNPLANNED RECEIPT' or Trans_Type ='OPENING RECEIPT') and";
                    SSQL = SSQL + "  where ItemName='" + da_Check_ItemName.Rows[j]["ItemName"].ToString() + "'";
                    SSQL = SSQL + " and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    SSQL = SSQL + "  And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' ";
                    SSQL = SSQL + " group by ItemCode,ItemName";
                    da_Val = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (da_Val.Rows.Count != 0)
                    {
                        DataTable da_chek_print = new DataTable();


                        SSQL = "select * from Print_stock_details where ItemName='" + da_Check_ItemName.Rows[j]["ItemName"].ToString() + "'";
                        da_chek_print = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (da_chek_print.Rows.Count == 0)
                        {
                            SSQL = "insert into Print_stock_details(Ccode,LCode,DeptName,ItemCode,ItemName,Qty,Amount)";
                            SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + DeptName_val + "',";
                            SSQL = SSQL + " '" + da_Val.Rows[0]["ItemCode"].ToString() + "','" + da_Val.Rows[0]["ItemName"].ToString() + "',";
                            SSQL = SSQL + " '" + da_Val.Rows[0]["QTY"].ToString() + "','" + da_Val.Rows[0]["Amount"].ToString() + "')";
                            da_chek_print = objdata.RptEmployeeMultipleDetails(SSQL);
                        }


                    }

                }
            }

            DataTable da_check_val = new DataTable();
            SSQL = "select * from Print_stock_details where Qty<>'0' ";
            da_check_val = objdata.RptEmployeeMultipleDetails(SSQL);
            if (da_check_val.Rows.Count != 0)
            {
                for (int j = 0; j < da_check_val.Rows.Count; j++)
                {
                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();
                    AutoDataTable.Rows[j]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDataTable.Rows[j]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDataTable.Rows[j]["CompanyCode"] = SessionCcode;
                    AutoDataTable.Rows[j]["LocationCode"] = SessionLcode;
                    AutoDataTable.Rows[j]["DeptName"] = da_check_val.Rows[j]["DeptName"];

                    AutoDataTable.Rows[j]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDataTable.Rows[j]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
                    AutoDataTable.Rows[j]["ItemCode"] = da_check_val.Rows[j]["ItemCode"]; ;
                    AutoDataTable.Rows[j]["ItemName"] = da_check_val.Rows[j]["ItemName"]; ;
                    AutoDataTable.Rows[j]["QTY"] = da_check_val.Rows[j]["QTY"];
                    AutoDataTable.Rows[j]["Amount"] = da_check_val.Rows[j]["Amount"];
                }
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDataTable);
                RD_Report.Load(Server.MapPath("~/crystal/Sample_Old_StockDetailsStatement.rpt"));
                RD_Report.DataDefinition.FormulaFields["ReportHeading"].Text = "'" + Heading + "'";
                RD_Report.SetDataSource(ds1.Tables[0]);
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }



        }
        else
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }


    public void Dept_Stock_Details()
    {
        DataTable StockDT = new DataTable();
        DataTable Department_DT = new DataTable();
        DataTable Item_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable Purchase_DT = new DataTable();
        DataTable Issue_DT = new DataTable();
        DataTable ZoneBin_DT = new DataTable();
        DataTable Item_UOM = new DataTable();
        bool Errflag_Fromdate = false;
        string Heading = "";
        DataTable da_check_Dept = new DataTable();
        DataTable da_Check_ItemName = new DataTable();

        SSQL = "delete from Print_stock_details ";
        da_Check_ItemName = objdata.RptEmployeeMultipleDetails(SSQL);

        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("CompanyCode");
        AutoDataTable.Columns.Add("LocationCode");
        AutoDataTable.Columns.Add("Address1");
        AutoDataTable.Columns.Add("Address2");
        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("ItemCode");
        AutoDataTable.Columns.Add("ItemName");
        AutoDataTable.Columns.Add("CostName");

        AutoDataTable.Columns.Add("Open_Qty");
        AutoDataTable.Columns.Add("Open_Rate");
        AutoDataTable.Columns.Add("Open_Value");

        AutoDataTable.Columns.Add("Pur_Qty");
        AutoDataTable.Columns.Add("Pur_Rate");
        AutoDataTable.Columns.Add("Pur_Value");

        AutoDataTable.Columns.Add("TotalQty");
        AutoDataTable.Columns.Add("TotalValue");

        AutoDataTable.Columns.Add("Iss_Qty");
        AutoDataTable.Columns.Add("Iss_Rate");
        AutoDataTable.Columns.Add("Iss_Value");

        AutoDataTable.Columns.Add("Stock_Qty");
        AutoDataTable.Columns.Add("Stock_Value");



        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        //A.CostName, Group by A.CostName

        SSQL = "select A.DeptName ,A.ItemCode  ,A.ItemName,sum(A.Open_Qty) as[Open_Qty],sum(A.Open_Rate) as [Open_Rate] ,sum(A.Open_Qty * A.Open_Rate) as [Open_Value],";
        SSQL = SSQL + "sum(A.Pur_Qty) as [Pur_Qty],sum(A.Pur_Rate) as [Pur_Rate],sum(A.Pur_Qty * A.Pur_Rate) as [Pur_Value],sum(A.Open_Qty + A.Pur_Qty) as [TotalQty],";
        SSQL = SSQL + "sum(A.Open_Rate + A.Pur_Rate) as [TotalValue], sum(A.Iss_Qty) as [Iss_Qty], sum(A.Iss_Rate) as [Iss_Rate],SUM(A.Iss_Qty * A.Iss_Rate) as [Iss_Value],";
        SSQL = SSQL + "sum((A.Open_Qty + A.Pur_Qty) - A.Iss_Qty) as[Stock_Qty], sum((A.Open_Rate + A.Pur_Rate) - A.Iss_Rate) as [Stock_Value], ";
        SSQL = SSQL + "'" + frmDate.ToString("dd/MM/yyyy") + "' as [address1],'" + toDate.ToString("dd/MM/yyyy") + "' as [address2]";
        SSQL = SSQL + " From ( ";
        SSQL = SSQL + "select SLA.DeptName as[DeptName],SLA.ItemCode as [ItemCode],SLA.ItemName As[ItemName],SLA.CostCenterName as [CostName],(SUM(SLA.Add_Qty) - SUM(Minus_Qty)) as Open_Qty, (sum(SLA.Add_Value) - SUM(SLA.Minus_Value))Open_Rate,";
        SSQL = SSQL + "0 Pur_Qty, 0 as Pur_Rate, 0 Iss_Qty, 0 Iss_Rate From Stock_Ledger_All SLA ";
        SSQL = SSQL + "Where SLA.Ccode='" + SessionCcode + "' And SLA.Lcode='" + SessionLcode + "' And SLA.FinYearCode='" + SessionFinYearCode + "'";

        if (DeptName != "-Select- ")
        {
            SSQL = SSQL + "And SLA.DeptName='" + DeptName + "'";
        }

        if (WareHouse != "-Select-")
        {
            SSQL = SSQL + "And SLA.WarehouseName='" + WareHouse + "'";
        }

        if (CostCenter != "-Select- ")
        {
            SSQL = SSQL + "And SLA.CostCenterName='" + CostCenter + "'";
        }

        if (ItemName != "-Select-")
        {
            SSQL = SSQL + "And SLA.ItemName='" + ItemName + "'";
        }

        SSQL = SSQL + " And CONVERT(datetime,SLA.Trans_Date, 103) < CONVERT(datetime, '" + frmDate.ToString("dd/MM/yyyy") + "', 103) Group By SLA.DeptName,SLA.ItemName,SLA.ItemCode,SLA.CostCenterName ";

        SSQL = SSQL + "Union All ";

        SSQL = SSQL + "select SLA.DeptName as[DeptName],SLA.ItemCode as [ItemCode], SLA.ItemName As [ItemName],SLA.CostCenterName as [CostName],0 Open_Rate ,0 Open_Rate,";
        SSQL = SSQL + "SUM(SLA.Add_Qty) Pur_Qty,sum(SLA.Add_Value)Pur_Rate,sum(SLA.Minus_Qty)Iss_Qty,SUM(SLA.Minus_Value)Iss_Rate From Stock_Ledger_All SLA ";
        SSQL = SSQL + "Where SLA.Ccode='" + SessionCcode + "' And SLA.Lcode='" + SessionLcode + "' And SLA.FinYearCode='" + SessionFinYearCode + "'";

        if (DeptName != "-Select- ")
        {
            SSQL = SSQL + "And SLA.DeptName='" + DeptName + "'";
        }

        if (WareHouse != "-Select-")
        {
            SSQL = SSQL + "And SLA.WarehouseName='" + WareHouse + "'";
        }

        if (CostCenter != "-Select- ")
        {
            SSQL = SSQL + "And SLA.CostCenterName='" + CostCenter + "'";
        }

        if (ItemName != "-Select-")
        {
            SSQL = SSQL + "And SLA.ItemName='" + ItemName + "'";
        }

        SSQL = SSQL + " And CONVERT(datetime,SLA.Trans_Date, 103) >= CONVERT(datetime, '" + frmDate.ToString("dd/MM/yyyy") + "', 103) ";
        SSQL = SSQL + " And CONVERT(datetime,SLA.Trans_Date, 103) <= CONVERT(datetime, '" + toDate.ToString("dd/MM/yyyy") + "', 103) ";
        SSQL = SSQL + "Group By SLA.DeptName,SLA.ItemName,SLA.ItemCode,SLA.CostCenterName)A Group by A.DeptName ,A.ItemCode  ,A.ItemName";

        da_check_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

        if (da_check_Dept.Rows.Count > 0)
        {
            for (int j = 0; j < da_check_Dept.Rows.Count; j++)
            {
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();
                AutoDataTable.Rows[j]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                AutoDataTable.Rows[j]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                AutoDataTable.Rows[j]["CompanyCode"] = SessionCcode;
                AutoDataTable.Rows[j]["LocationCode"] = SessionLcode;
                AutoDataTable.Rows[j]["DeptName"] = da_check_Dept.Rows[j]["DeptName"];

                AutoDataTable.Rows[j]["Address1"] = frmDate.ToString("dd/MM/yyyy");
                AutoDataTable.Rows[j]["Address2"] = toDate.ToString("dd/MM/yyyy");
                AutoDataTable.Rows[j]["ItemCode"] = da_check_Dept.Rows[j]["ItemCode"];
                AutoDataTable.Rows[j]["ItemName"] = da_check_Dept.Rows[j]["ItemName"];
                //AutoDataTable.Rows[j]["CostName"] = da_check_Dept.Rows[j]["CostName"];


                AutoDataTable.Rows[j]["Open_Qty"] = da_check_Dept.Rows[j]["Open_Qty"];
                AutoDataTable.Rows[j]["Open_Rate"] = da_check_Dept.Rows[j]["Open_Rate"];
                AutoDataTable.Rows[j]["Open_Value"] = da_check_Dept.Rows[j]["Open_Value"];

                AutoDataTable.Rows[j]["Pur_Qty"] = da_check_Dept.Rows[j]["Pur_Qty"];
                AutoDataTable.Rows[j]["Pur_Rate"] = da_check_Dept.Rows[j]["Pur_Rate"];
                AutoDataTable.Rows[j]["Pur_Value"] = da_check_Dept.Rows[j]["Pur_Value"];

                AutoDataTable.Rows[j]["TotalQty"] = da_check_Dept.Rows[j]["TotalQty"];
                AutoDataTable.Rows[j]["TotalValue"] = da_check_Dept.Rows[j]["TotalValue"];

                AutoDataTable.Rows[j]["Iss_Qty"] = da_check_Dept.Rows[j]["Iss_Qty"];
                AutoDataTable.Rows[j]["Iss_Rate"] = da_check_Dept.Rows[j]["Iss_Rate"];
                AutoDataTable.Rows[j]["Iss_Value"] = da_check_Dept.Rows[j]["Iss_Value"];

                AutoDataTable.Rows[j]["Stock_Qty"] = da_check_Dept.Rows[j]["Stock_Qty"];
                AutoDataTable.Rows[j]["Stock_Value"] = da_check_Dept.Rows[j]["Stock_Value"];

            }
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDataTable);
            RD_Report.Load(Server.MapPath("~/crystal/Dept_StockDetailsStatement.rpt"));
            RD_Report.DataDefinition.FormulaFields["ReportHeading"].Text = "'" + Heading + "'";
            RD_Report.SetDataSource(ds1.Tables[0]);
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }

    public void Dept_Summary_Details()
    {
        DataTable StockDT = new DataTable();
        DataTable Department_DT = new DataTable();
        DataTable Item_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable Purchase_DT = new DataTable();
        DataTable Issue_DT = new DataTable();
        DataTable ZoneBin_DT = new DataTable();
        DataTable Item_UOM = new DataTable();
        bool Errflag_Fromdate = false;
        string Heading = "";
        DataTable da_check_Dept = new DataTable();
        DataTable da_Check_ItemName = new DataTable();

        SSQL = "delete from Print_stock_details ";
        da_Check_ItemName = objdata.RptEmployeeMultipleDetails(SSQL);

        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("CompanyCode");
        AutoDataTable.Columns.Add("LocationCode");
        AutoDataTable.Columns.Add("Address1");
        AutoDataTable.Columns.Add("Address2");
        AutoDataTable.Columns.Add("DeptName");

        AutoDataTable.Columns.Add("Open_Qty");
        AutoDataTable.Columns.Add("Open_Rate");
        AutoDataTable.Columns.Add("Open_Value");

        AutoDataTable.Columns.Add("Pur_Qty");
        AutoDataTable.Columns.Add("Pur_Rate");
        AutoDataTable.Columns.Add("Pur_Value");

        AutoDataTable.Columns.Add("TotalQty");
        AutoDataTable.Columns.Add("TotalValue");

        AutoDataTable.Columns.Add("Iss_Qty");
        AutoDataTable.Columns.Add("Iss_Rate");
        AutoDataTable.Columns.Add("Iss_Value");

        AutoDataTable.Columns.Add("Stock_Qty");
        AutoDataTable.Columns.Add("Stock_Value");



        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select A.DeptName ,";
        SSQL = SSQL + "sum(A.Open_Qty) as [Open_Qty],sum(A.Open_Rate) as [Open_Rate] ,";
        SSQL = SSQL + "sum(A.Open_Qty* A.Open_Rate) as [Open_Value],";
        SSQL = SSQL + "sum(A.Pur_Qty) as [Pur_Qty] ,sum(A.Pur_Rate) as [Pur_Rate],";
        SSQL = SSQL + "sum(A.Pur_Qty* A.Pur_Rate)as [Pur_Value],";
        SSQL = SSQL + "sum(A.Open_Qty + A.Pur_Qty) as [TotalQty] , sum(A.Open_Rate + A.Pur_Rate) as [TotalValue],";
        SSQL = SSQL + "sum(A.Iss_Qty) as [Iss_Qty] ,sum( A.Iss_Rate) as [Iss_Rate],";
        SSQL = SSQL + "sum(A.Iss_Qty * A.Iss_Rate) as [Iss_Value],";
        SSQL = SSQL + "sum((A.Open_Qty + A.Pur_Qty) - A.Iss_Qty) as [Stock_Qty],sum((A.Open_Rate + A.Pur_Rate) - A.Iss_Rate) as [Stock_Value] ";
        SSQL = SSQL + " From ( ";
        SSQL = SSQL + "select SLA.DeptName as[DeptName],(SUM(SLA.Add_Qty) - SUM(Minus_Qty)) as Open_Qty, (sum(SLA.Add_Value) - SUM(SLA.Minus_Value))Open_Rate,";
        SSQL = SSQL + "0 Pur_Qty, 0 as Pur_Rate, 0 Iss_Qty, 0 Iss_Rate From Stock_Ledger_All SLA ";
        SSQL = SSQL + "Where SLA.Ccode='" + SessionCcode + "' And SLA.Lcode='" + SessionLcode + "' And SLA.FinYearCode='" + SessionFinYearCode + "'";

        if (DeptName != "-Select- ")
        {
            SSQL = SSQL + "And SLA.DeptName='" + DeptName + "'";
        }

        if (WareHouse != "-Select-")
        {
            SSQL = SSQL + "And SLA.WarehouseName='" + WareHouse + "'";
        }

        SSQL = SSQL + " And CONVERT(datetime,SLA.Trans_Date, 103) < CONVERT(datetime, '" + frmDate.ToString("dd/MM/yyyy") + "', 103) Group By SLA.DeptName ";

        SSQL = SSQL + "Union All ";

        SSQL = SSQL + "select SLA.DeptName as[DeptName],0 Open_Rate ,0 Open_Rate,";
        SSQL = SSQL + "SUM(SLA.Add_Qty) Pur_Qty,sum(SLA.Add_Value)Pur_Rate,sum(SLA.Minus_Qty)Iss_Qty,SUM(SLA.Minus_Value)Iss_Rate From Stock_Ledger_All SLA ";
        SSQL = SSQL + "Where SLA.Ccode='" + SessionCcode + "' And SLA.Lcode='" + SessionLcode + "' And SLA.FinYearCode='" + SessionFinYearCode + "'";

        if (DeptName != "-Select- ")
        {
            SSQL = SSQL + "And SLA.DeptName='" + DeptName + "'";
        }

        if (WareHouse != "-Select-")
        {
            SSQL = SSQL + "And SLA.WarehouseName='" + WareHouse + "'";
        }

        SSQL = SSQL + " And CONVERT(datetime,SLA.Trans_Date, 103) >= CONVERT(datetime, '" + frmDate.ToString("dd/MM/yyyy") + "', 103) ";
        SSQL = SSQL + " And CONVERT(datetime,SLA.Trans_Date, 103) <= CONVERT(datetime, '" + toDate.ToString("dd/MM/yyyy") + "', 103) ";
        SSQL = SSQL + "Group By SLA.DeptName)A Group By A.DeptName ";



        da_check_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

        if (da_check_Dept.Rows.Count > 0)
        {
            for (int j = 0; j < da_check_Dept.Rows.Count; j++)
            {
                DataTable dtdCompanyAddress = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();
                AutoDataTable.Rows[j]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                AutoDataTable.Rows[j]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                AutoDataTable.Rows[j]["CompanyCode"] = SessionCcode;
                AutoDataTable.Rows[j]["LocationCode"] = SessionLcode;
                AutoDataTable.Rows[j]["DeptName"] = da_check_Dept.Rows[j]["DeptName"];

                AutoDataTable.Rows[j]["Address1"] = frmDate.ToString("dd/MM/yyyy");
                AutoDataTable.Rows[j]["Address2"] = toDate.ToString("dd/MM/yyyy");
                //AutoDataTable.Rows[j]["ItemCode"] = da_check_Dept.Rows[j]["ItemCode"];
                //AutoDataTable.Rows[j]["ItemName"] = da_check_Dept.Rows[j]["ItemName"];


                AutoDataTable.Rows[j]["Open_Qty"] = da_check_Dept.Rows[j]["Open_Qty"];
                AutoDataTable.Rows[j]["Open_Rate"] = da_check_Dept.Rows[j]["Open_Rate"];
                AutoDataTable.Rows[j]["Open_Value"] = da_check_Dept.Rows[j]["Open_Value"];

                AutoDataTable.Rows[j]["Pur_Qty"] = da_check_Dept.Rows[j]["Pur_Qty"];
                AutoDataTable.Rows[j]["Pur_Rate"] = da_check_Dept.Rows[j]["Pur_Rate"];
                AutoDataTable.Rows[j]["Pur_Value"] = da_check_Dept.Rows[j]["Pur_Value"];

                AutoDataTable.Rows[j]["TotalQty"] = da_check_Dept.Rows[j]["TotalQty"];
                AutoDataTable.Rows[j]["TotalValue"] = da_check_Dept.Rows[j]["TotalValue"];

                AutoDataTable.Rows[j]["Iss_Qty"] = da_check_Dept.Rows[j]["Iss_Qty"];
                AutoDataTable.Rows[j]["Iss_Rate"] = da_check_Dept.Rows[j]["Iss_Rate"];
                AutoDataTable.Rows[j]["Iss_Value"] = da_check_Dept.Rows[j]["Iss_Value"];

                AutoDataTable.Rows[j]["Stock_Qty"] = da_check_Dept.Rows[j]["Stock_Qty"];
                AutoDataTable.Rows[j]["Stock_Value"] = da_check_Dept.Rows[j]["Stock_Value"];

            }
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(AutoDataTable);
            RD_Report.Load(Server.MapPath("~/crystal/Dept_Summary_Details.rpt"));
            RD_Report.DataDefinition.FormulaFields["ReportHeading"].Text = "'" + Heading + "'";
            RD_Report.SetDataSource(ds1.Tables[0]);
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void Sample_Stock_Details_Reports()
    {
        DataTable StockDT = new DataTable();
        DataTable Department_DT = new DataTable();
        DataTable Item_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable Purchase_DT = new DataTable();
        DataTable Issue_DT = new DataTable();
        DataTable ZoneBin_DT = new DataTable();
        DataTable Item_UOM = new DataTable();
        bool Errflag_Fromdate = false;
        string Heading = "";
        DataTable da_check_Dept = new DataTable();
        DataTable da_Check_ItemName = new DataTable();

        SSQL = "delete from Print_stock_details ";
        da_Check_ItemName = objdata.RptEmployeeMultipleDetails(SSQL);

        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("CompanyCode");
        AutoDataTable.Columns.Add("LocationCode");
        AutoDataTable.Columns.Add("Address1");
        AutoDataTable.Columns.Add("Address2");
        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("ItemCode");
        AutoDataTable.Columns.Add("ItemName");
        AutoDataTable.Columns.Add("QTY");
        AutoDataTable.Columns.Add("Rate");
        AutoDataTable.Columns.Add("Amount");
        //  AutoDataTable.Columns.Add("Cost");


        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        //string[] finyear = SessionFinYearVal.Split('_');
        //string FromDate_Chk = "01/04/" + finyear[0];
        //DateTime fromDate_Chk = Convert.ToDateTime(FromDate_Chk);


        string[] FromDate_ChkStr = FromDate.Split('/');

        SSQL = "select distinct DeptName from Stock_Transaction_Ledger ";
        SSQL = SSQL + " where (Trans_Type ='UNPLANNED RECEIPT' or Trans_Type ='OPENING RECEIPT' or Trans_Type='MATERIAL ISSUE' or Trans_Type='GATE PASS OUT' )";
        SSQL = SSQL + " and  Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        SSQL = SSQL + "  And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' ";
        if (FromDate != "" && ToDate != "")
        {
            string DateFormat = FromDate + "   ToDate :" + ToDate;
            SSQL = SSQL + " And CONVERT(varchar(20),Trans_Date, 103)>=CONVERT(varchar(20),'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(varchar(20),Trans_Date, 103)<=CONVERT(varchar(20),'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            Heading = "FromDate : " + DateFormat;
            Errflag_Fromdate = true;
        }
        if (WareHouse != "-Select-")
        {
            SSQL = SSQL + " And WarehouseName='" + WareHouse + "'";
            Heading = "WarehouseName = " + WareHouse;
        }

        if (ItemName != "-Select-")
        {
            SSQL = SSQL + " And ItemName='" + ItemName + "'";
            Heading = "ItemName = " + ItemName;
        }

        if (CostCenter != "-Select- ")
        {
            SSQL = SSQL + " And CostCenterName='" + CostCenter + "'";
            Heading = "CostCenterName : " + CostCenter;
        }
        if (DeptName != "-Select- ")
        {
            SSQL = SSQL + " And DeptName='" + DeptName + "'";
            Heading = "DeptName : " + DeptName;
        }


        da_check_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

        if (da_check_Dept.Rows.Count != 0)
        {
            string DeptName_val = "";
            string DeptCode = "";
            for (int k = 0; k < da_check_Dept.Rows.Count; k++)
            {
                DeptName_val = da_check_Dept.Rows[k]["DeptName"].ToString();

                if (DeptName_val == "DRAW FRAME")
                {
                    DeptName_val = "DRAW FRAME";
                }

                SSQL = "select distinct ItemName from Stock_Transaction_Ledger";
                SSQL = SSQL + " where (Trans_Type ='UNPLANNED RECEIPT' or Trans_Type ='OPENING RECEIPT' or Trans_Type='MATERIAL ISSUE' or Trans_Type='GATE PASS OUT' )";
                SSQL = SSQL + " and DeptName='" + DeptName_val + "'";
                SSQL = SSQL + " and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                SSQL = SSQL + "  And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' ";
                if (ItemName != "-Select-")
                {
                    SSQL = SSQL + " And ItemName='" + ItemName + "'";
                    Heading = "ItemName = " + ItemName;
                }
                da_Check_ItemName = objdata.RptEmployeeMultipleDetails(SSQL);

                for (int j = 0; j < da_Check_ItemName.Rows.Count; j++)
                {
                    DataTable da_Val = new DataTable();
                    SSQL = "select Distinct ItemCode,ItemName  ,SUM(Add_Qty) - SUM(Minus_Qty)  as QTY, ";
                    SSQL = SSQL + " SUM(Add_Value)-SUM(Minus_Value) as Amount from Stock_Ledger_All ";
                    SSQL = SSQL + " where (Trans_Type ='UNPLANNED RECEIPT' or Trans_Type ='OPENING RECEIPT'  or Trans_Type='MATERIAL ISSUE' or Trans_Type='GATE PASS OUT') ";
                    SSQL = SSQL + "  and ItemName='" + da_Check_ItemName.Rows[j]["ItemName"].ToString() + "'";
                    SSQL = SSQL + " and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    SSQL = SSQL + "  And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' ";
                    SSQL = SSQL + " group by ItemCode,ItemName";
                    da_Val = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (da_Val.Rows.Count != 0)
                    {
                        DataTable da_chek_print = new DataTable();


                        SSQL = "select * from Print_stock_details where ItemName='" + da_Check_ItemName.Rows[j]["ItemName"].ToString() + "'";
                        da_chek_print = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (da_chek_print.Rows.Count == 0)
                        {
                            SSQL = "insert into Print_stock_details(Ccode,LCode,DeptName,ItemCode,ItemName,Qty,Amount)";
                            SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + DeptName_val + "',";
                            SSQL = SSQL + " '" + da_Val.Rows[0]["ItemCode"].ToString() + "','" + da_Val.Rows[0]["ItemName"].ToString() + "',";
                            SSQL = SSQL + " '" + da_Val.Rows[0]["QTY"].ToString() + "','" + da_Val.Rows[0]["Amount"].ToString() + "')";
                            da_chek_print = objdata.RptEmployeeMultipleDetails(SSQL);
                        }


                    }

                }
            }

            DataTable da_check_val = new DataTable();
            SSQL = "select * from Print_stock_details where Qty<>'0' order by ItemName asc ";
            da_check_val = objdata.RptEmployeeMultipleDetails(SSQL);
            if (da_check_val.Rows.Count != 0)
            {
                for (int j = 0; j < da_check_val.Rows.Count; j++)
                {
                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();
                    AutoDataTable.Rows[j]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDataTable.Rows[j]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDataTable.Rows[j]["CompanyCode"] = SessionCcode;
                    AutoDataTable.Rows[j]["LocationCode"] = SessionLcode;
                    AutoDataTable.Rows[j]["DeptName"] = da_check_val.Rows[j]["DeptName"];

                    AutoDataTable.Rows[j]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDataTable.Rows[j]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
                    AutoDataTable.Rows[j]["ItemCode"] = da_check_val.Rows[j]["ItemCode"]; ;
                    AutoDataTable.Rows[j]["ItemName"] = da_check_val.Rows[j]["ItemName"]; ;
                    AutoDataTable.Rows[j]["QTY"] = da_check_val.Rows[j]["QTY"];
                    AutoDataTable.Rows[j]["Amount"] = da_check_val.Rows[j]["Amount"];
                    // AutoDataTable.Rows[j]["Cost"] = da_check_val.Rows[j]["CostCenterName"];


                }
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDataTable);
                RD_Report.Load(Server.MapPath("~/crystal/Sample_StockDetailsStatement.rpt"));
                RD_Report.DataDefinition.FormulaFields["ReportHeading"].Text = "'" + Heading + "'";
                RD_Report.SetDataSource(ds1.Tables[0]);
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }



        }
        else
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }

    public void Sample_Stock_Issue_Details_Reports()
    {
        string Heading = "";
        DataTable StockDT = new DataTable();
        DataTable StockIssueDT = new DataTable();
        DataTable OldStockDT = new DataTable();

        AutoDataTable.Columns.Add("ItemName");
        AutoDataTable.Columns.Add("Old_Stock_Qty");
        AutoDataTable.Columns.Add("Old_Issue_Qty");
        AutoDataTable.Columns.Add("New_Stock_Qty");
        AutoDataTable.Columns.Add("New_Stock_Value");
        AutoDataTable.Columns.Add("New_Issue_Qty");
        AutoDataTable.Columns.Add("New_Issue_Value");

        SSQL = "delete from Print_Stock_Issue_details";
        objdata.RptEmployeeMultipleDetails(SSQL);

        // New Stock Details 
        SSQL = "select ItemCode,ItemName,SUM(Add_Qty) - SUM(Minus_Qty)  as QTY ,";
        SSQL = SSQL + " cast((SUM(Add_Value)-SUM(Minus_Value))/ 8  as decimal(16,2)) as Rate ";
        SSQL = SSQL + " ,SUM(Add_Value)-SUM(Minus_Value) as Amount  from dbo.Stock_Ledger_All ";
        SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";

        if (FromDate != "" && ToDate != "")
        {

            frmDate = Convert.ToDateTime(FromDate);
            toDate = Convert.ToDateTime(ToDate);

            string DateFormat = FromDate + "   ToDate :" + ToDate;
            SSQL = SSQL + " And CONVERT(varchar(20),Trans_Date_Str, 103)>=CONVERT(varchar(20),'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(varchar(20),Trans_Date_Str, 103)<=CONVERT(varchar(20),'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            Heading = "FromDate : " + DateFormat;
        }
        if (WareHouse != "-Select-")
        {
            SSQL = SSQL + " And WarehouseName='" + WareHouse + "'";
            Heading = "WarehouseName : " + WareHouse;
        }
        if (ItemName != "-Select-")
        {
            SSQL = SSQL + " And ItemName='" + ItemName + "'";
            Heading = "ItemName : " + ItemName;
        }

        SSQL = SSQL + " group by ItemCode,ItemName ";
        StockDT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (StockDT.Rows.Count != 0)
        {

            for (int i = 0; i < StockDT.Rows.Count; i++)
            {

                SSQL = "insert into Print_Stock_Issue_details(Ccode,Lcode,FinYearCode,FinYearVal,ItemCode,ItemName,New_Stock_Qty,";
                SSQL = SSQL + " New_Stock_Rate,New_Stock_Value,New_Issue_Qty,New_Issue_Value,Old_Stock_Qty,Old_Issue_Qty)";
                SSQL = SSQL + "  values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                SSQL = SSQL + " '" + StockDT.Rows[i]["ItemCode"].ToString() + "','" + StockDT.Rows[i]["ItemName"].ToString() + "',";
                SSQL = SSQL + " '" + StockDT.Rows[i]["QTY"].ToString() + "','" + StockDT.Rows[i]["Rate"].ToString() + "',";
                SSQL = SSQL + " '" + StockDT.Rows[i]["Amount"].ToString() + "','0.0','0.0','0.0','0.0')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

        }


        //Stock Issue Qty
        SSQL = "select ItemCode,ItemName,SUM(Minus_Qty) as Issus_Qty,SUM(Minus_Value) as  Rate from Stock_Ledger_All";
        SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
        if (FromDate != "" && ToDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
            toDate = Convert.ToDateTime(ToDate);

            SSQL = SSQL + " And CONVERT(varchar(20),Trans_Date_Str, 103)>=CONVERT(varchar(20),'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(varchar(20),Trans_Date_Str, 103)<=CONVERT(varchar(20),'" + toDate.ToString("dd/MM/yyyy") + "',103)";

        }
        if (WareHouse != "-Select-")
        {
            SSQL = SSQL + " And WarehouseName='" + WareHouse + "'";

        }
        if (ItemName != "-Select-")
        {
            SSQL = SSQL + " And ItemName='" + ItemName + "'";

        }

        SSQL = SSQL + " and Trans_Type='MATERIAL ISSUE'  group by ItemCode,ItemName ";
        StockIssueDT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (StockIssueDT.Rows.Count != 0)
        {

            for (int j = 0; j < StockIssueDT.Rows.Count; j++)
            {
                DataTable Check_StockIssueDT = new DataTable();
                SSQL = "select ItemCode,ItemName from Print_Stock_Issue_details where ItemName='" + StockIssueDT.Rows[j]["ItemName"].ToString() + "'";
                SSQL = SSQL + " and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " group by ItemCode,ItemName ";
                Check_StockIssueDT = objdata.RptEmployeeMultipleDetails(SSQL);

                if (StockIssueDT.Rows.Count != 0)
                {
                    SSQL = "UPDATE Print_Stock_Issue_details set New_Issue_Qty='" + StockIssueDT.Rows[j]["Issus_Qty"].ToString() + "',New_Issue_Value='" + StockIssueDT.Rows[j]["Rate"].ToString() + "'";
                    SSQL = SSQL + " where ItemCode='" + StockIssueDT.Rows[j]["ItemCode"].ToString() + "' and ItemName='" + StockIssueDT.Rows[j]["ItemName"].ToString() + "'";
                    SSQL = SSQL + " And Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
                else
                {
                    SSQL = "insert into Print_Stock_Issue_details(Ccode,Lcode,FinYearCode,FinYearVal,ItemCode,ItemName,New_Stock_Qty,";
                    SSQL = SSQL + " New_Stock_Rate,New_Stock_Value,New_Issue_Qty,New_Issue_Value,Old_Stock_Qty,Old_Issue_Qty)";
                    SSQL = SSQL + "  values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                    SSQL = SSQL + " '" + StockIssueDT.Rows[j]["ItemCode"].ToString() + "','" + StockIssueDT.Rows[j]["ItemName"].ToString() + "',";
                    SSQL = SSQL + " '0.0','0.0',0.0,'" + StockDT.Rows[j]["Issus_Qty"].ToString() + "',";
                    SSQL = SSQL + " '" + StockIssueDT.Rows[j]["Rate"].ToString() + "','0.0','0.0')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }
        }


        //OLD Stock
        SSQL = "select ItemCode,ItemName  ,SUM(Add_Qty) - SUM(Minus_Qty)  as Stock_QTY ,";
        SSQL = SSQL + "  SUM(Minus_Qty) as Issus_Qty from  Reuse_Stock_Ledger_All";
        SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And ";
        SSQL = SSQL + " FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
        if (FromDate != "" && ToDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
            toDate = Convert.ToDateTime(ToDate);

            SSQL = SSQL + " And CONVERT(varchar(20),Trans_Date_Str, 103)>=CONVERT(varchar(20),'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(varchar(20),Trans_Date_Str, 103)<=CONVERT(varchar(20),'" + toDate.ToString("dd/MM/yyyy") + "',103)";

        }
        if (WareHouse != "-Select-")
        {
            SSQL = SSQL + " And WarehouseName='" + WareHouse + "'";

        }
        if (ItemName != "-Select-")
        {
            SSQL = SSQL + " And ItemName='" + ItemName + "'";

        }

        SSQL = SSQL + " group by ItemCode,ItemName ";
        OldStockDT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (OldStockDT.Rows.Count != 0)
        {
            for (int k = 0; k < OldStockDT.Rows.Count; k++)
            {
                DataTable Check_OldStock = new DataTable();
                SSQL = "select ItemCode,ItemName from Print_Stock_Issue_details where ItemName='" + OldStockDT.Rows[k]["ItemName"].ToString() + "'";
                SSQL = SSQL + " and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                SSQL = SSQL + " group by ItemCode,ItemName ";
                Check_OldStock = objdata.RptEmployeeMultipleDetails(SSQL);


                if (Check_OldStock.Rows.Count != 0)
                {
                    SSQL = "UPDATE Print_Stock_Issue_details set Old_Stock_Qty='" + OldStockDT.Rows[k]["Stock_QTY"].ToString() + "',Old_Issue_Qty='" + OldStockDT.Rows[k]["Issus_Qty"].ToString() + "'";
                    SSQL = SSQL + " where ItemCode='" + Check_OldStock.Rows[0]["ItemCode"].ToString() + "' and ItemName='" + Check_OldStock.Rows[0]["ItemName"].ToString() + "'";
                    SSQL = SSQL + " And Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
                else
                {
                    SSQL = "insert into Print_Stock_Issue_details(Ccode,Lcode,FinYearCode,FinYearVal,ItemCode,ItemName,New_Stock_Qty,";
                    SSQL = SSQL + " New_Stock_Rate,New_Stock_Value,New_Issue_Qty,New_Issue_Value,Old_Stock_Qty,Old_Issue_Qty)";
                    SSQL = SSQL + "  values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
                    SSQL = SSQL + " '" + OldStockDT.Rows[k]["ItemCode"].ToString() + "','" + OldStockDT.Rows[k]["ItemName"].ToString() + "',";
                    SSQL = SSQL + " '0.0','0.0',0.0,'0.0','0.0',";
                    SSQL = SSQL + " '" + OldStockDT.Rows[k]["Stock_QTY"].ToString() + "','" + OldStockDT.Rows[k]["Issus_Qty"].ToString() + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }

            }

        }
        DataTable Print_Dt = new DataTable();
        SSQL = "select * from Print_Stock_Issue_details where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' ";
        SSQL = SSQL + "  And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
        Print_Dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (Print_Dt.Rows.Count != 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(Print_Dt);
            RD_Report.Load(Server.MapPath("~/crystal/Sample_Stock_Issue_Details1.rpt"));
            RD_Report.DataDefinition.FormulaFields["ReportHeading"].Text = "'" + Heading + "'";
            RD_Report.SetDataSource(ds1.Tables[0]);
            //Company Details Add
            DataTable dtdCompanyAddress = new DataTable();
            DataTable Close_Det = new DataTable();

            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void Stock_Details_Report()
    {
        DataTable StockDT = new DataTable();
        DataTable Department_DT = new DataTable();
        DataTable Item_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable Purchase_DT = new DataTable();
        DataTable Issue_DT = new DataTable();
        DataTable ZoneBin_DT = new DataTable();
        DataTable Item_UOM = new DataTable();

        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("Opening_Val");
        AutoDataTable.Columns.Add("PO_Val");
        AutoDataTable.Columns.Add("Issue_Val");


        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        string[] finyear = SessionFinYearVal.Split('_');
        string FromDate_Chk = "01/04/" + finyear[0];
        DateTime fromDate_Chk = Convert.ToDateTime(FromDate_Chk);


        string[] FromDate_ChkStr = FromDate.Split('/');


        if (FromDate == FromDate_Chk)
        {
            SSQL = "Delete Stock_Details_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select Distinct ItemCode from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' ";
            SSQL = SSQL + "And FinYearVal='" + SessionFinYearVal + "'";

            if (DeptName != "-Select-")
            {
                SSQL = SSQL + " And DeptName='" + DeptName + "'";
            }
            if (CostCenterName != "-Select-")
            {
                SSQL = SSQL + " And CostCenterName='" + CostCenterName + "'";
            }
            if (WareHouse != "-Select-")
            {
                SSQL = SSQL + " And WareHouseName='" + WareHouse + "'";
            }

            if (ItemName != "-Select-")
            {
                SSQL = SSQL + " And ItemName='" + ItemName + "'";
            }

            Item_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int i = 0; i < Item_DT.Rows.Count; i++)
            {
                SSQL = "Select Distinct DeptName from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' And ItemCode='" + Item_DT.Rows[i]["ItemCode"].ToString() + "'";
                Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < Department_DT.Rows.Count; j++)
                {

                    SSQL = "Select DeptName,sum(Open_Qty) as Open_Qty,sum(Open_Value) as Open_Value from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + "And ItemCode='" + Item_DT.Rows[i]["ItemCode"].ToString() + "' And DeptName='" + Department_DT.Rows[j]["DeptName"].ToString() + "'";
                    SSQL = SSQL + "And Trans_Type='OPENING RECEIPT' group by DeptName";
                    //SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";

                    Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "select DeptName,(sum(General_Qty)+sum(PO_Qty)) as PO_Qty,(sum(General_Value)+sum(PO_Value)) as PO_Val";
                    SSQL = SSQL + " from Stock_Transaction_Ledger";
                    SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + " And ItemCode='" + Item_DT.Rows[i]["ItemCode"].ToString() + "' And DeptName='" + Department_DT.Rows[j]["DeptName"].ToString() + "'";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103) group by DeptName";

                    Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "select DeptName,sum(Issue_Qty) as Issue_Qty,sum(Issue_Value) as Issue_Val";
                    SSQL = SSQL + " from Stock_Transaction_Ledger";
                    SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + " And ItemCode='" + Item_DT.Rows[i]["ItemCode"].ToString() + "' And DeptName='" + Department_DT.Rows[j]["DeptName"].ToString() + "'";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103) group by DeptName";

                    Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select ZoneName,BinName";
                    SSQL = SSQL + " from Stock_Transaction_Ledger";
                    SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + " And ItemCode='" + Item_DT.Rows[i]["ItemCode"].ToString() + "' And DeptName='" + Department_DT.Rows[j]["DeptName"].ToString() + "'";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";

                    ZoneBin_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select PurchaseUOM,ItemShortName";
                    SSQL = SSQL + " from MstItemMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ItemCode='" + Item_DT.Rows[i]["ItemCode"].ToString() + "'";
                    Item_UOM = objdata.RptEmployeeMultipleDetails(SSQL);



                    SSQL = "Insert Into Stock_Details_Print(Ccode,Lcode,FinYearCode,FinYearVal,ItemName,UOM,DeptName,Opening_Qty,Opening_Val,PO_Qty,PO_Val,";
                    SSQL = SSQL + "Issue_Qty,Issue_Val,From_Date,To_Date,ZoneName,BinName,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                    SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Item_UOM.Rows[0]["ItemShortName"].ToString() + "','" + Item_UOM.Rows[0]["PurchaseUOM"].ToString() + "','" + Department_DT.Rows[j]["DeptName"].ToString() + "',";
                    if (Open_Stck.Rows.Count != 0)
                    {
                        if (Open_Stck.Rows[0]["Open_Value"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Open_Qty"] + "','" + Open_Stck.Rows[0]["Open_Value"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + " '0.00','0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + " '0.00','0.00',";
                    }

                    if (Purchase_DT.Rows.Count != 0)
                    {
                        if (Purchase_DT.Rows[0]["PO_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["PO_Qty"] + "','" + Purchase_DT.Rows[0]["PO_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00','0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                    if (Issue_DT.Rows.Count != 0)
                    {
                        if (Issue_DT.Rows[0]["Issue_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Issue_Qty"] + "','" + Issue_DT.Rows[0]["Issue_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00','0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }


                    SSQL = SSQL + " '" + FromDate + "','" + ToDate + "',";
                    if (ZoneBin_DT.Rows.Count != 0)
                    {
                        SSQL = SSQL + "'" + ZoneBin_DT.Rows[0]["ZoneName"].ToString() + "','" + ZoneBin_DT.Rows[0]["BinName"].ToString() + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'','',";
                    }
                    SSQL = SSQL + "'" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }

            //Get Closing Stock
            SSQL = "Select * from Stock_Details_Print where";
            SSQL = SSQL + " Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And CONVERT(DATETIME,From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " order by DeptName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {

                //if (RptName == "Closing Stock Statement")
                //{

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(GenReceiptDT);
                RD_Report.Load(Server.MapPath("~/crystal/StockDetailsStatement.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
                //}

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }

        }
        else
        {

            SSQL = "Delete  Stock_Details_Print";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select Distinct ItemCode,ItemName from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And ";
            SSQL = SSQL + " FinYearVal ='" + SessionFinYearVal + "'";
            //SSQL = SSQL + " And ItemCode='AC-1'";

            if (DeptName != "-Select-")
            {
                SSQL = SSQL + " And DeptName='" + DeptName + "'";
            }
            if (CostCenterName != "-Select-")
            {
                SSQL = SSQL + " And CostCenterName='" + CostCenterName + "'";
            }
            if (WareHouse != "-Select-")
            {
                SSQL = SSQL + " And WareHouseName='" + WareHouse + "'";
            }

            if (ItemName != "-Select-")
            {
                SSQL = SSQL + " And ItemName='" + ItemName + "'";
            }

            Item_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            for (int i = 0; i < Item_DT.Rows.Count; i++)
            {
                string Stk_ItemName = "";
                string Stk_UOM;

                if (Item_DT.Rows[i]["ItemCode"].ToString() == "BL-51")
                {
                    SSQL = SSQL;
                }
                SSQL = "Select Distinct DeptName from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And ";
                SSQL = SSQL + " FinYearCode ='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' And ItemCode='" + Item_DT.Rows[i]["ItemCode"].ToString() + "'";

                if (DeptName != "-Select-")
                {
                    SSQL = SSQL + " And DeptName='" + DeptName + "'";
                }
                if (CostCenterName != "-Select-")
                {
                    SSQL = SSQL + " And CostCenterName='" + CostCenterName + "'";
                }
                if (WareHouse != "-Select-")
                {
                    SSQL = SSQL + " And WareHouseName='" + WareHouse + "'";
                }



                Department_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < Department_DT.Rows.Count; j++)
                {

                    SSQL = "select ((sum(Open_Qty)+sum(General_Qty)+sum(PO_Qty))-(sum(Issue_Qty))) as Open_Qty,((sum(Open_Value)+sum(General_Value)+sum(PO_Value))-(sum(Issue_Value))) as Open_Value from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + " And ItemCode='" + Item_DT.Rows[i]["ItemCode"].ToString() + "' And DeptName='" + Department_DT.Rows[j]["DeptName"].ToString() + "'";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + frmDate.AddDays(-1).ToString("dd/MM/yyyy") + "',103)";
                    }
                    else
                    {
                        SSQL = SSQL + "And Trans_Type='OPENING RECEIPT'";
                    }

                    if (DeptName != "-Select-")
                    {
                        SSQL = SSQL + " And DeptName='" + DeptName + "'";
                    }
                    if (CostCenterName != "-Select-")
                    {
                        SSQL = SSQL + " And CostCenterName='" + CostCenterName + "'";
                    }
                    if (WareHouse != "-Select-")
                    {
                        SSQL = SSQL + " And WareHouseName='" + WareHouse + "'";
                    }

                    Open_Stck = objdata.RptEmployeeMultipleDetails(SSQL);


                    SSQL = "select sum(General_Qty)+sum(PO_Qty) as PO_Qty,sum(General_Value)+sum(PO_Value) as PO_Val";
                    SSQL = SSQL + " from Stock_Transaction_Ledger";
                    SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + " And ItemCode='" + Item_DT.Rows[i]["ItemCode"].ToString() + "' And DeptName='" + Department_DT.Rows[j]["DeptName"].ToString() + "'";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    }
                    if (DeptName != "-Select-")
                    {
                        SSQL = SSQL + " And DeptName='" + DeptName + "'";
                    }
                    if (CostCenterName != "-Select-")
                    {
                        SSQL = SSQL + " And CostCenterName='" + CostCenterName + "'";
                    }
                    if (WareHouse != "-Select-")
                    {
                        SSQL = SSQL + " And WareHouseName='" + WareHouse + "'";
                    }

                    Purchase_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "select sum(Issue_Qty) as Issue_Qty,sum(Issue_Value) as Issue_Val";
                    SSQL = SSQL + " from Stock_Transaction_Ledger";
                    SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + " And ItemCode='" + Item_DT.Rows[i]["ItemCode"].ToString() + "' And DeptName='" + Department_DT.Rows[j]["DeptName"].ToString() + "'";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    }

                    if (DeptName != "-Select-")
                    {
                        SSQL = SSQL + " And DeptName='" + DeptName + "'";
                    }
                    if (CostCenterName != "-Select-")
                    {
                        SSQL = SSQL + " And CostCenterName='" + CostCenterName + "'";
                    }
                    if (WareHouse != "-Select-")
                    {
                        SSQL = SSQL + " And WareHouseName='" + WareHouse + "'";
                    }

                    Issue_DT = objdata.RptEmployeeMultipleDetails(SSQL);


                    SSQL = "Select ZoneName,BinName";
                    SSQL = SSQL + " from Stock_Transaction_Ledger";
                    SSQL = SSQL + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    SSQL = SSQL + " And ItemCode='" + Item_DT.Rows[i]["ItemCode"].ToString() + "' And DeptName='" + Department_DT.Rows[j]["DeptName"].ToString() + "'";
                    if (FromDate != "" && ToDate != "")
                    {
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                        SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date_Str, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
                    }

                    if (DeptName != "-Select-")
                    {
                        SSQL = SSQL + " And DeptName='" + DeptName + "'";
                    }
                    if (CostCenterName != "-Select-")
                    {
                        SSQL = SSQL + " And CostCenterName='" + CostCenterName + "'";
                    }
                    if (WareHouse != "-Select-")
                    {
                        SSQL = SSQL + " And WareHouseName='" + WareHouse + "'";
                    }

                    ZoneBin_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select PurchaseUOM,ItemShortName";
                    SSQL = SSQL + " from MstItemMaster where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And ItemCode='" + Item_DT.Rows[i]["ItemCode"].ToString() + "'";
                    //SSQL = SSQL + " ItemLongName = '" + Item_DT.Rows[i]["ItemName"].ToString() + "'";

                    if (DeptName != "-Select-")
                    {
                        SSQL = SSQL + " And DeptName='" + DeptName + "'";
                    }
                    if (CostCenterName != "-Select-")
                    {
                        SSQL = SSQL + " And CostCenterName='" + CostCenterName + "'";
                    }
                    //if (WareHouse != "-Select-")
                    //{
                    //    SSQL = SSQL + " And WareHouseName='" + WareHouse + "'";
                    //}

                    Item_UOM = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (Item_UOM.Rows.Count == 0)
                    {
                        Stk_ItemName = "";
                        Stk_UOM = "";
                    }
                    else
                    {
                        Stk_ItemName = Item_UOM.Rows[0]["ItemShortName"].ToString();
                        Stk_UOM = Item_UOM.Rows[0]["PurchaseUOM"].ToString();
                    }

                    SSQL = "Insert Into Stock_Details_Print(Ccode,Lcode,FinYearCode,FinYearVal,ItemName,UOM,DeptName,Opening_Qty,Opening_Val,PO_Qty,PO_Val,";
                    SSQL = SSQL + "Issue_Qty,Issue_Val,From_Date,To_Date,ZoneName,BinName,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "',";
                    SSQL = SSQL + " '" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Stk_ItemName + "','" + Stk_UOM + "','" + Department_DT.Rows[j]["DeptName"].ToString() + "',";
                    if (Open_Stck.Rows.Count != 0)
                    {
                        if (Open_Stck.Rows[0]["Open_Value"].ToString() != "")
                        {
                            SSQL = SSQL + " '" + Open_Stck.Rows[0]["Open_Qty"] + "','" + Open_Stck.Rows[0]["Open_Value"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00','0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + " '0.00','0.00',";
                    }

                    if (Purchase_DT.Rows.Count != 0)
                    {
                        if (Purchase_DT.Rows[0]["PO_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Purchase_DT.Rows[0]["PO_Qty"] + "','" + Purchase_DT.Rows[0]["PO_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00','0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                    if (Issue_DT.Rows.Count != 0)
                    {
                        if (Issue_DT.Rows[0]["Issue_Val"].ToString() != "")
                        {
                            SSQL = SSQL + "'" + Issue_DT.Rows[0]["Issue_Qty"] + "','" + Issue_DT.Rows[0]["Issue_Val"] + "',";
                        }
                        else
                        {
                            SSQL = SSQL + "'0.00','0.00',";
                        }
                    }
                    else
                    {
                        SSQL = SSQL + "'0.00','0.00',";
                    }
                    if (FromDate != "" && ToDate != "")
                    {

                        SSQL = SSQL + " '" + FromDate + "','" + ToDate + "',";
                    }
                    else
                    {
                        SSQL = SSQL + " '','',";
                    }
                    if (ZoneBin_DT.Rows.Count != 0)
                    {
                        SSQL = SSQL + "'" + ZoneBin_DT.Rows[0]["ZoneName"].ToString() + "','" + ZoneBin_DT.Rows[0]["BinName"].ToString() + "',";
                    }
                    else
                    {
                        SSQL = SSQL + "'','',";
                    }


                    SSQL = SSQL + "'" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }

            }
            
            //Get Closing Stock
            SSQL = "Select CMP.Cname,CMP.Address1,CMP.Address2 + '-' + CMP.Pincode[CmpAddr],CMP.ImagePath,SDP.Ccode,SDP.Lcode,SDP.FinYearCode,SDP.FinYearVal,SDP.ItemName,";
            SSQL = SSQL + "SDP.UOM,SDP.DeptName,SDP.Opening_Qty,SDP.Opening_Val,SDP.PO_Qty,SDP.PO_Val,SDP.Issue_Qty,SDP.Issue_Val,SDP.From_Date,SDP.To_Date,SDP.ZoneName,";
            SSQL = SSQL + "SDP.BinName,SDP.UserID,SDP.UserName from Stock_Details_Print SDP inner join AdminRights CMP on CMP.Ccode = SDP.Ccode ";
            SSQL = SSQL + "Where SDP.Ccode='" + SessionCcode + "' And SDP.Lcode='" + SessionLcode + "' And SDP.FinYearCode='" + SessionFinYearCode + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,SDP.From_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,SDP.To_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";
            }
            SSQL = SSQL + " order by SDP.DeptName";

            GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (GenReceiptDT.Rows.Count != 0)
            {

                DataSet ds1 = new DataSet();
                ds1.Tables.Add(GenReceiptDT);
                RD_Report.Load(Server.MapPath("~/crystal/StockDetailsStatement.rpt"));
                RD_Report.SetDataSource(ds1.Tables[0]);
                //Company Details Add
                DataTable dtdCompanyAddress = new DataTable();
                DataTable Close_Det = new DataTable();
                SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtdCompanyAddress.Rows.Count != 0)
                {
                    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                }


                RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = RD_Report;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();



            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }


        }

    }

    //ALL Supplier Monthly Payment Details Report()
    public void ALL_Supplier_Monthly_Payment_Details_Report_Load()
    {
        //Get All Supplier Monthly Payment Details
        DataTable SMP_DT = new DataTable();
        SSQL = "Select Trans_No,Trans_Date,Supp_Name,ChequeNo,ChequeDate,BankName,BranchName,TotalBalance from Unplanned_Receipt_Supp_Payment_Main where";
        SSQL = SSQL + "  Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)>=CONVERT(DATETIME,'" + FromDate + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + ToDate + "',103)";
        }
        SSQL = SSQL + " Order by CONVERT(DATETIME,Trans_Date,103) Asc";
        SMP_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (SMP_DT.Rows.Count != 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(SMP_DT);
            RD_Report.Load(Server.MapPath("~/crystal/Supplier_Monthly_Payment.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);
            //Company Details Add
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }
            if (FromDate != "" && ToDate != "")
            {
                RD_Report.DataDefinition.FormulaFields["Title"].Text = "'" + "SUPPLIER PAYMENT PAID AMOUNT DETAILS - FROM : " + FromDate + " TO : " + ToDate + "'";
            }
            else
            {
                RD_Report.DataDefinition.FormulaFields["Title"].Text = "'" + "SUPPLIER PAYMENT PAID AMOUNT DETAILS" + "'";
            }
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }


    //Single Supplier Ledger Report Details()
    public void Single_Supplier_Payment_Ledger_Details_Report_Load()
    {
        DataTable SLP_DT = new DataTable();
        string Vat_Per = "0";
        string Particulars = "";
        SSQL = "Delete from Unplanned_Receipt_Supp_Payment_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);

        //Insert Purchase Receipt Details
        SSQL = "insert into Unplanned_Receipt_Supp_Payment_Ledger(Ccode,Lcode,Trans_No,Trans_Date,Voucher_Type,Debit,Credit)";
        SSQL = SSQL + " Select Ccode,Lcode,UnPlan_Recp_No,UnPlan_Recp_Date,'IN PASS' as V_Type,NetAmount,'0.00' as Credit from Unplanned_Receipt_Main";
        SSQL = SSQL + " Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Supp_Code='" + Supplier_Code + "'";
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,UnPlan_Recp_Date, 103)>=CONVERT(DATETIME,'" + FromDate + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,UnPlan_Recp_Date, 103)<=CONVERT(DATETIME,'" + ToDate + "',103)";
        }
        objdata.RptEmployeeMultipleDetails(SSQL);

        //Find VAT Percentage
        DataTable Q_DT = new DataTable();
        SSQL = "Select * from Unplanned_Receipt_Supp_Payment_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SLP_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        for (int i = 0; i < SLP_DT.Rows.Count; i++)
        {
            //Get Vat Percentage
            SSQL = "Select * from Unplanned_Receipt_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And UnPlan_Recp_No='" + SLP_DT.Rows[i]["Trans_No"].ToString() + "' And TaxPer <> '0.00'";
            Q_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (Q_DT.Rows.Count != 0)
            {
                if (Q_DT.Rows[0]["TaxPer"].ToString() != "0.00")
                {
                    Vat_Per = Q_DT.Rows[0]["TaxPer"].ToString();
                    Particulars = "To IN Pass. Vat " + Vat_Per + "%";
                }
                else
                {
                    Vat_Per = "0.00";
                    Particulars = "To IN Pass";
                }
            }
            else
            {
                Vat_Per = "0.00";
                Particulars = "To IN Pass";
            }
            SSQL = "Update Unplanned_Receipt_Supp_Payment_Ledger set Vat_Per='" + Vat_Per + "',Particulars='" + Particulars + "'";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Trans_No='" + SLP_DT.Rows[i]["Trans_No"].ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
        }

        // Paid Amount Details Insert Ledger Table
        SSQL = "Insert Into Unplanned_Receipt_Supp_Payment_Ledger(Ccode,Lcode,Trans_No,Trans_Date,Particulars,Voucher_Type,Vat_Per,Debit,Credit)";
        SSQL = SSQL + " Select Ccode,Lcode,Trans_No,Trans_Date,'By Payment Receipt' as Partic,'Payment Advice' as V_Type,'0.00' as Vat_Per,'0.00' as Debit,TotalBalance";
        SSQL = SSQL + " from Unplanned_Receipt_Supp_Payment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Supp_Code='" + Supplier_Code + "'";
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)>=CONVERT(DATETIME,'" + FromDate + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,Trans_Date, 103)<=CONVERT(DATETIME,'" + ToDate + "',103)";
        }
        objdata.RptEmployeeMultipleDetails(SSQL);

        //Get Supplier Payment Ledger Report Details
        SSQL = "Select Trans_No,Trans_Date,Particulars,Voucher_Type,Debit,Credit from Unplanned_Receipt_Supp_Payment_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " Order by CONVERT(DATETIME,Trans_Date,103) Asc";
        SLP_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (SLP_DT.Rows.Count != 0)
        {
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(SLP_DT);
            RD_Report.Load(Server.MapPath("~/crystal/Supplier_Ledger_Report.rpt"));
            RD_Report.SetDataSource(ds1.Tables[0]);
            //Company Details Add
            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
            }
            if (FromDate != "" && ToDate != "")
            {
                RD_Report.DataDefinition.FormulaFields["Title"].Text = "'" + SupplierName + " - PAYMENT DETAILS - FROM : " + FromDate + " TO : " + ToDate + "'";
            }
            else
            {
                RD_Report.DataDefinition.FormulaFields["Title"].Text = "'" + SupplierName + " - PAYMENT DETAILS" + "'";
            }


            //Get Supplier Address
            string Supp_Add1 = "";
            string Supp_Add2 = "";
            string Supp_City = "";
            SSQL = "Select * From MstSupplier where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' And SuppCode='" + Supplier_Code + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtdCompanyAddress.Rows.Count != 0)
            {
                Supp_Add1 = dtdCompanyAddress.Rows[0]["Address1"].ToString();
                Supp_Add2 = dtdCompanyAddress.Rows[0]["Address2"].ToString();
                Supp_City = dtdCompanyAddress.Rows[0]["City"].ToString();
            }
            RD_Report.DataDefinition.FormulaFields["Supp_Add1"].Text = "'" + Supp_Add1 + "'";
            RD_Report.DataDefinition.FormulaFields["Supp_Add2"].Text = "'" + Supp_Add2 + "'";
            RD_Report.DataDefinition.FormulaFields["Supp_City"].Text = "'" + Supp_City + "'";
            RD_Report.DataDefinition.FormulaFields["Supp_Name"].Text = "'" + SupplierName + "'";


            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void Gate_Pass_In_New()
    {
        string SSQL = "";
        DataTable Dt = new DataTable();

        SSQL = "select CMP.Cname as[CMPNAME],GPIM.GP_IN_No as [GPINO],GPIM.GP_IN_Date as [GPIDATE],GPIM.GP_Out_No as [GPONO],GPIM.GP_Out_Date as [GPODATE],GPIM.InvNo as[INVNO],GPIM.InvDate as[INVDATE],GPIM.Supp_Name as [SUPPNAME],MSUP.Address1 as[SUPPADDR1],";
        SSQL = SSQL + "MSUP.Address2 as[SUPPADDR2],MSUP.City AS [SUPCITY],MSUP.Pincode AS [SUPPIN],MSUP.State AS[SUPSTATE],MSUP.MobileNo AS [SUPMOBI],MSUP.CstNo AS [SUPGST],GPIM.Others as [OTHERS],";
        SSQL = SSQL + "GPIM.DeptName as [DEPTNAME],GPIM.CostCenterName as [COSTCENTERNAME],GPIM.WarehouseName as [WAREHOUSENAME],GPIS.BinName as [BINNAME],";
        SSQL = SSQL + "GPIS.ZoneName as [ZONENAME],GPIS.Remarks as [REMARKS],GPIS.ItemName as [ITEMNAME],GPIS.UOMCode as [UOM],GPIS.OutQty as [OUTQTY],GPIS.INQty as [INQty],";
        SSQL = SSQL + "GPIS.Rate as [RATE],GPIS.Value as [VALUE],GPIS.PackingAmt as [PACKINGAMT],GPIS.DiscPer AS [DISCPER],GPIS.DiscAmt AS [DISCAMT],";
        SSQL = SSQL + "GPIS.CGSTPer as [CGSTPER],GPIS.CGSTAmt as[CGSTAmt],GPIS.SGSTPer as [SGSTPER],";
        SSQL = SSQL + "GPIS.SGSTAmt as[SGSTAmt],GPIS.IGSTPer as [IGSTPER],GPIS.IGSTAmt as[IGSTAmt],ISNULL(GPIS.TotAmt,0.0) as [ITEMTOTAL],";
        SSQL = SSQL + "GPIM.SubTotal as [SUBTOTAL],GPIM.PackingAmt as[TOTALPACKING],GPIM.OtherAmt as[OTHERAMT],GPIM.NetTotal as [NETAMT] ";
        SSQL = SSQL + "From GatePass_IN_Main GPIM Inner Join GATEPASS_IN_MAIN_SUB GPIS on GPIS.GP_IN_No=GPIM.GP_IN_No And GPIS.Ccode=GPIM.Ccode ";
        SSQL = SSQL + "Inner Join MstSupplier MSUP on MSUP.SuppCode=GPIM.Supp_Code And GPIS.Ccode=GPIM.Ccode And MSUP.Ccode=GPIM.Ccode ";
        SSQL = SSQL + "Inner Join AdminRights CMP on CMP.Ccode=GPIM.Ccode ";
        SSQL = SSQL + "Where GPIM.GP_IN_No = '" + GPINNo + "' and GPIM.Ccode = '" + SessionCcode + "' and GPIM.Lcode = '" + SessionLcode + "' and ";
        SSQL = SSQL + "GPIM.FinYearCode = '" + SessionFinYearCode + "'";

        Dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Dt.Rows.Count > 0)
        {
            DataTable DtTst = new DataTable();
            DataRow dr;

            DtTst = Dt;

            if (DtTst.Rows.Count < 4)
            {
                for (int i = DtTst.Rows.Count; i < 15; i++)
                {
                    dr = DtTst.NewRow();
                    foreach (DataColumn cols in DtTst.Columns)
                    {
                        if (DtTst.Columns[cols.ColumnName].DataType.ToString() == "System.String")
                        {

                            if (cols.ColumnName == "CGSTAmt")
                            {
                                if (cols.ColumnName != "")
                                {
                                    dr[cols.ColumnName] = "0";
                                }
                            }
                            else if (cols.ColumnName == "SGSTAmt")
                            {
                                if (cols.ColumnName != "")
                                {
                                    dr[cols.ColumnName] = "0";
                                }
                            }
                            else if (cols.ColumnName == "IGSTAmt")
                            {
                                if (cols.ColumnName != "")
                                {
                                    dr[cols.ColumnName] = "0";
                                }
                            }
                            else
                            {
                                dr[cols.ColumnName] = "";
                            }

                        }

                        if (DtTst.Columns[cols.ColumnName].DataType.ToString() == "System.Decimal")
                        {
                            if (cols.ColumnName == "TotAmt")
                            {
                                dr[cols.ColumnName] = Dt.Rows[0]["TotAmt"];
                            }
                            else if (cols.ColumnName == "SUBTOTAL")
                            {
                                dr[cols.ColumnName] = Dt.Rows[0]["SUBTOTAL"];
                            }
                            else if (cols.ColumnName == "TOTALPACKING")
                            {
                                dr[cols.ColumnName] = Dt.Rows[0]["TOTALPACKING"];
                            }
                            else if (cols.ColumnName == "OTHERAMT")
                            {
                                dr[cols.ColumnName] = Dt.Rows[0]["OTHERAMT"];
                            }
                            else if (cols.ColumnName == "NETAMT")
                            {
                                dr[cols.ColumnName] = Dt.Rows[0]["NETAMT"];
                            }
                            else
                            {
                                dr[cols.ColumnName] = 0;
                            }

                        }

                    }
                    DtTst.Rows.Add(dr);
                }
            }
            
            if (Dt.Rows[0]["CMPNAME"].ToString() == "Kumaran Gin & Pressing Private Limited")
            {
                RD_Report.Load(Server.MapPath("~/crystal/GatePass_In_New_Unit1.rpt"));
            }
            else
            {
                RD_Report.Load(Server.MapPath("~/crystal/GatePass_In_New _Unit2.rpt"));
            }
            RD_Report.SetDataSource(DtTst);
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void Service_Entry_Details()
    {
        string SSQL = "";
        DataTable Dt = new DataTable();

        SSQL = " Select CMP.Cname as[CMPNAME],SEM.SE_No as [GPINO],SEM.SE_Date as [GPIDATE],SEM.INVNo as [GPONO],SEM.INVDate as [GPODATE],";
        SSQL = SSQL + " SEM.InvNo as[INVNO],SEM.InvDate as[INVDATE],SEM.Supp_Name as [SUPPNAME],MSUP.Address1 as[SUPPADDR1],";
        SSQL = SSQL + " MSUP.Address2 as[SUPPADDR2],MSUP.City AS [SUPCITY],MSUP.Pincode AS [SUPPIN],MSUP.State AS[SUPSTATE],";
        SSQL = SSQL + " MSUP.MobileNo AS [SUPMOBI],MSUP.CstNo AS [SUPGST],SEM.Others as [OTHERS],SEM.DeptName as [DEPTNAME],";
        SSQL = SSQL + " SEM.ServicerName[BINNAME],SEM.MobileNo[ZONENAME],";
        SSQL = SSQL + " SES.Remarks as [REMARKS],SES.ItemName as [ITEMNAME],SES.UOM as [UOM],SES.Qty as [OUTQTY],";
        SSQL = SSQL + " SES.Rate as [RATE],SES.Value as [VALUE],SES.DiscPer AS [DISCPER],SES.DiscAmt AS [DISCAMT],";
        SSQL = SSQL + " SES.CGSTPer as [CGSTPER],SES.CGSTAmt as[CGSTAmt],SES.SGSTPer as [SGSTPER],SES.SGSTAmt as[SGSTAmt],";
        SSQL = SSQL + " SES.IGSTPer as [IGSTPER],SES.IGSTAmt as[IGSTAmt],ISNULL(SES.TotAmt,0.0) as [ITEMTOTAL],";
        SSQL = SSQL + " SEM.SubTotal as [SUBTOTAL],SEM.PackingAmt as[TOTALPACKING],SEM.OtherAmt as[OTHERAMT],";
        SSQL = SSQL + " SEM.NetTotal as [NETAMT] From Service_Entry_Main SEM ";
        SSQL = SSQL + " Inner Join Service_Entry_Main_Sub SES on SES.SE_No=SEM.SE_No And SES.Ccode=SEM.Ccode  ";
        SSQL = SSQL + " Inner Join MstSupplier MSUP on MSUP.SuppCode=SEM.Supp_Code And SES.Ccode=SEM.Ccode And MSUP.Ccode=SEM.Ccode ";
        SSQL = SSQL + " Inner Join AdminRights CMP on CMP.Ccode=SEM.Ccode ";
        SSQL = SSQL + " Where SEM.SE_No = '" + SENo + "' and SEM.Ccode = '" + SessionCcode + "' and SEM.Lcode = '" + SessionLcode + "' and ";
        SSQL = SSQL + " SEM.FinYearCode = '" + SessionFinYearCode + "'";

        Dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Dt.Rows.Count > 0)
        {
            DataTable DtTst = new DataTable();
            DataRow dr;

            DtTst = Dt;

            if (DtTst.Rows.Count < 4)
            {
                for (int i = DtTst.Rows.Count; i < 15; i++)
                {
                    dr = DtTst.NewRow();
                    foreach (DataColumn cols in DtTst.Columns)
                    {
                        if (DtTst.Columns[cols.ColumnName].DataType.ToString() == "System.String")
                        {

                            if (cols.ColumnName == "CGSTAmt")
                            {
                                if (cols.ColumnName != "")
                                {
                                    dr[cols.ColumnName] = "0";
                                }
                            }
                            else if (cols.ColumnName == "SGSTAmt")
                            {
                                if (cols.ColumnName != "")
                                {
                                    dr[cols.ColumnName] = "0";
                                }
                            }
                            else if (cols.ColumnName == "IGSTAmt")
                            {
                                if (cols.ColumnName != "")
                                {
                                    dr[cols.ColumnName] = "0";
                                }
                            }
                            else
                            {
                                dr[cols.ColumnName] = "";
                            }

                        }

                        if (DtTst.Columns[cols.ColumnName].DataType.ToString() == "System.Decimal")
                        {
                            if (cols.ColumnName == "TotAmt")
                            {
                                dr[cols.ColumnName] = Dt.Rows[0]["TotAmt"];
                            }
                            else if (cols.ColumnName == "SUBTOTAL")
                            {
                                dr[cols.ColumnName] = Dt.Rows[0]["SUBTOTAL"];
                            }
                            else if (cols.ColumnName == "TOTALPACKING")
                            {
                                dr[cols.ColumnName] = Dt.Rows[0]["TOTALPACKING"];
                            }
                            else if (cols.ColumnName == "OTHERAMT")
                            {
                                dr[cols.ColumnName] = Dt.Rows[0]["OTHERAMT"];
                            }
                            else if (cols.ColumnName == "NETAMT")
                            {
                                dr[cols.ColumnName] = Dt.Rows[0]["NETAMT"];
                            }
                            else
                            {
                                dr[cols.ColumnName] = 0;
                            }

                        }

                    }
                    DtTst.Rows.Add(dr);
                }
            }

            if (Dt.Rows[0]["CMPNAME"].ToString() == "Kumaran Gin & Pressing Private Limited")
            {
                RD_Report.Load(Server.MapPath("~/crystal/Service_Entry_Unit1.rpt"));
            }
            else
            {
                RD_Report.Load(Server.MapPath("~/crystal/Service_Entry_Unit2.rpt"));
            }
            RD_Report.SetDataSource(DtTst);
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }
    }

    public void GatePass_IN_Free_Service()
    {
        string SSQL = "";
        DataTable Dt = new DataTable();

        if (FromDate != "")
        {
            frmDate = Convert.ToDateTime(FromDate);
        }

        if (ToDate != "")
        {
            toDate = Convert.ToDateTime(ToDate);
        }

        SSQL = "select GIM.GP_IN_No[GPInNo],GIM.GP_IN_Date[GPInDate] ,GIM.GP_Out_No[GPOutNo],GIM.GP_Out_Date[GPOutDATE],GIM.StockType[StkType],GIM.Supp_Name[SupName],";
        SSQL = SSQL + "GIM.Others[Others],GIM.WarehouseName[WarehouseName],GIM.DeptName[DeptName],GIS.ItemCode[ItemCode],GIS.ItemName[ItemName],'Kg'[UOM],";
        SSQL = SSQL + "GIS.OutQty[OutQty],GIS.INQty[INQty],GIS.SCRABQTY[SCRABQTY],GIS.Rate[Rate],GIS.Value[BeforGst],GIS.PackingAmt[PackingAmt],GIS.DiscAmt[DiscAmt],GIS.CGSTPer[CGSTPer],";
        SSQL = SSQL + "GIS.CGSTAmt[CGSTAmt] ,GIS.SGSTPer[SGSTPer],GIS.SGSTAmt[SGSTAmt],GIS.IGSTPer[IGSTPer],GIS.IGSTAmt[IGSTAmt] ,GIS.TotAmt[TotAmt],";
        SSQL = SSQL + "GIS.ZoneName[ZoneName],GIS.BinName[BinName],'" + frmDate.ToString("dd/MM/yyyy") + "' [FDate],'" + toDate.ToString("dd/MM/yyyy") + "' [TDate] From GatePass_IN_Main GIM ";

        //SSQL = "select GIM.GP_IN_No,GIM.GP_IN_Date,GIM.GP_Out_No,GIM.GP_Out_Date,GIM.StockType,";
        //SSQL = SSQL + "GIM.Supp_Name,GIM.Supp_Det,GIM.Others,GIM.WarehouseName,GIS.ItemCode,GIS.ItemName,";
        //SSQL = SSQL + "GIS.UOMCode,GIS.OutQty,GIS.INQty,GIS.Value,GIS.ZoneName,GIS.BinName";
        SSQL = SSQL + "Inner join GatePass_IN_Main_Sub GIS ";
        SSQL = SSQL + "on GIM.GP_IN_No=GIS.GP_IN_No where CONVERT(DATETIME,GIM.GP_IN_Date, 103)>=CONVERT(DATETIME,'" + frmDate.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,GIM.GP_IN_Date, 103)<=CONVERT(DATETIME,'" + toDate.ToString("dd/MM/yyyy") + "',103)";


        if (WareHouse != "-Select-")
        {
            SSQL = SSQL + " And GIM.WarehouseName='" + WareHouse + "'";
        }

        if (SupplierName != "-Select-")
        {
            SSQL = SSQL + " And GIM.Supp_Name='" + SupplierName + "'";
        }

        if (ItemName != "-Select-")
        {
            SSQL = SSQL + " And GIS.ItemName='" + ItemName + "'";
        }

        if (DeptName != "-Select-")
        {
            SSQL = SSQL + " And GIM.DeptName='" + DeptName + "'";
        }

        if (GPINNo != "-Select-")
        {
            SSQL = SSQL + " And GIM.GP_IN_No='" + GPINNo + "'";
        }

        if (GPOutNo != "-Select-")
        {
            SSQL = SSQL + " And GIM.GP_Out_No='" + GPOutNo + "'";
        }


        SSQL = SSQL + " And GIM.Ccode='" + SessionCcode + "' And GIM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And GIM.FinYearCode='" + SessionFinYearCode + "' And GIS.Ccode='" + SessionCcode + "'";
        SSQL = SSQL + " And GIS.Lcode='" + SessionLcode + "' And GIS.FinYearCode='" + SessionFinYearCode + "' And GIM.ServiceType='1'";

        Dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Dt.Rows.Count > 0)
        {
            RD_Report.Load(Server.MapPath("~/crystal/GatePassINFreeServiceRpt.rpt"));
            RD_Report.SetDataSource(Dt);
            RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = RD_Report;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }
}
