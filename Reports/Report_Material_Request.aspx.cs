﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;



using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Reports_Report_Material_Request : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGatePassINNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string ButtonName = "";
    string MatReqNo = ""; string Perparedby = ""; string DeptName = "";
    string WareHouse = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Page.Title = "ERP Stores Module :: Material Request Report";

        if (!IsPostBack)
        {
            Load_Data_Empty_WareHouse();
            Load_Data_Empty_Dept();
            Load_Data_Empty_MatReqNo();
            Load_Data_Empty_Perparedby();
        }
        
        Load_Data_Empty_ItemCode();
        
    }
    protected void btnSlipReports_Click(object sender, EventArgs e)
    {
        RptName = "Material Request Report";
        ButtonName = "Request Slip";

        if (txtMatReqNo.SelectedItem.Text != "-Select-")
        {
            MatReqNo = txtMatReqNo.SelectedItem.Text;
        }
        else
        {
            MatReqNo = "";
        }
        if (txtPerparedby.SelectedItem.Text != "-Select-")
        {
            Perparedby = txtPerparedby.SelectedItem.Text;
        }
        else
        {
            Perparedby = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }



        ResponseHelper.Redirect("ReportDisplay.aspx?WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&MatReqNo=" + MatReqNo + "&MatPreBy=" + Perparedby + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
    }
    protected void btnDetReports_Click(object sender, EventArgs e)
    {
        RptName = "Material Request Report";
        ButtonName = "Request Details";

        if (txtMatReqNo.SelectedItem.Text != "-Select-")
        {
            MatReqNo = txtMatReqNo.SelectedItem.Text;
        }
        else
        {
            MatReqNo = "";
        }
        if (txtPerparedby.SelectedItem.Text != "-Select-")
        {
            Perparedby = txtPerparedby.SelectedItem.Text;
        }
        else
        {
            Perparedby = "";
        }
        if (txtDepartmentName.SelectedItem.Text != "-Select-")
        {
            DeptName = txtDepartmentName.SelectedItem.Text;
        }
        else
        {
            DeptName = "";
        }
        if (txtWarehouseName.SelectedItem.Text != "-Select-")
        {
            WareHouse = txtWarehouseName.SelectedItem.Text;
        }
        else
        {
            WareHouse = "";
        }


        ResponseHelper.Redirect("ReportDisplay.aspx?WareHouse=" + WareHouse + "&DeptName=" + DeptName + "&MatReqNo=" + MatReqNo + "&MatPreBy=" + Perparedby + "&ItemName=" + txtItemName.Text + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        
        txtWarehouseName.SelectedValue = "-Select-";
       // txtWarehouseCodeHide.Value = "";
        txtDepartmentName.SelectedValue = "-Select-";
        //txtDeptCodeHide.Value = "";
        txtMatReqNo.SelectedValue = "-Select-";
        //txtMatReqDateHide.Value = "";
        txtPerparedby.SelectedValue = "-Select-";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtItemCodeHide.Value = "";
        txtItemName.Text = "";

     }


    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        //modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();

    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtWarehouseName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }

    private void Load_Data_Empty_Dept()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }

    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnMatReqNo_Click(object sender, EventArgs e)
    {
        //modalPop_MatReqNo.Show();
    }

    private void Load_Data_Empty_MatReqNo()
    {
        
        string query = "";
        DataTable Main_DT = new DataTable();

        txtMatReqNo.Items.Clear();
        query = "Select Mat_Req_No,Mat_Req_Date from Meterial_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And Mat_Req_Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtMatReqNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Mat_Req_No"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtMatReqNo.DataTextField = "Mat_Req_No";

        txtMatReqNo.DataBind();

        
        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Mat_Req_No,Mat_Req_Date from Meterial_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And Mat_Req_Status='1'";
        //DT = objdata.RptEmployeeMultipleDetails(query);
      

        //Repeater_MatReqNo.DataSource = DT;
        //Repeater_MatReqNo.DataBind();
        //Repeater_MatReqNo.Visible = true;

    }

    protected void GridViewClick_MatReqNo(object sender, CommandEventArgs e)
    {
        txtMatReqNo.Text = Convert.ToString(e.CommandArgument);
        //txtMatReqDateHide.Value = Convert.ToString(e.CommandName);
    }

    protected void btnPerparedby_Click(object sender, EventArgs e)
    {
        //modalPop_Perparedby.Show();
    }

    private void Load_Data_Empty_Perparedby()
    {


        string query = "";
        DataTable Main_DT = new DataTable();

        txtPerparedby.Items.Clear();
        query = "Select Preparedby from Meterial_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And Mat_Req_Status='1'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtPerparedby.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["Preparedby"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtPerparedby.DataTextField = "Preparedby";

        txtPerparedby.DataBind();


        //string query = "";
        //DataTable DT = new DataTable();

        //query = "Select Preparedby from Meterial_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And Mat_Req_Status='1'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_Perparedby.DataSource = DT;
        //Repeater_Perparedby.DataBind();
        //Repeater_Perparedby.Visible = true;

    }

    protected void GridViewClick_Perparedby(object sender, CommandEventArgs e)
    {
        txtPerparedby.Text = Convert.ToString(e.CommandArgument);

    }
}
