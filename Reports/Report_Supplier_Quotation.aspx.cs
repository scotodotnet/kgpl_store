﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class Reports_Report_Supplier_Quotation : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionBlanketPOOrderNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SSQL;
    DateTime FromDate;
    DateTime ToDate;
    DataTable AutoDataTable = new DataTable();
    DataSet ds = new DataSet();
    string ReportName = "SUPPLIER QUOTATION REPORT";
    string SupQtnNo="";


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        
        Page.Title = "ERP Stores Module :: Report Supplier Quotation";
           
      

        Load_Data_Empty_ItemCode();
        Load_Data_Empty_Supp1();
        if (!IsPostBack)
        {
            Load_Data_Empty_SupQtnNo();
        }

    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        if (txtSPONo.SelectedItem.Text != "-Select-")
        {
            SupQtnNo = txtSPONo.SelectedItem.Text;
        }
        else
        {
            SupQtnNo = "";
        }

        ResponseHelper.Redirect("Supplier_Quotation_Crystalpage.aspx?QNumber=" + SupQtnNo + "&ReportName=" + ReportName.ToString() + "&SupplierName=" + txtSupplierName.Text + "&ItemName=" + txtItemName.Text + "&FDate=" + txtFromDate.Text + "&TDate=" + txtToDate.Text, "_blank", "");
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtItemCodeHide.Value = "";
        txtItemName.Text = "";
        //txtSPODateHide.Value = "";
        txtSPONo.SelectedValue = "-Select-";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtSuppCodehide.Value = "";
        txtSupplierName.Text = "";
    }


    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnSupp1_Click(object sender, EventArgs e)
    {
        modalPop_Supp1.Show();
    }
    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier1.DataSource = DT;
        Repeater_Supplier1.DataBind();
        Repeater_Supplier1.Visible = true;

    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
        txtSupplierName.Text = Convert.ToString(e.CommandName);
    }


    protected void btnSupQtnNo_Click(object sender, EventArgs e)
    {
        //modalPop_SupQtnNo.Show();
    }

    private void Load_Data_Empty_SupQtnNo()
    {

        string query = "";
        DataTable Main_DT = new DataTable();


        txtSPONo.Items.Clear();
        query = "Select QuotNo,QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
      
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtSPONo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["QuotNo"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtSPONo.DataTextField = "QuotNo";

        txtSPONo.DataBind();

        //string query = "";
        //DataTable DT = new DataTable();


        //query = "Select QuotNo,QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
      
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_SupQtnNo.DataSource = DT;
        //Repeater_SupQtnNo.DataBind();
        //Repeater_SupQtnNo.Visible = true;

    }

    protected void GridViewClick_SupQtnNo(object sender, CommandEventArgs e)
    {
        txtSPONo.Text = Convert.ToString(e.CommandArgument);
        //txtSPODateHide.Value = Convert.ToString(e.CommandName);


    }

}
