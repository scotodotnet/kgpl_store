﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Report_General_Receipt.aspx.cs" Inherits="Reports_Report_General_Receipt" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#WareHouse').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#WareHouse').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Supplier1').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Supplier1').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#UPRecNo').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#UPRecNo').dataTable();               
            }
        });
    };
</script>




<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable').dataTable();               
            }
        });
    };
</script>

<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
   $find('WareHouse_Close').hide();
   $find('Supp1_Close').hide();
   $find('Item_Close').hide();
    $find('UPRecNo_Close').hide();
      }
   } 
 </script>
<!--Ajax popup End-->




<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">General Receipt Details</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
    <div class="row">
        <div class="col-md-12">        
            <div class="col-md-9">
			    <div class="panel panel-white">
			        <div class="panel panel-primary">
				    <div class="panel-heading clearfix">
					    <h4 class="panel-title">General Receipt Details</h4>
				    </div>
				    </div>
				    <form class="form-horizontal">
				        <div class="panel-body">
					        <div class="col-md-12">
					            <div class="row">
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Warehouse</label>
					                    <asp:DropDownList ID="txtWarehouseName" runat="server" class="js-states form-control"></asp:DropDownList>
					                 </div>
                                    <div class="form-group col-md-4">
					                    <label for="exampleInputName">Department</label>
					                    <asp:DropDownList ID="txtDepartmentName" runat="server" class="js-states form-control"></asp:DropDownList>
					                </div>
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Supplier</label>
					                    <asp:DropDownList ID="ddlSupplier" runat="server" class="js-states form-control"></asp:DropDownList>
					                </div>
                                    <div class="col-md-12">
					                    <div class="row">
					                        <div class="form-group col-md-4">
					                            <label for="exampleInputName">Item Name</label>
					                            <asp:DropDownList ID="ddlItemName" runat="server" class="js-states form-control"></asp:DropDownList>
					                        </div>
					                        <div class="form-group col-md-2">
					                            <label for="exampleInputName">From Date</label>
					                            <asp:TextBox ID="txtFromDate" MaxLength="20" class="form-control date-picker"  AutoComplete="off" runat="server"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderFromDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtFromDate" ValidChars="0123456789./">
                                                </cc1:FilteredTextBoxExtender>
					                        </div>
					                        <div class="form-group col-md-2">
					                            <label for="exampleInputName">To Date</label>
					                            <asp:TextBox ID="txtToDate" class="form-control date-picker" runat="server"  AutoComplete="off" ></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderToDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtToDate" ValidChars="0123456789./">
                                                </cc1:FilteredTextBoxExtender>
					                        </div>
					                        <div class="form-group col-md-4">
					            <label for="exampleInputName">UnPlan Receipt No</label>
					            
					      <asp:DropDownList ID="txtUPRNo" runat="server" class="js-states form-control">
                          </asp:DropDownList>
					            
					            
					            <%--<asp:TextBox ID="txtUPRNo" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnUPRecNo" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnUPRecNo_Click"/>--%>
                    <%--<cc1:ModalPopupExtender ID="modalPop_UPRecNo"  runat="server" PopupControlID="Panel3" TargetControlID="btnUPRecNo"
                       CancelControlID="BtnClear_UPRecNo" BackgroundCssClass="modalBackground" BehaviorID="UPRecNo_Close">
                    </cc1:ModalPopupExtender>--%>
                   
                    <%--<asp:Panel ID="Panel3" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                           UnPlan Receipt No Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_UPRecNo" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="UPRecNo" class="display table">
                         <thead >
                         <tr>
                         <th>UnPlan_Recp_No</th>
                         <th>UnPlan_Recp_Date</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("UnPlan_Recp_No")%></td>
                          <td><%# Eval("UnPlan_Recp_Date")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditUPRecNo" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_UPRecNo" CommandArgument='<%# Eval("UnPlan_Recp_No")%>' CommandName='<%# Eval("UnPlan_Recp_Date")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_UPRecNo" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            <%--<asp:HiddenField ID="txtUPRDateHide" runat="server" />--%>
					        </div>
					                    </div>
					                </div>
					                <div class="clearfix"></div>
					                <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">TAX</label>
					            <asp:DropDownList ID="txtTaxPercent" Width="220" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					        </div>
					        <div class="form-group col-md-1">
					        </div> 
					          <div class="form-group col-md-4">
					            <label for="exampleInputName">Cost Center Name</label>
					            <asp:DropDownList ID="txtCostCenterName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					           
                                </div> 
					       
					    </div>
					</div>
					
					                <div class="clearfix"></div>
					                <div class="form-group row"></div>	
                                    <!-- Button start -->
                                    <div class="txtcenter">
                        <asp:Button ID="btnReport" class="btn-success"  runat="server" 
                            Text="DetailsReport" onclick="btnReport_Click" height="32" Width="120"/>
                            <asp:Button ID="btnSlip" class="btn-success"  runat="server" 
                            Text="SlipReport" onclick="btnSlip_Click" height="32" Width="120"/>
                        <asp:Button ID="btnItemWiseReport" class="btn-success"  runat="server" 
                            Text="Purchase Item" onclick="btnItemWiseReport_Click"  height="32" Width="120"/>
                            <asp:Button ID="btnDeptWiseReport" class="btn-success"  runat="server" 
                            Text="Department Wise" onclick="btnDeptWiseReport_Click"  height="32" Width="120"/>
                        <asp:Button ID="BtnTaxReport" class="btn-success" runat="server" 
                            Text="Tax Report" onclick="BtnTaxReport_Click"  height="32" Width="120"/>
                        <asp:Button ID="btnClear" class="btn-success" runat="server" 
                            Text="Clear" onclick="btnClear_Click"  height="32" Width="120"/>
                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group row"></div>
                                    <div class="txtcenter">
                      <asp:Button ID="BtnGSTReport" class="btn-success" runat="server" 
                            Text="GST Report"  height="32" Width="120" onclick="BtnGSTReport_Click"/>
                        <asp:Button ID="btnApp_Cancel" class="btn-success" runat="server"    
                        Text="Approval_Cancel"  height="32" Width="120" 
                            onclick="btnApp_Cancel_Click" />
                                <asp:Button ID="btnSamplereport" class="btn-success" runat="server"    
                        Text="SampleReport"  height="32" Width="120" onclick="btnSamplereport_Click" />

                                <asp:Button ID="btnCmbinRpt" class="btn-success" runat="server"    
                        Text="Combain Report"  height="32" Width="120" OnClick="btnCmbinRpt_Click"/>

                    </div>
                                    <div class="txtcenter"></div>
                                    <!-- Button end -->
                      
                     
                                                    
				                </div><!-- panel body end -->
                            </div>
                        </div>
				    </form>
			    </div><!-- panel white end -->
		    </div><!-- col-9 end -->
        </div><!-- col 12 end -->
  </div><!-- row end -->
</div><!-- main-wrapper end -->

</asp:Content>

