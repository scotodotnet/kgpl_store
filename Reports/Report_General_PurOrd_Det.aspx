﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Report_General_PurOrd_Det.aspx.cs" Inherits="Reports_Report_General_PurOrd_Det" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Dept').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Dept').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Supplier1').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Supplier1').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#GenPurOrdNo').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#GenPurOrdNo').dataTable();               
            }
        });
    };
</script>


<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
  
   $find('Dept_Close').hide();
   $find('Supp1_Close').hide();
   $find('GenPurOrdNo_Close').hide();
   $find('Item_Close').hide();
 
      }
   } 
 </script>
<!--Ajax popup End-->



 <div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">General Purchase Order Details</li></h4> 
    </ol>
</div>
    
 <div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">        
             <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">General Purchase Order Details</h4>
				</div>
				</div>
			<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Department</label>
					            
					            <asp:DropDownList ID="txtDepartmentName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					            
					            
					            <%--<asp:TextBox ID="txtDepartmentName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					              <asp:Button ID="btnDept" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnDept_Click"/>--%>
                    <%--<cc1:ModalPopupExtender ID="modalPop_Dept"  runat="server" PopupControlID="Panel1" TargetControlID="btnDept"
                       CancelControlID="BtnClear_Dept" BackgroundCssClass="modalBackground" BehaviorID="Dept_Close">
                    </cc1:ModalPopupExtender>--%>
                   
                    <%--<asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Department Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_Dept" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="Dept" class="display table">
                         <thead >
                         <tr>
                         <th>DeptCode</th>
                         <th>DeptName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("DeptCode")%></td>
                          <td><%# Eval("DeptName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_Dept" CommandArgument='<%# Eval("DeptCode")%>' CommandName='<%# Eval("DeptName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_Dept" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            <%--<asp:HiddenField ID="txtDeptCodeHide" runat="server" />--%>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Supplier</label>
                                <asp:DropDownList ID="ddlSupplier" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					            
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Gen.Pur_Order.No</label>
					            
					            <asp:DropDownList ID="txtGenPurOrdNo" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					            
					            
					           <%-- <asp:TextBox ID="txtGenPurOrdNo" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnGenPurOrdNo" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnGenPurOrdNo_Click"/>--%>
					            
					            <%--<cc1:ModalPopupExtender ID="modalPop_GenPurOrdNo"  runat="server" PopupControlID="Panel2" TargetControlID="btnGenPurOrdNo"
                       CancelControlID="BtnClear_GenPurOrdNo" BackgroundCssClass="modalBackground" BehaviorID="GenPurOrdNo_Close">
                    </cc1:ModalPopupExtender>--%>
                              
                                <%--<asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            General Purchase Order Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_GenPurOrdNo" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="GenPurOrdNo" class="display table">
                         <thead >
                         <tr>
                         <th>Gen_PO_No</th>
                         <th>Gen_PO_Date</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("Gen_PO_No")%></td>
                          <td><%# Eval("Gen_PO_Date")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditGenPurOrdNo" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_GenPurOrdNo" CommandArgument='<%# Eval("Gen_PO_No")%>' CommandName='<%# Eval("Gen_PO_Date")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_GenPurOrdNo" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            <%--<asp:HiddenField ID="txtGenPurOrdDateHide" runat="server" />--%>
					        </div>
					        </div>
					        </div>
					        
					        
					   <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Rate Type</label>
					            <asp:RadioButtonList ID="RdpPORateType" class="form-control" runat="server" 
                                    RepeatColumns="2">
                                    <asp:ListItem Value="1" Text="With Rate" style="padding-right:40px" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Without Rate" ></asp:ListItem>
                                </asp:RadioButtonList>
					            <asp:TextBox ID="txtSPONo" class="form-control" runat="server" Enabled="false" Visible="false"></asp:TextBox>
					            <asp:HiddenField ID="txtSPODateHide" runat="server" />
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Name</label>
					            <asp:DropDownList ID="ddlItemName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">From Date</label>
					            <asp:TextBox ID="txtFromDate" MaxLength="20" class="form-control date-picker" 
                                    runat="server" AutoComplete="off"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderFromDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtFromDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">To Date</label>
					            <asp:TextBox ID="txtToDate" class="form-control date-picker" runat="server" AutoComplete="off"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderToDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtToDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					    </div>
					</div>
					
					<div class="clearfix"></div>
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnReport" class="btn btn-success"  runat="server" 
                            Text="Report" onclick="btnReport_Click" />
                            <asp:Button ID="btnInvFormat" class="btn btn-success"  runat="server" 
                            Text="Invoice Format" onclick="btnInvFormat_Click" />
                        <asp:Button ID="btnClear" class="btn btn-success" runat="server" 
                            Text="Clear" onclick="btnClear_Click"  />
                    </div>
                    <!-- Button end -->
                      
                       <div class="clearfix"></div>
					<div class="form-group row"></div>	
					   <br />
					      <br />
                     <div class="col-md-12">
					    <div class="row">
					     <div class="form-group row">							 
						
					
						<label for="input-Default" class="col-sm-1 control-label">Status</label>
						<div class="col-sm-2">
                            <asp:DropDownList ID="txtRequestStatus" runat="server" class="js-states form-control" AutoPostBack="true">
                            
                                    <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Rejected List"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Pending List"></asp:ListItem>
                                    
                            </asp:DropDownList>
                            
						</div>
						
						<label for="input-Default" class="col-sm-1 control-label">FromDate</label>
						<div class="col-sm-2">
						<asp:TextBox ID="txtFrmDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtFrmDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
                         </div>
                         
                         <label for="input-Default" class="col-sm-1 control-label">ToDate</label>
						<div class="col-sm-2">
						<asp:TextBox ID="txtTDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
                         </div>
                      
						<div class="form-group col-md-1">
					         
					            <asp:Button ID="btnView" Width="50" Height="30" class="btn-success"  
                                    runat="server" Text="View" onclick="btnView_Click"/>
					        </div>
					        <div class="form-group col-md-1">
					         
					            <asp:Button ID="btn_Clear" Width="50" Height="30" class="btn-success"  
                                    runat="server" Text="Clear" onclick="btn_Clear_Click"/>
					        </div>
					        
                     </div>
                     
                     </div>
                     </div>
                                                    
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
             <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
	<div class="col-md-2"></div>
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->


</asp:Content>

