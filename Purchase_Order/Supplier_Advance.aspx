﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Supplier_Advance.aspx.cs" Inherits="Purchase_Order_Supplier_Advance" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Supplier1').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Supplier1').dataTable();               
            }
        });
    };
</script>








<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
    $find('Supp1_Close').hide();
     }
   } 
 </script>
<!--Ajax popup End-->


<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Supplier Advance Details</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Supplier Advance Details</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
						<div class="form-group col-md-4">
							<label for="exampleInputName">Trans. No<span class="mandatory">*</span></label>
							 <asp:Label ID="txtTrans_No" runat="server" class="form-control"></asp:Label>
							<%--<asp:TextBox ID="txtPurchaseReturnNo" class="form-control" runat="server"></asp:TextBox>--%>
						</div>
						
						<div class="form-group col-md-3">
                            <label for="exampleInputName">Trans. Date<span class="mandatory">*</span></label>
							<asp:TextBox ID="txtTransDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
				            <asp:RequiredFieldValidator ControlToValidate="txtTransDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                            </asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                TargetControlID="txtTransDate" ValidChars="0123456789./">
                            </cc1:FilteredTextBoxExtender>
						</div>
						<div class="form-group col-md-5">
							<label for="exampleInputName">SupplierName<span class="mandatory">*</span></label>
							<asp:TextBox ID="txtSupplierName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
							<asp:Button ID="btnSupp1" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnSupp1_Click"/>					            
				            <cc1:ModalPopupExtender ID="modalPop_Supp1"  runat="server" PopupControlID="Panl1" TargetControlID="btnSupp1"
                               CancelControlID="BtnClear" BackgroundCssClass="modalBackground" BehaviorID="Supp1_Close">
                            </cc1:ModalPopupExtender>
                    
                            <asp:Panel ID="Panl1" runat="server" CssClass="modalPopup" style="display:none" >
                                <div class="header">Supplier Details</div>
                                <div class="body">
				                    <div class="col-md-12 headsize">					
					                    <asp:Repeater ID="Repeater_Supplier1" runat="server" EnableViewState="false">
					                        <HeaderTemplate>
                                                 <table id="Supplier1" class="display table">
                                                 <thead >
                                                 <tr>
                                                 <th>SuppCode</th>
                                                 <th>SuppName</th>
                                                 <th>View</th>
                                                 </tr>
                                                 </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                              <tr>
                                              <td><%# Eval("SuppCode")%></td>
                                              <td><%# Eval("SuppName")%></td>
                                                                                               
                                              <td>
                                              <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridViewClick" CommandArgument='<%# Eval("SuppCode")%>' CommandName='<%# Eval("SuppName")%>'>
                                              </asp:LinkButton>
                                              </td>
                                              </tr>
                                            </ItemTemplate>
                                            <FooterTemplate></table></FooterTemplate>                                
					                    </asp:Repeater>
					                </div>
				                </div>
					            <div class="footer" align="right">
					                <asp:Button ID="BtnClear" class="btn btn-rounded" runat="server" Text="Close"/>
					            </div>
					        </asp:Panel>
					        <asp:HiddenField ID="txtSuppCodehide" runat="server" />
					        <asp:RequiredFieldValidator ControlToValidate="txtSupplierName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                            </asp:RequiredFieldValidator>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="row">
					    <div class="form-group col-md-3">
							<label for="exampleInputName">Cheque No<span class="mandatory">*</span></label>
							<asp:TextBox ID="txtChequeNo" class="form-control" runat="server"></asp:TextBox>
							<asp:RequiredFieldValidator ControlToValidate="txtChequeNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                            </asp:RequiredFieldValidator>
						</div>
						<div class="form-group col-md-3">
							<label for="exampleInputName">Cheque Date<span class="mandatory">*</span></label>
							<asp:TextBox ID="txtChequeDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
				            <asp:RequiredFieldValidator ControlToValidate="txtChequeDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                            </asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                TargetControlID="txtChequeDate" ValidChars="0123456789./">
                            </cc1:FilteredTextBoxExtender>
						</div>
						<div class="form-group col-md-3">
				            <label for="exampleInputName">Bank</label>
				            <asp:DropDownList ID="txtBankName" runat="server" class="js-states form-control">
                            </asp:DropDownList>
				        </div>
				        <div class="form-group col-md-3">
				            <label for="exampleInputName">Branch</label>
				            <asp:DropDownList ID="txtBranch" runat="server" class="js-states form-control">
                            </asp:DropDownList>
				        </div>
					</div>
				</div>	
				<div class="col-md-12">
					<div class="row">
						<div class="form-group col-md-12">
							<label for="exampleInputName">Description</label>
							<asp:TextBox ID="txtDescription" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
						</div>
					</div>
				</div>	
		
		        <!-- table start --> 
		        <div class="form-group row"></div>
		           
                    <!-- table end -->
                    <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-9"></div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Advance Amt</label>
					            <asp:TextBox ID="txtTotAmt" class="form-control" runat="server" Text="0.00"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTotAmt" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ControlToValidate="txtTotAmt" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                            </asp:RequiredFieldValidator>
					        </div>
					    </div>
					</div>   
                           
                           
					<!-- Button start -->
					<div class="form-group row"></div>
                    <div class="txtcenter">
                       <asp:Button ID="BtnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" 
                            onclick="BtnSave_Click"  />
                       <asp:Button ID="BtnCancel" class="btn btn-danger" runat="server" Text="Cancel" 
                            onclick="BtnCancel_Click" />
                       <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" 
                            Text="Back" onclick="btnBackEnquiry_Click" />
                    </div>
                    <!-- Button End -->
                                      
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    <!-- Dashboard start -->
		    <div class="col-lg-3 col-md-7">
                <div class="panel panel-white" style="height: 100%;">
                    <div class="panel-heading">
                        <h4 class="panel-title">DashBoard Details</h4>
                        <div class="panel-control"></div>
                    </div>
                    <div class="panel-body">
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-7">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                          
					    </div>
                    </div>                    
                </div>
            </div>  
                        
             
		    <!-- Dashboard End -->
		    <div class="col-md-2">
		    
		    
		    </div>
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 



</asp:Content>

