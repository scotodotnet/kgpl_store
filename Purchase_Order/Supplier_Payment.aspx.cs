﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Order_Supplier_Payment : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPaymentTransNo;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Supplier Payment Entry";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh_SuppBill();
            Initial_Data_Referesh_Item_Det();
            Initial_Data_Referesh_SuppAdvance();
            Load_Bank_Details();

            if (Session["Supp_payment_Trans_No"] == null)
            {
                SessionPaymentTransNo = "";
            }
            else
            {
                SessionPaymentTransNo = Session["Supp_payment_Trans_No"].ToString();
                txtTrans_No.Text = SessionPaymentTransNo;
                btnSearch_Click(sender, e);
            }
        }

        
        Load_OLD_data_Item_Det();
        Load_OLD_data_Supp_Bill();
        Load_Data_Empty_Supp1();
        Load_OLD_data_Supp_Advance();
        //Load_Data_Enquiry_Grid();
    }

    private void Load_Bank_Details()
    {
        DataTable DT_Bank = new DataTable();
        string query = "Select Distinct BankName from MstBank where Ccode='" + SessionCcode + "'";
        query = query + " And Lcode='" + SessionLcode + "'";
        DT_Bank = objdata.RptEmployeeMultipleDetails(query);
        txtBankName.Items.Clear();

        if (DT_Bank.Rows.Count != 0)
        {
            for (int i = 0; i < DT_Bank.Rows.Count; i++)
            {
                txtBankName.Items.Add(DT_Bank.Rows[i]["BankName"].ToString());
            }
        }

        query = "Select Distinct BranchName from MstBank where Ccode='" + SessionCcode + "'";
        query = query + " And Lcode='" + SessionLcode + "'";
        DT_Bank = objdata.RptEmployeeMultipleDetails(query);
        txtBranch.Items.Clear();

        if (DT_Bank.Rows.Count != 0)
        {
            for (int i = 0; i < DT_Bank.Rows.Count; i++)
            {
                txtBranch.Items.Add(DT_Bank.Rows[i]["BranchName"].ToString());
            }
        }

    }

    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier1.DataSource = DT;
        Repeater_Supplier1.DataBind();
        Repeater_Supplier1.Visible = true;

    }

    protected void btnSupp1_Click(object sender, EventArgs e)
    {
        modalPop_Supp1.Show();
    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {

        txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
        txtSupplierName.Text = Convert.ToString(e.CommandName);
        Load_Data_Supplier_Bill_Det();
        Load_Data_Supplier_Advance_Det();
    }

    private void Load_Data_Supplier_Bill_Det()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("InvDate");
        DT.Columns.Add("InvNo");
        DT.Columns.Add("TotalAmt");

        query = "Select InvDate,InvNo,TotalAmt,UnPlan_Recp_No from Unplanned_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "'";
        query = query + " And Supp_Code='" + txtSuppCodehide.Value + "' And Supp_Name='" + txtSupplierName.Text + "'";
        query = query + " And UnPlan_PO_Receipt_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        DataTable dt_Bill = new DataTable();
        DataTable dt_dummy = new DataTable();
        DataRow dr = null;
        dt_Bill.Columns.Add(new DataColumn("InvDate", typeof(string)));
        dt_Bill.Columns.Add(new DataColumn("InvNo", typeof(string)));
        dt_Bill.Columns.Add(new DataColumn("TotalAmt", typeof(string)));
        dt_Bill.Columns.Add(new DataColumn("UnPlan_Recp_No", typeof(string)));

        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                query = "Select * from Unplanned_Receipt_Supp_Payment_Main_Sub where Ccode='" + SessionCcode + "'";
                query = query + " And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "'";
                query = query + " And Supp_Code='" + txtSuppCodehide.Value + "' And Supp_Name='" + txtSupplierName.Text + "'";
                query = query + " And InvNo='" + DT.Rows[i]["InvNo"].ToString() + "' And UnPlan_Recp_No='" + DT.Rows[i]["UnPlan_Recp_No"].ToString() + "'";
                dt_dummy = objdata.RptEmployeeMultipleDetails(query);
                if (dt_dummy.Rows.Count == 0)
                {
                    dr = dt_Bill.NewRow();
                    dr["InvDate"] = DT.Rows[i]["InvDate"].ToString();
                    dr["InvNo"] = DT.Rows[i]["InvNo"].ToString();
                    dr["TotalAmt"] = DT.Rows[i]["TotalAmt"].ToString();
                    dr["UnPlan_Recp_No"] = DT.Rows[i]["UnPlan_Recp_No"].ToString();
                    dt_Bill.Rows.Add(dr);
                }
                else
                {
                    query = "Select * from Unplanned_Receipt_Supp_Payment_Main_Sub where Ccode='" + SessionCcode + "'";
                    query = query + " And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    query = query + " And Supp_Code='" + txtSuppCodehide.Value + "' And Supp_Name='" + txtSupplierName.Text + "'";
                    query = query + " And InvNo='" + DT.Rows[i]["InvNo"].ToString() + "' And UnPlan_Recp_No='" + DT.Rows[i]["UnPlan_Recp_No"].ToString() + "'";
                    dt_dummy = objdata.RptEmployeeMultipleDetails(query);
                    if (dt_dummy.Rows[dt_dummy.Rows.Count - 1]["CreditAmt"].ToString() != "0.00")
                    {
                        dr = dt_Bill.NewRow();
                        dr["InvDate"] = DT.Rows[i]["InvDate"].ToString();
                        dr["InvNo"] = DT.Rows[i]["InvNo"].ToString();
                        dr["TotalAmt"] = dt_dummy.Rows[dt_dummy.Rows.Count - 1]["CreditAmt"].ToString();
                        dr["UnPlan_Recp_No"] = DT.Rows[i]["UnPlan_Recp_No"].ToString();
                        dt_Bill.Rows.Add(dr);
                    }
                }
            }
        }

        Repeater2.DataSource = dt_Bill;
        Repeater2.DataBind();
        ViewState["SuppBillTable"] = Repeater2.DataSource;

    }


    private void Load_Data_Supplier_Advance_Det()
    {

        string query = "";
        DataTable DT = new DataTable();
        

        DT.Columns.Add("InvDate");
        DT.Columns.Add("InvNo");
        DT.Columns.Add("TotalAmt");

        query = "Select Trans_No,Trans_Date,AdvAmount from Unplanned_Receipt_Supp_Advance_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "'";
        query = query + " And Supp_Code='" + txtSuppCodehide.Value + "' And Supp_Name='" + txtSupplierName.Text + "'";
       
        DT = objdata.RptEmployeeMultipleDetails(query);

        DataTable dt_Bill = new DataTable();
        DataTable dt_dummy = new DataTable();
        DataRow dr = null;
        dt_Bill.Columns.Add(new DataColumn("Trans_Date", typeof(string)));
        dt_Bill.Columns.Add(new DataColumn("Trans_No", typeof(string)));
        dt_Bill.Columns.Add(new DataColumn("AdvAmount", typeof(string)));
       
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                query = "Select * from Unplanned_Receipt_Supp_Payment_Main_Sub where Ccode='" + SessionCcode + "'";
                query = query + " And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "'";
                query = query + " And Supp_Code='" + txtSuppCodehide.Value + "' And Supp_Name='" + txtSupplierName.Text + "'";
                query = query + " And AdvNo='" + DT.Rows[i]["Trans_No"].ToString() + "'";
                dt_dummy = objdata.RptEmployeeMultipleDetails(query);
                if (dt_dummy.Rows.Count == 0)
                {
                    dr = dt_Bill.NewRow();
                    dr["Trans_Date"] = DT.Rows[i]["Trans_Date"].ToString();
                    dr["Trans_No"] = DT.Rows[i]["Trans_No"].ToString();
                    dr["AdvAmount"] = DT.Rows[i]["AdvAmount"].ToString();
                  
                    dt_Bill.Rows.Add(dr);
                }
            }
        }

        Repeater3.DataSource = dt_Bill;
        Repeater3.DataBind();
        ViewState["SuppAdvanceTable"] = Repeater3.DataSource;

    }


    protected void GridViewSuppBill(object sender, CommandEventArgs e)
    {
        string Receipt_No = "";
        string query = "";
        DataTable DT = new DataTable();
        DataTable dt_dummy = new DataTable();
        Receipt_No = Convert.ToString(e.CommandArgument);
        //Get Amount Details
        query = "Select InvDate,InvNo,NetAmount as TotalAmt,UnPlan_Recp_No from Unplanned_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "'";
        query = query + " And UnPlan_Recp_No='" + Receipt_No + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                query = "Select * from Unplanned_Receipt_Supp_Payment_Main_Sub where Ccode='" + SessionCcode + "'";
                query = query + " And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "'";
                query = query + " And Supp_Code='" + txtSuppCodehide.Value + "' And Supp_Name='" + txtSupplierName.Text + "'";
                query = query + " And InvNo='" + DT.Rows[i]["InvNo"].ToString() + "' And UnPlan_Recp_No='" + DT.Rows[i]["UnPlan_Recp_No"].ToString() + "' And CreditAmt!='0.00'";
                dt_dummy = objdata.RptEmployeeMultipleDetails(query);
                if (dt_dummy.Rows.Count == 0)
                {

                    txtInvDate.Text = DT.Rows[0]["InvDate"].ToString();
                    txtInvNo.Text = DT.Rows[0]["InvNo"].ToString();
                    txtBillAmount.Text = DT.Rows[0]["TotalAmt"].ToString();
                    txtNetPayBillAmt.Text = ((Convert.ToDecimal(txtBillAmount.Text)) - (Convert.ToDecimal(txtAdvanceAmount.Text))).ToString();

                    txtReceiptNo.Value = Receipt_No;
                }
                else
                {
                    query = "Select * from Unplanned_Receipt_Supp_Payment_Main_Sub where Ccode='" + SessionCcode + "'";
                    query = query + " And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "'";
                    query = query + " And Supp_Code='" + txtSuppCodehide.Value + "' And Supp_Name='" + txtSupplierName.Text + "'";
                    query = query + " And InvNo='" + DT.Rows[i]["InvNo"].ToString() + "' And UnPlan_Recp_No='" + DT.Rows[i]["UnPlan_Recp_No"].ToString() + "'";
                    dt_dummy = objdata.RptEmployeeMultipleDetails(query);
                    if (dt_dummy.Rows[dt_dummy.Rows.Count - 1]["CreditAmt"].ToString() != "0.00")
                    {
                        txtInvDate.Text = DT.Rows[0]["InvDate"].ToString();
                        txtInvNo.Text = DT.Rows[0]["InvNo"].ToString();
                        txtBillAmount.Text = dt_dummy.Rows[dt_dummy.Rows.Count - 1]["CreditAmt"].ToString();
                        txtNetPayBillAmt.Text = ((Convert.ToDecimal(txtBillAmount.Text)) - (Convert.ToDecimal(txtAdvanceAmount.Text))).ToString();

                        txtReceiptNo.Value = Receipt_No;
                    }
                }
            }
        }
        else
        {
            txtInvDate.Text = "";
            txtInvNo.Text = "";
            txtBillAmount.Text = "0.00";
            txtReceiptNo.Value = "";
            if (txtBillAmount.Text != "0.00")
            {
                txtNetPayBillAmt.Text = ((Convert.ToDecimal(txtBillAmount.Text)) - (Convert.ToDecimal(txtAdvanceAmount.Text))).ToString();
            }
        }

    }

    protected void GridViewSuppAdvance(object sender, CommandEventArgs e)
    {
        string Receipt_No = "";
        string query = "";
        DataTable DT = new DataTable();
        Receipt_No = Convert.ToString(e.CommandArgument);
        //Get Amount Details
        query = "Select Trans_No,Trans_Date,AdvAmount from Unplanned_Receipt_Supp_Advance_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "'";
        query = query + " And Trans_No='" + Receipt_No + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtAdvNo.Value = DT.Rows[0]["Trans_No"].ToString();
            txtAdvDate.Value = DT.Rows[0]["Trans_Date"].ToString();
            txtAdvanceAmount.Text= DT.Rows[0]["AdvAmount"].ToString();
            txtNetPayBillAmt.Text = ((Convert.ToDecimal(txtBillAmount.Text)) - (Convert.ToDecimal(txtAdvanceAmount.Text))).ToString();
    
            
        }
        else
        {
            txtAdvNo.Value = "";
            txtAdvDate.Value = "";
            txtAdvanceAmount.Text = "0.00";
            txtNetPayBillAmt.Text = ((Convert.ToDecimal(txtBillAmount.Text)) - (Convert.ToDecimal(txtAdvanceAmount.Text))).ToString();
    
            
        }

    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Bill Details..');", true);
        }

        //Check Supplier Name
        query = "Select * from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And SuppCode='" + txtSuppCodehide.Value + "' And SuppName='" + txtSupplierName.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Supplier Name...');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (BtnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "2", "Supplier Payments");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Supplier Payment Entry...');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "2", "Supplier Payments");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Supplier Payment Entry...');", true);
            }
        }

        //User Rights Check End

        //Auto generate Transaction Function Call

        if (BtnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Supplier Payments", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtTrans_No.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            query = "Select * from Unplanned_Receipt_Supp_Payment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTrans_No.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Unplanned_Receipt_Supp_Payment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTrans_No.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Unplanned_Receipt_Supp_Payment_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTrans_No.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }


            //Response.Write(strValue);   	

            //Insert Main Table  

            query = "Insert Into Unplanned_Receipt_Supp_Payment_Main(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Supp_Code,Supp_Name,";
            query = query + " ChequeNo,ChequeDate,BankName,BranchName,Description,NetAmount,TotalCredit,TotalAdvance,TotalDiscount,TotalAddorLess,TotalBalance,UserID,UserName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTrans_No.Text + "','" + txtTransDate.Text + "','" + txtSuppCodehide.Value + "','" + txtSupplierName.Text + "',";
            query = query + " '" + txtChequeNo.Text + "','" + txtChequeDate.Text + "','" + txtBankName.Text + "','" + txtBranch.Text + "',";
            query = query + " '" + txtDescription.Text + "','" + txtTotAmt.Text + "','" + txtTotalCredit.Text + "','" + txtTotAdvance.Text + "','" + txtTotalDiscount.Value + "','" + txtTotAddLess.Text + "','" + txtTotBalAmt.Text + "',";
            query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Unplanned_Receipt_Supp_Payment_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Supp_Code,Supp_Name,UnPlan_Recp_No,InvNo,InvDate,TotalAmt,CreditAmt,AdvNo,AdvDate,AdvAmt,BalAmt,Discount,AddOrLess,NetBalAmt,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTrans_No.Text + "','" + txtTransDate.Text + "','" + txtSuppCodehide.Value + "','" + txtSupplierName.Text + "','" + dt.Rows[i]["UnPlan_Recp_No"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["InvNo"].ToString() + "','" + dt.Rows[i]["InvDate"].ToString() + "','" + dt.Rows[i]["TotalAmt"].ToString() + "','" + dt.Rows[i]["CreditAmt"].ToString() + "','" + dt.Rows[i]["AdvNo"].ToString() + "','" + dt.Rows[i]["AdvDate"].ToString() + "','" + dt.Rows[i]["AdvAmt"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["BalAmt"].ToString() + "','" + dt.Rows[i]["Discount"].ToString() + "','" + dt.Rows[i]["AddOrLess"].ToString() + "','" + dt.Rows[i]["NetBalAmt"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Payment Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Payment Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Supp_payment_Trans_No"] = txtTrans_No.Text;
            BtnSave.Text = "Update";

            //Load_Data_Enquiry_Grid();
        }
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (txtInvDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Bill Date...');", true);
        }
        if (txtInvNo.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Bill No...');", true);
        }

        if (txtBillAmount.Text == "" || txtBillAmount.Text == "0.00")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Bill Amount...');", true);
        }

        

        //Check Biil No
        query = "Select * from Unplanned_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearVal='" + SessionFinYearVal + "'";
        query = query + " And Supp_Code='" + txtSuppCodehide.Value + "' And Supp_Name='" + txtSupplierName.Text + "'";
        query = query + " And UnPlan_Recp_No='" + txtReceiptNo.Value + "' And InvNo='" + txtInvNo.Text + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Bill Details Correctly...');", true);
        }

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["InvNo"].ToString().ToUpper() == txtInvNo.Text.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Bill No Already Added..');", true);
                    }
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (txtAdvNo.Value.ToString().ToUpper() != "")
                    {
                        if (dt.Rows[i]["AdvNo"].ToString().ToUpper() == txtAdvNo.Value.ToString().ToUpper())
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Advance No Already Added..');", true);
                        }
                    }
                }



                if (!ErrFlag)
                {
                    dr = dt.NewRow();

                    dr["InvDate"] = txtInvDate.Text;
                    dr["InvNo"] = txtInvNo.Text;
                    dr["TotalAmt"] = txtBillAmount.Text;
                    dr["UnPlan_Recp_No"] = txtReceiptNo.Value;
                    dr["AdvDate"] = txtAdvDate.Value;
                    dr["AdvNo"] = txtAdvNo.Value;
                    dr["AdvAmt"] = txtAdvanceAmount.Text;
                    dr["CreditAmt"] = txtCreditNote.Text;
                    dr["Discount"] = txtDiscount.Text;
                    dr["AddOrLess"] = txtAddOrLess.Text;

                    txtBalanceAmt.Value = (Convert.ToDecimal(txtBillAmount.Text) - ((Convert.ToDecimal(txtDiscount.Text)) + (Convert.ToDecimal(txtCreditNote.Text)))).ToString();
                   
                    dr["BalAmt"] = txtBalanceAmt.Value;
                    dr["NetBalAmt"] = txtNetPayBillAmt.Text;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    Totalsum();
                    txtInvDate.Text = ""; txtInvNo.Text = "";
                    txtBillAmount.Text = "0.00"; txtReceiptNo.Value = ""; txtAdvanceAmount.Text = "0.00";
                    txtAdvDate.Value = ""; txtAdvNo.Value = ""; txtBalanceAmt.Value = "0.00"; txtDiscount.Text = "0";
                    txtAddOrLess.Text = "0"; txtNetPayBillAmt.Text = "0"; txtCreditNote.Text = "0.00";
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["InvDate"] = txtInvDate.Text;
                dr["InvNo"] = txtInvNo.Text;
                dr["TotalAmt"] = txtBillAmount.Text;
                dr["UnPlan_Recp_No"] = txtReceiptNo.Value;
                dr["AdvDate"] = txtAdvDate.Value;
                dr["AdvNo"] = txtAdvNo.Value;
                dr["AdvAmt"] = txtAdvanceAmount.Text;
                dr["CreditAmt"] = txtCreditNote.Text;
                dr["Discount"] = txtDiscount.Text;
                dr["AddOrLess"] = txtAddOrLess.Text;

                txtBalanceAmt.Value = (Convert.ToDecimal(txtBillAmount.Text) - ((Convert.ToDecimal(txtDiscount.Text)) + (Convert.ToDecimal(txtCreditNote.Text)))).ToString();
                 
                dr["BalAmt"] = txtBalanceAmt.Value;
                dr["NetBalAmt"] = txtNetPayBillAmt.Text;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                Totalsum();
                txtInvDate.Text = ""; txtInvNo.Text = "";
                txtBillAmount.Text = "0.00"; txtReceiptNo.Value = ""; txtAdvanceAmount.Text = "0.00";
                txtAdvDate.Value = ""; txtAdvNo.Value = ""; txtBalanceAmt.Value = "0.00"; txtDiscount.Text = "0";
                txtAddOrLess.Text = "0"; txtNetPayBillAmt.Text = "0"; txtCreditNote.Text = "0.00";
            }
        }
    }

    public void Totalsum()
    {

        string Total_BillAmount = "0.00";
        string Total_AdvanceAmount = "0.00";
        string Total_Credit = "0.00";
        string Total_AddLess = "0.00";
        string Total_NetPay = "0.00";
        string Total_Discount = "0.00";
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            Total_BillAmount = (Convert.ToDecimal(Total_BillAmount) + Convert.ToDecimal(dt.Rows[i]["TotalAmt"])).ToString();
            Total_AdvanceAmount = (Convert.ToDecimal(Total_AdvanceAmount) + Convert.ToDecimal(dt.Rows[i]["AdvAmt"])).ToString();
            Total_Credit = (Convert.ToDecimal(Total_Credit) + Convert.ToDecimal(dt.Rows[i]["CreditAmt"])).ToString();
            Total_AddLess = (Convert.ToDecimal(Total_AddLess) + Convert.ToDecimal(dt.Rows[i]["AddOrLess"])).ToString();
            Total_Discount = (Convert.ToDecimal(Total_Discount) + Convert.ToDecimal(dt.Rows[i]["Discount"])).ToString();
            Total_NetPay = (Convert.ToDecimal(Total_NetPay) + Convert.ToDecimal(dt.Rows[i]["NetBalAmt"])).ToString();
        }
        txtTotAmt.Text = Total_BillAmount;
        txtTotalCredit.Text = Total_Credit;
        txtTotAdvance.Text = Total_AdvanceAmount;
        txtTotAddLess.Text = Total_AddLess;
        txtTotalDiscount.Value = Total_Discount;
        txtTotBalAmt.Text = Total_NetPay;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("Supplier_Payment_Main.aspx");
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["InvNo"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data_Item_Det();
        Totalsum();

    }

    private void Load_OLD_data_Supp_Bill()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["SuppBillTable"];
        Repeater2.DataSource = dt;
        Repeater2.DataBind();
    }
    private void Load_OLD_data_Supp_Advance()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["SuppAdvanceTable"];
        Repeater3.DataSource = dt;
        Repeater3.DataBind();
    }
    private void Initial_Data_Referesh_SuppBill()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("InvDate", typeof(string)));
        dt.Columns.Add(new DataColumn("InvNo", typeof(string)));
        dt.Columns.Add(new DataColumn("TotalAmt", typeof(string)));
        dt.Columns.Add(new DataColumn("UnPlan_Recp_No", typeof(string)));
        Repeater2.DataSource = dt;
        Repeater2.DataBind();
        ViewState["SuppBillTable"] = Repeater2.DataSource;
        //dt = Repeater1.DataSource;
    }

    private void Initial_Data_Referesh_SuppAdvance()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("Trans_Date", typeof(string)));
        dt.Columns.Add(new DataColumn("Trans_No", typeof(string)));
        dt.Columns.Add(new DataColumn("AdvAmount", typeof(string)));
      
        Repeater3.DataSource = dt;
        Repeater3.DataBind();
        ViewState["SuppAdvanceTable"] = Repeater3.DataSource;
        //dt = Repeater1.DataSource;
    }


    private void Load_OLD_data_Item_Det()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Initial_Data_Referesh_Item_Det()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("InvDate", typeof(string)));
        dt.Columns.Add(new DataColumn("InvNo", typeof(string)));
        dt.Columns.Add(new DataColumn("TotalAmt", typeof(string)));
        dt.Columns.Add(new DataColumn("UnPlan_Recp_No", typeof(string)));
        dt.Columns.Add(new DataColumn("AdvDate", typeof(string)));
        dt.Columns.Add(new DataColumn("AdvNo", typeof(string)));
        dt.Columns.Add(new DataColumn("AdvAmt", typeof(string)));
        dt.Columns.Add(new DataColumn("CreditAmt", typeof(string)));
        dt.Columns.Add(new DataColumn("BalAmt", typeof(string)));
        dt.Columns.Add(new DataColumn("Discount", typeof(string)));
        dt.Columns.Add(new DataColumn("AddOrLess", typeof(string)));
        dt.Columns.Add(new DataColumn("NetBalAmt", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;
        //dt = Repeater1.DataSource;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Supplier Bill Details
        string query = "";
        int intI = 0;
        DataTable Main_DT = new DataTable();
        query = "Select * from Unplanned_Receipt_Supp_Payment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTrans_No.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtTransDate.Text = Main_DT.Rows[0]["Trans_Date"].ToString();
            txtSuppCodehide.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
            txtSupplierName.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtChequeNo.Text = Main_DT.Rows[0]["ChequeNo"].ToString();
            txtChequeDate.Text = Main_DT.Rows[0]["ChequeDate"].ToString();

            txtBankName.Text = Main_DT.Rows[0]["BankName"].ToString();
            txtBranch.Text = Main_DT.Rows[0]["BranchName"].ToString();

            txtDescription.Text = Main_DT.Rows[0]["Description"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["NetAmount"].ToString();
            txtTotalCredit.Text = Main_DT.Rows[0]["TotalCredit"].ToString();
            txtTotAdvance.Text = Main_DT.Rows[0]["TotalAdvance"].ToString();
            txtTotalDiscount.Value = Main_DT.Rows[0]["TotalDiscount"].ToString();
            txtTotAddLess.Text = Main_DT.Rows[0]["TotalAddorLess"].ToString();
            txtTotBalAmt.Text = Main_DT.Rows[0]["TotalBalance"].ToString();

            //Supplier Bill Details_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select InvDate,InvNo,TotalAmt,UnPlan_Recp_No,AdvNo,AdvDate,AdvAmt,CreditAmt,BalAmt,Discount,AddOrLess,NetBalAmt from Unplanned_Receipt_Supp_Payment_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTrans_No.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
            Totalsum();
            BtnSave.Text = "Update";

            Load_Data_Supplier_Bill_Det();
            Load_Data_Supplier_Advance_Det();
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Clear_All_Field()
    {
        txtTrans_No.Text = ""; txtTransDate.Text = "";
        txtSuppCodehide.Value = ""; txtSupplierName.Text = "";
        txtChequeNo.Text = ""; txtChequeDate.Text = "";
        txtDescription.Text = "";
        txtInvDate.Text = ""; txtInvNo.Text = ""; txtBillAmount.Text = "0.00";
        txtReceiptNo.Value = ""; txtAdvanceAmount.Text = "0.00";
        txtTotAmt.Text = "0.00"; txtTotAdvance.Text = "0.00";
        txtTotBalAmt.Text = "0.00"; txtTotAddLess.Text = "0.00";
        txtTotalCredit.Text = "0.00"; txtTotalDiscount.Value = "0.00";


        BtnSave.Text = "Save";
        Initial_Data_Referesh_Item_Det();
        Initial_Data_Referesh_SuppBill();
        Initial_Data_Referesh_SuppAdvance();
        Session.Remove("Supp_payment_Trans_No");
    }

    protected void txtDiscount_TextChanged(object sender, EventArgs e)
    {
        txtNetPayBillAmt.Text = ((Convert.ToDecimal(txtNetPayBillAmt.Text)) - ((Convert.ToDecimal(txtDiscount.Text)) + (Convert.ToDecimal(txtCreditNote.Text)))).ToString();
        Final_Total_Calculate();
    }
    protected void txtAddOrLess_TextChanged(object sender, EventArgs e)
    {
        if (txtAddOrLess.Text.ToString() != "") { Final_Total_Calculate(); }
        //txtNetAmt.Text = ((Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
    }
    private void Final_Total_Calculate()
    {

        string Final_NetAmt = "0";
        string AddorLess = "0";
        string Total_Amt = txtNetPayBillAmt.Text;

        if (Convert.ToDecimal(txtAddOrLess.Text.ToString()) != 0) { AddorLess = txtAddOrLess.Text.ToString(); }
        Final_NetAmt = (Convert.ToDecimal(AddorLess) + Convert.ToDecimal(Total_Amt)).ToString();
        //txtFinal_Total_Amt.Text = Final_NetAmt;
        txtNetPayBillAmt.Text = Final_NetAmt;

    }

    protected void txtCreditNote_TextChanged(object sender, EventArgs e)
    {
        txtNetPayBillAmt.Text = ((Convert.ToDecimal(txtNetPayBillAmt.Text)) - ((Convert.ToDecimal(txtDiscount.Text)) + (Convert.ToDecimal(txtCreditNote.Text)))).ToString();
        Final_Total_Calculate();
    }
}
