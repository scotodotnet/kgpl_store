﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Standard_Purchase_Order_Approve_View.aspx.cs" Inherits="Purchase_Order_Standard_Purchase_Order_Approve_View" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 
<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function() {
            $('#ItemTable').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable').dataTable();
            }
        });
    };
</script>




<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>
<style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
        table
        {
            border: 1px solid #ccc;
            width: auto;
            height:auto;
        }
        table th
        {
            background-color: #F7F7F7;
            color: #333;
            font-weight: bold;
        }
        table th, table td
        {
            padding: 10px;
            border-color: #ccc;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=40);
            opacity: 0.4;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            border: 3px solid #0DA9D0;
            width: auto;
            height: auto;
        }
        .modalPopup .header
        {
            background-color: #2FBDF1;
            height: 30px;
            color: White;
            line-height: 30px;
            text-align: center;
            font-weight: bold;
        }
        .modalPopup .body
        {
            min-height: 50px;
            line-height: 30px;
            text-align: center;
            padding: 5px;
        }
        .modalPopup .footer
        {
            padding: 3px;
        }
        .modalPopup .button
        {
            height: 23px;
            color: White;
            line-height: 23px;
            text-align: center;
            font-weight: bold;
            cursor: pointer;
            background-color: #9F9F9F;
            border: 1px solid #5C5C5C;
        }
        .modalPopup td
        {
            text-align: left;
        }
    </style>
<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Standard Purchase Order Approve View</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Standard Purchase Order Approve View</h4>
				</div>
			</div>
			<form class="form-horizontal">
				<div class="panel-body">				
				    <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Order No<span class="mandatory">*</span></label>
					            <asp:Label ID="txtStdOrderNo" runat="server" class="form-control"></asp:Label>					            
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Date</label>
					            <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" 
                                    runat="server" ReadOnly="True"></asp:TextBox>
					           <%-- <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>--%>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Supplier Name</label>
					            <asp:TextBox ID="txtSupplierName" class="form-control" runat="server" 
                                    ReadOnly="True"></asp:TextBox>
					            <asp:HiddenField ID="txtSuppCodehide" runat="server" />
					           <%-- <asp:RequiredFieldValidator ControlToValidate="txtSupplierName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					       
					        
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Payment Mode</label>
					           <asp:TextBox ID="txtPaymentMode" TextMode="MultiLine" class="form-control" runat="server" ReadOnly="True"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Description</label>
					            <asp:TextBox ID="txtDescription" TextMode="MultiLine" class="form-control" runat="server" ReadOnly="True"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Note</label>
					            <asp:TextBox ID="txtNote" TextMode="MultiLine" class="form-control" runat="server" ReadOnly="True"></asp:TextBox>
					        </div>
					       </div>
					</div>
					     
					<div class="col-md-12">
					    <div class="row">    
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Others</label>
					            <asp:TextBox ID="txtOthers" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
					        </div>
					    </div>
					</div>
					
					<div class="col-md-12">
					    <div class="row">
					        
					    </div>
					</div>
					
					
				    <div class="clearfix"></div>				    
				    <div class="timeline-options">
				        <h4 class="panel-title">Item Details</h4>
				    </div>
				
					
					
					<!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                   <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>LPD</th>
                                                <th>LPR</th>
                                                <th>Min/Max</th>
                                                <th>Item Name</th>
                                               
                                                <th>Order Qty</th>
                                                <th>Rate</th>
                                                <th>Total</th>
                                                <th>View</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("LastPoDate")%></td>
                                        <td><%# Eval("LastPoRate")%></td>
                                        <td><%# Eval("Min/Max")%></td>     
                                            
                                        <td><asp:Label ID="lblCustomerId" runat="server" Text='<%# Eval("ItemName") %>' /></td>
                                        <td><%# Eval("OrderQty")%></td>
                                        <td><span style='<%#Eval("ID")%>'><%#Eval("Rate")%></span></td>
                                        <td><%# Eval("LineTotal")%></td>
                                        <td>
                                        <%--<asp:LinkButton ID="btnOrders" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="DisplayOrders" >
                                                    </asp:LinkButton>--%>
                                       <asp:Button Text="View" class="btn btn-success btn-sm fa fa-pencil" runat="server" ID="btnOrders" OnClick="DisplayOrders" />
                                      </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
			               
					    </div>
					</div>
					<!-- table End -->
					 <asp:LinkButton Text="" ID="lnkFake" runat="server" />
					 <cc1:ModalPopupExtender ID="mpe" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
            CancelControlID="btnClose" BackgroundCssClass="modalBackground">
        </cc1:ModalPopupExtender>
        <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup" Style="display: none">
            <div class="header">
                Details
            </div>
            <div class="body">
                <asp:Repeater ID="rptOrders" runat="server">
                    <HeaderTemplate>
                        <table>
                            <tr>
                                
                                   <th>Date</th>
                                   <th>SuppName</th>
                                   <th>Rate</th>
                               
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                             <td><%# Eval("PO_Receipt_Date")%></td>
                             <td><%# Eval("ItemName")%></td>
                             <td><%# Eval("ItemRate")%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <div class="footer" align="right">
                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button" />
            </div>
        </asp:Panel>
					<div class="col-md-12">
					    <div class="row">
					     <div class="form-group col-md-10"></div>
					      <div class="form-group col-md-2">
					            <label for="exampleInputName">Total Amt</label>
					            <asp:TextBox ID="txtTotAmt" class="form-control" runat="server" Text="0.0" ReadOnly="True"></asp:TextBox>
					           <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTotAmt" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>--%>
					        </div>
					    </div>
					</div>
					
					
					<div class="col-md-12">
					    <div class="row">
					    <div class="form-group col-md-2">
					            <label for="exampleInputName">Tax Per</label>
					            <asp:TextBox ID="txtTax" class="form-control" runat="server" 
                                  AutoPostBack="true" ReadOnly="True"></asp:TextBox>
					            <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTax" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>--%>
					        </div>
					    <div class="form-group col-md-2">
					            <label for="exampleInputName">Tax Amount</label>
					            <asp:Label ID="txtTaxAmt" runat="server" class="form-control" ReadOnly="True"></asp:Label>
					         </div>
					    <div class="form-group col-md-2">
					            <label for="exampleInputName">Discount</label>
					            <asp:TextBox ID="txtDiscount" class="form-control" runat="server" ReadOnly="True"></asp:TextBox>
					           <%-- <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDiscount" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>--%>
					        </div>
					        
					    <div class="form-group col-md-2">
					            <label for="exampleInputName">Other Charge</label>
					            <asp:TextBox ID="txtOtherCharge" class="form-control" runat="server" 
                                     AutoPostBack="true" ReadOnly="True"></asp:TextBox>
					          <%--  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtOtherCharge" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>--%>
					        </div>
					        
					       
					    <div class="form-group col-md-2">
					            <label for="exampleInputName">Net Amt</label>
					            <asp:Label ID="txtNetAmt" runat="server" class="form-control"></asp:Label>
					           
					        </div>
					     </div>
					</div>
					   
					      	
						<div class="form-group row"></div>	
                        <!-- Button start -->
                        <div class="txtcenter">
                        <asp:Button ID="btnApprove" class="btn btn-success" runat="server" Text="Approve" OnClick="btnApprove_Click"/>
                        <asp:Button ID="btnPending" class="btn btn-success" runat="server" Text="Pending" 
                                onclick="btnPending_Click" />
                       <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                       <asp:Button ID="btnback" class="btn btn-success" runat="server" Text="Back" 
                                onclick="btnback_Click"/>
                      </div>
                        <!-- Button end -->
                    </div><!-- panel Body-->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
             </div>  
                        
             <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
             </div> 
		    <!-- Dashboard End -->
		    <div class="col-md-2"></div>
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->


</asp:Content>

