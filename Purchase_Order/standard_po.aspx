<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="standard_po.aspx.cs" Inherits="Purchase_Order_standard_po" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Supplier1').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Supplier1').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#SuppRefDocNo').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#SuppRefDocNo').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Dept').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Dept').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable_Normal').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable_Normal').dataTable();               
            }
        });
    };
</script>
<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable_PR').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable_PR').dataTable();               
            }
        });
    };
</script>


<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
     $find('Supp1_Close').hide();
     $find('SuppRefDocNo_Close').hide(); 
     $find('Dept_Close').hide();
     $find('Item_Close').hide();
     }
   } 
 </script>
<!--Ajax popup End-->



<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <h4><li class="active">Standard Purchase Order</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        
            <div class="col-md-12">
			<div class="panel panel-white">
			    <div class="panel panel-primary">
				    <div class="panel-heading clearfix">
					    <h4 class="panel-title">Standard Purchase Order</h4>
				    </div>
			    </div>
				<form class="form-horizontal">
				<div class="panel-body">		
				 <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-2">
                                <asp:Label ID="lblStatus" runat="server" Font-Bold="True" Font-Size="Large" 
                                    ForeColor="Red"></asp:Label>
					        </div>
					        </div>
					        </div>
				
						
				    <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Order No<span class="mandatory">*</span></label>
					            <asp:Label ID="txtStdOrderNo" runat="server" class="form-control"></asp:Label>					            
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Date</label>
					            <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Supplier Name</label>
					            <asp:TextBox ID="txtSupplierName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnSupp1" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnSupp1_Click"/>
					            
					            <cc1:ModalPopupExtender ID="modalPop_Supp1"  runat="server" PopupControlID="Panl1" TargetControlID="btnSupp1"
                       CancelControlID="BtnClear" BackgroundCssClass="modalBackground" BehaviorID="Supp1_Close">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="Panl1" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Supplier Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_Supplier1" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="Supplier1" class="display table">
                         <thead >
                         <tr>
                         <th>SuppCode</th>
                         <th>SuppName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("SuppCode")%></td>
                          <td><%# Eval("SuppName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick" CommandArgument='<%# Eval("SuppCode")%>' CommandName='<%# Eval("SuppName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>
					            
					            
					            
					            
					            <asp:HiddenField ID="txtSuppCodehide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtSupplierName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <div class="form-group col-md-2" runat="server" visible="false">
					            <label for="exampleInputName">Supp.Ref.Doc.No</label>
					            
					             <asp:DropDownList ID="txtSuppRefDocNo" runat="server" class="js-states form-control" AutoPostBack="true" 
                                    onselectedindexchanged="txtSuppRefDocNo_SelectedIndexChanged">
                                </asp:DropDownList>
					            
					          
					             <%--<asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtSuppRefDocNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					            
					            
					        </div>
					        <div class="form-group col-md-2" runat="server" visible="false">
					            <label for="exampleInputName">Doc Date</label>
					            <asp:TextBox ID="txtDocDate" MaxLength="20"  class="form-control" runat="server"></asp:TextBox>
					            <%-- <asp:RequiredFieldValidator ControlToValidate="txtDocDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDocDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>--%>
					        
					        
					        </div>
					         <div class="form-group col-md-2">
					            <label for="exampleInputName">Delivery Mode</label>
					            <select name="txtDeliveryMode" id="txtDeliveryMode" class="js-states form-control" runat="server">
                                </select>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Delivery Date</label>
					            <asp:TextBox ID="txtDeliveryDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtDeliveryDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDeliveryDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					       
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Delivery At</label>
					            <asp:TextBox ID="txtDeliveryAt" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Payment Mode</label>
					            <select name="txtPaymentMode" id="txtPaymentMode" class="js-states form-control" runat="server">
	                                <option value="- select -">- select -</option>
	                                <option value="CASH">CASH</option>
	                                <option value="CHEQUE">CHEQUE</option>
	                                <option value="NEFT">NEFT</option>
	                                <option value="RTGS">RTGS</option>
	                                <option value="DD">DD</option>
	                            </select>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Department Name</label>
					            
					            <asp:DropDownList ID="txtDepartmentName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					            
					           
					            
					   <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtDepartmentName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>          
					          
					        </div>
					         <div class="form-group col-md-3">
					            <label for="exampleInputName">Payment Terms</label>
					            <asp:TextBox ID="txtPaymentTerms" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					        
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Description</label>
					            <asp:TextBox ID="txtDescription" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
					        </div>
					       
					        
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Note</label>
					            <asp:TextBox ID="txtNote" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Others</label>
					            <asp:TextBox ID="txtOthers" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
					        </div>
										        
					    </div>
					</div>
					
					<div class="col-md-12">
					    <div class="row">
					    
					       <div class="form-group col-md-4">
					            <label for="exampleInputName">Pur Order Type</label>
					            <asp:RadioButtonList ID="RdpPOType" class="form-control" runat="server" 
                                    RepeatColumns="3" onselectedindexchanged="RdpPOType_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="1" Text="Direct" style="padding-right:40px" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Req/Amend" ></asp:ListItem>
                                <asp:ListItem Value="3" Text="Special" ></asp:ListItem>
                                </asp:RadioButtonList>					            
					        </div>
				
					       <div class="form-group col-md-2">
					            <label for="exampleInputName">Request No</label>
					            <asp:TextBox ID="txtRequestNo" class="form-control" runat="server"></asp:TextBox>
					           <%-- <asp:RequiredFieldValidator ControlToValidate="txtRequestNo" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Request Date</label>
					            <asp:TextBox ID="txtRequestDate" class="form-control date-picker" runat="server"></asp:TextBox>
					        </div>
					   </div>
					</div>
			
					<div class="col-md-12">
					    <div class="row">
					        
					    </div>
					</div>
					
					
				    <div class="clearfix"></div>				    
				    <div class="timeline-options">
				        <h4 class="panel-title">Item Details</h4>
				    </div>
				
					<div class="col-md-12">
					    <div class="row">
					       
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Name</label>
					            <asp:TextBox ID="txtItemName" class="form-control" runat="server" style="float: left;width: 82%;"></asp:TextBox>
					         
					            <asp:Button ID="btnItemCode" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnItemCode_Click"/>
                                
                         <cc1:ModalPopupExtender ID="modalPop_ItemCode"  runat="server" PopupControlID="Panel5" TargetControlID="btnItemCode"
                       CancelControlID="BtnItemClose" BackgroundCssClass="modalBackground" BehaviorID="Item_Close">
                    </cc1:ModalPopupExtender>
                         <asp:Panel ID="Panel5" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Item Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_ItemCode_Normal" runat="server" EnableViewState="false" Visible="false">
					    <HeaderTemplate>
                         <table id="ItemTable_Normal" class="display table">
                         <thead >
                         <tr>
                         <th>ItemCode</th>
                         <th>ItemName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ItemCode")%></td>
                          <td><%# Eval("ItemName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEdit_ItemCode" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ItemCode" CommandArgument='<%# Eval("ItemCode")%>' CommandName='<%# Eval("ItemName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					    
					    
					    <asp:Repeater ID="Repeater_ItemCode_PR" runat="server" EnableViewState="false" Visible="false">
					    <HeaderTemplate>
                         <table id="ItemTable_PR" class="display table">
                         <thead >
                         <tr>
                         <th>ItemCode</th>
                         <th>ItemName</th>
                         <th>RequiredQty</th>
                         <th>RequiredDate</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ItemCode")%></td>
                          <td><%# Eval("ItemName")%></td>
                           <td><%# Eval("ReuiredQty")%></td>
                          <td><%# Eval("ReuiredDate")%></td>
                          <td>
                          <asp:LinkButton ID="btnEdit_ItemCode_PR" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ItemCode_PR" CommandArgument='<%# Eval("ItemCode")%>' CommandName='<%# Eval("ItemName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					    
					    
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnItemClose" class="btn btn-rounded" runat="server" Text="Close" />
					     </div>
					     </asp:Panel>
					            
					            
					            <asp:HiddenField ID="txtItemCodeHide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtItemName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					         

					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Required Qty</label>
					            <asp:TextBox ID="txtRequiredQty" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Order Qty</label>
					            <asp:TextBox ID="txtOrderQty" class="form-control" runat="server" AutoPostBack="true" 
                                    ontextchanged="txtOrderQty_TextChanged"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtOrderQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtOrderQty" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Required Date</label>
					            <asp:TextBox ID="txtRequiredDate" class="form-control date-picker" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtRequiredDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Rate</label>
					            <asp:TextBox ID="txtRate" class="form-control" runat="server" Text="0.0"  AutoPostBack="true"
                                    ontextchanged="txtRate_TextChanged"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtRate" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtRate" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					        </div>
					        </div>
					        
					<div class="col-md-12">
					    <div class="row"> 
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Remarks</label>
					            <asp:TextBox ID="txtRemarks" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Item Total</label>
					            <asp:TextBox ID="txtItemTotal" runat="server" class="form-control" Text="0.0"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-1">
					            <label for="exampleInputName">Disc. Per</label>
					            <asp:TextBox ID="txtDiscount_Per" class="form-control" runat="server" 
                                    Text="0.0" AutoPostBack="true" ontextchanged="txtDiscount_Per_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDiscount_Per" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Discount Amount</label>
					            <asp:Label ID="txtDiscount_Amount" runat="server" class="form-control" Text="0.0"></asp:Label>
					         </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">GST Type</label>
					            <asp:DropDownList ID="txtGST_Type" runat="server" class="form-control" 
                                    AutoPostBack="true" onselectedindexchanged="txtGST_Type_SelectedIndexChanged">
                                </asp:DropDownList>
					          </div>
					        <div class="form-group col-md-1">
					            <label for="exampleInputName">CGST %</label>
					            <asp:TextBox ID="txtCGSTPer" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtCGSTPer" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					         <div class="form-group col-md-2">
					            <label for="exampleInputName">CGST Amt</label>
					            <asp:TextBox ID="txtCGSTAmt" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
					         </div>
				          <div class="form-group col-md-1">
					            <label for="exampleInputName">SGST %</label>
					            <asp:TextBox ID="txtSGSTPer" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtSGSTPer" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
				                <label for="exampleInputName">SGST Amt</label>
				                <asp:TextBox ID="txtSGSTAmt" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
				            </div>
				           <div class="form-group col-md-1">
					            <label for="exampleInputName">IGST %</label>
					            <asp:TextBox ID="txtIGSTPer" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtSGSTPer" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
				                <label for="exampleInputName">IGST Amt</label>
				                <asp:TextBox ID="txtIGSTAmt" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
				            </div>
				            
				            <div class="form-group col-md-1">
					            <label for="exampleInputName">Other Amt</label>
					            <asp:TextBox ID="txtOtherCharge" class="form-control" runat="server" Text="0" 
                                    AutoPostBack="true" ontextchanged="txtOtherCharge_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtOtherCharge" ValidChars="0123456789.-">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
				                <label for="exampleInputName">Total Amt</label>
				                <asp:Label ID="txtFinal_Total_Amt" runat="server" class="form-control" Text="0.0"></asp:Label>
				            </div>
					        <div class="form-group col-md-1">
					            <br />
					            <asp:Button ID="btnAddItem" Width="50" Height="30" class="btn-success"  runat="server" Text="Add" ValidationGroup="Item_Validate_Field"  OnClick="btnAddItem_Click"/>
					        </div>
					    </div>
					</div>
					
					<!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                               <%-- <th>StdPo Number</th>--%>
                                                <th>Item Name</th>
                                                <th>Requ. Qty</th>
                                                <th>Order Qty</th>
                                                <th>Requ. Date</th>
                                                <th>Rate</th>
                                                <th>Remarks</th>
                                                <th>Total</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                       <%-- <td><%# Eval("Pur_Request_No")%></td>
                                        <td><%# Eval("Pur_Request_Date")%></td>--%>
                                        <%--<td><%# Eval("Std_PO_No")%></td>--%>
                                        <td><%# Eval("ItemName")%></td>
                                        <%--<td><%# Eval("UOMCode")%></td>--%>
                                        <td><%# Eval("ReuiredQty")%></td>
                                        <td><%# Eval("OrderQty")%></td>
                                        <td><%# Eval("ReuiredDate")%></td>
                                        <td><%# Eval("Rate")%></td>
                                        <td><%# Eval("Remarks")%></td>
                                        <td><%# Eval("LineTotal")%></td>
                                        <td>
                                         <%--CommandArgument='<%# Eval("Std_PO_No")%>'--%>
                                        
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandName='<%# Eval("ItemCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
					
					<div class="col-md-12">
					    <div class="row">
					     <div class="form-group col-md-10"></div>
					      <div class="form-group col-md-2">
					            <label for="exampleInputName">Total Amt</label>
					            <asp:TextBox ID="txtTotAmt" class="form-control" runat="server" Text="0.0"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTotAmt" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					    </div>
					    </div>
					
					
					<div class="col-md-12">
					    <div class="row">
					    <div class="form-group col-md-8">
					       </div>
					    <div class="form-group col-md-1" runat="server" visible="false">
					            <label for="exampleInputName">Tax Per</label>
					            <asp:TextBox ID="txtTax" class="form-control" runat="server"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTax" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2" runat="server" visible="false">
					            <label for="exampleInputName">Tax Amount</label>
					            <asp:Label ID="txtTaxAmt" runat="server" class="form-control" Text="0"></asp:Label>
					         </div>
					       <div class="form-group col-md-1" runat="server" visible="false">
					            <label for="exampleInputName">Discount</label>
					            <asp:TextBox ID="txtDiscount" class="form-control" runat="server" 
                                    ontextchanged="txtDiscount_TextChanged" Text="0" AutoPostBack="true" ></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDiscount" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					      
					        
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Add or Less</label>
					            <asp:TextBox ID="txtAddOrLess" class="form-control" runat="server" 
                                    Text="0" AutoPostBack="true" ontextchanged="txtAddOrLess_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtAddOrLess" ValidChars="0123456789.-">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Net Amt</label>
					            <asp:Label ID="txtNetAmt" runat="server" class="form-control"></asp:Label>
					           
					        </div>
					        </div>
					</div>
					   
					      	
						<div class="form-group row"></div>	
                        <!-- Button start -->
                        <div class="txtcenter">
                            <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                            <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                            <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                            <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" Text="Back" OnClick="btnBackRequest_Click"/>
                            <asp:Button ID="btnApprove" class="btn btn-success" runat="server" Text="Approve" OnClick="btnApprove_Click"/>
                        </div>
                        <!-- Button end -->
                    </div><!-- panel Body-->
				</form>
			
			</div><!-- panel white end -->
		    </div><!-- col-12 end -->
		    
		    
		    <div class="col-md-2"></div>
		    
            
      
         
 </div><!-- col 12 end -->
  </div><!-- row end -->
 </div>
 
</asp:Content>

