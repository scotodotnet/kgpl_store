﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="general_po.aspx.cs" Inherits="Purchase_Order_general_po" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Supplier1').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Supplier1').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Dept').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Dept').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable').dataTable();               
            }
        });
    };
</script>




<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
     $find('Supp1_Close').hide();
     $find('Dept_Close').hide();
     $find('Item_Close').hide();
     }
   } 
 </script>
<!--Ajax popup End-->




<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <h4><li class="active">General Purchase Order (Non-Inventory Items)</li></h4> 
    </ol>
</div>
                
        
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">General Purchase Order</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
				<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-2">
                                <asp:Label ID="lblStatus" runat="server" Font-Bold="True" Font-Size="Large" 
                                    ForeColor="Red"></asp:Label>
					        </div>
					        </div>
					        </div>
				
				
				
				    <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Order No<span class="mandatory">*</span></label>
					            <asp:Label ID="txtGenOrderNo" runat="server" class="form-control"></asp:Label>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Date</label>
					            <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server" AutoComplete="Off"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Supplier Name</label>
					            
                                <asp:DropDownList ID="ddlSupplier" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					      
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Supp.Ref.Doc.No</label>
					            <asp:TextBox ID="txtSuppRefDocNo" class="form-control" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtSuppRefDocNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Doc Date</label>
					            <asp:TextBox ID="txtDocDate" MaxLength="20" class="form-control date-picker" runat="server" AutoComplete="Off"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Delivery Mode</label>
					            <select name="txtDeliveryMode" id="txtDeliveryMode" class="js-states form-control" runat="server">
                                </select>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Delivery Date</label>
					            <asp:TextBox ID="txtDeliveryDate" MaxLength="20" class="form-control date-picker" runat="server" AutoComplete="Off"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtDeliveryDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDeliveryDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Delivery At</label>
					            <asp:TextBox ID="txtDeliveryAt" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Payment Mode</label>
					            <select name="txtPaymentMode" id="txtPaymentMode" class="js-states form-control" runat="server">
	                                <option value="- select -">- select -</option>
	                                <option value="CASH">CASH</option>
	                                <option value="CHEQUE">CHEQUE</option>
	                                <option value="NEFT">NEFT</option>
	                                <option value="RTGS">RTGS</option>
	                                <option value="DD">DD</option>
	                            </select>
					        </div>
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Department Name</label>
					            
					            <asp:DropDownList ID="txtDepartmentName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					             <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtDepartmentName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>    
					            
					           <%-- <asp:TextBox ID="txtDepartmentName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>--%>
					            <%--<asp:Button ID="btnDept" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnDept_Click"/>--%>
                    <%--<cc1:ModalPopupExtender ID="modalPop_Dept"  runat="server" PopupControlID="Panel2" TargetControlID="btnDept"
                       CancelControlID="BtnClear_Dept" BackgroundCssClass="modalBackground" BehaviorID="Dept_Close">
                    </cc1:ModalPopupExtender>--%>
                   
                    <%--<asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Department Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_Dept" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="Dept" class="display table">
                         <thead >
                         <tr>
                         <th>DeptCode</th>
                         <th>DeptName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("DeptCode")%></td>
                          <td><%# Eval("DeptName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_Dept" CommandArgument='<%# Eval("DeptCode")%>' CommandName='<%# Eval("DeptName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_Dept" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            
					            
					            <%--<asp:HiddenField ID="txtDeptCodeHide" runat="server" />--%>
					            <%--<asp:RequiredFieldValidator ControlToValidate="txtDepartmentName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Payment Terms</label>
					            <asp:TextBox ID="txtPaymentTerms" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Others</label>
					            <asp:TextBox ID="txtOthers" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
					        </div>
					    </div>
					</div>
					
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-12">
					            <label for="exampleInputName">Description</label>
					            <asp:TextBox ID="txtDescription" TextMode="MultiLine" class="form-control" runat="server" AutoPostBack="true"></asp:TextBox>
					        </div>
					        <asp:TextBox ID="txtNote" TextMode="MultiLine" class="form-control" runat="server" Visible="false"></asp:TextBox>
					    </div>
					</div>
					
					
				    <div class="clearfix"></div>				    
				    <div class="timeline-options">
				        <h4 class="panel-title">Item Details</h4>
				    </div>
				
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-5">
					            <label for="exampleInputName">Item Name</label>
					             <asp:DropDownList ID="ddlItemName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Order Qty</label>
					            <asp:TextBox ID="txtOrderQty" class="form-control" runat="server" 
                                    AutoPostBack="true" ontextchanged="txtOrderQty_TextChanged"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtOrderQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtOrderQty" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Rate</label>
					            <asp:TextBox ID="txtRate" class="form-control" runat="server" Text="0.0" 
                                    AutoPostBack="true" ontextchanged="txtRate_TextChanged"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtRate" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtRate" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					     </div>
					     </div>  
					        
					<div class="col-md-12">
					    <div class="row">        
					       
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Remarks</label>
					            <asp:TextBox ID="txtRemarks" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Item Total</label>
					            <asp:TextBox ID="txtItemTotal" runat="server" class="form-control" Text="0.0"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Disc. Per</label>
					            <asp:TextBox ID="txtDiscount_Per" class="form-control" runat="server" 
                                    Text="0.0" AutoPostBack="true" ontextchanged="txtDiscount_Per_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDiscount_Per" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Discount Amount</label>
					            <asp:Label ID="txtDiscount_Amount" runat="server" class="form-control" Text="0.0"></asp:Label>
					         </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">GST Type</label>
					            <asp:DropDownList ID="txtGST_Type" runat="server" class="form-control" 
                                    AutoPostBack="true" onselectedindexchanged="txtGST_Type_SelectedIndexChanged">
                                </asp:DropDownList>
					          </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">CGST %</label>
					            <asp:TextBox ID="txtCGSTPer" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtCGSTPer" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					         <div class="form-group col-md-2">
					            <label for="exampleInputName">CGST Amt</label>
					            <asp:TextBox ID="txtCGSTAmt" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
					         </div>
				          <div class="form-group col-md-2">
					            <label for="exampleInputName">SGST %</label>
					            <asp:TextBox ID="txtSGSTPer" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtSGSTPer" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
				                <label for="exampleInputName">SGST Amt</label>
				                <asp:TextBox ID="txtSGSTAmt" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
				            </div>
				           <div class="form-group col-md-2">
					            <label for="exampleInputName">IGST %</label>
					            <asp:TextBox ID="txtIGSTPer" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtSGSTPer" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
				                <label for="exampleInputName">IGST Amt</label>
				                <asp:TextBox ID="txtIGSTAmt" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
				            </div>
				            
				            <div class="form-group col-md-2">
					            <label for="exampleInputName">Other Amt</label>
					            <asp:TextBox ID="txtOtherCharge" class="form-control" runat="server" Text="0" 
                                    AutoPostBack="true" ontextchanged="txtOtherCharge_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtOtherCharge" ValidChars="0123456789.-">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
				                <label for="exampleInputName">Total Amt</label>
				                <asp:Label ID="txtFinal_Total_Amt" runat="server" class="form-control" Text="0.0"></asp:Label>
				            </div>
					       
					        <div class="form-group col-md-1">
					            <br />
					            <asp:Button ID="btnAddItem" Width="50" Height="30" class="btn-success"  runat="server" Text="Add" ValidationGroup="Item_Validate_Field"  OnClick="btnAddItem_Click"/>
					        </div>
					    </div>
					</div>
						
					<!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Item Code</th>
                                                <th>Item Name</th>
                                                <th>UOM Code</th>
                                                <th>Order Qty</th>
                                                <th>Rate</th>
                                              
                                                <th>Remarks</th>
                                                <th>Total</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("ItemCode")%></td>
                                        <td><%# Eval("ItemName")%></td>
                                        <td><%# Eval("UOMCode")%></td>
                                        <td><%# Eval("OrderQty")%></td>
                                        <td><%# Eval("Rate")%></td>
                                       
                                        <td><%# Eval("Remarks")%></td>
                                        <td><%# Eval("LineTotal")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument='Delete' CommandName='<%# Eval("ItemCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
					
					
					<div class="col-md-12">
					    <div class="row">
					     <div class="form-group col-md-10"></div>

                           
                            <div class="form-group col-md-2" runat="server" visible="false">
                                <asp:CheckBox ID="chkPacking" runat="server" Text="IncludeTax"/> 
                            </div>

                            
					      <div class="form-group col-md-2">
					            <label for="exampleInputName">Total Amt</label>
					            <asp:TextBox ID="txtTotAmt" class="form-control" runat="server" Text="0.0"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTotAmt" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					    </div>
					    </div>
					
					
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-1" runat="server" visible="false">
					            <label for="exampleInputName">Dis.Per</label>
					            <asp:TextBox ID="txtdis" class="form-control" runat="server" 
                                     Text="0" ></asp:TextBox>
					            
					        </div>
					        <div class="form-group col-md-2" runat="server" visible="false">
					            <label for="exampleInputName">Discount Amount</label>
					            <asp:Label ID="txtDiscount11" runat="server" class="form-control" Text="0"></asp:Label>
					         </div>
					        <div class="form-group col-md-1" runat="server" visible="false">
					            <label for="exampleInputName">Tax.Per</label>
					            <asp:TextBox ID="txtTax" class="form-control" runat="server" 
                                     AutoPostBack="true" ontextchanged="txtTax_TextChanged" Text="0"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTax" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2" runat="server" visible="false">
					            <label for="exampleInputName">Tax Amount</label>
					            <asp:Label ID="txtTaxAmt" runat="server" class="form-control" Text="0"></asp:Label>
					        </div>
					        <div class="form-group col-md-2" runat="server" visible="false">
					            <label for="exampleInputName">Excise Duty</label>
					            <asp:TextBox ID="txtOtherCharge1" class="form-control" runat="server" 
                                     AutoPostBack="true" ontextchanged="txtOtherCharge_TextChanged" Text="0"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtOtherCharge" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2" runat="server" visible="false">
					            <label for="exampleInputName">Excise Duty Amt</label>
					            <asp:Label ID="txtExciseDutyAmt" runat="server" class="form-control" Text="0"></asp:Label>
					        </div>
					       <div id="Div1" class="form-group col-md-4">
					        </div>

                            <div class="form-group col-md-2">
					            <label for="exampleInputName">Packing Amt</label>
					            <asp:TextBox ID="txtPackingAmt" class="form-control" runat="server" Text="0.0" AutoPostBack="true" 
                                    OnTextChanged="txtPackingAmt_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtPackingAmt" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>

                            <div class="form-group col-md-2">
					            <label for="exampleInputName">Total Discount</label>
					            <asp:TextBox ID="txtTotDisc" class="form-control" runat="server" Text="0.0" AutoPostBack="true" OnTextChanged="txtTotDisc_TextChanged" 
                                    ></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtPackingAmt" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>

					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Add or Less</label>
					            <asp:TextBox ID="txtAddOrLess" class="form-control" runat="server" 
                                    Text="0" AutoPostBack="true" ontextchanged="txtAddOrLess_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtAddOrLess" ValidChars="0123456789.-">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					       
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Net Amt</label>
					            <asp:Label ID="txtNetAmt" runat="server" class="form-control"></asp:Label>
					           
					        </div>
					        </div>
					</div>
					
					
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                        <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                        <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" Text="Back" OnClick="btnBackRequest_Click"/>
                       <asp:Button ID="btnApprove" class="btn btn-success" runat="server" Text="Approve" OnClick="btnApprove_Click"/>
                    </div>
                    <!-- Button end -->
					 
                </div> <!-- Panel-Body End -->
			</form>
			
		</div><!-- panel white end -->
	</div><!-- col-9 end -->
		    
	<div class="col-md-2"></div>
		    
            
 <!-- Dashboard start -->
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-white" style="height: 100%;">
            <div class="panel-heading">
                <h4 class="panel-title">Dashboard Details</h4>
                <div class="panel-control">
                    
                    
                </div>
            </div>
            <div class="panel-body">
                
                
            </div>
        </div>
    </div>  
                        
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-white">
            <div class="panel-body">
                <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                   
                </div>
            </div>
        </div>
        <div class="panel panel-white">
            <div class="panel-body">
                <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                    
                    
                    
                </div>
            </div>
        </div>
    </div> 
    <!-- Dashboard end -->
                        
    <div class="col-md-2"></div>
                        
    </div>     <!-- col 12 end -->
    </div><!-- row end -->
</div>
 
    
</asp:Content>

