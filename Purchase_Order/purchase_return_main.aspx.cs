﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Order_purchase_return_main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Purchase Return";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Load_Data_PurchaseReturn_Grid();
    }

    public void Load_Data_PurchaseReturn_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Pur_Return_No,Pur_Return_Date,Supp_Code,Supp_Name,DCNo,DCDate,WarehouseCode,WarehouseName,DeptName from Purc_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string Enquiry_No_Str = e.CommandName.ToString();
        Session.Remove("Pur_Return_No");
        Session["Pur_Return_No"] = Enquiry_No_Str;
        Response.Redirect("purchase_return.aspx");

    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "2", "Purchase Return");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete Purchase Return Details..');", true);
        }
        //User Rights Check End

        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        query = "Select * from Purc_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Return_No='" + e.CommandName.ToString() + "' And PO_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Delete Purchase Return Details Already Approved..');", true);
        }
        //Check With Already Approved End

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Purc_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Return_No='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //Delete Main Table
                query = "Delete from Purc_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Return_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                //Delete Main Sub Table
                query = "Delete from Purc_Return_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Return_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Return Details Deleted Successfully');", true);
                Load_Data_PurchaseReturn_Grid();
            }
        }
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start


        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "2", "Purchase Return");
        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Purchase Order Receipt..');", true);
        }
        else
        {
            Session.Remove("Pur_Return_No");
            Response.Redirect("purchase_return.aspx");
        }
    }
}
