﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Order_Standard_Purchase_Order_Approve : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionStdPOOrderNo;
  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Standard Purchase Order Approve";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
            purchase_Request_Status_Add();
            
        }
        Load_Data_Empty_Grid();
        //Load_OLD_data();

    }


    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        string Std_No_Str = e.CommandArgument.ToString();
        string PO_Type=   e.CommandName.ToString();
        if (PO_Type == "Std/PO")
        {
            Session.Remove("Std_PO");
            Session.Remove("Blanket_PO");
            Session.Remove("Gen_PO");
            Session["Std_PO"] = Std_No_Str;
            Response.Redirect("Standard_Purchase_Order_Approve_View.aspx");
        }
        else if (PO_Type == "Blnk/PO")
        {
            Session.Remove("Std_PO");
            Session.Remove("Blanket_PO");
            Session.Remove("Gen_PO");
            Session["Blanket_PO"] = Std_No_Str;
            Response.Redirect("Blanket_po_Approve_View.aspx");
        }
        else if (PO_Type == "Gen/PO")
        {
            Session.Remove("Std_PO");
            Session.Remove("Blanket_PO");
            Session.Remove("Gen_PO");
            Session["Gen_PO"] = Std_No_Str;
            Response.Redirect("General_po_Approve_View.aspx");
        }
    }


  
    private void purchase_Request_Status_Add()
    {
        txtRequestStatus.Items.Clear();
       
        txtRequestStatus.Items.Add("Pending List");
        txtRequestStatus.Items.Add("MD Pending List");
        txtRequestStatus.Items.Add("Approved List");
        txtRequestStatus.Items.Add("Rejected List");
    }
    private void Load_Data_Empty_Grid()
    {

        string query = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();


        DT.Columns.Add("Transaction_No");
        DT.Columns.Add("Date");
        DT.Columns.Add("Type_PO");
        DT.Columns.Add("SupplierName");
        DT.Columns.Add("TotalQuantity");
        DT.Columns.Add("TotalAmt");
      

        if (txtRequestStatus.Text == "Pending List")
        {
            query = "Select Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,TotalAmt from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And (Status = '0' or Status is null)";
            DT = objdata.RptEmployeeMultipleDetails(query);

            Repeater_StdPurOrder.DataSource = DT;
            Repeater_StdPurOrder.DataBind();
            Repeater_StdPurOrder.Visible = true;
        }
        else if (txtRequestStatus.Text == "MD Pending List")
        {
            query = "Select Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,TotalAmt from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '3'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            Repeater_StdPurOrder.DataSource = DT;
            Repeater_StdPurOrder.DataBind();
            Repeater_StdPurOrder.Visible = true;
        }
        else if (txtRequestStatus.Text == "Approved List")
        {
            query = "Select Top 30 Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,TotalAmt from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '1' ORDER BY Date Desc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_StdPurOrder.DataSource = DT;
            Repeater_StdPurOrder.DataBind();
            Repeater_StdPurOrder.Visible = true;
        }
        else if (txtRequestStatus.Text == "Rejected List")
        {
            query = "Select Top 30 Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,TotalAmt from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '2' ORDER BY Date Desc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_StdPurOrder.DataSource = DT;
            Repeater_StdPurOrder.DataBind();
            Repeater_StdPurOrder.Visible = true;
        }
    }
    protected void txtRequestStatus_SelectedIndexChanged(object sender, EventArgs e)
    {

        string query = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();

        DT.Columns.Add("Transaction_No");
        DT.Columns.Add("Date");
        DT.Columns.Add("Type_PO");
        DT.Columns.Add("SupplierName");
        DT.Columns.Add("TotalQuantity");
        DT.Columns.Add("TotalAmt");

         if (txtRequestStatus.Text == "Pending List")
        {
            query = "Select Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,TotalAmt from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And (Status = '0' or Status is null or Status = '3')";
            DT = objdata.RptEmployeeMultipleDetails(query);

            Repeater_StdPurOrder.DataSource = DT;
            Repeater_StdPurOrder.DataBind();
            Repeater_StdPurOrder.Visible = true;
        }
         else if (txtRequestStatus.Text == "MD Pending List")
         {
             query = "Select Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,TotalAmt from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '3'";
             DT = objdata.RptEmployeeMultipleDetails(query);

             Repeater_StdPurOrder.DataSource = DT;
             Repeater_StdPurOrder.DataBind();
             Repeater_StdPurOrder.Visible = true;
         }
        else if (txtRequestStatus.Text == "Approved List")
        {
            query = "Select Top 30 Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,TotalAmt from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '1' ORDER BY Date Desc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_StdPurOrder.DataSource = DT;
            Repeater_StdPurOrder.DataBind();
            Repeater_StdPurOrder.Visible = true;
        }
        else if (txtRequestStatus.Text == "Rejected List")
        {
            query = "Select Top 30 Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,TotalAmt from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '2' ORDER BY Date Desc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_StdPurOrder.DataSource = DT;
            Repeater_StdPurOrder.DataBind();
            Repeater_StdPurOrder.Visible = true;
        }
    }
   

}
