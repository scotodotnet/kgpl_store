﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Order_blanket_po : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionBlanketPOOrderNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static Decimal OrderQty;
    static string sum;
    string SessionPurRequestNoApproval;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Blanket Purchase Order";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_Data_Empty_SuppRefDocNo();
            Load_Data_Empty_Dept();
            Delivery_Mode_Add();
            if (Session["Blanket_PO_No"] == null)
            {
                SessionBlanketPOOrderNo = "";
               
            }
            else
            {
                SessionBlanketPOOrderNo = Session["Blanket_PO_No"].ToString();
                txtBlanketNo.Text = SessionBlanketPOOrderNo;
                btnSearch_Click(sender, e);
               
            }
            if (Session["Pur_RequestNo_Approval"] == null)
            {
                SessionPurRequestNoApproval = "";
                
            }
            else
            {
                RdpPOType.SelectedValue = "2";
                SessionPurRequestNoApproval = Session["Pur_RequestNo_Approval"].ToString();
                txtBlanketNo.Text = SessionPurRequestNoApproval;
                btnSearchView_Click(sender, e);
            

            }



        }
        Load_OLD_data();
        Load_Data_Empty_Supp1();
        
        
        Load_Data_Empty_ItemCode();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }
        if (txtDeliveryMode.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Delivery Mode...');", true);
        }
        if (txtPaymentMode.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Payment Mode...');", true);
        }

        //Check Supplier Name And Quotation No
        query = "Select * from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And QuotNo='" + txtSuppRefDocNo.SelectedItem.Text + "' And SuppCode='" + txtSuppCodehide.Value + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Supplier Name And Supp.Ref.Doc.No...');", true);
        }
        //Check Department Name
        query = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + txtDepartmentName.SelectedValue + "' And DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Department Name...');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Blanket Purchase Order");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Blanket Purchase Order Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Blanket Purchase Order");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Blanket Purchase Order..');", true);
            }
        }
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Blanket Purchase Order", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtBlanketNo.Text = Auto_Transaction_No;
                }
            }
        }


        if (btnSave.Text == "Update")
        {
            query = "Select * from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + txtBlanketNo.Text + "' And Blanket_PO_Status='1'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket Purchase Order Details get Approved Cannot Update it..');", true);
            }
        }


        if (!ErrFlag)
        {
            query = "Select * from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + txtBlanketNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + txtBlanketNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Blanket_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + txtBlanketNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);

                query = "Delete from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + txtBlanketNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);

                query = "Delete from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + txtBlanketNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);



            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into Blanket_Purchase_Order_Main(Ccode,Lcode,FinYearCode,FinYearVal,Blanket_PO_No,Blanket_PO_Date,Supp_Code,Supp_Name,Supp_Qtn_No,Supp_Qtn_Date,DeliveryMode,";
            query = query + " DeliveryAt,PaymentMode,DeptCode,DeptName,PaymentTerms,Description,Note,Others,TotalOrderQty,TotalAmt,UserID,UserName,Blanket_PO_Type,Blanket_PO_Status) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtBlanketNo.Text + "','" + txtDate.Text + "','" + txtSuppCodehide.Value + "','" + txtSupplierName.Text + "',";
            query = query + " '" + txtSuppRefDocNo.SelectedItem.Text + "','" + txtDocDate.Text + "','" + txtDeliveryMode.Value + "','" + txtDeliveryAt.Text + "',";
            query = query + " '" + txtPaymentMode.Value + "','" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "','" + txtPaymentTerms.Text + "',";
            query = query + " '" + txtDescription.Text + "','" + txtNote.Text + "','" + txtOthers.Text + "','" + OrderQty + "','" + txtTotAmt.Text + "','" + SessionUserID + "','" + SessionUserName + "','" + RdpPOType.SelectedItem.Text + "',";
          
             if(RdpPOType.SelectedItem.Text=="Special")
            {
                query = query + " '4')";
            }
            else
            {
                query = query + " '0')";
            }
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Blanket_Purchase_Order_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Blanket_PO_No,Blanket_PO_Date,ItemCode,ItemName,UOMCode,ReuiredQty,OrderQty,Rate,LineTotal,Schedule,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtBlanketNo.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["ReuiredQty"].ToString() + "','" + dt.Rows[i]["OrderQty"].ToString() + "','" + dt.Rows[i]["Rate"].ToString() + "','" + dt.Rows[0]["LineTotal"].ToString() + "','" + dt.Rows[i]["Schedule"].ToString() + "',";
                query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }

            if (RdpPOType.SelectedItem.Text == "Special")
            {
                //Insert Purchase Request Approval Table
                query = "Insert Into Pur_Request_Approval(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_Request_Amend,SupplierName,TotalQuantity,TotalAmt,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtBlanketNo.Text + "','" + txtDate.Text + "','Blnk/PO','" + txtSupplierName.Text + "','" + OrderQty + "','" + txtTotAmt.Text + "',";
                query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }

            if (RdpPOType.SelectedItem.Text == "Direct")
            {
                //Insert Purchase Request Approval Table
                query = "Insert Into Std_Purchase_Order_Approve(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,TotalAmt,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtBlanketNo.Text + "','" + txtDate.Text + "','Blnk/PO','" + txtSupplierName.Text + "','" + OrderQty + "','" + txtTotAmt.Text + "',";
                query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }

            
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket Purchase Order Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket Purchase Order Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Blanket_PO_No"] = txtBlanketNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtBlanketNo.Text = ""; txtDate.Text = ""; txtSuppCodehide.Value = ""; txtSupplierName.Text = "";
        txtSuppRefDocNo.SelectedItem.Text = "-Select-"; txtDocDate.Text = ""; txtDeliveryMode.Value = "";
        txtDeliveryAt.Text = ""; txtPaymentMode.Value = ""; txtDepartmentName.SelectedItem.Text = "-Select-";
        txtPaymentTerms.Text = ""; txtDescription.Text = ""; txtNote.Text = ""; txtOthers.Text = "";
        txtItemCodeHide.Value = ""; txtItemName.Text = "";
        txtRequiredQty.Text = ""; txtOrderQty.Text = ""; txtRate.Text = "0.0"; RdpPOType.SelectedValue = "1";


        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Blanket_PO_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (txtOrderQty.Text == "0.0" || txtOrderQty.Text == "0" || txtOrderQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Order Qty...');", true);
        }
        if (txtRate.Text == "0.0" || txtRate.Text == "0" || txtRate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Rate...');", true);
        }
        ////Check Purchase Request No
        //query = "Select * from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "'";
        //qry_dt = objdata.RptEmployeeMultipleDetails(query);
        //if (qry_dt.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Purchase Request No..');", true);
        //}

        ////check with Item Code And Item Name 
        //query = "Select * from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + txtItemCodeHide.Value + "' And ItemName='" + txtItemName.Text + "' And Pur_Request_No='" + txtRequestNo.Text + "'";
        //qry_dt = objdata.RptEmployeeMultipleDetails(query);
        //if (qry_dt.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
        //}
        if (RdpPOType.SelectedValue == "1")
        {
            // Check item in item master

            query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCodeHide.Value + "' And PurchaseType='Normal'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Purchase Request No..');", true);
            }
            if (!ErrFlag)
            {
                //UOM Code get
                string UOM_Code_Str = qry_dt.Rows[0]["PurchaseUOM"].ToString();
                string Line_Total = "";
                Line_Total = ((Convert.ToDecimal(txtOrderQty.Text)) * Convert.ToDecimal(txtRate.Text)).ToString();
                Line_Total = (Math.Round(Convert.ToDecimal(Line_Total), 0, MidpointRounding.AwayFromZero)).ToString();

                // check view state is not null  
                if (ViewState["ItemTable"] != null)
                {
                    //get datatable from view state   
                    dt = (DataTable)ViewState["ItemTable"];

                    //check Item Already add or not
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                        }
                    }
                    if (!ErrFlag)
                    {


                        dr = dt.NewRow();

                        dr["ItemCode"] = txtItemCodeHide.Value;
                        dr["ItemName"] = txtItemName.Text;
                        dr["UOMCode"] = UOM_Code_Str;
                        dr["ReuiredQty"] = txtRequiredQty.Text;
                        dr["OrderQty"] = txtOrderQty.Text;
                        dr["Rate"] = txtRate.Text;
                        dr["LineTotal"] = Line_Total;
                        dr["Schedule"] = false;
                        
                        dt.Rows.Add(dr);
                        ViewState["ItemTable"] = dt;
                        Repeater1.DataSource = dt;
                        Repeater1.DataBind();

                        Totalsum();

                        txtItemCodeHide.Value = ""; txtItemName.Text = "";
                        txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRate.Text = "0.0";
                       
                    }
                }
                else
                {


                    dr = dt.NewRow();

                    dr["ItemCode"] = txtItemCodeHide.Value;
                    dr["ItemName"] = txtItemName.Text;
                    dr["UOMCode"] = UOM_Code_Str;
                    dr["ReuiredQty"] = txtRequiredQty.Text;
                    dr["OrderQty"] = txtOrderQty.Text;
                    dr["Rate"] = txtRate.Text;
                    dr["LineTotal"] = Line_Total;
                    dr["Schedule"] = false;
                    
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    Totalsum();

                    txtItemCodeHide.Value = ""; txtItemName.Text = "";
                    txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRate.Text = "0.0";
                    
                }
            }
        }

        else if (RdpPOType.SelectedValue == "2")
        {
            // Check item in item master

            query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCodeHide.Value + "' And PurchaseType='Special'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
            if (qry_dt.Rows.Count == 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Purchase Request No..');", true);
            }
            if (!ErrFlag)
            {
                //UOM Code get
                string UOM_Code_Str = qry_dt.Rows[0]["PurchaseUOM"].ToString();
                string Line_Total = "";
                Line_Total = ((Convert.ToDecimal(txtOrderQty.Text)) * Convert.ToDecimal(txtRate.Text)).ToString();
                Line_Total = (Math.Round(Convert.ToDecimal(Line_Total), 0, MidpointRounding.AwayFromZero)).ToString();

                // check view state is not null  
                if (ViewState["ItemTable"] != null)
                {
                    //get datatable from view state   
                    dt = (DataTable)ViewState["ItemTable"];

                    //check Item Already add or not
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                        }
                    }
                    if (!ErrFlag)
                    {
                        dr = dt.NewRow();

                        dr["ItemCode"] = txtItemCodeHide.Value;
                        dr["ItemName"] = txtItemName.Text;
                        dr["UOMCode"] = UOM_Code_Str;
                        dr["ReuiredQty"] = txtRequiredQty.Text;
                        dr["OrderQty"] = txtOrderQty.Text;
                        dr["Rate"] = txtRate.Text;
                        dr["LineTotal"] = Line_Total;
                        dr["Schedule"] = false;
                        
                        dt.Rows.Add(dr);
                        ViewState["ItemTable"] = dt;
                        Repeater1.DataSource = dt;
                        Repeater1.DataBind();

                        Totalsum();

                        txtItemCodeHide.Value = ""; txtItemName.Text = "";
                        txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0";  txtRate.Text = "0.0";
                       
                    }
                }
                else
                {
                    dr = dt.NewRow();

                    dr["ItemCode"] = txtItemCodeHide.Value;
                    dr["ItemName"] = txtItemName.Text;
                    dr["UOMCode"] = UOM_Code_Str;
                    dr["ReuiredQty"] = txtRequiredQty.Text;
                    dr["OrderQty"] = txtOrderQty.Text;
                    dr["Rate"] = txtRate.Text;
                    dr["LineTotal"] = Line_Total;
                    dr["Schedule"] = false;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    Totalsum();
                    
                    txtItemCodeHide.Value = ""; txtItemName.Text = "";
                    txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRate.Text = "0.0";
                    
                }
            }
        }
    }
    public void Totalsum()
    {
        sum = "0";
        OrderQty = 0;
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(dt.Rows[i]["LineTotal"])).ToString();
            txtTotAmt.Text = sum;
            OrderQty = Convert.ToDecimal(OrderQty) + Convert.ToDecimal(dt.Rows[i]["OrderQty"]);
        }
        
    }
    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredQty", typeof(string)));
        dt.Columns.Add(new DataColumn("OrderQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("Schedule", typeof(Boolean)));
        
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        Totalsum();

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + txtBlanketNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {

            if (Main_DT.Rows[0]["Blanket_PO_Status"].ToString() == "1")
            {
                lblStatus.Text = "Approved by: " + Main_DT.Rows[0]["Approvedby"].ToString();
            }
            else if (Main_DT.Rows[0]["Blanket_PO_Status"].ToString() == "2")
            {
                lblStatus.Text = "Cancelled by: " + Main_DT.Rows[0]["Approvedby"].ToString();
            }
            else if (Main_DT.Rows[0]["Blanket_PO_Status"].ToString() == "5" || Main_DT.Rows[0]["Blanket_PO_Status"].ToString() == "" || Main_DT.Rows[0]["Blanket_PO_Status"].ToString() == "")
            {
                lblStatus.Text = "Pending..";
            }
            else if (Main_DT.Rows[0]["Blanket_PO_Status"].ToString() == "6")
            {
                lblStatus.Text = "Rejected by FM..";
            }


            txtDate.Text = Main_DT.Rows[0]["Blanket_PO_Date"].ToString();
            txtSuppCodehide.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
            txtSupplierName.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtSuppRefDocNo.SelectedItem.Text = Main_DT.Rows[0]["Supp_Qtn_No"].ToString();
            txtDocDate.Text = Main_DT.Rows[0]["Supp_Qtn_Date"].ToString();
            txtDeliveryMode.Value = Main_DT.Rows[0]["DeliveryMode"].ToString();
            txtDeliveryAt.Text = Main_DT.Rows[0]["DeliveryAt"].ToString();
            txtPaymentMode.Value = Main_DT.Rows[0]["PaymentMode"].ToString();
            txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtPaymentTerms.Text = Main_DT.Rows[0]["PaymentTerms"].ToString();
            txtDescription.Text = Main_DT.Rows[0]["Description"].ToString();
            txtNote.Text = Main_DT.Rows[0]["Note"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["TotalAmt"].ToString();

            if (Main_DT.Rows[0]["Blanket_PO_Type"].ToString() == "Direct")
            {
                RdpPOType.SelectedValue = "1";
            }
            else if (Main_DT.Rows[0]["Blanket_PO_Type"].ToString() == "Special")
            {
                RdpPOType.SelectedValue = "2";
            }


            //Std_Purchase_Order_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select ItemCode,ItemName,UOMCode,ReuiredQty,OrderQty,Rate,LineTotal,Schedule from Blanket_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + txtBlanketNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
           

            btnSave.Text = "Update";
           
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("blanket_po_main.aspx");
    }

    private void Delivery_Mode_Add()
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        txtDeliveryMode.Items.Clear();
        query = "Select * from MstDeliveryMode where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeliveryMode Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDeliveryMode.Items.Add("-Select-");
        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {
            txtDeliveryMode.Items.Add(Main_DT.Rows[i]["DeliveryMode"].ToString());
        }
    }

    //protected void btnApprove_Click(object sender, EventArgs e)
    //{
    //    string query = "";
    //    DataTable DT_Check = new DataTable();
    //    bool ErrFlag = false;

    //    //User Rights Check Start
    //    bool Rights_Check = false;
    //    Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Blanket Purchase Order");
    //    if (Rights_Check == false)
    //    {
    //        ErrFlag = true;
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Blanket Purchase Order...');", true);
    //    }
    //    //User Rights Check End

    //    query = "Select * from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + txtBlanketNo.Text + "'";
    //    DT_Check = objdata.RptEmployeeMultipleDetails(query);
    //    if (DT_Check.Rows.Count == 0)
    //    {
    //        ErrFlag = true;
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Blanket Purchase Order Details..');", true);
    //    }

    //    query = "Select * from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + txtBlanketNo.Text + "' And Blanket_PO_Status='1'";
    //    DT_Check = objdata.RptEmployeeMultipleDetails(query);
    //    if (DT_Check.Rows.Count != 0)
    //    {
    //        ErrFlag = true;
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket Purchase Order Details Already Approved..');", true);
    //    }

    //    if (!ErrFlag)
    //    {
    //        query = "Update Blanket_Purchase_Order_Main set Blanket_PO_Status='1' where Ccode='" + SessionCcode + "'";
    //        query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
    //        query = query + "  And Blanket_PO_No='" + txtBlanketNo.Text + "'";
    //        objdata.RptEmployeeMultipleDetails(query);
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket Purchase Order Details Approved Successfully..');", true);
    //    }
    //}


    protected void btnSearchView_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + txtBlanketNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            
            txtDate.Text = Main_DT.Rows[0]["Blanket_PO_Date"].ToString();
            txtSuppCodehide.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
            txtSupplierName.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtSuppRefDocNo.SelectedItem.Text = Main_DT.Rows[0]["Supp_Qtn_No"].ToString();
            txtDocDate.Text = Main_DT.Rows[0]["Supp_Qtn_Date"].ToString();
            txtDeliveryMode.Value = Main_DT.Rows[0]["DeliveryMode"].ToString();
            txtDeliveryAt.Text = Main_DT.Rows[0]["DeliveryAt"].ToString();
            txtPaymentMode.Value = Main_DT.Rows[0]["PaymentMode"].ToString();
            txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtPaymentTerms.Text = Main_DT.Rows[0]["PaymentTerms"].ToString();
            txtDescription.Text = Main_DT.Rows[0]["Description"].ToString();
            txtNote.Text = Main_DT.Rows[0]["Note"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["TotalAmt"].ToString();
            if (Main_DT.Rows[0]["Blanket_PO_Type"].ToString() == "Direct")
            {
                RdpPOType.SelectedValue = "1";
            }
            else if (Main_DT.Rows[0]["Blanket_PO_Type"].ToString() == "Special")
            {
                RdpPOType.SelectedValue = "2";
            }
          

            //Std_Purchase_Order_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select ItemCode,ItemName,UOMCode,ReuiredQty,OrderQty,Rate,LineTotal,Schedule from Blanket_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + txtBlanketNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
            //Totalsum();

            btnSave.Visible = false;
        }
        else
        {
            Clear_All_Field();
        }
    }


    protected void btnSupp1_Click(object sender, EventArgs e)
    {
        modalPop_Supp1.Show();
    }
    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier1.DataSource = DT;
        Repeater_Supplier1.DataBind();
        Repeater_Supplier1.Visible = true;

    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
        txtSupplierName.Text = Convert.ToString(e.CommandName);


        txtSuppRefDocNo.SelectedItem.Text = "";
        txtDocDate.Text = "";

        Load_Data_Empty_SuppRefDocNo();
    }

    protected void btnSuppRefDocNo_Click(object sender, EventArgs e)
    {
        //modalPop_SuppRefDocNo.Show();
    }
    private void Load_Data_Empty_SuppRefDocNo()
    {
        
        string query = "";
        DataTable Main_DT = new DataTable();


        query = "Select QuotNo,QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And SuppCode='" + txtSuppCodehide.Value + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);



        txtSuppRefDocNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["QuotNo"] = "-Select-";
        //dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtSuppRefDocNo.DataTextField = "QuotNo";
        //txtDeptCode.DataValueField = "DeptCode";
        txtSuppRefDocNo.DataBind();

    }
    protected void txtSuppRefDocNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        txtDocDate.Text = "";
        if (txtSuppRefDocNo.SelectedItem.Text != "-Select-")
        {
            query = "Select QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + " And QuotNo='" + txtSuppRefDocNo.SelectedItem.Text + "'";
            Main_DT = objdata.RptEmployeeMultipleDetails(query);
        }
        if (Main_DT.Rows.Count != 0)
        {
            txtDocDate.Text = Main_DT.Rows[0]["QuotDate"].ToString();
        }
        else
        {
            txtDocDate.Text = "";
        }



    }
    protected void GridViewClick_SuppRefDocNo(object sender, CommandEventArgs e)
    {
        txtSuppRefDocNo.Text = Convert.ToString(e.CommandArgument);
        txtDocDate.Text = Convert.ToString(e.CommandName);
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        string ss = "";
        string PO_Type = "";
        string Request_No = "";

        PO_Type = RdpPOType.SelectedValue;
       
        
        DataTable DT = new DataTable();
       
        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");


        if (PO_Type == "1")
        {
            query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And PurchaseType='Normal'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            Repeater_ItemCode_Normal.DataSource = DT;
            Repeater_ItemCode_Normal.DataBind();
            Repeater_ItemCode_Normal.Visible = true;
          


        }
        else if (PO_Type == "2")
        {
            query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And PurchaseType='Special'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            Repeater_ItemCode_Normal.DataSource = DT;
            Repeater_ItemCode_Normal.DataBind();
            Repeater_ItemCode_Normal.Visible = true;
           

        }

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }

    protected void RdpPOType_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtItemCodeHide.Value = "";
        txtItemName.Text = "";
        Load_Data_Empty_ItemCode();
    }

}
