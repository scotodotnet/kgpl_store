﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Request_StandardOrder : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionStdPOOrderNo;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Unplanned Receipt";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Load_Data_Approval_Grid();

        //if (Session["Transaction_No"] == null)
        //{
        //    SessionStdPOOrderNo = "";
        //}
        //else
        //{
        //    SessionStdPOOrderNo = Session["Transaction_No"].ToString();
        //    //txtStdOrderNo.Text = SessionStdPOOrderNo;
        //    //btnSearch_Click(sender, e);

        //}
    }

    public void Load_Data_Approval_Grid()
    {
        string query = "";

        //Check Purchase Req Item and Purchase Order Item are same code start to update Purchase request approval
        DataTable dtd_REqNo = new DataTable();

        //if (!ErrFlag)
        //{
        query = "select Pur_Request_No from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "'";
        query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        
        dtd_REqNo = objdata.RptEmployeeMultipleDetails(query);
        
        if (dtd_REqNo.Rows.Count > 0)
        {
            for(int i=0;i<dtd_REqNo.Rows.Count;i++)
            {
            string Pur_Req_No = dtd_REqNo.Rows[i]["Pur_Request_No"].ToString();
            DataTable Std_Req_Apprv = new DataTable();

            DataTable Pur_Req_Apprv = new DataTable();

            //Check Purchase Req Item and Purchase Order Item are same code start

            if (Pur_Req_No != "")
            {

                query = "Select *from Std_Purchase_Order_Main_Sub where Pur_Request_No='" + Pur_Req_No + "'";
                Std_Req_Apprv = objdata.RptEmployeeMultipleDetails(query);

                string ss = Pur_Req_No.Substring(0, 3);

                if (ss == "PR/")
                {
                    query = "Select *from Pur_Request_Main_Sub where Pur_Request_No='" + Pur_Req_No + "'";
                    Pur_Req_Apprv = objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    query = "Select *from Pur_Request_Main_Sub where Amend_No='" + Pur_Req_No + "'";
                    Pur_Req_Apprv = objdata.RptEmployeeMultipleDetails(query);
                }


                if (Std_Req_Apprv.Rows.Count == Pur_Req_Apprv.Rows.Count)
                {
                    query = "Update Pur_Request_Approval set PO_Status='1' where Ccode='" + SessionCcode + "'";
                    query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                    query = query + "  And Transaction_No='" + Pur_Req_No + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    query = "Update Pur_Request_Approval set PO_Status='0' where Ccode='" + SessionCcode + "'";
                    query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                    query = query + "  And Transaction_No='" + Pur_Req_No + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
            }
        }
       }
        //Check Purchase Req Item and Purchase Order Item are same code end
        //}
        //Check Purchase Req Item and Purchase Order Item are same code end to update Purchase request approval 



        query = "";
        DataTable DT = new DataTable();
        query = "Select Transaction_No,Date,Type_Request_Amend,SupplierName,Approvedby from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '1' And (PO_Status = '0' or PO_Status is null) And (Type_Request_Amend = 'Request' or Type_Request_Amend = 'Amend')";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }



    protected void GridViewEnquiryClick(object sender, CommandEventArgs e)
    {
        string Enquiry_No_Str = e.CommandName.ToString();
        Session.Remove("Std_PO_No");
        Session.Remove("Transaction_No");
        Session["Transaction_No"] = Enquiry_No_Str;
        Response.Redirect("standard_po.aspx");
    }



}
