﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Order_general_po : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionGenPOOrderNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static string sum;
    static Decimal OrderQty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: General Purchase Order (Non-Inventory Items)";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_Data_Empty_Dept();
            Delivery_Mode_Add();
            Load_Data_GST();

            Load_Data_Empty_Supp1();
            Load_Data_Empty_ItemCode();

            Load_TaxData();
            if (Session["Gen_PO_No"] == null)
            {
                SessionGenPOOrderNo = "";
               
            }
            else
            {
                SessionGenPOOrderNo = Session["Gen_PO_No"].ToString();
                txtGenOrderNo.Text = SessionGenPOOrderNo;
                btnSearch_Click(sender, e);
               
            }
        }
        Load_OLD_data();
       
        
       
    }
    private void Load_Data_GST()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtGST_Type.Items.Clear();
        query = "Select *from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtGST_Type.DataSource = Main_DT;
        //DataRow dr = Main_DT.NewRow();
        //dr["GST_Type"] = "-Select-";
        //dr["GST_Type"] = "-Select-";
        //Main_DT.Rows.InsertAt(dr, 0);
        txtGST_Type.DataTextField = "GST_Type";
        txtGST_Type.DataValueField = "GST_Type";
        txtGST_Type.DataBind();

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }
        if (txtDeliveryMode.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Delivery Mode...');", true);
        }
        if (txtPaymentMode.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Payment Mode...');", true);
        }
        
        //Check Department Name
        query = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + txtDepartmentName.SelectedValue + "' And DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Department Name...');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "6", "2", "General Purchase Order");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify General Purchase Order Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "6", "2", "General Purchase Order");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New General Purchase Order..');", true);
            }
        }
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "General Purchase Order", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtGenOrderNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            query = "Select * from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Gen_PO_No='" + txtGenOrderNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Gen_PO_No='" + txtGenOrderNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from General_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Gen_PO_No='" + txtGenOrderNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into General_Purchase_Order_Main(Ccode,Lcode,FinYearCode,FinYearVal,Gen_PO_No,Gen_PO_Date,Supp_Code,Supp_Name,Supp_Qtn_No,";
            query = query + " Supp_Qtn_Date,DeliveryMode,DeliveryDate,DeliveryAt,PaymentMode,DeptCode,DeptName,PaymentTerms, Description,Note,";
            query = query + " Others,TotalOrderQty,TotalAmt,Discount_Per,Discount,TaxPer,TaxAmount,OtherCharge,ExciseDuty,PackingAmt,AddOrLess,NetAmount,UserID,";
            query = query + " UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
            query = query + " '" + txtGenOrderNo.Text + "','" + txtDate.Text + "','" + ddlSupplier.SelectedValue + "','" + ddlSupplier.SelectedItem.Text + "',";
            query = query + " '" + txtSuppRefDocNo.Text + "','" + txtDocDate.Text + "','" + txtDeliveryMode.Value + "','" + txtDeliveryDate.Text + "',";
            query = query + " '" + txtDeliveryAt.Text + "','" + txtPaymentMode.Value + "','" + txtDepartmentName.SelectedValue + "',";
            query = query + " '" + txtDepartmentName.SelectedItem.Text + "','" + txtPaymentTerms.Text + "','" + txtDescription.Text + "',";
            query = query + " '" + txtNote.Text + "','" + txtOthers.Text + "','" + OrderQty + "','" + txtTotAmt.Text + "','0','"+ txtTotDisc.Text +"','0','0','0','0',";
            query = query + " '" + txtPackingAmt.Text + "','" + txtAddOrLess.Text + "','" + txtNetAmt.Text + "','" + SessionUserID + "',";
            query = query + " '" + SessionUserName + "')";

            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into General_Purchase_Order_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Gen_PO_No,Gen_PO_Date,ItemCode,ItemName,UOMCode,";
                query = query + " OrderQty,Rate,Remarks,ItemTotal,Discount_Per,Discount,CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,";
                query = query + " IGSTPer,IGSTAmount,OtherCharge,LineTotal,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtGenOrderNo.Text + "',";
                query = query + " '" + txtDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["OrderQty"].ToString() + "','" + dt.Rows[i]["Rate"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["Remarks"].ToString() + "','" + dt.Rows[i]["ItemTotal"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["Discount_Per"].ToString() + "','" + dt.Rows[i]["Discount"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["CGSTPer"].ToString() + "','" + dt.Rows[i]["CGSTAmount"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["SGSTPer"].ToString() + "','" + dt.Rows[i]["SGSTAmount"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["IGSTPer"].ToString() + "','" + dt.Rows[i]["IGSTAmount"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["OtherCharge"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["LineTotal"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Insert Purchase Order Approval Table
            query = "Insert Into Std_Purchase_Order_Approve(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,TotalAmt,UserID,UserName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtGenOrderNo.Text + "','" + txtDate.Text + "','Gen/PO','" + ddlSupplier.SelectedItem.Text + "','" + OrderQty + "','" + txtNetAmt.Text + "',";
            query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('General Purchase Order Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('General Purchase Order Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Gen_PO_No"] = txtGenOrderNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtGenOrderNo.Text = ""; txtDate.Text = ""; ddlSupplier.SelectedValue = "-Select-"; ddlSupplier.SelectedItem.Text = "-Select-";
        txtSuppRefDocNo.Text = ""; txtDocDate.Text = ""; txtDeliveryMode.Value = ""; txtDeliveryDate.Text = "";
        txtDeliveryAt.Text = ""; txtPaymentMode.Value = ""; txtDepartmentName.SelectedItem.Text = "-Select-";
        txtPaymentTerms.Text = ""; txtDescription.Text = ""; txtNote.Text = ""; txtOthers.Text = "";
        ddlItemName.SelectedItem.Text = "-Select-";txtOrderQty.Text = ""; txtRate.Text = "0.0";
        txtRemarks.Text = "";
        txtTotAmt.Text = "0.0";
        txtDiscount_Per.Text = "0.00";
        txtDiscount_Amount.Text = "0.0";
        txtTax.Text = "0"; txtTaxAmt.Text = "0.00";
        txtOtherCharge.Text = "0"; txtExciseDutyAmt.Text = "0.00";
        txtNetAmt.Text = "0";

        txtAddOrLess.Text = "0.00";

        txtItemTotal.Text = "0.00";
        txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
        txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
        txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
        txtFinal_Total_Amt.Text = "0.00";


        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Gen_PO_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (txtOrderQty.Text == "0.0" || txtOrderQty.Text == "0" || txtOrderQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Order Qty...');", true);
        }
        if (txtRate.Text == "0.0" || txtRate.Text == "0" || txtRate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Rate...');", true);
        }
        
        //check with Item Code And Item Name 
        query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And ItemShortName='" + ddlItemName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
        }

        if (!ErrFlag)
        {
            //UOM Code get
            string UOM_Code_Str = qry_dt.Rows[0]["PurchaseUOM"].ToString();
            string Line_Total = "";
            Line_Total = ((Convert.ToDecimal(txtOrderQty.Text)) * Convert.ToDecimal(txtRate.Text)).ToString();
            Line_Total = (Math.Round(Convert.ToDecimal(Line_Total), 0, MidpointRounding.AwayFromZero)).ToString();

            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == ddlItemName.SelectedValue.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["ItemCode"] = ddlItemName.SelectedValue;
                    dr["ItemName"] = ddlItemName.SelectedItem.Text;
                    dr["UOMCode"] = UOM_Code_Str;
                    dr["OrderQty"] = txtOrderQty.Text;
                    dr["Rate"] = txtRate.Text;
                    dr["Remarks"] = txtRemarks.Text;



                    dr["ItemTotal"] = txtItemTotal.Text;
                    dr["Discount_Per"] = txtDiscount_Per.Text;
                    dr["Discount"] = txtDiscount_Amount.Text;
                    dr["TaxPer"] = txtTax.Text;
                    dr["TaxAmount"] = txtTaxAmt.Text;
                    dr["CGSTPer"] = txtCGSTPer.Text;
                    dr["CGSTAmount"] = txtCGSTAmt.Text;
                    dr["SGSTPer"] = txtSGSTPer.Text;
                    dr["SGSTAmount"] = txtSGSTAmt.Text;
                    dr["IGSTPer"] = txtIGSTPer.Text;
                    dr["IGSTAmount"] = txtIGSTAmt.Text;
                    dr["OtherCharge"] = txtOtherCharge.Text;
                    dr["LineTotal"] = txtFinal_Total_Amt.Text;


                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    //sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(Line_Total)).ToString();
                    //txtTotAmt.Text = sum;
                    Totalsum();

                    ddlItemName.Text = "-Select-"; txtOrderQty.Text = "0.0"; txtRate.Text = "0.0";
                    txtRemarks.Text = "";

                    txtItemTotal.Text = "0.00";
                    txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                    txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                    //txtCGSTPer.Text = "0.0"; txtSGSTPer.Text = "0.0"; txtIGSTPer.Text = "0.0";
                    txtCGSTAmt.Text = "0.00"; txtSGSTAmt.Text = "0.00"; txtIGSTAmt.Text = "0.00";
                    txtFinal_Total_Amt.Text = "0.00";
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["ItemCode"] = ddlItemName.SelectedValue;
                dr["ItemName"] = ddlItemName.SelectedItem.Text;
                dr["UOMCode"] = UOM_Code_Str;
                dr["OrderQty"] = txtOrderQty.Text;
                dr["Rate"] = txtRate.Text;
                dr["Remarks"] = txtRemarks.Text;

                dr["ItemTotal"] = txtItemTotal.Text;
                dr["Discount_Per"] = txtDiscount_Per.Text;
                dr["Discount"] = txtDiscount_Amount.Text;
                dr["TaxPer"] = txtTax.Text;
                dr["TaxAmount"] = txtTaxAmt.Text;
                dr["CGSTPer"] = txtCGSTPer.Text;
                dr["CGSTAmount"] = txtCGSTAmt.Text;
                dr["SGSTPer"] = txtSGSTPer.Text;
                dr["SGSTAmount"] = txtSGSTAmt.Text;
                dr["IGSTPer"] = txtIGSTPer.Text;
                dr["IGSTAmount"] = txtIGSTAmt.Text;
                dr["OtherCharge"] = txtOtherCharge.Text;
                dr["LineTotal"] = txtFinal_Total_Amt.Text;


                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                //sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(Line_Total)).ToString();
                //txtTotAmt.Text = sum;
                Totalsum();

                ddlItemName.Text = "-Select-"; txtOrderQty.Text = "0.0"; txtRate.Text = "0.0";
                txtRemarks.Text = "";

                txtItemTotal.Text = "0.00";
                txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                //txtCGSTPer.Text = "0.0"; txtSGSTPer.Text = "0.0"; txtIGSTPer.Text = "0.0";
                txtCGSTAmt.Text = "0.00";  txtSGSTAmt.Text = "0.00";  txtIGSTAmt.Text = "0.00";
                txtFinal_Total_Amt.Text = "0.00";
            }
        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("OrderQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        //dt.Columns.Add(new DataColumn("Discount", typeof(string)));
        //dt.Columns.Add(new DataColumn("Tax", typeof(string)));
        //dt.Columns.Add(new DataColumn("OtherCharge", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));

        dt.Columns.Add(new DataColumn("ItemTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("Discount_Per", typeof(string)));
        dt.Columns.Add(new DataColumn("Discount", typeof(string)));
        dt.Columns.Add(new DataColumn("TaxPer", typeof(string)));
        dt.Columns.Add(new DataColumn("TaxAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("BDUTaxPer", typeof(string)));
        dt.Columns.Add(new DataColumn("BDUTaxAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("OtherCharge", typeof(string)));
        
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));





        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        Totalsum();

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Gen_PO_No='" + txtGenOrderNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            if (Main_DT.Rows[0]["PO_Status"].ToString() == "1")
            {
                lblStatus.Text = "Approved by: " + Main_DT.Rows[0]["Approvedby"].ToString();
            }
            else if (Main_DT.Rows[0]["PO_Status"].ToString() == "2")
            {
                lblStatus.Text = "Cancelled by: " + Main_DT.Rows[0]["Approvedby"].ToString();
            }
            else
            {
                lblStatus.Text = "Pending..";
            }



            txtDate.Text = Main_DT.Rows[0]["Gen_PO_Date"].ToString();
            ddlSupplier.SelectedValue = Main_DT.Rows[0]["Supp_Code"].ToString();
            ddlSupplier.SelectedItem.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtSuppRefDocNo.Text = Main_DT.Rows[0]["Supp_Qtn_No"].ToString();
            txtDocDate.Text = Main_DT.Rows[0]["Supp_Qtn_Date"].ToString();
            txtDeliveryMode.Value = Main_DT.Rows[0]["DeliveryMode"].ToString();
            txtDeliveryDate.Text = Main_DT.Rows[0]["DeliveryDate"].ToString();
            txtDeliveryAt.Text = Main_DT.Rows[0]["DeliveryAt"].ToString();
            txtPaymentMode.Value = Main_DT.Rows[0]["PaymentMode"].ToString();
            txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
            //txtDeptCodeHide.Value = Main_DT.Rows[0]["DeptCode"].ToString();
            //txtDepartmentName.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtPaymentTerms.Text = Main_DT.Rows[0]["PaymentTerms"].ToString();
            txtDescription.Text = Main_DT.Rows[0]["Description"].ToString();
            txtNote.Text = Main_DT.Rows[0]["Note"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["TotalAmt"].ToString();
            //txtDiscount_Per.Text = Main_DT.Rows[0]["Discount_Per"].ToString();
            
            //txtTax.Text = Main_DT.Rows[0]["TaxPer"].ToString();
            //txtTaxAmt.Text = Main_DT.Rows[0]["TaxAmount"].ToString();
            //txtOtherCharge.Text = Main_DT.Rows[0]["OtherCharge"].ToString();
            txtPackingAmt.Text = Main_DT.Rows[0]["PackingAmt"].ToString();
            txtTotDisc.Text = Main_DT.Rows[0]["Discount"].ToString();
            txtAddOrLess.Text = Main_DT.Rows[0]["AddOrLess"].ToString();
            txtNetAmt.Text = Main_DT.Rows[0]["NetAmount"].ToString();

            //Std_Purchase_Order_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select *from General_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Gen_PO_No='" + txtGenOrderNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            Totalsum();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("general_po_main.aspx");
    }

    private void Delivery_Mode_Add()
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        txtDeliveryMode.Items.Clear();
        query = "Select * from MstDeliveryMode where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeliveryMode Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDeliveryMode.Items.Add("-Select-");
        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {
            txtDeliveryMode.Items.Add(Main_DT.Rows[i]["DeliveryMode"].ToString());
        }
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "General Purchase Order");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve General Purchase Order...');", true);
        //}
        //User Rights Check End

        query = "Select * from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Gen_PO_No='" + txtGenOrderNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this General Purchase Order Details..');", true);
        }

        query = "Select * from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Gen_PO_No='" + txtGenOrderNo.Text + "' And PO_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('General Purchase Order Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            query = "Update General_Purchase_Order_Main set PO_Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And Gen_PO_No='" + txtGenOrderNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('General Purchase Order Details Approved Successfully..');", true);
        }
    }

    protected void txtTax_TextChanged(object sender, EventArgs e)
    {
        if (txtTax.Text != "") { Total_Calculate(); }
        //txtTaxAmt.Text = (Convert.ToDecimal(txtTotAmt.Text) * (Convert.ToDecimal(txtTax.Text) / 100)).ToString();
        //txtNetAmt.Text = ((Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
    }
    


    public void Totalsum()
    {
        sum = "0";
        OrderQty = 0;
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(dt.Rows[i]["LineTotal"])).ToString();
            txtTotAmt.Text = sum;
            OrderQty = Convert.ToDecimal(OrderQty) + Convert.ToDecimal(dt.Rows[i]["OrderQty"]);
        }
        Final_Total_Calculate();
        //txtNetAmt.Text = ((Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
    }                        
    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        
        ddlSupplier.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["SuppName"] = "-Select-";
        dr["SuppCode"] = "-Select-";
        DT.Rows.InsertAt(dr,0);
        ddlSupplier.DataTextField = "SuppName";
        ddlSupplier.DataValueField = "SuppCode";
        ddlSupplier.DataBind();
      
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
       // txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlItemName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);
        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataValueField = "ItemCode";

        ddlItemName.DataBind();
    }
    protected void txtGST_Type_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TaxData();
    }

    private void Load_TaxData()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GST_Type='" + txtGST_Type.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtCGSTPer.Text = DT.Rows[0]["CGST_Percent"].ToString();
            txtSGSTPer.Text = DT.Rows[0]["SGST_Percent"].ToString();
            txtIGSTPer.Text = DT.Rows[0]["IGST_Percent"].ToString();
        }
        else
        {
            txtCGSTPer.Text = "0";
            txtSGSTPer.Text = "0";
            txtIGSTPer.Text = "0";
        }

        Total_Calculate();
    }
    protected void txtOtherCharge_TextChanged(object sender, EventArgs e)
    {
        Total_Calculate();
    }

    private void Total_Calculate()
    {
        string Qty_Val = "0";
        string Item_Rate = "0";
        string Item_Total = "0";
        string Discount_Percent = "0";
        string Discount_Amt = "0";
        string Tax_Per = "0";
        string Tax_Amt = "0";
        string CGST_Per = "0";
        string CGST_Amt = "0";
        string SGST_Per = "0";
        string SGST_Amt = "0";
        string IGST_Per = "0";
        string IGST_Amt = "0";
        string BDUTax_Per = "0";
        string BDUTax_Amt = "0";
        string Other_Charges = "0";
        string Final_Amount = "0";
        string PackingAmt = "0";
        if (txtOrderQty.Text != "") { Qty_Val = txtOrderQty.Text.ToString(); }
        if (txtRate.Text != "") { Item_Rate = txtRate.Text.ToString(); }


        Item_Total = (Convert.ToDecimal(Qty_Val) * Convert.ToDecimal(Item_Rate)).ToString();
        Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();
        if (Convert.ToDecimal(Item_Total) != 0)
        {
            if (Convert.ToDecimal(txtDiscount_Per.Text.ToString()) != 0) { Discount_Percent = txtDiscount_Per.Text.ToString(); }
            if (Convert.ToDecimal(txtOtherCharge.Text.ToString()) != 0) { Other_Charges = txtOtherCharge.Text.ToString(); }
            if (Convert.ToDecimal(txtCGSTPer.Text.ToString()) != 0) { CGST_Per = txtCGSTPer.Text.ToString(); }
            if (Convert.ToDecimal(txtSGSTPer.Text.ToString()) != 0) { SGST_Per = txtSGSTPer.Text.ToString(); }
            if (Convert.ToDecimal(txtIGSTPer.Text.ToString()) != 0) { IGST_Per = txtIGSTPer.Text.ToString(); }



            //Discount Amt Calculate
            if (Convert.ToDecimal(Discount_Percent) != 0)
            {
                Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                Discount_Amt = "0.00";
            }

            if (Convert.ToDecimal(CGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                CGST_Amt = "0.00";
            }

            if (Convert.ToDecimal(SGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                SGST_Amt = "0.00";
            }

            if (Convert.ToDecimal(IGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                IGST_Amt = "0.00";
            }





            //Other Charges
            if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

            //Final Amt
            Final_Amount = ((Convert.ToDecimal(Item_Total)) - Convert.ToDecimal(Discount_Amt)).ToString();
            //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
            Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
            Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 2, MidpointRounding.AwayFromZero)).ToString();

            txtItemTotal.Text = Item_Total;
            txtDiscount_Amount.Text = Discount_Amt;
            txtTaxAmt.Text = Tax_Amt;
            txtCGSTAmt.Text = CGST_Amt;
            txtSGSTAmt.Text = SGST_Amt;
            txtIGSTAmt.Text = IGST_Amt;
            txtFinal_Total_Amt.Text = Final_Amount;
            txtNetAmt.Text = Final_Amount;



        }
    }
 
    protected void txtDiscount_Per_TextChanged(object sender, EventArgs e)
    {
        Total_Calculate();
    }
   
    private void Final_Total_Calculate()
    {

        string Final_NetAmt = "0";
        string AddorLess = "0";
        string PackingAmt = "0";
        string TotDisc = "0";
        string Item_Total_Amt = txtTotAmt.Text;

        if (Convert.ToDecimal(txtAddOrLess.Text.ToString()) != 0) { AddorLess = txtAddOrLess.Text.ToString(); }
        if (Convert.ToDecimal(txtPackingAmt.Text.ToString()) != 0) { PackingAmt = txtPackingAmt.Text.ToString(); }
        if (Convert.ToDecimal(txtTotDisc.Text.ToString()) != 0) { TotDisc = txtTotDisc.Text.ToString(); }

        Final_NetAmt = ((Convert.ToDecimal(PackingAmt) + Convert.ToDecimal(AddorLess) + Convert.ToDecimal(Item_Total_Amt))-Convert.ToDecimal(TotDisc)).ToString();
        //txtFinal_Total_Amt.Text = Final_NetAmt;
        txtNetAmt.Text = Final_NetAmt;

    }

    protected void txtAddOrLess_TextChanged(object sender, EventArgs e)
    {
        if (txtRate.Text.ToString() != "") { Total_Calculate(); }
        if (txtAddOrLess.Text.ToString() != "") { Final_Total_Calculate(); }

    }
   
    protected void txtOrderQty_TextChanged(object sender, EventArgs e)
    {
        if (txtOrderQty.Text.ToString() != "") { Total_Calculate(); }
    }
    protected void txtRate_TextChanged(object sender, EventArgs e)
    {
        if (txtRate.Text.ToString() != "") { Total_Calculate(); }
    }

    protected void txtPackingAmt_TextChanged(object sender, EventArgs e)
    {
        if (chkPacking.Checked==true)
        {

        }
        else
        {
            Final_Total_Calculate();
        }
    }

    protected void txtTotDisc_TextChanged(object sender, EventArgs e)
    {
        Final_Total_Calculate();
    }
}
