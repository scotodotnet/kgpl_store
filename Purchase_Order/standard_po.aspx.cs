﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Purchase_Order_standard_po : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionStdPOOrderNo;
    string SessionStdPOOrderNot;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static string sum;
    static Decimal OrderQty;
    string NameTypeRequest;
    string SessionPurRequestNoApproval;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
       
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Standard Purchase Order";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Delivery_Mode_Add();
            Load_Data_Empty_SuppRefDocNo();
            Load_Data_Empty_Dept();
            Load_Data_GST();
            Load_TaxData();

            if (Session["Std_PO_No"] == null)
            {
                SessionStdPOOrderNo = "";
               
                RdpPOType.SelectedValue = "1";
            }
            else
            {
                SessionStdPOOrderNo = Session["Std_PO_No"].ToString();
                txtStdOrderNo.Text = SessionStdPOOrderNo;
                
                btnSearch_Click(sender, e);
                
               
            }
            if (Session["Transaction_No"] == null)
            {
                SessionStdPOOrderNot = "";
              
                //RdpPOType.SelectedValue = "1";
            }
            else
            {
                SessionStdPOOrderNot = Session["Transaction_No"].ToString();
                btnStd_Purchase_Click(sender, e);
             
                RdpPOType.SelectedValue = "2";
            }

            if (Session["Pur_RequestNo_Approval"] == null)
            {
                SessionPurRequestNoApproval = "";
               
            }
            else
            {
                RdpPOType.SelectedValue = "3";
                SessionPurRequestNoApproval = Session["Pur_RequestNo_Approval"].ToString();
                txtStdOrderNo.Text = SessionPurRequestNoApproval;
                btnSearchView_Click(sender, e);

              
                
            }


            if (RdpPOType.SelectedValue == "1" || RdpPOType.SelectedValue == "3")
            {
                txtRequestNo.Enabled = false;
                txtRequestDate.Enabled = false;
            }

        }
        Load_OLD_data();
        Load_Data_Empty_Supp1();
        
       
        Load_Data_Empty_ItemCode();
    }

    private void Load_Data_GST()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtGST_Type.Items.Clear();
        query = "Select *from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtGST_Type.DataSource = Main_DT;
        //DataRow dr = Main_DT.NewRow();
        //dr["GST_Type"] = "-Select-";
        //dr["GST_Type"] = "-Select-";
        //Main_DT.Rows.InsertAt(dr, 0);
        txtGST_Type.DataTextField = "GST_Type";
        txtGST_Type.DataValueField = "GST_Type";
        txtGST_Type.DataBind();

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }
        if (txtDeliveryMode.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Delivery Mode...');", true);
        }
        if (txtPaymentMode.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Payment Mode...');", true);
        }
      
        //Check Supplier Name And Quotation No
        query = "Select * from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And QuotNo='" + txtSuppRefDocNo.Text + "' And SuppCode='" + txtSuppCodehide.Value + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        //if (qry_dt.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Supplier Name And Supp.Ref.Doc.No...');", true);
        //}
        //Check Department Name
        query = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + txtDepartmentName.SelectedValue + "' And DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Department Name...');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "6", "2", "Std Purchase Order");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Std Purchase Order Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "6", "2", "Std Purchase Order");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Std Purchase Order..');", true);
            }
        }
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Standard Purchase Order", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtStdOrderNo.Text = Auto_Transaction_No;
                }
            }
        }


        if (btnSave.Text == "Update")
        {
            query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "' And PO_Status='1'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Standard Purchase Order Details get Approved Cannot Update it..');", true);
            }
        }


        if (!ErrFlag)
        {
            query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);

                query = "Delete from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + txtStdOrderNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);


            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into Std_Purchase_Order_Main(Ccode,Lcode,FinYearCode,FinYearVal,Std_PO_No,Std_PO_Date,Supp_Code,Supp_Name,Supp_Qtn_No,Supp_Qtn_Date,Pur_Request_No,Pur_Request_Date,DeliveryMode,";
            query = query + " DeliveryDate,DeliveryAt,PaymentMode,DeptCode,DeptName,PaymentTerms,Description,Note,Others,TotalQuantity,TotalAmt,Discount,TaxPer,TaxAmount,OtherCharge,AddOrLess,NetAmount,UserID,UserName,PO_Type,PO_Status) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtStdOrderNo.Text + "','" + txtDate.Text + "','" + txtSuppCodehide.Value + "','" + txtSupplierName.Text + "',";
            query = query + " '" + txtSuppRefDocNo.Text + "','" + txtDocDate.Text + "','"+ txtRequestNo.Text +"','"+ txtRequestDate.Text + "','" + txtDeliveryMode.Value + "','" + txtDeliveryDate.Text + "','" + txtDeliveryAt.Text + "',";
            query = query + " '" + txtPaymentMode.Value + "','" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "','" + txtPaymentTerms.Text + "',";
            query = query + " '" + txtDescription.Text + "','" + txtNote.Text + "','" + txtOthers.Text + "','"+ OrderQty +"','" + txtTotAmt.Text + "','0','0','0','0','" + txtAddOrLess.Text + "','"  + txtNetAmt.Text + "','" + SessionUserID + "','" + SessionUserName + "','" + RdpPOType.SelectedItem.Text + "',";
            if(RdpPOType.SelectedItem.Text=="Special")
            {
                query = query + " '4')";
            }
            else
            {
                query = query + " '0')";
            }
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Std_Purchase_Order_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Std_PO_No,Std_PO_Date,Pur_Request_No,Pur_Request_Date,ItemCode,ItemName,UOMCode,ReuiredQty,OrderQty,ReuiredDate,Rate,Remarks,ItemTotal,Discount_Per,Discount,";
                query = query + "CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,IGSTPer,IGSTAmount,OtherCharge,LineTotal,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtStdOrderNo.Text + "','" + txtDate.Text + "','" + txtRequestNo.Text + "','" + txtRequestDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["ReuiredQty"].ToString() + "','" + dt.Rows[i]["OrderQty"].ToString() + "','" + dt.Rows[i]["ReuiredDate"].ToString() + "','" + dt.Rows[i]["Rate"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["Remarks"].ToString() + "','" + dt.Rows[i]["ItemTotal"].ToString() + "','" + dt.Rows[i]["Discount_Per"].ToString() + "','" + dt.Rows[i]["Discount"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["CGSTPer"].ToString() + "','" + dt.Rows[i]["CGSTAmount"].ToString() + "','" + dt.Rows[i]["SGSTPer"].ToString() + "','" + dt.Rows[i]["SGSTAmount"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["IGSTPer"].ToString() + "','" + dt.Rows[i]["IGSTAmount"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["OtherCharge"].ToString() + "','" + dt.Rows[i]["LineTotal"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            if (RdpPOType.SelectedItem.Text == "Special")
            {
                //Insert Purchase Request Approval Table
                query = "Insert Into Pur_Request_Approval(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_Request_Amend,SupplierName,TotalQuantity,TotalAmt,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtStdOrderNo.Text + "','" + txtDate.Text + "','Std/PO','" + txtSupplierName.Text + "','" + OrderQty + "','" + txtNetAmt.Text + "',";
                query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            if (RdpPOType.SelectedItem.Text == "Direct" || RdpPOType.SelectedItem.Text == "Req/Amend")
            {
                //Insert Purchase Request Approval Table
                query = "Insert Into Std_Purchase_Order_Approve(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,TotalAmt,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtStdOrderNo.Text + "','" + txtDate.Text + "','Std/PO','" + txtSupplierName.Text + "','" + OrderQty + "','" + txtNetAmt.Text + "',";
                query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }



            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Standard Purchase Order Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Standard Purchase Order Details Updated Successfully');", true);
            }


            DataTable dtd_REqNo = new DataTable();

            //if (!ErrFlag)
            //{
            query = "select Pur_Request_No from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And Std_PO_No='" + txtStdOrderNo.Text + "'";

            dtd_REqNo = objdata.RptEmployeeMultipleDetails(query);

            if (dtd_REqNo.Rows.Count > 0)
            {
                string Pur_Req_No = dtd_REqNo.Rows[0]["Pur_Request_No"].ToString();
                DataTable Std_Req_Apprv = new DataTable();

                DataTable Pur_Req_Apprv = new DataTable();

                //Check Purchase Req Item and Purchase Order Item are same code start

                if (Pur_Req_No != "")
                {

                    query = "Select *from Std_Purchase_Order_Main_Sub where Pur_Request_No='" + Pur_Req_No + "'";
                    Std_Req_Apprv = objdata.RptEmployeeMultipleDetails(query);

                    string ss = Pur_Req_No.Substring(0, 3);

                    if (ss == "PR/")
                    {
                        query = "Select *from Pur_Request_Main_Sub where Pur_Request_No='" + Pur_Req_No + "'";
                        Pur_Req_Apprv = objdata.RptEmployeeMultipleDetails(query);
                    }
                    else
                    {
                        query = "Select *from Pur_Request_Main_Sub where Amend_No='" + Pur_Req_No + "'";
                        Pur_Req_Apprv = objdata.RptEmployeeMultipleDetails(query);
                    }


                    if (Std_Req_Apprv.Rows.Count == Pur_Req_Apprv.Rows.Count)
                    {
                        query = "Update Pur_Request_Approval set PO_Status='1' where Ccode='" + SessionCcode + "'";
                        query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                        query = query + "  And Transaction_No='" + Pur_Req_No + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                    }
                    else
                    {
                        query = "Update Pur_Request_Approval set PO_Status='0' where Ccode='" + SessionCcode + "'";
                        query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                        query = query + "  And Transaction_No='" + Pur_Req_No + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                    }
                }

            }
            //Check Purchase Req Item and Purchase Order Item are same code end
            //}




            //Clear_All_Field();
            Session["Std_PO_No"] = txtStdOrderNo.Text;
            btnSave.Text = "Update";
            //Session["Transaction_No"] = txtStdOrderNo.Text;
            //Load_Data_Enquiry_Grid();

        }

       
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtStdOrderNo.Text = ""; txtDate.Text = ""; txtSuppCodehide.Value = ""; txtSupplierName.Text = "";
        txtSuppRefDocNo.SelectedItem.Text = "-Select-"; txtDocDate.Text = ""; txtDeliveryMode.Value = ""; txtDeliveryDate.Text = "";
        txtDeliveryAt.Text = ""; txtPaymentMode.Value = ""; txtDepartmentName.SelectedItem.Text = "-Select-";
        txtPaymentTerms.Text = ""; txtDescription.Text = ""; txtNote.Text = ""; txtOthers.Text = "";
        txtRequestNo.Text = ""; txtRequestDate.Text = ""; txtItemCodeHide.Value = ""; txtItemName.Text = "";
        txtRequiredQty.Text = ""; txtOrderQty.Text = ""; txtRequiredDate.Text = ""; txtRate.Text = "0.0";
        txtDiscount.Text = ""; txtTax.Text = "0"; txtOtherCharge.Text = "0"; txtRemarks.Text = "";
        txtTaxAmt.Text = "0"; txtTotAmt.Text = "0.0"; txtNetAmt.Text = "0"; RdpPOType.SelectedValue = "1";

        txtAddOrLess.Text = "0";

        txtItemTotal.Text = "0.00";
        txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
        txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
        txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
        txtFinal_Total_Amt.Text = "0.00";


        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Std_PO_No");
        Session.Remove("Transaction_No");
        Session.Remove("Pur_RequestNo_Approval");
        //Load_Data_Enquiry_Grid();
        //txtDeptCodeHide.Value = "";
        if (RdpPOType.SelectedValue == "1")
        {
            txtRequestNo.Enabled = false;
            txtRequestDate.Enabled = false;
        }

    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (txtOrderQty.Text == "0.0" || txtOrderQty.Text == "0" || txtOrderQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Order Qty...');", true);
        }
        if (txtRate.Text == "0.0" || txtRate.Text == "0" || txtRate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Rate...');", true);
        }

        if (!ErrFlag)
        {

            if (RdpPOType.SelectedValue == "2")
            {
                // Get RequestType from Type_Request_Amend

                if (txtRequestNo.Text != "")
                {
                    query = "select Type_Request_Amend from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + txtRequestNo.Text + "'";
                    qry_dt = objdata.RptEmployeeMultipleDetails(query);
                    if (qry_dt.Rows.Count != 0)
                    {
                        NameTypeRequest = qry_dt.Rows[0]["Type_Request_Amend"].ToString();
                    }
                }

                // Check RequestType = Request

                if (NameTypeRequest.Trim() == "Request")
                {
                    query = "Select * from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + txtItemCodeHide.Value + "' And ItemName='" + txtItemName.Text + "'";
                    query = query + "And Pur_Request_No='" + txtRequestNo.Text + "'";
                    qry_dt = objdata.RptEmployeeMultipleDetails(query);

                    if (qry_dt.Rows.Count != 0)
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details Already Exists in Purchase Order..');", true);
                        txtItemCodeHide.Value = ""; txtItemName.Text = "";
                        txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRate.Text = "0.0";
                        txtRemarks.Text = "";

                    }

                    query = "Select * from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "'";
                    qry_dt = objdata.RptEmployeeMultipleDetails(query);
                    if (qry_dt.Rows.Count == 0)
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Purchase Request No..');", true);
                    }

                    //check with Item Code And Item Name

                    query = "Select * from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + txtItemCodeHide.Value + "' And ItemName='" + txtItemName.Text + "'";
                    query = query + "And Pur_Request_No='" + txtRequestNo.Text + "'";
                    qry_dt = objdata.RptEmployeeMultipleDetails(query);
                    if (qry_dt.Rows.Count == 0)
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
                    }

                }

                // Check RequestType = Amend

                else if (NameTypeRequest.Trim() == "Amend")
                {
                    query = "Select * from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + txtItemCodeHide.Value + "' And ItemName='" + txtItemName.Text + "'";
                    query = query + "And Pur_Request_No='" + txtRequestNo.Text + "'";
                    qry_dt = objdata.RptEmployeeMultipleDetails(query);

                    if (qry_dt.Rows.Count != 0)
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details Already Exists in Purchase Order..');", true);
                        txtItemCodeHide.Value = ""; txtItemName.Text = "";
                        txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRate.Text = "0.0";
                        txtRemarks.Text = "";
                    }
                    query = "Select * from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + txtItemCodeHide.Value + "' And ItemName='" + txtItemName.Text + "'";
                    query = query + "And Amend_No='" + txtRequestNo.Text + "'";
                    qry_dt = objdata.RptEmployeeMultipleDetails(query);
                    if (qry_dt.Rows.Count == 0)
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
                    }


                }

                if (!ErrFlag)
                {
                    //UOM Code get
                    string UOM_Code_Str = qry_dt.Rows[0]["UOMCode"].ToString();
                    string Line_Total = "";
                    Line_Total = ((Convert.ToDecimal(txtOrderQty.Text)) * Convert.ToDecimal(txtRate.Text)).ToString();
                    Line_Total = (Math.Round(Convert.ToDecimal(Line_Total), 0, MidpointRounding.AwayFromZero)).ToString();

                    // check view state is not null  
                    if (ViewState["ItemTable"] != null)
                    {
                        //get datatable from view state   
                        dt = (DataTable)ViewState["ItemTable"];

                        //check Item Already add or not
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                            {
                                ErrFlag = true;
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                            }
                        }
                        if (!ErrFlag)
                        {
                            dr = dt.NewRow();

                            dr["ItemCode"] = txtItemCodeHide.Value;
                            dr["ItemName"] = txtItemName.Text;
                            dr["UOMCode"] = UOM_Code_Str;
                            dr["ReuiredQty"] = txtRequiredQty.Text;
                            dr["OrderQty"] = txtOrderQty.Text;
                            dr["ReuiredDate"] = txtRequiredDate.Text;
                            dr["Rate"] = txtRate.Text;
                            dr["Remarks"] = txtRemarks.Text;


                            dr["ItemTotal"] = txtItemTotal.Text;
                            dr["Discount_Per"] = txtDiscount_Per.Text;
                            dr["Discount"] = txtDiscount_Amount.Text;
                            dr["TaxPer"] = txtTax.Text;
                            dr["TaxAmount"] = txtTaxAmt.Text;
                            dr["CGSTPer"] = txtCGSTPer.Text;
                            dr["CGSTAmount"] = txtCGSTAmt.Text;
                            dr["SGSTPer"] = txtSGSTPer.Text;
                            dr["SGSTAmount"] = txtSGSTAmt.Text;
                            dr["IGSTPer"] = txtIGSTPer.Text;
                            dr["IGSTAmount"] = txtIGSTAmt.Text;
                            dr["OtherCharge"] = txtOtherCharge.Text;
                            dr["LineTotal"] = txtFinal_Total_Amt.Text;

                            dt.Rows.Add(dr);
                            ViewState["ItemTable"] = dt;
                            Repeater1.DataSource = dt;
                            Repeater1.DataBind();
                            Totalsum();
                            txtItemCodeHide.Value = ""; txtItemName.Text = "";
                            txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRate.Text = "0.0";
                            txtRemarks.Text = "";

                            txtItemTotal.Text = "0.00";
                            txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00"; 
                            txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                            txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                            txtFinal_Total_Amt.Text = "0.00";
                        }
                        else
                        {
                            if (NameTypeRequest.Trim() == "Request")
                            {
                                query = "Select * from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                                qry_dt = objdata.RptEmployeeMultipleDetails(query);
                                if (qry_dt.Rows.Count != 0)
                                {
                                    txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                                    txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                                    txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                                }
                            }
                            else if (NameTypeRequest.Trim() == "Amend")
                            {
                                query = "Select * from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                                qry_dt = objdata.RptEmployeeMultipleDetails(query);
                                if (qry_dt.Rows.Count != 0)
                                {
                                    txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                                    txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                                    txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                                }

                            }
                        }
                    }
                    else
                    {
                        dr = dt.NewRow();
                        dr["ItemCode"] = txtItemCodeHide.Value;
                        dr["ItemName"] = txtItemName.Text;
                        dr["UOMCode"] = UOM_Code_Str;
                        dr["ReuiredQty"] = txtRequiredQty.Text;
                        dr["OrderQty"] = txtOrderQty.Text;
                        dr["ReuiredDate"] = txtRequiredDate.Text;
                        dr["Rate"] = txtRate.Text;
                        dr["Remarks"] = txtRemarks.Text;


                        dr["ItemTotal"] = txtItemTotal.Text;
                        dr["Discount_Per"] = txtDiscount_Per.Text;
                        dr["Discount"] = txtDiscount_Amount.Text;
                        dr["TaxPer"] = txtTax.Text;
                        dr["TaxAmount"] = txtTaxAmt.Text;
                        dr["CGSTPer"] = txtCGSTPer.Text;
                        dr["CGSTAmount"] = txtCGSTAmt.Text;
                        dr["SGSTPer"] = txtSGSTPer.Text;
                        dr["SGSTAmount"] = txtSGSTAmt.Text;
                        dr["IGSTPer"] = txtIGSTPer.Text;
                        dr["IGSTAmount"] = txtIGSTAmt.Text;
                        dr["OtherCharge"] = txtOtherCharge.Text;
                        dr["LineTotal"] = txtFinal_Total_Amt.Text;
                        
                        dt.Rows.Add(dr);
                        ViewState["ItemTable"] = dt;
                        Repeater1.DataSource = dt;
                        Repeater1.DataBind();

                        Totalsum();

                        txtItemCodeHide.Value = ""; txtItemName.Text = "";
                        txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRate.Text = "0.0";
                        txtRemarks.Text = "";

                        txtItemTotal.Text = "0.00";
                        txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                        txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                        txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                        txtFinal_Total_Amt.Text = "0.00";
                    }
                }
                else
                {
                    if (NameTypeRequest.Trim() == "Request")
                    {
                        query = "Select * from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                        qry_dt = objdata.RptEmployeeMultipleDetails(query);
                        if (qry_dt.Rows.Count != 0)
                        {
                            txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                            txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                            txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                        }
                    }
                    else if (NameTypeRequest.Trim() == "Amend")
                    {
                        query = "Select * from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtRequestNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                        qry_dt = objdata.RptEmployeeMultipleDetails(query);
                        if (qry_dt.Rows.Count != 0)
                        {
                            txtRequestDate.Text = qry_dt.Rows[0]["Pur_Request_Date"].ToString();
                            txtRequiredQty.Text = qry_dt.Rows[0]["ReuiredQty"].ToString();
                            txtRequiredDate.Text = qry_dt.Rows[0]["ReuiredDate"].ToString();
                        }

                    }
                }
            }
            else if (RdpPOType.SelectedValue == "1")
            {
                // Check item in item master

                query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCodeHide.Value + "' And PurchaseType='Normal'";
                qry_dt = objdata.RptEmployeeMultipleDetails(query);
                if (qry_dt.Rows.Count == 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Purchase Request No..');", true);
                }
                if (!ErrFlag)
                {
                    //UOM Code get
                    string UOM_Code_Str = qry_dt.Rows[0]["PurchaseUOM"].ToString();
                    string Line_Total = "";
                    Line_Total = ((Convert.ToDecimal(txtOrderQty.Text)) * Convert.ToDecimal(txtRate.Text)).ToString();
                    Line_Total = (Math.Round(Convert.ToDecimal(Line_Total), 0, MidpointRounding.AwayFromZero)).ToString();

                    // check view state is not null  
                    if (ViewState["ItemTable"] != null)
                    {
                        //get datatable from view state   
                        dt = (DataTable)ViewState["ItemTable"];

                        //check Item Already add or not
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                            {
                                ErrFlag = true;
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                            }
                        }
                        if (!ErrFlag)
                        {
                            dr = dt.NewRow();

                            dr["ItemCode"] = txtItemCodeHide.Value;
                            dr["ItemName"] = txtItemName.Text;
                            dr["UOMCode"] = UOM_Code_Str;
                            dr["ReuiredQty"] = txtRequiredQty.Text;
                            dr["OrderQty"] = txtOrderQty.Text;
                            dr["ReuiredDate"] = txtRequiredDate.Text;
                            dr["Rate"] = txtRate.Text;
                            dr["Remarks"] = txtRemarks.Text;

                            dr["ItemTotal"] = txtItemTotal.Text;
                            dr["Discount_Per"] = txtDiscount_Per.Text;
                            dr["Discount"] = txtDiscount_Amount.Text;
                            dr["TaxPer"] = txtTax.Text;
                            dr["TaxAmount"] = txtTaxAmt.Text;
                            dr["CGSTPer"] = txtCGSTPer.Text;
                            dr["CGSTAmount"] = txtCGSTAmt.Text;
                            dr["SGSTPer"] = txtSGSTPer.Text;
                            dr["SGSTAmount"] = txtSGSTAmt.Text;
                            dr["IGSTPer"] = txtIGSTPer.Text;
                            dr["IGSTAmount"] = txtIGSTAmt.Text;
                            dr["OtherCharge"] = txtOtherCharge.Text;
                            dr["LineTotal"] = txtFinal_Total_Amt.Text;
                        


                            dt.Rows.Add(dr);
                            ViewState["ItemTable"] = dt;
                            Repeater1.DataSource = dt;
                            Repeater1.DataBind();

                            Totalsum();

                            txtItemCodeHide.Value = ""; txtItemName.Text = "";
                            txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRate.Text = "0.0";
                            txtRemarks.Text = "";

                            txtItemTotal.Text = "0.00";
                            txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                            txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                            txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                            txtFinal_Total_Amt.Text = "0.00";
                        }
                    }
                    else
                    {
                        dr = dt.NewRow();

                        dr["ItemCode"] = txtItemCodeHide.Value;
                        dr["ItemName"] = txtItemName.Text;
                        dr["UOMCode"] = UOM_Code_Str;
                        dr["ReuiredQty"] = txtRequiredQty.Text;
                        dr["OrderQty"] = txtOrderQty.Text;
                        dr["ReuiredDate"] = txtRequiredDate.Text;
                        dr["Rate"] = txtRate.Text;
                        dr["Remarks"] = txtRemarks.Text;

                        dr["ItemTotal"] = txtItemTotal.Text;
                        dr["Discount_Per"] = txtDiscount_Per.Text;
                        dr["Discount"] = txtDiscount_Amount.Text;
                        dr["TaxPer"] = txtTax.Text;
                        dr["TaxAmount"] = txtTaxAmt.Text;
                        dr["CGSTPer"] = txtCGSTPer.Text;
                        dr["CGSTAmount"] = txtCGSTAmt.Text;
                        dr["SGSTPer"] = txtSGSTPer.Text;
                        dr["SGSTAmount"] = txtSGSTAmt.Text;
                        dr["IGSTPer"] = txtIGSTPer.Text;
                        dr["IGSTAmount"] = txtIGSTAmt.Text;
                        dr["OtherCharge"] = txtOtherCharge.Text;
                        dr["LineTotal"] = txtFinal_Total_Amt.Text;
                        

                        dt.Rows.Add(dr);
                        ViewState["ItemTable"] = dt;
                        Repeater1.DataSource = dt;
                        Repeater1.DataBind();
                        Totalsum();
                        txtItemCodeHide.Value = ""; txtItemName.Text = "";
                        txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRate.Text = "0.0";
                        txtRemarks.Text = "";

                        txtItemTotal.Text = "0.00";
                        txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                        txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                        txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                        txtFinal_Total_Amt.Text = "0.00";
                    }
                }
            }

            else if (RdpPOType.SelectedValue == "3")
            {
                // Check item in item master

                query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCodeHide.Value + "' And PurchaseType='Special'";
                qry_dt = objdata.RptEmployeeMultipleDetails(query);
                if (qry_dt.Rows.Count == 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Purchase Request No..');", true);
                }
                if (!ErrFlag)
                {
                    //UOM Code get
                    string UOM_Code_Str = qry_dt.Rows[0]["PurchaseUOM"].ToString();
                    string Line_Total = "";
                    Line_Total = ((Convert.ToDecimal(txtOrderQty.Text)) * Convert.ToDecimal(txtRate.Text)).ToString();
                    Line_Total = (Math.Round(Convert.ToDecimal(Line_Total), 0, MidpointRounding.AwayFromZero)).ToString();

                    // check view state is not null  
                    if (ViewState["ItemTable"] != null)
                    {
                        //get datatable from view state   
                        dt = (DataTable)ViewState["ItemTable"];

                        //check Item Already add or not
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                            {
                                ErrFlag = true;
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                            }
                        }
                        if (!ErrFlag)
                        {
                            dr = dt.NewRow();

                            dr["ItemCode"] = txtItemCodeHide.Value;
                            dr["ItemName"] = txtItemName.Text;
                            dr["UOMCode"] = UOM_Code_Str;
                            dr["ReuiredQty"] = txtRequiredQty.Text;
                            dr["OrderQty"] = txtOrderQty.Text;
                            dr["ReuiredDate"] = txtRequiredDate.Text;
                            dr["Rate"] = txtRate.Text;
                            dr["Remarks"] = txtRemarks.Text;

                            dr["ItemTotal"] = txtItemTotal.Text;
                            dr["Discount_Per"] = txtDiscount_Per.Text;
                            dr["Discount"] = txtDiscount_Amount.Text;
                            dr["TaxPer"] = txtTax.Text;
                            dr["TaxAmount"] = txtTaxAmt.Text;
                            dr["CGSTPer"] = txtCGSTPer.Text;
                            dr["CGSTAmount"] = txtCGSTAmt.Text;
                            dr["SGSTPer"] = txtSGSTPer.Text;
                            dr["SGSTAmount"] = txtSGSTAmt.Text;
                            dr["IGSTPer"] = txtIGSTPer.Text;
                            dr["IGSTAmount"] = txtIGSTAmt.Text;
                            dr["OtherCharge"] = txtOtherCharge.Text;
                            dr["LineTotal"] = txtFinal_Total_Amt.Text;
                        

                            dt.Rows.Add(dr);
                            ViewState["ItemTable"] = dt;
                            Repeater1.DataSource = dt;
                            Repeater1.DataBind();

                            Totalsum();

                            txtItemCodeHide.Value = ""; txtItemName.Text = "";
                            txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRate.Text = "0.0";
                            txtRemarks.Text = "";

                            txtItemTotal.Text = "0.00";
                            txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                            txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                            txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                            txtFinal_Total_Amt.Text = "0.00";
                        }
                    }
                    else
                    {
                        dr = dt.NewRow();

                        dr["ItemCode"] = txtItemCodeHide.Value;
                        dr["ItemName"] = txtItemName.Text;
                        dr["UOMCode"] = UOM_Code_Str;
                        dr["ReuiredQty"] = txtRequiredQty.Text;
                        dr["OrderQty"] = txtOrderQty.Text;
                        dr["ReuiredDate"] = txtRequiredDate.Text;
                        dr["Rate"] = txtRate.Text;
                        dr["Remarks"] = txtRemarks.Text;


                        dr["ItemTotal"] = txtItemTotal.Text;
                        dr["Discount_Per"] = txtDiscount_Per.Text;
                        dr["Discount"] = txtDiscount_Amount.Text;
                        dr["TaxPer"] = txtTax.Text;
                        dr["TaxAmount"] = txtTaxAmt.Text;
                        dr["CGSTPer"] = txtCGSTPer.Text;
                        dr["CGSTAmount"] = txtCGSTAmt.Text;
                        dr["SGSTPer"] = txtSGSTPer.Text;
                        dr["SGSTAmount"] = txtSGSTAmt.Text;
                        dr["IGSTPer"] = txtIGSTPer.Text;
                        dr["IGSTAmount"] = txtIGSTAmt.Text;
                        dr["OtherCharge"] = txtOtherCharge.Text;
                        dr["LineTotal"] = txtFinal_Total_Amt.Text;
                        

                        dt.Rows.Add(dr);
                        ViewState["ItemTable"] = dt;
                        Repeater1.DataSource = dt;
                        Repeater1.DataBind();
                        Totalsum();
                        txtItemCodeHide.Value = ""; txtItemName.Text = "";
                        txtRequiredQty.Text = ""; txtOrderQty.Text = "0.0"; txtRequiredDate.Text = ""; txtRate.Text = "0.0";
                        txtRemarks.Text = "";

                        txtItemTotal.Text = "0.00";
                        txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                        txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00";
                        txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                        txtFinal_Total_Amt.Text = "0.00";
                    }
                }
            }
//


        }
      }

    public void Totalsum()
    {
        sum = "0";
        OrderQty = 0;
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(dt.Rows[i]["LineTotal"])).ToString();
            txtTotAmt.Text = sum;
            OrderQty = Convert.ToDecimal(OrderQty) + Convert.ToDecimal(dt.Rows[i]["OrderQty"]);
        }
         Final_Total_Calculate();
       
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredQty", typeof(string)));
        dt.Columns.Add(new DataColumn("OrderQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredDate", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));

        dt.Columns.Add(new DataColumn("ItemTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("Discount_Per", typeof(string)));
        dt.Columns.Add(new DataColumn("Discount", typeof(string)));
        dt.Columns.Add(new DataColumn("TaxPer", typeof(string)));
        dt.Columns.Add(new DataColumn("TaxAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("BDUTaxPer", typeof(string)));
        dt.Columns.Add(new DataColumn("BDUTaxAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("OtherCharge", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));


        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;
    
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        Totalsum();
        txtTax.Text = "0";
        txtTaxAmt.Text = "0";
        txtDiscount.Text = "0";
        txtOthers.Text = "0";
        txtNetAmt.Text = "0";
    }

   protected void btnStd_Purchase_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + SessionStdPOOrderNot + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtRequestNo.Text = Main_DT.Rows[0]["Transaction_No"].ToString();
            txtRequestDate.Text = Main_DT.Rows[0]["Date"].ToString();
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            
            if (Main_DT.Rows[0]["PO_Status"].ToString() == "1")
            {
                lblStatus.Text = "Approved by : " + Main_DT.Rows[0]["Approvedby"].ToString();
            }
            else if (Main_DT.Rows[0]["PO_Status"].ToString() == "2")
            {
                lblStatus.Text = "Cancelled by : " + Main_DT.Rows[0]["Approvedby"].ToString();
            }
            else if (Main_DT.Rows[0]["PO_Status"].ToString() == "3" || Main_DT.Rows[0]["PO_Status"].ToString() == "0" || Main_DT.Rows[0]["PO_Status"].ToString() == "5" || Main_DT.Rows[0]["PO_Status"].ToString() == "")
            {
                lblStatus.Text = "Pending..";
            }
            else if (Main_DT.Rows[0]["PO_Status"].ToString() == "6")
            {
                lblStatus.Text = "Rejected by FM..";
            }


            txtDate.Text = Main_DT.Rows[0]["Std_PO_Date"].ToString();
            txtSuppCodehide.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
            txtSupplierName.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtSuppRefDocNo.SelectedItem.Text = Main_DT.Rows[0]["Supp_Qtn_No"].ToString();
            txtDocDate.Text = Main_DT.Rows[0]["Supp_Qtn_Date"].ToString();
            txtDeliveryMode.Value = Main_DT.Rows[0]["DeliveryMode"].ToString();
            txtDeliveryDate.Text = Main_DT.Rows[0]["DeliveryDate"].ToString();
            txtDeliveryAt.Text = Main_DT.Rows[0]["DeliveryAt"].ToString();
            txtPaymentMode.Value = Main_DT.Rows[0]["PaymentMode"].ToString();
            //txtDeptCodeHide.Value = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtPaymentTerms.Text = Main_DT.Rows[0]["PaymentTerms"].ToString();
            txtDescription.Text = Main_DT.Rows[0]["Description"].ToString();
            txtNote.Text = Main_DT.Rows[0]["Note"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["TotalAmt"].ToString();
            //txtDiscount.Text = Main_DT.Rows[0]["Discount"].ToString();
            //txtTax.Text=Main_DT.Rows[0]["TaxPer"].ToString();
            //txtTaxAmt.Text = Main_DT.Rows[0]["TaxAmount"].ToString();
            txtAddOrLess.Text = Main_DT.Rows[0]["AddOrLess"].ToString();
            txtNetAmt.Text = Main_DT.Rows[0]["NetAmount"].ToString();
            txtRequestNo.Text = Main_DT.Rows[0]["Pur_Request_No"].ToString();
            txtRequestDate.Text = Main_DT.Rows[0]["Pur_Request_Date"].ToString();
            if (Main_DT.Rows[0]["PO_Type"].ToString() == "Special")
            {
                RdpPOType.SelectedValue = "3";
            }
            else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Direct")
            {
                RdpPOType.SelectedValue = "1";
            }
            else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Req/Amend")
            {
                RdpPOType.SelectedValue = "2";
            }
           

            //Std_Purchase_Order_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select *from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("standard_po_main.aspx");
    }

    private void Delivery_Mode_Add()
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        txtDeliveryMode.Items.Clear();
        query = "Select * from MstDeliveryMode where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeliveryMode Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDeliveryMode.Items.Add("-Select-");
        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {
            txtDeliveryMode.Items.Add(Main_DT.Rows[i]["DeliveryMode"].ToString());
        }
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Std Purchase Order");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Std Purchase Order...');", true);
        //}
        //User Rights Check End

        query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Std Purchase Order Details..');", true);
        }

        query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "' And PO_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Std Purchase Order Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            query = "Update Std_Purchase_Order_Main set PO_Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And Std_PO_No='" + txtStdOrderNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);


            query = "Update Std_Purchase_Order_Approve set Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Transaction_No='" + txtStdOrderNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);


            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Std Purchase Order Details Approved Successfully..');", true);
        }
    }

    protected void txtTax_TextChanged(object sender, EventArgs e)
    {
        txtTaxAmt.Text = (Convert.ToDecimal(txtTotAmt.Text) * (Convert.ToDecimal(txtTax.Text) / 100)).ToString();

        txtNetAmt.Text = ((Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
    }

   
    protected void txtDiscount_TextChanged(object sender, EventArgs e)
    {
        txtNetAmt.Text = ((Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
    }


    protected void btnSearchView_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
                       

            txtDate.Text = Main_DT.Rows[0]["Std_PO_Date"].ToString();
            txtSuppCodehide.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
            txtSupplierName.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtSuppRefDocNo.SelectedItem.Text = Main_DT.Rows[0]["Supp_Qtn_No"].ToString();
            txtDocDate.Text = Main_DT.Rows[0]["Supp_Qtn_Date"].ToString();
            txtDeliveryMode.Value = Main_DT.Rows[0]["DeliveryMode"].ToString();
            txtDeliveryDate.Text = Main_DT.Rows[0]["DeliveryDate"].ToString();
            txtDeliveryAt.Text = Main_DT.Rows[0]["DeliveryAt"].ToString();
            txtPaymentMode.Value = Main_DT.Rows[0]["PaymentMode"].ToString();
            //txtDeptCodeHide.Value = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtPaymentTerms.Text = Main_DT.Rows[0]["PaymentTerms"].ToString();
            txtDescription.Text = Main_DT.Rows[0]["Description"].ToString();
            txtNote.Text = Main_DT.Rows[0]["Note"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["TotalAmt"].ToString();
            //txtDiscount.Text = Main_DT.Rows[0]["Discount"].ToString();
            //txtTax.Text = Main_DT.Rows[0]["TaxPer"].ToString();
            //txtTaxAmt.Text = Main_DT.Rows[0]["TaxAmount"].ToString();
            txtAddOrLess.Text = Main_DT.Rows[0]["AddOrLess"].ToString();
            txtNetAmt.Text = Main_DT.Rows[0]["NetAmount"].ToString();
            txtRequestNo.Text = Main_DT.Rows[0]["Pur_Request_No"].ToString();
            txtRequestDate.Text = Main_DT.Rows[0]["Pur_Request_Date"].ToString();

            if (Main_DT.Rows[0]["PO_Type"].ToString() == "Special")
            {
                RdpPOType.SelectedValue = "3";
            }
            else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Direct")
            {
                RdpPOType.SelectedValue = "1";
            }
            else if (Main_DT.Rows[0]["PO_Type"].ToString() == "Req/Amend")
            {
                RdpPOType.SelectedValue = "2";
            }


            //Std_Purchase_Order_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select *from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Visible = false;
        }
        else
        {
            Clear_All_Field();
        }
    }


    protected void btnSupp1_Click(object sender, EventArgs e)
    {
        modalPop_Supp1.Show();
    }
    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier1.DataSource = DT;
        Repeater_Supplier1.DataBind();
        Repeater_Supplier1.Visible = true;

    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
        txtSupplierName.Text = Convert.ToString(e.CommandName);


        txtSuppRefDocNo.SelectedItem.Text = "-Select-";
        txtDocDate.Text = "";

        Load_Data_Empty_SuppRefDocNo();
    }

    protected void btnSuppRefDocNo_Click(object sender, EventArgs e)
    {
        //modalPop_SuppRefDocNo.Show();
    }

    private void Load_Data_Empty_SuppRefDocNo()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

       

        query = "Select QuotNo,QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And SuppCode='" + txtSuppCodehide.Value + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);



        txtSuppRefDocNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["QuotNo"] = "-Select-";
        //dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtSuppRefDocNo.DataTextField = "QuotNo";
        //txtDeptCode.DataValueField = "DeptCode";
        txtSuppRefDocNo.DataBind();


    }

    protected void GridViewClick_SuppRefDocNo(object sender, CommandEventArgs e)
    {
        txtSuppRefDocNo.Text = Convert.ToString(e.CommandArgument);
        txtDocDate.Text = Convert.ToString(e.CommandName);
    }

    protected void btnDept_Click(object sender, EventArgs e)
    {
       // modalPop_Dept.Show();
    }

    private void Load_Data_Empty_Dept()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();
      
    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
       // txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
       // txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }

    private void Load_Data_Empty_ItemCode()
    {
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        string ss = "";
        string PO_Type = "";
        string Request_No = "";

        PO_Type = RdpPOType.SelectedValue;
        Request_No = txtRequestNo.Text;

        if (PO_Type == "2")
        {
            ss = Request_No.Substring(0, 3);
        }
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");
        DT.Columns.Add("ReuiredQty");
        DT.Columns.Add("ReuiredDate");

        DataTable DT1 = new DataTable();
        DT1.Columns.Add("ItemCode");
        DT1.Columns.Add("ItemName");
        

        if (PO_Type == "2")
        {
            if (ss != "PRA")
            {
                query = "Select ItemCode,ItemName,ReuiredQty,ReuiredDate from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                query = query + " And Pur_Request_No='" + Request_No + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                Repeater_ItemCode_PR.DataSource = DT;
                Repeater_ItemCode_PR.DataBind();
                Repeater_ItemCode_PR.Visible = true;
                Repeater_ItemCode_Normal.Visible = false;
       
            }
            else
            {
                query = "Select ItemCode,ItemName,ReuiredQty,ReuiredDate from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                query = query + " And Amend_No='" + Request_No + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                Repeater_ItemCode_PR.DataSource = DT;
                Repeater_ItemCode_PR.DataBind();
                Repeater_ItemCode_PR.Visible = true;
                Repeater_ItemCode_Normal.Visible = false;
               
            }
        }
        else if (PO_Type == "1")
        {
            query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And PurchaseType='Normal'";
            DT1 = objdata.RptEmployeeMultipleDetails(query);
            Repeater_ItemCode_Normal.DataSource = DT1;
            Repeater_ItemCode_Normal.DataBind();
            Repeater_ItemCode_Normal.Visible = true;
            Repeater_ItemCode_PR.Visible = false;
            

        }
        else if (PO_Type == "3")
        {
            query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And PurchaseType='Special'";
            DT1 = objdata.RptEmployeeMultipleDetails(query);
            Repeater_ItemCode_Normal.DataSource = DT1;
            Repeater_ItemCode_Normal.DataBind();
            Repeater_ItemCode_Normal.Visible = true;
            Repeater_ItemCode_PR.Visible = false;


        }

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }

    protected void GridViewClick_ItemCode_PR(object sender, CommandEventArgs e)
    {
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        string ss = "";
        string PO_Type = "";
        string Request_No = "";
        DataTable DT = new DataTable();


        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
              

        PO_Type = RdpPOType.SelectedValue;
        Request_No = txtRequestNo.Text;

        if (PO_Type == "2")
        {
            ss = Request_No.Substring(0, 3);
        }

        if (PO_Type == "2")
        {
            if (ss != "PRA")
            {
                query = "Select ReuiredQty,ReuiredDate from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                query = query + " And Pur_Request_No='" + Request_No + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
            }
            else
            {
                query = "Select ReuiredQty,ReuiredDate from Pur_Request_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                query = query + " And Amend_No='" + Request_No + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
            }

        }

       txtRequiredQty.Text=DT.Rows[0]["ReuiredQty"].ToString();
       txtRequiredDate.Text = DT.Rows[0]["ReuiredDate"].ToString();

    }

    protected void RdpPOType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_ItemCode();
    }

    protected void txtSuppRefDocNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        //txtDeptName.Items.Clear();
        if (txtSuppRefDocNo.Text != "" && txtSuppRefDocNo.Text != "-Select-")
        {
            query = "Select QuotDate from Supp_Qut_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + " And QuotNo='" + txtSuppRefDocNo.Text + "'";
            Main_DT = objdata.RptEmployeeMultipleDetails(query);

            for (int i = 0; i < Main_DT.Rows.Count; i++)
            {
                txtDocDate.Text = Main_DT.Rows[i]["QuotDate"].ToString();
                //txtDeptName.Text = Main_DT.Rows[i]["DeptName"].ToString();
            }
        }
    }


    protected void txtDiscount_Per_TextChanged(object sender, EventArgs e)
    {
        Total_Calculate();
    }
    protected void txtGST_Type_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TaxData();
    }

    private void Load_TaxData()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GST_Type='" + txtGST_Type.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtCGSTPer.Text = DT.Rows[0]["CGST_Percent"].ToString();
            txtSGSTPer.Text = DT.Rows[0]["SGST_Percent"].ToString();
            txtIGSTPer.Text = DT.Rows[0]["IGST_Percent"].ToString();
        }
        else
        {
            txtCGSTPer.Text = "0";
            txtSGSTPer.Text = "0";
            txtIGSTPer.Text = "0";
        }

        Total_Calculate();
    }
    protected void txtOtherCharge_TextChanged(object sender, EventArgs e)
    {
        Total_Calculate();
    }

    private void Total_Calculate()
    {
        string Qty_Val = "0";
        string Item_Rate = "0";
        string Item_Total = "0";
        string Discount_Percent = "0";
        string Discount_Amt = "0";
        string Tax_Per = "0";
        string Tax_Amt = "0";
        string CGST_Per = "0";
        string CGST_Amt = "0";
        string SGST_Per = "0";
        string SGST_Amt = "0";
        string IGST_Per = "0";
        string IGST_Amt = "0";
        string BDUTax_Per = "0";
        string BDUTax_Amt = "0";
        string Other_Charges = "0";
        string Final_Amount = "0";
        string PackingAmt = "0";
        if (txtOrderQty.Text != "") { Qty_Val = txtOrderQty.Text.ToString(); }
        if (txtRate.Text != "") { Item_Rate = txtRate.Text.ToString(); }


        Item_Total = (Convert.ToDecimal(Qty_Val) * Convert.ToDecimal(Item_Rate)).ToString();
        Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();
        if (Convert.ToDecimal(Item_Total) != 0)
        {
            if (Convert.ToDecimal(txtDiscount_Per.Text.ToString()) != 0) { Discount_Percent = txtDiscount_Per.Text.ToString(); }
            if (Convert.ToDecimal(txtOtherCharge.Text.ToString()) != 0) { Other_Charges = txtOtherCharge.Text.ToString(); }
            if (Convert.ToDecimal(txtCGSTPer.Text.ToString()) != 0) { CGST_Per = txtCGSTPer.Text.ToString(); }
            if (Convert.ToDecimal(txtSGSTPer.Text.ToString()) != 0) { SGST_Per = txtSGSTPer.Text.ToString(); }
            if (Convert.ToDecimal(txtIGSTPer.Text.ToString()) != 0) { IGST_Per = txtIGSTPer.Text.ToString(); }

            

            //Discount Amt Calculate
            if (Convert.ToDecimal(Discount_Percent) != 0)
            {
                Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                Discount_Amt = "0.00";
            }
            
            if (Convert.ToDecimal(CGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                CGST_Amt = "0.00";
            }

            if (Convert.ToDecimal(SGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                SGST_Amt = "0.00";
            }

            if (Convert.ToDecimal(IGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                IGST_Amt = "0.00";
            }


           


            //Other Charges
            if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

            //Final Amt
            Final_Amount = ((Convert.ToDecimal(Item_Total)) - Convert.ToDecimal(Discount_Amt)).ToString();
            //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
            Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
            Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 2, MidpointRounding.AwayFromZero)).ToString();

            txtItemTotal.Text = Item_Total;
            txtDiscount_Amount.Text = Discount_Amt;
            txtTaxAmt.Text = Tax_Amt;
            txtCGSTAmt.Text = CGST_Amt;
            txtSGSTAmt.Text = SGST_Amt;
            txtIGSTAmt.Text = IGST_Amt;
            txtFinal_Total_Amt.Text = Final_Amount;
            txtNetAmt.Text = Final_Amount;



        }
    }
    private void Final_Total_Calculate()
    {

        string Final_NetAmt = "0";
        string AddorLess = "0";
        string Item_Total_Amt = txtTotAmt.Text;

        if (Convert.ToDecimal(txtAddOrLess.Text.ToString()) != 0) { AddorLess = txtAddOrLess.Text.ToString(); }
        Final_NetAmt = (Convert.ToDecimal(AddorLess) + Convert.ToDecimal(Item_Total_Amt)).ToString();
        //txtFinal_Total_Amt.Text = Final_NetAmt;
        txtNetAmt.Text = Final_NetAmt;

    }

    protected void txtAddOrLess_TextChanged(object sender, EventArgs e)
    {
        if (txtAddOrLess.Text.ToString() != "") { Final_Total_Calculate(); }
       
    }
    protected void txtOrderQty_TextChanged(object sender, EventArgs e)
    {
        if (txtOrderQty.Text.ToString() != "") { Total_Calculate(); }
    }
    protected void txtRate_TextChanged(object sender, EventArgs e)
    {
        if (txtRate.Text.ToString() != "") { Total_Calculate(); }
    }
}
