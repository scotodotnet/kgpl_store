﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Order_Blanket_po_Approve : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionStdPOOrderNo;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Standard Purchase Order Approve";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
            //if (Session["Std_PO_No"] == null)
            //{
            //    SessionStdPOOrderNo = "";
            //}
            //else
            //{
            //}
        }
        Load_OLD_data();
    }

    private void Load_OLD_data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And (Blanket_PO_Status = '0' or Blanket_PO_Status is null)";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        string Std_No_Str = e.CommandName.ToString();
        Session.Remove("Blanket_PO_No");
        Session["Blanket_PO_No"] = Std_No_Str;
        Response.Redirect("Blanket_po_Approve_View.aspx");
    }
}
