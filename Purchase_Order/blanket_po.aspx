﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="blanket_po.aspx.cs" Inherits="Purchase_Order_blanket_po" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Supplier1').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Supplier1').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#SuppRefDocNo').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#SuppRefDocNo').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Dept').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Dept').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable_Normal').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable_Normal').dataTable();               
            }
        });
    };
</script>


<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
     $find('Supp1_Close').hide();
     $find('SuppRefDocNo_Close').hide(); 
     $find('Dept_Close').hide();
     $find('Item_Close').hide();
     }
   } 
 </script>
<!--Ajax popup End-->



<%--Supplier Name Select List Script Start--%>    
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_Supp_Code();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Supp_Code);
        });
        function initializer_Supp_Code() {
            $("#<%=txtSupplierName.ClientID %>").autocomplete({                 
                source: function (request, response) {                        
                      $.ajax({ 
                          url: '<%=ResolveUrl("../List_Service.asmx/GetSupplierCode_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) {
                                  return {
                                      label: item.split('-')[0] + "-" + item.split('-')[1],
                                      val: item.split('-')[1],
                                      text_val:item.split('-')[0],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtSupplierName.ClientID %>").val(i.item.val);
                      //$("#<%=txtSupplierName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtSupplierName.ClientID %>").val(i.item.text_val);
                      $("#<%=txtSuppCodehide.ClientID %>").val(i.item.val);
                   return false;
                  }
              });
          }
      </script>--%>
<%--Supplier Name Select List Script End--%>

<%--Supplier Quotation No Select List Script Saart--%>    
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_Supp_Quotation();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Supp_Quotation);
        });
        function initializer_Supp_Quotation() {
            $("#<%=txtSuppRefDocNo.ClientID %>").autocomplete({
                source: function (request, response) {
                      $.ajax({
                          url: '<%=ResolveUrl("../List_Service.asmx/GetSupplierQutNoSelect_Det") %>',
                          data: "{ 'prefix': '" + request.term + "','Supp_Code_No': '" + $('#<%=txtSuppCodehide.ClientID %>').val() + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) { 
                                  return {                                  
                                      label: item.split('|')[0],
                                      val: item.split('|')[0],
                                      Qut_date:item.split('|')[1],
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtSuppRefDocNo.ClientID %>").val(i.item.val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtSuppRefDocNo.ClientID %>").val(i.item.val);
                      $("#<%=txtDocDate.ClientID %>").val(i.item.Qut_date);
                   return false;
                  }
              }); 
          }
      </script>--%>
<%--Supplier Quotation No Select List Script End--%>

<%--Department Name Select List Script Start--%>    
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_Dept_Code();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Dept_Code);
        });
        function initializer_Dept_Code() {
            $("#<%=txtDepartmentName.ClientID %>").autocomplete({                 
                source: function (request, response) {                        
                      $.ajax({ 
                          url: '<%=ResolveUrl("../List_Service.asmx/GetDeptFull_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) {
                                  return {
                                      label: item.split('|')[0] + "|" + item.split('|')[1],
                                      val: item.split('|')[1],
                                      text_val:item.split('|')[0],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtDepartmentName.ClientID %>").val(i.item.val);
                      //$("#<%=txtDepartmentName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtDepartmentName.ClientID %>").val(i.item.text_val);
                      $("#<%=txtDeptCodeHide.ClientID %>").val(i.item.val);
                   return false;
                  }
              });
          }
      </script>--%>
<%--Department Name Select List Script End--%>


<%--Item Select List Script Saart--%>    
   <%-- <script type="text/javascript">
         $(document).ready(function () {
            initializer_Item();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Item);
        });
        function initializer_Item() {
            $("#<%=txtItemName.ClientID %>").autocomplete({                 
                source: function (request, response) {
                      $.ajax({
                          url: '<%=ResolveUrl("../List_Service.asmx/GetBOPurRequestItemCodeSelect_Det_PO") %>',
                          data: "{ 'prefix': '" + request.term + "','PO_Type': '" + $("[id*=RdpPOType] input:checked").val() + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) { 
                                  return {                                  
                                      label: item.split('-')[0],
                                      val: item.split('-')[0],
                                      Item_Code:item.split('-')[1],
                                      Require_Qty:item.split('-')[2],                                     
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtItemName.ClientID %>").val(i.item.val);
                      //$("#<%=txtItemName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtItemName.ClientID %>").val(i.item.val);
                      $("#<%=txtItemCodeHide.ClientID %>").val(i.item.Item_Code);
                      $("#<%=txtRequiredQty.ClientID %>").val(i.item.Require_Qty);
                   return false;
                  }
              }); 
          }
      </script>--%>
<%--Item Select List Script End--%>

<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Blanket Purchase Order</li></h4> 
    </ol>
</div>
                
                        
<div id="main-wrapper" class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-9">
		        <div class="panel panel-white">
		            <div class="panel panel-primary">
			            <div class="panel-heading clearfix">
				            <h4 class="panel-title">Blanket Purchase Order</h4>
			            </div>
			        </div>
			        <form class="form-horizontal">
			            <div class="panel-body">
			            
			            <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-2">
                                <asp:Label ID="lblStatus" runat="server" Font-Bold="True" Font-Size="Large" 
                                    ForeColor="Red"></asp:Label>
					        </div>
					        </div>
					        </div>
					        
			                <div class="col-md-12">
				                <div class="row">
				                    <div class="form-group col-md-3">
				                        <label for="exampleInputName">Blanket No<span class="mandatory">*</span></label>
				                        <asp:Label ID="txtBlanketNo" runat="server" class="form-control"></asp:Label>
				                    </div>
				                    <div class="form-group col-md-2">
				                        <label for="exampleInputName">Date</label>
				                        <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
				                        <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtDate" ValidChars="0123456789./">
                                        </cc1:FilteredTextBoxExtender>
				                    </div>					                
				                    <div class="form-group col-md-4">
				                        <label for="exampleInputName">Supplier Name</label>
				                        <asp:TextBox ID="txtSupplierName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
				                         <asp:Button ID="btnSupp1" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnSupp1_Click"/>
					            
					            <cc1:ModalPopupExtender ID="modalPop_Supp1"  runat="server" PopupControlID="Panl1" TargetControlID="btnSupp1"
                       CancelControlID="BtnClear" BackgroundCssClass="modalBackground" BehaviorID="Supp1_Close">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="Panl1" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Supplier Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_Supplier1" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="Supplier1" class="display table">
                         <thead >
                         <tr>
                         <th>SuppCode</th>
                         <th>SuppName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("SuppCode")%></td>
                          <td><%# Eval("SuppName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick" CommandArgument='<%# Eval("SuppCode")%>' CommandName='<%# Eval("SuppName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>
				                        
				                        
				                        
				                        <asp:HiddenField ID="txtSuppCodehide" runat="server" />
				                        <asp:RequiredFieldValidator ControlToValidate="txtSupplierName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
				                    </div>
				                    <div class="form-group col-md-3">
				                        <label for="exampleInputName">Supp.Ref.Doc.No</label>
				                        
				                        <asp:DropDownList ID="txtSuppRefDocNo" runat="server" class="js-states form-control" AutoPostBack="true" 
                                    onselectedindexchanged="txtSuppRefDocNo_SelectedIndexChanged">
                                </asp:DropDownList>
				                         <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtSuppRefDocNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
				                         <%--<asp:TextBox ID="txtSuppRefDocNo" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
				                       <asp:Button ID="btnSuppRefDocNo" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnSuppRefDocNo_Click"/>--%>
					            
					           <%--<cc1:ModalPopupExtender ID="modalPop_SuppRefDocNo"  runat="server" PopupControlID="Panel1" TargetControlID="btnSuppRefDocNo"
                       CancelControlID="BtnClear_SuppRefDocNo" BackgroundCssClass="modalBackground" BehaviorID="SuppRefDocNo_Close">
                    </cc1:ModalPopupExtender>--%>
                    <%--<asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Supplier Quotation Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_SuppRefDocNo" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="SuppRefDocNo" class="display table">
                         <thead >
                         <tr>
                         <th>Supp.Ref.Doc.No</th>
                         <th>Doc Date</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("QuotNo")%></td>
                          <td><%# Eval("QuotDate")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEdit_SuppRefDocNo" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_SuppRefDocNo" CommandArgument='<%# Eval("QuotNo")%>' CommandName='<%# Eval("QuotDate")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_SuppRefDocNo" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
				                        
				                        
				                        
				                        
				                        
				                    </div>
				                </div>
				            </div>					        
				            <div class="col-md-12">
				                <div class="row">
				                    <div class="form-group col-md-4">
				                        <label for="exampleInputName">Doc Date</label>
				                        <asp:TextBox ID="txtDocDate" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
				                    </div>
				                    <div class="form-group col-md-4">
				                        <label for="exampleInputName">Delivery Mode</label>
				                        <select name="txtDeliveryMode" id="txtDeliveryMode" class="js-states form-control" runat="server">
                                        </select>
				                    </div>
				                    <div class="form-group col-md-4">
				                        <label for="exampleInputName">Delivery At</label>
				                        <asp:TextBox ID="txtDeliveryAt" class="form-control" runat="server"></asp:TextBox>
				                    </div>
				                    <div class="form-group col-md-4">
				                        <label for="exampleInputName">Payment Mode</label>
				                        <select name="txtPaymentMode" id="txtPaymentMode" class="js-states form-control" runat="server">
                                            <option value="- select -">- select -</option>
                                            <option value="CASH">CASH</option>
                                            <option value="CHEQUE">CHEQUE</option>
                                            <option value="NEFT">NEFT</option>
                                            <option value="RTGS">RTGS</option>
                                            <option value="DD">DD</option>
                                        </select>
				                    </div>
				                    <div class="form-group col-md-4">
				                        <label for="exampleInputName">Department Name</label>
				                        <asp:DropDownList ID="txtDepartmentName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					             <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtDepartmentName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator> 
				                        <%--<asp:TextBox ID="txtDepartmentName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
				                        <asp:Button ID="btnDept" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnDept_Click"/>--%>
                   <%-- <cc1:ModalPopupExtender ID="modalPop_Dept"  runat="server" PopupControlID="Panel2" TargetControlID="btnDept"
                       CancelControlID="BtnClear_Dept" BackgroundCssClass="modalBackground" BehaviorID="Dept_Close">
                    </cc1:ModalPopupExtender>--%>
                   
                    <%--<asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Department Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_Dept" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="Dept" class="display table">
                         <thead >
                         <tr>
                         <th>DeptCode</th>
                         <th>DeptName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("DeptCode")%></td>
                          <td><%# Eval("DeptName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_Dept" CommandArgument='<%# Eval("DeptCode")%>' CommandName='<%# Eval("DeptName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_Dept" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
				                        
				                        
				                        
				                       <%-- <asp:HiddenField ID="txtDeptCodeHide" runat="server" />
				                        <asp:RequiredFieldValidator ControlToValidate="txtDepartmentName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>--%>
				                    </div>
				                    <div class="form-group col-md-4">
				                        <label for="exampleInputName">Payment Terms</label>
				                        <asp:TextBox ID="txtPaymentTerms" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
				                    </div>
				                </div>
				            </div>
				            <div class="col-md-12">
				                <div class="row">
				                    <div class="form-group col-md-4">
				                        <label for="exampleInputName">Description</label>
				                        <asp:TextBox ID="txtDescription" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
				                    </div>
				                    <div class="form-group col-md-4">
				                        <label for="exampleInputName">Note</label>
				                        <asp:TextBox ID="txtNote" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
				                    </div>
				                    <div class="form-group col-md-4">
				                        <label for="exampleInputName">Others</label>
				                        <asp:TextBox ID="txtOthers" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
				                    </div>
				                </div>
				            </div>
				            
				            <div class="clearfix"></div>				    
				            <div class="timeline-options">
				                <h4 class="panel-title">Item Details</h4>
				            </div>
					        					               
					        <div class="col-md-12">
					            <div class="row">
					                <%--<div class="form-group col-md-3">
					                    <label for="exampleInputName">Request No</label>
					                    <asp:TextBox ID="txtRequestNo" class="form-control" runat="server"></asp:TextBox>
					                    <asp:RequiredFieldValidator ControlToValidate="txtRequestNo" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
					                </div>
					                <div class="form-group col-md-3">
					                    <label for="exampleInputName">Request Date</label>
					                    <asp:TextBox ID="txtRequestDate" class="form-control" runat="server"></asp:TextBox>
					                </div>--%>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Pur Order Type</label>
					            <asp:RadioButtonList ID="RdpPOType" class="form-control" runat="server" RepeatColumns="2" onselectedindexchanged="RdpPOType_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="1" Text="Direct" style="padding-right:40px" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Special" ></asp:ListItem>
                                </asp:RadioButtonList>					            
					        </div>
					                
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Item Name</label>
					                    <asp:TextBox ID="txtItemName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					                    <asp:Button ID="btnItemCode" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnItemCode_Click"/>
                                
                         <cc1:ModalPopupExtender ID="modalPop_ItemCode"  runat="server" PopupControlID="Panel5" TargetControlID="btnItemCode"
                       CancelControlID="BtnItemClose" BackgroundCssClass="modalBackground" BehaviorID="Item_Close">
                    </cc1:ModalPopupExtender>
                         <asp:Panel ID="Panel5" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Item Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_ItemCode_Normal" runat="server" EnableViewState="false" Visible="false">
					    <HeaderTemplate>
                         <table id="ItemTable_Normal" class="display table">
                         <thead >
                         <tr>
                         <th>ItemCode</th>
                         <th>ItemName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ItemCode")%></td>
                          <td><%# Eval("ItemName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEdit_ItemCode" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ItemCode" CommandArgument='<%# Eval("ItemCode")%>' CommandName='<%# Eval("ItemName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnItemClose" class="btn btn-rounded" runat="server" Text="Close" />
					     </div>
					     </asp:Panel>
					                    
					                    
					                    
					                    <asp:HiddenField ID="txtItemCodeHide" runat="server" />
					                    <asp:RequiredFieldValidator ControlToValidate="txtItemName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
					                </div>
					                <div class="form-group col-md-2">
					                    <label for="exampleInputName">Required Qty</label>
					                    <asp:TextBox ID="txtRequiredQty" class="form-control" runat="server"></asp:TextBox>
					                </div>
					            </div>
					        </div>
					        <div class="col-md-12">
					            <div class="row">
					                <div class="form-group col-md-3">
					                    <label for="exampleInputName">Order Qty</label>
					                    <asp:TextBox ID="txtOrderQty" class="form-control" runat="server"></asp:TextBox>
					                    <asp:RequiredFieldValidator ControlToValidate="txtOrderQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtOrderQty" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
					                </div>
					                <div class="form-group col-md-3">
					                    <label for="exampleInputName">Rate</label>
					                    <asp:TextBox ID="txtRate" class="form-control" runat="server" Text="0.0"></asp:TextBox>
					                    <asp:RequiredFieldValidator ControlToValidate="txtRate" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtRate" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
					                </div>
					                <div class="form-group col-md-1">
					                    <br />
					                    <asp:Button ID="btnAddItem" Width="50" Height="30" class="btn-success"  runat="server" Text="Add" ValidationGroup="Item_Validate_Field"  OnClick="btnAddItem_Click"/>
					                </div>
					            </div>
					        </div>
					        
					        <!-- table start -->
					        <div class="col-md-12">
					            <div class="row">
					                <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                            <HeaderTemplate>
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>S.No</th>
                                                        <th>Item Name</th>
                                                        <th>UOM Code</th>
                                                        <th>Requ. Qty</th>
                                                        <th>Order Qty</th>
                                                        <th>Rate</th>
                                                        <th>LineTotal</th>
                                                        <th>IsSchedule</th>
                                                        <th>Mode</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 %></td>
                                                <%--<td><%# Eval("Pur_Request_No")%></td>
                                                <td><%# Eval("Pur_Request_Date")%></td>--%>
                                                <%--<td><%# Eval("ItemCode")%></td>--%>
                                                <td><%# Eval("ItemName")%></td>
                                                <td><%# Eval("UOMCode")%></td>
                                                <td><%# Eval("ReuiredQty")%></td>
                                                <td><%# Eval("OrderQty")%></td>
                                                <td><%# Eval("Rate")%></td>
                                                <td><%# Eval("LineTotal")%></td>
                                                <td>
                                                    <asp:checkbox id="check1" runat="server" Checked='<%# Eval("Schedule")%>' Enabled="false"/>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteClick" CommandName='<%# Eval("ItemCode")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
			                        </asp:Repeater>
					            </div>
					        </div>
					        <!-- table End -->
					        
					        <div class="col-md-12">
					    <div class="row">
					     <div class="form-group col-md-10"></div>
					      <div class="form-group col-md-2">
					            <label for="exampleInputName">Total Amt</label>
					            <asp:TextBox ID="txtTotAmt" class="form-control" runat="server" Text="0.0"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTotAmt" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					    </div>
					    </div>
					
					        
					        
					        <div class="form-group row"></div>	
                            <!-- Button start -->
                            <div class="txtcenter">
                                <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                                <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" Text="Back" OnClick="btnBackRequest_Click"/>
                                <%--<asp:Button ID="btnApprove" class="btn btn-success" runat="server" Text="Approve" OnClick="btnApprove_Click"/>--%>
                            </div>
                            <!-- Button end -->
                            
                	    </div>
			        </form>			
		        </div><!-- panel white end -->
            </div> <!-- col-9 end -->
            
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-white" style="height: 100%;">
                    <div class="panel-heading">
                        <h4 class="panel-title">Dashboard Details</h4>
                        <div class="panel-control"></div>
                    </div>
                    <div class="panel-body"></div>
                </div>
            </div>  
                        
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                        </div>
                    </div>
                </div>
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div><!-- col 12 end -->
    </div><!-- row end -->
</div>
 
 
 
</asp:Content>

