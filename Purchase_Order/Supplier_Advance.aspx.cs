﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Order_Supplier_Advance : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionAdvanceTransNo;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Supplier Advance Entry";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
            //Initial_Data_Referesh_SuppBill();
            //Initial_Data_Referesh_Item_Det();
            Load_Bank_Details();

            if (Session["Supp_Advance_Trans_No"] == null)
            {
                SessionAdvanceTransNo = "";
            }
            else
            {
                SessionAdvanceTransNo = Session["Supp_Advance_Trans_No"].ToString();
                txtTrans_No.Text = SessionAdvanceTransNo;
                btnSearch_Click(sender, e);
            }
        }


       
        Load_Data_Empty_Supp1();

        //Load_Data_Enquiry_Grid();
    }


    private void Load_Bank_Details()
    {
        DataTable DT_Bank = new DataTable();
        string query = "Select Distinct BankName from MstBank where Ccode='" + SessionCcode + "'";
        query = query + " And Lcode='" + SessionLcode + "'";
        DT_Bank = objdata.RptEmployeeMultipleDetails(query);
        txtBankName.Items.Clear();

        if (DT_Bank.Rows.Count != 0)
        {
            for (int i = 0; i < DT_Bank.Rows.Count; i++)
            {
                txtBankName.Items.Add(DT_Bank.Rows[i]["BankName"].ToString());
            }
        }

        query = "Select Distinct BranchName from MstBank where Ccode='" + SessionCcode + "'";
        query = query + " And Lcode='" + SessionLcode + "'";
        DT_Bank = objdata.RptEmployeeMultipleDetails(query);
        txtBranch.Items.Clear();

        if (DT_Bank.Rows.Count != 0)
        {
            for (int i = 0; i < DT_Bank.Rows.Count; i++)
            {
                txtBranch.Items.Add(DT_Bank.Rows[i]["BranchName"].ToString());
            }
        }

    }

    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier1.DataSource = DT;
        Repeater_Supplier1.DataBind();
        Repeater_Supplier1.Visible = true;

    }

    protected void btnSupp1_Click(object sender, EventArgs e)
    {
        modalPop_Supp1.Show();
    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {

        txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
        txtSupplierName.Text = Convert.ToString(e.CommandName);
     
    }

    protected void BtnSave_Click(object sender, EventArgs e)
    {
       
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;


        //Check Supplier Name
        query = "Select * from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And SuppCode='" + txtSuppCodehide.Value + "' And SuppName='" + txtSupplierName.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Supplier Name...');", true);
        }


        if (txtTotAmt.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the advance amount...');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (BtnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "2", "Supplier Advance");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Supplier Advance Entry...');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "2", "Supplier Advance");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Supplier Advance Entry...');", true);
            }
        }

        //User Rights Check End

        //Auto generate Transaction Function Call

        if (BtnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Supplier Advance", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtTrans_No.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            query = "Select * from Unplanned_Receipt_Supp_Advance_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTrans_No.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Unplanned_Receipt_Supp_Advance_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTrans_No.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                
            }


            //Response.Write(strValue);   	

            //Insert Main Table  

            query = "Insert Into Unplanned_Receipt_Supp_Advance_Main(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Supp_Code,Supp_Name,";
            query = query + " ChequeNo,ChequeDate,BankName,BranchName,Description,AdvAmount,UserID,UserName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtTrans_No.Text + "','" + txtTransDate.Text + "','" + txtSuppCodehide.Value + "','" + txtSupplierName.Text + "',";
            query = query + " '" + txtChequeNo.Text + "','" + txtChequeDate.Text + "','" + txtBankName.Text + "','" + txtBranch.Text + "',";
            query = query + " '" + txtDescription.Text + "','" + txtTotAmt.Text + "',";
            query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

           
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Advance Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Advance Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Supp_Advance_Trans_No"] = txtTrans_No.Text;
            BtnSave.Text = "Update";

            //Load_Data_Enquiry_Grid();
        }
    

    }
    protected void BtnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Session.Remove("Supp_Advance_Trans_No");
        Response.Redirect("Supplier_Advance_Main.aspx");
    }
    private void Clear_All_Field()
    {
        txtTrans_No.Text = ""; txtTransDate.Text = "";
        txtSuppCodehide.Value = ""; txtSupplierName.Text = "";
        txtChequeNo.Text = ""; txtChequeDate.Text = "";
        txtDescription.Text = "";
       
        txtTotAmt.Text = "0.00";

        BtnSave.Text = "Save";

        Session.Remove("Supp_Advance_Trans_No");
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Supplier Bill Details
        string query = "";
        int intI = 0;
        DataTable Main_DT = new DataTable();
        query = "Select * from Unplanned_Receipt_Supp_Advance_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtTrans_No.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtTransDate.Text = Main_DT.Rows[0]["Trans_Date"].ToString();
            txtSuppCodehide.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
            txtSupplierName.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtChequeNo.Text = Main_DT.Rows[0]["ChequeNo"].ToString();
            txtChequeDate.Text = Main_DT.Rows[0]["ChequeDate"].ToString();

            txtBankName.Text = Main_DT.Rows[0]["BankName"].ToString();
            txtBranch.Text = Main_DT.Rows[0]["BranchName"].ToString();

            txtDescription.Text = Main_DT.Rows[0]["Description"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["AdvAmount"].ToString();

            

            BtnSave.Text = "Update";

          
        }
        else
        {
            Clear_All_Field();
        }
    }

}
