﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Order_general_po_main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    bool ErrFlag = false;
    string RptName = "";
    string ButtonName = "";
    Boolean Errflag = true;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: General Purchase Order";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Load_Data_Enquiry_Grid();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "6", "2", "General Purchase Order");
        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New General Purchase Order..');", true);
        }
        else
        {
            Session.Remove("Gen_PO_No");
            Response.Redirect("general_po.aspx");
        }
    }

    protected void GridPrintClick(object sender, CommandEventArgs e)
    {
        ButtonName = "Issue Slip";

        RptName = "General Purchase Order Invoice Format";

        string UPRecNo = e.CommandName.ToString();

        string FDate = "";
        string TDate = "";
        string SupQtNo = "";
        string RateType = "";
        string SupplierName = "";
        string ItemName = "";
        string DeptName = "";


        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?RateType=" + RateType + "&DeptName="+ DeptName + "&SupQtNo=" + SupQtNo + "&SupplierName="+ SupplierName + "&GenPurOrdNo=" + UPRecNo + "&ItemName=" + ItemName  + "&FromDate="+ FDate +"&ToDate=" + TDate + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");


        //ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?RptName=" + RptName + "&InvoiceNo=" + e.CommandName.ToString(), "_blank", "");
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
             string query = "";
             bool ErrFlag = false;

            DataTable dtdpurchase = new DataTable();
            query = "select PO_Status from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Gen_PO_No='" + e.CommandName.ToString() + "'";
            dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
            string status = dtdpurchase.Rows[0]["PO_Status"].ToString();

            if (status == "" || status == "0")
            {
                string Enquiry_No_Str = e.CommandName.ToString();
                Session.Remove("Gen_PO_No");
                Session["Gen_PO_No"] = Enquiry_No_Str;
                Response.Redirect("general_po.aspx");
            }
            else if (status == "2")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('General Purchase Order Already Rejected..');", true);
            }
            else if (status == "3")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('General Purchase Order put in pending..');", true);
            }
            else if (status == "1")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approved General Purchase Order Cant Edit..');", true);
            }
          
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "6", "2", "General Purchase Order");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New General Purchase Order..');", true);
        }
        //User Rights Check End

        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        query = "Select * from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Gen_PO_No='" + e.CommandName.ToString() + "' And PO_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Delete General Purchase Order Details Already Approved..');", true);
        }
        //Check With Already Approved End

        if (!ErrFlag)
        {

            DataTable dtdpurchase = new DataTable();
            query = "select PO_Status from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Gen_PO_No='" + e.CommandName.ToString() + "'";
            dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
            string status = dtdpurchase.Rows[0]["PO_Status"].ToString();

            if (status == "" || status == "0")
            {
                DataTable DT = new DataTable();
                query = "Select * from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Gen_PO_No='" + e.CommandName.ToString() + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    //Delete Main Table
                    query = "Delete from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Gen_PO_No='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    //Delete Main Sub Table
                    query = "Delete from General_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Gen_PO_No='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('General Purchase Order Details Deleted Successfully');", true);
                    Load_Data_Enquiry_Grid();  
                }
            }
            else if (status == "2")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('General Purchase Order Already Rejected..');", true);
            }
            else if (status == "3")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('General Purchase Order put in pending..');", true);
            }
          
        }
    }

    private void Load_Data_Enquiry_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Gen_PO_No,Gen_PO_Date,Supp_Name,DeptName,DeliveryMode from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }
}
