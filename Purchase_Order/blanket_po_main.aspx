﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="blanket_po_main.aspx.cs" Inherits="Purchase_Order_blanket_po_main" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable_Normal').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable_Normal').dataTable();               
            }
        });
    };
</script>


<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
      $find('Item_Close').hide();
     }
   } 
 </script>
<!--Ajax popup End-->



<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <h4><li class="active">Blanket Purchase Order</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-9">
			    <div class="panel panel-white">
			        <div class="panel panel-primary">
				        <div class="panel-heading clearfix">
					        <h4 class="panel-title">Blanket Purchase Order</h4>
				        </div>
				    </div>
				    <form class="form-horizontal">
				        <div class="panel-body">
				            <div class="col-md-12">
					            <div class="row">
					                <div class="form-group col-md-4">
				                        <asp:Button ID="btnAddNew" class="btn btn-success"  runat="server" Text="Add New" OnClick="btnAddNew_Click"/>
				                    </div>
				                </div>
				            </div>
				            <div class="form-group row"></div>
                            <div class="form-group row"></div>
				            <!-- table start -->
					        <div class="panel-body">
                                <div>
					                <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
					                    <HeaderTemplate>
                                            <table id="example" class="display table">
                                                <thead>
                                                    <tr>
                                                        <th>Blanket. PO.No</th>
                                                        <th>Date</th>
                                                        <th>Supplier</th>
                                                        <th>Approved/CancelledBy</th>
                                                        <th>Mode</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Eval("Blanket_PO_No")%></td>
                                                <td><%# Eval("Blanket_PO_Date")%></td>
                                                <td><%# Eval("Supp_Name")%></td>
                                                <td><%# Eval("Approvedby")%></td>
                                                <td>
                                                    <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("Blanket_PO_No")%>'>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("Blanket_PO_No")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Blanket Purchase Order details?');">
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
					                </asp:Repeater>
					            </div>
				        </div>
					        <!-- table End -->
					        
					        <div class="clearfix"></div>				    
				            <div class="timeline-options">
				                <h4 class="panel-title">Add Item Schedule</h4>
				            </div>
				            <div class="col-md-12">
					            <div class="row">
					                <asp:UpdatePanel ID="UpdatePanelSchedule" runat="server">
					                    <ContentTemplate>
					                        <div class="form-group col-md-3">
					                            <label for="exampleInputName">Blanket No</label>
                                                <asp:DropDownList ID="txtBlanketNo" runat="server" class="js-states form-control" tabindex="-1"
                                                    onselectedindexchanged="txtBlanketNo_SelectedIndexChanged" AutoPostBack="True">
                                                </asp:DropDownList>
					                        </div>
					                    </ContentTemplate>
					                </asp:UpdatePanel>
					                <div class="form-group col-md-3">
					                    <label for="exampleInputName">Blanket Date</label>
					                    <asp:TextBox ID="txtBlanketDate" class="form-control" runat="server"></asp:TextBox>
					                </div>
					                <div class="form-group col-md-4">
					                    <label for="exampleInputName">Item Name</label>
					                    <asp:TextBox ID="txtItemName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					                     <asp:Button ID="btnItemCode" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnItemCode_Click"/>
                                    
                                    <cc1:ModalPopupExtender ID="modalPop_ItemCode"  runat="server" PopupControlID="Panel5" TargetControlID="btnItemCode"
                       CancelControlID="BtnItemClose" BackgroundCssClass="modalBackground" BehaviorID="Item_Close">
                    </cc1:ModalPopupExtender>
                         <asp:Panel ID="Panel5" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Item Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_ItemCode_Normal" runat="server" EnableViewState="false" Visible="false">
					    <HeaderTemplate>
                         <table id="ItemTable_Normal" class="display table">
                         <thead >
                         <tr>
                         <th>ItemCode</th>
                         <th>ItemName</th>
                         <th>OrderQty</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ItemCode")%></td>
                          <td><%# Eval("ItemName")%></td>
                          <td><%# Eval("OrderQty")%></td>                                               
                          <td>
                          <asp:LinkButton ID="btnEdit_ItemCode" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ItemCode" CommandArgument='<%# Eval("ItemCode")%>' CommandName='<%# Eval("ItemName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnItemClose" class="btn btn-rounded" runat="server" Text="Close" />
					     </div>
					     </asp:Panel>
					
					                    
					                    
					                    
					                    <asp:HiddenField ID="txtItemCodeHide" runat="server" />
					                    <asp:RequiredFieldValidator ControlToValidate="txtItemName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
					                </div>
					                <div class="form-group col-md-2">
					                    <label for="exampleInputName">Qty</label>
					                    <asp:TextBox ID="txtQty" class="form-control" runat="server"></asp:TextBox>
					                    <asp:RequiredFieldValidator ControlToValidate="txtQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtQty" ValidChars="0123456789.">
                                        </cc1:FilteredTextBoxExtender>
					                </div>
					                <div class="form-group col-md-3">
				                        <label for="exampleInputName">DeliveryDate</label>
				                        <asp:TextBox ID="txtDeliveryDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
				                        <asp:RequiredFieldValidator ControlToValidate="txtDeliveryDate" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                            TargetControlID="txtDeliveryDate" ValidChars="0123456789./">
                                        </cc1:FilteredTextBoxExtender>
				                    </div>
				                    <div class="form-group col-md-4">
					                    <label for="exampleInputName">Remarks</label>
					                    <asp:TextBox ID="txtRemarks" class="form-control" runat="server"></asp:TextBox>
					                </div>
					                <div class="form-group col-md-1">
					                    <br />
					                    <asp:Button ID="btnAddItem" Width="50" Height="30" class="btn-success"  runat="server" Text="Add" ValidationGroup="Item_Validate_Field"  OnClick="btnAddItem_Click"/>
					                </div>
					            </div>
					        </div>
					        
					        <!-- table start -->
					        <div class="col-md-12">
					            <div class="row">
					                <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                            <HeaderTemplate>
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>S.No</th>
                                                        <th>Item Code</th>
                                                        <th>Item Name</th>
                                                        <th>Qty</th>
                                                        <th>Delivery Date</th>
                                                        <th>Remarks</th>
                                                        <th>Mode</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 %></td>
                                                <td><%# Eval("ItemCode")%></td>
                                                <td><%# Eval("ItemName")%></td>
                                                <td><%# Eval("Qty")%></td>
                                                <td><%# Eval("DeliveryDate")%></td>
                                                <td><%# Eval("Remarks")%></td>
                                                <td>
                                                    <asp:LinkButton ID="btnEditItemGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditScheduleClick" CommandArgument="Edit" CommandName='<%# Eval("ItemCode")%>'>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteItemGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteScheduleClick" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Schedule Item details?');">
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
			                        </asp:Repeater>
					            </div>
					        </div>
					        <!-- table End -->
					        
				            
				        </div><!-- panel body end -->
				    </form>
				</div><!-- panel white end -->
			</div><!-- col-9 end -->
			
			<!-- Dashboard start -->
			<div class="col-lg-3 col-md-6">
			    <div class="panel panel-white" style="height: 100%;">
                    <div class="panel-heading">
                        <h4 class="panel-title">Dashboard Details</h4>
                        <div class="panel-control"></div>
                    </div>
                    <div class="panel-body"></div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                        </div>
                    </div>
                </div>
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                        </div>
                    </div>
                </div>
            </div>
			<!-- Dashboard End -->
			<div class="col-md-2"></div>
		</div> <!-- col 12 end -->
    </div><!-- row end -->
</div><!-- main-wrapper end -->

</asp:Content>

