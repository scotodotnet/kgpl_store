﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Order_purchase_return : System.Web.UI.Page
{
 
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionPOReturnNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Purchase Return";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_Data_Empty_Dept();
            Load_Data_Empty_WareHouse();
            Load_Data_Empty_ZoneName();
            Load_Data_Empty_BinName();
            if (Session["Pur_Return_No"] == null)
            {
                SessionPOReturnNo = "";
            }
            else
            {
                SessionPOReturnNo = Session["Pur_Return_No"].ToString();
                txtPurchaseReturnNo.Text = SessionPOReturnNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();

        
        Load_Data_Empty_Supp1();
        Load_Data_Empty_ItemCode();
        
       
    }

    
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";
        
        if (txtReturnQuantity.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Return Qty...');", true);
        }
        if (txtValue.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Rate...');", true);
        }

        //check with Item Code And Item Name 
        query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCodeHide.Value + "' And ItemShortName='" + txtItemName.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
        }

        if (!ErrFlag)
        {
            //UOM Code get
            string UOM_Code_Str = qry_dt.Rows[0]["PurchaseUOM"].ToString();
            string Line_Total = "";

            Line_Total = ((Convert.ToDecimal(txtReturnQuantity.Text)) * Convert.ToDecimal(txtValue.Text)).ToString();
            Line_Total = (Math.Round(Convert.ToDecimal(Line_Total), 0, MidpointRounding.AwayFromZero)).ToString();

            
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCodeHide.Value.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["ItemCode"] = txtItemCodeHide.Value;
                    dr["ItemName"] = txtItemName.Text;
                    dr["ReturnQty"] = txtReturnQuantity.Text;
                    dr["Pur_Value"] = txtValue.Text;
                    dr["zoneName"] = txtBinName.SelectedItem.Text;
                    dr["BinName"] = txtBinName.SelectedItem.Text;
                    dr["Remarks"] = txtRemarks.Text;
                    dr["Total"] = Line_Total.ToString();
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    txtItemCodeHide.Value = ""; 
                    txtItemName.Text = "";
                    txtReturnQuantity.Text = "";
                    txtValue.Text = "";
                    txtZoneName.SelectedValue = null;
                    //txtZoneName.SelectedItem.Text = "-Select-";
                    txtBinName.SelectedValue = null;
                    txtRemarks.Text = "";
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["ItemCode"] = txtItemCodeHide.Value;
                dr["ItemName"] = txtItemName.Text;
                dr["ReturnQty"] = txtReturnQuantity.Text;
                dr["Pur_Value"] = txtValue.Text;
                dr["zoneName"] = txtBinName.SelectedItem.Text;
                dr["BinName"] = txtBinName.SelectedItem.Text;
                dr["Remarks"] = txtRemarks.Text;
                dr["Total"] = Line_Total.ToString();
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                txtItemCodeHide.Value = "";
                txtItemName.Text = "";
                txtReturnQuantity.Text = "";
                txtValue.Text = "";
                txtZoneName.SelectedValue = null;
                //txtZoneName.SelectedItem.Text = "-Select-";
                txtBinName.SelectedValue = null;
                txtRemarks.Text = "";
            }

            Load_OLD_data();
        }
    }
    
  
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("ReturnQty", typeof(string)));
        dt.Columns.Add(new DataColumn("zoneName", typeof(string)));
        dt.Columns.Add(new DataColumn("Pur_Value", typeof(string)));
        dt.Columns.Add(new DataColumn("BinName", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));
        dt.Columns.Add(new DataColumn("Total", typeof(string)));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;
        //dt = Repeater1.DataSource;
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("purchase_return_main.aspx");
    }

    private void Clear_All_Field()
    {
        txtBinName.SelectedValue = null;
        txtDepartmentName.SelectedValue = "-Select-";
        //txtDeptCodeHide.ToString() = "";
        txtDocDate.Text = "";
        //txtItemCodeHide.ToString() = "";
        txtItemName.Text = "";
        txtNote.Text = "";
        txtOthers.Text = "";
        txtPurchaseReturnDate.Text = "";
        txtPurchaseReturnNo.Text = "";
        txtRefDocNo.Text = "";
        txtRemarks.Text = "";
        txtReturnQuantity.Text = "";
        //txtSuppCodehide.Text = "";
        txtSupplierName.Text = "";
        txtValue.Text = "";
        //txtWarehouseCodeHide.ToString() = "";
        txtWarehouseName.SelectedValue ="-Select-";
        txtZoneName.SelectedValue = null;
      
        Load_Data_Empty_ZoneName();
        Load_Data_Empty_BinName();

        BtnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Pur_Return_No");

    }

    private void Delivery_Mode_Add()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        //txtDeliveryMode.Items.Clear();
        //query = "Select * from MstDeliveryMode where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeliveryMode Asc";
        //Main_DT = objdata.RptEmployeeMultipleDetails(query);
        //txtDeliveryMode.Items.Add("-Select-");
        //for (int i = 0; i < Main_DT.Rows.Count; i++)
        //{
        //    txtDeliveryMode.Items.Add(Main_DT.Rows[i]["DeliveryMode"].ToString());
        //}
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "2", "Purchase Return");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Purchase Return...');", true);
        //}
        //User Rights Check End

        query = "Select * from Purc_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Return_No='" + txtPurchaseReturnNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Purchase Return Details..');", true);
        }

        query = "Select * from Purc_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Return_No='" + txtPurchaseReturnNo.Text + "' And PO_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Return Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            query = "Update Purc_Return_Main set PO_Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And Pur_Return_No='" + txtPurchaseReturnNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            Stock_Add();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Return Details Approved Successfully..');", true);
        }
    }

    private void Stock_Add()
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();

             
        query = "Select * from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtPurchaseReturnNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            query = "Delete from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtPurchaseReturnNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }


        DateTime transDate = Convert.ToDateTime(txtPurchaseReturnDate.Text);

        //Insert Stock_Transaction_Ledger
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {

           
                //query = "Insert Into Stock_Transaction_Ledger(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
                //query = query + " Issue_Value,Issue_Return_Qty,Issue_Return_Value,GP_IN_Qty,GP_IN_Value,GP_OUT_Qty,GP_OUT_Value,Transfer_Qty,Transfer_Value,Damage_Qty,Damage_Value,Spares_ReSales_Qty,Spares_ReSales_Value,WarehouseCode,WarehouseName,ZoneName,BinName,Supp_Code,";
                //query = query + " Supp_Name,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                //query = query + " '" + SessionFinYearVal + "','" + txtIssueNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtDate.Text + "','MATERIAL ISSUE','" + dt.Rows[i]["DeptCode"].ToString() + "','" + dt.Rows[i]["DeptName"].ToString() + "','" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "',";
                //query = query + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                //query = query + " '0.0','0.0','0.0','0.0','0.0','0.0','" + dt.Rows[i]["IssueQty"].ToString() + "','" + dt.Rows[i]["Value"].ToString() + "','0.0',";
                //query = query + " '0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','" + dt.Rows[i]["WarehouseCode"].ToString() + "',";
                //query = query + " '" + dt.Rows[i]["WarehouseName"].ToString() + "','" + dt.Rows[i]["ZoneName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "',";
                //query = query + " '','','" + SessionUserID + "','" + SessionUserName + "')";
                //objdata.RptEmployeeMultipleDetails(query);


                query = "Insert Into Stock_Transaction_Ledger(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
                query = query + " Trans_Type,DeptCode,DeptName,CostCenterCode,CostCenterName,ItemCode,ItemName,Open_Qty,Open_Value,PO_Qty,PO_Value,General_Qty,General_Value,Issue_Qty,";
                query = query + " Issue_Value,Issue_Return_Qty,Issue_Return_Value,GP_IN_Qty,GP_IN_Value,GP_OUT_Qty,GP_OUT_Value,Transfer_Qty,Transfer_Value,Damage_Qty,Damage_Value,Spares_ReSales_Qty,Spares_ReSales_Value,WarehouseCode,WarehouseName,ZoneName,BinName,Supp_Code,";
                query = query + " Supp_Name,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
                query = query + " '" + SessionFinYearVal + "','" + txtPurchaseReturnNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtPurchaseReturnDate.Text + "','PURCHASE RETURN','','','','',";
                query = query + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                query = query + " '0.0','0.0','0.0','0.0','0.0','0.0','" + dt.Rows[i]["ReturnQty"].ToString() + "','" + dt.Rows[i]["Pur_Value"].ToString() + "','0.0',";
                query = query + " '0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','',";
                query = query + " '','" + dt.Rows[i]["BinName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "',";
                query = query + " '','','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
                CommonClass_Function.cls_Trans_Type = "PURCHASE RETURN";
                CommonClass_Function.cls_ItemCode = dt.Rows[i]["ItemCode"].ToString();
                CommonClass_Function.cls_ItemName = dt.Rows[i]["ItemName"].ToString();
                CommonClass_Function.cls_Add_Qty = "0.0";
                CommonClass_Function.cls_Add_Value = "0.0";
                CommonClass_Function.cls_Minus_Qty = dt.Rows[i]["ReturnQty"].ToString();
                CommonClass_Function.cls_Minus_Value = dt.Rows[i]["Pur_Value"].ToString();
                CommonClass_Function.cls_DeptCode = "";
                CommonClass_Function.cls_DeptName = "";
                CommonClass_Function.cls_CostCenterCode = "";
                CommonClass_Function.cls_CostCenterName = "";
                CommonClass_Function.cls_WarehouseCode = "";
                CommonClass_Function.cls_WarehouseName = "";
                CommonClass_Function.cls_ZoneName = dt.Rows[i]["BinName"].ToString();
                CommonClass_Function.cls_BinName = dt.Rows[i]["BinName"].ToString();
                CommonClass_Function.cls_Supp_Code = "";
                CommonClass_Function.cls_Supp_Name = "";
                CommonClass_Function.cls_UserID = SessionUserID;
                CommonClass_Function.cls_UserName = SessionUserName;
                //CommonClass_Function.cls_Stock_Qty = "";
                //CommonClass_Function.cls_Stock_Value = "";

                CommonClass_Function.Stock_Ledger_And_Current_Stock_Add(SessionCcode, SessionLcode, SessionFinYearCode, SessionFinYearVal, txtPurchaseReturnNo.Text, txtPurchaseReturnDate.Text);
            


        }




    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        int intI = 0;
        DataTable Main_DT = new DataTable();
        query = "Select * from Purc_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Return_No='" + txtPurchaseReturnNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtPurchaseReturnDate.Text = Main_DT.Rows[0]["Pur_Return_Date"].ToString();
            txtWarehouseName.SelectedValue = Main_DT.Rows[0]["WarehouseCode"].ToString();
            txtWarehouseName.SelectedItem.Text = Main_DT.Rows[0]["WarehouseName"].ToString();
            txtSuppCodehide.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
            txtSupplierName.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtDepartmentName.SelectedValue= Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtDocDate.Text = Main_DT.Rows[0]["DCDate"].ToString();
            txtRefDocNo.Text = Main_DT.Rows[0]["DCNo"].ToString();
            txtNote.Text = Main_DT.Rows[0]["Note"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();

            //Std_Purchase_Return_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select ItemCode,ItemName,ReturnQty,Pur_Value,zoneName,BinName,Remarks,Total from Purc_Return_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Return_No='" + txtPurchaseReturnNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            BtnSave.Text = "Update";

            //Std_Purchase_Order_Main_Sub Table Load
            //DataTable dt = new DataTable();
            //query = "Select ItemCode,ItemName,UOMCode,OrderQty,Rate,Discount,Tax,OtherCharge,Remarks,LineTotal from General_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Gen_PO_No='" + txtPurchaseReturnNo.Text + "'";
            //dt = objdata.RptEmployeeMultipleDetails(query);
            //ViewState["ItemTable"] = dt;
            //Repeater1.DataSource = dt;
            //Repeater1.DataBind();

            //BtnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }
   
    protected void BtnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }

        //Check Department Name
        query = "Select * from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + txtDepartmentName.SelectedValue + "' And DeptName='" + txtDepartmentName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Department Name...');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (BtnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Purchase Return");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Purchase Return...');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Purchase Return");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Purchase Return...');", true);
            }
        }

        //User Rights Check End

        //Auto generate Transaction Function Call

        if (BtnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Purchase Return", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtPurchaseReturnNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            query = "Select * from Purc_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Return_No='" + txtPurchaseReturnNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Purc_Return_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Return_No='" + txtPurchaseReturnNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Purc_Return_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Return_No='" + txtPurchaseReturnNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }


            //Response.Write(strValue);   	

            //Insert Main Table  

            query = "Insert Into Purc_Return_Main(Ccode,Lcode,FinYearCode,FinYearVal,Pur_Return_No,Pur_Return_Date,Supp_Code,Supp_Name,";
            query = query + " DCNo,DCDate,WarehouseCode,WarehouseName,DeptCode,DeptName,Note,Others,UserID,UserName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtPurchaseReturnNo.Text + "','" + txtPurchaseReturnDate.Text + "','" + txtSuppCodehide.Value + "','" + txtSupplierName.Text + "',";
            query = query + " '" + txtRefDocNo.Text + "','" + txtDocDate.Text + "',";
            query = query + " '" + txtWarehouseName.SelectedValue + "','" + txtWarehouseName.SelectedItem.Text + "',";
            query = query + " '" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "',";
            query = query + " '" + txtNote.Text + "','" + txtOthers.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
             objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Purc_Return_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Pur_Return_No,Pur_Return_Date,ItemCode,ItemName,ReturnQty,Pur_Value,zoneName,BinName,Remarks,Total,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtPurchaseReturnNo.Text + "','" + txtPurchaseReturnDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["ItemName"].ToString() + "','" + dt.Rows[i]["ReturnQty"].ToString() + "','" + dt.Rows[i]["Pur_Value"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["BinName"].ToString() + "','" + dt.Rows[i]["Remarks"].ToString() + "','" + dt.Rows[i]["Total"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Return Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Return Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Gen_PO_No"] = txtPurchaseReturnNo.Text;
            BtnSave.Text = "Update";

            //Load_Data_Enquiry_Grid();
        }

    }


    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        //modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();




    }
    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtWarehouseName.Text = Convert.ToString(e.CommandName);

        Load_Data_Empty_ZoneName();
        Load_Data_Empty_BinName();
    }

    protected void txtWarehouseName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_ZoneName();
        Load_Data_Empty_BinName();
    }
    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnZoneName_Click(object sender, EventArgs e)
    {
        //modalPop_ZoneName.Show();
    }

    private void Load_Data_Empty_ZoneName()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtZoneName.Items.Clear();
        query = "Select ZoneName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And WarehouseCode='" + txtWarehouseName.SelectedValue + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtZoneName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["ZoneName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtZoneName.DataTextField = "ZoneName";

        txtZoneName.DataBind();


    }
    protected void txtZoneName_SelectedIndexChanged(object sender, EventArgs e)
    {

        Load_Data_Empty_BinName();
    }
    protected void GridViewClick_ZoneName(object sender, CommandEventArgs e)
    {
        txtZoneName.Text = Convert.ToString(e.CommandArgument);

        Load_Data_Empty_BinName();
    }

    protected void btnBinName_Click(object sender, EventArgs e)
    {
       // modalPop_BinName.Show();
    }

    private void Load_Data_Empty_BinName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtBinName.Items.Clear();
        query = "Select BinName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And WarehouseCode='" + txtWarehouseName.SelectedValue + "'"; //And ZoneName='" + txtZoneName.SelectedItem.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtBinName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();

        dr["BinName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtBinName.DataTextField = "BinName";

        txtBinName.DataBind();

    }

    protected void GridViewClick_BinName(object sender, CommandEventArgs e)
    {
        txtBinName.Text = Convert.ToString(e.CommandArgument);
    }

    protected void btnSupp1_Click(object sender, EventArgs e)
    {
        modalPop_Supp1.Show();
    }
    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier1.DataSource = DT;
        Repeater_Supplier1.DataBind();
        Repeater_Supplier1.Visible = true;

    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {

        txtSuppCodehide.Value = Convert.ToString(e.CommandArgument);
        txtSupplierName.Text = Convert.ToString(e.CommandName);
       
    }
    protected void btnDept_Click(object sender, EventArgs e)
    {
        //modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

}
