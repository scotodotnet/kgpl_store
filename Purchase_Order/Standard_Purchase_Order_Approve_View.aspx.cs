﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;


public partial class Purchase_Order_Standard_Purchase_Order_Approve_View : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionStdPOOrderNo;
    string query;
    DataTable Std_Pur_Main = new DataTable();
    DataTable Std_Pur_Sub = new DataTable();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
     
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Standard Purchase Order Approve View";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
            //this.BindRepeater();
            Initial_Data_Referesh();
            if (Session["Std_PO"] == null)
            {
                SessionStdPOOrderNo = "";
            }
            else
            {
                SessionStdPOOrderNo = Session["Std_PO"].ToString();
                txtStdOrderNo.Text = SessionStdPOOrderNo;
                btnSearch_Click(sender, e);
            }
        }
        Load_OLD_data();
    }
    protected void DisplayOrders(object sender, EventArgs e)
    {
        RepeaterItem item = (sender as Button).NamingContainer as RepeaterItem;
        string id = (item.FindControl("lblCustomerId") as Label).Text;
        BindOrders(id);
        mpe.Show();
    }
   
    private void BindOrders(string customerId)
    {
        DataTable DT = new DataTable();

        query = "SELECT Top 5 PS.ItemCode,PS.ItemName,PS.PO_Receipt_Date,PS.ItemRate FROM Pur_Order_Receipt_Main PM inner join ";
        query = query + " Pur_Order_Receipt_Main_Sub PS on PM.PO_Receipt_No=PS.PO_Receipt_No where PS.ItemName='" + customerId.ToString() + "'";
        query = query + " And PS.Ccode='" + SessionCcode + "' And PS.Lcode='" + SessionLcode + "' and PM.Ccode='" + SessionCcode + "'";
        query = query + " And PM.Lcode='" + SessionLcode + "'";
        query = query + " And PS.Pur_Order_No!='" + txtStdOrderNo.Text + "'";
        query = query + "ORDER BY PS.PO_Receipt_Date DESC";
        DT = objdata.RptEmployeeMultipleDetails(query);

        rptOrders.DataSource = DT;
        rptOrders.DataBind();
    }
    //public void Load_OLD_data()
    //{
    //    query = "select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + SessionStdPOOrderNo + "'";
    //    Std_Pur_Main = objdata.RptEmployeeMultipleDetails(query);
    //    if (Std_Pur_Main.Rows.Count > 0)
    //    {
    //        txtStdOrderNo.Text = Std_Pur_Main.Rows[0]["Std_PO_No"].ToString();
    //        txtDate.Text = Std_Pur_Main.Rows[0]["Std_PO_Date"].ToString();
    //        txtSupplierName.Text = Std_Pur_Main.Rows[0]["Supp_Name"].ToString();
    //        txtPaymentMode.Text = Std_Pur_Main.Rows[0]["PaymentMode"].ToString();
    //        txtDescription.Text = Std_Pur_Main.Rows[0]["Description"].ToString();
    //        txtNote.Text = Std_Pur_Main.Rows[0]["Note"].ToString();
    //        txtOthers.Text = Std_Pur_Main.Rows[0]["Others"].ToString();
    //        txtTotAmt.Text = Std_Pur_Main.Rows[0]["TotalAmt"].ToString();
    //        txtTax.Text = Std_Pur_Main.Rows[0]["TaxPer"].ToString();
    //        txtTaxAmt.Text = Std_Pur_Main.Rows[0]["TaxAmount"].ToString();
    //        txtDiscount.Text = Std_Pur_Main.Rows[0]["Discount"].ToString();
    //        txtOtherCharge.Text = Std_Pur_Main.Rows[0]["OtherCharge"].ToString();
    //        txtNetAmt.Text = Std_Pur_Main.Rows[0]["NetAmount"].ToString();
    //    }
    //}
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Std Purchase Order");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Std Purchase Order...');", true);
        //}
        //User Rights Check End

        //query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
        //DT_Check = objdata.RptEmployeeMultipleDetails(query);
        //if (DT_Check.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Std Purchase Order Details..');", true);
        //}


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + txtStdOrderNo.Text + "' And Status = '1'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Order Details Approved.. Cannot Cancel it..');", true);

            }
        }


        DataTable dtd_REqNo = new DataTable();

        if (!ErrFlag)
        {

            query = "Select * from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + txtStdOrderNo.Text + "' And Status = '2'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Order Details Already Cancelled..');", true);
            }
            if (!ErrFlag)
            {
                query = "Update Std_Purchase_Order_Approve set Status='2',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Transaction_No='" + txtStdOrderNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);


                query = "Update Std_Purchase_Order_Main set Others='" + txtOthers.Text + "',PO_Status='2',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "'";
                query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                query = query + "  And Std_PO_No='" + txtStdOrderNo.Text + "'";

                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Std Purchase Order Details Cancelled Successfully..');", true);
            }
        }
        Initial_Data_Referesh();
        Session["Std_PO"] = txtStdOrderNo.Text;
        btnSearch_Click(sender, e);
        
    }

    private void Clear_All_Field()
    {
        txtStdOrderNo.Text = ""; txtDate.Text = ""; txtSuppCodehide.Value = ""; txtSupplierName.Text = "";
        txtDescription.Text = ""; txtNote.Text = ""; txtOthers.Text = "";
       
        txtDiscount.Text = ""; txtTax.Text = ""; txtOtherCharge.Text = ""; 
        txtTaxAmt.Text = ""; txtTotAmt.Text = "0.0"; txtNetAmt.Text = "";
     
        Initial_Data_Referesh();
        Session.Remove("Std_PO_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "6", "2", "Std Purchase Order");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Std Purchase Order...');", true);
        //}
        //User Rights Check End

        query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Std Purchase Order Details..');", true);
        }


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + txtStdOrderNo.Text + "' And Status = '2'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Order Details Cancelled.. Cannot Approved it..');", true);
                
            }
        }
        

        DataTable dtd_REqNo = new DataTable();

        if (!ErrFlag)
        {

            query = "Select * from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + txtStdOrderNo.Text + "' And Status = '1'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Order Details Already Approved..');", true);
            }
            if (!ErrFlag)
            {
                query = "Update Std_Purchase_Order_Approve set Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Transaction_No='" + txtStdOrderNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);


                query = "Update Std_Purchase_Order_Main set Others='" + txtOthers.Text + "',PO_Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "'";
                query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                query = query + "  And Std_PO_No='" + txtStdOrderNo.Text + "'";

                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Std Purchase Order Details Approved Successfully..');", true);
            }
        }
        Initial_Data_Referesh();
        Session["Std_PO"] = txtStdOrderNo.Text;
        btnSearch_Click(sender, e);

    }

    private void Initial_Data_Referesh()
    {
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("OrderQty", typeof(string)));
        dt.Columns.Add(new DataColumn("Min/Max", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("LastPoDate", typeof(string)));
        dt.Columns.Add(new DataColumn("LastPoRate", typeof(string)));

        dt.Columns.Add(new DataColumn("ID", typeof(string)));
        
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Std_Purchase_Order_Main Table Load
        query = "select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
        Std_Pur_Main = objdata.RptEmployeeMultipleDetails(query);
        if (Std_Pur_Main.Rows.Count > 0)
        {
            txtStdOrderNo.Text = Std_Pur_Main.Rows[0]["Std_PO_No"].ToString();
            txtDate.Text = Std_Pur_Main.Rows[0]["Std_PO_Date"].ToString();
            txtSupplierName.Text = Std_Pur_Main.Rows[0]["Supp_Name"].ToString();
            txtPaymentMode.Text = Std_Pur_Main.Rows[0]["PaymentMode"].ToString();
            txtDescription.Text = Std_Pur_Main.Rows[0]["Description"].ToString();
            txtNote.Text = Std_Pur_Main.Rows[0]["Note"].ToString();
            txtOthers.Text = Std_Pur_Main.Rows[0]["Others"].ToString();
            txtTotAmt.Text = Std_Pur_Main.Rows[0]["TotalAmt"].ToString();
            txtTax.Text = Std_Pur_Main.Rows[0]["TaxPer"].ToString();
            txtTaxAmt.Text = Std_Pur_Main.Rows[0]["TaxAmount"].ToString();
            txtDiscount.Text = Std_Pur_Main.Rows[0]["Discount"].ToString();
            txtOtherCharge.Text = Std_Pur_Main.Rows[0]["OtherCharge"].ToString();
            txtNetAmt.Text = Std_Pur_Main.Rows[0]["NetAmount"].ToString();



            //Std_Purchase_Order_Main_Sub Table Load

            DataTable dtnew = new DataTable();
            DataTable dtOldData = new DataTable();
            DataTable dtStckData = new DataTable();
            DataTable dtItemData = new DataTable();
            string stckQty = "";
            string OrdrQtyChk = "";
            string MinChk = "";
            string MaxChk = "";

            query = "Select ItemCode,ItemName,OrderQty,ReuiredDate,Rate,LineTotal,'' as ID from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
            dtnew = objdata.RptEmployeeMultipleDetails(query);
            if (dtnew.Rows.Count > 0)
            {
                for (int i = 0; i < dtnew.Rows.Count; i++)
                {
                    dt.NewRow();
                    dt.Rows.Add();
                    string itemcode = dtnew.Rows[i]["ItemCode"].ToString();
                    string MinMaxStr = "";

                    //Item Maxmium/Minimum count
                    query = "select Stock_Qty from Stock_Current_All where ItemCode='" + itemcode + "'";
                    dtStckData = objdata.RptEmployeeMultipleDetails(query);
                    if (dtStckData.Rows.Count > 0)
                    {
                        stckQty = dtStckData.Rows[0]["Stock_Qty"].ToString();
                    }
                    else
                    {
                        stckQty = "0";
                    }
                    OrdrQtyChk = (Convert.ToDecimal(stckQty) + Convert.ToDecimal(dtnew.Rows[i]["OrderQty"])).ToString();

                    query = "select MaxQtyLevel,MinQtyLevel from MstItemMaster where ItemCode='" + itemcode + "'";
                    dtItemData = objdata.RptEmployeeMultipleDetails(query);

                    if (dtItemData.Rows.Count > 0)
                    {
                        MaxChk = dtItemData.Rows[0]["MaxQtyLevel"].ToString();
                        MinChk = dtItemData.Rows[0]["MinQtyLevel"].ToString();
                    }
                    if (MinChk == "0.00" || MaxChk == "0.00")
                    {
                        MinMaxStr = "--";
                    }
                    else if (Convert.ToDecimal(MinChk) > Convert.ToDecimal(OrdrQtyChk))
                    {
                        MinMaxStr = "Min";
                    }
                    else if (Convert.ToDecimal(MaxChk) < Convert.ToDecimal(OrdrQtyChk))
                    {
                        MinMaxStr = "Max";
                    }
                    //Item Maxmium/Minimum count

                    string itemname = dtnew.Rows[i]["ItemName"].ToString();
                    dt.Rows[i]["ItemCode"] = dtnew.Rows[i]["ItemCode"].ToString();
                    dt.Rows[i]["ItemName"] = dtnew.Rows[i]["ItemName"].ToString();
                    dt.Rows[i]["OrderQty"] = dtnew.Rows[i]["OrderQty"].ToString();
                    dt.Rows[i]["Min/Max"] = MinMaxStr;
                    dt.Rows[i]["Rate"] = dtnew.Rows[i]["Rate"].ToString();
                    dt.Rows[i]["LineTotal"] = dtnew.Rows[i]["LineTotal"].ToString();
                    query = "SELECT Top 1 * FROM Std_Purchase_Order_Main_Sub where ItemName='" + itemname.ToString() + "'";
                    query = query + "ORDER BY Std_PO_Date DESC";
                    dtOldData = objdata.RptEmployeeMultipleDetails(query);
                    if (dtOldData.Rows.Count > 0)
                    {
                        dt.Rows[i]["LastPoRate"] = dtOldData.Rows[0]["Rate"].ToString();
                        dt.Rows[i]["LastPoDate"] = dtOldData.Rows[0]["Std_PO_Date"].ToString();
                    }
                    else
                    {
                        dt.Rows[i]["LastPoRate"] = "0";
                        dt.Rows[i]["LastPoDate"] = "";
                    }

                    DataTable DT = new DataTable();
                    query = "SELECT Top 1 PS.ItemCode,PS.ItemName,PS.PO_Receipt_Date,PS.ItemRate FROM Pur_Order_Receipt_Main PM inner join ";
                    query = query + " Pur_Order_Receipt_Main_Sub PS on PM.PO_Receipt_No=PS.PO_Receipt_No where PS.ItemName='" + itemname.ToString() + "'";
                    query = query + " And PS.Ccode='" + SessionCcode + "' And PS.Lcode='" + SessionLcode + "' and PM.Ccode='" + SessionCcode + "'";
                    query = query + " And PM.Lcode='" + SessionLcode + "'";
                    query = query + " And PS.Pur_Order_No!='" + txtStdOrderNo.Text + "'";
                    query = query + "ORDER BY PS.PO_Receipt_Date DESC";
                    DT = objdata.RptEmployeeMultipleDetails(query);

                    if (DT.Rows.Count != 0)
                    {
                        if (Convert.ToDecimal(DT.Rows[0]["ItemRate"].ToString()) >= Convert.ToDecimal(dt.Rows[i]["Rate"].ToString()))
                        {
                            dt.Rows[i]["ID"] = "color :white; background: green; padding:4px;";
                        }
                        else
                        {
                            dt.Rows[i]["ID"] = "color :white; background: red; padding:4px;";
                        }
                    }
                    else
                    {
                        dt.Rows[i]["ID"] = "color :white; background: green; padding:4px;";
                    }
                    
                }

                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();


        }
        
            
       }
        else
        {
            Clear_All_Field();
        }
            
         
    }

    protected void btnPending_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Std Purchase Order");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Std Purchase Order...');", true);
        //}
        //User Rights Check End

        //query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.Text + "'";
        //DT_Check = objdata.RptEmployeeMultipleDetails(query);
        //if (DT_Check.Rows.Count == 0)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Std Purchase Order Details..');", true);
        //}


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + txtStdOrderNo.Text + "' And Status = '2'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Order Details Cancelled.. Cannot put to Pending..');", true);

            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + txtStdOrderNo.Text + "' And Status = '1'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Order Details Approved.. Cannot put to Pending..');", true);

            }
        }


        DataTable dtd_REqNo = new DataTable();

        if (!ErrFlag)
        {

            query = "Select * from Std_Purchase_Order_Approve where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + txtStdOrderNo.Text + "' And Status = '3'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Order Details Already in Pending..');", true);
            }
            if (!ErrFlag)
            {
                query = "Update Std_Purchase_Order_Approve set Status='3',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Transaction_No='" + txtStdOrderNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);


                query = "Update Std_Purchase_Order_Main set Others='" + txtOthers.Text + "',PO_Status='3',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "'";
                query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                query = query + "  And Std_PO_No='" + txtStdOrderNo.Text + "'";

                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Std Purchase Order Details Put in Pending..');", true);
            }
        }
        Initial_Data_Referesh();
        Session["Std_PO"] = txtStdOrderNo.Text;
        btnSearch_Click(sender, e);

    }
    protected void btnback_Click(object sender, EventArgs e)
    {
    Session.Remove("Std_PO");
    Response.Redirect("Standard_Purchase_Order_Approve.aspx");
    }

  
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        //Repeater_ItemCode.DataSource = DT;
        //Repeater_ItemCode.DataBind();
        //Repeater_ItemCode.Visible = true;

    }

    
}
