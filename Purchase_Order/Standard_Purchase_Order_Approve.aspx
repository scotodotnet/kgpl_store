﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Standard_Purchase_Order_Approve.aspx.cs" Inherits="Purchase_Order_Standard_Purchase_Order_Approve" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>
<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <h4><li class="active">Purchase Order Approve</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-9">
			    <div class="panel panel-white">
			        <div class="panel panel-primary">
				        <div class="panel-heading clearfix">
					        <h4 class="panel-title">Purchase Order Approve</h4>
				        </div>
				    </div>
				    <form class="form-horizontal">
				        <div class="panel-body">
				            <div class="form-group row">							 
						
					
						<label for="input-Default" class="col-sm-2 control-label">Request Status</label>
						<div class="col-sm-3">
                            <asp:DropDownList ID="txtRequestStatus" runat="server" class="js-states form-control"
                                onselectedindexchanged="txtRequestStatus_SelectedIndexChanged" AutoPostBack="True">
                            </asp:DropDownList>
                            
						</div>
                    </div>
				            <div class="form-group row"></div>
                            <div class="form-group row"></div>
				            <!-- table start -->
					        <div class="col-md-12">
					            <div class="row">
					                <asp:Repeater ID="Repeater_StdPurOrder" runat="server" EnableViewState="false">
					                    <HeaderTemplate>
                                            <table id="example" class="display table">
                                                <thead>
                                                    <tr>
                                               <th>Trans No</th>
                                                <th>Date</th>
                                                <th>Type</th>
                                                <th>Supplier</th>
                                                <th>Tot Qty</th>
                                                <th>Net Amt</th>
                                                <th>View</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                        <td><%# Eval("Transaction_No")%></td>
                                        <td><%# Eval("Date")%></td>
                                        <td><%# Eval("Type_PO")%></td>
                                        <td><%# Eval("SupplierName")%></td>
                                        <td><%# Eval("TotalQuantity")%></td>
                                        <td><%# Eval("TotalAmt")%></td>
                                                
                                                <td>
                                                    <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridViewClick" CommandArgument='<%# Eval("Transaction_No")%>' CommandName='<%# Eval("Type_PO")%>'>
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
					                </asp:Repeater>
					            </div>
					        </div>
					        <!-- table End -->
				        </div><!-- panel body end -->
				    </form>
				</div><!-- panel white end -->
			</div><!-- col-9 end -->
			
			<!-- Dashboard start -->
			<div class="col-lg-3 col-md-6">
			    <div class="panel panel-white" style="height: 100%;">
                    <div class="panel-heading">
                        <h4 class="panel-title">Dashboard Details</h4>
                        <div class="panel-control"></div>
                    </div>
                    <div class="panel-body"></div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                        </div>
                    </div>
                </div>
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                        </div>
                    </div>
                </div>
            </div>
			<!-- Dashboard End -->
			<div class="col-md-2"></div>
		</div> <!-- col 12 end -->
    </div><!-- row end -->
</div><!-- main-wrapper end -->

</asp:Content>

