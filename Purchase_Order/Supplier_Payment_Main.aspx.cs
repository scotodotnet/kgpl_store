﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Order_Supplier_Payment_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Supplier Payment Details";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Load_Data_Enquiry_Grid();
    }


    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "2", "Supplier Payments");
        //if (Rights_Check == false)
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Supplier Payments Details..');", true);
        //}
        //else
        //{
            Session.Remove("Supp_payment_Trans_No");
            Response.Redirect("Supplier_Payment.aspx");

        //}
    }


    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        bool ErrFlag = false;

        string Enquiry_No_Str = e.CommandName.ToString();
        Session.Remove("Supp_payment_Trans_No");
        Session["Supp_payment_Trans_No"] = Enquiry_No_Str;
        Response.Redirect("Supplier_Payment.aspx");
    }


    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        //Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "4", "2", "Supplier Payments");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete Supplier Payment Details..');", true);
        //}
        //User Rights Check End

        
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Unplanned_Receipt_Supp_Payment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //Delete Main Table
                query = "Delete from Unplanned_Receipt_Supp_Payment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                //Delete Main Sub Table
                query = "Delete from Unplanned_Receipt_Supp_Payment_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Standard Purchase Order Details Deleted Successfully');", true);
                Load_Data_Enquiry_Grid();
            }
        }
    }


    private void Load_Data_Enquiry_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Trans_No,Trans_Date,Supp_Name,NetAmount from Unplanned_Receipt_Supp_Payment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

}
