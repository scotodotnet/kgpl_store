﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Order_standard_po_amend : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionStdPOOrderAmendNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    static string sum ;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
       
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Standard Purchase Order (Amendment)";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
           
            Load_Data_Empty_StdPurOrd();
            if (Session["Std_PO_Amnt_No"] == null)
            {
                SessionStdPOOrderAmendNo = "";
                Load_Std_PO_Item_Details();
                sum = "0";
            }
            else
            {
                SessionStdPOOrderAmendNo = Session["Std_PO_Amnt_No"].ToString();
                txtAmendmentNo.Text = SessionStdPOOrderAmendNo;
                btnSearch_Click(sender, e);
                sum = "0";
            }
        }
        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check Std Purchase Order No
        query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Std. Purchase Order No Correctly...');", true);
        }

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }
        if (txtTax.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter Tax Percent...');", true);
        }
        if (txtDiscount.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter Discount Amount...');", true);
        }
        if (txtOtherCharge.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter Other Charge Amount...');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Std Purchase Order (A)");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Std Purchase Order Amendment Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Std Purchase Order (A)");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Std Purchase Order Amendment..');", true);
            }
        }
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Standard Purchase Order (Amendment)", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtAmendmentNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            query = "Select * from Std_Purchase_Order_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_Amend_No='" + txtAmendmentNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Std_Purchase_Order_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_Amend_No='" + txtAmendmentNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Std_Purchase_Order_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_Amend_No='" + txtAmendmentNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into Std_Purchase_Order_Amend_Main(Ccode,Lcode,FinYearCode,FinYearVal,Std_PO_Amend_No,Std_PO_Amend_Date,Std_PO_No,Std_PO_Date,Supp_Code,Supp_Name,Supp_Qtn_No,Supp_Qtn_Date,";
            query = query + " DeptCode,DeptName,Note,Others,TotalAmt,Discount,TaxPer,TaxAmt,OtherCharge,NetAmount,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtAmendmentNo.Text + "','" + txtAmendmentDate.Text + "',";
            query = query + " '" + txtStdOrderNo.SelectedItem.Text + "','" + txtStdOrderDate.Text + "','" + txtSuppCodehide.Value + "','" + txtSupplierName.Text + "',";
            query = query + " '" + txtSuppRefDocNo.Text + "','" + txtDocDate.Text + "','" + txtDeptCodeHide.Value + "','" + txtDepartmentName.Text + "',";
            query = query + " '" + txtNote.Text + "','" + txtOthers.Text + "','" + txtTotAmt.Text + "','" + txtDiscount.Text + "','" + txtTax.Text + "','" + txtTaxAmt.Text + "','" + txtOtherCharge.Text + "','" + txtNetAmt.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Std_Purchase_Order_Amend_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Std_PO_Amend_No,Std_PO_Amend_Date,Std_PO_No,Std_PO_Date,Pur_Request_No,Pur_Request_Date,ItemCode,ItemName,UOMCode,ReuiredQty,OrderQty,ReuiredDate,Rate,Remarks,LineTotal,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtAmendmentNo.Text + "','" + txtAmendmentDate.Text + "','" + txtStdOrderNo.SelectedItem.Text + "','" + txtStdOrderDate.Text + "','" + dt.Rows[i]["Pur_Request_No"].ToString() + "','" + dt.Rows[i]["Pur_Request_Date"].ToString() + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["ReuiredQty"].ToString() + "','" + dt.Rows[i]["OrderQty"].ToString() + "','" + dt.Rows[i]["ReuiredDate"].ToString() + "','" + dt.Rows[i]["Rate"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["Remarks"].ToString() + "','" + dt.Rows[i]["LineTotal"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Standard Purchase Order Amendment Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Standard Purchase Order Amendment Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Std_PO_Amnt_No"] = txtAmendmentNo.Text;
            btnSave.Text = "Update";
            Load_Std_PO_Item_Details();
            //Load_Data_Enquiry_Grid();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtAmendmentNo.Text = ""; txtAmendmentDate.Text = ""; txtStdOrderNo.SelectedItem.Text = "-Select-"; txtStdOrderDate.Text = "";
        txtSuppCodehide.Value = ""; txtSupplierName.Text = ""; txtSuppRefDocNo.Text = ""; txtDocDate.Text = "";
        txtDeptCodeHide.Value = ""; txtDepartmentName.Text = ""; txtNote.Text = ""; txtOthers.Text = ""; txtTotAmt.Text = "0.0";
        txtTax.Text = ""; txtTaxAmt.Text = "0.0"; txtTaxAmt.Text = "0"; txtDiscount.Text = "0"; txtOtherCharge.Text = "0"; txtNetAmt.Text = "0";
        Load_Std_PO_Item_Details();

        btnSave.Text = "Save";
        Session.Remove("Std_PO_Amnt_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Std_Purchase_Order_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_Amend_No='" + txtAmendmentNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtAmendmentDate.Text = Main_DT.Rows[0]["Std_PO_Amend_Date"].ToString();
            txtStdOrderNo.Text= Main_DT.Rows[0]["Std_PO_No"].ToString();
            txtStdOrderDate.Text = Main_DT.Rows[0]["Std_PO_Date"].ToString();
            txtSuppCodehide.Value = Main_DT.Rows[0]["Supp_Code"].ToString();
            txtSupplierName.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtSuppRefDocNo.Text = Main_DT.Rows[0]["Supp_Qtn_No"].ToString();
            txtDocDate.Text = Main_DT.Rows[0]["Supp_Qtn_Date"].ToString();
            txtDeptCodeHide.Value = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtNote.Text = Main_DT.Rows[0]["Note"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["TotalAmt"].ToString();
            txtTax.Text = Main_DT.Rows[0]["TaxPer"].ToString();
            txtTaxAmt.Text = Main_DT.Rows[0]["TaxAmt"].ToString();
            txtDiscount.Text = Main_DT.Rows[0]["Discount"].ToString();
            txtOtherCharge.Text = Main_DT.Rows[0]["OtherCharge"].ToString();
            txtNetAmt.Text = Main_DT.Rows[0]["NetAmount"].ToString();


            //Std_Purchase_Order_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select Pur_Request_No,Pur_Request_Date,ItemCode,ItemName,UOMCode,ReuiredQty,OrderQty,ReuiredDate,Rate,Remarks,LineTotal from Std_Purchase_Order_Amend_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_Amend_No='" + txtAmendmentNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("standard_po_amend_main.aspx");
    }
    public void Totalsum()
    {
        sum = "0";
       
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(dt.Rows[i]["LineTotal"])).ToString();

        }

        txtTotAmt.Text = sum;



        txtNetAmt.Text = ((Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
    }

    private void Load_Std_PO_Item_Details()
    {
        string query = "";
        DataTable dt = new DataTable();
        query = "Select Pur_Request_No,Pur_Request_Date,ItemCode,ItemName,UOMCode,ReuiredQty,OrderQty,ReuiredDate,Rate,Remarks,LineTotal from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + txtStdOrderNo.SelectedItem.Text + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        ViewState["ItemTable"] = dt;

        

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        Totalsum();
    }

    protected void txtStdOrderNo_TextChanged(object sender, EventArgs e)
    {
        Load_Std_PO_Item_Details();
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Std Purchase Order (A)");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Std Purchase Order Amendment...');", true);
        //}
        //User Rights Check End

        query = "Select * from Std_Purchase_Order_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_Amend_No='" + txtAmendmentNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Std Purchase Order Amendment Details..');", true);
        }

        query = "Select * from Std_Purchase_Order_Amend_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_Amend_No='" + txtAmendmentNo.Text + "' And Std_PO_Ament_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Std Purchase Order Amendment Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            query = "Update Std_Purchase_Order_Amend_Main set Std_PO_Ament_Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And Std_PO_Amend_No='" + txtAmendmentNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Std Purchase Order Amendment Details Approved Successfully..');", true);
        }
        btnSearch_Click(sender, e);
    }

    protected void txtTax_TextChanged(object sender, EventArgs e)
    {
        txtTaxAmt.Text = (Convert.ToDecimal(txtTotAmt.Text) * (Convert.ToDecimal(txtTax.Text) / 100)).ToString();
        txtNetAmt.Text = ((Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
    }


    protected void txtOtherCharge_TextChanged(object sender, EventArgs e)
    {
        txtNetAmt.Text = ((Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
       
    }

    protected void btnStdPurOrd_Click(object sender, EventArgs e)
    {
        //modalPop_StdPurOrd.Show();
    }

    private void Load_Data_Empty_StdPurOrd()
    {

        string query = "";
        DataTable Main_DT = new DataTable();
    
        query = "Select Std_PO_No,Std_PO_Date,Supp_Name,Supp_Qtn_No,Supp_Qtn_Date,DeptName from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);

        txtStdOrderNo.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["Std_PO_No"] = "-Select-";
        //dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtStdOrderNo.DataTextField = "Std_PO_No";
        //txtDeptCode.DataValueField = "DeptCode";
        txtStdOrderNo.DataBind();

    }

    protected void GridViewClick_StdPurOrd(object sender, CommandEventArgs e)
    {
        txtStdOrderNo.Text = Convert.ToString(e.CommandArgument);
        txtStdOrderDate.Text = Convert.ToString(e.CommandName);

        string query = "";
        DataTable DT = new DataTable();
       
        query = "Select Supp_Code,Supp_Name,Supp_Qtn_No,Supp_Qtn_Date,DeptCode,DeptName from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And Std_PO_No='" + txtStdOrderNo.Text + "'";
        
        DT = objdata.RptEmployeeMultipleDetails(query);

        txtSuppCodehide.Value = DT.Rows[0]["Supp_Code"].ToString();
        txtSupplierName.Text = DT.Rows[0]["Supp_Name"].ToString();
        txtSuppRefDocNo.Text = DT.Rows[0]["Supp_Qtn_No"].ToString();
        txtDocDate.Text = DT.Rows[0]["Supp_Qtn_Date"].ToString();
        txtDeptCodeHide.Value = DT.Rows[0]["DeptCode"].ToString();
        txtDepartmentName.Text = DT.Rows[0]["DeptName"].ToString();

        Load_Std_PO_Item_Details();

    }

    protected void txtStdOrderNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();

        query = "Select Std_PO_Date,Supp_Code,Supp_Name,Supp_Qtn_No,Supp_Qtn_Date,DeptCode,DeptName from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And Std_PO_No='" + txtStdOrderNo.SelectedItem.Text + "'";

        DT = objdata.RptEmployeeMultipleDetails(query);

        txtStdOrderDate.Text = DT.Rows[0]["Std_PO_Date"].ToString();
        txtSuppCodehide.Value = DT.Rows[0]["Supp_Code"].ToString();
        txtSupplierName.Text = DT.Rows[0]["Supp_Name"].ToString();
        txtSuppRefDocNo.Text = DT.Rows[0]["Supp_Qtn_No"].ToString();
        txtDocDate.Text = DT.Rows[0]["Supp_Qtn_Date"].ToString();
        txtDeptCodeHide.Value = DT.Rows[0]["DeptCode"].ToString();
        txtDepartmentName.Text = DT.Rows[0]["DeptName"].ToString();

        Load_Std_PO_Item_Details();

    }


}
