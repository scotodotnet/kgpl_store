﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="standard_po_amend.aspx.cs" Inherits="Purchase_Order_standard_po_amend" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="updatepanel3" runat="server">
				<ContentTemplate>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#StdPurOrd').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#StdPurOrd').dataTable();               
            }
        });
    };
</script>

<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
     $find('StdPurOrd_Close').hide();
     
     }
   } 
 </script>
<!--Ajax popup End-->



<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Standard Purchase Order (A)</li>
                       <h4>
                       </h4>
                        <h4>
                       </h4>
                        <h4>
                       </h4>
                        <h4>
                       </h4>
                        </h4> 
                    </ol>
                </div>
  
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
    <div class="page-inner">
           
            <div class="col-md-12">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Standard Purchase Order (Amendment)</h4>
				</div>
				</div>
				<form class="form-horizontal">
				
				
				<div class="panel-body">
				
				    <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Amendment No<span class="mandatory">*</span></label>
					            <asp:Label ID="txtAmendmentNo" runat="server" class="form-control"></asp:Label>	
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Date</label>
					            <asp:TextBox ID="txtAmendmentDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtAmendmentDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderAmendDate" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtAmendmentDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Std. Order No<span class="mandatory">*</span></label>
					            <asp:DropDownList ID="txtStdOrderNo" runat="server" class="js-states form-control" AutoPostBack="true" 
                                    onselectedindexchanged="txtStdOrderNo_SelectedIndexChanged">
                                </asp:DropDownList>
					            
					            
					           <%-- <asp:TextBox ID="txtStdOrderNo" MaxLength="20" class="form-control" 
                                    runat="server" ontextchanged="txtStdOrderNo_TextChanged" style="float: left;width: 80%;"></asp:TextBox>--%>
                                    
                                     <%--<asp:Button ID="btnStdPurOrd" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnStdPurOrd_Click"/>--%>
                    <%--<cc1:ModalPopupExtender ID="modalPop_StdPurOrd"  runat="server" PopupControlID="Panel2" TargetControlID="btnStdPurOrd"
                       CancelControlID="BtnClear_StdPurOrd" BackgroundCssClass="modalBackground" BehaviorID="StdPurOrd_Close">
                    </cc1:ModalPopupExtender>--%>
                   
                    <%--<asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Standard PO Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_StdPurOrd" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="StdPurOrd" class="display table">
                         <thead >
                         <tr>
                         <th>Std_PO_No</th>
                         <th>Std_PO_Date</th>
                         <th>Supp_Name</th>
                         <th>Supp_Qtn_No</th>
                         <th>Supp_Qtn_Date</th>
                         <th>DeptName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("Std_PO_No")%></td>
                          <td><%# Eval("Std_PO_Date")%></td>
                          <td><%# Eval("Supp_Name")%></td>
                          <td><%# Eval("Supp_Qtn_No")%></td>
                          <td><%# Eval("Supp_Qtn_Date")%></td>
                          <td><%# Eval("DeptName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditStdPurOrd_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_StdPurOrd" CommandArgument='<%# Eval("Std_PO_No")%>' CommandName='<%# Eval("Std_PO_Date")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_StdPurOrd" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
                                    
                                    
                         <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtStdOrderNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>                      
                                    
					            <%--<asp:RequiredFieldValidator ControlToValidate="txtStdOrderNo" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Std. Date</label>
					            <asp:TextBox ID="txtStdOrderDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtStdOrderDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtStdOrderDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Supplier Name</label>
					            <asp:TextBox ID="txtSupplierName" class="form-control" runat="server"></asp:TextBox>
					            <asp:HiddenField ID="txtSuppCodehide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtSupplierName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>					        
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					        
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Supp.Ref.Doc.No</label>
					            <asp:TextBox ID="txtSuppRefDocNo" class="form-control" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtSuppRefDocNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Doc Date</label>
					            <asp:TextBox ID="txtDocDate" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Department Name</label>
					            <asp:TextBox ID="txtDepartmentName" class="form-control" runat="server"></asp:TextBox>
					            <asp:HiddenField ID="txtDeptCodeHide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtDepartmentName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Note</label>
					            <asp:TextBox ID="txtNote" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Others</label>
					            <asp:TextBox ID="txtOthers" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
					        </div>
					    </div>
					</div>
					<div class="clearfix"></div>
					<div class="col-md-12">
					    <div class="row">
					        
					    </div>
					</div>
					
					
				    <div class="clearfix"></div>				    
				    <div class="timeline-options">
				        <h4 class="panel-title">Item Details</h4>
				    </div>
				    
					<!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Req. No</th>
                                                <th>Req. Date</th>
                                                <%--<th>Item Code</th>--%>
                                                <th>Item Name</th>
                                                <%--<th>UOM Code</th>--%>
                                                <th>Requ. Qty</th>
                                                <th>Order Qty</th>
                                                <th>Requ. Date</th>
                                                <th>Rate</th>                                                
                                                <th>Remarks</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("Pur_Request_No")%></td>
                                        <td><%# Eval("Pur_Request_Date")%></td>
                                        <%--<td><%# Eval("ItemCode")%></td>--%>
                                        <td><%# Eval("ItemName")%></td>
                                        <%--<td><%# Eval("UOMCode")%></td>--%>
                                        <td><%# Eval("ReuiredQty")%></td>
                                        <td><%# Eval("OrderQty")%></td>
                                        <td><%# Eval("ReuiredDate")%></td>
                                        <td><%# Eval("Rate")%></td>                                       
                                        <td><%# Eval("Remarks")%></td>
                                        <td><%# Eval("LineTotal")%></td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
					
					
					<div class="col-md-12">
					    <div class="row">
					     <div class="form-group col-md-10"></div>
					      <div class="form-group col-md-2">
					            <label for="exampleInputName">Total Amt</label>
					            <asp:TextBox ID="txtTotAmt" class="form-control" runat="server" Text="0.0"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTotAmt" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					    </div>
					    </div>
					
					
					<div class="col-md-12">
					    <div class="row">
					    <div class="form-group col-md-1">
					            <label for="exampleInputName">Tax Per</label>
					            <asp:TextBox ID="txtTax" class="form-control" runat="server" 
                                     AutoPostBack="true" ontextchanged="txtTax_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTax" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Tax Amount</label>
					            <asp:Label ID="txtTaxAmt" runat="server" class="form-control" Text="0"></asp:Label>
					           
					        </div>
					       <div class="form-group col-md-1">
					            <label for="exampleInputName">Discount</label>
					            <asp:TextBox ID="txtDiscount" class="form-control" runat="server" Text="0"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDiscount" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					       <div class="form-group col-md-2">
					            <label for="exampleInputName">Other Charge</label>
					            <asp:TextBox ID="txtOtherCharge" class="form-control" runat="server" 
                                     AutoPostBack="true" ontextchanged="txtOtherCharge_TextChanged" Text="0"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtOtherCharge" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					       
					        <div class="form-group col-md-2">
					           <label for="exampleInputName">Net Amt</label>
					           <asp:Label ID="txtNetAmt" runat="server" class="form-control"></asp:Label>
					           
					        </div>
					        </div>
					</div>
					   
						
						<div class="form-group row"></div>	
                        <!-- Button start -->
                        <div class="txtcenter">
                            <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                            <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                            <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                            <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" Text="Back" OnClick="btnBackRequest_Click"/>
                            <asp:Button ID="btnApprove" class="btn btn-success" runat="server" Text="Approve" OnClick="btnApprove_Click"/>
                        </div>
                        <!-- Button end -->
                    </div><!-- panel Body-->
                    
				</form>
			
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		   
   
		    <div class="col-md-2"></div>
		    
            
      
 </div><!-- page-inner-end -->	        
 </div><!-- col 12 end -->
  </div><!-- row end -->
 </div>
 </ContentTemplate>
				</asp:UpdatePanel>
</asp:Content>

