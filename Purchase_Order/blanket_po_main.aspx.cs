﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Order_blanket_po_main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Blanket Purchase Order";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_Blanket_No_Dropdown();
        }        
        Load_Data_Enquiry_Grid();
        Load_Blanket_Schedule_Det();
        Load_Data_Empty_ItemCode();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Blanket Purchase Order");
        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Blanket Purchase Order..');", true);
        }
        else
        {
            Session.Remove("Pur_RequestNo_Approval");
            Session.Remove("Blanket_PO_No");
            Response.Redirect("blanket_po.aspx");
        }
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
             string query = "";
             bool ErrFlag = false;

            DataTable dtdpurchase = new DataTable();
            query = "select Blanket_PO_Status from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + e.CommandName.ToString() + "'";
            dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
            string status = dtdpurchase.Rows[0]["Blanket_PO_Status"].ToString();

            if (status == "" || status == "0")
            {
                string Enquiry_No_Str = e.CommandName.ToString();
                Session.Remove("Pur_RequestNo_Approval");
                Session.Remove("Blanket_PO_No");
                Session["Blanket_PO_No"] = Enquiry_No_Str;
                Response.Redirect("blanket_po.aspx");
            }
          
            else if (status == "2")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket Purchase Order Already Rejected..');", true);
            }
            else if (status == "3")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket Purchase Order put in pending..');", true);
            }
            else if (status == "1")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approved Blanket Purchase Order Cant Edit..');", true);
            }
            else if (status == "6")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket Purchase Order Rejected by FM..');", true);
            }
            else if (status == "5")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket Purchase Order Pending by FM..');", true);
            }
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Blanket Purchase Order");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New Blanket Purchase Order..');", true);
        }
        //User Rights Check End

        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        query = "Select * from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + e.CommandName.ToString() + "' And Blanket_PO_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Delete Blanket Purchase Order Details Already Approved..');", true);
        }
        //Check With Already Approved End

        if (!ErrFlag)
        {
            DataTable dtdpurchase = new DataTable();
            query = "select Blanket_PO_Status from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + e.CommandName.ToString() + "'";
            dtdpurchase = objdata.RptEmployeeMultipleDetails(query);
            string status = dtdpurchase.Rows[0]["Blanket_PO_Status"].ToString();

            if (status == "" || status == "0")
            {

                DataTable DT = new DataTable();
                query = "Select * from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + e.CommandName.ToString() + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    //Delete Main Table
                    query = "Delete from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    //Delete Main Sub Table
                    query = "Delete from Blanket_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket Purchase Order Details Deleted Successfully');", true);
                    Load_Data_Enquiry_Grid();
                }
            }
            else if (status == "2")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket Purchase Order Already Rejected..');", true);
            }
            else if (status == "3")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket Purchase Order put in pending..');", true);
            }
            else if (status == "1")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approved Blanket Purchase Order Cant Edit..');", true);
            }
            else if (status == "6")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket Purchase Order Rejected by FM..');", true);
            }
            else if (status == "5")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket Purchase Order Pending by FM..');", true);
            }
        }
    }

    private void Load_Data_Enquiry_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Blanket_PO_No,Blanket_PO_Date,Supp_Name,Approvedby from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("Qty", typeof(string)));
        dt.Columns.Add(new DataColumn("DeliveryDate", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();

        //dt = Repeater1.DataSource;
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string query = "";

        if (txtBlanketNo.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Blanket No...');", true);
        }
        if (txtQty.Text == "0.0" || txtQty.Text == "0" || txtQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Qty...');", true);
        }
        
        //Check Blanket PO No
        query = "Select * from Blanket_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + txtBlanketNo.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Correct Blanket Order No..');", true);
        }

        //check with Item Code And Item Name 
        query = "Select * from Blanket_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + txtItemCodeHide.Value + "' And ItemName='" + txtItemName.Text + "' And Blanket_PO_No='" + txtBlanketNo.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
        }
        string SaveMode = "Insert";
        if (!ErrFlag)
        {
            query = "Select * from Blanket_Purchase_Order_Schedule where Blanket_PO_No='" + txtBlanketNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
            query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
            if (qry_dt.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Blanket_Purchase_Order_Schedule where Blanket_PO_No='" + txtBlanketNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
                query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
                objdata.RptEmployeeMultipleDetails(query);

            }
            else
            {
                SaveMode = "Insert";
            }

            //Insert Compnay Details
            query = "Insert Into Blanket_Purchase_Order_Schedule(Ccode,Lcode,FinYearCode,FinYearVal,Blanket_PO_No,Blanket_PO_Date,ItemCode,ItemName,Qty,DeliveryDate,Remarks,UserID,UserName)";
            query = query + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtBlanketNo.Text + "','" + txtBlanketDate.Text + "','" + txtItemCodeHide.Value + "',";
            query = query + "'" + txtItemName.Text + "','" + txtQty.Text + "','" + txtDeliveryDate.Text + "','" + txtRemarks.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            //update Schedule true in Blanket_Purchase_Order_Main_Sub Table
            query = "Update Blanket_Purchase_Order_Main_Sub set Schedule='True' where Blanket_PO_No='" + txtBlanketNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
            query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            objdata.RptEmployeeMultipleDetails(query);

            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket PO Item Schedule Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket PO Item Schedule Details Updated Successfully');", true);
            }

            txtItemName.Text = ""; txtItemCodeHide.Value = ""; txtQty.Text = ""; txtDeliveryDate.Text = ""; txtRemarks.Text = "";
            Load_Blanket_Schedule_Det();

        }
    }
    protected void txtBlanketNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        //Find Date
        query = "Select * from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + txtBlanketNo.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtBlanketDate.Text = DT.Rows[0]["Blanket_PO_Date"].ToString();
        }
        else
        {
            txtBlanketDate.Text = "";
        }

        Load_Blanket_Schedule_Det();
        Load_Data_Empty_ItemCode();
    }

    protected void GridEditScheduleClick(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = e.CommandName.ToString();
        Load_Item_Det();

    }
    protected void GridDeleteScheduleClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Blanket_Purchase_Order_Schedule where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + txtBlanketNo.Text + "' And ItemCode='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            //Delete Blanket Schedule Table
            query = "Delete from Blanket_Purchase_Order_Schedule where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + txtBlanketNo.Text + "' And ItemCode='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            //update Schedule true in Blanket_Purchase_Order_Main_Sub Table
            query = "Update Blanket_Purchase_Order_Main_Sub set Schedule='False' where Blanket_PO_No='" + txtBlanketNo.Text + "' And ItemCode='" + e.CommandName.ToString() + "'";
            query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Blanket_Purchase_Order_Schedule Details Deleted Successfully');", true);
            Load_Blanket_Schedule_Det();
        }
    }

    private void Load_Blanket_No_Dropdown()
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        txtBlanketNo.Items.Clear();
        query = "Select * from Blanket_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' order by Blanket_PO_No Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtBlanketNo.Items.Add("-Select-");
        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {
            txtBlanketNo.Items.Add(Main_DT.Rows[i]["Blanket_PO_No"].ToString());
        }
    }

    private void Load_Item_Det()
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Blanket_Purchase_Order_Schedule where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And Blanket_PO_No='" + txtBlanketNo.Text + "' And ItemCode='" + txtItemCodeHide.Value + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtItemName.Text = Main_DT.Rows[0]["ItemName"].ToString();
            txtItemCodeHide.Value = Main_DT.Rows[0]["ItemCode"].ToString();
            txtQty.Text = Main_DT.Rows[0]["Qty"].ToString();
            txtDeliveryDate.Text = Main_DT.Rows[0]["DeliveryDate"].ToString();
            txtRemarks.Text = Main_DT.Rows[0]["Remarks"].ToString();
        }
        else
        {
            txtItemName.Text = ""; txtQty.Text = ""; txtDeliveryDate.Text = ""; txtRemarks.Text = "";
            txtItemCodeHide.Value = "";
        }
    }

    private void Load_Blanket_Schedule_Det()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Blanket_PO_No,Blanket_PO_Date,ItemCode,ItemName,Qty,DeliveryDate,Remarks from Blanket_Purchase_Order_Schedule where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Blanket_PO_No='" + txtBlanketNo.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {
        string query = "";
           
        DataTable DT = new DataTable();
        
        query = "Select ItemCode,ItemName,OrderQty from Blanket_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And Blanket_PO_No='" + txtBlanketNo.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode_Normal.DataSource = DT;
        Repeater_ItemCode_Normal.DataBind();
        Repeater_ItemCode_Normal.Visible = true;


    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);

        string query = "";
        DataTable DT = new DataTable();
        query = "Select OrderQty from Blanket_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And ItemCode='" + txtItemCodeHide.Value + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        txtQty.Text = DT.Rows[0]["OrderQty"].ToString();

    }


}
