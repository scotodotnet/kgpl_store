﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Script.Services;
using System.Data;

using Altius.BusinessAccessLayer.BALDataAccess;

/// <summary>
/// Summary description for List_Service
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]


public class List_Service : System.Web.Services.WebService {

    BALDataAccess objdata = new BALDataAccess();
    public List_Service () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
        //if (Session["UserId"] == null)
        //{
        //    HttpContext.Current.Response.Redirect("Default.aspx");
        //    HttpContext.Current.Response.Write("Your session expired");
        //}
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetCompany_Code(string prefix)
    {
        List<string> Company_Code = new List<string>();
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Ccode from MstCompany where Ccode like '%" + prefix + "%'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Company_Code.Add(DT.Rows[i]["Ccode"].ToString());
                //Company_Code.Add(string.Format("{0}-{1}", DT.Rows[i]["Ccode"].ToString(), DT.Rows[i]["Ccode"].ToString()));
            }
        }

        return Company_Code.ToArray();
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetLocation_Code(string prefix)
    {
        List<string> Location_Code = new List<string>();
        string query = "";
        string Comp_Code="";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Lcode from MstLocation where Lcode like '%" + prefix + "%' And Ccode='" + Comp_Code + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Location_Code.Add(DT.Rows[i]["Lcode"].ToString());
                //Company_Code.Add(string.Format("{0}-{1}", DT.Rows[i]["Ccode"].ToString(), DT.Rows[i]["Ccode"].ToString()));
            }
        }

        return Location_Code.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetDept_Det(string prefix)
    {
        List<string> Department_Code = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        DataTable DT = new DataTable();
        query = "Select DeptCode from MstDepartment where DeptCode like '%" + prefix + "%' And Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Department_Code.Add(DT.Rows[i]["DeptCode"].ToString());
                //Company_Code.Add(string.Format("{0}-{1}", DT.Rows[i]["Ccode"].ToString(), DT.Rows[i]["Ccode"].ToString()));
            }
        }

        return Department_Code.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetSupplierCode_Det(string prefix)
    {
        List<string> Supplier_Code = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        DataTable DT = new DataTable();
        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
        query = query + " And (SuppName like '%" + prefix + "%' or SuppCode like '%" + prefix + "%')";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Supplier_Code.Add(DT.Rows[i]["SuppName"].ToString() + "-" + DT.Rows[i]["SuppCode"].ToString());
            }
        }

        return Supplier_Code.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetTransportCode_Det(string prefix)
    {
        List<string> Transport_Code = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        DataTable DT = new DataTable();
        query = "Select TransPortCode from MstTransporter where TransPortCode like '%" + prefix + "%' And Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Transport_Code.Add(DT.Rows[i]["TransPortCode"].ToString());
            }
        }

        return Transport_Code.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetEmployeeCode_Det(string prefix)
    {
        List<string> Employee_Code = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        DataTable DT = new DataTable();
        query = "Select EmpCode from MstEmployee where EmpCode like '%" + prefix + "%' And Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Employee_Code.Add(DT.Rows[i]["EmpCode"].ToString());
            }
        }

        return Employee_Code.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetTCDCode_Det(string prefix)
    {
        List<string> TCD_Code = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        DataTable DT = new DataTable();
        query = "Select TCDCode from MstTaxMaster where TCDCode like '%" + prefix + "%' And Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                TCD_Code.Add(DT.Rows[i]["TCDCode"].ToString());
            }
        }

        return TCD_Code.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetAccountCode_Det(string prefix)
    {
        List<string> Account_Code = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        DataTable DT = new DataTable();
        query = "Select AccCode from MstAccountHead where AccCode like '%" + prefix + "%' And Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Account_Code.Add(DT.Rows[i]["AccCode"].ToString());
            }
        }

        return Account_Code.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetAccountType_Det(string prefix)
    {
        List<string> Account_Type = new List<string>();
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Distinct AccType from MstAccountHead where AccType like '" + prefix + "%'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Account_Type.Add(DT.Rows[i]["AccType"].ToString());
            }
        }
        return Account_Type.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetAccountGroupName_Det(string prefix)
    {
        List<string> Account_GroupName = new List<string>();
        string query = "";
        DataTable DT = new DataTable();
        query = "Select Distinct AccGroupName from MstAccountHead where AccGroupName like '" + prefix + "%'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Account_GroupName.Add(DT.Rows[i]["AccGroupName"].ToString());
            }
        }
        return Account_GroupName.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetItemCodeSelect_Det(string prefix)
    {
        List<string> Item_Code_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        DataTable DT = new DataTable();
        query = "Select ItemCode,ItemShortName from MstItemMaster where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
        query = query + " And (ItemShortName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%')";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Item_Code_Select.Add(DT.Rows[i]["ItemShortName"].ToString() + "-" + DT.Rows[i]["ItemCode"].ToString());
            }
        }

        return Item_Code_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetDeptFull_Det(string prefix)
    {
        List<string> Dept_Full_Code = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        DataTable DT = new DataTable();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
        query = query + " And (DeptName like '%" + prefix + "%' or DeptCode like '%" + prefix + "%')";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Dept_Full_Code.Add(DT.Rows[i]["DeptName"].ToString() + "|" + DT.Rows[i]["DeptCode"].ToString());
            }
        }

        return Dept_Full_Code.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetPurRequestNoSelect_Det(string prefix)
    {
        List<string> PurRequest_Full_Code = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Pur_Request_No,Pur_Request_Date,Others,Approvedby from Pur_Request_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Pur_Request_No like '%" + prefix + "%'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                PurRequest_Full_Code.Add(DT.Rows[i]["Pur_Request_No"].ToString() + "|" + DT.Rows[i]["Pur_Request_Date"].ToString() + "|" + DT.Rows[i]["Others"].ToString() + "|" + DT.Rows[i]["Approvedby"].ToString());
            }
        }

        return PurRequest_Full_Code.ToArray();
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetPurRequestItemCodeSelect_Det(string prefix, string Request_No)
    {
        List<string> Item_Code_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select ItemCode,ItemName from Pur_Request_Main_Sub where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Pur_Request_No='" + Request_No + "' And (ItemName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%')";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Item_Code_Select.Add(DT.Rows[i]["ItemName"].ToString() + "-" + DT.Rows[i]["ItemCode"].ToString());
            }
        }

        return Item_Code_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetSupplierQutNoSelect_Det(string prefix, string Supp_Code_No)
    {
        List<string> Supp_QutNo_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select QuotNo,QuotDate from Supp_Qut_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And QuotNo like '%" + prefix + "%' And SuppCode='" + Supp_Code_No + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Supp_QutNo_Select.Add(DT.Rows[i]["QuotNo"].ToString() + "|" + DT.Rows[i]["QuotDate"].ToString());
            }
        }

        return Supp_QutNo_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetPurRequestSelect_Det_PO(string prefix)
    {
        List<string> Dept_Full_Code = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Pur_Request_No,Pur_Request_Date from Pur_Request_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Pur_Request_No like '%" + prefix + "%'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Dept_Full_Code.Add(DT.Rows[i]["Pur_Request_No"].ToString() + "|" + DT.Rows[i]["Pur_Request_Date"].ToString());
            }
        }

        return Dept_Full_Code.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetPurRequestItemCodeSelect_Det_PO(string prefix, string Request_No, string PO_Type)
    {
        List<string> Item_Code_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        string ss = "";
        if (PO_Type == "2")
        {
             ss = Request_No.Substring(0, 3);
        }
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        if (PO_Type == "2")
        {
            if (ss != "PRA")
            {
                query = "Select ItemCode,ItemName,ReuiredQty,ReuiredDate from Pur_Request_Main_Sub where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
                query = query + " And Pur_Request_No='" + Request_No + "' And (ItemName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%')";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        Item_Code_Select.Add(DT.Rows[i]["ItemName"].ToString() + "-" + DT.Rows[i]["ItemCode"].ToString() + "-" + DT.Rows[i]["ReuiredQty"].ToString() + "-" + DT.Rows[i]["ReuiredDate"].ToString());
                    }
                }
            }
            else
            {
                query = "Select ItemCode,ItemName,ReuiredQty,ReuiredDate from Pur_Request_Amend_Main_Sub where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
                query = query + " And Amend_No='" + Request_No + "' And (ItemName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%')";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        Item_Code_Select.Add(DT.Rows[i]["ItemName"].ToString() + "-" + DT.Rows[i]["ItemCode"].ToString() + "-" + DT.Rows[i]["ReuiredQty"].ToString() + "-" + DT.Rows[i]["ReuiredDate"].ToString());
                    }
                }
            }
        }
        else if (PO_Type == "1")
        {
            query = "Select ItemCode,ItemShortName from MstItemMaster where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
            query = query + " And (ItemShortName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%') And PurchaseType='Normal'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Item_Code_Select.Add(DT.Rows[i]["ItemShortName"].ToString() + "-" + DT.Rows[i]["ItemCode"].ToString());
                }
            }

           
        }
        else if (PO_Type == "3")
        {
            query = "Select ItemCode,ItemShortName from MstItemMaster where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
            query = query + " And (ItemShortName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%') And PurchaseType='Special'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Item_Code_Select.Add(DT.Rows[i]["ItemShortName"].ToString() + "-" + DT.Rows[i]["ItemCode"].ToString());
                }
            }


        }

        return Item_Code_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetPurStdOrderNoSelect_Det(string prefix)
    {
        List<string> Std_PO_No_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Std_PO_No,Std_PO_Date,Supp_Code,Supp_Name,Supp_Qtn_No,Supp_Qtn_Date,DeptCode,DeptName from Std_Purchase_Order_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Std_PO_No like '%" + prefix + "%'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Std_PO_No_Select.Add(DT.Rows[i]["Std_PO_No"].ToString() + "|" + 
                    DT.Rows[i]["Std_PO_Date"].ToString() + "|" + DT.Rows[i]["Supp_Code"].ToString() +
                    "|" + DT.Rows[i]["Supp_Name"].ToString() + "|" + DT.Rows[i]["Supp_Qtn_No"].ToString() +
                    "|" + DT.Rows[i]["Supp_Qtn_Date"].ToString() + "|" + DT.Rows[i]["DeptCode"].ToString() +
                    "|" + DT.Rows[i]["DeptName"].ToString());
            }
        }

        return Std_PO_No_Select.ToArray();
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetBlanketPOItemCodeSelect_Det(string prefix, string Blanket_No)
    {
        List<string> Item_Code_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select ItemCode,ItemName,OrderQty from Blanket_Purchase_Order_Main_Sub where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Blanket_PO_No='" + Blanket_No + "' And (ItemName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%')";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Item_Code_Select.Add(DT.Rows[i]["ItemName"].ToString() + "-" + DT.Rows[i]["ItemCode"].ToString() + "-" + DT.Rows[i]["OrderQty"].ToString());
            }
        }

        return Item_Code_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetWarehouse_Det(string prefix)
    {
        List<string> Warehouse_Code = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        DataTable DT = new DataTable();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
        query = query + " And (WarehouseName like '%" + prefix + "%')";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Warehouse_Code.Add(DT.Rows[i]["WarehouseName"].ToString() + "-" + DT.Rows[i]["WarehouseCode"].ToString());
            }
        }

        return Warehouse_Code.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetZoneListDet(string prefix, string WarehouseName)
    {
        List<string> ZoneName_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        DataTable DT = new DataTable();
        query = "Select ZoneName from MstWarehouse where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
        query = query + " And WarehouseCode='" + WarehouseName + "' And ZoneName like '%" + prefix + "%'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                ZoneName_Select.Add(DT.Rows[i]["ZoneName"].ToString());
            }
        }

        return ZoneName_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetBinListDet(string prefix, string WarehouseName, string ZoneName)
    {
        List<string> BinName_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        DataTable DT = new DataTable();
        query = "Select BinName from MstWarehouse where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
        query = query + " And WarehouseCode='" + WarehouseName + "' And ZoneName='" + ZoneName + "' And BinName like '%" + prefix + "%'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                BinName_Select.Add(DT.Rows[i]["BinName"].ToString());
            }
        }

        return BinName_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetPOOrderNoSupplierWise(string prefix, string Supp_No, string OrderType)
    {
        List<string> PO_OrderNo_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        if (OrderType == "1")
        {
            query = "Select Std_PO_No as PO_Order_No,Std_PO_Date as PO_Order_Date from Std_Purchase_Order_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
            query = query + " And Supp_Code='" + Supp_No + "' And Std_PO_No like '%" + prefix + "%' And PO_Status='1'";
        }
        else
        {
            query = "Select Blanket_PO_No as PO_Order_No,Blanket_PO_Date as PO_Order_Date from Blanket_Purchase_Order_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
            query = query + " And Supp_Code='" + Supp_No + "' And Blanket_PO_No like '%" + prefix + "%' And Blanket_PO_Status='1'";
        }
        
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                PO_OrderNo_Select.Add(DT.Rows[i]["PO_Order_No"].ToString() + "|" + DT.Rows[i]["PO_Order_Date"].ToString());
            }
        }

        return PO_OrderNo_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetItemCodeSearchPurchaseOrderWise(string prefix, string PO_Order_No, string OrderType)
    {
        List<string> Item_Code_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        if (OrderType == "1")
        {
            query = "Select ItemCode,ItemName,OrderQty,Rate from Std_Purchase_Order_Main_Sub where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
            query = query + " And Std_PO_No='" + PO_Order_No + "' And (ItemName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%')";

        }
        else
        {
            query = "Select ItemCode,ItemName,OrderQty,Rate from Blanket_Purchase_Order_Main_Sub where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
            query = query + " And Blanket_PO_No='" + PO_Order_No + "' And (ItemName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%')";
        }
        
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Item_Code_Select.Add(DT.Rows[i]["ItemName"].ToString() + "|" + DT.Rows[i]["ItemCode"].ToString() + "|" + DT.Rows[i]["OrderQty"].ToString() + "|" + DT.Rows[i]["Rate"].ToString());
            }
        }

        return Item_Code_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetCostCenter_Det(string prefix)
    {
        List<string> CostCenter_Code = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        DataTable DT = new DataTable();
        query = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
        query = query + " And (CostcenterName like '%" + prefix + "%')";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                CostCenter_Code.Add(DT.Rows[i]["CostcenterName"].ToString() + "-" + DT.Rows[i]["CostcenterCode"].ToString());
            }
        }

        return CostCenter_Code.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetMaterial_RequestNo_Det(string prefix)
    {
        List<string> Material_RequestNo_Code = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Mat_Req_No,Mat_Req_Date from Meterial_Request_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And (Mat_Req_No like '%" + prefix + "%')";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Material_RequestNo_Code.Add(DT.Rows[i]["Mat_Req_No"].ToString() + "|" + DT.Rows[i]["Mat_Req_Date"].ToString());
            }
        }

        return Material_RequestNo_Code.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetItemCodeSearchMaterialRequestNoWise(string prefix, string Mat_Req_No, string IssueType)
    {
        List<string> Item_Code_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        if (IssueType == "1")
        {
            query = "Select ItemCode,ItemName,UOMCode,ReuiredQty,ReuiredDate from Meterial_Request_Main_Sub where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
            query = query + " And Mat_Req_No='" + Mat_Req_No + "' And (ItemName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%')";
        }
        else
        {
            query = "Select ItemCode,ItemShortName as ItemName,PurchaseUOM as UOMCode,'' as ReuiredQty,'' as ReuiredDate from MstItemMaster where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
            query = query + " And (ItemShortName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%')";
        }

        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Item_Code_Select.Add(DT.Rows[i]["ItemName"].ToString() + "|" + DT.Rows[i]["ItemCode"].ToString() + "|" + DT.Rows[i]["UOMCode"].ToString() + "|" + DT.Rows[i]["ReuiredQty"].ToString() + "|" + DT.Rows[i]["ReuiredDate"].ToString());
            }
        }

        return Item_Code_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetMaterial_IssueNo_Det(string prefix)
    {
        List<string> Material_IssueNo_Code = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Mat_Issue_No,Mat_Issue_Date from Meterial_Issue_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And (Mat_Issue_No like '%" + prefix + "%')";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Material_IssueNo_Code.Add(DT.Rows[i]["Mat_Issue_No"].ToString() + "|" + DT.Rows[i]["Mat_Issue_Date"].ToString());
            }
        }

        return Material_IssueNo_Code.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetItemCodeSearchMaterialIssueNoWise(string prefix, string Mat_Issue_No)
    {
        List<string> Item_Code_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select ItemCode,ItemName,UOMCode from Meterial_Issue_Main_Sub where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Mat_Issue_No='" + Mat_Issue_No + "' And (ItemName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%')";
        
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Item_Code_Select.Add(DT.Rows[i]["ItemName"].ToString() + "|" + DT.Rows[i]["ItemCode"].ToString() + "|" + DT.Rows[i]["UOMCode"].ToString());
            }
        }

        return Item_Code_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetItemCodeSearchMaterialStockTransfer(string prefix)
    {
        List<string> Item_Code_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        DataTable DT = new DataTable();

        query = "Select ItemCode,ItemShortName as ItemName,PurchaseUOM as UOMCode,ValuationType from MstItemMaster where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
        query = query + " And (ItemShortName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%')";
        
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Item_Code_Select.Add(DT.Rows[i]["ItemName"].ToString() + "-" + DT.Rows[i]["ItemCode"].ToString() + "-" + DT.Rows[i]["UOMCode"].ToString() + "-" + DT.Rows[i]["ValuationType"].ToString());
            }
        }

        return Item_Code_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetGatePassOutNo_Det(string prefix)
    {
        List<string> GatePass_OutNo_Code = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select GP_Out_No,GP_Out_Date,Supp_Code,Supp_Name,Supp_Det from GatePass_Out_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And (GP_Out_No like '%" + prefix + "%') And GP_Type='1' And GP_Out_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                GatePass_OutNo_Code.Add(DT.Rows[i]["GP_Out_No"].ToString() + "|" + DT.Rows[i]["GP_Out_Date"].ToString()
                 + "|" + DT.Rows[i]["Supp_Code"].ToString() + "|" + DT.Rows[i]["Supp_Name"].ToString()
                  + "|" + DT.Rows[i]["Supp_Det"].ToString()  
                    );
            }
        }

        return GatePass_OutNo_Code.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetItemCodeSearchGPOutNoWise(string prefix, string GP_Out_No)
    {
        List<string> Item_Code_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select ItemCode,ItemName,UOMCode,OutQty from GatePass_Out_Main_Sub where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And GP_Out_No='" + GP_Out_No + "' And (ItemName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%')";
        

        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Item_Code_Select.Add(DT.Rows[i]["ItemName"].ToString() + "-" + DT.Rows[i]["ItemCode"].ToString() + "-" + DT.Rows[i]["UOMCode"].ToString() + "-" + DT.Rows[i]["OutQty"].ToString());
            }
        }

        return Item_Code_Select.ToArray();
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetPurEnqryNoSelect_Det(string prefix)
    {
        List<string> PE_No_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Pur_EnquiryNo,Enquiry_Date from Pur_Enq_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Pur_EnquiryNo like '%" + prefix + "%' ";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                PE_No_Select.Add(DT.Rows[i]["Pur_EnquiryNo"].ToString() + "|" +
                    DT.Rows[i]["Enquiry_Date"].ToString());
            }
        }

        return PE_No_Select.ToArray();
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetSupplierQuotationNumber_Det(string prefix)
    {
        List<string> Item_Code_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select QuotNo,QuotDate from Supp_Qut_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And QuotNo like '%" + prefix + "%'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Item_Code_Select.Add(DT.Rows[i]["QuotNo"].ToString() + "|" + DT.Rows[i]["QuotDate"].ToString());
            }
        }

        return Item_Code_Select.ToArray();
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetAmendNoSelect_Det(string prefix)
    {
        List<string> AM_No_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Amend_No,Amend_Date from Pur_Request_Amend_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Amend_No like '%" + prefix + "%'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                AM_No_Select.Add(DT.Rows[i]["Amend_No"].ToString() + "|" +
                    DT.Rows[i]["Amend_Date"].ToString());
            }
        }

        return AM_No_Select.ToArray();
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetPurchaseRequestNoAmend_Det(string prefix)
    {
        List<string> PurRq_No_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Pur_Request_No,Pur_Request_Date from Pur_Request_Amend_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Pur_Request_No like '%" + prefix + "%'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                PurRq_No_Select.Add(DT.Rows[i]["Pur_Request_No"].ToString() + "|" +
                    DT.Rows[i]["Pur_Request_Date"].ToString());
            }
        }

        return PurRq_No_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetAmendBy_Det(string prefix)
    {
        List<string> Amendby_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Amendby from Pur_Request_Amend_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Amendby like '%" + prefix + "%'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Amendby_Select.Add(DT.Rows[i]["Amendby"].ToString());
            }
        }

        return Amendby_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetPurReqNoSelect_Det(string prefix)
    {
        List<string> PurRq_No_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Pur_Request_No,Pur_Request_Date from Pur_Request_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Pur_Request_No like '%" + prefix + "%'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                PurRq_No_Select.Add(DT.Rows[i]["Pur_Request_No"].ToString() + "|" +
                    DT.Rows[i]["Pur_Request_Date"].ToString());
            }
        }

        return PurRq_No_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetReqBy_Det(string prefix)
    {
        List<string> Requestby_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Requestby from Pur_Request_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Requestby like '%" + prefix + "%'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Requestby_Select.Add(DT.Rows[i]["Requestby"].ToString());
            }
        }

        return Requestby_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetMatIssueNoSelect_Det(string prefix)
    {
        List<string> MatIss_No_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Mat_Issue_No,Mat_Issue_Date from Meterial_Issue_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Mat_Issue_No like '%" + prefix + "%' And Mat_Issue_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                MatIss_No_Select.Add(DT.Rows[i]["Mat_Issue_No"].ToString() + "|" +
                    DT.Rows[i]["Mat_Issue_Date"].ToString());
            }
        }

        return MatIss_No_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetMatIssueBy_Det(string prefix)
    {
        List<string> Issuedby_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Issuedby from Meterial_Issue_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Issuedby like '%" + prefix + "%' And Mat_Issue_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Issuedby_Select.Add(DT.Rows[i]["Issuedby"].ToString());
            }
        }

        return Issuedby_Select.ToArray();
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetMatIssueReturnNoSelect_Det(string prefix)
    {
        List<string> MatIssRet_No_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Mat_Issue_Return_No,Mat_Issue_Return_Date from Meterial_Issue_Return_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Mat_Issue_Return_No like '%" + prefix + "%' And Mat_Issue_Return_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                MatIssRet_No_Select.Add(DT.Rows[i]["Mat_Issue_Return_No"].ToString() + "|" +
                    DT.Rows[i]["Mat_Issue_Return_Date"].ToString());
            }
        }

        return MatIssRet_No_Select.ToArray();
    }

    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetMatReturnBy_Det(string prefix)
    {
        List<string> Returnby_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Returnby from Meterial_Issue_Return_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Returnby like '%" + prefix + "%' And Mat_Issue_Return_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Returnby_Select.Add(DT.Rows[i]["Returnby"].ToString());
            }
        }

        return Returnby_Select.ToArray();
    }
   
 
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetMatRequestNoSelect_Det(string prefix)
    {
        List<string> MatReq_No_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Mat_Req_No,Mat_Req_Date from Meterial_Request_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Mat_Req_No like '%" + prefix + "%' And Mat_Req_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                MatReq_No_Select.Add(DT.Rows[i]["Mat_Req_No"].ToString() + "|" +
                    DT.Rows[i]["Mat_Req_Date"].ToString());
            }
        }

        return MatReq_No_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetMatReqPreparedBy_Det(string prefix)
    {
        List<string> Preparedby_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Preparedby from Meterial_Request_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Preparedby like '%" + prefix + "%' And Mat_Req_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Preparedby_Select.Add(DT.Rows[i]["Preparedby"].ToString());
            }
        }

        return Preparedby_Select.ToArray();
    }
   

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetGPOutNoSelect_Det(string prefix)
    {
        List<string> GPOut_No_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select GP_Out_No,GP_Out_Date from GatePass_Out_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And GP_Out_No like '%" + prefix + "%' And GP_Out_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                GPOut_No_Select.Add(DT.Rows[i]["GP_Out_No"].ToString() + "|" +
                    DT.Rows[i]["GP_Out_Date"].ToString());
            }
        }

        return GPOut_No_Select.ToArray();
    }

    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetGPOutIssuedBy_Det(string prefix)
    {
        List<string> GPIssued_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Issuedby from GatePass_Out_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Issuedby like '%" + prefix + "%' And GP_Out_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                GPIssued_Select.Add(DT.Rows[i]["Issuedby"].ToString());
            }
        }

        return GPIssued_Select.ToArray();
    }
   

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetGatePassINNo_Det(string prefix)
    {
        List<string> GPIN_No_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select GP_IN_No,GP_IN_Date from GatePass_IN_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And GP_IN_No like '%" + prefix + "%' And GP_IN_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                GPIN_No_Select.Add(DT.Rows[i]["GP_IN_No"].ToString() + "|" +
                    DT.Rows[i]["GP_IN_Date"].ToString());
            }
        }

        return GPIN_No_Select.ToArray();
    }

    

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetGPOutNo_NONReturn_Det(string prefix)
    {
        List<string> GPOutNo_NonRet_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select GP_Out_No,GP_Out_Date from GatePass_Out_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And GP_Out_No like '%" + prefix + "%' And GP_Type='2' And GP_Out_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                GPOutNo_NonRet_Select.Add(DT.Rows[i]["GP_Out_No"].ToString() + "|" +
                    DT.Rows[i]["GP_Out_Date"].ToString());
            }
        }

        return GPOutNo_NonRet_Select.ToArray();
    }

    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetGPOutNonRetIssuedBy_Det(string prefix)
    {
        List<string> GPIssued_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Issuedby from GatePass_Out_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Issuedby like '%" + prefix + "%' And GP_Type='2' And GP_Out_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                GPIssued_Select.Add(DT.Rows[i]["Issuedby"].ToString());
            }
        }

        return GPIssued_Select.ToArray();
    }
 
  
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetStdPur_OrdNo_Det(string prefix)
    {
        List<string> StdPur_OrdNo_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Std_PO_No,Std_PO_Date from Std_Purchase_Order_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Std_PO_No like '%" + prefix + "%' And PO_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                StdPur_OrdNo_Select.Add(DT.Rows[i]["Std_PO_No"].ToString() + "|" +
                    DT.Rows[i]["Std_PO_Date"].ToString());
            }
        }

        return StdPur_OrdNo_Select.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetGenPur_OrdNo_Det(string prefix)
     {
        List<string> GenPur_OrdNo_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Gen_PO_No,Gen_PO_Date from General_Purchase_Order_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Gen_PO_No like '%" + prefix + "%' And PO_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                GenPur_OrdNo_Select.Add(DT.Rows[i]["Gen_PO_No"].ToString() + "|" +
                    DT.Rows[i]["Gen_PO_Date"].ToString());
            }
        }

        return GenPur_OrdNo_Select.ToArray();
     }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetOpenRecptNo_Det(string prefix)
     {
        List<string> OpnRecpt_No_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Open_Recp_No,Open_Recp_Date from Opening_Receipt_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Open_Recp_No like '%" + prefix + "%' And OP_Receipt_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                OpnRecpt_No_Select.Add(DT.Rows[i]["Open_Recp_No"].ToString() + "|" +
                    DT.Rows[i]["Open_Recp_Date"].ToString());
            }
        }

        return OpnRecpt_No_Select.ToArray();
     }

    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetBlanketPurOrdNo_Det(string prefix)
     {
        List<string> BlanketPO_No_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Blanket_PO_No,Blanket_PO_Date from Blanket_Purchase_Order_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Blanket_PO_No like '%" + prefix + "%' And Blanket_PO_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                BlanketPO_No_Select.Add(DT.Rows[i]["Blanket_PO_No"].ToString() + "|" +
                    DT.Rows[i]["Blanket_PO_Date"].ToString());
            }
        }

        return BlanketPO_No_Select.ToArray();
     }

    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetPurchaseOrderReceipt_Det(string prefix)
     {
        List<string> POReceipt_No_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select PO_Receipt_No,PO_Receipt_Date from Pur_Order_Receipt_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And PO_Receipt_No like '%" + prefix + "%' And PO_Receipt_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                POReceipt_No_Select.Add(DT.Rows[i]["PO_Receipt_No"].ToString() + "|" +
                    DT.Rows[i]["PO_Receipt_Date"].ToString());
            }
        }

        return POReceipt_No_Select.ToArray();
     }


    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetUnplannedReceipt_Det(string prefix)
     {
        List<string> UnPReceipt_No_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select UnPlan_Recp_No,UnPlan_Recp_Date from Unplanned_Receipt_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And UnPlan_Recp_No like '%" + prefix + "%' And UnPlan_PO_Receipt_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                UnPReceipt_No_Select.Add(DT.Rows[i]["UnPlan_Recp_No"].ToString() + "|" +
                    DT.Rows[i]["UnPlan_Recp_Date"].ToString());
            }
        }

        return UnPReceipt_No_Select.ToArray();
     }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetStockTranNo_Det(string prefix)
     {
        List<string> StockTran_No_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Mat_Stock_Transfer_No,Mat_Stock_Transfer_Date from Meterial_Stock_Transfer_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Mat_Stock_Transfer_No like '%" + prefix + "%' And Mat_Stock_Transfer_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                StockTran_No_Select.Add(DT.Rows[i]["Mat_Stock_Transfer_No"].ToString() + "|" +
                    DT.Rows[i]["Mat_Stock_Transfer_Date"].ToString());
            }
        }

        return StockTran_No_Select.ToArray();
     }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetTransTypeSelect_Det(string prefix)
     {
        List<string> GenPur_OrdNo_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Trans_Type,Trans_Date_Str from Stock_Transaction_Ledger where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Trans_Type like '%" + prefix + "%'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                GenPur_OrdNo_Select.Add(DT.Rows[i]["Trans_Type"].ToString() + "|" +
                    DT.Rows[i]["Trans_Date_Str"].ToString());
            }
        }

        return GenPur_OrdNo_Select.ToArray();
     }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetPurchaseReturnNumber_Det(string prefix)
    {
        List<string> Item_Code_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Pur_Return_No,Pur_Return_Date from Purc_Return_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And Pur_Return_No like '%" + prefix + "%' And PO_Status='1'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Item_Code_Select.Add(DT.Rows[i]["Pur_Return_No"].ToString() + "|" + DT.Rows[i]["Pur_Return_Date"].ToString());
            }
        }

        return Item_Code_Select.ToArray();
    }

   


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetBOPurRequestItemCodeSelect_Det_PO(string prefix, string PO_Type)
    {
        List<string> Item_Code_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        if (PO_Type == "1")
        {
            query = "Select ItemCode,ItemShortName from MstItemMaster where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
            query = query + " And (ItemShortName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%') And PurchaseType='Normal'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Item_Code_Select.Add(DT.Rows[i]["ItemShortName"].ToString() + "-" + DT.Rows[i]["ItemCode"].ToString());
                }
            }


        }
        else if (PO_Type == "2")
        {
            query = "Select ItemCode,ItemShortName from MstItemMaster where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
            query = query + " And (ItemShortName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%') And PurchaseType='Special'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Item_Code_Select.Add(DT.Rows[i]["ItemShortName"].ToString() + "-" + DT.Rows[i]["ItemCode"].ToString());
                }
            }


        }

        return Item_Code_Select.ToArray();
    }




    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetPOOrderNoSupplierWise_New(string prefix, string Supp_No, string OrderType)
    {
        List<string> PO_OrderNo_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        bool error=false;
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();
        DataTable DT3 = new DataTable();

        DataTable DT = new DataTable();
        DT.Columns.Add("PO_Order_No");
        DT.Columns.Add("PO_Order_Date");

        if (OrderType == "1")
        {
            query = "Select Std_PO_No,Std_PO_Date from Std_Purchase_Order_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
            query = query + " And Supp_Code='" + Supp_No + "' And Std_PO_No like '%" + prefix + "%' And PO_Status='1'";
            DT1 = objdata.RptEmployeeMultipleDetails(query);

            if(DT1.Rows.Count!=0)
            {
                int k = 0;
                for (int i = 0; i < DT1.Rows.Count; i++)
                {
                    error = false;
                    query = "select ItemCode,OrderQty from Std_Purchase_Order_Main_Sub where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "' And Std_PO_No='" + DT1.Rows[i]["Std_PO_No"].ToString() + "'";
                    DT2 = objdata.RptEmployeeMultipleDetails(query);

                    for (int j = 0; j < DT2.Rows.Count; j++)
                    {
                      
                       query = "select Sum(ReceivedQty) as ReceivedQty from Pur_Order_Receipt_Main_Sub where ItemCode='" + DT2.Rows[j]["ItemCode"].ToString() + "' And Pur_Order_No='" + DT1.Rows[i]["Std_PO_No"].ToString() + "'";
                        DT3 = objdata.RptEmployeeMultipleDetails(query);
                        if (DT3.Rows[0]["ReceivedQty"].ToString() != "")
                        {
                            if (Convert.ToDecimal(DT3.Rows[0]["ReceivedQty"]) < Convert.ToDecimal(DT2.Rows[j]["OrderQty"]))
                            {
                                error = true;
                            }
                        }

                    }
                    if (error)
                    {
                        DT.Rows.Add();
                        DT.Rows[k]["PO_Order_No"] = DT1.Rows[i]["Std_PO_No"].ToString();
                        DT.Rows[k]["PO_Order_Date"] = DT1.Rows[i]["Std_PO_Date"].ToString();
                        k = k + 1;

                    }


                }
            }




        }
        else
        {
            query = "Select Blanket_PO_No,Blanket_PO_Date from Blanket_Purchase_Order_Main where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
            query = query + " And Supp_Code='" + Supp_No + "' And Blanket_PO_No like '%" + prefix + "%' And Blanket_PO_Status='1'";
            DT1 = objdata.RptEmployeeMultipleDetails(query);
            if (DT1.Rows.Count != 0)
            {
                int k = 0;
                for (int i = 0; i < DT1.Rows.Count; i++)
                {
                    error = false;
                    query = "select ItemCode,OrderQty from Blanket_Purchase_Order_Main_Sub where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "' And Blanket_PO_No='" + DT1.Rows[i]["Blanket_PO_No"].ToString() + "'";
                    DT2 = objdata.RptEmployeeMultipleDetails(query);

                    for (int j = 0; j < DT2.Rows.Count; j++)
                    {
                        
                        query = "select Sum(ReceivedQty) as ReceivedQty from Pur_Order_Receipt_Main_Sub where ItemCode='" + DT2.Rows[j]["ItemCode"].ToString() + "' And Pur_Order_No='" + DT1.Rows[i]["Blanket_PO_No"].ToString() + "'";
                        DT3 = objdata.RptEmployeeMultipleDetails(query);
                        if (DT3.Rows[0]["ReceivedQty"].ToString() != "")
                        {
                            if (Convert.ToDecimal(DT3.Rows[0]["ReceivedQty"]) < Convert.ToDecimal(DT2.Rows[j]["OrderQty"]))
                            {
                                error = true;
                            }
                        }

                    }
                    if (error)
                    {
                        DT.Rows.Add();
                        DT.Rows[k]["PO_Order_No"] = DT1.Rows[i]["Blanket_PO_No"].ToString();
                        DT.Rows[k]["PO_Order_Date"] = DT1.Rows[i]["Blanket_PO_Date"].ToString();
                        k = k + 1;

                    }


                }
            }

        }

        
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                PO_OrderNo_Select.Add(DT.Rows[i]["PO_Order_No"].ToString() + "|" + DT.Rows[i]["PO_Order_Date"].ToString());
            }
        }

        return PO_OrderNo_Select.ToArray();
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetItemCodeSearchPurchaseOrderWise_New(string prefix, string PO_Order_No, string OrderType)
    {
        List<string> Item_Code_Select = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        decimal bal = 0;
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");
        DT.Columns.Add("OrderQty");
        DT.Columns.Add("Rate");
        DT.Columns.Add("Balance");

        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();

        if (OrderType == "1")
        {
            query = "Select ItemCode,ItemName,OrderQty,Rate from Std_Purchase_Order_Main_Sub where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
            query = query + " And Std_PO_No='" + PO_Order_No + "' And (ItemName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%')";
            DT1 = objdata.RptEmployeeMultipleDetails(query);

            if (DT1.Rows.Count != 0)
            {
                int k = 0;
                for (int i = 0; i < DT1.Rows.Count; i++)
                {
                    query = "select Sum(ReceivedQty) as ReceivedQty from Pur_Order_Receipt_Main_Sub where ItemCode='" + DT1.Rows[i]["ItemCode"].ToString() + "' And Pur_Order_No='" + PO_Order_No + "'";
                    DT2 = objdata.RptEmployeeMultipleDetails(query);
                    if (DT2.Rows[0]["ReceivedQty"].ToString() != "")
                    {
                        if (Convert.ToDecimal(DT2.Rows[0]["ReceivedQty"]) < Convert.ToDecimal(DT1.Rows[i]["OrderQty"]))
                        {
                            bal = Convert.ToDecimal(DT1.Rows[i]["OrderQty"]) - Convert.ToDecimal(DT2.Rows[0]["ReceivedQty"]);
                        }
                        else if (Convert.ToDecimal(DT2.Rows[0]["ReceivedQty"]) == Convert.ToDecimal(DT1.Rows[i]["OrderQty"]))
                        {
                            bal = 0;
                        }

                    }
                    else
                    {
                        bal = Convert.ToDecimal(DT1.Rows[i]["OrderQty"]);
                    }

                    DT.Rows.Add();
                    DT.Rows[i]["ItemCode"] = DT1.Rows[i]["ItemCode"].ToString();
                    DT.Rows[i]["ItemName"] = DT1.Rows[i]["ItemName"].ToString();
                    DT.Rows[i]["OrderQty"] = DT1.Rows[i]["OrderQty"].ToString();
                    DT.Rows[i]["Rate"] = DT1.Rows[i]["Rate"].ToString();
                    DT.Rows[i]["Balance"] = bal.ToString();
                }
            }
        }
        else
        {
            query = "Select ItemCode,ItemName,OrderQty,Rate from Blanket_Purchase_Order_Main_Sub where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
            query = query + " And Blanket_PO_No='" + PO_Order_No + "' And (ItemName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%')";
            DT1 = objdata.RptEmployeeMultipleDetails(query);

            if (DT1.Rows.Count != 0)
            {
                int k = 0;
                for (int i = 0; i < DT1.Rows.Count; i++)
                {
                    query = "select Sum(ReceivedQty) as ReceivedQty from Pur_Order_Receipt_Main_Sub where ItemCode='" + DT1.Rows[i]["ItemCode"].ToString() + "' And Pur_Order_No='" + PO_Order_No + "'";
                    DT2 = objdata.RptEmployeeMultipleDetails(query);
                    if (DT2.Rows[0]["ReceivedQty"].ToString() != "")
                    {
                        if (Convert.ToDecimal(DT2.Rows[0]["ReceivedQty"]) < Convert.ToDecimal(DT1.Rows[i]["OrderQty"]))
                        {
                            bal = Convert.ToDecimal(DT1.Rows[i]["OrderQty"]) - Convert.ToDecimal(DT2.Rows[0]["ReceivedQty"]);
                        }
                        else if (Convert.ToDecimal(DT2.Rows[0]["ReceivedQty"]) == Convert.ToDecimal(DT1.Rows[i]["OrderQty"]))
                        {
                            bal = 0;
                        }

                    }
                    else
                    {
                        bal = Convert.ToDecimal(DT1.Rows[i]["OrderQty"]);
                    }

                    DT.Rows.Add();
                    DT.Rows[i]["ItemCode"] = DT1.Rows[i]["ItemCode"].ToString();
                    DT.Rows[i]["ItemName"] = DT1.Rows[i]["ItemName"].ToString();
                    DT.Rows[i]["OrderQty"] = DT1.Rows[i]["OrderQty"].ToString();
                    DT.Rows[i]["Rate"] = DT1.Rows[i]["Rate"].ToString();
                    DT.Rows[i]["Balance"] = bal.ToString();
                }
            }
        }

        
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Item_Code_Select.Add(DT.Rows[i]["ItemName"].ToString() + "|" + DT.Rows[i]["ItemCode"].ToString() + "|" + DT.Rows[i]["OrderQty"].ToString() + "|" + DT.Rows[i]["Rate"].ToString() + "|" + DT.Rows[i]["Balance"].ToString());
            }
        }

        return Item_Code_Select.ToArray();
    }



    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetItemCodeSearchGPOutNoWise_New(string prefix, string GP_Out_No)
    {
        List<string> Item_Code_Select = new List<string>();
        decimal bal = 0;
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        string Fin_Year_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        Fin_Year_Code = HttpContext.Current.Session["FinYearCode"].ToString();
        DataTable DT = new DataTable();
        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");
        DT.Columns.Add("UOMCode");
        DT.Columns.Add("OutQty");
        DT.Columns.Add("Balance");

        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();

        query = "Select ItemCode,ItemName,UOMCode,OutQty from GatePass_Out_Main_Sub where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "' And FinYearCode='" + Fin_Year_Code + "'";
        query = query + " And GP_Out_No='" + GP_Out_No + "' And (ItemName like '%" + prefix + "%' or ItemCode like '%" + prefix + "%')";
        DT1 = objdata.RptEmployeeMultipleDetails(query);


        if (DT1.Rows.Count != 0)
        {
            int k = 0;
            for (int i = 0; i < DT1.Rows.Count; i++)
            {
                query = "select Sum(INQty) as INQty from GatePass_IN_Main_Sub where ItemCode='" + DT1.Rows[i]["ItemCode"].ToString() + "' And GP_Out_No='" + GP_Out_No + "'";
                DT2 = objdata.RptEmployeeMultipleDetails(query);
                if (DT2.Rows[0]["INQty"].ToString() != "")
                {
                    if (Convert.ToDecimal(DT2.Rows[0]["INQty"]) < Convert.ToDecimal(DT1.Rows[i]["OutQty"]))
                    {
                        bal = Convert.ToDecimal(DT1.Rows[i]["OutQty"]) - Convert.ToDecimal(DT2.Rows[0]["INQty"]);
                    }
                    else if (Convert.ToDecimal(DT2.Rows[0]["INQty"]) == Convert.ToDecimal(DT1.Rows[i]["OutQty"]))
                    {
                        bal = 0;
                    }

                }
                else
                {
                    bal = Convert.ToDecimal(DT1.Rows[i]["OutQty"]);
                }

                DT.Rows.Add();
                DT.Rows[i]["ItemCode"] = DT1.Rows[i]["ItemCode"].ToString();
                DT.Rows[i]["ItemName"] = DT1.Rows[i]["ItemName"].ToString();
                DT.Rows[i]["UOMCode"] = DT1.Rows[i]["UOMCode"].ToString();
                DT.Rows[i]["OutQty"] = DT1.Rows[i]["OutQty"].ToString();
                DT.Rows[i]["Balance"] = bal.ToString();
            }
        }



        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Item_Code_Select.Add(DT.Rows[i]["ItemName"].ToString() + "-" + DT.Rows[i]["ItemCode"].ToString() + "-" + DT.Rows[i]["UOMCode"].ToString() + "-" + DT.Rows[i]["OutQty"].ToString() + "-" + DT.Rows[i]["Balance"].ToString());
            }
        }

        return Item_Code_Select.ToArray();
    }
    






}