﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for BaseBusiness
/// </summary>
namespace Altius.BusinessAccessLayer
{
    public class BaseBusiness : BaseClass
    {
        #region  Private Variables

        private DateTime _CreatedDate;
        private DateTime _UpdatedDate;
        private Int32 _CreatedBy;
        private Int32 _UpdatedBy;
        private bool _IsDeleted;

        #endregion

        #region  Constructor
        /// <summary>
        /// private Constructor, for making the class as singleton
        /// </summary>
        public BaseBusiness()
        {
        }
        #endregion

        #region  Properties


        public DateTime CreatedDate
        {
            get
            {
                return _CreatedDate;
            }
            set
            {
                _CreatedDate = value;
            }
        }

        public DateTime UpdatedDate
        {
            get
            {
                return _UpdatedDate;
            }
            set
            {
                _UpdatedDate = value;
            }
        }

        public Int32 CreatedBy
        {
            get
            {
                return _CreatedBy;
            }
            set
            {
                _CreatedBy = value;
            }
        }

        public Int32 UpdatedBy
        {
            get
            {
                return _UpdatedBy;
            }
            set
            {
                _UpdatedBy = value;
            }
        }

        public bool IsDeleted
        {
            get
            {
                return _IsDeleted;
            }
            set
            {
                _IsDeleted = value;
            }
        }

        # endregion

    }
}
