﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for DALDataAccess
/// </summary>
namespace Altius.DataAccessLayer.DALDataAccess
{
    public class DALDataAccess : BaseDataAccess
    {
        //
        // TODO: Add constructor logic here
        //

        public void MstCompany(MastersClass objdis)
        {
            System.Data.SqlClient.SqlParameter[] MstDistrict = 
            {
                new System.Data.SqlClient.SqlParameter("@CompanyNm",objdis.CompanyNm),
              //  new System.Data.SqlClient.SqlParameter("@CompanyNmTN",objdis.CompanyNm)
                
               
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "CompanyInserMaster_SP", MstDistrict);

        }

        public string CompanyMaxValue()
        {
            System.Data.SqlClient.SqlParameter[] CompanyMax = 
            {
                
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "MaxCompanyCd_SP", CompanyMax);
            return str;
        }
        public void MstDistrict(MastersClass objdis)
        {
            System.Data.SqlClient.SqlParameter[] MstDistrict = 
            {
                new System.Data.SqlClient.SqlParameter("@StateCd",objdis.StateCd),
                new System.Data.SqlClient.SqlParameter("@DistrictCd",objdis.DistrictCd),
                new System.Data.SqlClient.SqlParameter("@DistrictNm",objdis.DistrictNm),
                new System.Data.SqlClient.SqlParameter("@DistrictNmTn",objdis.DistrictNmTN)
               
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "MstDistrict_SP", MstDistrict);

        }
        public void allocation(MastersClass objleave)
        {
            System.Data.SqlClient.SqlParameter[] allocation =
            {
                new System.Data.SqlClient.SqlParameter("@LeaveDays",objleave.allocatedleave)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Leaveallocation_Insert", allocation);
        }
        public void MstInsurance(MastersClass objIns)
        {
            System.Data.SqlClient.SqlParameter[] MstInsur = 
            {
                new System.Data.SqlClient.SqlParameter("@Insurancecmpcd",objIns.InsuranceCmpCd),
                new System.Data.SqlClient.SqlParameter("@InsuranceCmpNm",objIns.InsuranceCmpNm)
      
               
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "MstInsurance_SP", MstInsur);

        }
        public void MstProbation(MastersClass objPro)
        {
            System.Data.SqlClient.SqlParameter[] MstProbation = 
            {
                new System.Data.SqlClient.SqlParameter("@ProbationCd",objPro.ProbationCd),
                new System.Data.SqlClient.SqlParameter("@ProbationMonth",objPro.ProbationMonth)
      
               
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "MstProbation_SP", MstProbation);

        }


        public void MstTaluk(MastersClass objTaluk)
        {
            System.Data.SqlClient.SqlParameter[] MstTaluk =
            {
                new System.Data.SqlClient.SqlParameter("@StateCd",objTaluk.StateCd),
                new System.Data.SqlClient.SqlParameter("@DistrictCd",objTaluk.DistrictCd),
                new System.Data.SqlClient.SqlParameter("@TalukCd",objTaluk.TalukCd),
                new System.Data.SqlClient.SqlParameter("@TalukNm",objTaluk.TalukNm),
                new System.Data.SqlClient.SqlParameter("@TalukNmTn",objTaluk.TalukNmTN)

            };

            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "MstTaluk_SP", MstTaluk);
        }

        public void MstDepartment(MastersClass objDip)
        {
            System.Data.SqlClient.SqlParameter[] MstDepartment = 
            {
                new System.Data.SqlClient.SqlParameter("@DepartmentCd",objDip.DepartmentCd),
                new System.Data.SqlClient.SqlParameter("@DepartmentNm",objDip.DepartmentNm)

               
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "MstDepartmentInserMaster_SP", MstDepartment);

        }
        public DataTable MstDistrictDisplay()
        {
            System.Data.SqlClient.SqlParameter[] MstDistrictDisplay = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("MstDistrictDisplay_SP", MstDistrictDisplay);
            return dt;
        }
        public DataTable MstCompanyDisplay()
        {
            System.Data.SqlClient.SqlParameter[] MstCompanyDisplay = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("MstCompanyDisplay_SP", MstCompanyDisplay);
            return dt;
        }
        public DataTable MstDropDownDepartmentDisplay()
        {
            System.Data.SqlClient.SqlParameter[] MstDropDipDisplay = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("MstDropDownDepartmentDisplay_SP", MstDropDipDisplay);
            return dt;
        }
        public DataTable DropDownDepartment()
        {
            System.Data.SqlClient.SqlParameter[] DropDepartment = 
            {
               
            };
            DataTable dt = SQL.ExecuteDatatable("DropDownDepartment_SP", DropDepartment);
            return dt;
        }
        public DataTable DropDownDepartment_category(string category, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] DropDepartment_category = 
            {
               new System.Data.SqlClient.SqlParameter("@Category", category),
               new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
               new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("DropDowndept_category_SP", DropDepartment_category);
            return dt;
        }
        public DataTable salaryhistory_DropDowndept_category(string category, string Ccode, string Lcode, string Activemode)
        {
            System.Data.SqlClient.SqlParameter[] DropDepartment_category = 
            {
               new System.Data.SqlClient.SqlParameter("@Category", category),
               new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
               new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
               new System.Data.SqlClient.SqlParameter("@Activemode", Activemode)
            };
            DataTable dt = SQL.ExecuteDatatable("salaryhistory_DropDowndept_category_SP", DropDepartment_category);
            return dt;
        }
        public DataTable DropDownFood()
        {
            System.Data.SqlClient.SqlParameter[] DropFood = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("Fooddetails", DropFood);
            return dt;
        }
        public DataTable MstDepartmentGridDisplay()
        {
            System.Data.SqlClient.SqlParameter[] MstDepartmentGridDisplay = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("MstDepartmentDisplay_SP", MstDepartmentGridDisplay);
            return dt;
        }
        public DataTable DDdEPARTMENT_LOAD()
        {
            System.Data.SqlClient.SqlParameter[] dddepartment = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DDdEPARTMENT_LOAD", dddepartment);
            return dt;
        }
        public DataTable MstInsuranceDisplay()
        {
            System.Data.SqlClient.SqlParameter[] MstInsuranceDispalay = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("MstInuranceDisplay_SP", MstInsuranceDispalay);
            return dt;
        }
        public DataTable EmployeeNoandName(string Staff, string Dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] EmployeeNoandName = 
            {
                new System.Data.SqlClient.SqlParameter("@Staff",Staff),
                new  System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)

            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeNameandNumber_SP", EmployeeNoandName);
            return dt;
        }
        public DataTable EmployeeNoandName_user(string Staff, string Dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] EmployeeNoandName = 
            {
                new System.Data.SqlClient.SqlParameter("@Staff",Staff),
                new  System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)

            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeNameandNumber_user_SP", EmployeeNoandName);
            return dt;
        }
        public DataTable DropDownEmployeeName(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] EmployeeNoandName = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                

            };
            DataTable dt = SQL.ExecuteDatatable("DropDownEmployeeName_SP", EmployeeNoandName);
            return dt;
        }
        public DataTable DropDownEmployeeNumber(string EmpName, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] EmployeeNoandName = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpName",EmpName),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                

            };
            DataTable dt = SQL.ExecuteDatatable("DropDownEmployeeNumber_SP", EmployeeNoandName);
            return dt;
        }
        public DataTable MstProbationDisplay()
        {
            System.Data.SqlClient.SqlParameter[] MstProbation = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("MstProbationDisplay_SP", MstProbation);
            return dt;
        }
        public DataTable MstCompanyGridDisplay()
        {
            System.Data.SqlClient.SqlParameter[] MstCompanyGridDisplay = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("MstCompanyDisplay_SP", MstCompanyGridDisplay);
            return dt;
        }

        public DataTable MstTalukDisplay()
        {
            System.Data.SqlClient.SqlParameter[] MstTalukDisplay =
               {

               };
            DataTable dt = SQL.ExecuteDatatable("MstTalukDisplay_SP", MstTalukDisplay);
            return dt;
        }

        public DataTable EmployeeVerifyforLeaveAllocation(string EmpNo, string dept, string stafforlabour, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] LeaveAllocationStaff =
            {
                new System.Data.SqlClient.SqlParameter("@EmpCode",EmpNo),
                new System.Data.SqlClient.SqlParameter("@dept", dept),
                new System.Data.SqlClient.SqlParameter("@staff", stafforlabour),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("LeaveAllocationEmployeeVerification", LeaveAllocationStaff);
            return dt;

        }
        public DataTable EmployeeVerifyforLeaveAllocationForExistingNo(string ExisitingNo, string dept, string stafforlabour, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] LeaveAllocationStaff =
            {
                new System.Data.SqlClient.SqlParameter("@ExistingNO",ExisitingNo),
                new System.Data.SqlClient.SqlParameter("@dept", dept),
                new System.Data.SqlClient.SqlParameter("@staff", stafforlabour),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("LeaveAllocationEmployeeSearchingForExistingNo_SP", LeaveAllocationStaff);
            return dt;

        }
        public DataTable EmployeeVerifyforLeaveAllocExistingNoandEmpNo(string ExisitingNo, string EmpNo, string dept, string stafforlabour, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] LeaveAllocationStaff =
            {
                new System.Data.SqlClient.SqlParameter("@ExistingNO",ExisitingNo),
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@dept", dept),
                new System.Data.SqlClient.SqlParameter("@staff", stafforlabour),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("LeaveAllocationEmployeeSearchingExistingNoandEmpNo_SP", LeaveAllocationStaff);
            return dt;

        }
        public DataTable EmployeeLeaveAllocForReterivew(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] LeaveAllocationStaff =
            {
               
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                
            };
            DataTable dt = SQL.ExecuteDatatable("LeaveAllocationReterview_SP", LeaveAllocationStaff);
            return dt;

        }
        public void UpdateTaluk(MastersClass objTaluk)
        {
            System.Data.SqlClient.SqlParameter[] UpdateTaluk =
                {
                new System.Data.SqlClient.SqlParameter("@DistrictCd",objTaluk.DistrictCd),
                new System.Data.SqlClient.SqlParameter("@TalukCd",objTaluk.TalukCd),
                new System.Data.SqlClient.SqlParameter("@TalukNm",objTaluk.TalukNm)
                };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UpdateTaluk_sp", UpdateTaluk);
        }

        public void UpdateDistrict(MastersClass objDis)
        {
            System.Data.SqlClient.SqlParameter[] UpdateDistrict = 
            {
                new System.Data.SqlClient.SqlParameter("@Districtcd",objDis.DistrictCd ),
                new System.Data.SqlClient.SqlParameter("@DistrictNm",objDis.DistrictNm)

            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UpdateDsitrct_Sp", UpdateDistrict);
        }

        public void UpdateMstCompany(MastersClass objCom)
        {
            System.Data.SqlClient.SqlParameter[] UpdateCompany = 
            {
                new System.Data.SqlClient.SqlParameter("@CompanyCd",objCom.CompanyCd ),
                new System.Data.SqlClient.SqlParameter("@CompanyNm",objCom.CompanyNm)

            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UpdateCompany_Sp", UpdateCompany);
        }
        public void UpdateMstInsurance(MastersClass objIns)
        {
            System.Data.SqlClient.SqlParameter[] UpdateInsurance = 
            {
                new System.Data.SqlClient.SqlParameter("@Insurancecmpcd",objIns.InsuranceCmpCd),
                new System.Data.SqlClient.SqlParameter("@InsuranceCmpNm",objIns.InsuranceCmpNm)

            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UpdateInsurance_Sp", UpdateInsurance);
        }
        public void UpdateMstProbation(MastersClass objProb)
        {
            System.Data.SqlClient.SqlParameter[] UpdateInsurance = 
            {
                new System.Data.SqlClient.SqlParameter("@ProbationCd",objProb.ProbationCd),
                new System.Data.SqlClient.SqlParameter("@ProbationMonth",objProb.ProbationMonth)

            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UpdateProbation_Sp", UpdateInsurance);
        }

        public void UpdateMstDepartment(MastersClass objDip)
        {
            System.Data.SqlClient.SqlParameter[] UpdateDepartment = 
            {
                new System.Data.SqlClient.SqlParameter("@DepartmentCd",objDip.DepartmentCd ),
                new System.Data.SqlClient.SqlParameter("@DepartmentNm",objDip.DepartmentNm)

            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UpdateDepartment_Sp", UpdateDepartment);
        }

        public void MstQualification(MastersClass objMas)
        {
            System.Data.SqlClient.SqlParameter[] MstQualification = 
            {
                new System.Data.SqlClient.SqlParameter("@QualificationCd",objMas.Qualificationcd),
                new System.Data.SqlClient.SqlParameter("@QualificationNm",objMas.QualificationNm)

            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "MstQualification_SP", MstQualification);
        }
        public DataTable MstQualificationDisplay()
        {
            System.Data.SqlClient.SqlParameter[] MstQualificatioDisplay = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("MstQulificationDisplay_SP", MstQualificatioDisplay);
            return dt;
        }
        public void UpdateQualification(MastersClass objMas)
        {
            System.Data.SqlClient.SqlParameter[] UpdateQualification = 
            {
                new System.Data.SqlClient.SqlParameter("@Qualificationcd",objMas.Qualificationcd),
                new System.Data.SqlClient.SqlParameter("@QualificationNm",objMas.QualificationNm)


            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UpdateQualification_SP", UpdateQualification);
        }
        public void MstLeave(MastersClass objMas)
        {
            System.Data.SqlClient.SqlParameter[] MstLeave = 
            {
                new System.Data.SqlClient.SqlParameter("@LeaveCd",objMas.LeaveCd),
                new System.Data.SqlClient.SqlParameter("@LeaveType",objMas.LeaveType)

            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "MstLeave_SP", MstLeave);
        }

        public DataTable MstLeaveDisplay()
        {
            System.Data.SqlClient.SqlParameter[] MstLeaveDisplay =
            {

            };
            DataTable dt = SQL.ExecuteDatatable("MstleaveDisplay_SP", MstLeaveDisplay);
            return dt;
        }

        public DataTable EmployeeDetailsDisplay()
        {
            System.Data.SqlClient.SqlParameter[] EmployeeDetailsDisplay =
            {

            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeDetailsDisplay_SP", EmployeeDetailsDisplay);
            return dt;
        }
        public DataTable MstLeavePayMasterDisplay()
        {
            System.Data.SqlClient.SqlParameter[] MstLeavPayster =
            {

            };
            DataTable dt = SQL.ExecuteDatatable("MstLeavePayMasterDisplay_SP", MstLeavPayster);
            return dt;
        }
        public void UpdateLeave(MastersClass objMas)
        {
            System.Data.SqlClient.SqlParameter[] UpdateLeave = 
            {
                new System.Data.SqlClient.SqlParameter("@LeaveCd",objMas.LeaveCd),
                new System.Data.SqlClient.SqlParameter("@LeaveType",objMas.LeaveType)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UpdateLeave_SP", UpdateLeave);
        }
        public void UpdateLeaveDaysMaster(string Leaveid, string LeaveDasy)
        {
            System.Data.SqlClient.SqlParameter[] UpdateLeaveDays = 
            {
                new System.Data.SqlClient.SqlParameter("@LeaveiD",Leaveid),
                new System.Data.SqlClient.SqlParameter("@LeaveDays",LeaveDasy)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UpdateLeavePayMaster_SP", UpdateLeaveDays);
        }
        public void MstEmployeeType(MastersClass objMas)
        {
            System.Data.SqlClient.SqlParameter[] MstEmployeeType = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpTypeCd",objMas.EmpTypeCd),
                new System.Data.SqlClient.SqlParameter("@EmpType",objMas.EmpType),
                new System.Data.SqlClient.SqlParameter("@EmpCategory",objMas.EmpCategory)
            };

            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "MstEmployeeType_SP", MstEmployeeType);
        }

        public DataTable MstEmployeeTypeDisplay()
        {
            System.Data.SqlClient.SqlParameter[] MstEmployeeDisplay = 
            {
                
            };
            DataTable dt = SQL.ExecuteDatatable("MstEmployeeDisplay_SP", MstEmployeeDisplay);
            return dt;

        }
        public void UpdateEmployeeType(MastersClass objMas)
        {
            System.Data.SqlClient.SqlParameter[] UpdateEmployee = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpTypeCd",objMas.EmpTypeCd),
                new System.Data.SqlClient.SqlParameter("@EmpType",objMas.EmpType)

            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UpdateEmployeeType_SP", UpdateEmployee);

        }

        public DataTable DropDownDistrict()
        {
            System.Data.SqlClient.SqlParameter[] DropDownDistrict = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("DropDownDistrict_SP", DropDownDistrict);
            return dt;

        }
        public DataTable DropDownCategory()
        {
            System.Data.SqlClient.SqlParameter[] DropDownCategory = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("DropDownCategory_SP", DropDownCategory);
            return dt;

        }
        public DataTable DropDownPSTaluk(string Districtcd)
        {
            System.Data.SqlClient.SqlParameter[] DropDownPSTaluk = 
            {
                new System.Data.SqlClient.SqlParameter("@Districtcd",Districtcd)
            };
            DataTable dt = SQL.ExecuteDatatable("DropDownPSTaluk", DropDownPSTaluk);
            return dt;
        }

        public DataTable DropDownDistrictCheckBox(string Districtcd)
        {
            System.Data.SqlClient.SqlParameter[] DropDownDistrictCheckbox = 
            {
                new System.Data.SqlClient.SqlParameter("@Districtcd",Districtcd)
            };
            DataTable dt = SQL.ExecuteDatatable("DropdownDistrictCheckbox_SP", DropDownDistrictCheckbox);
            return dt;
        }
        public DataTable DropDownTalukCheckBox(string Districtcd, string TalukCd)
        {
            System.Data.SqlClient.SqlParameter[] DropDownTalukCheckbox = 
            {
                new System.Data.SqlClient.SqlParameter("@Districtcd",Districtcd),
                new System.Data.SqlClient.SqlParameter("@TalukCd",TalukCd)
            };
            DataTable dt = SQL.ExecuteDatatable("DropdownTalukCheckbox_SP", DropDownTalukCheckbox);
            return dt;
        }
        public DataTable DropdownPMTaluk(string DistrictCd)
        {
            System.Data.SqlClient.SqlParameter[] DropDownPMTaluk = 
            {
                new System.Data.SqlClient.SqlParameter("@DistrictCd",DistrictCd)
            };
            DataTable dt = SQL.ExecuteDatatable("DropDownPMTaluk_SP", DropDownPMTaluk);
            return dt;
        }
        public DataTable EmployeeDropDown(string category)
        {
            System.Data.SqlClient.SqlParameter[] EmployeeDropdown = 
            {
                 new System.Data.SqlClient.SqlParameter("@Category",category)
            };
            DataTable dt = SQL.ExecuteDatatable("DropDownEmployeeType_SP", EmployeeDropdown);
            return dt;
        }
        public DataTable EmployeeDropDown_no(string type)
        {
            System.Data.SqlClient.SqlParameter[] EmployeeDropdown = 
            {
                new System.Data.SqlClient.SqlParameter("@Type", type)
            };
            DataTable dt = SQL.ExecuteDatatable("DropDownEmployeeType_SP_no", EmployeeDropdown);
            return dt;
        }
        public DataTable LabourDropDown()
        {
            System.Data.SqlClient.SqlParameter[] EmployeeDropdown = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("DropDownLabourType_SP", EmployeeDropdown);
            return dt;
        }
        public DataTable Qualification()
        {
            System.Data.SqlClient.SqlParameter[] Qualification = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("DropDownQualification_SP", Qualification);
            return dt;
        }
        public string MaxEmpNo()
        {
            System.Data.SqlClient.SqlParameter[] MaxEmpNo = 
            {

            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "MaxNo_SP", MaxEmpNo);
            return str;
        }
        public string IsAdmin(string UserName)
        {
            System.Data.SqlClient.SqlParameter[] IsAdmin = 
            {
                new System.Data.SqlClient.SqlParameter("@UserName",UserName)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "IsAdmin_SP", IsAdmin);
            return str;
        }

        public string username(UserRegistrationClass objuser)
        {
            System.Data.SqlClient.SqlParameter[] UserName = 
            {
                new System.Data.SqlClient.SqlParameter("@UserName",objuser.UserCode),
                new System.Data.SqlClient.SqlParameter("@Password",objuser.Password)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UserName_SP", UserName);
            return str;
        }

        public string usernameDisplay(UserRegistrationClass objuser)
        {
            System.Data.SqlClient.SqlParameter[] UserNameDisplay = 
            {
                new System.Data.SqlClient.SqlParameter("@UserName",objuser.UserCode),
                new System.Data.SqlClient.SqlParameter("@Password",objuser.Password)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UserNameDisplay_SP", UserNameDisplay);
            return str;
        }

        public string UserPassword(UserRegistrationClass objuser)
        {
            System.Data.SqlClient.SqlParameter[] UserNamePassword = 
            {
                new System.Data.SqlClient.SqlParameter("@UserName",objuser.UserCode),
                new System.Data.SqlClient.SqlParameter("@Password",objuser.Password)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UserNamePassword_SP", UserNamePassword);
            return str;
        }
        public DataTable FillEmployeeRegistration(string EmpNo, string stafflabour, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] FillEmployeeRegistration =
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@staffLabour", stafflabour),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("FillEmployeeRegistration_SP", FillEmployeeRegistration);

            return dt;
        }
        public DataTable FillEmployeeRegistration_Existno(string exist, string stafflabour, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] FillEmployee_Existno =
            {
                new System.Data.SqlClient.SqlParameter("@exist",exist),
                new System.Data.SqlClient.SqlParameter("@staffLabour", stafflabour),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("FillEmployee_Exist_SP", FillEmployee_Existno);

            return dt;
        }
        public void InsertEmployeeDetails(EmployeeClass objemp, int Regno, string dob, string stafflabour, string NonPFGrade, string RoleCode)
        {
            System.Data.SqlClient.SqlParameter[] EmployeeDetails = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",objemp.EmpCode),
                new System.Data.SqlClient.SqlParameter("@MachineNo",objemp.MachineCode),
                new System.Data.SqlClient.SqlParameter("@ExisistingCode",objemp.ExisitingCode),
                new System.Data.SqlClient.SqlParameter("@EmpName",objemp.EmpName),
                new System.Data.SqlClient.SqlParameter("@FatherName",objemp.EmpFname),
                new System.Data.SqlClient.SqlParameter("@Gender",objemp.Gender),
                new System.Data.SqlClient.SqlParameter("@Dob",dob),
                new System.Data.SqlClient.SqlParameter("@PSAdd1",objemp.PSAdd1),
                new System.Data.SqlClient.SqlParameter("@PSAdd2",objemp.PSAdd2),
                new System.Data.SqlClient.SqlParameter("@PSAdd3",objemp.PSAdd3),
                new System.Data.SqlClient.SqlParameter("@PSTaluk",objemp.PSTaluk),
                new System.Data.SqlClient.SqlParameter("@PSDistrict",objemp.PSDistrict),
                new System.Data.SqlClient.SqlParameter("@PSState",objemp.PSState),
                new System.Data.SqlClient.SqlParameter("@UnEducated",objemp.UnEducated),
                new System.Data.SqlClient.SqlParameter("@UnEduDept",objemp.UnEduDept),
                new System.Data.SqlClient.SqlParameter("@UnEduDesig",objemp.UnEduDesig),
                new System.Data.SqlClient.SqlParameter("@SchoolCollege",objemp.SchoolCollege),
                new System.Data.SqlClient.SqlParameter("@Phone",objemp.Phone),
                new System.Data.SqlClient.SqlParameter("@Mobile",objemp.Mobile),
                new System.Data.SqlClient.SqlParameter("@Passportno",objemp.PassportNo),
                new System.Data.SqlClient.SqlParameter("@DrivinglicenceNo",objemp.DrivingLicNo),
                new System.Data.SqlClient.SqlParameter("@Qualification",objemp.Qualification),
                new System.Data.SqlClient.SqlParameter("@University",objemp.University),
                new System.Data.SqlClient.SqlParameter("@CompanyName",objemp.CompanyName),
                new System.Data.SqlClient.SqlParameter("@Department",objemp.Department),
                new System.Data.SqlClient.SqlParameter("@Designation",objemp.Desingation),
                new System.Data.SqlClient.SqlParameter("@Shift",objemp.Shift),
                new System.Data.SqlClient.SqlParameter("@EmployeeType",objemp.EmployeeType),
                new System.Data.SqlClient.SqlParameter("@CreatedBy",objemp.CreatedBy),
                //new System.Data.SqlClient.SqlParameter("@CreatedDate",dd),
                new System.Data.SqlClient.SqlParameter("@ModifiedBy",objemp.ModifiedBy),
               // new System.Data.SqlClient.SqlParameter("@ExisitingCode",objemp.ExisitingCode),
                new System.Data.SqlClient.SqlParameter("@MaxNo",Regno),
                new System.Data.SqlClient.SqlParameter("@StaffLabour",stafflabour),
                 new System.Data.SqlClient.SqlParameter("@NonPFGrade",NonPFGrade),
                   new System.Data.SqlClient.SqlParameter("@RoleCode",RoleCode),
                   new System.Data.SqlClient.SqlParameter("@Ccode", objemp.Ccode),
                   new System.Data.SqlClient.SqlParameter("@Lcode", objemp.Lcode),
                   new System.Data.SqlClient.SqlParameter("@Martial", objemp.Martial),
                new System.Data.SqlClient.SqlParameter("@Initial", objemp.Initial),
                new System.Data.SqlClient.SqlParameter("@Contract", objemp.ContractType),
                new System.Data.SqlClient.SqlParameter("@OldID", objemp.OldID),
                new System.Data.SqlClient.SqlParameter("@hostel", objemp.Hostel)

  
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "EmployeeDetails_SP", EmployeeDetails);
        }

        public void UpdateEmployeeRegistration(EmployeeClass objemp, string EmpNo, string stafflabor, string DOB, string NonPFGrade)
        {
            System.Data.SqlClient.SqlParameter[] UpdateEmployeeRegistration =
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@MachineNo",objemp.MachineCode),
                
                new System.Data.SqlClient.SqlParameter("@EmpName",objemp.EmpName),
                new System.Data.SqlClient.SqlParameter("@FatherName",objemp.EmpFname),
                new System.Data.SqlClient.SqlParameter("@Gender",objemp.Gender),
                new System.Data.SqlClient.SqlParameter("@DOB",DOB),
                new System.Data.SqlClient.SqlParameter("@PSAdd1",objemp.PSAdd1),
                new System.Data.SqlClient.SqlParameter("@PSAdd2",objemp.PSAdd2),
                new System.Data.SqlClient.SqlParameter("@PSAdd3",objemp.PSAdd3),
                new System.Data.SqlClient.SqlParameter("@PSDistrict",objemp.PSDistrict),
                new System.Data.SqlClient.SqlParameter("@PSTaluk",objemp.PSTaluk),
                new System.Data.SqlClient.SqlParameter("@PSState",objemp.PSState),
                new System.Data.SqlClient.SqlParameter("@Phone",objemp.Phone),
                new System.Data.SqlClient.SqlParameter("@Mobile",objemp.Mobile),
                new System.Data.SqlClient.SqlParameter("@PassportNo",objemp.PassportNo),
                new System.Data.SqlClient.SqlParameter("@DrivingLicenceNo",objemp.DrivingLicNo),
                new System.Data.SqlClient.SqlParameter("@Qualification",objemp.Qualification),
                new System.Data.SqlClient.SqlParameter("@SchoolCollege",objemp.SchoolCollege),
                new System.Data.SqlClient.SqlParameter("@Uneducated",objemp.UnEducated),
                new System.Data.SqlClient.SqlParameter("@University",objemp.University),
                new System.Data.SqlClient.SqlParameter("@Department",objemp.Department),
                new System.Data.SqlClient.SqlParameter("@Designation",objemp.Desingation),
                new System.Data.SqlClient.SqlParameter("@UnEduDept",objemp.UnEduDept),
                new System.Data.SqlClient.SqlParameter("@UnEduDesig",objemp.UnEduDesig),
                new System.Data.SqlClient.SqlParameter("@Shift",objemp.Shift),
                new System.Data.SqlClient.SqlParameter("@EmployeeType",objemp.EmployeeType),
                new System.Data.SqlClient.SqlParameter("@StafforLabor",stafflabor),
                new System.Data.SqlClient.SqlParameter("@NonPFGrade", NonPFGrade),
                new System.Data.SqlClient.SqlParameter("@Ccode", objemp.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", objemp.Lcode),
                new System.Data.SqlClient.SqlParameter("@Martial", objemp.MaritalStatus),
                new System.Data.SqlClient.SqlParameter("@Initial", objemp.Initial),
                new System.Data.SqlClient.SqlParameter("@OldID", objemp.OldID),
                new System.Data.SqlClient.SqlParameter("@hostel", objemp.Hostel)
           
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "updateEmployeeRegistration_sp", UpdateEmployeeRegistration);
        }

        public DataTable DropDownPSTaluk_ForEdit(int Districtcd, int TalukCd)
        {
            System.Data.SqlClient.SqlParameter[] DropDownPSTaluk = 
            {
                new System.Data.SqlClient.SqlParameter("@Districtcd",Districtcd),
                 new System.Data.SqlClient.SqlParameter("@TalukCd",TalukCd),
            };
            DataTable dt = SQL.ExecuteDatatable("DropDownTaluk_ForEdit_SP", DropDownPSTaluk);
            return dt;
        }
        public string EmployeeVerify(string EmpNo)
        {
            System.Data.SqlClient.SqlParameter[] EmployeeVerify =
            {
                new System.Data.SqlClient.SqlParameter("@EmpCode",EmpNo)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "employeeVerification", EmployeeVerify);
            return str;
        }


        public string EmployeeName(string Empcode)
        {
            System.Data.SqlClient.SqlParameter[] EmployeeName = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpCode",Empcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "EmployeeName_SP", EmployeeName);
            return str;
        }
        public string SalaryMonthFromLeave(string Empcode, string FinaYear, string Ccode, string Lcode, string Mont, DateTime dfrom, DateTime dto)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMonth = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",Empcode),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Mont", Mont),
                new System.Data.SqlClient.SqlParameter("@from", dfrom),
                new System.Data.SqlClient.SqlParameter("@to", dto)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalaryMonth_SP", SalaryMonth);
            return str;
        }
        public string SalaryPayDay()
        {
            System.Data.SqlClient.SqlParameter[] SalaryMonth = 
            {
               
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalaryPayDayForStaff_SP", SalaryMonth);
            return str;
        }
        public string SalaryType_ForLabour(string EmpNo, string FinaYear, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMonth = 
            {
               new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
               new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
               new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalaryType_ForLabour_SP", SalaryMonth);
            return str;
        }
        public string EligibleForESI(string EmpNo, string FinaYear, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMonth = 
            {
               new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
               new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
               new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "EligibleForESI_SP", SalaryMonth);
            return str;
        }
        public string ExisistingNo(string EmpNo)
        {
            System.Data.SqlClient.SqlParameter[] ExisistingNo = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpCode",EmpNo)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "ExisistingNo_SP", ExisistingNo);
            return str;
        }

        public DataTable EmployeeDeactivate_loadgrid(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] employeeDeactivate =
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("deactivate_verify", employeeDeactivate);
            return dt;

        }
        public DataTable EmployeeDeactivate_loadgrid_exist(string exist, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] employeeDeactivate =
            {
                new System.Data.SqlClient.SqlParameter("@Exist",exist),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Deactivate_verify_exist", employeeDeactivate);
            return dt;

        }
        public void UpdateEmployeeActivateDeactivate(string EmpNo, string Isactive, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] UpdateEmployeeActivateDeactivate =
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@active",Isactive),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "EmployeeDeactivate_Update", UpdateEmployeeActivateDeactivate);
        }

        public void UpdateEmployeeDeactivate(string EmpNo, DateTime deactivedate, string Isactive, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] UpdateEmployeeDeactivate =
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@deactivedate",deactivedate),
                new System.Data.SqlClient.SqlParameter("@active",Isactive),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "EmployeeDeactivate_Update_Date", UpdateEmployeeDeactivate);
        }


        public string LeaveDaysFromSalary(string empNo, string FinaYear, string Month, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] MonthYear = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",empNo),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Month",Month),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)

            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "LeaveDetailsFromSalary_SP", MonthYear);
            return str;
        }
        public string LeaveDaysFromSalary_ForLabour(string empNo, string FinaYear, string Month, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] MonthYear = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",empNo),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Month",Month),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)

            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "LeaveDetailsFromSalary_For_Labour_SP", MonthYear);
            return str;
        }
        public DataTable TakenLeaveFromMonth(string EmpNo, string Month, string FinaYear)
        {
            System.Data.SqlClient.SqlParameter[] Salary =
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@Months",Month),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear)
            };
            DataTable dt = SQL.ExecuteDatatable("TakenLeaveFromMonth_SP", Salary);
            return dt;
        }
        public DataTable MstBankDropDown()
        {
            System.Data.SqlClient.SqlParameter[] Salary =
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DropDownBank_SP", Salary);
            return dt;
        }
        public void SalaryDetailsForLabor(string EmpNo, string Workeddays, string Flatrate, DateTime Salarydate, Decimal Totamout, string Month, string Year, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryDetails =
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@Workeddays",Workeddays),
                new System.Data.SqlClient.SqlParameter("@Flatrate",Flatrate),
                new System.Data.SqlClient.SqlParameter("@Salarydate",Salarydate),
                new System.Data.SqlClient.SqlParameter("@TotalAmt",Totamout),
                new System.Data.SqlClient.SqlParameter("@Month",Month),
                new System.Data.SqlClient.SqlParameter("@Year",Year),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                     
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalaryDetailsForLabor_SP", SalaryDetails);
        }
        public void SalaryDetails(SalaryClass objSal, string EmpNo, string ExisistingNo, DateTime MyDate, string Month, string Year, string FinaYear, DateTime TransDate, DateTime fromdt, DateTime todt, string Cash_Bank, string WagesType)
        {
            System.Data.SqlClient.SqlParameter[] SalaryDetails =
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@MachineNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@ExisistingCode",ExisistingNo),
                new System.Data.SqlClient.SqlParameter("@SalaryThrough",objSal.SalaryThrough),
                new System.Data.SqlClient.SqlParameter("@FromBankACno",objSal.FromBankACno),
                new System.Data.SqlClient.SqlParameter("@FromBankName",objSal.FromBankName),
                new System.Data.SqlClient.SqlParameter("@FromBranch",objSal.FromBranch),
                new System.Data.SqlClient.SqlParameter("@ToBankACno",objSal.FromBankACno),
                new System.Data.SqlClient.SqlParameter("@ToBankName",objSal.FromBankName),
                new System.Data.SqlClient.SqlParameter("@ToBranch",objSal.FromBranch),
                new System.Data.SqlClient.SqlParameter("@Cash",objSal.Cash),
                new System.Data.SqlClient.SqlParameter("@BasicandDA",objSal.BasicAndDA),
                new System.Data.SqlClient.SqlParameter("@ProvidentFund",objSal.ProvidentFund),
                new System.Data.SqlClient.SqlParameter("@HRA",objSal.HRA),
                new System.Data.SqlClient.SqlParameter("@ESI",objSal.ESI),
                new System.Data.SqlClient.SqlParameter("@Conveyance",objSal.Conveyance),
                new System.Data.SqlClient.SqlParameter("@ProfessionalTax",objSal.ProvisionalTax),
                new System.Data.SqlClient.SqlParameter("@CCA",objSal.CCA),
                new System.Data.SqlClient.SqlParameter("@IncomeTax",objSal.IncomTax),
                new System.Data.SqlClient.SqlParameter("@GrossEarnings",objSal.GrossEarnings),
                new System.Data.SqlClient.SqlParameter("@SalaryAdvanceTrans",objSal.SalaryAdvanceTrans),
                new System.Data.SqlClient.SqlParameter("@MedicalReimburse",objSal.MedicalReiburse),
                new System.Data.SqlClient.SqlParameter("@PerIncentive",objSal.PerIncentive),
                new System.Data.SqlClient.SqlParameter("@allowances1",objSal.Allowances1),
                new System.Data.SqlClient.SqlParameter("@allowances2",objSal.Allowances2),
                new System.Data.SqlClient.SqlParameter("@allowances3",objSal.Allowances3),
                new System.Data.SqlClient.SqlParameter("@allowances4",objSal.Allowances4),
                new System.Data.SqlClient.SqlParameter("@allowances5",objSal.Allowances5),
                new System.Data.SqlClient.SqlParameter("@Deduction1",objSal.Deduction1),
                new System.Data.SqlClient.SqlParameter("@Deduction2",objSal.Deduction2),
                new System.Data.SqlClient.SqlParameter("@Deduction3",objSal.Deduction3),
                new System.Data.SqlClient.SqlParameter("@Deduction4",objSal.Deduction4),
                new System.Data.SqlClient.SqlParameter("@Deduction5",objSal.Deduction5),
                new System.Data.SqlClient.SqlParameter("@LossPay",objSal.LossofPay),
                new System.Data.SqlClient.SqlParameter("@TotalDeductions",objSal.TotalDeduction),
                new System.Data.SqlClient.SqlParameter("@TotalPay",objSal.Totalpay),
                new System.Data.SqlClient.SqlParameter("@NetPay",objSal.NetPay),
                new System.Data.SqlClient.SqlParameter("@SalaryDate",MyDate),
                new System.Data.SqlClient.SqlParameter("@Month",Month),
                new System.Data.SqlClient.SqlParameter("@Year",Year),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@TotalWorkingDays",objSal.TotalWorkingDays),
                new System.Data.SqlClient.SqlParameter("@MessDeduction",objSal.MessDeduction),
                new System.Data.SqlClient.SqlParameter("@HostelDeduction",objSal.HostelDeduction),
                new System.Data.SqlClient.SqlParameter("@LeaveDays",objSal.ApplyLeaveDays),
                new System.Data.SqlClient.SqlParameter("@LOPDays",objSal.LOPDays),
                new System.Data.SqlClient.SqlParameter("@TransDate",TransDate),
                new System.Data.SqlClient.SqlParameter("@Ccode", objSal.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", objSal.Lcode),
                new System.Data.SqlClient.SqlParameter("@NFh", objSal.NFh),
                new System.Data.SqlClient.SqlParameter("@OT", objSal.OT),
                new System.Data.SqlClient.SqlParameter("@FDA", objSal.FDA),
                new System.Data.SqlClient.SqlParameter("@VDA", objSal.VDA),
                new System.Data.SqlClient.SqlParameter("@Advance", objSal.Advance),
                new System.Data.SqlClient.SqlParameter("@stamp", objSal.Stamp),
                new System.Data.SqlClient.SqlParameter("@union", objSal.Union),
                new System.Data.SqlClient.SqlParameter("@pfsal", objSal.PfSalary),
                new System.Data.SqlClient.SqlParameter("@Words",objSal.Words),
                new System.Data.SqlClient.SqlParameter("@WorkedDays",objSal.work),
                new System.Data.SqlClient.SqlParameter("@weekoff", objSal.weekoff),
                new System.Data.SqlClient.SqlParameter("@CL", objSal.CL),
                new System.Data.SqlClient.SqlParameter("@FixBase", objSal.FixedBase),
                new System.Data.SqlClient.SqlParameter("@FixFDA", objSal.FixedFDA),
                new System.Data.SqlClient.SqlParameter("@FixHRA", objSal.FixedFRA),
                new System.Data.SqlClient.SqlParameter("@EmpPF", objSal.Emp_PF),
                new System.Data.SqlClient.SqlParameter("@from", fromdt),
                new System.Data.SqlClient.SqlParameter("@to", todt),
                new System.Data.SqlClient.SqlParameter("@home", objSal.HomeTown),
                new System.Data.SqlClient.SqlParameter("@HalfNightAmt", objSal.HalfNightAmt),
                new System.Data.SqlClient.SqlParameter("@FullNightAmt", objSal.FullNightAmt),
                new System.Data.SqlClient.SqlParameter("@SpinningAmt", objSal.SpinningAmt),
                new System.Data.SqlClient.SqlParameter("@DayIncentive", objSal.DayIncentive),
                new System.Data.SqlClient.SqlParameter("@ThreesidedAmt", objSal.ThreesidedAmt),
                new System.Data.SqlClient.SqlParameter("@Cash_Bank", Cash_Bank),
                new System.Data.SqlClient.SqlParameter("@WagesType", WagesType),
                new System.Data.SqlClient.SqlParameter("@BasicforSM", objSal.BasicforSM),
                new System.Data.SqlClient.SqlParameter("@BasicAndDANew", objSal.BasicDA),
                new System.Data.SqlClient.SqlParameter("@BasicHRA", objSal.Basic_HRA),
                new System.Data.SqlClient.SqlParameter("@ConvAllow", objSal.Conv_Allow),
                new System.Data.SqlClient.SqlParameter("@EduAllow", objSal.Edu_Allow),
                new System.Data.SqlClient.SqlParameter("@MediAllow", objSal.Medi_Allow),
                new System.Data.SqlClient.SqlParameter("@BasicRAI", objSal.Basic_RAI),
                new System.Data.SqlClient.SqlParameter("@WashingAllow", objSal.Washing_Allow),
                new System.Data.SqlClient.SqlParameter("@RoundOffNetPay", objSal.RoundOffNetPay),
                new System.Data.SqlClient.SqlParameter("@OTHoursNew", objSal.OTHoursNew),
                new System.Data.SqlClient.SqlParameter("@OTHoursAmt", objSal.OTHoursAmt),
                new System.Data.SqlClient.SqlParameter("@Leave_Credit_Check", objSal.Leave_Credit_Check),
                new System.Data.SqlClient.SqlParameter("@Fixed_Work_Days", objSal.Fixed_Work_Days),
                new System.Data.SqlClient.SqlParameter("@WH_Work_Days", objSal.WH_Work_Days),
                new System.Data.SqlClient.SqlParameter("@DedOthers1", objSal.Ded_Others1),
                new System.Data.SqlClient.SqlParameter("@DedOthers2", objSal.Ded_Others2),
                new System.Data.SqlClient.SqlParameter("@EmployeerPFone", objSal.EmployeerPFone),
                new System.Data.SqlClient.SqlParameter("@EmployeerPFTwo", objSal.EmployeerPFTwo),
                new System.Data.SqlClient.SqlParameter("@EmployeerESI", objSal.EmployeerESI),
                new System.Data.SqlClient.SqlParameter("@Leave_Credit_Days", objSal.Leave_Credit_Days),
                new System.Data.SqlClient.SqlParameter("@Leave_Credit_Add", objSal.Leave_Credit_Add),
                new System.Data.SqlClient.SqlParameter("@Leave_Credit_Type", objSal.Leave_Credit_Type)
             
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalaryDetails_SP", SalaryDetails);
        }

        public void LeaveDetails(LeaveClass objLeav, DateTime myDate, string EmpNo, string month)
        {
            System.Data.SqlClient.SqlParameter[] LeaveDetails = 
            {
                new System.Data.SqlClient.SqlParameter("@TotalWorkingDays",objLeav.TotalWorkingDays),
                new System.Data.SqlClient.SqlParameter("@EnterDate",myDate),
                new System.Data.SqlClient.SqlParameter("@LeaveType1",objLeav.LeaveType1),
                new System.Data.SqlClient.SqlParameter("@LeaveType2",objLeav.LeaveType2),
                new System.Data.SqlClient.SqlParameter("@LeaveType3",objLeav.LeaveType3),
                new System.Data.SqlClient.SqlParameter("@NoofLeave1",objLeav.Noofleave1),
                new System.Data.SqlClient.SqlParameter("@NoofLeave2",objLeav.Noofleave2),
                new System.Data.SqlClient.SqlParameter("@NoofLeave3",objLeav.Noofleave3),
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@Month",month)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "InsertLeaveDetails_SP", LeaveDetails);
        }
        public void LeaveDetailsUpdateRowGrid(LeaveClass objLeav, DateTime myDate, string EmpNo, string month, string Year)
        {
            System.Data.SqlClient.SqlParameter[] LeaveDetails = 
            {
                new System.Data.SqlClient.SqlParameter("@TotalWorkingDays",objLeav.TotalWorkingDays),
                new System.Data.SqlClient.SqlParameter("@EnterDate",myDate),
                new System.Data.SqlClient.SqlParameter("@CasualLeave",objLeav.CasualLeave),
                new System.Data.SqlClient.SqlParameter("@SickLeave",objLeav.SickLeave),
                new System.Data.SqlClient.SqlParameter("@EarnedLeave",objLeav.EarnedLeave),
                new System.Data.SqlClient.SqlParameter("@MaternalLeave",objLeav.MaternalLeave),
                new System.Data.SqlClient.SqlParameter("@NoofLeave",objLeav.NoofLeave),
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@Month",month),
                new System.Data.SqlClient.SqlParameter("@Year",Year)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "InsertLeaveDetailsFromGridRow_SP", LeaveDetails);
        }

        public void UserRegistration(UserRegistrationClass objReg)
        {
            System.Data.SqlClient.SqlParameter[] UserRegistration = 
            {
                new System.Data.SqlClient.SqlParameter("@UserCode",objReg.UserCode),
                new System.Data.SqlClient.SqlParameter("@UserName",objReg.UserName),
          
                new System.Data.SqlClient.SqlParameter("@Password",objReg.Password),
                new System.Data.SqlClient.SqlParameter("@IsAdmin",objReg.IsAdmin),
                new System.Data.SqlClient.SqlParameter("@Mobile",objReg.Mobile),
                new System.Data.SqlClient.SqlParameter("@Department",objReg.Department),
                new System.Data.SqlClient.SqlParameter("@Designation",objReg.Designation),
                new System.Data.SqlClient.SqlParameter("@Ccode", objReg.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", objReg.Lcode)
             
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "MstUserRegistration_SP", UserRegistration);
        }

        public DataTable DropDwonLeaveType()
        {
            System.Data.SqlClient.SqlParameter[] LeaveType = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("DropDownLeave_SP", LeaveType);
            return dt;
        }
        public DataTable LeaveDatailsDisplayFromGridView(string Month)
        {
            System.Data.SqlClient.SqlParameter[] LeaveDetails = 
            {
                new System.Data.SqlClient.SqlParameter("@Month",Month)
            };
            DataTable dt = SQL.ExecuteDatatable("LeaveDetailsDisplayFromGridView_SP", LeaveDetails);
            return dt;
        }
        public DataTable LeaveDatailsDisplayFromGridViewAllNewEntry()
        {
            System.Data.SqlClient.SqlParameter[] LeaveDetails = 
            {
                
            };
            DataTable dt = SQL.ExecuteDatatable("LeaveDetailsDisplayFromGridViewAllNewEntry_SP", LeaveDetails);
            return dt;
        }
        public DataTable LeaveallocationfromGrid()
        {
            System.Data.SqlClient.SqlParameter[] Leaveallocation =
            {
            };
            DataTable dt = SQL.ExecuteDatatable("Leaveallocation_SP", Leaveallocation);
            return dt;
        }

        public string Rolecd(string EmpNo)
        {
            System.Data.SqlClient.SqlParameter[] Rolecd = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "RoleCd_SP", Rolecd);
            return str;
        }
        public string EmpGrade(string EmpNo)
        {
            System.Data.SqlClient.SqlParameter[] EmpGrade = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "EmpGrade_SP", EmpGrade);
            return str;
        }

        public string SalaryMonth(string EmpNo, string FinaYear, string Ccode, string Lcode, string month, DateTime fromdt, DateTime todt)
        {
            System.Data.SqlClient.SqlParameter[] EmpGrade = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Mont", month),
                new System.Data.SqlClient.SqlParameter("@from", fromdt),
                new System.Data.SqlClient.SqlParameter("@to", todt)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalaryMonth_SP", EmpGrade);
            return str;
        }
        public DataTable DropDownBank()
        {
            System.Data.SqlClient.SqlParameter[] DropDownBank =
            {

            };
            DataTable dt = SQL.ExecuteDatatable("DropDownBank_SP", DropDownBank);
            return dt;
        }
        public DataTable OfficialProfileGridViewForAdmin(string staff, string Dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] OfficialProfileGrid =
            {
                new System.Data.SqlClient.SqlParameter("@Staff",staff),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
             
               
            };
            DataTable dt = SQL.ExecuteDatatable("OfficialProfileFromEmpDetailsForAdmin_SP", OfficialProfileGrid);
            return dt;
        }
        public DataTable OfficialProfileGridView(string staff, string Dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] OfficialProfileGrid =
            {
                new System.Data.SqlClient.SqlParameter("@Staff",staff),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
               
            };
            DataTable dt = SQL.ExecuteDatatable("OfficialProfileFromEmpDetails_SP", OfficialProfileGrid);
            return dt;
        }
        public DataTable DropDownProbationPriod()
        {
            System.Data.SqlClient.SqlParameter[] ProbationPeriodDis =
            {

            };
            DataTable dt = SQL.ExecuteDatatable("DropDownProbationPriod_SP", ProbationPeriodDis);
            return dt;
        }
        public DataTable DropDownInsuracne()
        {
            System.Data.SqlClient.SqlParameter[] Insurance =
            {

            };
            DataTable dt = SQL.ExecuteDatatable("DropDownInsurance_SP", Insurance);
            return dt;
        }

        public void OfficalProfile(OfficialprofileClass objOff, string empno, DateTime DofJoin, DateTime DofConfirm, DateTime ExpiryDate, DateTime contractStartDate, DateTime contractEndDate, string pf_type, DateTime PFDOJ, DateTime ESIDOJ)
        {
            System.Data.SqlClient.SqlParameter[] officialprofiel = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",empno),
                new System.Data.SqlClient.SqlParameter("@MachineNo",empno),
                new System.Data.SqlClient.SqlParameter("@Dateofjoining",DofJoin),
                //new System.Data.SqlClient.SqlParameter("@Probationperiod",objOff.Probationperiod),
                new System.Data.SqlClient.SqlParameter("@Confirmationdate",DofConfirm),
                new System.Data.SqlClient.SqlParameter("@Pannumber",objOff.Pannumber),
                new System.Data.SqlClient.SqlParameter("@ESICnumber",objOff.ESICnumber),
                new System.Data.SqlClient.SqlParameter("@PFnumber",objOff.PFnumber),
                new System.Data.SqlClient.SqlParameter("@Insucompanyname",objOff.InsurancecompanyName),
                new System.Data.SqlClient.SqlParameter("@Insurancenumber",objOff.Insurancenumber),
                new System.Data.SqlClient.SqlParameter("@Eligibleforovertime",objOff.Eligibleforovertime),
                new System.Data.SqlClient.SqlParameter("@ModeofPayement",objOff.modeofpayment),
                new System.Data.SqlClient.SqlParameter("@ReportingAuthorityname",objOff.ReportingAuthorityName),
                new System.Data.SqlClient.SqlParameter("@LabourType",objOff.LabourType),
                new System.Data.SqlClient.SqlParameter("@ContractType",objOff.ContractType),
                new System.Data.SqlClient.SqlParameter("@ExpiryDate",ExpiryDate),
                new System.Data.SqlClient.SqlParameter("@Wages",objOff.WagesType),
                new System.Data.SqlClient.SqlParameter("@ContractPeriod",objOff.contractperiod),
                new System.Data.SqlClient.SqlParameter("@BasicSalary",objOff.BasicSalary),
                new System.Data.SqlClient.SqlParameter("@ElgibleESI",objOff.EligibleESI),
                new System.Data.SqlClient.SqlParameter("@ContractStartDate",contractStartDate),
                new System.Data.SqlClient.SqlParameter("@ContractEndDate",contractEndDate),
                new System.Data.SqlClient.SqlParameter("@FinancialYear",objOff.Financialperiod),
                new System.Data.SqlClient.SqlParameter("@ProfileType",objOff.ProfileType),
                new System.Data.SqlClient.SqlParameter("@SalaryThroug",objOff.modeofpayment),
                new System.Data.SqlClient.SqlParameter("@BankAccno",objOff.BankACno),
                new System.Data.SqlClient.SqlParameter("@BankName",objOff.BankName),
                new System.Data.SqlClient.SqlParameter("@BranchName",objOff.Branchname),
                new System.Data.SqlClient.SqlParameter("@EligiblePF", objOff.EligblePF),
                new System.Data.SqlClient.SqlParameter("@Ccode", objOff.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", objOff.Lcode),
                new System.Data.SqlClient.SqlParameter("@pf_type", pf_type),
                new System.Data.SqlClient.SqlParameter("@chkMonths", objOff.ChkMonths),
                new System.Data.SqlClient.SqlParameter("@chkYears", objOff.ChkYears),
                new System.Data.SqlClient.SqlParameter("Duration", objOff.DurationPeriod),
                new System.Data.SqlClient.SqlParameter("@ESICode", objOff.ESICode),
                new System.Data.SqlClient.SqlParameter("@PFDOJ",PFDOJ),
                new System.Data.SqlClient.SqlParameter("@ESIDOJ",ESIDOJ)



                   
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Officialprofile_SP", officialprofiel);
        }


        public string EmployeeNoFromOfficialProfile(string empNo)
        {
            System.Data.SqlClient.SqlParameter[] employereg = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",empNo)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "EmployeeNoFromOfficialDetails_SP", employereg);
            return str;
        }
        public void Bank(MastersClass objMas)
        {
            System.Data.SqlClient.SqlParameter[] Bank =
            {
                new System.Data.SqlClient.SqlParameter("@Bankcd",objMas.Bankcd),
                new System.Data.SqlClient.SqlParameter("@Bankname",objMas.Bankname)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Bank_SP", Bank);
        }
        public void UpdateBank(MastersClass objMas)
        {
            System.Data.SqlClient.SqlParameter[] UpdateBank =
            {
                new System.Data.SqlClient.SqlParameter("@Bankcd",objMas.Bankcd),
                new System.Data.SqlClient.SqlParameter("@Bankname",objMas.Bankname)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UpdateBank_SP", UpdateBank);
        }



        public DataTable BankDisplay()
        {
            System.Data.SqlClient.SqlParameter[] BankDisplay =
            {

            };
            DataTable dt = SQL.ExecuteDatatable("BankDisplay_SP", BankDisplay);
            return dt;
        }
        public DataTable PFMonthlyStatement(string EmpNo, string Month, string Year)
        {
            System.Data.SqlClient.SqlParameter[] PFMonthlyStatement = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@Month",Month),
                new System.Data.SqlClient.SqlParameter("@Year",Year)

            };
            DataTable dt = SQL.ExecuteDatatable("PFMonthlysatementSearchRecord_SP", PFMonthlyStatement);
            return dt;
        }

        public void InsertPFMonthlyStatement(PFMonthlySatementClass objPFM, string Month, string Year, DateTime PFDate)
        {
            System.Data.SqlClient.SqlParameter[] PFMonthlyInsert = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",objPFM.EmpNo),
                new System.Data.SqlClient.SqlParameter("@EmpName",objPFM.EmpName),
                new System.Data.SqlClient.SqlParameter("@PFNumber",objPFM.PFNumber),
                new System.Data.SqlClient.SqlParameter("@WorkedDays",objPFM.WorkedDays),
                new System.Data.SqlClient.SqlParameter("@Basic",objPFM.Basic),
                new System.Data.SqlClient.SqlParameter("@PF",objPFM.PF),
                new System.Data.SqlClient.SqlParameter("@VPF",objPFM.VPF),
                new System.Data.SqlClient.SqlParameter("@EPS",objPFM.EPS),
                new System.Data.SqlClient.SqlParameter("@EPF",objPFM.EPF),
                new System.Data.SqlClient.SqlParameter("@Month",Month),
                new System.Data.SqlClient.SqlParameter("@Year",Year),
                new System.Data.SqlClient.SqlParameter("@PFDate",PFDate)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "InsertPFMonthlyStatement_SP", PFMonthlyInsert);
        }

        public void InsertPFMonthlyStatement(PFForm3AClass objPF3A, string Month, string Year, DateTime PFDate)
        {
            System.Data.SqlClient.SqlParameter[] PF3ASatementInsert = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",objPF3A.EmpNo),
                new System.Data.SqlClient.SqlParameter("@PFNumber",objPF3A.PFNumber),
                new System.Data.SqlClient.SqlParameter("@AmountWages",objPF3A.AmountWages),
                new System.Data.SqlClient.SqlParameter("@EPF",objPF3A.EPF),
                new System.Data.SqlClient.SqlParameter("@RateofHigerValue",objPF3A.RateofHiger),
                new System.Data.SqlClient.SqlParameter("@EPFDiff",objPF3A.EPFDiff),
                new System.Data.SqlClient.SqlParameter("@PensionFund",objPF3A.PensonFund),
                new System.Data.SqlClient.SqlParameter("@RefundAdvance",objPF3A.RefundofAdd),
                new System.Data.SqlClient.SqlParameter("@NonContribution",objPF3A.NonContribution),
                new System.Data.SqlClient.SqlParameter("@CompanyCdNumber",objPF3A.CompCdnumber),
                new System.Data.SqlClient.SqlParameter("@CompanyAdd1",objPF3A.CompAdd1),
                new System.Data.SqlClient.SqlParameter("@CompanyAdd2",objPF3A.CompAdd2),
                new System.Data.SqlClient.SqlParameter("@Location",objPF3A.Location),
                new System.Data.SqlClient.SqlParameter("@Month",Month),
                new System.Data.SqlClient.SqlParameter("@Year",Year),
                new System.Data.SqlClient.SqlParameter("@PFDate",PFDate)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "PFForm3AInsertSatement_SP", PF3ASatementInsert);
        }

        public DataTable ESIMonthlyStatement(string EmpNo, string Month, string Year)
        {
            System.Data.SqlClient.SqlParameter[] ESIMonthlyStatement = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@Month",Month),
                new System.Data.SqlClient.SqlParameter("@Year",Year)

            };
            DataTable dt = SQL.ExecuteDatatable("ESIMonthlyStatementSearch_SP", ESIMonthlyStatement);
            return dt;
        }
        public void InsertESIMonthlyStatement(ESIMonthlyStatementClass objESI, string Month, string Year, DateTime ESIDate, DateTime ChallenDate)
        {
            System.Data.SqlClient.SqlParameter[] ESIMonthlySatementInsert = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",objESI.EmpNo),
                new System.Data.SqlClient.SqlParameter("@ESINumber",objESI.ESINumber),
                new System.Data.SqlClient.SqlParameter("@ESIAmount",objESI.ESIAmount),
                new System.Data.SqlClient.SqlParameter("@ChallenDate",ChallenDate),
                new System.Data.SqlClient.SqlParameter("@BankName",objESI.BankNm),
                new System.Data.SqlClient.SqlParameter("@BranchName",objESI.Branch),
                new System.Data.SqlClient.SqlParameter("@BranchOffice",objESI.OfficeNm),
                new System.Data.SqlClient.SqlParameter("@ESIDate",ESIDate),
                new System.Data.SqlClient.SqlParameter("@Month",Month),
                new System.Data.SqlClient.SqlParameter("@Year",Year)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "ESIMonthlyStatementInsert_SP", ESIMonthlySatementInsert);
        }

        public DataTable ESIHalfYarlySearchStatement(string EmpNo, string Month, string Year)
        {
            System.Data.SqlClient.SqlParameter[] ESIMonthlyStatement = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                //new System.Data.SqlClient.SqlParameter("@Month",Month),
                //new System.Data.SqlClient.SqlParameter("@Year",Year)

            };
            DataTable dt = SQL.ExecuteDatatable("ESIHalfYearlyForm6Search_SP", ESIMonthlyStatement);
            return dt;
        }

        public void InsertESIForm6AStatement(ESIHalfYarlyForm6Class objESI6, string Month, string Year, DateTime ESIDate)
        {
            System.Data.SqlClient.SqlParameter[] ESIForm6SatementInsert = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",objESI6.EmpNo),
                new System.Data.SqlClient.SqlParameter("@ESINumber",objESI6.ESINumber),
                new System.Data.SqlClient.SqlParameter("@ESIAmount",objESI6.ESIAmount),
                new System.Data.SqlClient.SqlParameter("@ESIOffice",objESI6.ESIOffice),
                new System.Data.SqlClient.SqlParameter("@FirstDaysWages",objESI6.RateofFirstWages),
                new System.Data.SqlClient.SqlParameter("@NoofDaysWagesPaid",objESI6.NoofdayswagesPeriod),
                new System.Data.SqlClient.SqlParameter("@TotalAmtWagesPaid",objESI6.TotalAmtWagesPeriod),
                new System.Data.SqlClient.SqlParameter("@EmpShareContri",objESI6.EmpShareContribution),
                new System.Data.SqlClient.SqlParameter("@ESIDate",ESIDate),
                new System.Data.SqlClient.SqlParameter("@Month",Month),
                new System.Data.SqlClient.SqlParameter("@Year",Year)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "ESIHalfYearlyForm6Insert_SP", ESIForm6SatementInsert);
        }
        public DataTable AlreadyTakenLeave(string EmpNo, string FinaYear, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AlreadyTakenLeave = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                 new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                 new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                
            };
            DataTable dt = SQL.ExecuteDatatable("alreadyleaveapproval", AlreadyTakenLeave);
            return dt;
        }
        public DataTable AlreadyTakenLeaveForLabour(string EmpNo, string FinaYear, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AlreadyTakenLeave = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                 new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                 new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                
            };
            DataTable dt = SQL.ExecuteDatatable("AllreadyleaveTakenForLaour_SP", AlreadyTakenLeave);
            return dt;
        }
        public DataTable LeaveAllocationEmployeeForEdit(string stafforLabour, string dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] LeaveAllocationStaff = 
            {
                new System.Data.SqlClient.SqlParameter("@staff",stafforLabour),
                new System.Data.SqlClient.SqlParameter("@dept",dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                
            };
            DataTable dt = SQL.ExecuteDatatable("StaffLeaveAllocation_Loadgrid_ForEdit", LeaveAllocationStaff);
            return dt;
        }
        public DataTable LeaveAllocationEmployeeForEdit_user(string stafforLabour, string dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] LeaveAllocationStaff = 
            {
                new System.Data.SqlClient.SqlParameter("@staff",stafforLabour),
                new System.Data.SqlClient.SqlParameter("@dept",dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                
            };
            DataTable dt = SQL.ExecuteDatatable("staffLeaveAllocation_Loadgrid_ForEdit_User", LeaveAllocationStaff);
            return dt;
        }
        public DataTable LeaveAllocationEmployee(string stafforLabour, string dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] LeaveAllocationStaff = 
            {
                new System.Data.SqlClient.SqlParameter("@staff",stafforLabour),
                new System.Data.SqlClient.SqlParameter("@dept",dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                
            };
            DataTable dt = SQL.ExecuteDatatable("staffLeaveAllocation_Loadgrid", LeaveAllocationStaff);
            return dt;
        }
        public DataTable LeaveAllocationLabour(string stafforLabour, string dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] LeaveAllocationStaff = 
            {
                new System.Data.SqlClient.SqlParameter("@staff",stafforLabour),
                new System.Data.SqlClient.SqlParameter("@dept",dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                
            };
            DataTable dt = SQL.ExecuteDatatable("LabourLeaveAllocation_SP", LeaveAllocationStaff);
            return dt;
        }
        public DataTable LeaveAllocation_User(string stafforLabour, string dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] LeaveAllocationStaff = 
            {
                new System.Data.SqlClient.SqlParameter("@staff",stafforLabour),
                new System.Data.SqlClient.SqlParameter("@dept",dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                
            };
            DataTable dt = SQL.ExecuteDatatable("staffLeaveAllocation_user_sp", LeaveAllocationStaff);
            return dt;
        }
        public DataTable LeaveAllocationLabour_user(string stafforLabour, string dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] LeaveAllocationStaff = 
            {
                new System.Data.SqlClient.SqlParameter("@staff",stafforLabour),
                new System.Data.SqlClient.SqlParameter("@dept",dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                
            };
            DataTable dt = SQL.ExecuteDatatable("LabourLeaveAllocation_user_SP", LeaveAllocationStaff);
            return dt;
        }
        public string EmployeeLeaveallocationupdate(string Empcode)
        {
            System.Data.SqlClient.SqlParameter[] EmployeeCode = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpCode",Empcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Leave_Allocation_UpdateVerify", EmployeeCode);
            return str;
        }
        public void LeaveallocationUpdate(string empCode, string LeaveAllocation, string Designation)
        {
            System.Data.SqlClient.SqlParameter[] UpdateLeaveAllocation = 
            {
                new System.Data.SqlClient.SqlParameter("@EmployeeNo",empCode),
                new System.Data.SqlClient.SqlParameter("@LeaveDays",LeaveAllocation),
                new System.Data.SqlClient.SqlParameter("@Designation",Designation)
               
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Leaveallocation_Update", UpdateLeaveAllocation);
        }
        public void InsertLeaveAllocation(LeaveAllocation objla)
        {
            System.Data.SqlClient.SqlParameter[] LeaveAllocationInsert = 
            {
                new System.Data.SqlClient.SqlParameter("@EmployeeNo",objla.EmpNo),
                new System.Data.SqlClient.SqlParameter("@LeaveDays",objla.LeaveDays),
                new System.Data.SqlClient.SqlParameter("@Designation",objla.Designation),
                new System.Data.SqlClient.SqlParameter("@Department",objla.Department),
                new System.Data.SqlClient.SqlParameter("@StafforLabour",objla.StafforLabour),
                new System.Data.SqlClient.SqlParameter("@FinYear",objla.FinYear),
                new System.Data.SqlClient.SqlParameter("@LeaveAllForMoth",objla.LeaveAllForMonth)
               
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Leaveallocation_Insert", LeaveAllocationInsert);
        }
        public void UpdateLeaveAllocation(LeaveAllocation objla)
        {
            System.Data.SqlClient.SqlParameter[] LeaveAllocationInsert = 
            {
                new System.Data.SqlClient.SqlParameter("@EmployeeNo",objla.EmpNo),
                new System.Data.SqlClient.SqlParameter("@LeaveDays",objla.LeaveDays),
                new System.Data.SqlClient.SqlParameter("@LeaveAllForMoth",objla.LeaveAllForMonth)
               
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Leaveallocation_Update_SP", LeaveAllocationInsert);
        }
        #region ApplyForLeave
        public DataTable ApplyForLeaveinEmployee(string EmpNo, string FinaYear, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] ApplyForLeave = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
               

            };
            DataTable dt = SQL.ExecuteDatatable("EmpDetailsForApplyForLeave_SP", ApplyForLeave);
            return dt;
        }
        public DataTable ApplyForLeaveinLabour_1(string EmpNo, string FinaYear, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] ApplyForLeave = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear)
               

            };
            DataTable dt = SQL.ExecuteDatatable("EmpDetailsForApplyForLeaveinLabour_SP", ApplyForLeave);
            return dt;
        }
        public void ApplyForLeave(string EmpNo, string Leavetype, string ApplyLeave, string Totalworkingdays, string Leavecode, DateTime FromDate, DateTime ToDate, string month, string Year, string AllocatedLeave, string AlreadyTakenLeave, string ExcessLEave, string ApprovedBy, DateTime ApprovedDate, string FinYear, string Maternity, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] LeaveDetails = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@Leavetype",Leavetype),
                new System.Data.SqlClient.SqlParameter("@ApplyLeave",ApplyLeave),
                new System.Data.SqlClient.SqlParameter("@TotalWorkingdays",Totalworkingdays),
                new System.Data.SqlClient.SqlParameter("@Leavecode",Leavecode),
                new System.Data.SqlClient.SqlParameter("@FromDate",FromDate),
                new System.Data.SqlClient.SqlParameter("@ToDate",ToDate),
                new System.Data.SqlClient.SqlParameter("@Month",month),
                new System.Data.SqlClient.SqlParameter("@Year",Year),
                new System.Data.SqlClient.SqlParameter("@AllocatedLeaves",AllocatedLeave),
                new System.Data.SqlClient.SqlParameter("@AlreadyTakenLeave",AlreadyTakenLeave),
                new System.Data.SqlClient.SqlParameter("@ExcessLeave",ExcessLEave),
                new System.Data.SqlClient.SqlParameter("@Approvedby",ApprovedBy),
                new System.Data.SqlClient.SqlParameter("@ApprovedDate",ApprovedDate),
                new System.Data.SqlClient.SqlParameter("@FinYear",FinYear),
                new System.Data.SqlClient.SqlParameter("@Maternity",Maternity),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)

            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "ApplyForLeave_Insert_SP", LeaveDetails);
        }
        public void ApplyForLeaveinLabour(string EmpNo, string Leavetype, string ApplyLeave, string Totalworkingdays, string Leavecode, DateTime FromDate, DateTime ToDate, string month, string Year, string ApprovedBy, DateTime ApprovedDate, string FinYear, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] LeaveDetailsLabour = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@Leavetype",Leavetype),
                new System.Data.SqlClient.SqlParameter("@TotalWorkingdays",Totalworkingdays),
                new System.Data.SqlClient.SqlParameter("@Leavecode",Leavecode),
                new System.Data.SqlClient.SqlParameter("@ApplyLeave",ApplyLeave),
                new System.Data.SqlClient.SqlParameter("@FromDate",FromDate),
                new System.Data.SqlClient.SqlParameter("@ToDate",ToDate),
                new System.Data.SqlClient.SqlParameter("@Month",month),
                new System.Data.SqlClient.SqlParameter("@Year",Year),
                new System.Data.SqlClient.SqlParameter("@ExcessLeave",ApplyLeave),
                new System.Data.SqlClient.SqlParameter("@Approvedby",ApprovedBy),
                new System.Data.SqlClient.SqlParameter("@ApprovedDate",ApprovedDate),
                new System.Data.SqlClient.SqlParameter("@FinYear",FinYear),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
             

            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "ApplyForLeave_Labour_Insert_SP", LeaveDetailsLabour);
        }
        public DataTable AllReadyApplyForLeaveDate(string EmpNo, DateTime FromDate, DateTime ToDate, string FinaYear, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] CheckApplyLeave = 
            {
               new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
               new System.Data.SqlClient.SqlParameter("@FromDate",FromDate),
               new System.Data.SqlClient.SqlParameter("@ToDate",ToDate),
               new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
               new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)

            };
            DataTable dt = SQL.ExecuteDatatable("AllReadyApplyForLeaveDate_SP", CheckApplyLeave);
            return dt;

        }
        public string Leavecode()
        {
            System.Data.SqlClient.SqlParameter[] Leavecoce = 
            {
              
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "LeaveCode_SP", Leavecoce);
            return str;
        }
        public string Female(string EmpCode, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Leavecoce = 
            {
               new System.Data.SqlClient.SqlParameter("@EmpCode",EmpCode),
               new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Female_SP", Leavecoce);
            return str;
        }
        public string AvailableMaternityLeave(string EmpCode, string FinancialYr, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Leavecoce = 
            {
               new System.Data.SqlClient.SqlParameter("@EmpNo",EmpCode),
               new System.Data.SqlClient.SqlParameter("@FinaYear",FinancialYr),
               new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "AvailableMaternityLeave_SP", Leavecoce);
            return str;
        }

        public DataTable FillApplyForGridView()
        {
            System.Data.SqlClient.SqlParameter[] ApplyForLeave = 
            {
              
               

            };
            DataTable dt = SQL.ExecuteDatatable("FillForApplyLeaveinGridview_SP", ApplyForLeave);
            return dt;
        }
        public DataTable FillApplyForGridViewForStatus(string Month)
        {
            System.Data.SqlClient.SqlParameter[] ApplyForLeave = 
            {
              
               new System.Data.SqlClient.SqlParameter("@Month",Month)

            };
            DataTable dt = SQL.ExecuteDatatable("LeaveStatusGridView_SP", ApplyForLeave);
            return dt;
        }
        public DataTable FillApplyForGridViewForLeaveCode(string Leavecd)
        {
            System.Data.SqlClient.SqlParameter[] ApplyForLeave = 
            {
              
               new System.Data.SqlClient.SqlParameter("@LeaveCd",Leavecd)

            };
            DataTable dt = SQL.ExecuteDatatable("FillForApplyLeaveinGridview_LeaveCode_SP", ApplyForLeave);
            return dt;
        }
        public DataTable FillMonth()
        {
            System.Data.SqlClient.SqlParameter[] ApplyForLeave = 
            {
              
             

            };
            DataTable dt = SQL.ExecuteDatatable("FillMonth_SP", ApplyForLeave);
            return dt;
        }
        public void UpdateForApplyLeave(string empno, string Status, string leavecd, string noofleave)
        {
            System.Data.SqlClient.SqlParameter[] updateleave = 
            {
              
               new System.Data.SqlClient.SqlParameter("@EmpNo",empno),
               new System.Data.SqlClient.SqlParameter("@Status",Status),
               new System.Data.SqlClient.SqlParameter("@Leavecd",leavecd),
               new System.Data.SqlClient.SqlParameter("@Noofleave",noofleave)

            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UpdateForApplyLeave_SP", updateleave);
        }

        public string LeavecountMessage()
        {
            System.Data.SqlClient.SqlParameter[] Leavecoce = 
            {
              
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "LeavcountForMessage_SP", Leavecoce);
            return str;
        }
        #endregion

        public DataTable SalaryEmpIDLoad(string stafflabour, string department, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryEmpIDLoad = 
            {
                new System.Data.SqlClient.SqlParameter("@Staff", stafflabour),
                new System.Data.SqlClient.SqlParameter("@Dept", department),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeNameandNumber_SP", SalaryEmpIDLoad);
            return dt;
        }
        //public DataTable BonusEmployeeName(string EmpID)
        //{
        //    System.Data.SqlClient.SqlParameter[] BonusEmployeeName = 
        //    {
        //        new System.Data.SqlClient.SqlParameter("@empCode", EmpID)

        //    };
        //    DataTable dt = SQL.ExecuteDatatable("empNameLoad_bonus", BonusEmployeeName);
        //    return dt;
        //}
        public DataTable BasicAmountForSalary(string EmpNo, string FinaYear)
        {
            System.Data.SqlClient.SqlParameter[] BasicAmountForSalry = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@FinaYear", FinaYear)
               
            };
            DataTable dt = SQL.ExecuteDatatable("BasicAmountForSalary_SP", BasicAmountForSalry);
            return dt;
        }
        #region From Ragu
        public DataTable EditOfficialprofile_Load(string empID)
        {
            System.Data.SqlClient.SqlParameter[] EditOfficialprofile_Load = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpID", empID)
            };
            DataTable dt = SQL.ExecuteDatatable("EditOfficialprofile_Load", EditOfficialprofile_Load);
            return dt;
        }

        public void UpdateOfficalProfile(OfficialprofileClass objOff, string empno, DateTime DofJoin, DateTime DofConfirm, DateTime ExpiryDate, DateTime contractStartDate, DateTime contractEndDate, string pf_type, DateTime PFDOJ, DateTime ESIDOJ)
        {
            System.Data.SqlClient.SqlParameter[] officialprofiel = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",empno),
                new System.Data.SqlClient.SqlParameter("@MachineNo",empno),
                new System.Data.SqlClient.SqlParameter("@Dateofjoining",DofJoin),
                //new System.Data.SqlClient.SqlParameter("@Probationperiod",objOff.Probationperiod),
                new System.Data.SqlClient.SqlParameter("@Confirmationdate",DofConfirm),
                new System.Data.SqlClient.SqlParameter("@Pannumber",objOff.Pannumber),
                new System.Data.SqlClient.SqlParameter("@ESICnumber",objOff.ESICnumber),
                new System.Data.SqlClient.SqlParameter("@PFnumber",objOff.PFnumber),
                new System.Data.SqlClient.SqlParameter("@Insucompanyname",objOff.InsurancecompanyName),
                new System.Data.SqlClient.SqlParameter("@Insurancenumber",objOff.Insurancenumber),
                new System.Data.SqlClient.SqlParameter("@Eligibleforovertime",objOff.Eligibleforovertime),
                new System.Data.SqlClient.SqlParameter("@ModeofPayement",objOff.modeofpayment),
                new System.Data.SqlClient.SqlParameter("@ReportingAuthorityname",objOff.ReportingAuthorityName),
                new System.Data.SqlClient.SqlParameter("@LabourType",objOff.LabourType),
                new System.Data.SqlClient.SqlParameter("@ContractType",objOff.ContractType),
                new System.Data.SqlClient.SqlParameter("@ExpiryDate",ExpiryDate),
                new System.Data.SqlClient.SqlParameter("@Wages",objOff.WagesType),
                new System.Data.SqlClient.SqlParameter("@ContractPeriod",objOff.contractperiod),
                new System.Data.SqlClient.SqlParameter("@BasicSalary",objOff.BasicSalary),
                new System.Data.SqlClient.SqlParameter("@ElgibleESI",objOff.EligibleESI),
                new System.Data.SqlClient.SqlParameter("@ContractStartDate",contractStartDate),
                new System.Data.SqlClient.SqlParameter("@ContractEndDate",contractEndDate),
                new System.Data.SqlClient.SqlParameter("@FinancialYear",objOff.Financialperiod),
                new System.Data.SqlClient.SqlParameter("@ProfileType", objOff.ProfileType),
                new System.Data.SqlClient.SqlParameter("@SalaryThroug",objOff.modeofpayment),
                new System.Data.SqlClient.SqlParameter("@BankAccno",objOff.BankACno),
                new System.Data.SqlClient.SqlParameter("@BankName",objOff.BankName),
                new System.Data.SqlClient.SqlParameter("@BranchName",objOff.Branchname),
                new System.Data.SqlClient.SqlParameter("@Elibiblepf", objOff.EligblePF),
                new System.Data.SqlClient.SqlParameter("@Ccode", objOff.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", objOff.Lcode),
                new System.Data.SqlClient.SqlParameter("@pf_type", pf_type),
                new System.Data.SqlClient.SqlParameter("@chkMonths", objOff.ChkMonths),
                new System.Data.SqlClient.SqlParameter("@chkYears", objOff.ChkYears),
                new System.Data.SqlClient.SqlParameter("Duration", objOff.DurationPeriod),
                new System.Data.SqlClient.SqlParameter("@ESICode", objOff.ESICode),
                new System.Data.SqlClient.SqlParameter("@PFDOJ", PFDOJ),
                new System.Data.SqlClient.SqlParameter("@ESIDOJ", ESIDOJ)
                   
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UpdateOfficialprofile_SP", officialprofiel);
        }
        public DataTable OfficalProfileempType(string empCategory)
        {
            System.Data.SqlClient.SqlParameter[] OfficalProfileempType = 
            {
                new System.Data.SqlClient.SqlParameter("@Category",empCategory)
            };
            DataTable dt = SQL.ExecuteDatatable("ddEmpType", OfficalProfileempType);
            return dt;
        }

        public DataTable EditOfficialProfileGridView(string staff, string Dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] EditOfficialProfileGridView =
            {
                new System.Data.SqlClient.SqlParameter("@Staff",staff),
                new System.Data.SqlClient.SqlParameter("@Dept", Dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("EditOfficialProfileFromEmpDetails_SP", EditOfficialProfileGridView);
            return dt;
        }

        public DataTable EditOfficialProfileGridView_TokenNoWise(string staff, string TokenNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] EditOfficialProfileGridView =
            {
                new System.Data.SqlClient.SqlParameter("@Staff",staff),
                new System.Data.SqlClient.SqlParameter("@TokenNo", TokenNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("EditOfficialProfileFromEmpDetails_TokenNoWise_SP", EditOfficialProfileGridView);
            return dt;
        }

        public DataTable EditOfficialProfileGridView_user(string staff, string Dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] EditOfficialProfileGridView =
            {
                new System.Data.SqlClient.SqlParameter("@Staff",staff),
                new System.Data.SqlClient.SqlParameter("@Dept", Dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("EditOfficialProfile_user_SP", EditOfficialProfileGridView);
            return dt;
        }
        public DataTable Contractbreak_EmployeeLoad(string department, string cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Contractbreak_EmployeeLoad = 
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@cate", cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Contract_EmployeeLoad", Contractbreak_EmployeeLoad);
            return dt;
        }
        public DataTable Contractbreak_EmployeeLoad_user(string department, string cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Contractbreak_EmployeeLoad = 
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@cate",cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Contract_EmployeeLoad_USER", Contractbreak_EmployeeLoad);
            return dt;
        }
        public DataTable Contractbreak_EmployeeSearch(string department, string empNo, string cate, string isadmin, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Contractbreak_EmployeeSearch = 
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@empNo",empNo),
                new System.Data.SqlClient.SqlParameter("@cate", cate),
                new System.Data.SqlClient.SqlParameter("@isadmin", isadmin),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Contractbreak_EmployeeSearch", Contractbreak_EmployeeSearch);
            return dt;
        }
        public string ContractExcessLeave(string empNo)
        {
            System.Data.SqlClient.SqlParameter[] ContractExcessLeave = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",empNo)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "ContractExcessLeave_load", ContractExcessLeave);
            return str;
        }
        public string ServerDate()
        {
            System.Data.SqlClient.SqlParameter[] ContractExcessLeave = 
            {
              
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "ServerDate_SP", ContractExcessLeave);
            return str;
        }
        public string ExistingNO_Verify(string ExistingNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] ExistingNO_Verify = 
            {
                new System.Data.SqlClient.SqlParameter("@ExistingNo", ExistingNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "ExistingNO_Verify", ExistingNO_Verify);
            return str;
        }
        public string OLDNO_Verify(string OLDNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] OLDNO_Verify = 
            {
                new System.Data.SqlClient.SqlParameter("@OLDNo", OLDNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "OLDNO_Verify", OLDNO_Verify);
            return str;
        }

        #region SalaryMasterExiusst

        //public void SalaryMaster_Insert(SalaryMasterClass objsalary)
        //{
        //    System.Data.SqlClient.SqlParameter[] SalaryMaster_Insert = 
        //    {
        //        new System.Data.SqlClient.SqlParameter("@EmpNo",objsalary.EmpNo),
        //        new System.Data.SqlClient.SqlParameter("@ProfileType",objsalary.ProfileType),
        //        new System.Data.SqlClient.SqlParameter("@BasicSalary", objsalary.Basic),
        //        new System.Data.SqlClient.SqlParameter("@HRAPer",objsalary.hraper),
        //        new System.Data.SqlClient.SqlParameter("@HRAamt",objsalary.hraAmt),
        //        new System.Data.SqlClient.SqlParameter("@ConveyancePer",objsalary.conveyanceper),
        //        new System.Data.SqlClient.SqlParameter("@ConveyanceAmt",objsalary.conveyanceamt),
        //        new System.Data.SqlClient.SqlParameter("@CCAper",objsalary.CCAper),
        //        new System.Data.SqlClient.SqlParameter("@CCAamt",objsalary.CCaamt),
        //        new System.Data.SqlClient.SqlParameter("@Medicalper", objsalary.medicalper),
        //        new System.Data.SqlClient.SqlParameter("@Medicalamt", objsalary.medicalamt),
        //        new System.Data.SqlClient.SqlParameter("@Allowance1per", objsalary.Allowance1per),
        //        new System.Data.SqlClient.SqlParameter("@Alllowance1amt", objsalary.Allowance1Amt),
        //        new System.Data.SqlClient.SqlParameter("@Allowance2per", objsalary.Allowance2per),
        //        new System.Data.SqlClient.SqlParameter("@Allowance2amt", objsalary.Allowance2Amt),
        //        new System.Data.SqlClient.SqlParameter("@Allowance3per", objsalary.Allowance3per),
        //        new System.Data.SqlClient.SqlParameter("@Allowance3amt", objsalary.Allowance3Amt),
        //        new System.Data.SqlClient.SqlParameter("@NetPay", objsalary.netPay),
        //        new System.Data.SqlClient.SqlParameter("@CreatedBy", objsalary.username)
        //    };
        //    SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalaryMaster_Insert", SalaryMaster_Insert);
        //}
        //public void SalaryMaster_Update(SalaryMasterClass objsalary)
        //{
        //    System.Data.SqlClient.SqlParameter[] SalaryMaster_Update = 
        //    {
        //        new System.Data.SqlClient.SqlParameter("@EmpNo",objsalary.EmpNo),
        //        new System.Data.SqlClient.SqlParameter("@ProfileType",objsalary.ProfileType),
        //        new System.Data.SqlClient.SqlParameter("@BasicSalary", objsalary.Basic),
        //        new System.Data.SqlClient.SqlParameter("@HRAPer",objsalary.hraper),
        //        new System.Data.SqlClient.SqlParameter("@HRAamt",objsalary.hraAmt),
        //        new System.Data.SqlClient.SqlParameter("@ConveyancePer",objsalary.conveyanceper),
        //        new System.Data.SqlClient.SqlParameter("@ConveyanceAmt",objsalary.conveyanceamt),
        //        new System.Data.SqlClient.SqlParameter("@CCAper",objsalary.CCAper),
        //        new System.Data.SqlClient.SqlParameter("@CCAamt",objsalary.CCaamt),
        //        new System.Data.SqlClient.SqlParameter("@Medicalper", objsalary.medicalper),
        //        new System.Data.SqlClient.SqlParameter("@Medicalamt", objsalary.medicalamt),
        //        new System.Data.SqlClient.SqlParameter("@Allowance1per", objsalary.Allowance1per),
        //        new System.Data.SqlClient.SqlParameter("@Alllowance1amt", objsalary.Allowance1Amt),
        //        new System.Data.SqlClient.SqlParameter("@Allowance2per", objsalary.Allowance2per),
        //        new System.Data.SqlClient.SqlParameter("@Allowance2amt", objsalary.Allowance2Amt),
        //        new System.Data.SqlClient.SqlParameter("@Allowance3per", objsalary.Allowance3per),
        //        new System.Data.SqlClient.SqlParameter("@Allowance3amt", objsalary.Allowance3Amt),
        //        new System.Data.SqlClient.SqlParameter("@NetPay", objsalary.netPay),
        //        new System.Data.SqlClient.SqlParameter("@CreatedBy", objsalary.username)
        //    };
        //    SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalaryMaster_Update", SalaryMaster_Update);
        //}
        #endregion




        public DataTable StaffBonusLoad(string empNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] StaffBonusLoadee = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", empNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("StaffBonusLoad", StaffBonusLoadee);
            return dt;
        }
        public DataTable LabourBonus_Load(string empNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] LabourBonus_Load = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", empNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("LabourBonus_Load", LabourBonus_Load);
            return dt;
        }

        //public DataTable StandardBonusLabour_Load(string empno, string Wages)
        //{
        //    System.Data.SqlClient.SqlParameter[] StandardBonusLabour_Load = 
        //    {
        //        new System.Data.SqlClient.SqlParameter("@Department", empno),
        //        new System.Data.SqlClient.SqlParameter("@Wages", Wages)
        //    };
        //    DataTable dt = SQL.ExecuteDatatable("StandardBonusLabour_Load", StandardBonusLabour_Load);
        //    return dt;
        //}
        #endregion

        #region BonusCalculation
        public string TotalWorkingDays(string empno, DateTime FromDate, DateTime Todate)
        {
            System.Data.SqlClient.SqlParameter[] Totalworkdingdays = 
            {
                   new System.Data.SqlClient.SqlParameter("@EmpNo",empno),
                   new System.Data.SqlClient.SqlParameter("@FromDate",FromDate),
                   new System.Data.SqlClient.SqlParameter("@ToDate",Todate)
              
              
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "BonusTotalWorkingDays_SP", Totalworkdingdays);
            return str;
        }
        public void InsertBonus(BonusInsertClass objBonusIns, DateTime fromdate, DateTime ToDate)
        {
            System.Data.SqlClient.SqlParameter[] BonusInsert = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpCode", objBonusIns.EmpNo),
                new System.Data.SqlClient.SqlParameter("@FromDate", fromdate),
                new System.Data.SqlClient.SqlParameter("@ToDate", ToDate),
                new System.Data.SqlClient.SqlParameter("@WorkingDays", objBonusIns.WorkingDays),
                new System.Data.SqlClient.SqlParameter("@Department", objBonusIns.Department),
                new System.Data.SqlClient.SqlParameter("@CalMode", objBonusIns.CalMode),
                new System.Data.SqlClient.SqlParameter("@BasBasicAmt", objBonusIns.BasBasicAmt),
                new System.Data.SqlClient.SqlParameter("@BasAllowAmt", objBonusIns.BasAllowAmt),
                new System.Data.SqlClient.SqlParameter("@BasbasicAllow", objBonusIns.BasbasicAllow),
                new System.Data.SqlClient.SqlParameter("@BasPer", objBonusIns.basPer),
                new System.Data.SqlClient.SqlParameter("@BasPerCal", objBonusIns.BasPerCal),
                new System.Data.SqlClient.SqlParameter("@BasDays", objBonusIns.BasDays),
                new System.Data.SqlClient.SqlParameter("@BasTotalBonus", objBonusIns.BasTotalBonus),
                new System.Data.SqlClient.SqlParameter("@PerBasicAmt", objBonusIns.PerBasicAmt),
                new System.Data.SqlClient.SqlParameter("@PerBasicpercent", objBonusIns.PerBasicpercent),
                new System.Data.SqlClient.SqlParameter("@PerTotalbasicAmt", objBonusIns.PerTotalbasicAmt),
                new System.Data.SqlClient.SqlParameter("@PerAllowance", objBonusIns.PerAllowance),
                new System.Data.SqlClient.SqlParameter("@PerAllowancePercent", objBonusIns.PerAllowancePercent),
                new System.Data.SqlClient.SqlParameter("@PerTotalAllow", objBonusIns.PerTotalAllow),
                new System.Data.SqlClient.SqlParameter("@PerBasicAllowance", objBonusIns.PerBasicAllowance),
                new System.Data.SqlClient.SqlParameter("@PerDays", objBonusIns.PerDays),
                new System.Data.SqlClient.SqlParameter("@PerTotalBonus", objBonusIns.PerTotalBonus),
                
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "BonusCal", BonusInsert);
        }
        #endregion

        #region AttendanceReports
        public DataTable RptAttendanceSingleEmpForMonth(string EmpNo, DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceSingleEmpMonth = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@FromDate",FromDate),
                new System.Data.SqlClient.SqlParameter("@ToDate",ToDate),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForSinglEmployeeMonth_SP", AttendanceSingleEmpMonth);
            return dt;
        }
        public DataTable RptAttendanceSingleEmpForMonth_user(string EmpNo, DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceSingleEmpMonth = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@FromDate",FromDate),
                new System.Data.SqlClient.SqlParameter("@ToDate",ToDate),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForSinglEmployeeMonth_SP_user", AttendanceSingleEmpMonth);
            return dt;
        }
        public DataTable RptAttendanceSingleEmpForYear(string EmpNo, string FinaYear, string Dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceSingleEmpYear = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)

            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForSinglEmployeeFinaYear_SP", AttendanceSingleEmpYear);
            return dt;
        }
        public DataTable RptAttendanceSingleEmpForYear_user(string EmpNo, string FinaYear, string Dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceSingleEmpYear = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForSinglEmployeeFinaYear_SP_user", AttendanceSingleEmpYear);
            return dt;
        }
        public DataTable RptAttendanceAllEmpForMonth(DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceAllEmpMonth = 
            {
                new System.Data.SqlClient.SqlParameter("@FromDate",FromDate),
                new System.Data.SqlClient.SqlParameter("@ToDate",ToDate),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForAllEmployeeMonth_SP", AttendanceAllEmpMonth);
            return dt;
        }
        public DataTable RptAttendanceAllEmpForMonth_user(DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceAllEmpMonth = 
            {
                new System.Data.SqlClient.SqlParameter("@FromDate",FromDate),
                new System.Data.SqlClient.SqlParameter("@ToDate",ToDate),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForAllEmployeeMonth_user", AttendanceAllEmpMonth);
            return dt;
        }
        public DataTable RptAttendanceAllEmpForYear(string FinaYear, string Dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceAllEmpMonth = 
            {
                
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForAllEmployeeFinaYear_SP", AttendanceAllEmpMonth);
            return dt;
        }
        public DataTable RptAttendanceAllEmpForYear_user(string FinaYear, string Dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceAllEmpMonth = 
            {
                
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForAllEmployeeFinaYear_user", AttendanceAllEmpMonth);
            return dt;
        }
        public DataTable RptAttendanceSingleLabourForMonth(string EmpNo, DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceSingleEmpMonth = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@FromDate",FromDate),
                new System.Data.SqlClient.SqlParameter("@ToDate",ToDate),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Cate",Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForSinglLabourMonth_SP", AttendanceSingleEmpMonth);
            return dt;
        }
        public DataTable RptAttendanceSingleLabourForMonth_user(string EmpNo, DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceSingleEmpMonth = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@FromDate",FromDate),
                new System.Data.SqlClient.SqlParameter("@ToDate",ToDate),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Cate",Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForSinglLabourMonth_SP_user", AttendanceSingleEmpMonth);
            return dt;
        }
        public DataTable RptAttendanceSingleLabourForYear(string EmpNo, string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceSingleEmpMonth = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Cate",Cate)
            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForSinglLabourYear_SP", AttendanceSingleEmpMonth);
            return dt;
        }
        public DataTable RptAttendanceSingleLabourForYear_user(string EmpNo, string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceSingleEmpMonth = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Cate",Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForSinglLabourYear_user", AttendanceSingleEmpMonth);
            return dt;
        }
        public DataTable RptAttendanceAllLabourForMonth(DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceSingleEmpMonth = 
            {
                new System.Data.SqlClient.SqlParameter("@FromDate",FromDate),
                new System.Data.SqlClient.SqlParameter("@ToDate",ToDate),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Cate",Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForAllLabourMonth_SP", AttendanceSingleEmpMonth);
            return dt;
        }
        public DataTable RptAttendanceAllLabourForMonth_user(DateTime FromDate, DateTime ToDate, string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceSingleEmpMonth = 
            {
                new System.Data.SqlClient.SqlParameter("@FromDate",FromDate),
                new System.Data.SqlClient.SqlParameter("@ToDate",ToDate),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Cate",Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForAllLabourMonth_USER", AttendanceSingleEmpMonth);
            return dt;
        }
        public DataTable RptAttendanceAllLabourForYear(string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceSingleEmpMonth = 
            {
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Cate",Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForAllLabourYear_SP", AttendanceSingleEmpMonth);
            return dt;
        }
        public DataTable RptAttendanceAllLabourForYear_user(string FinaYear, string Dept, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] AttendanceSingleEmpMonth = 
            {
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@Dept",Dept),
                new System.Data.SqlClient.SqlParameter("@Cate",Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptAttendanceForAllLabourYear_user", AttendanceSingleEmpMonth);
            return dt;
        }
        public DataTable DepartmentwiseEmployee(string department, string gender, string IsAdmin, string Cate, string Ccode, string Lcode, string pfgrade)
        {
            System.Data.SqlClient.SqlParameter[] DepartmentwiseEmployee =
            {
                new System.Data.SqlClient.SqlParameter("@Department",department),
                new System.Data.SqlClient.SqlParameter("@Gender",gender),
                new System.Data.SqlClient.SqlParameter("@IsAdmin",IsAdmin),
                new System.Data.SqlClient.SqlParameter("@Cate",Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@pfgrade", pfgrade)
            };
            DataTable dt = SQL.ExecuteDatatable("Departmentwiseemp_SP", DepartmentwiseEmployee);
            return dt;
        }
        public DataTable DepartmentwiseEmployee_user(string department, string gender, string Cate)
        {
            System.Data.SqlClient.SqlParameter[] DepartmentwiseEmployee =
            {
                new System.Data.SqlClient.SqlParameter("@Department",department),
                new System.Data.SqlClient.SqlParameter("@Gender",gender),
                new System.Data.SqlClient.SqlParameter("@Cate",Cate)
            };
            DataTable dt = SQL.ExecuteDatatable("Departmentwiseemp_user", DepartmentwiseEmployee);
            return dt;
        }
        public DataTable Departmentwise(string department, string empNo)
        {
            System.Data.SqlClient.SqlParameter[] Departmentwise =
            {
                new System.Data.SqlClient.SqlParameter("@Department",department),
                new System.Data.SqlClient.SqlParameter("@empNo",empNo)
            };
            DataTable dt = SQL.ExecuteDatatable("Departmentwisesearch_SP", Departmentwise);
            return dt;
        }

        public DataTable Probationperiod(string department, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Probationperiod =
            {
                new System.Data.SqlClient.SqlParameter("@Department",department),
                new System.Data.SqlClient.SqlParameter("@Cate",Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Probationperiod_SP", Probationperiod);
            return dt;
        }
        public DataTable Probationperiod_user(string department, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Probationperiod =
            {
                new System.Data.SqlClient.SqlParameter("@Department",department),
                new System.Data.SqlClient.SqlParameter("@Cate",Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Probationperiod_user", Probationperiod);
            return dt;
        }
        public DataTable Probationperiodsearch(string department, string empNo, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Probationperiodsearch =
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@empNo",empNo),
                 new System.Data.SqlClient.SqlParameter("@Cate",Cate),
                 new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
        };
            DataTable dt = SQL.ExecuteDatatable("Probationperiodsearch_SP", Probationperiodsearch);
            return dt;
        }
        public DataTable Probationperiodsearch_user(string department, string empNo, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Probationperiodsearch =
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@empNo",empNo),
                 new System.Data.SqlClient.SqlParameter("@Cate",Cate),
                 new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
        };
            DataTable dt = SQL.ExecuteDatatable("Probationperiodsearch_user", Probationperiodsearch);
            return dt;
        }
        #endregion
        #region StandardBonusForAll
        public DataTable StandardBonusEmpLoad(string department, string staffLabour, string finance, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] StandardBonusEmpLoadEmp = 
            {
                new System.Data.SqlClient.SqlParameter("@department", department),
                new System.Data.SqlClient.SqlParameter("@staffLabour", staffLabour),
                new System.Data.SqlClient.SqlParameter("@finance", finance),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("StandardBonusEmpLoad", StandardBonusEmpLoadEmp);
            return dt;
        }

        public DataTable StandardBonusConfirmEmpLoad(string department, string staffLabour, string finance, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] StandardBonusConLoadEmp = 
            {
                new System.Data.SqlClient.SqlParameter("@department", department),
                new System.Data.SqlClient.SqlParameter("@staffLabour", staffLabour),
                new System.Data.SqlClient.SqlParameter("@finance", finance),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("StandardBonusConfirmEmpLoad", StandardBonusConLoadEmp);
            return dt;
        }
        public DataTable StandardBonusProbationEmpLoad(string department, string staffLabour, string financial, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] StandardBonusprLoadEmp = 
            {
                new System.Data.SqlClient.SqlParameter("@department", department),
                new System.Data.SqlClient.SqlParameter("@staffLabour", staffLabour),
                new System.Data.SqlClient.SqlParameter("@Financial", financial),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("StandardBonusProbationEmpLoad", StandardBonusprLoadEmp);
            return dt;
        }
        public DataTable StandardBonuLabour(string department, string staffLabour, string finance, string day, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] StandardBonusLoadlabour = 
            {
                new System.Data.SqlClient.SqlParameter("@department", department),
                new System.Data.SqlClient.SqlParameter("@staffLabour", staffLabour),
                new System.Data.SqlClient.SqlParameter("@finance", finance),
                new System.Data.SqlClient.SqlParameter("@wage", day),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("StandardBonuLabour", StandardBonusLoadlabour);
            return dt;
        }



        public void BonusforallInsert(BonusforAllclass objbonus)
        {
            System.Data.SqlClient.SqlParameter[] BonusforallInsert = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpCode", objbonus.EmpCode),
                new System.Data.SqlClient.SqlParameter("@Department", objbonus.Department),
                new System.Data.SqlClient.SqlParameter("@Category", objbonus.Category),
                new System.Data.SqlClient.SqlParameter("@BonusCalculation", objbonus.BonusCalculation),
                new System.Data.SqlClient.SqlParameter("@Basic", objbonus.Basic),
                new System.Data.SqlClient.SqlParameter("@HRA",objbonus.HRA),
                new System.Data.SqlClient.SqlParameter("@Allowance1", objbonus.Allowance1),
                new System.Data.SqlClient.SqlParameter("@Allowance2", objbonus.Allowance2),
                new System.Data.SqlClient.SqlParameter("@Allowance3", objbonus.Allowance3),
                new System.Data.SqlClient.SqlParameter("@Allowance4", objbonus.Allowance4),
                new System.Data.SqlClient.SqlParameter("@CalculationAmt", objbonus.CalculationAmt),
                new System.Data.SqlClient.SqlParameter("@Precentage", objbonus.Percentage),
                new System.Data.SqlClient.SqlParameter("@PrecentageAmt", objbonus.PercentageAmt),
                new System.Data.SqlClient.SqlParameter("@Noofmonths", objbonus.Noofmonths),
                new System.Data.SqlClient.SqlParameter("@BonusAmt", objbonus.BonusAmt),
                new System.Data.SqlClient.SqlParameter("@CreatedBy", objbonus.CreatedBy),
                new System.Data.SqlClient.SqlParameter("@DailyWages", objbonus.DailyWages),
                new System.Data.SqlClient.SqlParameter("@NoofDays", objbonus.NoofDays),
                new System.Data.SqlClient.SqlParameter("@LabourBonusAmount", objbonus.LabourBonusAmount),
                new System.Data.SqlClient.SqlParameter("@WagwsType", objbonus.rbwages),
                new System.Data.SqlClient.SqlParameter("@ConsolidatedSalary", objbonus.consolidated),
                new System.Data.SqlClient.SqlParameter("@deduction1", objbonus.deduction1),
                new System.Data.SqlClient.SqlParameter("@deduction2", objbonus.deduction2),
                new System.Data.SqlClient.SqlParameter("@deduction3", objbonus.deduction3),
                new System.Data.SqlClient.SqlParameter("@deductioncal", objbonus.deductioncal),
                new System.Data.SqlClient.SqlParameter("@lbrpercentage", objbonus.lbrpercentage),
                new System.Data.SqlClient.SqlParameter("@lbrpercentageAmt", objbonus.lbrpercentageAmt),
                new System.Data.SqlClient.SqlParameter("@lbrMonths", objbonus.lbrMonths),
                new System.Data.SqlClient.SqlParameter("@lbrbonus", objbonus.lbrbonus),
                new System.Data.SqlClient.SqlParameter("@FinancialPeriod", objbonus.Finance),
                new System.Data.SqlClient.SqlParameter("@DeductionChkVal", objbonus.DeductionChkVal),
                new System.Data.SqlClient.SqlParameter("@BonusType", objbonus.BonusType),
                new System.Data.SqlClient.SqlParameter("@MessDed", objbonus.messded),
                new System.Data.SqlClient.SqlParameter("@Hostelded", objbonus.Hostel),
                new System.Data.SqlClient.SqlParameter("@Ccode", objbonus.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", objbonus.Lcode),
                new System.Data.SqlClient.SqlParameter("@FDA", objbonus.FDA),
                new System.Data.SqlClient.SqlParameter("@VDA",objbonus.VDA),
                new System.Data.SqlClient.SqlParameter("@Attenance", objbonus.Attenance)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "BonusforAll_Insert", BonusforallInsert);
        }

        public DataTable StandardBonuswages_Load(string department, string Wages)
        {
            System.Data.SqlClient.SqlParameter[] StandardBonuswages_Load = 
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@Wages", Wages)
            };
            DataTable dt = SQL.ExecuteDatatable("StandardBonuswages_Load", StandardBonuswages_Load);
            return dt;
        }

        public DataTable StandardBonusLabour_Load(string department, string Wages)
        {
            System.Data.SqlClient.SqlParameter[] StandardBonusLabour_Load = 
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@Wages", Wages)
            };
            DataTable dt = SQL.ExecuteDatatable("StandardBonusLabour_Load", StandardBonusLabour_Load);
            return dt;
        }

        public string BonusFinancialYear(string EmpNo, string Finance, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] BonusFinancialYear = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Financial", Finance),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "BonusFinancialYear", BonusFinancialYear);
            return str;
        }

        #endregion
        #region SalaryMasters
        public DataTable BonusEmployeeID(string stafflabour, string department)
        {
            System.Data.SqlClient.SqlParameter[] BonusEmployeeID = 
            {
                new System.Data.SqlClient.SqlParameter("@stafflabour", stafflabour),
                new System.Data.SqlClient.SqlParameter("@Department", department)
            };
            DataTable dt = SQL.ExecuteDatatable("BonusCal_employeeLoad", BonusEmployeeID);
            return dt;
        }
        public DataTable EmployeeLoadSalaryMaster(string stafflabour, string department, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterEmpLoad = 
            {
                new System.Data.SqlClient.SqlParameter("@stafflabour", stafflabour),
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeLoadForSalaryMaster_SP", SalaryMasterEmpLoad);
            return dt;
        }
        public DataTable EmployeeLoadSalaryMasterAllDept(string stafflabour, string Ccode, string Lcode, string TokenNo)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterEmpLoad = 
            {
                new System.Data.SqlClient.SqlParameter("@stafflabour", stafflabour),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@TokenNo", TokenNo)
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeLoadForSalaryMasterAllDept_SP", SalaryMasterEmpLoad);
            return dt;
        }
        public DataTable EmployeeLoadSalaryMaster_user(string stafflabour, string department, string ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterEmpLoad = 
            {
                new System.Data.SqlClient.SqlParameter("@stafflabour", stafflabour),
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@Ccode", ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeLoadForSalaryMaster_USER_SP", SalaryMasterEmpLoad);
            return dt;
        }
        public DataTable EmployeeNameandNumber_SalaryDetails(string stafflabour, string department, string ccode, string Lcode, string Wages)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterEmpLoad = 
            {
                new System.Data.SqlClient.SqlParameter("@Staff", stafflabour),
                new System.Data.SqlClient.SqlParameter("@Dept", department),
                new System.Data.SqlClient.SqlParameter("@Ccode", ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Wages", Wages)
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeNameandNumber_SalaryDetails", SalaryMasterEmpLoad);
            return dt;
        }
        public DataTable BonusEmployeeName(string EmpID, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] BonusEmployeeName = 
            {
                new System.Data.SqlClient.SqlParameter("@empCode", EmpID),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
               
            };
            DataTable dt = SQL.ExecuteDatatable("empNameLoad_bonus", BonusEmployeeName);
            return dt;
        }
        public DataTable SalaryMasterProfile(string EmpCode, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterProfile = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpCode", EmpCode),
                new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("SalaryMaster_ProfileType", SalaryMasterProfile);
            return dt;
        }

        public string SalaryMasterVerify(string empCode, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterVerify = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpCode", empCode),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Salarymaster_Verify", SalaryMasterVerify);
            return str;
        }

        public DataTable SalaryMasterEmpLoad(string empCode, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterEmpLoad = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpCode", empCode),
                new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("SalaryMasterEmpLoad", SalaryMasterEmpLoad);
            return dt;
        }
        public DataTable SalaryMasterEmpLoad_ForLabour(string empCode, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterEmpLoad = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", empCode),
                new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("SalaryMasterEmpLoad_ForLabour_SP", SalaryMasterEmpLoad);
            return dt;
        }
        public DataTable SalaryMasterEmpLoad_Labour(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterEmpLoad = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("SalaryMasterEmpLoad_ForLabour_SP", SalaryMasterEmpLoad);
            return dt;
        }
        public void SalaryMaster_Update(SalaryMasterClass objsalary)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMaster_Update = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",objsalary.EmpNo),
                new System.Data.SqlClient.SqlParameter("@ProfileType",objsalary.ProfileType),
                new System.Data.SqlClient.SqlParameter("@BasicSalary", objsalary.Basic),
                new System.Data.SqlClient.SqlParameter("@HRAPer",objsalary.hraper),
                new System.Data.SqlClient.SqlParameter("@HRAamt",objsalary.hraAmt),
                new System.Data.SqlClient.SqlParameter("@ConveyancePer",objsalary.conveyanceper),
                new System.Data.SqlClient.SqlParameter("@ConveyanceAmt",objsalary.conveyanceamt),
                new System.Data.SqlClient.SqlParameter("@CCAper",objsalary.CCAper),
                new System.Data.SqlClient.SqlParameter("@CCAamt",objsalary.CCaamt),
                new System.Data.SqlClient.SqlParameter("@Medicalper", objsalary.medicalper),
                new System.Data.SqlClient.SqlParameter("@Medicalamt", objsalary.medicalamt),
                new System.Data.SqlClient.SqlParameter("@Allowance1per", objsalary.Allowance1per),
                new System.Data.SqlClient.SqlParameter("@Alllowance1amt", objsalary.Allowance1Amt),
                new System.Data.SqlClient.SqlParameter("@Allowance2per", objsalary.Allowance2per),
                new System.Data.SqlClient.SqlParameter("@Allowance2amt", objsalary.Allowance2Amt),
                new System.Data.SqlClient.SqlParameter("@Allowance3per", objsalary.Allowance3per),
                new System.Data.SqlClient.SqlParameter("@Allowance3amt", objsalary.Allowance3Amt),
                new System.Data.SqlClient.SqlParameter("@Deduction1Amt", objsalary.StaffDecution1),
                new System.Data.SqlClient.SqlParameter("@Deduction1Per", objsalary.StaffDeduction1Per),
                new System.Data.SqlClient.SqlParameter("@Deduction2Amt", objsalary.StaffDecution2),
                new System.Data.SqlClient.SqlParameter("@Deduction2Per", objsalary.StaffDeduction2Per),
                new System.Data.SqlClient.SqlParameter("@Deduction3Amt", objsalary.StaffDecution3),
                new System.Data.SqlClient.SqlParameter("@Deduction3Per", objsalary.StaffDeduction3Per),
                new System.Data.SqlClient.SqlParameter("@NetPay", objsalary.netPay),
                new System.Data.SqlClient.SqlParameter("@CreatedBy", objsalary.username),
                new System.Data.SqlClient.SqlParameter("@DailySalary", objsalary.dailySalary),
                new System.Data.SqlClient.SqlParameter("@Finance", objsalary.Finance),
                new System.Data.SqlClient.SqlParameter("@FoodType", objsalary.FoodType),
                new System.Data.SqlClient.SqlParameter("@Messdeduction", objsalary.mess),
                new System.Data.SqlClient.SqlParameter("@hosteldeduction", objsalary.Hostel),
                new System.Data.SqlClient.SqlParameter("@Deduction1", objsalary.deduction1),
                new System.Data.SqlClient.SqlParameter("@Deduction2", objsalary.deduction2),
                new System.Data.SqlClient.SqlParameter("@Deduction3", objsalary.deduction3),
                new System.Data.SqlClient.SqlParameter("@LabourAllowance1", objsalary.LabourAllowance1),
                new System.Data.SqlClient.SqlParameter("@LabourAllowance2", objsalary.LabourAllowance2),
                new System.Data.SqlClient.SqlParameter("@LabourAllowance3", objsalary.LabourAllowance3),
                new System.Data.SqlClient.SqlParameter("@Ccode", objsalary.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", objsalary.Lcode),
                new System.Data.SqlClient.SqlParameter("@Base", objsalary.Base),
                 new System.Data.SqlClient.SqlParameter("@FDA", objsalary.FDA),
                 new System.Data.SqlClient.SqlParameter("@VDA", objsalary.VDA),
                 new System.Data.SqlClient.SqlParameter("@HRA", objsalary.HRA),
                 new System.Data.SqlClient.SqlParameter("@Total", objsalary.total),
                 new System.Data.SqlClient.SqlParameter("@Union", objsalary.union),
                 new System.Data.SqlClient.SqlParameter("@PFS", objsalary.PFsalary)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalaryMaster_Update", SalaryMaster_Update);
        }
        public void SalaryMaster_Insert(SalaryMasterClass objsalary)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMaster_Insert = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",objsalary.EmpNo),
                new System.Data.SqlClient.SqlParameter("@ProfileType",objsalary.ProfileType),
                new System.Data.SqlClient.SqlParameter("@BasicSalary", objsalary.Basic),
                new System.Data.SqlClient.SqlParameter("@HRAPer",objsalary.hraper),
                new System.Data.SqlClient.SqlParameter("@HRAamt",objsalary.hraAmt),
                new System.Data.SqlClient.SqlParameter("@ConveyancePer",objsalary.conveyanceper),
                new System.Data.SqlClient.SqlParameter("@ConveyanceAmt",objsalary.conveyanceamt),
                new System.Data.SqlClient.SqlParameter("@CCAper",objsalary.CCAper),
                new System.Data.SqlClient.SqlParameter("@CCAamt",objsalary.CCaamt),
                new System.Data.SqlClient.SqlParameter("@Medicalper", objsalary.medicalper),
                new System.Data.SqlClient.SqlParameter("@Medicalamt", objsalary.medicalamt),
                new System.Data.SqlClient.SqlParameter("@Allowance1per", objsalary.Allowance1per),
                new System.Data.SqlClient.SqlParameter("@Alllowance1amt", objsalary.Allowance1Amt),
                new System.Data.SqlClient.SqlParameter("@Allowance2per", objsalary.Allowance2per),
                new System.Data.SqlClient.SqlParameter("@Allowance2amt", objsalary.Allowance2Amt),
                new System.Data.SqlClient.SqlParameter("@Allowance3per", objsalary.Allowance3per),
                new System.Data.SqlClient.SqlParameter("@Allowance3amt", objsalary.Allowance3Amt),

                new System.Data.SqlClient.SqlParameter("@Deduction1Amt", objsalary.StaffDecution1),
                new System.Data.SqlClient.SqlParameter("@Deduction1Per", objsalary.StaffDeduction1Per),
                new System.Data.SqlClient.SqlParameter("@Deduction2Amt", objsalary.StaffDecution2),
                new System.Data.SqlClient.SqlParameter("@Deduction2Per", objsalary.StaffDeduction2Per),
                new System.Data.SqlClient.SqlParameter("@Deduction3Amt", objsalary.StaffDecution3),
                new System.Data.SqlClient.SqlParameter("@Deduction3Per", objsalary.StaffDeduction3Per),
                new System.Data.SqlClient.SqlParameter("@NetPay", objsalary.netPay),
                new System.Data.SqlClient.SqlParameter("@CreatedBy", objsalary.username),
                new System.Data.SqlClient.SqlParameter("@DailySalary", objsalary.dailySalary),
                new System.Data.SqlClient.SqlParameter("@Finance", objsalary.Finance),
                new System.Data.SqlClient.SqlParameter("@FoodType", objsalary.FoodType),
                new System.Data.SqlClient.SqlParameter("@Messdeduction", objsalary.mess),
                new System.Data.SqlClient.SqlParameter("@hosteldeduction", objsalary.Hostel),
                new System.Data.SqlClient.SqlParameter("@Deduction1", objsalary.deduction1),
                new System.Data.SqlClient.SqlParameter("@Deduction2", objsalary.deduction2),
                new System.Data.SqlClient.SqlParameter("@Deduction3", objsalary.deduction3),
                new System.Data.SqlClient.SqlParameter("@LabourAllowance1", objsalary.LabourAllowance1),
                new System.Data.SqlClient.SqlParameter("@LabourAllowance2", objsalary.LabourAllowance2),
                 new System.Data.SqlClient.SqlParameter("@LabourAllowance3", objsalary.LabourAllowance3),
                 new System.Data.SqlClient.SqlParameter("@Ccode", objsalary.Ccode),
                 new System.Data.SqlClient.SqlParameter("@Lcode", objsalary.Lcode),
                 new System.Data.SqlClient.SqlParameter("@Base", objsalary.Base),
                 new System.Data.SqlClient.SqlParameter("@FDA", objsalary.FDA),
                 new System.Data.SqlClient.SqlParameter("@VDA", objsalary.VDA),
                 new System.Data.SqlClient.SqlParameter("@HRA", objsalary.HRA),
                 new System.Data.SqlClient.SqlParameter("@Total", objsalary.total),
                 new System.Data.SqlClient.SqlParameter("@Union", objsalary.union),
                 new System.Data.SqlClient.SqlParameter("@PFS", objsalary.PFsalary)

            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalaryMaster_Insert", SalaryMaster_Insert);
        }

        public DataTable SalaryMasterLabour_Load(string department, string wages, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterLabour_Load = 
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@Wages", wages),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("SalaryMasterLabour_Load", SalaryMasterLabour_Load);
            return dt;
        }
        public DataTable SalaryMasterLabour_ForSearchingExisiting(string department, string ExisistingNo, string Cate, string Ccode, string Lcode, string Admin)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterLabour_Load = 
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@ExisitingNo", ExisistingNo),
                new System.Data.SqlClient.SqlParameter("@Cate", Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Admin", Admin)
            };
            DataTable dt = SQL.ExecuteDatatable("SalaryMasterSearchigForLabour_SP", SalaryMasterLabour_Load);
            return dt;
        }
        public DataTable SalaryMasterLabour_ForSearchingExisiting_New(string department, string ExisistingNo, string Cate, string Ccode, string Lcode, string Admin, string Wages)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterLabour_Load = 
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@ExisitingNo", ExisistingNo),
                new System.Data.SqlClient.SqlParameter("@Cate", Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Admin", Admin),
                new System.Data.SqlClient.SqlParameter("@Wages", Wages)
            };
            DataTable dt = SQL.ExecuteDatatable("SalaryMasterLabour_ForSearchingExisiting_New", SalaryMasterLabour_Load);
            return dt;
        }
        public DataTable SalaryMasterLabour_ForSearchingExisiting_user(string department, string ExisistingNo, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterLabour_Load = 
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@ExisitingNo", ExisistingNo),
                new System.Data.SqlClient.SqlParameter("@Cate", Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("SalaryMasterSearchigForLabour_user_SP", SalaryMasterLabour_Load);
            return dt;
        }
        #endregion
        public void loaninsert(Bankloanclass objBankloan)
        {
            System.Data.SqlClient.SqlParameter[] loaninsert =
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",objBankloan.EmpNo),
                new System.Data.SqlClient.SqlParameter("@Empname",objBankloan.Empname),
                new System.Data.SqlClient.SqlParameter("@Department",objBankloan.Department),
                new System.Data.SqlClient.SqlParameter("@StafforLaabour",objBankloan.StafforLabour),
                new System.Data.SqlClient.SqlParameter("@Designation",objBankloan.Designation),
                new System.Data.SqlClient.SqlParameter("@Dateofjoining",objBankloan.Dateofjoining),
                new System.Data.SqlClient.SqlParameter("@Yearofexperience",objBankloan.Yearofexperience),
                new System.Data.SqlClient.SqlParameter("@Basicsalary",objBankloan.Basicsalary),
                new System.Data.SqlClient.SqlParameter("@Totalloanamount",objBankloan.Totalloanamount),
                new System.Data.SqlClient.SqlParameter("@Alreadytakenloan",objBankloan.Alreadytakenloan),
                new System.Data.SqlClient.SqlParameter("@FinancialYear",objBankloan.Financialyear),
                new System.Data.SqlClient.SqlParameter("@Durationfrommonth",objBankloan.Fromdate),
                new System.Data.SqlClient.SqlParameter("@Durationtomonth",objBankloan.Todate),
                new System.Data.SqlClient.SqlParameter("@Deduction",objBankloan.Deduction)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Bankloaninsert_SP", loaninsert);
        }
        public void BankLoanInsertRecord(Bankloanclass objBankloan)
        {
            System.Data.SqlClient.SqlParameter[] loaninsert =
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",objBankloan.EmpNo),
                new System.Data.SqlClient.SqlParameter("@Empname",objBankloan.Empname),
                new System.Data.SqlClient.SqlParameter("@Department",objBankloan.Department),
                new System.Data.SqlClient.SqlParameter("@StafforLaabour",objBankloan.StafforLabour),
                new System.Data.SqlClient.SqlParameter("@Designation",objBankloan.Designation),
                new System.Data.SqlClient.SqlParameter("@Dateofjoining",objBankloan.Dateofjoining),
                new System.Data.SqlClient.SqlParameter("@Yearofexperience",objBankloan.Yearofexperience),
                new System.Data.SqlClient.SqlParameter("@Basicsalary",objBankloan.Basicsalary),
                new System.Data.SqlClient.SqlParameter("@Totalloanamount",objBankloan.Totalloanamount),
                new System.Data.SqlClient.SqlParameter("@Alreadytakenloan",objBankloan.Alreadytakenloan),
                new System.Data.SqlClient.SqlParameter("@FinancialYear",objBankloan.Financialyear),
                new System.Data.SqlClient.SqlParameter("@Durationfrommonth",objBankloan.Fromdate),
                new System.Data.SqlClient.SqlParameter("@Durationtomonth",objBankloan.Todate),
                new System.Data.SqlClient.SqlParameter("@Deduction",objBankloan.Deduction)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Bankloaninsert_SP", loaninsert);
        }
        public DataTable Bankloan(string EmpNo, string Department)
        {
            System.Data.SqlClient.SqlParameter[] Bankloan =
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@Department",Department)
            };
            DataTable dt = SQL.ExecuteDatatable("Bankloan_SP", Bankloan);
            return dt;
        }
        public DataTable Empdetails(string EmpNo)
        {
            System.Data.SqlClient.SqlParameter[] Empdetails_loan =
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo)
            };
            DataTable dt = SQL.ExecuteDatatable("Bankloandetails_SP", Empdetails_loan);
            return dt;
        }

        public string RegularLaboure(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] RegularLabour = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "RegularLabour_SP", RegularLabour);
            return str;
        }
        public string MessDeductionType(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] RegularLabour = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "MessDecutionType_SP", RegularLabour);
            return str;
        }
        public DataTable StandardBonuLabourconform(string department, string staffLabour, string finance, string day, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] StandardBonusLoadlabour = 
            {
                new System.Data.SqlClient.SqlParameter("@department", department),
                new System.Data.SqlClient.SqlParameter("@staffLabour", staffLabour),
                new System.Data.SqlClient.SqlParameter("@finance", finance),
                new System.Data.SqlClient.SqlParameter("@wage", day),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("StandardBonuLabourconform", StandardBonusLoadlabour);
            return dt;
        }
        public DataTable StandardBonuLabourProbation(string department, string staffLabour, string day, string finance, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] StandardBonusLoadlabour = 
            {
                new System.Data.SqlClient.SqlParameter("@department", department),
                new System.Data.SqlClient.SqlParameter("@staffLabour", staffLabour),
                new System.Data.SqlClient.SqlParameter("@wage", day),
                new System.Data.SqlClient.SqlParameter("@finance", finance), 
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("StandardBonuLabourProbation", StandardBonusLoadlabour);
            return dt;
        }
        public string SearchEmployee_save_SP(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] RegularLabour = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SearchEmployee_save_SP", RegularLabour);
            return str;
        }
        public string labourWges(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] wages = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Wages_SP", wages);
            return str;
        }
        public DataTable AlreadyLeaveDateLoad(string EmpNo, string Financialyear, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Leave_load = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@FinaYear", Financialyear),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("alreadyleaveDate_Load_SP", Leave_load);
            return dt;
        }
        public DataTable BonusRetrive(string EmpNo, string Category, string FinancialYear, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Bonus_retrive = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Category", Category),
                new System.Data.SqlClient.SqlParameter("@FinYear", FinancialYear),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("BONUSINDIVIDUAL_RETRIVE", Bonus_retrive);
            return dt;
        }
        public DataTable LoadEmployeeForBonus(string stafflabour, string department, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterEmpLoad = 
            {
                new System.Data.SqlClient.SqlParameter("@stafflabour", stafflabour),
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("EmpLoad_Bonus_SP1", SalaryMasterEmpLoad);
            return dt;
        }
        public DataTable LoadEmployeeForBonus_Salary_History(string stafflabour, string department, string Ccode, string Lcode, string ActiveMode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterEmpLoad = 
            {
                new System.Data.SqlClient.SqlParameter("@stafflabour", stafflabour),
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@ActiveMode", ActiveMode)
            };
            DataTable dt = SQL.ExecuteDatatable("EmpLoad_SalaryHistory", SalaryMasterEmpLoad);
            return dt;
        }
        public DataTable LoadEmployeeForBonus_user(string stafflabour, string department)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterEmpLoad = 
            {
                new System.Data.SqlClient.SqlParameter("@stafflabour", stafflabour),
                new System.Data.SqlClient.SqlParameter("@Department", department)
            };
            DataTable dt = SQL.ExecuteDatatable("EmpLoad_Bonus_SP", SalaryMasterEmpLoad);
            return dt;
        }
        public DataTable EmpLoadFORBonus_USER(string stafflabour, string department, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterEmpLoad = 
            {
                new System.Data.SqlClient.SqlParameter("@stafflabour", stafflabour),
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("EmpLoadFORBonus_USER_SP", SalaryMasterEmpLoad);
            return dt;
        }
        public DataTable EmpLoadFORBonus_USER_SalaryHistory(string stafflabour, string department, string Ccode, string Lcode, string ActiveMode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterEmpLoad = 
            {
                new System.Data.SqlClient.SqlParameter("@stafflabour", stafflabour),
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@ActiveMode", ActiveMode)
            };
            DataTable dt = SQL.ExecuteDatatable("EmpLoad_USER_SalaryHistory", SalaryMasterEmpLoad);
            return dt;
        }
        public DataTable ExistNoSearchfor_bonus(string department, string stafflabour, string existno, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] exist_search = 
            {
                new System.Data.SqlClient.SqlParameter("@dept", department),
                new System.Data.SqlClient.SqlParameter("@staff", stafflabour),
                new System.Data.SqlClient.SqlParameter("@existNo", existno),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("BonusExisting_Search_SP", exist_search);
            return dt;
        }
        public DataTable ExistNoSearchfor_bonus_USER(string department, string stafflabour, string existno, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] exist_search = 
            {
                new System.Data.SqlClient.SqlParameter("@dept", department),
                new System.Data.SqlClient.SqlParameter("@staff", stafflabour),
                new System.Data.SqlClient.SqlParameter("@existNo", existno),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("BonusExisting_Search_uSER_SP", exist_search);
            return dt;
        }
        public void Bonus_Update(BonusforAllclass objbonus, string EmpNo)
        {
            System.Data.SqlClient.SqlParameter[] Bonus_update = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpCode", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Department", objbonus.Department),
                new System.Data.SqlClient.SqlParameter("@Category", objbonus.Category),
                new System.Data.SqlClient.SqlParameter("@BonusCalculation", objbonus.BonusCalculation),
                new System.Data.SqlClient.SqlParameter("@Basic", objbonus.Basic),
                new System.Data.SqlClient.SqlParameter("@HRA",objbonus.HRA),
                new System.Data.SqlClient.SqlParameter("@Allowance1", objbonus.Allowance1),
                new System.Data.SqlClient.SqlParameter("@Allowance2", objbonus.Allowance2),
                new System.Data.SqlClient.SqlParameter("@Allowance3", objbonus.Allowance3),
                new System.Data.SqlClient.SqlParameter("@Allowance4", objbonus.Allowance4),
                new System.Data.SqlClient.SqlParameter("@CalculationAmt", objbonus.CalculationAmt),
                new System.Data.SqlClient.SqlParameter("@Precentage", objbonus.Percentage),
                new System.Data.SqlClient.SqlParameter("@PrecentageAmt", objbonus.PercentageAmt),
                new System.Data.SqlClient.SqlParameter("@Noofmonths", objbonus.Noofmonths),
                new System.Data.SqlClient.SqlParameter("@BonusAmt", objbonus.BonusAmt),
                new System.Data.SqlClient.SqlParameter("@CreatedBy", objbonus.CreatedBy),
                new System.Data.SqlClient.SqlParameter("@DailyWages", objbonus.DailyWages),
                new System.Data.SqlClient.SqlParameter("@NoofDays", objbonus.NoofDays),
                new System.Data.SqlClient.SqlParameter("@LabourBonusAmount", objbonus.LabourBonusAmount),
                new System.Data.SqlClient.SqlParameter("@WagwsType", objbonus.rbwages),
                new System.Data.SqlClient.SqlParameter("@ConsolidatedSalary", objbonus.consolidated),
                new System.Data.SqlClient.SqlParameter("@deduction1", objbonus.deduction1),
                new System.Data.SqlClient.SqlParameter("@deduction2", objbonus.deduction2),
                new System.Data.SqlClient.SqlParameter("@deduction3", objbonus.deduction3),
                new System.Data.SqlClient.SqlParameter("@deductioncal", objbonus.deductioncal),
                new System.Data.SqlClient.SqlParameter("@lbrpercentage", objbonus.lbrpercentage),
                new System.Data.SqlClient.SqlParameter("@lbrpercentageAmt", objbonus.lbrpercentageAmt),
                new System.Data.SqlClient.SqlParameter("@lbrMonths", objbonus.lbrMonths),
                new System.Data.SqlClient.SqlParameter("@lbrbonus", objbonus.lbrbonus),
                new System.Data.SqlClient.SqlParameter("@FinancialPeriod", objbonus.Finance),
                new System.Data.SqlClient.SqlParameter("@DeductionChkVal", objbonus.DeductionChkVal),
                new System.Data.SqlClient.SqlParameter("@BonusType", objbonus.BonusType),
                new System.Data.SqlClient.SqlParameter("@MessDed", objbonus.messded),
                new System.Data.SqlClient.SqlParameter("@Hostelded", objbonus.Hostel),
                new System.Data.SqlClient.SqlParameter("@Ccode", objbonus.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", objbonus.Lcode),
                new System.Data.SqlClient.SqlParameter("@FDA", objbonus.FDA),
                new System.Data.SqlClient.SqlParameter("@VDA",objbonus.VDA),
                new System.Data.SqlClient.SqlParameter("@Attenance", objbonus.Attenance)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "BonusforAll_Update", Bonus_update);
        }
        public string BankName_verify_value(string bankname)
        {
            System.Data.SqlClient.SqlParameter[] bank = 
            {
                new System.Data.SqlClient.SqlParameter("@bankname", bankname)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "BankName_verify", bank);
            return str;
        }
        public string BankCd_verify_value(string bankcd)
        {
            System.Data.SqlClient.SqlParameter[] bank = 
            {
                new System.Data.SqlClient.SqlParameter("@bankcode", bankcd)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "BankCode_verify", bank);
            return str;
        }
        public string Bank_Delete(string bankcode)
        {
            System.Data.SqlClient.SqlParameter[] bank = 
            {
                new System.Data.SqlClient.SqlParameter("@bnkcd", bankcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "check_bank_details", bank);
            return str;
        }
        public string DepartmentName_check(string dptnm)
        {
            System.Data.SqlClient.SqlParameter[] dpt = 
            {
                new System.Data.SqlClient.SqlParameter("@dptname", dptnm)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "DepartName_check", dpt);
            return str;
        }
        public string Departmentcode_check(string dptcd)
        {
            System.Data.SqlClient.SqlParameter[] dpt = 
            {
                new System.Data.SqlClient.SqlParameter("@dptcd", dptcd)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Departcode_check", dpt);
            return str;
        }
        public string Departmentcode_delete(string dptcd)
        {
            System.Data.SqlClient.SqlParameter[] dpt = 
            {
                new System.Data.SqlClient.SqlParameter("@deptcode", dptcd)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Department_delete", dpt);
            return str;
        }
        public string EmpType_check(string empType, string staff)
        {
            System.Data.SqlClient.SqlParameter[] emp = 
            {
                new System.Data.SqlClient.SqlParameter("@empcd", empType),
                new System.Data.SqlClient.SqlParameter("@empCategory", staff)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "empType_check", emp);
            return str;
        }
        public string EmpTypeName_check(string empType, string staff)
        {
            System.Data.SqlClient.SqlParameter[] emp = 
            {
                new System.Data.SqlClient.SqlParameter("@empname", empType),
                new System.Data.SqlClient.SqlParameter("@empCategory", staff)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "empTypeName_check", emp);
            return str;
        }
        public string EmpType_delete(string empType)
        {
            System.Data.SqlClient.SqlParameter[] emp = 
            {
                new System.Data.SqlClient.SqlParameter("@empcd", empType)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "empType_delete", emp);
            return str;
        }
        public string Insuranceid_check(string id)
        {
            System.Data.SqlClient.SqlParameter[] insurance = 
            {
                new System.Data.SqlClient.SqlParameter("@insid", id)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "INSURANCEID_CHECK", insurance);
            return str;
        }
        public string Insurancename_check(string InsName)
        {
            System.Data.SqlClient.SqlParameter[] name = 
            {
                new System.Data.SqlClient.SqlParameter("@insname", InsName)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "INSURANCENAME_CHECK", name);
            return str;
        }
        public string Insurance_delete(string insid)
        {
            System.Data.SqlClient.SqlParameter[] ins = 
            {
                new System.Data.SqlClient.SqlParameter("@insid", insid)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "insurance_delete", ins);
            return str;
        }
        public string Leaveid_check(string id1)
        {
            System.Data.SqlClient.SqlParameter[] idvalue = 
            {
                new System.Data.SqlClient.SqlParameter("@leaveid", id1)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "leavemaasterid_check", idvalue);
            return str;
        }
        public string Leavename_check(string name)
        {
            System.Data.SqlClient.SqlParameter[] idvalue = 
            {
                new System.Data.SqlClient.SqlParameter("@leavename", name)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "LeaveName_check", idvalue);
            return str;
        }
        public string Leave_delete(string id1)
        {
            System.Data.SqlClient.SqlParameter[] idvalue = 
            {
                new System.Data.SqlClient.SqlParameter("@leaveid", id1)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "leave_delete", idvalue);
            return str;
        }
        public string Probationid_check(string Pid)
        {
            System.Data.SqlClient.SqlParameter[] idvalue = 
            {
                new System.Data.SqlClient.SqlParameter("@id", Pid)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "PROBATIONID_CHECK", idvalue);
            return str;
        }
        public string Probationmonth_check(string month)
        {
            System.Data.SqlClient.SqlParameter[] idvalue = 
            {
                new System.Data.SqlClient.SqlParameter("@month", month)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "PROBATIONNAME_CHECK", idvalue);
            return str;
        }
        public string Probation_delete(string Pid)
        {
            System.Data.SqlClient.SqlParameter[] idvalue = 
            {
                new System.Data.SqlClient.SqlParameter("@id", Pid)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "PROBATION_DELETE", idvalue);
            return str;
        }
        public string qualificationid_check(string Qid)
        {
            System.Data.SqlClient.SqlParameter[] idvalue = 
            {
                new System.Data.SqlClient.SqlParameter("@id", Qid)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "qualificationid_check", idvalue);
            return str;
        }
        public string qualificationname_check(string qname)
        {
            System.Data.SqlClient.SqlParameter[] idvalue = 
            {
                new System.Data.SqlClient.SqlParameter("@name", qname)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "qualification_check", idvalue);
            return str;
        }
        public string qualification_delete(string Qid)
        {
            System.Data.SqlClient.SqlParameter[] idvalue = 
            {
                new System.Data.SqlClient.SqlParameter("@id", Qid)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "qualification_delete", idvalue);
            return str;
        }
        public DataTable Deactive_Load_all(string Staff, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] idValue = 
            {
                new System.Data.SqlClient.SqlParameter("@staff", Staff),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("deactive_employee_load", idValue);
            return dt;
        }
        public DataTable user_gridLoad(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] USER = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("USER_lOAD", USER);
            return dt;
        }
        public DataTable user_edit(string userid)
        {
            System.Data.SqlClient.SqlParameter[] USER = 
            {
                new System.Data.SqlClient.SqlParameter("@user", userid)
            };
            DataTable dt = SQL.ExecuteDatatable("user_edit", USER);
            return dt;
        }
        public string user_verify(string user, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] user1 = 
           {
               new System.Data.SqlClient.SqlParameter("@user", user),
               new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
               new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
           };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "user_verify", user1);
            return str;
        }
        public void UserRegistration_update(UserRegistrationClass objReg)
        {
            System.Data.SqlClient.SqlParameter[] UserRegistration = 
            {
                new System.Data.SqlClient.SqlParameter("@UserCode",objReg.UserCode),
                new System.Data.SqlClient.SqlParameter("@UserName",objReg.UserName),
          
                new System.Data.SqlClient.SqlParameter("@Password",objReg.Password),
                new System.Data.SqlClient.SqlParameter("@IsAdmin",objReg.IsAdmin),
                new System.Data.SqlClient.SqlParameter("@Mobile",objReg.Mobile),
                new System.Data.SqlClient.SqlParameter("@Department",objReg.Department),
                new System.Data.SqlClient.SqlParameter("@Designation",objReg.Designation),
                new System.Data.SqlClient.SqlParameter("@Ccode", objReg.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", objReg.Lcode)
             
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "USER_UPDATE", UserRegistration);
        }
        public DataTable Loadgrid_leaveApply(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] leave = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("editleaveDetail_Load", leave);
            return dt;
        }
        public DataTable retrive_leaveApply(string EmpNo)
        {
            System.Data.SqlClient.SqlParameter[] Leave = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo)
            };
            DataTable dt = SQL.ExecuteDatatable("editLeaveDetail_Select", Leave);
            return dt;
        }
        public void leaveCancel(string tid)
        {
            System.Data.SqlClient.SqlParameter[] leaveCancel_1 = 
            {
                new System.Data.SqlClient.SqlParameter("@id", tid),
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "CANCEL_LEAVE_SP", leaveCancel_1);
        }
        public DataTable AlreadyLeaveDateLoad_edit(string EmpNo, string Financialyear, string tid, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Leave_load = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@FinaYear", Financialyear),
                new System.Data.SqlClient.SqlParameter("@id", tid),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)

            };
            DataTable dt = SQL.ExecuteDatatable("alreadyleaveDate_Load_Edit", Leave_load);
            return dt;
        }
        public DataTable AllReadyApplyForLeaveDate_edit(string EmpNo, DateTime FromDate, DateTime ToDate, string FinaYear, string tid, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] CheckApplyLeave = 
            {
               new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
               new System.Data.SqlClient.SqlParameter("@FromDate",FromDate),
               new System.Data.SqlClient.SqlParameter("@ToDate",ToDate),
               new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
               new System.Data.SqlClient.SqlParameter("@id", tid),
               new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)

            };
            DataTable dt = SQL.ExecuteDatatable("AllReadyApplyForLeaveDate_Edit", CheckApplyLeave);
            return dt;

        }
        public void leaveupdate_verify(string tid, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] leaveCancel_1 = 
            {
                new System.Data.SqlClient.SqlParameter("@id", tid),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UpdateLeave_verify_sp", leaveCancel_1);
        }
        public void Rpt_contractLoad(string department)
        {
            System.Data.SqlClient.SqlParameter[] rpt = 
            {
                new System.Data.SqlClient.SqlParameter("@department", department)
            };
            DataTable dt = SQL.ExecuteDatatable("Contract_employee_grid", rpt);
        }
        public DataTable Contractbreak_EmployeeSearch_exist(string department, string exist, string cate, string isadmin, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Contractbreak_EmployeeSearch = 
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@EXIST",exist),
                new System.Data.SqlClient.SqlParameter("@cate",cate),
                 new System.Data.SqlClient.SqlParameter("@isadmin", isadmin),
                 new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Contractbreak_EmployeeSearch_EXIST", Contractbreak_EmployeeSearch);
            return dt;
        }
        public DataTable Probationperiodsearch_exist(string department, string exist, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Probationperiodsearch =
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@exist",exist),
                 new System.Data.SqlClient.SqlParameter("@Cate",Cate),
                 new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                
        };
            DataTable dt = SQL.ExecuteDatatable("Probationperiodsearch_Exist", Probationperiodsearch);
            return dt;
        }
        public DataTable Probationperiodsearch_exist_user(string department, string exist, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Probationperiodsearch =
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@exist",exist),
                 new System.Data.SqlClient.SqlParameter("@Cate",Cate),
                 new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
        };
            DataTable dt = SQL.ExecuteDatatable("Probationperiodsearch_Exist_user", Probationperiodsearch);
            return dt;
        }
        public DataTable AdvanceSalary_EmpNo(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Advance = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
            };
            DataTable dt = SQL.ExecuteDatatable("Advance_Emp_Search", Advance);
            return dt;
        }
        public DataTable AdvanceSalary_exist(string Exist, string department, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Advance = 
            {
                new System.Data.SqlClient.SqlParameter("@exist", Exist),
                new System.Data.SqlClient.SqlParameter("@dpt", department),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Advance_Emp_Search_exist", Advance);
            return dt;
        }
        public DataTable AdvanceSalary_exist_user(string Exist, string department, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Advance = 
            {
                new System.Data.SqlClient.SqlParameter("@exist", Exist),
                new System.Data.SqlClient.SqlParameter("@dpt", department),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Advance_Emp_Search_exist_user", Advance);
            return dt;
        }
        public string Advance_employee_verify(string Exist, string empname)
        {
            System.Data.SqlClient.SqlParameter[] verify = 
            {
                new System.Data.SqlClient.SqlParameter("@exist", Exist),
                new System.Data.SqlClient.SqlParameter("@empName", empname)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Advance_employee_verify", verify);
            return (str);
        }
        public string Advance_insert_verify(string empNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] verify = 
            {
                new System.Data.SqlClient.SqlParameter("@empNo", empNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Advance_insert_verify", verify);
            return (str);
        }
        public void Advance_insert(AdvanceAmount objsal, DateTime transdate)
        {
            System.Data.SqlClient.SqlParameter[] ins = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", objsal.EmpNo),
                new System.Data.SqlClient.SqlParameter("@ExistNo", objsal.Exist),
                new System.Data.SqlClient.SqlParameter("@TransDate", transdate),
                new System.Data.SqlClient.SqlParameter("@Month", objsal.Months),
                new System.Data.SqlClient.SqlParameter("@Amount", objsal.Amount),
                new System.Data.SqlClient.SqlParameter("@MonthlyDeduction", objsal.MontlyDeduction),
                new System.Data.SqlClient.SqlParameter("@Ccode", objsal.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", objsal.Lcode)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Advance_Insert_SP", ins);
        }
        public DataTable Salary_advance_retrive(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] advanceamt = 
            {
                new System.Data.SqlClient.SqlParameter("@empNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Salary_advance_retrive", advanceamt);
            return dt;
        }
        public void Advance_repayInsert(string EmpNo, DateTime Transdate, string Months, string Amount, string AdvId, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] advins = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@TransDate", Transdate),
                new System.Data.SqlClient.SqlParameter("@Months",Months),
                new System.Data.SqlClient.SqlParameter("@Amount", Amount),
                new System.Data.SqlClient.SqlParameter("@AdvanceId", AdvId),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Repay_insert", advins);
        }
        public void Advance_update(string EmpNo, string BalanceAmount, string IncMonth, string DecMonth, string Id, string completed, DateTime modifiedDate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] advUpdate = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@BalanceAmount", BalanceAmount),
                new System.Data.SqlClient.SqlParameter("@IncreaseMonth", IncMonth),
                new System.Data.SqlClient.SqlParameter("@ReductionMonth", DecMonth),
                new System.Data.SqlClient.SqlParameter("@Id", Id),
                new System.Data.SqlClient.SqlParameter("@completed", completed),
                new System.Data.SqlClient.SqlParameter("@date", modifiedDate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Advance_Update", advUpdate);
        }
        public DataTable ADV_ALL_REPORT(string EmpNo)
        {
            System.Data.SqlClient.SqlParameter[] adv = 
            {
                new System.Data.SqlClient.SqlParameter("@empNo", EmpNo)
            };
            DataTable dt = SQL.ExecuteDatatable("ADV_ALL_REPORT", adv);
            return dt;
        }
        public DataTable Adv_history(string id, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] adv = 
            {
                new System.Data.SqlClient.SqlParameter("@id", id),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Adv_history", adv);
            return dt;
        }
        public DataTable Depart_Labour()
        {
            System.Data.SqlClient.SqlParameter[] load_dept = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("Department_Load_Labour", load_dept);
            return dt;
        }
        public DataTable repay_load(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] repay = 
            {
                new System.Data.SqlClient.SqlParameter("@empNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("repay_load", repay);
            return dt;
        }
        public DataTable months_load()
        {
            System.Data.SqlClient.SqlParameter[] months = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("month_load", months);
            return dt;
        }
        public void Attenance_insert(AttenanceClass objAtt, DateTime fromDate, DateTime Todate, string modeval)
        {
            System.Data.SqlClient.SqlParameter[] att_ins = 
            {
                new System.Data.SqlClient.SqlParameter("@DeptName", objAtt.department),
                new System.Data.SqlClient.SqlParameter("@EmpNo", objAtt.EmpNo),
                new System.Data.SqlClient.SqlParameter("@ExistingCode", objAtt.Exist),
                new System.Data.SqlClient.SqlParameter("@Days", objAtt.days),
                new System.Data.SqlClient.SqlParameter("@Month", objAtt.Months),
                new System.Data.SqlClient.SqlParameter("@FinancialYear", objAtt.finance),
                new System.Data.SqlClient.SqlParameter("@TotalDays", objAtt.TotalDays),
                new System.Data.SqlClient.SqlParameter("@Ccode", objAtt.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", objAtt.Lcode),
                new System.Data.SqlClient.SqlParameter("@nfh", objAtt.nfh),
                new System.Data.SqlClient.SqlParameter("@home", objAtt.HomeDays),
                new System.Data.SqlClient.SqlParameter("@work", objAtt.workingdays),
                new System.Data.SqlClient.SqlParameter("@cl", objAtt.cl),
                new System.Data.SqlClient.SqlParameter("@AbsentDays", objAtt.Absent),
                new System.Data.SqlClient.SqlParameter("@weekoff", objAtt.weekoff),
                new System.Data.SqlClient.SqlParameter("@fromDate", fromDate),
                new System.Data.SqlClient.SqlParameter("@ToDate", Todate),
                new System.Data.SqlClient.SqlParameter("@Modeval", modeval),
                new System.Data.SqlClient.SqlParameter("@half", objAtt.half),
                new System.Data.SqlClient.SqlParameter("@Full", objAtt.Full),
                new System.Data.SqlClient.SqlParameter("@Three", objAtt.ThreeSided),
                new System.Data.SqlClient.SqlParameter("@OTHoursNew", objAtt.OTHoursNew),
                new System.Data.SqlClient.SqlParameter("@Fixed_Work_Days", objAtt.Fixed_Work_Days),
                new System.Data.SqlClient.SqlParameter("@WH_Work_Days", objAtt.WH_Work_Days),
                new System.Data.SqlClient.SqlParameter("@NFH_Work_Days", objAtt.NFH_Work_Days)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Attenance_insert", att_ins);
        }
        public void Attenance_Update(AttenanceClass objAtt)
        {
            System.Data.SqlClient.SqlParameter[] att_upd = 
            {
                new System.Data.SqlClient.SqlParameter("@DeptName", objAtt.department),
                new System.Data.SqlClient.SqlParameter("@EmpNo", objAtt.EmpNo),
                new System.Data.SqlClient.SqlParameter("@ExistingCode", objAtt.Exist),
                new System.Data.SqlClient.SqlParameter("@Days", objAtt.days),
                new System.Data.SqlClient.SqlParameter("@Month", objAtt.Months),
                new System.Data.SqlClient.SqlParameter("@FinancialYear", objAtt.finance),
                new System.Data.SqlClient.SqlParameter("@TotalDays", objAtt.TotalDays),
                new System.Data.SqlClient.SqlParameter("@Ccode", objAtt.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", objAtt.Lcode),
                new System.Data.SqlClient.SqlParameter("@cl", objAtt.cl),
                new System.Data.SqlClient.SqlParameter("@AbsentDays", objAtt.Absent),
                new System.Data.SqlClient.SqlParameter("@weekoff", objAtt.weekoff)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Attenance_update", att_upd);
        }
        public DataTable Salary_attenanceDetails(string EmpNo, string Months, string Finance, string Department, string Ccode, string Lcode, DateTime dfrom, DateTime dto)
        {
            System.Data.SqlClient.SqlParameter[] attenance = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@finance", Finance),
                new System.Data.SqlClient.SqlParameter("@department", Department),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@dfrom", dfrom),
                new System.Data.SqlClient.SqlParameter("@dto", dto)
            };
            DataTable dt = SQL.ExecuteDatatable("Attenance_Employee_Load", attenance);
            return dt;
        }
        public DataTable Settle_EmpNo_load(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Settle = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Settle_EmpNo_load", Settle);
            return dt;
        }
        public DataTable Settle_Exist_load(string EmpNo, string category, string department, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Settle = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@cate", category),
                new System.Data.SqlClient.SqlParameter("@department", department),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Settle_Exist_load", Settle);
            return dt;
        }
        public void settle_insert(resignClass rej, DateTime doj, DateTime resigndate)
        {
            System.Data.SqlClient.SqlParameter[] resign_ins = 
            {
                new System.Data.SqlClient.SqlParameter("@Category",rej.Category),
                new System.Data.SqlClient.SqlParameter("@Department",rej.department),
                new System.Data.SqlClient.SqlParameter("@EmpNo",rej.EmpNo),
                new System.Data.SqlClient.SqlParameter("@EmpName",rej.EmpName),
                new System.Data.SqlClient.SqlParameter("@ExistingCode",rej.Exist),
                new System.Data.SqlClient.SqlParameter("@Designation",rej.designation),
                new System.Data.SqlClient.SqlParameter("@Doj",doj),
                new System.Data.SqlClient.SqlParameter("@Experience",rej.Experience),
                new System.Data.SqlClient.SqlParameter("@ResignDate",resigndate),
                new System.Data.SqlClient.SqlParameter("@ElgibleESI",rej.EligibleESI),
                new System.Data.SqlClient.SqlParameter("@PFAcc",rej.pfacc),
                new System.Data.SqlClient.SqlParameter("@ESIAcc",rej.esiacc),
                new System.Data.SqlClient.SqlParameter("@PFAmt",rej.pfamt),
                new System.Data.SqlClient.SqlParameter("@ESIAmt",rej.ESiamt),
                new System.Data.SqlClient.SqlParameter("@Username",rej.Username),
                new System.Data.SqlClient.SqlParameter("@reason",rej.Reason),
                new System.Data.SqlClient.SqlParameter("@Ccode", rej.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", rej.Lcode),
                new System.Data.SqlClient.SqlParameter("@Advance", rej.Advance)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Settlement_insert", resign_ins);
        }
        public string pf_Verify(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] pf = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "pf_Verify", pf);
            return str;
        }
        public DataTable PF_form5(string dpt, string staff)
        {
            System.Data.SqlClient.SqlParameter[] form5 = 
            {
                new System.Data.SqlClient.SqlParameter("@dpt", dpt),
                new System.Data.SqlClient.SqlParameter("@staff", staff)
            };
            DataTable dt = SQL.ExecuteDatatable("Form5_SP", form5);
            return dt;
        }
        public DataTable PF_form5_user(string dpt, string staff)
        {
            System.Data.SqlClient.SqlParameter[] form5 = 
            {
                new System.Data.SqlClient.SqlParameter("@dpt", dpt),
                new System.Data.SqlClient.SqlParameter("@staff", staff)
            };
            DataTable dt = SQL.ExecuteDatatable("Form5_SP_user", form5);
            return dt;
        }
        public DataTable Form10_sp(string cate, string dpt, string months, string finance)
        {
            System.Data.SqlClient.SqlParameter[] form10 = 
            {
                new System.Data.SqlClient.SqlParameter("@cate", cate),
                new System.Data.SqlClient.SqlParameter("@Dpt",dpt),
                new System.Data.SqlClient.SqlParameter("@mont",months),
                new System.Data.SqlClient.SqlParameter("@yr", finance)
            };
            DataTable dt = SQL.ExecuteDatatable("Form10_New", form10);
            return dt;
        }
        public DataTable Form10_sp_user(string cate, string dpt, string months, string finance)
        {
            System.Data.SqlClient.SqlParameter[] form10 = 
            {
                new System.Data.SqlClient.SqlParameter("@cate", cate),
                new System.Data.SqlClient.SqlParameter("@Dpt",dpt),
                new System.Data.SqlClient.SqlParameter("@mont",months),
                new System.Data.SqlClient.SqlParameter("@yr", finance)
            };
            DataTable dt = SQL.ExecuteDatatable("Form10_New_user", form10);
            return dt;
        }
        public DataTable Emp_Load_PF(string cate, string dpt)
        {
            System.Data.SqlClient.SqlParameter[] pf = 
            {
                new System.Data.SqlClient.SqlParameter("@cate", cate),
                new System.Data.SqlClient.SqlParameter("@dpt", dpt)
            };
            DataTable dt = SQL.ExecuteDatatable("Emp_Load_PF", pf);
            return dt;
        }
        public DataTable Emp_Load_PF_user(string cate, string dpt)
        {
            System.Data.SqlClient.SqlParameter[] pf = 
            {
                new System.Data.SqlClient.SqlParameter("@cate", cate),
                new System.Data.SqlClient.SqlParameter("@dpt", dpt)
            };
            DataTable dt = SQL.ExecuteDatatable("Emp_Load_PF_user", pf);
            return dt;
        }
        public DataTable Form3A_sp(string EmpNo, string finance)
        {
            System.Data.SqlClient.SqlParameter[] form3A = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@finance", finance)
            };
            DataTable dt = SQL.ExecuteDatatable("Form3A_sp", form3A);
            return dt;
        }
        public DataTable Emp_PFnumber_load(string EmpNo)
        {
            System.Data.SqlClient.SqlParameter[] pf = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo)
            };
            DataTable dt = SQL.ExecuteDatatable("Emp_PFnumber_load", pf);
            return dt;
        }
        public DataTable Form6A_New_SP(string cate, string dpt, string finance)
        {
            System.Data.SqlClient.SqlParameter[] form6A = 
            {
                new System.Data.SqlClient.SqlParameter("@staff", cate),
                new System.Data.SqlClient.SqlParameter("@dpt", dpt),
                new System.Data.SqlClient.SqlParameter("@finance", finance)
            };
            DataTable dt = SQL.ExecuteDatatable("Form6A_New_SP", form6A);
            return dt;
        }
        public DataTable Form6A_New_SP_user(string cate, string dpt, string finance)
        {
            System.Data.SqlClient.SqlParameter[] form6A = 
            {
                new System.Data.SqlClient.SqlParameter("@staff", cate),
                new System.Data.SqlClient.SqlParameter("@dpt", dpt),
                new System.Data.SqlClient.SqlParameter("@finance", finance)
            };
            DataTable dt = SQL.ExecuteDatatable("Form6A_New_SP_user", form6A);
            return dt;
        }
        public DataTable Emp_load_ESI(string cate, string department)
        {
            System.Data.SqlClient.SqlParameter[] esic = 
            {
                new System.Data.SqlClient.SqlParameter("@cate", cate),
                new System.Data.SqlClient.SqlParameter("@dpt", department)
            };
            DataTable dt = SQL.ExecuteDatatable("Emp_load_ESI", esic);
            return dt;
        }
        public DataTable Emp_load_ESI_user(string cate, string department)
        {
            System.Data.SqlClient.SqlParameter[] esic = 
            {
                new System.Data.SqlClient.SqlParameter("@cate", cate),
                new System.Data.SqlClient.SqlParameter("@dpt", department)
            };
            DataTable dt = SQL.ExecuteDatatable("Emp_load_ESI_user", esic);
            return dt;
        }
        public DataTable Form7_sp(string EmpNo)
        {
            System.Data.SqlClient.SqlParameter[] form7 = 
            {
                new System.Data.SqlClient.SqlParameter("@empNo", EmpNo)
            };
            DataTable dt = SQL.ExecuteDatatable("Form7_sp", form7);
            return dt;
        }
        public DataTable rpt_resign_yr(string fromdate, string todate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] rpt_yr = 
            {
                new System.Data.SqlClient.SqlParameter("@finance", fromdate),
                new System.Data.SqlClient.SqlParameter("@finance1", todate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptResign_yr", rpt_yr);
            return dt;
        }
        public DataTable rpt_resign_yr_user(string fromdate, string todate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] rpt_yr = 
            {
                new System.Data.SqlClient.SqlParameter("@finance", fromdate),
                new System.Data.SqlClient.SqlParameter("@finance1", todate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptResign_yr_user", rpt_yr);
            return dt;
        }
        public DataTable rpt_resign_month(string fromdate, string todate, string months, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] rpt_month = 
            {
                new System.Data.SqlClient.SqlParameter("@finance", fromdate),
                new System.Data.SqlClient.SqlParameter("@finance1", todate),
                new System.Data.SqlClient.SqlParameter("@month", months),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptResign_Month", rpt_month);
            return dt;
        }
        public DataTable rpt_resign_month_user(string fromdate, string todate, string months, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] rpt_month = 
            {
                new System.Data.SqlClient.SqlParameter("@finance", fromdate),
                new System.Data.SqlClient.SqlParameter("@finance1", todate),
                new System.Data.SqlClient.SqlParameter("@month", months),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("RptResign_Month_user", rpt_month);
            return dt;
        }
        public DataTable rpt_resign_empNo(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] rpt_resign = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("rpt_resign_empNo", rpt_resign);
            return dt;
        }
        public DataTable rpt_resign_empNo_user(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] rpt_resign = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("rpt_resign_empNo_user", rpt_resign);
            return dt;
        }
        public DataTable rpt_resign_exist(string exist, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] rpt_resign = 
            {
                new System.Data.SqlClient.SqlParameter("@Exist", exist),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("rpt_resign_exist", rpt_resign);
            return dt;
        }
        public DataTable rpt_resign_exist_user(string exist, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] rpt_resign = 
            {
                new System.Data.SqlClient.SqlParameter("@Exist", exist),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("rpt_resign_exist_user", rpt_resign);
            return dt;
        }
        public DataTable LeaveAllocation_default(string dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] allocate = 
            {
                new System.Data.SqlClient.SqlParameter("@Department", dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("LeaveAllocation_default", allocate);
            return dt;
        }
        public string CompnyCode_verify(string Ccode)
        {
            System.Data.SqlClient.SqlParameter[] C_code = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "CompnyCode_verify", C_code);
            return str;
        }
        public string LocCode_verify(string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] L_code = 
            {
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "LocCode_verify", L_code);
            return str;
        }
        public void AdminRights_Insert(Administratorclass Ad)
        {
            System.Data.SqlClient.SqlParameter[] Admin_insert = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ad.Ccode),
                new System.Data.SqlClient.SqlParameter("@Cname",Ad.Cname),
                new System.Data.SqlClient.SqlParameter("@Lcode",Ad.Lcode),
                new System.Data.SqlClient.SqlParameter("@Lname",Ad.Location),
                new System.Data.SqlClient.SqlParameter("@Address1",Ad.Address1),
                new System.Data.SqlClient.SqlParameter("@Address2",Ad.Address2),
                new System.Data.SqlClient.SqlParameter("@District",Ad.District),
                new System.Data.SqlClient.SqlParameter("@Taluk",Ad.Taluk),
                new System.Data.SqlClient.SqlParameter("@pincode",Ad.Pincode),
                new System.Data.SqlClient.SqlParameter("@phone",Ad.Phone),
                new System.Data.SqlClient.SqlParameter("@state",Ad.state),
                new System.Data.SqlClient.SqlParameter("@email",Ad.Email),
                new System.Data.SqlClient.SqlParameter("@Establishmnet",Ad.Establishment)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "AdminRights_Insert", Admin_insert);
        }
        public DataTable AdminRights_load()
        {
            System.Data.SqlClient.SqlParameter[] Loadgrid = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("AdminRights_load", Loadgrid);
            return dt;
        }
        public DataTable Admin_Retrive(string Ccode)
        {
            System.Data.SqlClient.SqlParameter[] Admin = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode",Ccode)
            };
            DataTable dt = SQL.ExecuteDatatable("Admin_retrive", Admin);
            return dt;
        }
        public void AdminRights_Update(Administratorclass Ad)
        {
            System.Data.SqlClient.SqlParameter[] Admin_update = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ad.Ccode),
                new System.Data.SqlClient.SqlParameter("@Cname",Ad.Cname),
                new System.Data.SqlClient.SqlParameter("@Lcode",Ad.Lcode),
                new System.Data.SqlClient.SqlParameter("@Lname",Ad.Location),
                new System.Data.SqlClient.SqlParameter("@Address1",Ad.Address1),
                new System.Data.SqlClient.SqlParameter("@Address2",Ad.Address2),
                new System.Data.SqlClient.SqlParameter("@District",Ad.District),
                new System.Data.SqlClient.SqlParameter("@Taluk",Ad.Taluk),
                new System.Data.SqlClient.SqlParameter("@pincode",Ad.Pincode),
                new System.Data.SqlClient.SqlParameter("@phone",Ad.Phone),
                new System.Data.SqlClient.SqlParameter("@state",Ad.state),
                new System.Data.SqlClient.SqlParameter("@email",Ad.Email),
                new System.Data.SqlClient.SqlParameter("@Establishmnet",Ad.Establishment)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "AdminRights_Update", Admin_update);
        }
        public DataTable Dropdown_Company()
        {
            System.Data.SqlClient.SqlParameter[] dd_company = 
            {
                
            };
            DataTable dt = SQL.ExecuteDatatable("Dropdown_Company", dd_company);
            return dt;
        }
        public DataTable dropdown_loc(string Ccode)
        {
            System.Data.SqlClient.SqlParameter[] ddloc = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                
            };
            DataTable dt = SQL.ExecuteDatatable("Dropdown_Location", ddloc);
            return dt;
        }
        public DataTable AltiusLogin(UserRegistrationClass objuser)
        {
            System.Data.SqlClient.SqlParameter[] Altius = 
            {
                new System.Data.SqlClient.SqlParameter("@userId", objuser.UserCode),
                new System.Data.SqlClient.SqlParameter("@password", objuser.Password)
            };
            DataTable dt = SQL.ExecuteDatatable("AltiusLogin", Altius);
            return dt;

        }
        public DataTable UserLogin(UserRegistrationClass objuser)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@userId", objuser.UserCode),
                new System.Data.SqlClient.SqlParameter("@password", objuser.Password),
                new System.Data.SqlClient.SqlParameter("@Ccode",objuser.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objuser.Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("UserLogin", user);
            return dt;
        }
        public DataTable Company_retrive(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Company = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Company_retrive", Company);
            return dt;
        }
        public DataTable Employee_EmpNo_load(string Ccode, String Lcode, string type, string staff)
        {
            System.Data.SqlClient.SqlParameter[] empload =
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@type", type),
                new System.Data.SqlClient.SqlParameter("@staff", staff)
            };
            DataTable dt = SQL.ExecuteDatatable("Employee_EmpNo_load", empload);
            return dt;
        }
        public string carry_load(string EmpNo, string ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] carry = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpCode", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "carry_load", carry);
            return str;
        }
        public string Bonus_attenance_Load(string EmpNo, string Dept, string finance, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Bonus_attenance = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Dept", Dept),
                new System.Data.SqlClient.SqlParameter("@finance", finance),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Bonus_attenance_Load", Bonus_attenance);
            return str;
        }
        public string Bonus_attenance_Load_months(string EmpNo, string Dept, string finance, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Bonus_attenance_m = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Dept", Dept),
                new System.Data.SqlClient.SqlParameter("@finance", finance),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Bonus_attenance_Load_months", Bonus_attenance_m);
            return str;
        }
        public DataTable OverTime_Load(string dept, string Admin, string Ccode, string Lcode, string WagesType)
        {
            System.Data.SqlClient.SqlParameter[] ot_load = 
            {
                new System.Data.SqlClient.SqlParameter("@dept", dept),
                new System.Data.SqlClient.SqlParameter("@Admin", Admin),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@wages", WagesType)

            };
            DataTable dt = SQL.ExecuteDatatable("OT_Load", ot_load);
            return dt;
        }
        public DataTable Overtime_Details(string dept, string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] ot_detail = 
            {
                new System.Data.SqlClient.SqlParameter("@dept", dept),
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("OT_SalaryLoad", ot_detail);
            return dt;
        }
        public DataTable OverTime_Exist(string dept, string EmpNo, string Ccode, string Lcode, string admin, string wages)
        {
            System.Data.SqlClient.SqlParameter[] ot_detail = 
            {
                new System.Data.SqlClient.SqlParameter("@dept", dept),
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",Lcode),
                new System.Data.SqlClient.SqlParameter("@admin", admin),
                new System.Data.SqlClient.SqlParameter("@wages", wages)
            };

            DataTable dt = SQL.ExecuteDatatable("OT_Exist", ot_detail);
            return dt;
        }
        public string OV_Exist_verify(string EmpNo, string Dept, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] verify = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Dept", Dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "OV_Exist_verify", verify);
            return str;
        }
        public string Overtime_verify(string EmpNo, string Dept, string Month, string finance, string Ccode, string Lcode, DateTime fromdt, DateTime todt)
        {
            System.Data.SqlClient.SqlParameter[] Same_verify = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@dept", Dept),
                new System.Data.SqlClient.SqlParameter("@Month", Month),
                new System.Data.SqlClient.SqlParameter("@finance", finance),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@from", fromdt),
                new System.Data.SqlClient.SqlParameter("@todt", todt)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Overtime_verify", Same_verify);
            return str;
        }
        public void OverTime_insert(Overtimeclass objot, string Ccode, string Lcode, DateTime fromdt, DateTime todt)
        {
            System.Data.SqlClient.SqlParameter[] objot_ins = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", objot.EmpNo),
                new System.Data.SqlClient.SqlParameter("@EmpName", objot.EmpName),
                new System.Data.SqlClient.SqlParameter("@dept", objot.department),
                new System.Data.SqlClient.SqlParameter("@finance", objot.Finance),
                new System.Data.SqlClient.SqlParameter("@months", objot.Months),
                new System.Data.SqlClient.SqlParameter("@dailysalary", objot.dailysalary),
                new System.Data.SqlClient.SqlParameter("@Hrs", objot.NHrs),
                new System.Data.SqlClient.SqlParameter("@chkmanual", objot.chkmanual),
                new System.Data.SqlClient.SqlParameter("@hrsSal", objot.hrsalary),
                new System.Data.SqlClient.SqlParameter("@netamt", objot.Netamt),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@from", fromdt),
                new System.Data.SqlClient.SqlParameter("@todt", todt)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "OverTime_insert", objot_ins);
        }
        public DataTable Attenance_ot(string EmpNo, string Months, string Finance, string dept, string Ccode, string Lcode, DateTime fromdt, DateTime todt)
        {
            System.Data.SqlClient.SqlParameter[] Att_ot = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Month", Months),
                new System.Data.SqlClient.SqlParameter("@finance", Finance),
                new System.Data.SqlClient.SqlParameter("@dept", dept),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@from", fromdt),
                new System.Data.SqlClient.SqlParameter("@to", todt)
            };
            DataTable dt = SQL.ExecuteDatatable("Salary_ot_load", Att_ot);
            return dt;
        }
        public DataTable PF_Download(string Months, string Finance, string Ccode, string Lcode, string Admin, string stafflabor)
        {
            System.Data.SqlClient.SqlParameter[] Download = 
            {
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@PFGrade", Admin),
                new System.Data.SqlClient.SqlParameter("@stafflabor", stafflabor)
            };
            DataTable dt = SQL.ExecuteDatatable("PF_Download", Download);
            return dt;
        }
        public DataTable Employee_Download(string Ccode, string Lcode, string Admin)
        {
            System.Data.SqlClient.SqlParameter[] Download_employee = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@admin", Admin)
            };
            DataTable dt = SQL.ExecuteDatatable("Employee_Download", Download_employee);
            return dt;
        }
        public DataTable Advance_restore(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Restore_data = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Advance_restore", Restore_data);
            return dt;
        }
        public void AdvanceAmount_Update_Loan(string EmpNo, string Months, string Balance, string Ccode, string Lcode, string due)
        {
            System.Data.SqlClient.SqlParameter[] Update_advance_Loan = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@Balace", Balance),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Monthly", due)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "AdvanceAmount_Update_Loan", Update_advance_Loan);
        }
        public DataTable ESI_PF_Load(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] ESI_PF = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("ESI_PF_Load", ESI_PF);
            return dt;
        }
        public string ESIPF_verify(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] ESI_PF_verify = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "ESIPF_verify", ESI_PF_verify);
            return str;
        }
        public void ESIPF_Insert(string PF, string ESI, string Sal, string Ccode, string Lcode, string Stamp, string VDA1, string VDA2, string Union, string spinning, string halfNight, string FullNight, string DayIncentive, string Three, string EmployeerPFone, string EmployeerPFtwo, string EmployeerESI)
        {
            System.Data.SqlClient.SqlParameter[] ESIPF_Ins = 
            {
                new System.Data.SqlClient.SqlParameter("@PF", PF),
                new System.Data.SqlClient.SqlParameter("@ESI",ESI),
                new System.Data.SqlClient.SqlParameter("@sal",Sal),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@stamp", Stamp),
                new System.Data.SqlClient.SqlParameter("@VDA1", VDA1),
                new System.Data.SqlClient.SqlParameter("@VDA2", VDA2),
                new System.Data.SqlClient.SqlParameter("@Union", Union),
                new System.Data.SqlClient.SqlParameter("@spinning", spinning),
                new System.Data.SqlClient.SqlParameter("@halfNight", halfNight),
                new System.Data.SqlClient.SqlParameter("@FullNight", FullNight),
                new System.Data.SqlClient.SqlParameter("@DayIncentive", DayIncentive),
                new System.Data.SqlClient.SqlParameter("@ThreeSide", Three),
                new System.Data.SqlClient.SqlParameter("@EmployeerPFone", EmployeerPFone),
                new System.Data.SqlClient.SqlParameter("@EmployeerPFtwo", EmployeerPFtwo),
                new System.Data.SqlClient.SqlParameter("@EmployeerESI", EmployeerESI)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "ESIPF_Insert", ESIPF_Ins);
        }
        public void ESIPF_Update(string PF, string ESI, string Sal, string Ccode, string Lcode, string Stamp, string VDA1, string VDA2, string Union, string spinning, string halfNight, string FullNight, string DayIncentive, string Three, string EmployeerPFone, string EmployeerPFtwo, string EmployeerESI)
        {
            System.Data.SqlClient.SqlParameter[] ESIPF_Upd = 
            {
                new System.Data.SqlClient.SqlParameter("@PF", PF),
                new System.Data.SqlClient.SqlParameter("@ESI",ESI),
                new System.Data.SqlClient.SqlParameter("@sal",Sal),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@stamp", Stamp),
                new System.Data.SqlClient.SqlParameter("@VDA1", VDA1),
                new System.Data.SqlClient.SqlParameter("@VDA2", VDA2),
                new System.Data.SqlClient.SqlParameter("@Union", Union),
                 new System.Data.SqlClient.SqlParameter("@spinning", spinning),
                new System.Data.SqlClient.SqlParameter("@halfNight", halfNight),
                new System.Data.SqlClient.SqlParameter("@FullNight", FullNight),
                new System.Data.SqlClient.SqlParameter("@DayIncentive", DayIncentive),
                new System.Data.SqlClient.SqlParameter("@ThreeSide", Three),
                new System.Data.SqlClient.SqlParameter("@EmployeerPFone", EmployeerPFone),
                new System.Data.SqlClient.SqlParameter("@EmployeerPFtwo", EmployeerPFtwo),
                new System.Data.SqlClient.SqlParameter("@EmployeerESI", EmployeerESI)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "ESIPF_Update", ESIPF_Upd);
        }
        public DataTable Salary_ExistNo(string department, string ExisistingNo, string Cate, string Ccode, string Lcode, string Admin)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterLabour_Load = 
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@ExisitingNo", ExisistingNo),
                new System.Data.SqlClient.SqlParameter("@Cate", Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Admin", Admin)
            };
            DataTable dt = SQL.ExecuteDatatable("Salary_ExistNo", SalaryMasterLabour_Load);
            return dt;
        }
        public DataTable NewSlary_Load(string Ccode, string Lcode, string Empcode)
        {
            System.Data.SqlClient.SqlParameter[] Load = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpNo", Empcode)
            };
            DataTable dt = SQL.ExecuteDatatable("NewSlary_Load", Load);
            return dt;
        }
        public DataTable Load_pf_details(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Lad_pf = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Load_pf_details", Lad_pf);
            return dt;
        }
        public DataTable EligibleESI_PF(string Ccode, string Lcode, string EmpNo)
        {
            System.Data.SqlClient.SqlParameter[] lad_all = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo)
            };
            DataTable dt = SQL.ExecuteDatatable("EligibleESI_PF", lad_all);
            return dt;
        }
        public string OT_Salary(string EmpNo, string Ccode, string Lcode, string Months, DateTime Mydate1, DateTime Mydate2)
        {
            System.Data.SqlClient.SqlParameter[] OT_val = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Month", Months),
                new System.Data.SqlClient.SqlParameter("@from", Mydate1),
                new System.Data.SqlClient.SqlParameter("@to", Mydate2)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "OT_Salary", OT_val);
            return str;
        }
        public DataTable PF_ESI_Amt(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Load_pf = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("PF_ESI_Amt", Load_pf);
            return dt;
        }
        public DataTable Rpt_overtime(string Ccode, string Lcode, string Finance, string Month)
        {
            System.Data.SqlClient.SqlParameter[] OT_rpt = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance),
                new System.Data.SqlClient.SqlParameter("@Month", Month)
            };
            DataTable dt = SQL.ExecuteDatatable("Rpt_OT", OT_rpt);
            return dt;
        }
        public DataTable Sample_EmpDownload(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Emp = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                
            };
            DataTable dt = SQL.ExecuteDatatable("Sample_EmpDownload", Emp);
            return dt;
        }
        public DataTable Sample_Attenance(string Ccode, string Lcode, string Salaryvalue, string staff)
        {
            System.Data.SqlClient.SqlParameter[] Attenance = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Salary", Salaryvalue),
                new System.Data.SqlClient.SqlParameter("@staff", staff)
                
            };
            DataTable dt = SQL.ExecuteDatatable("Sample_Attenance", Attenance);
            return dt;
        }
        public DataTable Sample_Attenance_Download(string Ccode, string Lcode, string Salaryvalue, string staff, string EmployeeType)
        {
            System.Data.SqlClient.SqlParameter[] Attenance = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Salary", Salaryvalue),
                new System.Data.SqlClient.SqlParameter("@staff", staff),
                new System.Data.SqlClient.SqlParameter("@EmployeeType", EmployeeType)
                
            };
            DataTable dt = SQL.ExecuteDatatable("Sample_Attenance_Download", Attenance);
            return dt;
        }
        public DataTable Wages_Download(string Ccode, string Lcode, string Salaryvalue, string staff, string EmployeeType)
        {
            System.Data.SqlClient.SqlParameter[] Attenance = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Salary", Salaryvalue),
                new System.Data.SqlClient.SqlParameter("@staff", staff),
                new System.Data.SqlClient.SqlParameter("@EmployeeType", EmployeeType)
                
            };
            DataTable dt = SQL.ExecuteDatatable("Wages_Download", Attenance);
            return dt;
        }
        public DataTable Sample_Leave(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Leave = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                
            };
            DataTable dt = SQL.ExecuteDatatable("Sample_Leave", Leave);
            return dt;
        }
        public DataTable ESI_Download(string Months, string Finance, string Ccode, string Lcode, string Admin, string staffLabour)
        {
            System.Data.SqlClient.SqlParameter[] Download = 
            {
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@PFGrade", Admin),
                new System.Data.SqlClient.SqlParameter("@staffLabour", staffLabour)
            };
            DataTable dt = SQL.ExecuteDatatable("ESI_Download", Download);
            return dt;
        }
        public DataTable OT_Download(string Ccode, string Lcode, string salarytype)
        {
            System.Data.SqlClient.SqlParameter[] OT_lad = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@SalaryType", salarytype)
                
            };
            DataTable dt = SQL.ExecuteDatatable("OT_Download", OT_lad);
            return dt;
        }
        public DataTable OT_Download_EmpTypeWise(string Ccode, string Lcode, string salarytype, string EmployeeType)
        {
            System.Data.SqlClient.SqlParameter[] OT_lad = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@SalaryType", salarytype),
                new System.Data.SqlClient.SqlParameter("@EmployeeType", EmployeeType)
                
            };
            DataTable dt = SQL.ExecuteDatatable("OT_Download_EmpTypeWise", OT_lad);
            return dt;
        }
        public DataTable SalaryDetails_rpt(string Months, string finance, string Ccode, string Lcode, string admin, string stafflabor)
        {
            System.Data.SqlClient.SqlParameter[] Sal = 
            {
                new System.Data.SqlClient.SqlParameter("@Month", Months),
                new System.Data.SqlClient.SqlParameter("@yr", finance),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@admin", admin),
                new System.Data.SqlClient.SqlParameter("@stafflabor", stafflabor)
            };
            DataTable dt = SQL.ExecuteDatatable("SalaryDetails_rpt", Sal);
            return dt;
        }
        public DataTable SalarySummary(string Finance, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] salSum = 
            {
                new System.Data.SqlClient.SqlParameter("@finance", Finance),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Salary_summary", salSum);
            return dt;
        }
        public DataTable IncentiveAmt(string Finance, string Months, string StaffLabour, string Admin, string Ccode, string Lcode, string department)
        {
            System.Data.SqlClient.SqlParameter[] Inc_amt = 
            {
                new System.Data.SqlClient.SqlParameter("@finance", Finance),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@staff", StaffLabour),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@department", department),
                new System.Data.SqlClient.SqlParameter("@admin", Admin)
            };
            DataTable dt = SQL.ExecuteDatatable("Incentive_Download", Inc_amt);
            return dt;
        }
        public DataTable Employee_muster(string Ccode, string Lcode, string department, string staff, string admin)
        {
            System.Data.SqlClient.SqlParameter[] Empmas = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@department", department),
                new System.Data.SqlClient.SqlParameter("@StaffLabour", staff),
                new System.Data.SqlClient.SqlParameter("@admin", admin)
            };
            DataTable dt = SQL.ExecuteDatatable("Employee_masters", Empmas);
            return dt;
        }
        public DataTable Salary_total_bonus(string EmpNo, int Finance, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Sal_total = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@finance", Finance),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Salary_year", Sal_total);
            return dt;
        }
        public DataTable BonusRpt(string Ccode, string Lcode, string finance, string admin, string Stafflabour)
        {
            System.Data.SqlClient.SqlParameter[] Rpt_bonus = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),                
                new System.Data.SqlClient.SqlParameter("@finance", finance),
                new System.Data.SqlClient.SqlParameter("@admin", admin),
                new System.Data.SqlClient.SqlParameter("@Stafflabour", Stafflabour)
            };
            DataTable dt = SQL.ExecuteDatatable("BonusRpt", Rpt_bonus);
            return dt;
        }
        public DataTable Salary_retrive(string EmpNo, string finance, string Month, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Sal_ret = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Financialyr",finance),
                new System.Data.SqlClient.SqlParameter("@Month",Month),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                
            };
            DataTable dt = SQL.ExecuteDatatable("Salary_retrive", Sal_ret);
            return dt;
        }
        public DataTable Advance_Edit(string EmpNo, string Months, string Amount, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Ad_Edit = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@Amount", Amount),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Advance_Edit", Ad_Edit);
            return dt;
        }
        public void SalaryDetails_update(SalaryClass objSal, string EmpNo, string ExisistingNo, DateTime MyDate, string Month, string Year, string FinaYear, DateTime TransDate, DateTime fromdt, DateTime Todt)
        {
            System.Data.SqlClient.SqlParameter[] SalaryDetails_upd =
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@MachineNo",EmpNo),
                new System.Data.SqlClient.SqlParameter("@ExisistingCode",ExisistingNo),
                new System.Data.SqlClient.SqlParameter("@SalaryThrough",objSal.SalaryThrough),
                new System.Data.SqlClient.SqlParameter("@FromBankACno",objSal.FromBankACno),
                new System.Data.SqlClient.SqlParameter("@FromBankName",objSal.FromBankName),
                new System.Data.SqlClient.SqlParameter("@FromBranch",objSal.FromBranch),
                new System.Data.SqlClient.SqlParameter("@ToBankACno",objSal.FromBankACno),
                new System.Data.SqlClient.SqlParameter("@ToBankName",objSal.FromBankName),
                new System.Data.SqlClient.SqlParameter("@ToBranch",objSal.FromBranch),
                new System.Data.SqlClient.SqlParameter("@Cash",objSal.Cash),
                new System.Data.SqlClient.SqlParameter("@BasicandDA",objSal.BasicAndDA),
                new System.Data.SqlClient.SqlParameter("@ProvidentFund",objSal.ProvidentFund),
                new System.Data.SqlClient.SqlParameter("@HRA",objSal.HRA),
                new System.Data.SqlClient.SqlParameter("@ESI",objSal.ESI),
                new System.Data.SqlClient.SqlParameter("@Conveyance",objSal.Conveyance),
                new System.Data.SqlClient.SqlParameter("@ProfessionalTax",objSal.ProvisionalTax),
                new System.Data.SqlClient.SqlParameter("@CCA",objSal.CCA),
                new System.Data.SqlClient.SqlParameter("@IncomeTax",objSal.IncomTax),
                new System.Data.SqlClient.SqlParameter("@GrossEarnings",objSal.GrossEarnings),
                new System.Data.SqlClient.SqlParameter("@SalaryAdvanceTrans",objSal.SalaryAdvanceTrans),
                new System.Data.SqlClient.SqlParameter("@MedicalReimburse",objSal.MedicalReiburse),
                new System.Data.SqlClient.SqlParameter("@PerIncentive",objSal.PerIncentive),
                new System.Data.SqlClient.SqlParameter("@allowances1",objSal.Allowances1),
                new System.Data.SqlClient.SqlParameter("@allowances2",objSal.Allowances2),
                new System.Data.SqlClient.SqlParameter("@allowances3",objSal.Allowances3),
                new System.Data.SqlClient.SqlParameter("@allowances4",objSal.Allowances4),
                new System.Data.SqlClient.SqlParameter("@allowances5",objSal.Allowances5),
                new System.Data.SqlClient.SqlParameter("@Deduction1",objSal.Deduction1),
                new System.Data.SqlClient.SqlParameter("@Deduction2",objSal.Deduction2),
                new System.Data.SqlClient.SqlParameter("@Deduction3",objSal.Deduction3),
                new System.Data.SqlClient.SqlParameter("@Deduction4",objSal.Deduction4),
                new System.Data.SqlClient.SqlParameter("@Deduction5",objSal.Deduction5),
                new System.Data.SqlClient.SqlParameter("@LossPay",objSal.LossofPay),
                new System.Data.SqlClient.SqlParameter("@TotalDeductions",objSal.TotalDeduction),
                new System.Data.SqlClient.SqlParameter("@TotalPay",objSal.Totalpay),
                new System.Data.SqlClient.SqlParameter("@NetPay",objSal.NetPay),
                new System.Data.SqlClient.SqlParameter("@SalaryDate",MyDate),
                new System.Data.SqlClient.SqlParameter("@Month",Month),
                new System.Data.SqlClient.SqlParameter("@Year",Year),
                new System.Data.SqlClient.SqlParameter("@FinaYear",FinaYear),
                new System.Data.SqlClient.SqlParameter("@TotalWorkingDays",objSal.TotalWorkingDays),
                new System.Data.SqlClient.SqlParameter("@MessDeduction",objSal.MessDeduction),
                new System.Data.SqlClient.SqlParameter("@HostelDeduction",objSal.HostelDeduction),
                new System.Data.SqlClient.SqlParameter("@LeaveDays",objSal.ApplyLeaveDays),
                new System.Data.SqlClient.SqlParameter("@LOPDays",objSal.LOPDays),
                new System.Data.SqlClient.SqlParameter("@TransDate",TransDate),
                new System.Data.SqlClient.SqlParameter("@Ccode", objSal.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", objSal.Lcode),
                new System.Data.SqlClient.SqlParameter("@NFh", objSal.NFh),
                new System.Data.SqlClient.SqlParameter("@OT", objSal.OT),
                new System.Data.SqlClient.SqlParameter("@FDA", objSal.FDA),
                new System.Data.SqlClient.SqlParameter("@VDA", objSal.VDA),
                new System.Data.SqlClient.SqlParameter("@Advance", objSal.Advance),
                new System.Data.SqlClient.SqlParameter("@stamp", objSal.Stamp),
                new System.Data.SqlClient.SqlParameter("@union", objSal.Union),
                new System.Data.SqlClient.SqlParameter("@pfsal", objSal.PfSalary),
                new System.Data.SqlClient.SqlParameter("@Words",objSal.Words),
                new System.Data.SqlClient.SqlParameter("@WorkedDays",objSal.work),
                new System.Data.SqlClient.SqlParameter("@weekoff", objSal.weekoff),
                new System.Data.SqlClient.SqlParameter("@CL", objSal.CL),
                new System.Data.SqlClient.SqlParameter("@FixBase", objSal.FixedBase),
                new System.Data.SqlClient.SqlParameter("@FixFDA", objSal.FixedFDA),
                new System.Data.SqlClient.SqlParameter("@FixHRA", objSal.FixedFRA),
                new System.Data.SqlClient.SqlParameter("@EmpPF", objSal.Emp_PF),
                new System.Data.SqlClient.SqlParameter("@from", fromdt),
                new System.Data.SqlClient.SqlParameter("@to", Todt),
                new System.Data.SqlClient.SqlParameter("@home", objSal.HomeTown),
                new System.Data.SqlClient.SqlParameter("@HalfNightAmt", objSal.HalfNightAmt),
                new System.Data.SqlClient.SqlParameter("@FullNightAmt", objSal.FullNightAmt),
                new System.Data.SqlClient.SqlParameter("@SpinningAmt", objSal.SpinningAmt),
                new System.Data.SqlClient.SqlParameter("@DayIncentive", objSal.DayIncentive),
                new System.Data.SqlClient.SqlParameter("@ThreesidedAmt", objSal.ThreesidedAmt)             
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalaryDetails_Update", SalaryDetails_upd);
        }
        public DataTable group_employee(string Months, string Finance, string Ccode, string Lcode, string EmpType, string stafflabour)
        {
            System.Data.SqlClient.SqlParameter[] group_empload = 
            {
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@FinancialYear", Finance),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpType", EmpType),
                new System.Data.SqlClient.SqlParameter("@staffLabour", stafflabour)

            };
            DataTable dt = SQL.ExecuteDatatable("GroupSalary_load", group_empload);
            return dt;
        }
        public DataTable GroupSalary_Load_attenance(string EmpNo, string Ccode, string Lcode, string Months, string Finance)
        {
            System.Data.SqlClient.SqlParameter[] Att = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance)
            };
            DataTable dt = SQL.ExecuteDatatable("GroupSalary_Load_attenance", Att);
            return dt;
        }
        public DataTable Group_salary_load(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] sal = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
                
            };
            DataTable dt = SQL.ExecuteDatatable("Group_salary_load", sal);
            return dt;
        }
        public DataTable Group_ot_load(string EmpNo, string Months, string Finance, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Att_ot = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Month", Months),
                new System.Data.SqlClient.SqlParameter("@finance", Finance),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Group_ot_load", Att_ot);
            return dt;
        }
        public string Group_Existing(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Exist = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Group_Existing", Exist);
            return str;
        }
        public DataTable Form3A_Load(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] form3A = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Form3A_Load", form3A);
            return dt;
        }
        public DataTable PF_EmpLoad(string Ccode, string Lcode, string EmpType)
        {
            System.Data.SqlClient.SqlParameter[] Emptype = 
            {
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Ccode",  Ccode),
                new System.Data.SqlClient.SqlParameter("@EmpType", EmpType)
            };
            DataTable DT = SQL.ExecuteDatatable("PF_EmpLoad", Emptype);
            return DT;
        }
        public DataTable PF_Load1(string EmpNo, string staff, string Ccode, string Lcode, string Finance)
        {
            System.Data.SqlClient.SqlParameter[] PF1 = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@staff", staff),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance)
            };
            DataTable dt = SQL.ExecuteDatatable("PF_Load1", PF1);
            return dt;
        }
        public DataTable PF_Load(string EmpNo, string staff, string Ccode, string Lcode, string Finance)
        {
            System.Data.SqlClient.SqlParameter[] PF = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@staff", staff),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance)
            };
            DataTable dt = SQL.ExecuteDatatable("PF_Load", PF);
            return dt;
        }
        public DataTable Form6A_load(string Ccode, string Lcode, string Finance, string Finance1)
        {
            System.Data.SqlClient.SqlParameter[] Form6A = 
            {
                
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance),
                new System.Data.SqlClient.SqlParameter("@Finance1", Finance1)
            };
            DataTable dt = SQL.ExecuteDatatable("Form6A_load", Form6A);
            return dt;
        }
        public string Establishment(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Establish = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Establishment", Establish);
            return str;
        }
        public void contract_insert_sp(ContractClass clss)
        {
            System.Data.SqlClient.SqlParameter[] con_sp = 
            {
                new System.Data.SqlClient.SqlParameter("@ContractName", clss.ContractName),
                new System.Data.SqlClient.SqlParameter("@ContractType", clss.ContractType),
                new System.Data.SqlClient.SqlParameter("@ContractYear", clss.ContractYear),
                new System.Data.SqlClient.SqlParameter("@ContractMonth", clss.ContractMonth),
                new System.Data.SqlClient.SqlParameter("@ContractDays", clss.ContractDays),
                new System.Data.SqlClient.SqlParameter("@FixedDays", clss.FixedDays),
                new System.Data.SqlClient.SqlParameter("@yrDays", clss.YrDays),
                new System.Data.SqlClient.SqlParameter("@PlannedDays", clss.PlannedDays)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Contract_insert_sp", con_sp);
        }
        public string Contract_verify(string contractname)
        {
            System.Data.SqlClient.SqlParameter[] con_verify = 
            {
                new System.Data.SqlClient.SqlParameter("@ContractName", contractname)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Contract_verify", con_verify);
            return str;
        }
        public DataTable Contract_retrive()
        {
            System.Data.SqlClient.SqlParameter[] con_ret = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("Contract_retieve", con_ret);
            return dt;
        }
        public DataTable Contract_Edit(string ContractCode)
        {
            System.Data.SqlClient.SqlParameter[] Con_Edit = 
            {
                new System.Data.SqlClient.SqlParameter("@ContractCode", ContractCode)
            };
            DataTable dt = SQL.ExecuteDatatable("Contract_Edit", Con_Edit);
            return dt;
        }
        public void Contract_Update_sp(ContractClass clss, string code)
        {
            System.Data.SqlClient.SqlParameter[] con_sp = 
            {
                new System.Data.SqlClient.SqlParameter("@ContractName", clss.ContractName),
                new System.Data.SqlClient.SqlParameter("@ContractType", clss.ContractType),
                new System.Data.SqlClient.SqlParameter("@ContractYear", clss.ContractYear),
                new System.Data.SqlClient.SqlParameter("@ContractMonth", clss.ContractMonth),
                new System.Data.SqlClient.SqlParameter("@ContractDays", clss.ContractDays),
                new System.Data.SqlClient.SqlParameter("@FixedDays", clss.FixedDays),
                new System.Data.SqlClient.SqlParameter("@ContractCode", code),
                new System.Data.SqlClient.SqlParameter("@yrDays", clss.YrDays),
                new System.Data.SqlClient.SqlParameter("@PlannedDays", clss.PlannedDays)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Contract_Update_sp", con_sp);
        }
        public DataTable DropDown_contract()
        {
            System.Data.SqlClient.SqlParameter[] dd_contract = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DropDown_contract", dd_contract);
            return dt;
        }
        public string Emp_contract(string EmpNo)
        {
            System.Data.SqlClient.SqlParameter[] Emp_con = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Emp_contract", Emp_con);
            return str;
        }
        public DataTable Official_contract(string Code)
        {
            System.Data.SqlClient.SqlParameter[] Con_code = 
            {
                new System.Data.SqlClient.SqlParameter("@ContractCode", Code)
            };
            DataTable dt = SQL.ExecuteDatatable("Official_contract", Con_code);
            return dt;
        }
        public void Insert_ContractDet(OfficialprofileClass objoff, string Date1)
        {
            System.Data.SqlClient.SqlParameter[] ins_con = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", objoff.EmpNo),
                new System.Data.SqlClient.SqlParameter("@ContractType", objoff.ContractType),
                new System.Data.SqlClient.SqlParameter("@yr", objoff.yr),
                new System.Data.SqlClient.SqlParameter("@Months", objoff.Months),
                new System.Data.SqlClient.SqlParameter("@Days", objoff.Days1),
                new System.Data.SqlClient.SqlParameter("@FixedDays", objoff.FixedDays),            
                new System.Data.SqlClient.SqlParameter("@StartingDate", Date1),
                new System.Data.SqlClient.SqlParameter("@Ccode", objoff.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", objoff.Lcode),
                new System.Data.SqlClient.SqlParameter("@ContractName", objoff.Contractname),
                new System.Data.SqlClient.SqlParameter("@YrDays", objoff.YrDays),
                new System.Data.SqlClient.SqlParameter("@PlannedDays", objoff.PlannedDays),
                new System.Data.SqlClient.SqlParameter("@BalanceDays", objoff.BalanceDays)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Insert_ContractDet", ins_con);
        }
        public DataTable Contract_Report(string Ccode, string Lcode, string ContractType)
        {
            System.Data.SqlClient.SqlParameter[] Con_report = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@ContractType", ContractType)
            };
            DataTable dt = SQL.ExecuteDatatable("Contract_Report", Con_report);
            return dt;
        }
        public string Contract_GetEmpDet(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] con_get = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Contract_GetEmpDet", con_get);
            return str;
        }
        public DataTable Contract_getPlanDetails(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] con_Det = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Contract_getPlanDetails", con_Det);
            return dt;
        }
        public void Contract_Reducing(string BalanceDays, string EmpNo, string val, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Con_Det_Upd = 
           {
               new System.Data.SqlClient.SqlParameter("@BalancedDays", BalanceDays),
               new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
               new System.Data.SqlClient.SqlParameter("@MonthDays", val),
               new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
               new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
           };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Contract_Reducing", Con_Det_Upd);
        }
        public void Contract_Reducing_Month(string BalanceDays, string EmpNo, string val, string Ccode, string Lcode, string months)
        {
            System.Data.SqlClient.SqlParameter[] Con_Det_Upd1 = 
           {
               new System.Data.SqlClient.SqlParameter("@BalancedDays", BalanceDays),
               new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
               new System.Data.SqlClient.SqlParameter("@MonthDays", val),
               new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
               new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
               new System.Data.SqlClient.SqlParameter("@Months", months)
           };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Contract_Reducing_Month", Con_Det_Upd1);
        }
        public void Not_Contract_Reducing_Month(string EmpNo, string val, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Con_Det_Upd2 = 
           {
               
               new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
               new System.Data.SqlClient.SqlParameter("@MonthDays", val),
               new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
               new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)              
           };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Not_Contract_Reducing_Month", Con_Det_Upd2);
        }
        public DataTable Contract_CompleteMonths(string Ccode, string Lcode, string ContractType)
        {
            System.Data.SqlClient.SqlParameter[] Con_Mon = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@ContractType", ContractType)
            };
            DataTable dt = SQL.ExecuteDatatable("Contract_CompleteMonths", Con_Mon);
            return dt;
        }
        public DataTable Contract_CompleteDays(string Ccode, string Lcode, string ContractType)
        {
            System.Data.SqlClient.SqlParameter[] Con_days = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@ContractType", ContractType)
            };
            DataTable dt = SQL.ExecuteDatatable("Contract_CompleteDays", Con_days);
            return dt;
        }
        public DataTable Contract_Details_CompletedReport(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] con_com = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Contract_Details_CompletedReport", con_com);
            return dt;
        }
        public void Contract_CompletedReport(OfficialprofileClass objclss, string Ccode, string Lcode, string startDate)
        {
            System.Data.SqlClient.SqlParameter[] con_rep = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", objclss.EmpNo),
                new System.Data.SqlClient.SqlParameter("@ContractType", objclss.ContractType),
                new System.Data.SqlClient.SqlParameter("@ContractName", objclss.Contractname),
                new System.Data.SqlClient.SqlParameter("@yr", objclss.yr),
                new System.Data.SqlClient.SqlParameter("@Months", objclss.Months),
                new System.Data.SqlClient.SqlParameter("@Days", objclss.Days1),
                new System.Data.SqlClient.SqlParameter("@FixedDays", objclss.FixedDays),
                new System.Data.SqlClient.SqlParameter("@YrDays", objclss.YrDays),
                new System.Data.SqlClient.SqlParameter("@PlannedDays", objclss.PlannedDays),
                new System.Data.SqlClient.SqlParameter("@StartingDate", startDate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Contract_CompletedReport", con_rep);
        }
        public void DeleteContract(string EmpNo, string Ccode, string Lcode, string ContractType, string ContractName)
        {
            System.Data.SqlClient.SqlParameter[] Con_del = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@ContractType", ContractType),
                new System.Data.SqlClient.SqlParameter("@ContractName", ContractName),
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "DeleteContract", Con_del);
        }
        public DataTable Contract_Load()
        {
            System.Data.SqlClient.SqlParameter[] Con_sel = 
            {
                
            };
            DataTable dt = SQL.ExecuteDatatable("ContractMst_Load", Con_sel);
            return dt;
        }
        public DataTable Contract_Load_Employee(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] con_emp = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", @Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Contract_EmpLoad", con_emp);
            return dt;
        }
        public string PF_Eligible_Salary(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] pf_val = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "PF_Eligible_Salary", pf_val);
            return str;
        }
        public string Verification_verify()
        {
            System.Data.SqlClient.SqlParameter[] Verify_verification = 
            {
               
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Verification_Get", Verify_verification);
            return str;
        }
        public DataTable Value_Verify()
        {
            System.Data.SqlClient.SqlParameter[] Value_ver = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("Value_Verify", Value_ver);
            return dt;
        }
        public void Insert_verificationValue()
        {
            System.Data.SqlClient.SqlParameter[] Verify_Insert = 
            {
               
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Insert_verificationValue", Verify_Insert);
        }
        public DataTable Employee_Payslip_Load(string Ccode, string Lcode, string Month, string FromDate, string ToDate, string Finance, string SalaryType)
        {
            System.Data.SqlClient.SqlParameter[] Employee_Payslip = 
            {                
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Month", Month),
                new System.Data.SqlClient.SqlParameter("@FromDate", FromDate),
                new System.Data.SqlClient.SqlParameter("@ToDate", ToDate),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance),
                new System.Data.SqlClient.SqlParameter("@SalaryType", SalaryType)
            };
            DataTable dt = SQL.ExecuteDatatable("Employee_Payslip_Load", Employee_Payslip);
            return dt;
        }
        public string TeamLeader_Count(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] count_val = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "TeamLeader_Count", count_val);
            return str;
        }
        public DataTable TeamLeader_New(string Ccode, string Lcode, string Department)
        {
            System.Data.SqlClient.SqlParameter[] TL_New = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@department", Department)
            };
            DataTable dt = SQL.ExecuteDatatable("TeamLeader_New", TL_New);
            return dt;
        }
        public DataTable TeamLeader_Exist(string Ccode, string Lcode, string Department)
        {
            System.Data.SqlClient.SqlParameter[] TL_New = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@department", Department)
            };
            DataTable dt = SQL.ExecuteDatatable("TeamLeader_Exist", TL_New);
            return dt;
        }
        public DataTable Team_Selection_Exist(string department, string ExisistingNo, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] TL_Exist = 
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@ExisitingNo", ExisistingNo),
                new System.Data.SqlClient.SqlParameter("@Cate", Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Team_Selection_Exist", TL_Exist);
            return dt;
        }
        public DataTable Team_Selection_EmpName(string department, string EmpNo, string Cate, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] TL_Name = 
            {
                new System.Data.SqlClient.SqlParameter("@Department", department),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Cate", Cate),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Team_Selection_EmpName", TL_Name);
            return dt;
        }
        public void TeamLeder_Insert(string EmpNo, string ID, string EmpName, string Ccode, string Lcode, string Department)
        {
            System.Data.SqlClient.SqlParameter[] TL_Insert = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@ID", ID),
                new System.Data.SqlClient.SqlParameter("@EmpName", EmpName),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Department", Department)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "TeamLeader_Insert", TL_Insert);
        }
        public DataTable TeamLeader_Grid(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] TL_grid = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("TeamLeader_Grid", TL_grid);
            return dt;
        }
        public string TeamLeader_Remove_verify(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] TL_r = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "TeamLeader_Remove_verify", TL_r);
            return str;
        }
        public void TeamLeader_Remove(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] TL_Delete = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "TeamLeader_Remove", TL_Delete);
        }
        public DataTable TL_Load(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] TL_Loading = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("TL_Load", TL_Loading);
            return dt;
        }
        public DataTable TL_Search_ID(string EmpID, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] TL_searchID = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpID", EmpID),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("TL_Search_ID", TL_searchID);
            return dt;
        }
        public DataTable TL_Search_Name(string EmpID, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] TL_searchID = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpID),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("TL_Search_Name", TL_searchID);
            return dt;
        }
        public DataTable TL_Loadgrid_ID(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] TL_Loadgrid_ID_1 = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("TL_Loadgrid_ID", TL_Loadgrid_ID_1);
            return dt;
        }
        public DataTable TeamMembers_Load(string Ccode, string Lcode, string Department)
        {
            System.Data.SqlClient.SqlParameter[] TM_Load = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Department", Department)
            };
            DataTable dt = SQL.ExecuteDatatable("TeamMembers_Load", TM_Load);
            return dt;
        }
        public void TeamMember_Insert(string tlEmpNo, string tmEmpNo, string Ccode, string Lcode, string TMname, string TMID, string TLID)
        {
            System.Data.SqlClient.SqlParameter[] TM_Insert = 
            {
                new System.Data.SqlClient.SqlParameter("@TLEmpNo", tlEmpNo),
                new System.Data.SqlClient.SqlParameter("@TMEmpNo", tmEmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@TMname", TMname),
                new System.Data.SqlClient.SqlParameter("@TMID", TMID),
                new System.Data.SqlClient.SqlParameter("@TLID", TLID)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "TeamMember_Insert", TM_Insert);
        }
        public void Team_Member_Remove(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] TM_remove = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Team_Member_Remove", TM_remove);
        }
        public void TeamLeaderIncentive_Insert(string Ccode, string Lcode, string TLCommission, string TLIncentive, string TLDays, string TLOT)
        {
            System.Data.SqlClient.SqlParameter[] TLINC_Insert = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@TLCommission", TLCommission),
                new System.Data.SqlClient.SqlParameter("@TLIncentive", TLIncentive),
                new System.Data.SqlClient.SqlParameter("@TLDays", TLDays),
                new System.Data.SqlClient.SqlParameter("@TLOT", TLOT)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "TeamLeaderIncentive_Insert", TLINC_Insert);
        }
        public string TeamLeaderIncentive_Verify(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] TL_ver = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "TeamLeaderIncentive_Verify", TL_ver);
            return str;
        }
        public void TeamLeaderIncentive_Update(string Ccode, string Lcode, string TLCommission, string TLIncentive, string TLDays, string TLOT)
        {
            System.Data.SqlClient.SqlParameter[] TLINC_Upd = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@TLCommission", TLCommission),
                new System.Data.SqlClient.SqlParameter("@TLIncentive", TLIncentive),
                new System.Data.SqlClient.SqlParameter("@TLDays", TLDays),
                new System.Data.SqlClient.SqlParameter("@TLOT", TLOT)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "TeamLeaderIncentive_Update", TLINC_Upd);
        }
        public DataTable TeamIncentive_Load(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] TL_Load = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("TeamIncentive_Load", TL_Load);
            return dt;
        }
        public void WorkerIncentive_Insert(string Ccode, string Lcode, string WorkerDays, string Amt, string MonthDays)
        {
            System.Data.SqlClient.SqlParameter[] Wk_ins = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@WorkerDays", WorkerDays),
                new System.Data.SqlClient.SqlParameter("@Amount", Amt),
                new System.Data.SqlClient.SqlParameter("@MonthDays", MonthDays)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "WorkerIncentive_Insert", Wk_ins);
        }
        public string WorkerIncentive_Verify(string Ccode, string Lcode, string Days, string MonthDays)
        {
            System.Data.SqlClient.SqlParameter[] Wk_ver = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@WorkerDays", Days),
                new System.Data.SqlClient.SqlParameter("@MonthDays", MonthDays)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "WorkerIncentive_Verify", Wk_ver);
            return str;
        }
        public DataTable Wk_Load(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] wk_load1 = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Wk_Load", wk_load1);
            return dt;
        }

        public DataTable CivilIncentive_Load(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] wk_load1 = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("CivilIncentive_Load", wk_load1);
            return dt;
        }

        public void WorkerIncentive_Update(string Ccode, string Lcode, string WorkerDays, string Amt, string MonthDays)
        {
            System.Data.SqlClient.SqlParameter[] Wk_ins = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@WorkerDays", WorkerDays),
                new System.Data.SqlClient.SqlParameter("@Amount", Amt),
                new System.Data.SqlClient.SqlParameter("@MonthDays", MonthDays)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "WorkerIncentive_Update", Wk_ins);
        }
        public void WorkerIncentive_Delete(string Ccode, string Lcode, string Days)
        {
            System.Data.SqlClient.SqlParameter[] Wk_del = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@WorkerDays", Days)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "WorkerIncentive_Delete", Wk_del);

        }
        public DataTable TeamMemberAttenance_Download(string Ccode, string Lcode, string MonthVal, string FinYearVal)
        {
            System.Data.SqlClient.SqlParameter[] TM_att_Dl = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@MonthVal", MonthVal),
                new System.Data.SqlClient.SqlParameter("@FinYearVal", FinYearVal)
            };
            DataTable dt = SQL.ExecuteDatatable("TeamMemberAttenance_Download", TM_att_Dl);
            return dt;
        }
        public void TeamAttenance_Insert(string Ccode, string Lcode, string EmpNo, string ID, string Days, string OT, string Months, string FYear, string Year_1)
        {
            System.Data.SqlClient.SqlParameter[] TeamAtt_ins = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@ID", ID),
                new System.Data.SqlClient.SqlParameter("@Days", Days),
                new System.Data.SqlClient.SqlParameter("@OTHours",OT),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@Year", Year_1),
                new System.Data.SqlClient.SqlParameter("@FYear", FYear)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "TeamAttenance_Insert", TeamAtt_ins);
        }
        public DataTable TeamMember_Load_Incentive(string Ccode, string Lcode, string EmpNo, string Months, string FYear)
        {
            System.Data.SqlClient.SqlParameter[] Tm_Load_Inc = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@FYear", FYear)
            };
            DataTable dt = SQL.ExecuteDatatable("TeamMember_Load_Incentive", Tm_Load_Inc);
            return dt;
        }
        public string TL_Data_Verify(string Ccode, string Lcode, string Months, string FYear, string EmpNo, string EmpNo_1)
        {
            System.Data.SqlClient.SqlParameter[] TL_Data_Ver = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@FinancialYear", FYear),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@EmpNo1", EmpNo_1)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "TL_Data_Verify", TL_Data_Ver);
            return str;
        }
        public string TL_Data_Process(string Ccode, string Lcode, string Months, string FYear, string EmpNo, string EmpNo_1)
        {
            System.Data.SqlClient.SqlParameter[] TL_Data_P = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@FinancialYear", FYear),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@EmpNo1", EmpNo_1)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "TL_Data_Process", TL_Data_P);
            return str;
        }
        public void TL_Data_Delete(string Ccode, string Lcode, string Months, string FYear, string EmpNo, string EmpNo_1)
        {
            System.Data.SqlClient.SqlParameter[] TL_Data_del = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@FinancialYear", FYear),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@EmpNo1", EmpNo_1)
            };
            SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "TL_Data_Delete", TL_Data_del);
        }
        public void Team_Leader_Data_Delete_Bulk(string Ccode, string Lcode, string Months, string FYear, string EmpNo)
        {
            System.Data.SqlClient.SqlParameter[] TL_Data_del = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@FinancialYear", FYear),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo)
            };
            SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Team_Leader_Data_Delete_Bulk", TL_Data_del);
        }
        public void TL_Data_Insert(TeamLeaderIncentive obj)
        {
            System.Data.SqlClient.SqlParameter[] Data_ins = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", obj.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", obj.Lcode),
                new System.Data.SqlClient.SqlParameter("@TLEmpNo", obj.TLEmpNo),
                new System.Data.SqlClient.SqlParameter("@TLId", obj.TLID),
                new System.Data.SqlClient.SqlParameter("@TLName", obj.TLEmpName),
                new System.Data.SqlClient.SqlParameter("@TMEmpNo", obj.TMEmpNo),
                new System.Data.SqlClient.SqlParameter("@TMId", obj.TMID),
                new System.Data.SqlClient.SqlParameter("@TMName", obj.TMName),
                new System.Data.SqlClient.SqlParameter("@TMDays", obj.Days),
                new System.Data.SqlClient.SqlParameter("@TMOT", obj.OT),
                new System.Data.SqlClient.SqlParameter("@TLIncentive", obj.Incentive),
                new System.Data.SqlClient.SqlParameter("@TLCommission", obj.Commission),
                new System.Data.SqlClient.SqlParameter("@Months", obj.Months),
                new System.Data.SqlClient.SqlParameter("@FinancialYear ", obj.FYear)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "TL_Data_Insert", Data_ins);

        }
        public DataTable TL_Data_Report(string Ccode, string Lcode, string EmpNo, string Months, string FYear)
        {
            System.Data.SqlClient.SqlParameter[] Tm_Load_rep = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@Finance", FYear)
            };
            DataTable dt = SQL.ExecuteDatatable("TL_Data_Report", Tm_Load_rep);
            return dt;
        }
        public DataTable TL_Data_All_Report(string Ccode, string Lcode, string Months, string FYear)
        {
            System.Data.SqlClient.SqlParameter[] Tm_Load_rep_all = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),                
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@Finance", FYear),
               
            };
            DataTable dt = SQL.ExecuteDatatable("TL_Data_All_Report", Tm_Load_rep_all);
            return dt;
        }
        public DataTable Emp_Load_Worker_Incentive(string Ccode, string Lcode, string Department)
        {
            System.Data.SqlClient.SqlParameter[] Worker_Load = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),                
                new System.Data.SqlClient.SqlParameter("@Department", Department)               
            };
            DataTable dt = SQL.ExecuteDatatable("Emp_Load_Worker_Incentive", Worker_Load);
            return dt;
        }
        public void Emp_Load_Worker_Update(string Ccode, string Lcode, string EmpNo, string Checkval, string Department)
        {
            System.Data.SqlClient.SqlParameter[] Emp_worker_upd = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),                
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Department", Department),
                new System.Data.SqlClient.SqlParameter("@Checkbox_val", Checkval)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Emp_Load_Worker_Update", Emp_worker_upd);
        }
        public DataTable Worker_Incentive_Retrive(string Ccode, string Lcode, string Months, string Finance)
        {
            System.Data.SqlClient.SqlParameter[] Worker_Retrive = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),                
                new System.Data.SqlClient.SqlParameter("@Month", Months),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance)                
            };
            DataTable dt = SQL.ExecuteDatatable("Worker_Incentive_Retrive", Worker_Retrive);
            return dt;
        }
        public string Worker_Cal(string Ccode, string Lcode, string Days)
        {
            System.Data.SqlClient.SqlParameter[] Worker_cal1 = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Days", Days)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Worker_Cal", Worker_cal1);
            return str;
        }
        public void Work_Incentive_Insert(string Ccode, string Lcode, string EmpNo, string Days, string Months, string FinanceYear, string Amt)
        {
            System.Data.SqlClient.SqlParameter[] Worker_Ins = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Days", Days),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@Fyear", FinanceYear),
                new System.Data.SqlClient.SqlParameter("@Amt", Amt)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Work_Incentive_Insert", Worker_Ins);
        }
        public string Work_Incentive_Process(string Ccode, string Lcode, string EmpNo, string Months, string FinanceYear)
        {
            System.Data.SqlClient.SqlParameter[] Worker_Pro = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),                
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@Fyear", FinanceYear)                
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Work_Incentive_Process", Worker_Pro);
            return str;
        }
        public string Work_Incentive_Process_Edit(string Ccode, string Lcode, string EmpNo, string Months, string FinanceYear)
        {
            System.Data.SqlClient.SqlParameter[] Worker_Verify = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),                
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@Fyear", FinanceYear)                
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Work_Incentive_Process_Edit", Worker_Verify);
            return str;
        }
        public void Work_Incentive_Delete(string Ccode, string Lcode, string EmpNo, string Months, string FinanceYear)
        {
            System.Data.SqlClient.SqlParameter[] Worker_del = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),                
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@Fyear", FinanceYear)                
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Work_Incentive_Delete", Worker_del);
        }
        public DataTable Worker_Incentive_Report(string Ccode, string Lcode, string Months, string Finance)
        {
            System.Data.SqlClient.SqlParameter[] Worker_Inc_rep = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@months", Months),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance)
            };
            DataTable dt = SQL.ExecuteDatatable("Worker_Incentive_Report", Worker_Inc_rep);
            return dt;
        }
        public void Worker_incentive_confirm(string Ccode, string Lcode, string Months, string Finance)
        {
            System.Data.SqlClient.SqlParameter[] Worker_Inc_Con = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@months", Months),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Worker_incentive_confirm", Worker_Inc_Con);
        }
        public void TL_Confirm(string Ccode, string Lcode, string EmpNo, string Months, string FYear)
        {
            System.Data.SqlClient.SqlParameter[] TL_Inc_Con = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@Finance", FYear)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "TL_Confirm", TL_Inc_Con);
        }
        public DataTable Contract_Scheme()
        {
            System.Data.SqlClient.SqlParameter[] contract_scheme1 = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("Contract_Scheme", contract_scheme1);
            return dt;
        }
        public DataTable SalarySlab_Load(string SchemeName)
        {
            System.Data.SqlClient.SqlParameter[] SalarySlab_Load = 
            {
                new System.Data.SqlClient.SqlParameter("@ID", SchemeName)
            };
            DataTable dt = SQL.ExecuteDatatable("SalarySlab_Load", SalarySlab_Load);
            return dt;
        }
        public string Salaryslab_Verify(string Slabname, string Scheme)
        {
            System.Data.SqlClient.SqlParameter[] SalarySlab = 
            {
                new System.Data.SqlClient.SqlParameter("@SlabName", Slabname),
                new System.Data.SqlClient.SqlParameter("@Scheme", Scheme)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalarySlab_Verify", SalarySlab);
            return str;
        }
        public string SalarySlab_ID(string Schemename)
        {
            System.Data.SqlClient.SqlParameter[] ID_Salary = 
            {
                new System.Data.SqlClient.SqlParameter("@SchemeName", Schemename)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalarySlab_ID", ID_Salary);
            return str;
        }
        public void SalarySlab_Insert(SalarySlab objSalSlab)
        {
            System.Data.SqlClient.SqlParameter[] Slab = 
            {
                new System.Data.SqlClient.SqlParameter("@SchemeName", objSalSlab.SchmemeName),
                new System.Data.SqlClient.SqlParameter("@SlabName", objSalSlab.SlabName),
                new System.Data.SqlClient.SqlParameter("@Amount", objSalSlab.Amount),
                new System.Data.SqlClient.SqlParameter("@ID", objSalSlab.ID)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalarySlab_Insert", Slab);
        }
        public void SalarySlab_Update(string Schemename, string Amt, string ID, string SlabName)
        {
            System.Data.SqlClient.SqlParameter[] SalarySlab_Upd = 
            {
                new System.Data.SqlClient.SqlParameter("@Amt", Amt),
                new System.Data.SqlClient.SqlParameter("@SchemeName", Schemename ),
                new System.Data.SqlClient.SqlParameter("@SlabName", SlabName ),
                new System.Data.SqlClient.SqlParameter("@ID", ID )
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalarySlab_Update", SalarySlab_Upd);
        }
        public DataTable Contract_SalaryCal(string EmpNo, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Con_sal = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Contract_SalaryCal", Con_sal);
            return dt;
        }
        public string HostelIncentive_Verify(string Ccode, string Lcode, string Amt)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_inc = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Amt", Amt)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "HostelIncentive_Verify", Hostel_inc);
            return str;
        }

        public string CivilIncentive_Verify(string Ccode, string Lcode, string ElbDays, string Amt)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_inc = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@ElbDays", ElbDays),
                new System.Data.SqlClient.SqlParameter("@Amt", Amt)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "CivilIncentive_Verify", Hostel_inc);
            return str;
        }

        public void HostelAmt_Insert(string Ccode, string Lcode, string Amt, string Wdays, string HAllowed, string OTDays, string NFHDays, string CalWdays, string CalHAllowed, string CalOTDays, string CalNFHDays, string NFHWorkDays, string CalNFHWorkDays)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_Insert = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Amt", Amt),
                new System.Data.SqlClient.SqlParameter("@Wdays", Wdays),
                new System.Data.SqlClient.SqlParameter("@HAllowed", HAllowed),
                new System.Data.SqlClient.SqlParameter("@OTDays", OTDays),
                new System.Data.SqlClient.SqlParameter("@NFHDays", NFHDays),
                new System.Data.SqlClient.SqlParameter("@CalWdays", CalWdays),
                new System.Data.SqlClient.SqlParameter("@CalHAllowed", CalHAllowed),
                new System.Data.SqlClient.SqlParameter("@CalOTDays", CalOTDays),
                new System.Data.SqlClient.SqlParameter("@CalNFHDays", CalNFHDays),
                new System.Data.SqlClient.SqlParameter("@NFHWorkDays", NFHWorkDays),
                new System.Data.SqlClient.SqlParameter("@CalNFHWorkDays", CalNFHWorkDays)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "HostelAmt_Insert", Hostel_Insert);
        }

        public void CivilAmt_Insert(string Ccode, string Lcode, string ElbDays, string Amt)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_Insert = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@ElbDays", ElbDays),
                new System.Data.SqlClient.SqlParameter("@Amt", Amt)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "CivilAmt_Insert", Hostel_Insert);
        }

        public void HostelAmt_Update(string Ccode, string Lcode, string Amt, string Wdays, string HAllowed, string OTDays, string NFHDays, string CalWdays, string CalHAllowed, string CalOTDays, string CalNFHDays, string NFHWorkDays, string CalNFHWorkDays)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_Insert = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Amt", Amt),
                new System.Data.SqlClient.SqlParameter("@Wdays", Wdays),
                new System.Data.SqlClient.SqlParameter("@HAllowed", HAllowed),
                new System.Data.SqlClient.SqlParameter("@OTDays", OTDays),
                new System.Data.SqlClient.SqlParameter("@NFHDays", NFHDays),
                new System.Data.SqlClient.SqlParameter("@CalWdays", CalWdays),
                new System.Data.SqlClient.SqlParameter("@CalHAllowed", CalHAllowed),
                new System.Data.SqlClient.SqlParameter("@CalOTDays", CalOTDays),
                new System.Data.SqlClient.SqlParameter("@CalNFHDays", CalNFHDays),
                new System.Data.SqlClient.SqlParameter("@NFHWorkDays", NFHWorkDays),
                new System.Data.SqlClient.SqlParameter("@CalNFHWorkDays", CalNFHWorkDays)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "HostelAmt_Update", Hostel_Insert);
        }

        public void CivilAmt_Update(string Ccode, string Lcode, string ElbDays, string Amt)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_Insert = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@ElbDays", ElbDays),
                new System.Data.SqlClient.SqlParameter("@Amt", Amt)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "CivilAmt_Update", Hostel_Insert);
        }

        public DataTable HostelIncentive_Load(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_ld = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("HostelIncentive_Load", Hostel_ld);
            return dt;
        }
        public void HostelIncentive_Delete(string Ccode, string Lcode, string Amt)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_Delete = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Amt", Amt)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "HostelIncentive_Delete", Hostel_Delete);
        }
        public DataTable Hoste_Load_Employee(string Ccode, string Lcode, string Months, string Finance)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_Emp_load = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance),
            };
            DataTable dt = SQL.ExecuteDatatable("Hoste_Load_Employee", Hostel_Emp_load);
            return dt;
        }
        public string Hostel_AmtCount(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_count = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Hostel_AmtCount", Hostel_count);
            return str;
        }
        public void HostelIncentive_Data_insert(string Ccode, string Lcode, string EmpNo, string OldID, string NewID, string EmpName, string Days, string Amt, string Months, string Finance)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_Emp_ins = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
                new System.Data.SqlClient.SqlParameter("@oldid", OldID),
                new System.Data.SqlClient.SqlParameter("@newId", NewID),
                new System.Data.SqlClient.SqlParameter("@empName", EmpName),
                new System.Data.SqlClient.SqlParameter("@days", Days),
                new System.Data.SqlClient.SqlParameter("@Amt", Amt),
                new System.Data.SqlClient.SqlParameter("@Months", Months),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "HostelIncentive_Data_insert", Hostel_Emp_ins);
        }
        public string Hostel_data_verify(string Ccode, string Lcode, string EmpNo, string months, string Finance)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_Emp_ver = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),                
                new System.Data.SqlClient.SqlParameter("@Months", months),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Hostel_data_verify", Hostel_Emp_ver);
            return str;
        }
        public string Hostel_data_verify_1(string Ccode, string Lcode, string EmpNo, string months, string Finance)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_Emp_ver1 = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),                
                new System.Data.SqlClient.SqlParameter("@Months", months),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Hostel_data_verify_1", Hostel_Emp_ver1);
            return str;
        }
        public void Hostel_data_Delete(string Ccode, string Lcode, string EmpNo, string months, string Finance)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_Emp_del = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),                
                new System.Data.SqlClient.SqlParameter("@Months", months),
                new System.Data.SqlClient.SqlParameter("@Finance", Finance)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Hostel_data_Delete", Hostel_Emp_del);
        }
        public DataTable Hostel_Incentive_Data_report(string Ccode, string Lcode, string months, string Finance)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_Emp_rep = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Months", months),                                
                new System.Data.SqlClient.SqlParameter("@Finance", Finance)
            };
            DataTable dt = SQL.ExecuteDatatable("Hostel_Incentive_Data_report", Hostel_Emp_rep);
            return dt;
        }
        public void Hostel_Update(string Ccode, string Lcode, string months, string Finance)
        {
            System.Data.SqlClient.SqlParameter[] Hostel_Upd = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Months", months),                                
                new System.Data.SqlClient.SqlParameter("@Finance", Finance)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Hostel_Update", Hostel_Upd);
        }
        public void ESICode_Insert(string ESI)
        {
            System.Data.SqlClient.SqlParameter[] ESI_ins = 
            {
                new System.Data.SqlClient.SqlParameter("@ESIno", ESI)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "ESICode_Insert", ESI_ins);
        }
        public string ESICode_verify(string ESI)
        {
            System.Data.SqlClient.SqlParameter[] ESI_ver = 
            {
                new System.Data.SqlClient.SqlParameter("@ESIno", ESI)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "ESICode_verify", ESI_ver);
            return str;

        }
        public DataTable ESICode_GRID()
        {
            System.Data.SqlClient.SqlParameter[] esi_ld = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("ESICode_GRID", esi_ld);
            return dt;
        }
        public DataTable MstDepartment_Short(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] mst_Short = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("MstDepartment_Short", mst_Short);
            return dt;
        }
        public DataTable Slab_cal_salary(string Scheme)
        {
            System.Data.SqlClient.SqlParameter[] mst_sal = 
            {
                new System.Data.SqlClient.SqlParameter("@Scheme", Scheme)
            };
            DataTable dt = SQL.ExecuteDatatable("Slab_cal_salary", mst_sal);
            return dt;
        }
        public DataTable RptEmployeeMultipleDetails(string Query_Val)
        {
            System.Data.SqlClient.SqlParameter[] AllTypeEmployeeDetails =
            {
                new System.Data.SqlClient.SqlParameter("@Query",Query_Val)
            };
            DataTable dt = SQL.ExecuteDatatable("RptEmployeeDetailsMultiple_SP", AllTypeEmployeeDetails);
            return dt;
        }
        public DataTable Emp_Designation()
        {
            System.Data.SqlClient.SqlParameter[] Designation = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("DropDownDesignation_SP", Designation);
            return dt;
        }
        #region LoanDetails
        public DataTable LoanDisplay()
        {
            System.Data.SqlClient.SqlParameter[] Loandetails = 
            {

            };
            DataTable dt = SQL.ExecuteDatatable("LoanDisplay", Loandetails);
            return dt;
        }

        public void UpdateLoan(Bankloanclass objmas)
        {
            System.Data.SqlClient.SqlParameter[] LoanUpdate = 
            {
                new System.Data.SqlClient.SqlParameter("@LoanID", objmas.LoanID),
                new System.Data.SqlClient.SqlParameter("@LoanName", objmas.LoanName)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "LoanMaster_Update", LoanUpdate);
        }

        public void LoanMaster_Insert(Bankloanclass objmas)
        {
            System.Data.SqlClient.SqlParameter[] LoanInsert = 
            {
                new System.Data.SqlClient.SqlParameter("@LoanID", objmas.LoanID),
                new System.Data.SqlClient.SqlParameter("@LoanName", objmas.LoanName)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "LoanMaster_Insert", LoanInsert);
        }

        #endregion

        #region Reports
        public DataTable SearchPFNumberForForm5(string PFNumber)
        {
            System.Data.SqlClient.SqlParameter[] PFForm5 = 
            {
                new System.Data.SqlClient.SqlParameter("@PFNumber",PFNumber)
                //new System.Data.SqlClient.SqlParameter("@Month",Month),
                //new System.Data.SqlClient.SqlParameter("@Year",Year)

            };
            DataTable dt = SQL.ExecuteDatatable("RptSearchPFNumberForForm5_SP", PFForm5);
            return dt;
        }
        #endregion
        public void mst(sample objs)
        {
            System.Data.SqlClient.SqlParameter[] sam =
            {
                new System.Data.SqlClient.SqlParameter("@id",objs.Id),
                new System.Data.SqlClient.SqlParameter("@name",objs.Name),
                new System.Data.SqlClient.SqlParameter("@age",objs.Age),
                new System.Data.SqlClient.SqlParameter("@city",objs.City),
                new System.Data.SqlClient.SqlParameter("cellno",objs.Cellno),
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "sp_addsample", sam);



            //throw new NotImplementedException();
        }



        public DataTable Deptdrop1()
        {
            System.Data.SqlClient.SqlParameter[] sam =
           {

           };

            // DataTable dt = SQL.ExecuteDatatable("month_load", months);
            DataTable dt = SQL.ExecuteDatatable("DropDowndept_category_SP1", sam);
            return dt;
            // SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "DropDowndept_category_SP1", sam);

        }

        public DataTable Deptdrop_Worker_Incentive(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] da =
            {
                new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("Deptdrop_Worker_Incentive", da);
            return dt;
        }


        public DataTable sal_his_all_report(string Ccode, string Lcode, string FinancialYear)
        {
            System.Data.SqlClient.SqlParameter[] da =
            {
                new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",Lcode),
               
                new System.Data.SqlClient.SqlParameter("@FinancialYear",FinancialYear),
            };
            DataTable dt = SQL.ExecuteDatatable("sal_his_report", da);
            return dt;
        }

        public DataTable deptdeactive(string Ccode, string Deparment)
        {
            System.Data.SqlClient.SqlParameter[] dt =
            {
            new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
            new System.Data.SqlClient.SqlParameter("@Department", Deparment),
        };
            DataTable dd = SQL.ExecuteDatatable("sp_deptdeactive", dt);
            return dd;
        }

        public DataTable Load_Incentive_Details(string Ccode, string Lcode, string MonthDays)
        {
            System.Data.SqlClient.SqlParameter[] Lad_Incentive = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@MonthDays", MonthDays)
            };
            DataTable dt = SQL.ExecuteDatatable("Worker_Incentive_Load_for_SalaryCalculation", Lad_Incentive);
            return dt;
        }

        public string BsaicDetails_Verify(string Ccode, string Lcode, string Category, string EmployeeType)
        {
            System.Data.SqlClient.SqlParameter[] Basic_ver = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Category", Category),
                new System.Data.SqlClient.SqlParameter("@EmployeeType", EmployeeType)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "BasicDetails_Verify", Basic_ver);
            return str;
        }

        public void BsaicDetails_Insert_New(string Ccode, string Lcode, string Category, string EmployeeType, string BasicDA, string HRA, string ConvAllow, string EduAllow, string MediAllow, string RAI, string WashingAllow)
        {
            System.Data.SqlClient.SqlParameter[] Basic_ins = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Category", Category),
                new System.Data.SqlClient.SqlParameter("@EmployeeType", EmployeeType),
                new System.Data.SqlClient.SqlParameter("@BasicDA", BasicDA),
                new System.Data.SqlClient.SqlParameter("@HRA", HRA),
                new System.Data.SqlClient.SqlParameter("@ConvAllow", ConvAllow),
                new System.Data.SqlClient.SqlParameter("@EduAllow", EduAllow),
                new System.Data.SqlClient.SqlParameter("@MediAllow", MediAllow),
                new System.Data.SqlClient.SqlParameter("@RAI", RAI),
                new System.Data.SqlClient.SqlParameter("@WashingAllow", WashingAllow)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "BasicDetails_Insert", Basic_ins);
        }

        public void BsaicDetails_Update(string Ccode, string Lcode, string Category, string EmployeeType, string BasicDA, string HRA, string ConvAllow, string EduAllow, string MediAllow, string RAI, string WashingAllow)
        {
            System.Data.SqlClient.SqlParameter[] Basic_ins = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Category", Category),
                new System.Data.SqlClient.SqlParameter("@EmployeeType", EmployeeType),
                new System.Data.SqlClient.SqlParameter("@BasicDA", BasicDA),
                new System.Data.SqlClient.SqlParameter("@HRA", HRA),
                new System.Data.SqlClient.SqlParameter("@ConvAllow", ConvAllow),
                new System.Data.SqlClient.SqlParameter("@EduAllow", EduAllow),
                new System.Data.SqlClient.SqlParameter("@MediAllow", MediAllow),
                new System.Data.SqlClient.SqlParameter("@RAI", RAI),
                new System.Data.SqlClient.SqlParameter("@WashingAllow", WashingAllow)
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "BasicDetails_Update", Basic_ins);
        }

        public DataTable BsaicDetails_Load(string Ccode, string Lcode, string Category, string EmployeeType)
        {
            System.Data.SqlClient.SqlParameter[] Basic_load1 = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                new System.Data.SqlClient.SqlParameter("@Category", Category),
                new System.Data.SqlClient.SqlParameter("@EmployeeType", EmployeeType)
            };
            DataTable dt = SQL.ExecuteDatatable("Basic_Load", Basic_load1);
            return dt;
        }

        public DataTable GetMachineNo(string Machine, string Ccode, string Lcode)
        {
            //throw new NotImplementedException();
            System.Data.SqlClient.SqlParameter[] Get =
            {
            new System.Data.SqlClient.SqlParameter("@ExisistingCode",Machine),
            new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
            new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("GetMachineNo_SP", Get);
            return dt;
        }

        public string SalaryMasterUploadVerify(string empCode, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] SalaryMasterVerify = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpCode", empCode),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalaryMasterUpload_Verify", SalaryMasterVerify);
            return str;
        }

        public DataTable DeleteSM_Sp(string EmpNo, string Ccode, string Lcode)
        {
            //throw new NotImplementedException();

            System.Data.SqlClient.SqlParameter[] Del =
            {
            new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
            new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
            new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("DeleteSM_SP", Del);
            return dt;
        }

        public void SalaryMasterUpload_Insert(SalaryMasterClass objsal)
        {
            //throw new NotImplementedException();
            System.Data.SqlClient.SqlParameter[] SalaryMaster_Insert = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",objsal.EmpNo),
                new System.Data.SqlClient.SqlParameter("@ProfileType",objsal.ProfileType),
                new System.Data.SqlClient.SqlParameter("@BasicSalary", objsal.Basic),
                new System.Data.SqlClient.SqlParameter("@HRAPer",objsal.hraper),
                new System.Data.SqlClient.SqlParameter("@HRAamt",objsal.hraAmt),
                new System.Data.SqlClient.SqlParameter("@ConveyancePer",objsal.conveyanceper),
                new System.Data.SqlClient.SqlParameter("@ConveyanceAmt",objsal.conveyanceamt),
                new System.Data.SqlClient.SqlParameter("@CCAper",objsal.CCAper),
                new System.Data.SqlClient.SqlParameter("@CCAamt",objsal.CCaamt),
                new System.Data.SqlClient.SqlParameter("@Medicalper", objsal.medicalper),
                new System.Data.SqlClient.SqlParameter("@Medicalamt", objsal.medicalamt),
                new System.Data.SqlClient.SqlParameter("@Allowance1per", objsal.Allowance1per),
                new System.Data.SqlClient.SqlParameter("@Alllowance1amt", objsal.Allowance1Amt),
                new System.Data.SqlClient.SqlParameter("@Allowance2per", objsal.Allowance2per),
                new System.Data.SqlClient.SqlParameter("@Allowance2amt", objsal.Allowance2Amt),
                new System.Data.SqlClient.SqlParameter("@Allowance3per", objsal.Allowance3per),
                new System.Data.SqlClient.SqlParameter("@Allowance3amt", objsal.Allowance3Amt),

                new System.Data.SqlClient.SqlParameter("@Deduction1Amt", objsal.StaffDecution1),
                new System.Data.SqlClient.SqlParameter("@Deduction1Per", objsal.StaffDeduction1Per),
                new System.Data.SqlClient.SqlParameter("@Deduction2Amt", objsal.StaffDecution2),
                new System.Data.SqlClient.SqlParameter("@Deduction2Per", objsal.StaffDeduction2Per),
                new System.Data.SqlClient.SqlParameter("@Deduction3Amt", objsal.StaffDecution3),
                new System.Data.SqlClient.SqlParameter("@Deduction3Per", objsal.StaffDeduction3Per),
                new System.Data.SqlClient.SqlParameter("@NetPay", objsal.netPay),
                new System.Data.SqlClient.SqlParameter("@CreatedBy", objsal.username),
                new System.Data.SqlClient.SqlParameter("@DailySalary", objsal.dailySalary),
                new System.Data.SqlClient.SqlParameter("@Finance", objsal.Finance),
                new System.Data.SqlClient.SqlParameter("@FoodType", objsal.FoodType),
                new System.Data.SqlClient.SqlParameter("@Messdeduction", objsal.mess),
                new System.Data.SqlClient.SqlParameter("@hosteldeduction", objsal.Hostel),
                new System.Data.SqlClient.SqlParameter("@Deduction1", objsal.deduction1),
                new System.Data.SqlClient.SqlParameter("@Deduction2", objsal.deduction2),
                new System.Data.SqlClient.SqlParameter("@Deduction3", objsal.deduction3),
                new System.Data.SqlClient.SqlParameter("@LabourAllowance1", objsal.LabourAllowance1),
                new System.Data.SqlClient.SqlParameter("@LabourAllowance2", objsal.LabourAllowance2),
                 new System.Data.SqlClient.SqlParameter("@LabourAllowance3", objsal.LabourAllowance3),
                 new System.Data.SqlClient.SqlParameter("@Ccode", objsal.Ccode),
                 new System.Data.SqlClient.SqlParameter("@Lcode", objsal.Lcode),
                 new System.Data.SqlClient.SqlParameter("@Base", objsal.Base),
                 new System.Data.SqlClient.SqlParameter("@FDA", objsal.FDA),
                 new System.Data.SqlClient.SqlParameter("@VDA", objsal.VDA),
                 new System.Data.SqlClient.SqlParameter("@HRA", objsal.HRA),
                 new System.Data.SqlClient.SqlParameter("@Total", objsal.total),
                 new System.Data.SqlClient.SqlParameter("@Union", objsal.union),
                 new System.Data.SqlClient.SqlParameter("@PFS", objsal.PFsalary)

            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "SalaryMasterUpload_Insert", SalaryMaster_Insert);
        }

        public DataTable DeleOff_SP(string EmpNo, string Ccode, string Lcode)
        {
            //throw new NotImplementedException();
            System.Data.SqlClient.SqlParameter[] Del =
            {
            new System.Data.SqlClient.SqlParameter("@EmpNo",EmpNo),
            new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
            new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("DelOff_SP", Del);
            return dt;

        }

        public void OfficalProfile_Upload(OfficialprofileClass objOff)
        {
            System.Data.SqlClient.SqlParameter[] officialprofiel = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",objOff.EmpNo),
                new System.Data.SqlClient.SqlParameter("@MachineNo",objOff.Machine),
                new System.Data.SqlClient.SqlParameter("@Dateofjoining",objOff.Dateofjoining),
                new System.Data.SqlClient.SqlParameter("@Probationperiod",objOff.Probationperiod),
                new System.Data.SqlClient.SqlParameter("@Confirmationdate",objOff.Confirmationdate),
                new System.Data.SqlClient.SqlParameter("@Pannumber",objOff.Pannumber),
                new System.Data.SqlClient.SqlParameter("@ESICnumber",objOff.ESICnumber),
                new System.Data.SqlClient.SqlParameter("@PFnumber",objOff.PFnumber),
                new System.Data.SqlClient.SqlParameter("@Grade",objOff.Grade),               
                new System.Data.SqlClient.SqlParameter("@EmployeeType","0"),
                new System.Data.SqlClient.SqlParameter("@ReportingAuthorityname",objOff.ReportingAuthorityName),
                new System.Data.SqlClient.SqlParameter("@Insucompanyname",objOff.InsurancecompanyName),
                new System.Data.SqlClient.SqlParameter("@Insurancenumber",objOff.Insurancenumber),
                new System.Data.SqlClient.SqlParameter("@Eligibleforovertime",objOff.Eligibleforovertime),
                new System.Data.SqlClient.SqlParameter("@SalaryThroug",objOff.Salarythrough),
                new System.Data.SqlClient.SqlParameter("@ModeofPayement",objOff.modepaycheque),
                new System.Data.SqlClient.SqlParameter("@Banknamecheque",objOff.Banknamecheque),
                new System.Data.SqlClient.SqlParameter("@Enteramountforcheque","0"),
                new System.Data.SqlClient.SqlParameter("@BankAccno",objOff.BankACno),
                new System.Data.SqlClient.SqlParameter("@Accountholdername",objOff.Accountholdername),
                new System.Data.SqlClient.SqlParameter("@BankName",objOff.BankName),
                new System.Data.SqlClient.SqlParameter("@BranchName",objOff.Branchname),
                new System.Data.SqlClient.SqlParameter("@AccNo",objOff.BankACno),
                new System.Data.SqlClient.SqlParameter("@Enteramountfortransaction",objOff.Enteramountfortransaction),
                new System.Data.SqlClient.SqlParameter("@LabourType",objOff.LabourType),
                new System.Data.SqlClient.SqlParameter("@ContractType",objOff.ContractType),
                new System.Data.SqlClient.SqlParameter("@ExpiryDate",objOff.ExpiryDate),
                new System.Data.SqlClient.SqlParameter("@Wages",objOff.WagesType),
                new System.Data.SqlClient.SqlParameter("@FinancialYear",objOff.Financialperiod),
                new System.Data.SqlClient.SqlParameter("@CreateDate", objOff.CreatedDate),
                new System.Data.SqlClient.SqlParameter("@CreatedBy",objOff.CreatedBy),
                new System.Data.SqlClient.SqlParameter("@Modifiedby",objOff.Modifiedby),
                new System.Data.SqlClient.SqlParameter("@ModifiedOn",objOff.ModifiedOn),
                new System.Data.SqlClient.SqlParameter("@ContractPeriod",objOff.contractperiod),
                new System.Data.SqlClient.SqlParameter("@BasicSalary",objOff.BasicSalary),
                new System.Data.SqlClient.SqlParameter("@ElgibleESI",objOff.EligibleESI),
                new System.Data.SqlClient.SqlParameter("@ContractStartDate",objOff.ContractStartDate),
                new System.Data.SqlClient.SqlParameter("@ContractEndDate",objOff.ContractEndDate),
                new System.Data.SqlClient.SqlParameter("@Status",objOff.Status),
                new System.Data.SqlClient.SqlParameter("@ProfileType",objOff.ProfileType),
                new System.Data.SqlClient.SqlParameter("@EligiblePF", objOff.EligblePF),
                new System.Data.SqlClient.SqlParameter("@Ccode", objOff.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", objOff.Lcode),
                new System.Data.SqlClient.SqlParameter("@pf_type", objOff.PF_Type),
                new System.Data.SqlClient.SqlParameter("@chkMonths",objOff.ChkMonths),
                new System.Data.SqlClient.SqlParameter("@chkYears",objOff.ChkYears),
                new System.Data.SqlClient.SqlParameter("@Duration",objOff.Duration),
                new System.Data.SqlClient.SqlParameter("@ESICode",objOff.ESICode)



                   
            };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Officialprofile_SP_Upload", officialprofiel);
        }

    }
}

