﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using System.IO;


using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Text;

public partial class MainPage : System.Web.UI.MasterPage
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    OfficialprofileClass objOff = new OfficialprofileClass();
    EmployeeClass objEmp = new EmployeeClass();
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    // static string ExisitingNo = "";
    static string UserID = "", SesRoleCode = "", SessionAdmin = "";
    string SessionCcode;
    string SessionLcode;
    string SessionRights;
    string SessionUser;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        UserID = Session["UserId"].ToString();
        SesRoleCode = Session["RoleCode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUser = Session["Usernmdisplay"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        lblUserNameDisp.Text = SessionLcode + " - " + Session["Usernmdisplay"].ToString();

        if (!IsPostBack)
        {
            lblCompanyName.Text = SessionCcode + " - " + "STORES" + " - " + SessionFinYearVal;

            string SQL = "select ModuleLink from [KGPL_Rights]..Module_List where ModuleID='6'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SQL);
            if (dt.Rows.Count > 0)
            {
                MainPage_Link.Attributes["href"] = dt.Rows[0]["ModuleLink"].ToString();
            }

            Login_User_Photo_Load();
            if (SesRoleCode != "1")
            {
                //Menu_Rights_Check();
                //NonAdmin_User_Rights_Check();
                //Mnu_FinYear.Visible = false;
                //Mnu_NumberSetup.Visible = false;
                //Mnu_NewUser.Visible = false;
                //Mnu_AccessRights.Visible = false;
                //All_Module_Disable();
                //Non_Admin_ModuleLink_Check();
                NonAdmin_User_Rights_Check();
                //Mnu_User_Default_Page.Visible = false;
            }
            else
            {
                //Payroll_ModuleLink_Check();
                if (SessionUser == "Scoto")
                {
                    //All_Module_Disable();
                    //NonAdmin_User_Rights_Check();
                }
                else
                {
                    //All_Module_Disable();
                    //Admin_ModuleLink_Check();
                    Admin_User_Rights_Check();
                }


            }


        }
        
    }

    private void Login_User_Photo_Load()
    {
        //string file_path = "assets/images/Login_user_photo/";
        //file_path = file_path + Session["Lcode"].ToString() + "/";
        //file_path = file_path + Session["UserId"].ToString() + ".jpg";
        ////File Check
        //if (File.Exists(Server.MapPath(file_path)))
        //{
        //    EmpPhoto.ImageUrl = (file_path);
        //}
        //else
        //{
        //    EmpPhoto.ImageUrl = "assets/images/Login_user_photo/NoImage.gif";
        //}
    }
    private void Company_Logo_Load()
    {
        //    string file_path = "assets/images/common/logo.png";
        //    //File Check
        //    if (File.Exists(Server.MapPath(file_path)))
        //    {
        //        ImgCompanyLogo.ImageUrl = (file_path);
        //    }
        //    else
        //    {
        //        ImgCompanyLogo.ImageUrl = "assets/images/common/logo.png";
        //    }
    }

    private void Menu_Rights_Check()
    {
        string query = "";
        bool Mnu_Check_Bool = false;
        bool Mnu_Check_Purchase_bool = false;
        DataTable dt = new DataTable();
        //Admin Menu Check Start
        query = "Select * from MstModuleFormUserRights where ModuleID='2' And MenuID='2' And UserID='" + UserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0)
        {
            Mnu_AccessRights.Visible = false;

            query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
            query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
            query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
            query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
            query = query + " And UR.MenuID='2' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
            dt = objdata.RptEmployeeMultipleDetails(query);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["AddRights"].ToString() == "1" || dt.Rows[i]["ModifyRights"].ToString() == "1" || dt.Rows[i]["DeleteRights"].ToString() == "1" || dt.Rows[i]["ViewRights"].ToString() == "1")
                {
                    //Skip
                    Mnu_Check_Bool = true;
                }
                else
                {
                    ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible=false;
                }
            }
            if (Mnu_Check_Bool == false)
            {
                Link_Admin_Master.Visible = false;
            }
        }
        else
        {
            Link_Admin_Master.Visible = false;
        }
        //Admin Menu Check End

        //Master Menu Check Start
        Mnu_Check_Bool = false;
        query = "Select * from MstModuleFormUserRights where ModuleID='2' And MenuID='3' And UserID='" + UserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0)
        {
            query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
            query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
            query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
            query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
            query = query + " And UR.MenuID='3' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
            dt = objdata.RptEmployeeMultipleDetails(query);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["AddRights"].ToString() == "1" || dt.Rows[i]["ModifyRights"].ToString() == "1" || dt.Rows[i]["DeleteRights"].ToString() == "1" || dt.Rows[i]["ViewRights"].ToString() == "1")
                {
                    //Skip
                    Mnu_Check_Bool = true;
                }
                else
                {
                    ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible = false;
                }
            }
            if (Mnu_Check_Bool == false)
            {
                Link_Master.Visible = false;
            }
        }
        else
        {
            Link_Master.Visible = false;
        }
        //Master Menu Check End

        //Purchase Menu Check Start
        Mnu_Check_Bool = false;
        Mnu_Check_Purchase_bool = false;
        query = "Select * from MstModuleFormUserRights where ModuleID='2' And (MenuID='4' or MenuID='5' Or MenuID='6' Or MenuID='7') And UserID='" + UserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0)
        {
            //Transaction Menu Check Start
            query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
            query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
            query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
            query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
            query = query + " And UR.MenuID='4' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
            dt = objdata.RptEmployeeMultipleDetails(query);
            if (dt.Rows.Count != 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["AddRights"].ToString() == "1" || dt.Rows[i]["ModifyRights"].ToString() == "1" || dt.Rows[i]["DeleteRights"].ToString() == "1" || dt.Rows[i]["ViewRights"].ToString() == "1")
                    {
                        //Skip
                        Mnu_Check_Bool = true;
                        Mnu_Check_Purchase_bool = true;
                    }
                    else
                    {
                        ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible = false;
                    }
                }
                if (Mnu_Check_Bool == false)
                {
                    Link_Transaction.Visible = false;
                }
            }
            else
            {
                Link_Transaction.Visible = false;
            }
            //Transaction Menu Check End

            //Purchase Order Menu Check Start
            Mnu_Check_Bool = false;
            query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
            query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
            query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
            query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
            query = query + " And UR.MenuID='5' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
            dt = objdata.RptEmployeeMultipleDetails(query);
            if (dt.Rows.Count != 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["AddRights"].ToString() == "1" || dt.Rows[i]["ModifyRights"].ToString() == "1" || dt.Rows[i]["DeleteRights"].ToString() == "1" || dt.Rows[i]["ViewRights"].ToString() == "1")
                    {
                        //Skip
                        Mnu_Check_Bool = true;
                        Mnu_Check_Purchase_bool = true;
                    }
                    else
                    {
                        ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible = false;
                    }
                }
                if (Mnu_Check_Bool == false)
                {
                    Link_PurchaseOrder.Visible = false;
                }
            }
            else
            {
                Link_PurchaseOrder.Visible = false;
            }
            //Purchase Order Menu Check End

            //Purchase Receipt Menu Check Start
            Mnu_Check_Bool = false;
            query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
            query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
            query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
            query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
            query = query + " And UR.MenuID='6' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
            dt = objdata.RptEmployeeMultipleDetails(query);
            if (dt.Rows.Count != 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["AddRights"].ToString() == "1" || dt.Rows[i]["ModifyRights"].ToString() == "1" || dt.Rows[i]["DeleteRights"].ToString() == "1" || dt.Rows[i]["ViewRights"].ToString() == "1")
                    {
                        //Skip
                        Mnu_Check_Bool = true;
                        Mnu_Check_Purchase_bool = true;
                    }
                    else
                    {
                        ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible = false;
                    }
                }
                if (Mnu_Check_Bool == false)
                {
                    Link_PurchaseReceipt.Visible = false;
                }
            }
            else
            {
                Link_PurchaseReceipt.Visible = false;
            }
            //Purchase Receipt Menu Check End

            //Purchase Return,Job Order And Voucher Menu Check Start
            Mnu_Check_Bool = false;
            query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
            query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
            query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
            query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
            query = query + " And UR.MenuID='7' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
            dt = objdata.RptEmployeeMultipleDetails(query);
            if (dt.Rows.Count != 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["AddRights"].ToString() == "1" || dt.Rows[i]["ModifyRights"].ToString() == "1" || dt.Rows[i]["DeleteRights"].ToString() == "1" || dt.Rows[i]["ViewRights"].ToString() == "1")
                    {
                        //Skip
                        Mnu_Check_Bool = true;
                        Mnu_Check_Purchase_bool = true;
                    }
                    else
                    {
                        ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible = false;
                    }
                }
                if (Mnu_Check_Bool == false)
                {
                    Mnu_PurReturn.Visible = false;
                   
                }
            }
            else
            {
                Mnu_PurReturn.Visible = false;
               
            }
            //Purchase Return,Job Order And Voucher Menu Check End

            if (Mnu_Check_Purchase_bool == false)
            {
                Link_Purchase.Visible = false;
            }
        }
        else
        {
            Link_Purchase.Visible = false;
        }
        //Purchase Menu Check End

        //Inventory Menu Check Start
        Mnu_Check_Bool = false;
        query = "Select * from MstModuleFormUserRights where ModuleID='2' And MenuID='8' And UserID='" + UserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0)
        {
            query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
            query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
            query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
            query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
            query = query + " And UR.MenuID='8' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
            dt = objdata.RptEmployeeMultipleDetails(query);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["AddRights"].ToString() == "1" || dt.Rows[i]["ModifyRights"].ToString() == "1" || dt.Rows[i]["DeleteRights"].ToString() == "1" || dt.Rows[i]["ViewRights"].ToString() == "1")
                {
                    //Skip
                    Mnu_Check_Bool = true;
                }
                else
                {
                    ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible = false;
                }
            }
            if (Mnu_Check_Bool == false)
            {
                Link_Inventory.Visible = false;
            }
        }
        else
        {
            Link_Inventory.Visible = false;
        }
        //Inventory Menu Check End

        //Inventory Reports Menu Check Start
        Mnu_Check_Bool = false;
        query = "Select * from MstModuleFormUserRights where ModuleID='2' And MenuID='9' And UserID='" + UserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0)
        {
            query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
            query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
            query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
            query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
            query = query + " And UR.MenuID='9' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
            dt = objdata.RptEmployeeMultipleDetails(query);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["PrintRights"].ToString() == "1")
                {
                    //Skip
                    Mnu_Check_Bool = true;
                }
                else
                {
                   
                    ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible = false;
                }
            }
            if (Mnu_Check_Bool == false)
            {
                Link_RptInventory.Visible= false;
            }
        }
        else
        {
            Link_RptInventory.Visible = false;
        }
        //Inventory Reports Menu Check End


        //Purchase Reports Menu Check Start
        Mnu_Check_Bool = false;
        query = "Select * from MstModuleFormUserRights where ModuleID='2' And MenuID='10' And UserID='" + UserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0)
        {
            query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
            query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
            query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
            query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
            query = query + " And UR.MenuID='10' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
            dt = objdata.RptEmployeeMultipleDetails(query);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["PrintRights"].ToString() == "1")
                {
                    //Skip
                    Mnu_Check_Bool = true;
                }
                else
                {
                    ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible = false;
                }
            }
            if (Mnu_Check_Bool == false)
            {
                Link_RptPurchase.Visible = false;
            }
        }
        else
        {
            Link_RptPurchase.Visible = false;
        }
        //Purchase Reports Menu Check End

        //Stock Reports Menu Check Start
        Mnu_Check_Bool = false;
        query = "Select * from MstModuleFormUserRights where ModuleID='2' And MenuID='11' And UserID='" + UserID + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count != 0)
        {
            query = "Select MF.Link_ID,UR.FormID,UR.FormName,UR.AddRights,UR.ModifyRights,UR.DeleteRights,UR.ViewRights,";
            query = query + " UR.ApproveRights,UR.PrintRights from MstModuleFormUserRights UR";
            query = query + " inner join MstModuleFormDet MF on MF.ModuleID=UR.ModuleID And MF.MenuID=UR.MenuID And MF.FormID=UR.FormID";
            query = query + " where UR.Ccode='" + SessionCcode + "' And UR.Lcode='" + SessionLcode + "' And UR.UserID='" + UserID + "'";
            query = query + " And UR.MenuID='11' And UR.ModuleID='2' Order by UR.ModuleID,UR.MenuID,UR.FormID Asc";
            dt = objdata.RptEmployeeMultipleDetails(query);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["PrintRights"].ToString() == "1")
                {
                    //Skip
                    Mnu_Check_Bool = true;
                }
                else
                {
                    ((HtmlGenericControl)this.FindControl(dt.Rows[i]["Link_ID"].ToString())).Visible = false;
                }
            }
            if (Mnu_Check_Bool == false)
            {
                Link_RptStock.Visible = false;
            }
        }
        else
        {
            Link_RptStock.Visible = false;
        }
        //Stock Reports Menu Check End


    }


    protected void btnEditEnquiry_Grid_Click(object sender, EventArgs e)
    {
        //string query = "";
        //DataTable dt = new DataTable();
        //query = "Select FormName,Link_Url from MstUser_Register where UserID='" + UserID + "'";
        //query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        //dt = objdata.RptEmployeeMultipleDetails(query);

       
        //string formName = dt.Rows[0]["FormName"].ToString().Trim();
        //string link_Url = dt.Rows[0]["Link_Url"].ToString().Trim();


        //if (link_Url != "")
        //{
        //    Response.Redirect("~/" + link_Url + "");
        //}
        //else
        //{
        //    Response.Redirect("~/Dashboard.aspx");
        //}
    }

    private void NonAdmin_User_Rights_Check()
    {
        ALL_Menu_Header_Disable();
        ALL_Menu_Header_Forms_Disable();
        string ModuleID = Encrypt("2").ToString();
        string MenuID = "";
        DataTable DT_Head = new DataTable();
        DataTable DT_Form = new DataTable();
        //Check with Header Menu
        string query = "Select Distinct Menu_LI_ID,MenuID from [" + SessionRights + "]..Company_Module_User_Rights where ModuleID='" + ModuleID + "' And UserName='" + SessionUser + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT_Head = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
            this.FindControl(DT_Head.Rows[i]["Menu_LI_ID"].ToString()).Visible = true;

            MenuID = DT_Head.Rows[i]["MenuID"].ToString();
            query = "Select Distinct Form_LI_ID from [" + SessionRights + "]..Company_Module_User_Rights where ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "' And UserName='" + SessionUser + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT_Form = objdata.RptEmployeeMultipleDetails(query);
            for (int J = 0; J < DT_Form.Rows.Count; J++)
            {
                this.FindControl(DT_Form.Rows[J]["Form_LI_ID"].ToString()).Visible = true;
            }
        }
        
    }

    private void ALL_Menu_Header_Disable()
    {
        this.FindControl("Link_DashBoard").Visible = false;
        this.FindControl("Link_Admin_Master").Visible = false;
        this.FindControl("Link_Master").Visible = false;
        this.FindControl("Link_Purchase").Visible = false;
        this.FindControl("Link_Transaction").Visible = false;
        this.FindControl("Link_PurchaseOrder").Visible = false;
        this.FindControl("Link_PurchaseReceipt").Visible = false;
        this.FindControl("Link_Inventory").Visible = false;
        this.FindControl("Link_RptInventory").Visible = false;
        this.FindControl("Link_RptPurchase").Visible = false;
        this.FindControl("Link_RptStock").Visible = false;
    }
    private void ALL_Menu_Header_Forms_Disable()
    {
        this.FindControl("Link_DashBoard").Visible = false;

        //Admin Master
        this.FindControl("Mnu_Company").Visible = false;
        this.FindControl("Mnu_Location").Visible = false;
        this.FindControl("Mnu_NewUser").Visible = false;
        this.FindControl("Mnu_UserRights").Visible = false;
        this.FindControl("Mnu_FinYear").Visible = false;
        this.FindControl("Mnu_NumberSetup").Visible = false;
        
        this.FindControl("Mnu_Company_Module_Rights").Visible = false;
        this.FindControl("Mnu_Company_Module_MenuHead_Rights").Visible = false;
        this.FindControl("Mnu_Module_Form_Rights").Visible = false;
        this.FindControl("Mnu_AccessRights").Visible = false;


        //Master
        this.FindControl("Mnu_Department").Visible = false;
        this.FindControl("Mnu_Supplier").Visible = false;
        this.FindControl("Mnu_BankMaster").Visible = false;
        this.FindControl("Mnu_Transport").Visible = false;
        this.FindControl("Mnu_ItemMst").Visible = false;
        this.FindControl("Mnu_AccHead").Visible = false;
        this.FindControl("Mnu_Employee").Visible = false;
        this.FindControl("Mnu_CostCenter").Visible = false;
        this.FindControl("Mnu_ItemVsSupp").Visible = false;
        this.FindControl("Mnu_WarehouseVsZone").Visible = false;
        this.FindControl("Mnu_DeliveryMode").Visible = false;
        this.FindControl("Mnu_ItemMasterUpload").Visible = false;
        this.FindControl("Mnu_OPT_StockUpload").Visible = false;
        this.FindControl("Mnu_OPTStock_Apprv").Visible = false;

        this.FindControl("Mnu_ItemType").Visible = false;
        this.FindControl("Mnu_Division").Visible = false;
        this.FindControl("Mnu_TaxMst").Visible = false;

        //Purchase
        this.FindControl("Mnu_PurReturn").Visible = false;
        this.FindControl("Mnu_Supp_Payments").Visible = false;
        this.FindControl("Mnu_Supp_Advance").Visible = false;

        //Transaction
        this.FindControl("Mnu_PurEnq").Visible = false;
        this.FindControl("Mnu_SuppQuot").Visible = false;
        this.FindControl("Mnu_PurRequest").Visible = false;
        this.FindControl("Mnu_PurRequestAmt").Visible = false;
        this.FindControl("Mnu_PurRequestApproval").Visible = false;
        this.FindControl("Mnu_PurRequestApproved").Visible = false;

        //Purchase Order
        this.FindControl("Mnu_StdPO").Visible = false;
        this.FindControl("Mnu_StdPOAmt").Visible = false;
        this.FindControl("Mnu_GeneralPO").Visible = false;
        this.FindControl("Mnu_BlanketPO").Visible = false;
        this.FindControl("Mnu_PurRequestStandard").Visible = false;
        this.FindControl("Mnu_StdPur_Approve").Visible = false;
        this.FindControl("Mnu_StdPur_Approved").Visible = false;

        //Purchase Receipt
        this.FindControl("Mnu_POReceipt").Visible = false;
        this.FindControl("Mnu_OpeningPO").Visible = false;
        this.FindControl("Mnu_UnPlannedPO").Visible = false;

        //Inventory
        this.FindControl("Mnu_MatRequest").Visible = false;
        this.FindControl("Mnu_MatIssue").Visible = false;
        this.FindControl("Mnu_IssueReturn").Visible = false;
        this.FindControl("Mnu_MatStockTransfer").Visible = false;
        this.FindControl("Mnu_GatePassIN").Visible = false;
        this.FindControl("Mnu_GatePassOut").Visible = false;
        this.FindControl("Mnu_AdjustStockEntry").Visible = false;
        this.FindControl("Mnu_AdjustStockApprove").Visible = false;
        this.FindControl("Mnu_DamageEntry").Visible = false;
        this.FindControl("Mnu_SparesReSales").Visible = false;
        this.FindControl("Mnu_UnitToUnitStockTransfer").Visible = false;
        this.FindControl("Mnu_Genset").Visible = false;
        this.FindControl("Mnu_ScrabEntry").Visible = false;
        this.FindControl("Mnu_ScrabReuse").Visible = false;
        this.FindControl("Mnu_Service_Entry").Visible = false;
        this.FindControl("Mnu_NonStockScrab").Visible = false;
        
        //Inventory Report
        this.FindControl("Rpt_MatrIss_Slip").Visible = false;
        this.FindControl("Rpt_MatrIssRet_Slip").Visible = false;
        this.FindControl("Rpt_MatReq").Visible = false;
        this.FindControl("Rpt_GPOutDet").Visible = false;
        this.FindControl("Rpt_GPINDet").Visible = false;
        this.FindControl("Rpt_GPNonRetDet").Visible = false;
        this.FindControl("Rpt_GPOutRet_Pend").Visible = false;
        this.FindControl("Rpt_Stock_Adjustment").Visible = false;
        this.FindControl("Rpt_TopMovingIssuedItemDetails").Visible = false;
        this.FindControl("Rpt_Costing").Visible = false;
        this.FindControl("Rpt_DamageMaterialStock").Visible = false;
        this.FindControl("Rpt_SparesReSales").Visible = false;
        this.FindControl("Rpt_GensetExpenseDetails").Visible = false;
        this.FindControl("Rpt_ScrabItemCostDetails").Visible = false;
        this.FindControl("Rpt_Reuse").Visible = false;
        this.FindControl("Rpt_NSSReq").Visible = false;
        this.FindControl("Rpt_Report_Service_Entry").Visible = false;
        
        //Purchase Report
        this.FindControl("Rpt_GenPurOrd_Det").Visible = false;
        this.FindControl("Rpt_GenReceipt_Det").Visible = false;
        this.FindControl("Rpt_Supp_Payment_Det").Visible = false;
        this.FindControl("Rpt_Supp_Advance_Det").Visible = false;
        this.FindControl("Rpt_Purchase_Enquiry").Visible = false;
        this.FindControl("Rpt_SupplierQut").Visible = false;
        this.FindControl("Rpt_PurReq").Visible = false;
        this.FindControl("Rpt_PurReq_Amend").Visible = false;
        this.FindControl("Rpt_StdPurOdr_Det").Visible = false;
        this.FindControl("Rpt_OpnRec_Det").Visible = false;
        this.FindControl("Rpt_BlanktPO_Det").Visible = false;
        this.FindControl("Rpt_POReceipt_Det").Visible = false;
        this.FindControl("Rpt_PurRet_Det").Visible = false;
        this.FindControl("Rpt_PO_Pending").Visible = false;
        this.FindControl("Rpt_MaxPurchaseItemDetails").Visible = false;

        //Stock Report
        this.FindControl("Rpt_StockTran_Det").Visible = false;
        this.FindControl("Rpt_StockDet").Visible = false;
        this.FindControl("Rpt_ClosingStock").Visible = false;
        this.FindControl("Rpt_SingleItem_Det").Visible = false;
        this.FindControl("Rpt_DeptPur_Issue").Visible = false;
        this.FindControl("Rpt_UnitToUnitStockTran_Det").Visible = false;

    }

    private void Admin_User_Rights_Check()
    {
        ALL_Menu_Header_Disable();
        ALL_Menu_Header_Forms_Disable();
        string ModuleID = Encrypt("2").ToString();
        string MenuID = "";
        DataTable DT_Head = new DataTable();
        DataTable DT_Form = new DataTable();
        //Check with Header Menu
        string query = "Select Distinct Menu_LI_ID,MenuID from [" + SessionRights + "]..Company_Module_MenuHead_Rights where ModuleID='" + ModuleID + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT_Head = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
            this.FindControl(DT_Head.Rows[i]["Menu_LI_ID"].ToString()).Visible = true;

            MenuID = DT_Head.Rows[i]["MenuID"].ToString();
            query = "Select Distinct Form_LI_ID from [" + SessionRights + "]..Company_Module_Menu_Form_Rights where ModuleID='" + ModuleID + "' And MenuID='" + MenuID + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT_Form = objdata.RptEmployeeMultipleDetails(query);
            for (int J = 0; J < DT_Form.Rows.Count; J++)
            {
                this.FindControl(DT_Form.Rows[J]["Form_LI_ID"].ToString()).Visible = true;
            }
        }
        
    }

    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    private string Decrypt(string cipherText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

    protected void btnModule_Home_Click(object sender, ImageClickEventArgs e)
    {
        //Get Cotton Link
        DataTable DT_Link = new DataTable();
        string query = "Select * from [" + SessionRights + "]..Module_List where ModuleID='6'";
        DT_Link = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Link.Rows.Count != 0)
        {
            string Link_Open = DT_Link.Rows[0]["ModuleLink"].ToString();
            query = "Delete from [" + SessionRights + "]..Module_Open_User";
            objdata.RptEmployeeMultipleDetails(query);
            //Get user Password
            query = "Select * from [" + SessionRights + "]..MstUsers where UserCode='" + SessionUser.Trim() + "' and IsAdmin='" + SessionAdmin + "' And LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
            DT_Link = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Link.Rows.Count != 0)
            {
                string Password_Str = DT_Link.Rows[0]["Password"].ToString();
                //Insert User Details
                query = "Insert Into [" + SessionRights + "]..Module_Open_User(CompCode,LocCode,UserName,Password,Fin_Year,Fin_Year_Value) Values";
                query = query + " ('" + SessionCcode + "','" + SessionLcode + "','" + SessionUser + "','" + Password_Str + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "')";
                objdata.RptEmployeeMultipleDetails(query);
                Response.Redirect(Link_Open);
            }

        }
    }
}
