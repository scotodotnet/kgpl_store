﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Net.Mail;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Weekly_Maill : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SSQL = "";
    ReportDocument RD_Report = new ReportDocument();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    bool Errflag;
    string FromDate = ""; string ToDate = "";
    DateTime frmDate_Date; DateTime toDate_Date;
    DataTable AutoDataTable = new DataTable();
    DataTable GenReceiptDT = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = "krishna";
        SessionLcode = "UNIT I";
        SessionFinYearCode = "2";
        SessionFinYearVal = "2017_2018";
        DateTime Date1 = DateTime.Now.AddDays(-1);
        DateTime Date2 = DateTime.Now.AddDays(-1);
        FromDate = Convert.ToString(Date1.ToString("dd/MM/yyyy"));
        ToDate = Convert.ToString(Date2.ToString("dd/MM/yyyy"));
        //FromDate = "01/02/2017";
        //ToDate = "28/02/2017";
       // ClosingStock();
       // Sample_Purchase_Report();
        Stock_details();

    }
    public void Stock_details()
    {
        DataTable StockDT = new DataTable();
        DataTable Department_DT = new DataTable();
        DataTable Item_DT = new DataTable();
        DataTable Open_Stck = new DataTable();
        DataTable Purchase_DT = new DataTable();
        DataTable Issue_DT = new DataTable();
        DataTable ZoneBin_DT = new DataTable();
        DataTable Item_UOM = new DataTable();
        bool Errflag_Fromdate = false;
        string Heading = "";
        DataTable da_check_Dept = new DataTable();
        DataTable da_Check_ItemName = new DataTable();

        SSQL = "delete from Print_stock_details ";
        da_Check_ItemName = objdata.RptEmployeeMultipleDetails(SSQL);

        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("CompanyCode");
        AutoDataTable.Columns.Add("LocationCode");
        AutoDataTable.Columns.Add("Address1");
        AutoDataTable.Columns.Add("Address2");
        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("ItemCode");
        AutoDataTable.Columns.Add("ItemName");
        AutoDataTable.Columns.Add("QTY");
        AutoDataTable.Columns.Add("Rate");
        AutoDataTable.Columns.Add("Amount");
        if (FromDate != "")
        {
            frmDate_Date = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate_Date = Convert.ToDateTime(ToDate);
        }

        string[] finyear = SessionFinYearVal.Split('_');
        string FromDate_Chk = "01/04/" + finyear[0];
        DateTime fromDate_Chk = Convert.ToDateTime(FromDate_Chk);


        string[] FromDate_ChkStr = FromDate.Split('/');

        string DateFormat = FromDate + "   ToDate :" + ToDate;



        SSQL = "select distinct DeptName from Stock_Transaction_Ledger ";
        SSQL = SSQL + " where (Trans_Type ='UNPLANNED RECEIPT' or Trans_Type ='OPENING RECEIPT')";
        SSQL = SSQL + " and  Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        SSQL = SSQL + "  And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' ";
        da_check_Dept = objdata.RptEmployeeMultipleDetails(SSQL);

        if (da_check_Dept.Rows.Count != 0)
        {
            string DeptName_val = "";
            string DeptCode = "";
            for (int k = 0; k < da_check_Dept.Rows.Count; k++)
            {
                DeptName_val = da_check_Dept.Rows[k]["DeptName"].ToString();

                SSQL = "select distinct ItemName from Stock_Transaction_Ledger";
                SSQL = SSQL + " where (Trans_Type ='UNPLANNED RECEIPT' or Trans_Type ='OPENING RECEIPT')";
                SSQL = SSQL + " and DeptName='" + DeptName_val + "'";
                SSQL = SSQL + " and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                SSQL = SSQL + "  And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' ";
                da_Check_ItemName = objdata.RptEmployeeMultipleDetails(SSQL);

                for (int j = 0; j < da_Check_ItemName.Rows.Count; j++)
                {
                    DataTable da_Val = new DataTable();
                    SSQL = "select Distinct ItemCode,ItemName  ,SUM(Add_Qty) - SUM(Minus_Qty)  as QTY, ";
                    SSQL = SSQL + " SUM(Add_Value)-SUM(Minus_Value) as Amount from Stock_Ledger_All where ";
                    //SSQL = SSQL + "  (Trans_Type ='UNPLANNED RECEIPT' or Trans_Type ='OPENING RECEIPT')";
                    SSQL = SSQL + "  ItemName='" + da_Check_ItemName.Rows[j]["ItemName"].ToString() + "'";
                    SSQL = SSQL + " and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    SSQL = SSQL + "  And FinYearCode='" + SessionFinYearCode + "' And FinYearVal='" + SessionFinYearVal + "' ";
                    SSQL = SSQL + " group by ItemCode,ItemName";
                    da_Val = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (da_Val.Rows.Count != 0)
                    {
                        DataTable da_chek_print = new DataTable();


                        SSQL = "select * from Print_stock_details where ItemName='" + da_Check_ItemName.Rows[j]["ItemName"].ToString() + "'";
                        da_chek_print = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (da_chek_print.Rows.Count == 0)
                        {
                            SSQL = "insert into Print_stock_details(Ccode,LCode,DeptName,ItemCode,ItemName,Qty,Amount)";
                            SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + DeptName_val + "',";
                            SSQL = SSQL + " '" + da_Val.Rows[0]["ItemCode"].ToString() + "','" + da_Val.Rows[0]["ItemName"].ToString() + "',";
                            SSQL = SSQL + " '" + da_Val.Rows[0]["QTY"].ToString() + "','" + da_Val.Rows[0]["Amount"].ToString() + "')";
                            da_chek_print = objdata.RptEmployeeMultipleDetails(SSQL);
                        }


                    }

                }
            }

            DataTable da_check_val = new DataTable();
            SSQL = "select * from Print_stock_details where Qty<>'0' ";
            da_check_val = objdata.RptEmployeeMultipleDetails(SSQL);
            if (da_check_val.Rows.Count != 0)
            {
                for (int j = 0; j < da_check_val.Rows.Count; j++)
                {
                    DataTable dtdCompanyAddress = new DataTable();
                    SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);


                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();
                    AutoDataTable.Rows[j]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                    AutoDataTable.Rows[j]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                    AutoDataTable.Rows[j]["CompanyCode"] = SessionCcode;
                    AutoDataTable.Rows[j]["LocationCode"] = SessionLcode;
                    AutoDataTable.Rows[j]["DeptName"] = da_check_val.Rows[j]["DeptName"];

                    AutoDataTable.Rows[j]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                    AutoDataTable.Rows[j]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();
                    AutoDataTable.Rows[j]["ItemCode"] = da_check_val.Rows[j]["ItemCode"]; ;
                    AutoDataTable.Rows[j]["ItemName"] = da_check_val.Rows[j]["ItemName"]; ;
                    AutoDataTable.Rows[j]["QTY"] = da_check_val.Rows[j]["QTY"];
                    AutoDataTable.Rows[j]["Amount"] = da_check_val.Rows[j]["Amount"];
                }
                Heading = Convert.ToString(frmDate_Date.ToString("dd/MM/yyyy"));
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(AutoDataTable);
                RD_Report.Load(Server.MapPath("~/crystal/Sample_StockDetailsStatement.rpt"));
                RD_Report.DataDefinition.FormulaFields["ReportHeading"].Text = "'Date:" + Heading + "'";
                RD_Report.SetDataSource(ds1.Tables[0]);
              //  RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                DataTable Close_Det = new DataTable();

                //SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                //dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);
                //if (dtdCompanyAddress.Rows.Count != 0)
                //{
                //    RD_Report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + dtdCompanyAddress.Rows[0]["Cname"].ToString() + "'";
                //    RD_Report.DataDefinition.FormulaFields["Address1"].Text = "'" + dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString() + "'";
                //    RD_Report.DataDefinition.FormulaFields["Address2"].Text = "'" + dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString() + "'";
                //}
                //RD_Report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                //Sent Mail Code Start
                MailMessage mail = new MailMessage();
               // mail.To.Add("kalyan@scoto.in");
                mail.To.Add("srikrishnatexsss@gmail.com");
                mail.To.Add("srikrishatexmd@gmail.com");
                mail.Bcc.Add("padmanabhanscoto@gmail.com");
                mail.From = new MailAddress("aatm2005@gmail.com");
                mail.Subject = "Stock Details Reports:" + FromDate + "";
               // mail.Body = "Stock Details :'" + Heading + "'";
                mail.IsBodyHtml = true;

                //mail.Attachments.Add(new Attachment(ms, "DayWise1.rpt", "crystal/DayWise1.rpt"));
                mail.Attachments.Add(new Attachment(RD_Report.ExportToStream(ExportFormatType.PortableDocFormat), "Stock Details Report.pdf"));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
                smtp.Credentials = new System.Net.NetworkCredential("aatm2005@gmail.com", "Altius2005");  //Or your Smtp Email ID and Password
                smtp.EnableSsl = true;
                smtp.Send(mail);
                //Sent Mail Code End
                lblUploadSuccessfully.Text = "Mail Sent Successfully";
              
            }
            else
            {

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
                lblUploadSuccessfully.Text = "No Records Matched..";
            }



        }
        else
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }

    }

    public void Sample_Purchase_Report()
    {
        
        DataTable AutoDTGenReceipt = new DataTable();
        string Heading = "ALL";

        if (FromDate != "")
        {
            frmDate_Date = Convert.ToDateTime(FromDate);
        }
        if (ToDate != "")
        {
            toDate_Date = Convert.ToDateTime(ToDate);
        }

        string[] finyear = SessionFinYearVal.Split('_');
        string FromDate_Chk = "01/04/" + finyear[0];
        DateTime fromDate_Chk = Convert.ToDateTime(FromDate_Chk);


        string[] FromDate_ChkStr = FromDate.Split('/');

        string DateFormat = FromDate + "   ToDate :" + ToDate;


        SSQL = "select UPM.UnPlan_Recp_No,UPM.UnPlan_Recp_Date,UPM.Supp_Name,UPM.InvNo,UPM.InvDate,";
        SSQL = SSQL + "UPM.DCNo,UPM.DCDate,UPM.Description,UPM.TotalAmt,UPM.AddOrLess,UPM.NetAmount,";
        SSQL = SSQL + "UPS.ItemCode,UPS.ItemName,UPS.ValuationType,UPS.UOMCode,UPS.DeptName,UPS.WarehouseName,";
        SSQL = SSQL + "UPS.ZoneName,UPS.BinName,UPS.ReceivedQty,UPS.PrevRate,UPS.ItemRate,";
        SSQL = SSQL + "UPS.ItemTotal,UPS.Discount_Per,UPS.Discount,UPS.TaxPer,UPS.TaxAmount,UPS.BDUTaxPer,UPS.BDUTaxAmount,UPS.OtherCharge,UPS.LineTotal";
        SSQL = SSQL + " from Unplanned_Receipt_Main UPM inner join Unplanned_Receipt_Main_Sub UPS ";
        SSQL = SSQL + "on UPM.UnPlan_Recp_No=UPS.UnPlan_Recp_No where UPM.Ccode='" + SessionCcode + "' And UPM.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And UPM.FinYearCode='" + SessionFinYearCode + "' And UPS.Ccode='" + SessionCcode + "'";
        SSQL = SSQL + " And UPS.Lcode='" + SessionLcode + "' And UPS.FinYearCode='" + SessionFinYearCode + "' And UPM.UnPlan_PO_Receipt_Status='1' ";
        DateFormat = FromDate + "   ToDate :" + ToDate;

        SSQL = SSQL + " And CONVERT(DATETIME,UPM.UnPlan_Recp_Date, 103)>=CONVERT(DATETIME,'" + frmDate_Date.ToString("dd/MM/yyyy") + "',103) And CONVERT(DATETIME,UPM.UnPlan_Recp_Date, 103)<=CONVERT(DATETIME,'" + toDate_Date.ToString("dd/MM/yyyy") + "',103)";
        Heading = "FromDate : " + DateFormat;
        


        SSQL = SSQL + " order by  UnPlan_Recp_Date";

        GenReceiptDT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (GenReceiptDT.Rows.Count > 0)
        {


            DataTable dtdCompanyAddress = new DataTable();
            SSQL = "select Cname,location,Address1,Address2,Pincode,State From AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dtdCompanyAddress = objdata.RptEmployeeMultipleDetails(SSQL);

            AutoDTGenReceipt.Columns.Add("CompanyName");
            AutoDTGenReceipt.Columns.Add("LocationName");
            AutoDTGenReceipt.Columns.Add("CompanyCode");
            AutoDTGenReceipt.Columns.Add("LocationCode");
            AutoDTGenReceipt.Columns.Add("Address1");
            AutoDTGenReceipt.Columns.Add("Address2");
            AutoDTGenReceipt.Columns.Add("UnPlanRecpNo");
            AutoDTGenReceipt.Columns.Add("UnPlanRecpDate");
            AutoDTGenReceipt.Columns.Add("SuppName");
            AutoDTGenReceipt.Columns.Add("InvNo");
            AutoDTGenReceipt.Columns.Add("InvDate");
            AutoDTGenReceipt.Columns.Add("DCNo");
            AutoDTGenReceipt.Columns.Add("DCDate");
            AutoDTGenReceipt.Columns.Add("WarehouseName");
            AutoDTGenReceipt.Columns.Add("Description");

            AutoDTGenReceipt.Columns.Add("ItemCode");
            AutoDTGenReceipt.Columns.Add("ItemName");
            AutoDTGenReceipt.Columns.Add("ValuationType");
            AutoDTGenReceipt.Columns.Add("UomCode");
            AutoDTGenReceipt.Columns.Add("DeptName");
            AutoDTGenReceipt.Columns.Add("ZoneName");
            AutoDTGenReceipt.Columns.Add("BinName");
            AutoDTGenReceipt.Columns.Add("ReceivedQty");
            AutoDTGenReceipt.Columns.Add("PrevRate");
            AutoDTGenReceipt.Columns.Add("ItemRate");
            AutoDTGenReceipt.Columns.Add("ItemTotal");
            AutoDTGenReceipt.Columns.Add("Discount_Per");
            AutoDTGenReceipt.Columns.Add("Discount");
            AutoDTGenReceipt.Columns.Add("TaxPer");
            AutoDTGenReceipt.Columns.Add("TaxAmount");
            AutoDTGenReceipt.Columns.Add("BDUTaxPer");
            AutoDTGenReceipt.Columns.Add("BDUTaxAmount");
            AutoDTGenReceipt.Columns.Add("OtherCharge");
            AutoDTGenReceipt.Columns.Add("LineTotal");
            AutoDTGenReceipt.Columns.Add("TotalAmt");

            AutoDTGenReceipt.Columns.Add("AddOrLess");
            AutoDTGenReceipt.Columns.Add("NetAmount");


            string Receipt_No = "";
            for (int i = 0; i < GenReceiptDT.Rows.Count; i++)
            {
                AutoDTGenReceipt.NewRow();
                AutoDTGenReceipt.Rows.Add();

                AutoDTGenReceipt.Rows[i]["CompanyName"] = dtdCompanyAddress.Rows[0]["Cname"].ToString();
                AutoDTGenReceipt.Rows[i]["LocationName"] = dtdCompanyAddress.Rows[0]["location"].ToString();
                AutoDTGenReceipt.Rows[i]["CompanyCode"] = SessionCcode;
                AutoDTGenReceipt.Rows[i]["LocationCode"] = SessionLcode;
                AutoDTGenReceipt.Rows[i]["Address1"] = dtdCompanyAddress.Rows[0]["Address1"].ToString() + "," + dtdCompanyAddress.Rows[0]["Address2"].ToString();
                AutoDTGenReceipt.Rows[i]["Address2"] = dtdCompanyAddress.Rows[0]["Pincode"].ToString() + "," + dtdCompanyAddress.Rows[0]["State"].ToString();

                Receipt_No = GenReceiptDT.Rows[i]["UnPlan_Recp_No"].ToString();

                AutoDTGenReceipt.Rows[i]["UnPlanRecpNo"] = GenReceiptDT.Rows[i]["UnPlan_Recp_No"];
                AutoDTGenReceipt.Rows[i]["UnPlanRecpDate"] = GenReceiptDT.Rows[i]["UnPlan_Recp_Date"];
                AutoDTGenReceipt.Rows[i]["SuppName"] = GenReceiptDT.Rows[i]["Supp_Name"];
                AutoDTGenReceipt.Rows[i]["InvNo"] = GenReceiptDT.Rows[i]["InvNo"];
                AutoDTGenReceipt.Rows[i]["InvDate"] = GenReceiptDT.Rows[i]["InvDate"];
                AutoDTGenReceipt.Rows[i]["DCNo"] = GenReceiptDT.Rows[i]["DCNo"];
                AutoDTGenReceipt.Rows[i]["DCDate"] = GenReceiptDT.Rows[i]["DCDate"];
                AutoDTGenReceipt.Rows[i]["WarehouseName"] = GenReceiptDT.Rows[i]["WarehouseName"];
                AutoDTGenReceipt.Rows[i]["Description"] = GenReceiptDT.Rows[i]["Description"];


                AutoDTGenReceipt.Rows[i]["ItemCode"] = GenReceiptDT.Rows[i]["ItemCode"];
                AutoDTGenReceipt.Rows[i]["ItemName"] = GenReceiptDT.Rows[i]["ItemName"];
                AutoDTGenReceipt.Rows[i]["ValuationType"] = GenReceiptDT.Rows[i]["ValuationType"];
                AutoDTGenReceipt.Rows[i]["UomCode"] = GenReceiptDT.Rows[i]["UOMCode"];
                AutoDTGenReceipt.Rows[i]["DeptName"] = GenReceiptDT.Rows[i]["DeptName"];
                AutoDTGenReceipt.Rows[i]["ZoneName"] = GenReceiptDT.Rows[i]["ZoneName"];
                AutoDTGenReceipt.Rows[i]["BinName"] = GenReceiptDT.Rows[i]["BinName"];
                AutoDTGenReceipt.Rows[i]["ReceivedQty"] = GenReceiptDT.Rows[i]["ReceivedQty"];
                AutoDTGenReceipt.Rows[i]["PrevRate"] = GenReceiptDT.Rows[i]["PrevRate"];
                AutoDTGenReceipt.Rows[i]["ItemRate"] = GenReceiptDT.Rows[i]["ItemRate"];
                AutoDTGenReceipt.Rows[i]["ItemTotal"] = GenReceiptDT.Rows[i]["ItemTotal"];
                AutoDTGenReceipt.Rows[i]["Discount_Per"] = GenReceiptDT.Rows[i]["Discount_Per"];
                AutoDTGenReceipt.Rows[i]["Discount"] = GenReceiptDT.Rows[i]["Discount"];
                AutoDTGenReceipt.Rows[i]["TaxPer"] = GenReceiptDT.Rows[i]["TaxPer"];
                AutoDTGenReceipt.Rows[i]["TaxAmount"] = GenReceiptDT.Rows[i]["TaxAmount"];
                AutoDTGenReceipt.Rows[i]["BDUTaxPer"] = GenReceiptDT.Rows[i]["BDUTaxPer"];
                AutoDTGenReceipt.Rows[i]["BDUTaxAmount"] = GenReceiptDT.Rows[i]["BDUTaxAmount"];
                AutoDTGenReceipt.Rows[i]["OtherCharge"] = GenReceiptDT.Rows[i]["OtherCharge"];
                AutoDTGenReceipt.Rows[i]["LineTotal"] = GenReceiptDT.Rows[i]["LineTotal"];
                AutoDTGenReceipt.Rows[i]["TotalAmt"] = GenReceiptDT.Rows[i]["TotalAmt"];

                AutoDTGenReceipt.Rows[i]["AddOrLess"] = GenReceiptDT.Rows[i]["AddOrLess"];
                AutoDTGenReceipt.Rows[i]["NetAmount"] = GenReceiptDT.Rows[i]["NetAmount"];

            }

            DataSet ds = new DataSet();

            ds.Tables.Add(AutoDTGenReceipt);
            ReportDocument report = new ReportDocument();

            report.Load(Server.MapPath("~/crystal/SamplePurchaseDetails_Sorting.rpt"));
            report.DataDefinition.FormulaFields["ReportHeading"].Text = "'" + Heading + "'";
            report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            //Sent Mail Code Start
            MailMessage mail = new MailMessage();
            mail.To.Add("kalyan@scoto.in");
            //mail.To.Add("srikrishnatexsss@gmail.com");
            //mail.Bcc.Add("padmanabhanscoto@gmail.com");
            mail.From = new MailAddress("aatm2005@gmail.com");
            mail.Subject = "Purchase Details Reports:" + FromDate + "";
            // mail.Body = "Stock Details :'" + Heading + "'";
            mail.IsBodyHtml = true;

            //mail.Attachments.Add(new Attachment(ms, "DayWise1.rpt", "crystal/DayWise1.rpt"));
            mail.Attachments.Add(new Attachment(report.ExportToStream(ExportFormatType.PortableDocFormat), "Purchase Details Report.pdf"));
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
            smtp.Credentials = new System.Net.NetworkCredential("aatm2005@gmail.com", "Altius2005");  //Or your Smtp Email ID and Password
            smtp.EnableSsl = true;
            smtp.Send(mail);
            //Sent Mail Code End
            lblUploadSuccessfully.Text = "Mail Sent Successfully";




        }

        else
        {
            Errflag = false;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
        }
        if (!Errflag)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Matched..');", true);
            lblUploadSuccessfully.Text = "No Records Matched..";
        }


    }
}
