﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Security.Permissions;
using System.Configuration;
using System.Web.Caching;
using System.Data;
using System.Diagnostics;
using System.IO;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Default2 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    protected void Page_Load(object sender, EventArgs e)
    {
        //SqlDependency.Start(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
    }

    protected void enableTable_Click(object sender, EventArgs e)
    {
        string query = "Delete from Alert_Check";
        objdata.RptEmployeeMultipleDetails(query);

        Process p = new Process();
        p.StartInfo.FileName = "E:\\Projects\\Salary_Cover_Open\\Salary_Cover_EXE_Open\\Mail_Sent_File_Open\\bin\\Debug\\Mail_Sent_File_Open.exe";
        p.Start();

        RegisterNotification();
    }






    void Initialization()
    {
        // Create a dependency connection.
        
        SqlDependency.Start(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString, "queueName");
    }

    void RegisterNotification()
    {
        //Get Machine ID
        DataTable DT_Check = new DataTable();
        string query = "Select * from Alert_Check";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);

        if (DT_Check.Rows.Count != 0)
        {
            TextBox1.Text = DT_Check.Rows[0]["MachineID"].ToString();
            label1.Text = DT_Check.Rows[0]["MachineID"].ToString();
            //upnl.Update();

            string file_path = "assets/images/Salary_Cover/";
            file_path = file_path + DT_Check.Rows[0]["MachineID"].ToString() + ".jpg";
            //File Check
            if (File.Exists(Server.MapPath(file_path)))
            {
                EmpPhoto.ImageUrl = (file_path);
            }
            else
            {
                EmpPhoto.ImageUrl = "assets/images/Login_user_photo/NoImage.gif";
            }
            upnl.Update();


        }
        else
        {
            TextBox1.Text = "No";
            label1.Text = "No";
            upnl.Update();
        }

        // Assume connection is an open SqlConnection.
        string CS = "Data source=SABA-PC;Initial Catalog=ERP_Stores; User Id=sa; Password=vls@123;";
        SqlConnection cn = null;
        cn = new SqlConnection(CS);

        SqlDataAdapter dad = new SqlDataAdapter("Select [MachineID] from Alert_Check Order by MachineID", cn);

        SqlDependency dependency = new SqlDependency(dad.SelectCommand);
        dependency.OnChange += this.sqlDependency_OnChange;
        SqlDependency.Start(CS);

        DataSet ds = new DataSet();
        dad.Fill(ds);
    }

    // Handler method
    private void sqlDependency_OnChange(object sender, SqlNotificationEventArgs e)
    {
        //this.Invoke(new UICallback(FillGrid));
        if (e.Info == SqlNotificationInfo.Insert)
        {
            TextBox1.Text = "Insert";
        }

        
        RegisterNotification();
        // Handle the event (for example, invalidate this cache entry).
    }

    void Termination()
    {
        // Release the dependency.
        //SqlDependency.Stop(connectionString, queueName);
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    
}
