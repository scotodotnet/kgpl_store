﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class master_forms_item_vs_supplier : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Item VS Supplier";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();

            //string handler = ClientScript.GetPostBackEventReference(this.btnSearch_Supp, "");
            //txtSupp_Code.Attributes.Add("onblur", handler);    
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        Load_OLD_data();
        Load_Data_Empty_Supp1();
        Load_Data_Empty_ItemCode();
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;
        //check with Supplier Code And Name 
        query = "Select * from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And SuppCode='" + txtSupp_Code.Text + "' And SuppName='" + txtSupp_Name.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Supplier Details..');", true);
        }
        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Item Vs Supplier");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Supplier Vs Item Details...');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Item Vs Supplier");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Supplier Vs Item...');", true);
            }
        }
        //User Rights Check End

        if (!ErrFlag)
        {
            query = "Select * from MstSupplier_Vs_Item where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And SuppCode='" + txtSupp_Code.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from MstSupplier_Vs_Item where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And SuppCode='" + txtSupp_Code.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Insert
            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into MstSupplier_Vs_Item(Ccode,Lcode,SuppCode,SuppName,ItemCode,ItemShortName,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + txtSupp_Code.Text + "','" + txtSupp_Name.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["ItemName"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Vs Item Master Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Supplier Vs Item Master Details Updated Successfully');", true);
            }
            Clear_All_Field();
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtSupp_Code.Text = ""; txtSupp_Name.Text = ""; txtItemCode.Text = ""; txtItemName.Text = "";
        Initial_Data_Referesh();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        //check with Item Code And Item Name 
        query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCode.Text + "' And ItemShortName='" + txtItemName.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
        }

        

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCode.Text.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["ItemCode"] = txtItemCode.Text;
                    dr["ItemName"] = txtItemName.Text;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    txtItemCode.Text = ""; txtItemName.Text = "";
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["ItemCode"] = txtItemCode.Text;
                dr["ItemName"] = txtItemName.Text;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                txtItemCode.Text = ""; txtItemName.Text = "";
            }

            
        }

    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;
        
        //dt = Repeater1.DataSource;
    }


    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count ; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;        
        Load_OLD_data();

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    //protected void btnSearch_Supp_Click(object sender, EventArgs e)
    //{
    //    //Search Item Master
    //    DataTable dt = new DataTable();
    //    string query = "";
    //    query = "Select ItemCode,ItemShortName as ItemName from MstSupplier_Vs_Item where Ccode='" + SessionCcode + "'";
    //    query = query + " And Lcode='" + SessionLcode + "' And SuppCode='" + txtSupp_Code.Text + "' And SuppName='" + txtSupp_Name.Text + "'";
    //    dt = objdata.RptEmployeeMultipleDetails(query);
    //    ViewState["ItemTable"] = dt;
    //    Repeater1.DataSource = dt;
    //    Repeater1.DataBind();

    //    txtSupp_Name.Focus();
    //}


    protected void btnSupp1_Click(object sender, EventArgs e)
    {
        modalPop_Supp1.Show();
    }
    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier1.DataSource = DT;
        Repeater_Supplier1.DataBind();
        Repeater_Supplier1.Visible = true;

    }

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
        txtSupp_Code.Text = Convert.ToString(e.CommandArgument);
        txtSupp_Name.Text = Convert.ToString(e.CommandName);


        DataTable dt = new DataTable();
        string query = "";
        query = "Select ItemCode,ItemShortName as ItemName from MstSupplier_Vs_Item where Ccode='" + SessionCcode + "'";
        query = query + " And Lcode='" + SessionLcode + "' And SuppCode='" + txtSupp_Code.Text + "' And SuppName='" + txtSupp_Name.Text + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        ViewState["ItemTable"] = dt;
        Repeater1.DataSource = dt;
        Repeater1.DataBind();

        txtSupp_Name.Focus();


    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCode.Text = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }



}
