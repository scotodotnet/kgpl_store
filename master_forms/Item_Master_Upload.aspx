﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Item_Master_Upload.aspx.cs" Inherits="master_forms_Item_Master_Upload" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
           
                
        
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
    <div class="page-inner">
           
            
          <div class="col-md-9">
			<div class="panel panel-white">
		    	<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Item Master Upload Details</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
				    
				    
				    <div class="row">
					        <div class="form-group col-md-3">
					   <asp:FileUpload ID="FileUpload" runat="server" class="btn btn-default btn-rounded" Text="Upload Details" />
					</div>
					</div>
				    <div class="clearfix"></div>				    
				   
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                     
                                  <asp:Button ID="btnupload" class="btn btn-success"  runat="server" 
                                   Text="Upload" onclick="btnupload_Click" />  
                                   
                    </div>
                       <!-- Button end -->
					 
					 <div class="txtcenter">
                        <asp:Label ID="lblUploadSuccessfully" runat="server" Font-Bold="True" 
                            Font-Size="Larger" ForeColor="Red"></asp:Label>
                                 
                    </div>
					 
                </div> <!-- Panel-Body End -->
			</form>
			
		   </div><!-- panel white end -->
	    </div><!-- col-9 end -->
		    
	           <div class="col-md-2"></div>
		    
            
                <!-- Dashboard start -->
                <div class="col-lg-3 col-md-6">
        <div class="panel panel-white" style="height: 100%;">
            <div class="panel-heading">
                <h4 class="panel-title">Dashboard Details</h4>
                <div class="panel-control">
                    
                    
                </div>
            </div>
            <div class="panel-body">
                
                
            </div>
        </div>
    </div>  
                        
                <div class="col-lg-3 col-md-6">
        <div class="panel panel-white">
            <div class="panel-body">
                <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                   
                </div>
            </div>
        </div>
        <div class="panel panel-white">
            <div class="panel-body">
                <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                    
                    
                    
                </div>
            </div>
        </div>
    </div> 
                <!-- Dashboard end -->
                             
              <div class="col-md-2"></div>
                        
             </div>     <!-- col 12 end -->
           </div><!-- row end -->
        </div>
 
    </div>
 </ContentTemplate>
    <Triggers>
       <%-- <asp:AsyncPostBackTrigger ControlID = "Btupload"
          EventName = "Click" />--%>
        <asp:PostBackTrigger ControlID = "btnupload" />
    </Triggers>
</asp:UpdatePanel>
</asp:Content>

