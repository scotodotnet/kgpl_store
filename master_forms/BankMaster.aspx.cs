﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class master_forms_BankMaster : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Bank Details";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        Load_Data();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;
        
        if (!ErrFlag)
        {
            query = "Select * from MstBank where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And BankName='" + txtBankName.Text + "' And BranchName='" + txtBranchName.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                if (btnSave.Text == "Update")
                {
                    SaveMode = "Update";
                    query = "Delete from MstBank where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And BankName='" + txtBankName.Text + "' And BranchName='" + txtBranchName.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    SaveMode = "Error";

                }

            }
            if (SaveMode != "Error")
            {
                //Insert Compnay Details
                query = "Insert Into MstBank(Ccode,Lcode,BankName,BranchName,AccountName,AccountNo,UserID,Username)";
                query = query + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtBankName.Text + "',";
                query = query + "'" + txtBranchName.Text + "','" + txtAccountName.Text + "','" + txtAccountNo.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Bank Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Bank Details Updated Successfully');", true);
                }


                Clear_All_Field();
                btnSave.Text = "Save";
                Load_Data();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Bank Details Already Saved');", true);
            }
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtBankName.Text = ""; txtBranchName.Text = ""; txtAccountName.Text = ""; txtAccountNo.Text = "";
        btnSave.Text = "Save";
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstBank where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And BankName='" + txtBankName.Text + "' And BranchName='" + txtBranchName.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtAccountName.Text = DT.Rows[0]["AccountName"].ToString();
            txtAccountNo.Text = DT.Rows[0]["AccountNo"].ToString();
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }
    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstBank where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtBankName.Text = e.CommandName.ToString();
        txtBranchName.Text = e.CommandArgument.ToString();
        btnSearch_Click(sender, e);
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;

        

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from MstBank where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And BankName='" + e.CommandName.ToString() + "' And BranchName='" + e.CommandArgument.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                query = "Delete from MstBank where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And BankName='" + e.CommandName.ToString() + "' And BranchName='" + e.CommandArgument.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Bank Details Deleted Successfully');", true);
                Load_Data();
                Clear_All_Field();

            }
        }
    }
}
