﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="OpeningUpload_Details.aspx.cs" Inherits="OpeningUpload_Details" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
   <script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
   <link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<%--<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>

<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <h4><li class="active">Openining Upload Details</li></h4> 
    </ol>
</div>
                --%>
                


<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
           
                
        
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
    <div class="page-inner">
           
            
          <div class="col-md-9">
			<div class="panel panel-white">
		    	<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Opening Stock Upload</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
				    
				    
				    <div class="row">
					        <div class="form-group col-md-3">
					   <asp:FileUpload ID="fileUpload" runat="server" class="btn btn-default btn-rounded" Text="Upload Details" />
					</div>
					</div>
				    <div class="clearfix"></div>				    
				   
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                     
                                  <asp:Button ID="Btupload" class="btn btn-success"  runat="server" 
                                   Text="Upload" onclick="btnUpload_Click"/>  
                    </div>
                    <!-- Button end -->
                    <div class="txtcenter">
                        <asp:Label ID="lblUploadSuccessfully" runat="server" Font-Bold="True" 
                            Font-Size="Larger" ForeColor="Red"></asp:Label>
                                 
                    </div>
                    
                    
                       
					 
                </div> <!-- Panel-Body End -->
			</form>
			
		   </div><!-- panel white end -->
	    </div><!-- col-9 end -->
		    
	           <div class="col-md-2"></div>
		    
            
                <!-- Dashboard start -->
                <div class="col-lg-3 col-md-6">
        <div class="panel panel-white" style="height: 100%;">
            <div class="panel-heading">
                <h4 class="panel-title">Dashboard Details</h4>
                <div class="panel-control">
                    
                    
                </div>
            </div>
            <div class="panel-body">
                
                
            </div>
        </div>
    </div>  
                        
                <div class="col-lg-3 col-md-6">
        <div class="panel panel-white">
            <div class="panel-body">
                <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                   
                </div>
            </div>
        </div>
        <div class="panel panel-white">
            <div class="panel-body">
                <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                    
                    
                    
                </div>
            </div>
        </div>
    </div> 
                <!-- Dashboard end -->
                             
              <div class="col-md-2"></div>
                        
             </div>     <!-- col 12 end -->
           </div><!-- row end -->
        </div>
 
    </div>
 </ContentTemplate>
    <Triggers>
       <%-- <asp:AsyncPostBackTrigger ControlID = "Btupload"
          EventName = "Click" />--%>
        <asp:PostBackTrigger ControlID = "Btupload" />
    </Triggers>
</asp:UpdatePanel>
 
</asp:Content>

