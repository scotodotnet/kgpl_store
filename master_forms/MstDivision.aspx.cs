﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class master_forms_MstDivision : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string Dept_Code_Delete = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Division";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");
            Load_Data();
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        Load_Data();
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;
        //if (btnSave.Text == "Update")
        //{
        //    Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Department Details...');", true);
        //    }
        //}
        //else
        //{
        //    Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Department");
        //    if (Rights_Check == false)
        //    {
        //        ErrFlag = true;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Department...');", true);
        //    }
        //}
        //User Rights Check End
        if (!ErrFlag)
        {
            query = "Select * from MstDivision where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DivisionCode='" + txtDivisionCode.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                if (btnSave.Text == "Update")
                {
                    SaveMode = "Update";
                    query = "Delete from MstDivision where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DivisionCode='" + txtDivisionCode.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    SaveMode = "Error";
                }
            }

            if (SaveMode != "Error")
            {
                //Insert Compnay Details
                query = "Insert Into MstDivision(Ccode,Lcode,DivisionCode,Division,UserID,Username)";
                query = query + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtDivisionCode.Text + "',";
                query = query + "'" + txtDivision.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Division Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Division Details Updated Successfully');", true);
                }


                Clear_All_Field();
                btnSave.Text = "Save";
                txtDivisionCode.Enabled = true;
                Load_Data();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Division Details Already Saved');", true);
            }
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtDivisionCode.Text = "";
        txtDivisionCode.Enabled = true;
        txtDivision.Text = "";
        btnSave.Text = "Save";

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstDivision where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DivisionCode='" + txtDivisionCode.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtDivisionCode.Text = DT.Rows[0]["DivisionCode"].ToString();
            txtDivision.Text = DT.Rows[0]["Division"].ToString();

            txtDivisionCode.Enabled = false;
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }


    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstDivision where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "dataTable_Reload();", true);
        //GVDepartment.DataSource = DT;
        //GVDepartment.DataBind();


    }




    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtDivisionCode.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;


        if (!ErrFlag)
        {
            DataTable dtdcheckGeneral = new DataTable();
            DataTable dtdcheckBlanket = new DataTable();
            DataTable dtdcheckStandard = new DataTable();



            DataTable DT = new DataTable();
            query = "Select * from MstDivision where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DivisionCode='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                query = "Delete from MstDivision where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DivisionCode='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Division Details Deleted Successfully');", true);
                Load_Data();
                Clear_All_Field();

            }

        }
    }

}
