﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class OpeningUpload_Details : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionOpenReceiptNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string Auto_Transaction_No;
    string Opening_Receipt_Date;
    string WareHouseCode;
    string CostcenterName;
    string CostcenterCode;
    string DeptCode;
    string DeptName;
    string ItemCode;
    string ItemCode1;
    string query;
    string Opening_Receipt_No;
    string UOM_Code_Str;
    string Valida;
    string ValuationType;
    string Opening_Receipt_Date1;

    string WarehouseName;
    string ZoneName;
    string BinName;
    string ItemName;
    
    string OpeningQty;
    string OpeningValue;



    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        lblUploadSuccessfully.Text = "";

        if (fileUpload.HasFile)
        {
        }
        else
        {
            lblUploadSuccessfully.Text = "";
        }

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Opening Stock";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
            //Initial_Data_Referesh();
            if (Session["OP_Recp_No"] == null)
            {
                SessionOpenReceiptNo = "";
            }
            else
            {
                SessionOpenReceiptNo = Session["OP_Recp_No"].ToString();
                //txtReceiptNo.Text = SessionOpenReceiptNo;
                //btnSearch_Click(sender, e);
            }
        }
        //Load_OLD_data();
    }

    private void Stock_Add()
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();

        query = "Select * from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + Opening_Receipt_No.ToString() + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            query = "Delete from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + Opening_Receipt_No.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }

        DateTime date1 = Convert.ToDateTime(Opening_Receipt_Date.ToString());

        string date2 = date1.ToString("dd/MM/yyyy");

        //Insert Stock_Transaction_Ledger
       
            query = "Insert Into Stock_Transaction_Ledger(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
            query = query + " Trans_Type,DeptCode,DeptName,CostCenterCode,CostCenterName,ItemCode,ItemName,Open_Qty,Open_Value,PO_Qty,PO_Value,General_Qty,General_Value,Issue_Qty,";
            query = query + " Issue_Value,Issue_Return_Qty,Issue_Return_Value,GP_IN_Qty,GP_IN_Value,GP_OUT_Qty,GP_OUT_Value,Transfer_Qty,Transfer_Value,WarehouseCode,WarehouseName,ZoneName,BinName,Supp_Code,";
            query = query + " Supp_Name,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
            query = query + " '" + SessionFinYearVal + "','" + Opening_Receipt_No.ToString() + "','" + date1.ToString("MM/dd/yyyy") + "','" + date1.ToString("dd/MM/yyyy") + "','OPENING RECEIPT','" + DeptCode + "','" + DeptName + "','" + CostcenterCode + "','" + CostcenterName + "',";
            query = query + " '" + ItemCode + "','" + ItemName + "',";
            query = query + " '" + OpeningQty + "','" + OpeningValue + "','0.0','0.0','0.0','0.0','0.0',";
            query = query + " '0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','" + WareHouseCode + "','" + WarehouseName + "','" + ZoneName + "',";
            query = query + " '" + BinName + "','','','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            CommonClass_Function.cls_Trans_Type = "OPENING RECEIPT";
            CommonClass_Function.cls_ItemCode = ItemCode.ToString();
            CommonClass_Function.cls_ItemName = ItemName.ToString();
            CommonClass_Function.cls_Add_Qty = OpeningQty.ToString();
            CommonClass_Function.cls_Add_Value = OpeningValue.ToString();
            CommonClass_Function.cls_Minus_Qty = "0.0";
            CommonClass_Function.cls_Minus_Value = "0.0";


            CommonClass_Function.cls_DeptCode = DeptCode.ToString();
            CommonClass_Function.cls_DeptName = DeptName.ToString();
            CommonClass_Function.cls_CostCenterCode = CostcenterCode.ToString();
            CommonClass_Function.cls_CostCenterName = CostcenterName.ToString();
            CommonClass_Function.cls_WarehouseCode = WareHouseCode.ToString();
            CommonClass_Function.cls_WarehouseName = WarehouseName.ToString();
            CommonClass_Function.cls_ZoneName = ZoneName.ToString();
            CommonClass_Function.cls_BinName = BinName.ToString();
            CommonClass_Function.cls_Supp_Code = "";
            CommonClass_Function.cls_Supp_Name = "";
            CommonClass_Function.cls_UserID = SessionUserID;
            CommonClass_Function.cls_UserName = SessionUserName;
            //CommonClass_Function.cls_Stock_Qty = "";
            //CommonClass_Function.cls_Stock_Value = "";

            CommonClass_Function.Stock_Ledger_And_Current_Stock_Add(SessionCcode, SessionLcode, SessionFinYearCode, SessionFinYearVal,Opening_Receipt_No,date2);

    }


    protected void btnUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (fileUpload.HasFile)
        {
            fileUpload.SaveAs(Server.MapPath("Upload/" + fileUpload.FileName));
            string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + fileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
            OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
            DataTable dts = new DataTable();
            using (sSourceConnection)
            {
                sSourceConnection.Open();
                OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                sSourceConnection.Close();
                using (OleDbCommand cmd = new OleDbCommand())
                {
                    command.CommandText = "Select * FROM [Sheet1$]";
                    sSourceConnection.Open();
                }
                using (OleDbDataReader dr = command.ExecuteReader())
                {

                    if (dr.HasRows)
                    {

                    }

                }
                OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                objDataAdapter.SelectCommand = command;
                DataSet ds = new DataSet();

                objDataAdapter.Fill(ds);
                DataTable dt = new DataTable();
                dt = ds.Tables[0];
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                }
                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    SqlConnection cn = new SqlConnection(constr);
                    DeptName = dt.Rows[j]["DeptName"].ToString();
                    CostcenterName = dt.Rows[j]["CostcenterName"].ToString();
                    WarehouseName = dt.Rows[j]["WarehouseName"].ToString();
                    ZoneName = dt.Rows[j]["ZoneName"].ToString();
                    BinName = dt.Rows[j]["BinName"].ToString();
                    //ItemCode1 = Convert.ToString(dt.Rows[j]["ItemCode"]);
                    //ItemCode = ItemCode1.ToString();
                    ItemName = dt.Rows[j]["ItemName"].ToString();
                    
                     OpeningQty = dt.Rows[j]["OpeningQty"].ToString();
                     OpeningValue = dt.Rows[j]["OpeningValue"].ToString();
                     if (ItemCode == "18 - B")
                     {
                         ItemCode = "18 - B";
                     }

                    string query = "";
                    DataTable DT_Check = new DataTable();
                    DataTable qry_dt = new DataTable();
                    string SaveMode = "Insert";



                    //Check DeptName
                    if (DeptName.ToString() == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add DeptName..');", true);
                    }
                    //Check CostcenterName
                    if (CostcenterName.ToString() == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add CostcenterName..');", true);
                    }

                    //Check WareHouse Name
                    if (WarehouseName.ToString() == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add WarehouseName..');", true);
                    }

                    //Check ZoneName
                    if (ZoneName.ToString() == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add ZoneName..');", true);
                    }

                    //Check BinName
                    if (BinName.ToString() == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add BinName..');", true);
                    }

                    ////Check ItemCode
                    //if (ItemCode.ToString() == "")
                    //{
                    //    ErrFlag = true;
                    //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add ItemCode..');", true);
                    //}

                    //Check ItemName
                    if (ItemName.ToString() == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add ItemName..');", true);
                    }

                    //Check Opening Quantity
                    if (OpeningQty.ToString() == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add Opening Quantity..');", true);
                    }

                    //Check Opening Value
                    if (OpeningValue.ToString() == "")
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add Opening Value..');", true);
                    }


                    //Check Warehouse Name
                    query = "Select WarehouseCode from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseName='" + WarehouseName.ToString() + "'";
                    qry_dt = objdata.RptEmployeeMultipleDetails(query);
                    WareHouseCode = qry_dt.Rows[0]["WarehouseCode"].ToString();
                    if (qry_dt.Rows.Count == 0)
                    {

                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Warehouse Name...');", true);
                    }


                    //Check Dept Name
                    query = "Select DeptCode from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptName='" + DeptName.ToString() + "'";
                    qry_dt = objdata.RptEmployeeMultipleDetails(query);
                    DeptCode = qry_dt.Rows[0]["DeptCode"].ToString();
                    if (qry_dt.Rows.Count == 0)
                    {

                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Dept Name...');", true);
                    }


                    //Check Costcenter Name
                    query = "Select CostcenterCode from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CostcenterName='" + CostcenterName.ToString() + "'";
                    qry_dt = objdata.RptEmployeeMultipleDetails(query);
                    CostcenterCode = qry_dt.Rows[0]["CostcenterCode"].ToString();
                    if (qry_dt.Rows.Count == 0)
                    {

                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with CostCenter Name...');", true);
                    }


                    //Check Item Name
                    query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemShortName='" + ItemName.ToString().Trim() + "'";
                    query = query + " And DeptName='" + DeptName.ToString() + "'";
                    qry_dt = objdata.RptEmployeeMultipleDetails(query);

                    UOM_Code_Str = qry_dt.Rows[0]["PurchaseUOM"].ToString();
                    ItemCode = qry_dt.Rows[0]["ItemCode"].ToString();
                    ValuationType = qry_dt.Rows[0]["ValuationType"].ToString();
                    qry_dt = objdata.RptEmployeeMultipleDetails(query);
                    if (qry_dt.Rows.Count == 0)
                    {

                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Item Name Not Exist...');", true);
                    }


                    //Auto generate Transaction Function Call


                    if (!ErrFlag)
                    {
                        TransactionNoGenerate TransNO = new TransactionNoGenerate();
                        Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Opening Receipt", SessionFinYearVal);
                        if (Auto_Transaction_No == "")
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                        }
                    }
                    Opening_Receipt_No = Auto_Transaction_No.ToString();
                    Opening_Receipt_Date1 = DateTime.Now.ToString();
                    Opening_Receipt_Date = DateTime.Now.ToString("dd/MM/yyyy");

                    if (!ErrFlag)
                    {

                        query = "delete from Stock_Current_All where ItemName = '" + ItemName.ToString() + "' and FinYearVal='" + SessionFinYearVal + "'";
                        qry_dt = objdata.RptEmployeeMultipleDetails(query);

                        query = "select * from Stock_Current_All where ItemName = '" + ItemName.ToString() + "' and FinYearVal='" + SessionFinYearVal + "'";
                        qry_dt = objdata.RptEmployeeMultipleDetails(query);


                        if (qry_dt.Rows.Count == 0)
                        {
                            //DataTable da_check = new DataTable();
                            //query = "select CostCenterCode,CostCenterName,WarehouseCode,WarehouseName,ZoneName, ";
                            //query = query + " BinName from Stock_Ledger_All where (Trans_Type='OPENING RECEIPT' or Trans_Type='UNPLANNED RECEIPT') ";
                            //query = query + "   and ItemCode='" + ItemCode.ToString() + "' and ItemName = '" + ItemName.ToString() + "'";
                            //da_check = objdata.RptEmployeeMultipleDetails(query);

                            //if (da_check.Rows.Count != 0)
                            //{
                            //    WareHouseCode = da_check.Rows[0]["WarehouseCode"].ToString();
                            //    WarehouseName = da_check.Rows[0]["WarehouseName"].ToString();
                            //    ZoneName = da_check.Rows[0]["ZoneName"].ToString();
                            //    BinName = da_check.Rows[0]["BinName"].ToString();
                            //    CostcenterCode = da_check.Rows[0]["CostcenterCode"].ToString();
                            //    CostcenterName = da_check.Rows[0]["CostCenterName"].ToString();

                            //}
                            //else
                            //{
                            //    WareHouseCode = "";
                            //    WarehouseName = "";
                            //    ZoneName = "";
                            //    BinName = "";
                            //    CostcenterCode = "";
                            //    CostcenterName = ""; 
                            //}

                            //Insert Data Opening Receipt Main
                            query = "insert into Opening_Receipt_Main(Ccode,Lcode,FinYearCode,FinYearVal,Open_Recp_No,Open_Recp_Date,DeptCode,DeptName,CostCenterCode,CostCenterName,WarehouseCode,WarehouseName,UserID,UserName,OP_Receipt_Status)";
                            query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Opening_Receipt_No + "',";
                            query = query + "'" + Opening_Receipt_Date.ToString() + "','" + DeptCode + "','" + DeptName + "','" + CostcenterCode + "','" + CostcenterName + "','" + WareHouseCode + "','" + WarehouseName + "','" + SessionUserID + "','" + SessionUserName + "','1')";
                            qry_dt = objdata.RptEmployeeMultipleDetails(query);

                            //Insert Data Opening Receipt Sub
                            query = "insert into Opening_Receipt_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Open_Recp_No,Open_Recp_Date,ItemCode,ItemName,ValuationType,UOMCode,ZoneName,BinName,OpenQty,OpenValue,UserID,UserName)";
                            query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Opening_Receipt_No + "',";
                            query = query + "'" + Opening_Receipt_Date.ToString() + "','" + ItemCode + "','" + ItemName + "','" + ValuationType.ToString() + "','" + UOM_Code_Str.ToString() + "','" + ZoneName.ToString() + "',";
                            query = query + "'" + BinName.ToString() + "','" + OpeningQty.ToString() + "','" + OpeningValue.ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                            qry_dt = objdata.RptEmployeeMultipleDetails(query);

                            //Insert Data Opening Stock in Temp Table
                            query = "insert into Temp_OpeningStock(Ccode,Lcode,FinYearCode,FinYearVal,Open_Recp_No,Open_Recp_Date,WarehouseCode,WarehouseName,ItemCode,ItemName,ValuationType,UOMCode,ZoneName,BinName,OpenQty,OpenValue,UserID,UserName,OP_Receipt_Status)";
                            query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + Opening_Receipt_No + "',";
                            query = query + "'" + Opening_Receipt_Date.ToString() + "','" + WareHouseCode + "','" + WarehouseName + "','" + ItemCode + "','" + ItemName + "',";
                            query = query + "'" + ValuationType.ToString() + "','" + UOM_Code_Str.ToString() + "','" + ZoneName.ToString() + "',";
                            query = query + "'" + BinName.ToString() + "','" + OpeningQty.ToString() + "','" + OpeningValue.ToString() + "','" + SessionUserID + "','" + SessionUserName + "','1')";
                            qry_dt = objdata.RptEmployeeMultipleDetails(query);
                            Stock_Add();
                        }
                        else
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Item Already Existed in Current All Table');", true);
                            lblUploadSuccessfully.Text = "Item Already Exist in Current All Table";
                        }

                    }
                }
                if (!ErrFlag)
                {
                 
                    lblUploadSuccessfully.Text = "Upload Successfully...";
                   // Response.Redirect("Opening_Stock_View.aspx");
                }

                if (fileUpload.HasFile)
                {

                }
                else
                {
                    lblUploadSuccessfully.Text = "";
                }
            }
        }

    }
    
}