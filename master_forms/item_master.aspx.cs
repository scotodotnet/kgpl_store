﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;


public partial class master_forms_item_master : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;

    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Item Master";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");

            Load_Data_Empty_ItemType();
            Load_Data_Empty_Division();
            Load_Data_Empty_Dept();
            Load_Data_Empty_CostCenter();
        }
        
        Load_Data();
    }
    private void Load_Data_Empty_CostCenter()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtCostCenter.Items.Clear();
        query = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And DivisionCode='" + txtDivision.SelectedValue + "' And DeptCode='" + txtDepartmentName.SelectedValue + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCostCenter.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["CostcenterCode"] = "-Select-";
        dr["CostcenterName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCostCenter.DataTextField = "CostcenterName";
        txtCostCenter.DataValueField = "CostcenterCode";
        txtCostCenter.DataBind();

    }

    private void Load_Data_Empty_Dept()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And DivisionCode='" + txtDivision.SelectedValue + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    private void Load_Data_Empty_Division()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtDivision.Items.Clear();
        query = "Select DivisionCode,Division from MstDivision where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDivision.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["Division"] = "-Select-";
        dr["DivisionCode"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDivision.DataTextField = "Division";
        txtDivision.DataValueField = "DivisionCode";
        txtDivision.DataBind();

    }


    private void Load_Data_Empty_ItemType()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtitem_type.Items.Clear();
        query = "Select ItemTypeCode,ItemType from MstItemType where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtitem_type.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["ItemTypeCode"] = "-Select-";
        dr["ItemType"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtitem_type.DataTextField = "ItemType";
        txtitem_type.DataValueField = "ItemTypeCode";
        txtitem_type.DataBind();

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Item Master");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Item Details...');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Item Master");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Item...');", true);
            }
        }


        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                string ItemCode = "";
                DataTable DT_Auto = new DataTable();
                string ItemCodeAuto = "";
                string Zero_Join = "";
                query = "Select * from MstItemCodeNumber";
                DT_Auto = objdata.RptEmployeeMultipleDetails(query);
                if (DT_Auto.Rows.Count != 0)
                {
                    ItemCodeAuto = DT_Auto.Rows[0]["ItemCode"].ToString();
                }

                if (ItemCodeAuto != "")
                {
                    if (ItemCodeAuto.Length < 4)
                    {
                        for (int i = ItemCodeAuto.Length; i <= 3; i++)
                        {
                            Zero_Join = Zero_Join + "0";
                        }
                    }

                    string Div_str = txtDivision.SelectedItem.Text.Substring(0, 1);
                    string Dept_str = txtDepartmentName.SelectedItem.Text.Substring(0, 1);
                    string Cost_str = txtCostCenter.SelectedItem.Text.Substring(0, 1);
                    string Item_str = txtitem_short_desc.Text.Substring(0, 1);


                    ItemCode = Item_str + Zero_Join + ItemCodeAuto + "-" + txtDivision.SelectedValue + Div_str + "-" + txtDepartmentName.SelectedValue + Dept_str;   //+"-" + txtCostCenter.SelectedValue + Cost_str;

                    txtitem_code.Text = ItemCode;

                    string New_Last_No = "";
                    New_Last_No = (Convert.ToDecimal(ItemCodeAuto) + Convert.ToDecimal(1)).ToString();
                    query = "Update MstItemCodeNumber Set ItemCode='" + New_Last_No + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Item Code...');", true);
                }
            }
        }

        //User Rights Check End
        if (!ErrFlag)
        {
            query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And (ItemCode='" + txtitem_code.Text + "' Or ItemShortName='" + txtitem_short_desc.Text + "')";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                if (btnSave.Text == "Update")
                {
                    SaveMode = "Update";
                    //
                    query = "UPDATE Stock_Current_All set ItemName='" + txtitem_short_desc.Text + "' where ItemCode='" + txtitem_code.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    query = "UPDATE Stock_Transaction_Ledger set ItemName='" + txtitem_short_desc.Text + "' where ItemCode='" + txtitem_code.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    query = "UPDATE Stock_Ledger_All set ItemName='" + txtitem_short_desc.Text + "' where ItemCode='" + txtitem_code.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    query = "UPDATE Reuse_Stock_Ledger_All set ItemName='" + txtitem_short_desc.Text + "' where ItemCode='" + txtitem_code.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    //query = "UPDATE Meterial_Issue_Main_Sub set ItemName='" + txtitem_short_desc.Text + "' where ItemCode='" + txtitem_code.Text + "'";
                    //objdata.RptEmployeeMultipleDetails(query);
                    query = "UPDATE Unplanned_Receipt_Main_Sub set ItemName='" + txtitem_short_desc.Text + "' where ItemCode='" + txtitem_code.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    query = "UPDATE Scrap_Meterial_ReUse_Main_Sub set ItemName='" + txtitem_short_desc.Text + "' where ItemCode='" + txtitem_code.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);




                    query = "Delete from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtitem_code.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    SaveMode = "Error";
                }
            }
            if (SaveMode != "Error")
            {
                string ActiveMode = Rdpitem_status.SelectedValue.ToString();
                string ExciseDet = RdpExcise_details.SelectedValue.ToString();
                string Modvat = RdpModvat.SelectedValue.ToString();
                //string ReorderQty = "0.00";
                //if (txtReorder_Qty.Text == "")
                //{
                //    ReorderQty = "0.00";
                //}
                //else
                //{
                //    ReorderQty = txtReorder_Qty.Text.ToString();
                //}
                string MaxQtyLevel = "0.00";
                if (txtMax_Qty_Level.Text == "")
                {
                    MaxQtyLevel = "0.00";
                }
                else
                {
                    MaxQtyLevel = txtMax_Qty_Level.Text.ToString();
                }
                string MinQtyLevel = "0.00";
                if (txtMin_Qty_Level.Text == "")
                {
                    MinQtyLevel = "0.00";
                }
                else
                {
                    MinQtyLevel = txtMin_Qty_Level.Text.ToString();
                }
                //string SafetyQtyLevel = "0.00";
                //if (txtSafety_Qty_Level.Text == "")
                //{
                //    SafetyQtyLevel = "0.00";
                //}
                //else
                //{
                //    SafetyQtyLevel = txtSafety_Qty_Level.Text.ToString();
                //}
                string StandardCost = "0.00";
                if (txtStandard_Cost.Text == "")
                {
                    StandardCost = "0.00";
                }
                else
                {
                    StandardCost = txtStandard_Cost.Text.ToString();
                }
                string PurchaseRate = "0.00";
                if (txtPurchase_Rate.Text == "")
                {
                    PurchaseRate = "0.00";
                }
                else
                {
                    PurchaseRate = txtPurchase_Rate.Text.ToString();
                }
                string SalesRate = "0.00";
                if (txtSales_Rate.Text == "")
                {
                    SalesRate = "0.00";
                }
                else
                {
                    SalesRate = txtSales_Rate.Text.ToString();
                }
                //string AnalyzInformation = RdbAnalyzing_Information.SelectedValue.ToString();


                //Insert Compnay Details
                query = "Insert Into MstItemMaster(Ccode,Lcode,ItemCode,ActiveMode,ItemShortName,ItemLongName,PurchaseUOM,SalesUOM,StockUOM,ManufacturingUOM,";
                query = query + " ItemType,ValuationType,ExciseDet,Bed,AED,SED,Modvat,FormType,MaxQtyLevel,MinQtyLevel,";
                query = query + " StandardCost,PurchaseRate,SalesRate,MovingType,PurchaseType,UserID,UserName,DivisionCode,Division,DeptCode,DeptName,CostcenterCode,CostcenterName,DrawingNo,CatalogueNo) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + txtitem_code.Text + "','" + ActiveMode + "','" + txtitem_short_desc.Text + "','" + txtitem_short_desc.Text + "',";
                query = query + " '" + txtpurchase_uom.Text + "','" + txtpurchase_uom.Text + "','" + txtpurchase_uom.Text + "','" + txtpurchase_uom.Text + "','" + txtitem_type.Text + "',";
                query = query + " '" + txtValuation_Type.Text + "','" + ExciseDet + "','" + txtBed.Text + "','" + txtAed.Text + "','" + txtSed.Text + "','" + Modvat + "',";
                query = query + " '" + txtForm_type.Text + "','" + MaxQtyLevel + "','" + MinQtyLevel + "',";
                query = query + " '" + StandardCost + "','" + PurchaseRate + "','" + SalesRate + "','" + txtmoving_type.Text + "','" + txtPur_Type.Text + "',";
                query = query + " '" + SessionUserID + "','" + SessionUserName + "','" + txtDivision.SelectedValue + "','" + txtDivision.SelectedItem.Text + "','" + txtDepartmentName.SelectedValue + "',";
                query = query + " '" + txtDepartmentName.SelectedItem.Text + "','" + txtCostCenter.SelectedValue + "','" + txtCostCenter.SelectedItem.Text + "','" + txtDrawNo.Text + "','" + txtCatalogueNo.Text + "')";
                objdata.RptEmployeeMultipleDetails(query);

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Item Master Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Item Master Details Updated Successfully');", true);
                }

                Load_Data();
                Clear_All_Field();
                btnSave.Text = "Save";
                txtitem_code.Enabled = true;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Account Head Details Already Saved Successfully');", true);
            }
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtitem_code.Text = ""; txtitem_short_desc.Text = ""; txtItem_Long_Desc.Text = ""; txtpurchase_uom.SelectedValue = "Numbers";
        txtsales_uom.SelectedValue = "Numbers"; txtstock_uom.SelectedValue = "Numbers"; txtmanufacturing_uom.SelectedValue = "Numbers";
        txtValuation_Type.Text = ""; txtBed.Text = ""; txtAed.Text = ""; txtSed.Text = ""; txtForm_type.Text = "";
        txtMax_Qty_Level.Text = ""; txtMin_Qty_Level.Text = "";
        txtStandard_Cost.Text = ""; txtPurchase_Rate.Text = ""; txtSales_Rate.Text = ""; txtmoving_type.Text = ""; txtPur_Type.Text = "";
        txtitem_type.SelectedValue = "-Select-";

        txtCatalogueNo.Text = "";
        txtDrawNo.Text = "";


        Load_Data_Empty_Division();
        Load_Data_Empty_Dept();
        Load_Data_Empty_CostCenter();

        btnSave.Text = "Save";
        txtitem_code.Enabled = true;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtitem_code.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            Rdpitem_status.SelectedValue = DT.Rows[0]["ActiveMode"].ToString();
            txtitem_short_desc.Text = DT.Rows[0]["ItemShortName"].ToString();
            txtItem_Long_Desc.Text = DT.Rows[0]["ItemLongName"].ToString();
            txtpurchase_uom.Text = DT.Rows[0]["PurchaseUOM"].ToString();
            txtsales_uom.Text = DT.Rows[0]["SalesUOM"].ToString();
            txtstock_uom.Text = DT.Rows[0]["StockUOM"].ToString();
            txtmanufacturing_uom.Text = DT.Rows[0]["ManufacturingUOM"].ToString();
            txtitem_type.SelectedItem.Text = DT.Rows[0]["ItemType"].ToString();
            txtValuation_Type.Text = DT.Rows[0]["ValuationType"].ToString();
            RdpExcise_details.SelectedValue = DT.Rows[0]["ExciseDet"].ToString();
            txtBed.Text = DT.Rows[0]["Bed"].ToString();
            txtAed.Text = DT.Rows[0]["AED"].ToString();
            txtSed.Text = DT.Rows[0]["SED"].ToString();
            RdpModvat.SelectedValue = DT.Rows[0]["Modvat"].ToString();
            txtForm_type.Text = DT.Rows[0]["FormType"].ToString();
            txtMax_Qty_Level.Text = DT.Rows[0]["MaxQtyLevel"].ToString();
            txtMin_Qty_Level.Text = DT.Rows[0]["MinQtyLevel"].ToString();
            txtStandard_Cost.Text = DT.Rows[0]["StandardCost"].ToString();
            txtPurchase_Rate.Text = DT.Rows[0]["PurchaseRate"].ToString();
            txtSales_Rate.Text = DT.Rows[0]["SalesRate"].ToString();
            txtmoving_type.Text = DT.Rows[0]["MovingType"].ToString();
            txtPur_Type.Text = DT.Rows[0]["PurchaseType"].ToString();

            txtDivision.SelectedValue = DT.Rows[0]["DivisionCode"].ToString();
            Load_Data_Empty_Dept();
            txtDepartmentName.SelectedValue = DT.Rows[0]["DeptCode"].ToString();
            Load_Data_Empty_CostCenter();
            txtCostCenter.SelectedValue = DT.Rows[0]["CostcenterCode"].ToString();


            txtDrawNo.Text = DT.Rows[0]["DrawingNo"].ToString();
            txtCatalogueNo.Text = DT.Rows[0]["CatalogueNo"].ToString();

            txtitem_code.Enabled = false;
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtitem_code.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Item Master");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New Item..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {
            DataTable dtdcheckGeneral = new DataTable();
            DataTable dtdcheckBlanket = new DataTable();
            DataTable dtdcheckStandard = new DataTable();
            DataTable dtdcurrentStock = new DataTable();
            DataTable dtdPurchaseReq = new DataTable();

            query = "select ItemCode,ItemName from General_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and ItemCode='" + e.CommandName.ToString() + "'";
            dtdcheckGeneral = objdata.RptEmployeeMultipleDetails(query);

            query = "select ItemCode,ItemName from Blanket_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and ItemCode='" + e.CommandName.ToString() + "'";
            dtdcheckBlanket = objdata.RptEmployeeMultipleDetails(query);

            query = "select ItemCode,ItemName from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and ItemCode='" + e.CommandName.ToString() + "'";
            dtdcheckStandard = objdata.RptEmployeeMultipleDetails(query);

            query = "select ItemCode,ItemName from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and ItemCode='" + e.CommandName.ToString() + "'";
            dtdcurrentStock = objdata.RptEmployeeMultipleDetails(query);

            query = "select ItemCode,ItemName from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and ItemCode='" + e.CommandName.ToString() + "'";
            dtdPurchaseReq = objdata.RptEmployeeMultipleDetails(query);

            if ((dtdcheckGeneral.Rows.Count == 0) && (dtdcheckBlanket.Rows.Count == 0) && (dtdcheckStandard.Rows.Count == 0) && (dtdcurrentStock.Rows.Count == 0) && (dtdPurchaseReq.Rows.Count == 0))
            {



                DataTable DT = new DataTable();
                query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + e.CommandName.ToString() + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    query = "Delete from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + e.CommandName.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Item Master Details Deleted Successfully');", true);
                    Load_Data();
                    Clear_All_Field();
                }
            }
            else
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Sorry Item Available in some other table..');", true);

            }
        }
    }
    protected void txtDivision_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_Dept();
        Load_Data_Empty_CostCenter();
    }
    protected void txtDepartmentName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_CostCenter();
    }
    
}
