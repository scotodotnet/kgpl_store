﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class master_forms_Tax_Master : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: GST MASTER";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");

            //Load_Data();
            
        }
        Load_OldData();
    }
    private void Load_OldData()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        if (!ErrFlag)
        {
            query = "Select * from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GST_Type='" + txtGST_Type.Text.ToUpper() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //if (btnSave.Text == "Update")
                //{
                SaveMode = "Update";
                query = "Delete from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GST_Type='" + txtGST_Type.Text.ToUpper() + "'";
                objdata.RptEmployeeMultipleDetails(query);
                //}
                //else
                //{
                //   SaveMode = "Error";

                //}

            }
            if (SaveMode != "Error")
            {
                //Insert Compnay Details
                query = "Insert Into MsTAX(Ccode,Lcode,VATID,VATPercent,GSTPercent,GST_Type,CGST_Percent,SGST_Percent,IGST_Percent,UserID,Username)";
                query = query + " Values('" + SessionCcode + "','" + SessionLcode + "','1','5','5','" + txtGST_Type.Text.ToUpper() + "',";
                query = query + "'" + txtCGST_Per.Text + "','" + txtSGST_Percent.Text + "','" + txtIGST_Percent.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                //Remaining Company to Insert
                DataTable DT_New = new DataTable();
                query = "Select * from AdminRights where Ccode!='" + SessionCcode + "' Or LCode!='" + SessionLcode + "'";
                DT_New = objdata.RptEmployeeMultipleDetails(query);

                for (int i = 0; i < DT_New.Rows.Count; i++)
                {
                    query = "Select * from MsTAX where Ccode='" + DT_New.Rows[i]["Ccode"].ToString() + "' And Lcode='" + DT_New.Rows[i]["LCode"].ToString() + "' And GST_Type='" + txtGST_Type.Text.ToUpper() + "'";
                    DT = objdata.RptEmployeeMultipleDetails(query);

                    if (DT.Rows.Count != 0)
                    {
                        query = "Delete from MsTAX where Ccode='" + DT_New.Rows[i]["Ccode"].ToString() + "' And Lcode='" + DT_New.Rows[i]["LCode"].ToString() + "' And GST_Type='" + txtGST_Type.Text.ToUpper() + "'";
                        objdata.RptEmployeeMultipleDetails(query);
                    }

                    query = "Insert Into MsTAX(Ccode,Lcode,VATID,VATPercent,GSTPercent,GST_Type,CGST_Percent,SGST_Percent,IGST_Percent,UserID,Username)";
                    query = query + " Values('" + DT_New.Rows[i]["Ccode"].ToString() + "','" + DT_New.Rows[i]["LCode"].ToString() + "','1','5','5','" + txtGST_Type.Text.ToUpper() + "',";
                    query = query + "'" + txtCGST_Per.Text + "','" + txtSGST_Percent.Text + "','" + txtIGST_Percent.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(query);
                }


                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GST Master Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GST Master Details Updated Successfully');", true);
                }


                Clear_All_Field();
                btnSave.Text = "Save";

                Load_OldData();
               // Load_Data();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GST Details Already Saved');", true);
            }
        }
    }


    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtGST_Type.Text = e.CommandName.ToString();
        Load_Data();
        //Load_OldData();
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        
       
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GST_Type='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                query = "Delete from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GST_Type='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GST Details Deleted Successfully');", true);
                Load_OldData();
                Clear_All_Field();
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtGST_Type.Enabled = true;
        txtGST_Type.Text = "";
        txtIGST_Percent.Text = "0";
        txtCGST_Per.Text="0";
        txtSGST_Percent.Text = "0";
        btnSave.Text = "Save";
        Load_OldData();
    }

    protected void txtGST_Type_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data();
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GST_Type='" + txtGST_Type.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtGST_Type.Enabled = false;
            txtCGST_Per.Text = DT.Rows[0]["CGST_Percent"].ToString();
            txtSGST_Percent.Text = DT.Rows[0]["SGST_Percent"].ToString();
            txtIGST_Percent.Text = DT.Rows[0]["IGST_Percent"].ToString();

            btnSave.Text = "Update";
        }
        else
        {
            txtGST_Type.Enabled = true;
            txtCGST_Per.Text = "0";
            txtSGST_Percent.Text = "0";
            txtIGST_Percent.Text = "0";
            btnSave.Text = "Save";
        }
        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();
    }

}
