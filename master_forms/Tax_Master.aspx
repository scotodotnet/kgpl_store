﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="Tax_Master.aspx.cs" Inherits="master_forms_Tax_Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();               
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">GST Master</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
			<div class="panel-heading clearfix">
					<h4 class="panel-title">GST Master</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					       
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">GST Type<span class="mandatory">*</span></label>							
                                <asp:TextBox ID="txtGST_Type" runat="server" class="form-control" AutoPostBack="true" 
                                    OnSelectedIndexChanged="txtGST_Type_SelectedIndexChanged">
                                <%--<asp:ListItem Value="SGST" Text="SGST"></asp:ListItem>
                                <asp:ListItem Value="IGST" Text="IGST"></asp:ListItem>--%>
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtGST_Type" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">CGST Percentage <span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtCGST_Per" runat="server" class="form-control" Text="0"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" 
                                    FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtCGST_Per" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">SGST Percentage <span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtSGST_Percent" runat="server" class="form-control" Text="0"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" 
                                    FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtSGST_Percent" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        </div>
					        <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">IGST Percentage <span class="mandatory">*</span></label>
                                <asp:TextBox ID="txtIGST_Percent" runat="server" class="form-control" Text="0"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" 
                                    FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtIGST_Percent" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					    </div>
					</div>
					
                    <!-- Button start -->
					<div class="col-md-12">
					    <div class="row">
					        <div class="txtcenter">
                                <div class="col-sm-11">
                                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                    <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                                </div>
                            </div>
					    </div>
					</div>                            
                    <!-- Button End -->
                    <div class="form-group row"></div>
                    
                    
                    <!-- table start -->
                    <div class="col-md-12">
		                <div class="row">
		                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
		                        <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>GST Type</th>
                                                <th>CGST %</th>
                                                <th>SGST %</th>
                                                <th>IGST %</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("GST_Type")%></td>
                                        <td><%# Eval("CGST_Percent")%></td>
                                        <td><%# Eval("SGST_Percent")%></td>
                                        <td><%# Eval("IGST_Percent")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("GST_Type")%>'>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("GST_Type")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this GST Type details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
		                    </asp:Repeater>		                    
		                </div>
		            </div>	
                    <!-- table End --> 
					
                    			
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		     <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                     </div>
                                </div>
                                <div class="panel-body">
                               </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
            
      
		    <div class="col-md-2"></div>
         
      </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 
 
</asp:Content>

