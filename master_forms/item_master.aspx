<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="item_master.aspx.cs" Inherits="master_forms_item_master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();               
            }
        });
    };
</script>
<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);

    }
</script>
<form class="form-horizontal">
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
        <div class="panel panel-primary">
            <div class="panel-heading clearfix">
                <h3 class="panel-title">Item Master</h3>
            </div>
            </div>
            <div class="panel-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Main</a></li>
                        <li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Others</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="tab1">
                        <%--<form class="form-horizontal">--%>
						
						<div class="col-md-12">
			            <div class="panel panel-white">
				            <%--<form class="form-horizontal">--%>
				            <div class="panel-body">
					            <div class="col-md-12">
					                <div class="row">
					                    <div class="form-group col-md-4">
					                        <label for="exampleInputName">Item Code<span class="mandatory">*</span></label>
					                        <asp:Label ID="txtitem_code" class="form-control" MaxLength="35" runat="server" Style="text-transform: uppercase"></asp:Label>
					                        <%--<asp:RequiredFieldValidator ControlToValidate="txtitem_code" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>--%>
					                    </div>
					                    <%--<asp:LinkButton ID="item_search" class="btn btn-primary btn-addon  btn-rounded btn-sm" runat="server" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-search"></i>Search</asp:LinkButton>--%>					                    
					                    <div class="form-group col-md-4">
					                        <label for="exampleInputName">Active Mode</label>
					                        <asp:RadioButtonList ID="Rdpitem_status" class="form-control" runat="server" RepeatColumns="2" >
                                            <asp:ListItem Value="1" Text="Active" style="padding-right:40px" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Deactive" ></asp:ListItem>
                                            </asp:RadioButtonList>
					                    </div>
					                    <div class="form-group col-md-4">
					                        <label for="exampleInputName">Item Desc<span class="mandatory">*</span></label>
					                        <asp:TextBox ID="txtitem_short_desc" class="form-control" runat="server"></asp:TextBox>
					                        <asp:RequiredFieldValidator ControlToValidate="txtitem_short_desc" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
					                    </div>
					                    </div>
					                     <div class="row">
					                    <div class="form-group col-md-4" runat="server" visible="false">
					                        <label for="exampleInputName">Item Long Desc<span class="mandatory">*</span></label>
					                        <asp:TextBox ID="txtItem_Long_Desc" runat="server" TextMode="MultiLine" class="form-control"></asp:TextBox>
					                        <%--<asp:RequiredFieldValidator ControlToValidate="txtItem_Long_Desc" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>--%>
					                    </div>
					                  
					                    <div class="form-group col-md-4" runat="server" visible="false">
					                        <label for="exampleInputName">Sales UOM</label>
					                        <asp:DropDownList ID="txtsales_uom" runat="server" class="form-control">
					                        <asp:ListItem Value="Numbers" Text="Numbers"></asp:ListItem>
                                            <asp:ListItem Value="Bags" Text="Bags"></asp:ListItem>
                                            <asp:ListItem Value="Boxes" Text="Boxes"></asp:ListItem>
                                            <asp:ListItem Value="Kilograms" Text="Kilograms"></asp:ListItem>
                                            <asp:ListItem Value="Liters" Text="Liters"></asp:ListItem>
                                            <asp:ListItem Value="Mtrs" Text="Mtrs"></asp:ListItem>
                                            </asp:DropDownList>
					                    </div>
					                     <div id="Div1" class="form-group col-md-4"  runat="server" visible="false">
					                        <label for="exampleInputName">Stock UOM</label>
                                            <asp:DropDownList ID="txtstock_uom" runat="server" class="form-control">
                                             <asp:ListItem Value="Numbers" Text="Numbers"></asp:ListItem>
                                             <asp:ListItem Value="Bags" Text="Bags"></asp:ListItem>
                                             <asp:ListItem Value="Boxes" Text="Boxes"></asp:ListItem>
                                             <asp:ListItem Value="Kilograms" Text="Kilograms"></asp:ListItem>
                                             <asp:ListItem Value="Liters" Text="Liters"></asp:ListItem>
                                             <asp:ListItem Value="Mtrs" Text="Mtrs"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
					                </div>					                
					            </div>
					            <div class="col-md-12">
					                <div class="row">
					                   
                                        <div class="form-group col-md-4" runat="server" visible="false">
                                            <label for="exampleInputName">Manufacturing UOM</label>
                                            <asp:DropDownList ID="txtmanufacturing_uom" runat="server" class="form-control">
                                                <asp:ListItem Value="Numbers" Text="Numbers"></asp:ListItem>
                                                <asp:ListItem Value="Bags" Text="Bags"></asp:ListItem>
                                                <asp:ListItem Value="Boxes" Text="Boxes"></asp:ListItem>
                                                <asp:ListItem Value="Kilograms" Text="Kilograms"></asp:ListItem>
                                                <asp:ListItem Value="Liters" Text="Liters"></asp:ListItem>
                                                <asp:ListItem Value="Mtrs" Text="Mtrs"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                          <div class="form-group col-md-4">
					                        <label for="exampleInputName">UOM</label>
                                            <asp:DropDownList ID="txtpurchase_uom" runat="server" class="form-control">
                                            <asp:ListItem Value="Numbers" Text="Numbers"></asp:ListItem>
                                            <asp:ListItem Value="Bags" Text="Bags"></asp:ListItem>
                                            <asp:ListItem Value="Boxes" Text="Boxes"></asp:ListItem>
                                            <asp:ListItem Value="Kilograms" Text="Kilograms"></asp:ListItem>
                                            <asp:ListItem Value="Liters" Text="Liters"></asp:ListItem>
                                            <asp:ListItem Value="Mtrs" Text="Mtrs"></asp:ListItem>
                                            </asp:DropDownList>
					                    </div>
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName">Item Type</label>
                                            <asp:DropDownList ID="txtitem_type" runat="server" class="form-control">
                                                <%--<asp:ListItem Value="Raw-material" Text="Raw-material"></asp:ListItem>
                                                <asp:ListItem Value="Intermediates" Text="Intermediates"></asp:ListItem>
                                                <asp:ListItem Value="Finished Goods" Text="Finished Goods"></asp:ListItem>
                                                <asp:ListItem Value="Spare Parts" Text="Spare Parts"></asp:ListItem>
                                                <asp:ListItem Value="Tools" Text="Tools"></asp:ListItem>
                                                <asp:ListItem Value="Consumables" Text="Consumables"></asp:ListItem>
                                                <asp:ListItem Value="Utilities" Text="Utilities"></asp:ListItem>
                                                <asp:ListItem Value="Capital" Text="Capital"></asp:ListItem>
                                                <asp:ListItem Value="Service" Text="Service"></asp:ListItem>
                                                <asp:ListItem Value="Model" Text="Model"></asp:ListItem>
                                                <asp:ListItem Value="Kit" Text="Kit"></asp:ListItem>
                                                <asp:ListItem Value="Lubricants" Text="Lubricants"></asp:ListItem>
                                                <asp:ListItem Value="Others" Text="Others"></asp:ListItem>--%>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ControlToValidate="txtitem_type" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName">Valuation Type</label>
                                            <asp:DropDownList ID="txtValuation_Type" runat="server" class="form-control">
                                            <asp:ListItem Value="FIFO" Text="FIFO"></asp:ListItem>
                                            <asp:ListItem Value="LIFO" Text="LIFO"></asp:ListItem>
                                            <asp:ListItem Value="STANDARD" Text="STANDARD"></asp:ListItem>
                                            <asp:ListItem Value="WIGHT AVERAGE" Text="WIGHT AVERAGE"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                         </div>	
                                         <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName">Excise Details</label>
                                            <asp:RadioButtonList ID="RdpExcise_details" runat="server" RepeatColumns="2" class="form-control">
                                                <asp:ListItem Value="1" Text="Yes" style="padding-right:40px" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="No" ></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        <div class="form-group col-md-4">
					            <label for="exampleInputName">Division<span class="mandatory">*</span></label>
					            
					            <asp:DropDownList ID="txtDivision" runat="server" class="js-states form-control" AutoPostBack="true" onselectedindexchanged="txtDivision_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ControlToValidate="txtDivision" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                </div>
                                 <div class="form-group col-md-4">
					            <label for="exampleInputName">Department<span class="mandatory">*</span></label>
					            
					            <asp:DropDownList ID="txtDepartmentName" runat="server" class="js-states form-control" AutoPostBack="true" onselectedindexchanged="txtDepartmentName_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ControlToValidate="txtDepartmentName" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                </div>
                                        <div class="form-group col-md-4">
                                            <%--<label for="exampleInputName" visible="false">BED</label>--%>
                                            <asp:TextBox ID="txtBed" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                        </div>
					                </div>
					            </div>
					            <div class="col-md-12">
					                <div class="row">
					                <div id="Div2" class="form-group col-md-4">
					            <label for="exampleInputName">Drawing No</label>
					            <asp:TextBox ID="txtDrawNo" class="form-control" runat="server"></asp:TextBox>
                                        </div>
					                <div id="Div3" class="form-group col-md-4">
					            <label for="exampleInputName">Catalogue No</label>
					            <asp:TextBox ID="txtCatalogueNo" class="form-control" runat="server"></asp:TextBox>
                                        </div>
					                
					                
                                 <div class="form-group col-md-4" runat="server" visible="false">
					            <label for="exampleInputName">Cost Center<span class="mandatory">*</span></label>
					            
					            <asp:DropDownList ID="txtCostCenter" runat="server" class="js-states form-control">
                                </asp:DropDownList>
                                <%--<asp:RequiredFieldValidator ControlToValidate="txtCostCenter" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
                                </div>
					                    <div class="form-group col-md-4">
					                        <%--<label for="exampleInputName" visible="false">AED</label>--%>
                                            <asp:TextBox ID="txtAed" class="form-control" runat="server" Visible="false"></asp:TextBox>
					                    </div>
					                    <div class="form-group col-md-4">
					                        <%--<label for="exampleInputName" visible="false">SED</label>--%>
                                            <asp:TextBox ID="txtSed" class="form-control" runat="server" Visible="false"></asp:TextBox>
					                    </div>
					                    <div class="form-group col-md-4">
					                        <%--<label for="exampleInputName" visible="false">MODVAT</label>--%>
                                            <asp:RadioButtonList ID="RdpModvat" runat="server" RepeatColumns="2" class="form-control" Visible="false">
                                                <asp:ListItem Value="1" Text="Yes" style="padding-right:40px" Selected="True" ></asp:ListItem>
                                                <asp:ListItem Value="2" Text="No" ></asp:ListItem>
                                            </asp:RadioButtonList>
					                    </div>
					                    <div class="form-group col-md-4">
					                        <%--<label for="exampleInputName" visible="false">Form Type</label>--%>
                                            <asp:TextBox ID="txtForm_type" class="form-control" runat="server" Visible="false"></asp:TextBox>
					                    </div>
					                </div>
					            </div>
						</div><!-- panel body end -->						
						<%--</form>--%>
			            </div><!-- panel white end -->
		                </div><!-- col-12 end -->
		                
                       	<div class="col-md-1"></div>
						<%--</form>--%>
                        </div><!-- tab1 end -->
                        <div role="tabpanel" class="tab-pane" id="tab2">
                        <%--<form class="form-horizontal">--%>
						<div class="col-md-12">
			            <div class="panel panel-white">
				            <%--<form class="form-horizontal">--%>
				                <div class="panel-body">
            					    <div class="col-md-12">
					                    <div class="row">
					                        
					                        <div class="form-group col-md-4">
					                            <label for="exampleInputName">Max Qty Level</label>
                                                <asp:TextBox ID="txtMax_Qty_Level" class="form-control"  runat="server"></asp:TextBox>
					                        </div>
					                        <div class="form-group col-md-4">
					                            <label for="exampleInputName">Min Qty Level</label>
                                                <asp:TextBox ID="txtMin_Qty_Level" class="form-control"  runat="server"></asp:TextBox>
					                        </div>
					                       
					                        <div class="form-group col-md-4">
					                            <label for="exampleInputName">Standard Cost</label>
                                                <asp:TextBox ID="txtStandard_Cost" class="form-control"  runat="server"></asp:TextBox>
					                        </div>
					                        <div class="form-group col-md-3">
					                            <label for="exampleInputName">Purchase Rate</label>
                                                <asp:TextBox ID="txtPurchase_Rate" class="form-control"  runat="server"></asp:TextBox>
					                        </div>
					                        <div class="form-group col-md-3">
					                            <label for="exampleInputName">Sales Rate</label>
                                                <asp:TextBox ID="txtSales_Rate" class="form-control"  runat="server"></asp:TextBox>
					                        </div>
					                       
					                        <div class="form-group col-md-3">
					                            <label for="exampleInputName">Moving Type</label>
                                                <asp:DropDownList ID="txtmoving_type" runat="server" class="form-control">
                                                <asp:ListItem Value="Fast" Text="">Fast</asp:ListItem>
                                                <asp:ListItem Value="Slow" Text="">Slow</asp:ListItem>
                                                <asp:ListItem Value="Medium" Text="">Medium</asp:ListItem>
                                                </asp:DropDownList>
					                        </div>
					                         <div class="form-group col-md-3">
					                          <label for="exampleInputName">Purchase Type</label>
                                              <asp:DropDownList ID="txtPur_Type" runat="server" class="form-control">
                                              <asp:ListItem Value="Normal" Text="">Normal</asp:ListItem>
                                              <asp:ListItem Value="Special" Text="">Special</asp:ListItem>
                                              </asp:DropDownList>
					                        </div>
					                        
					                        <%--<div class="form-group col-md-4">
					                            <label for="exampleInputName">Reorder Level</label>
                                                <asp:TextBox ID="txtReOrder_Level" class="form-control"  runat="server"></asp:TextBox>
					                        </div>
					                        <div class="form-group col-md-4">
					                            <label for="exampleInputName">Reorder Qty</label>
                                                <asp:TextBox ID="txtReorder_Qty" class="form-control"  runat="server"></asp:TextBox>
					                        </div>
					                        <div class="form-group col-md-4">
					                            <label for="exampleInputName">Safety Qty Level</label>
                                                <asp:TextBox ID="txtSafety_Qty_Level" class="form-control"  runat="server"></asp:TextBox>
					                        </div>
					                        <div class="form-group col-md-4">
					                            <label for="exampleInputName">Item</label>
                                                <asp:DropDownList ID="txtItem" runat="server" class="form-control">
                                                <asp:ListItem Value="1" Text="AS-CURAS-INVE-STROESSTOK"></asp:ListItem>
                                                </asp:DropDownList>
					                        </div>
					                        <div class="form-group col-md-4">
					                            <label for="exampleInputName">Consumtion</label>
                                                <asp:DropDownList ID="txtConsumtion" runat="server" class="form-control">
                                                <asp:ListItem Value="1" Text="EX-EXDIR-STOR"></asp:ListItem>
                                                </asp:DropDownList>
					                        </div>
					                        <div class="form-group col-md-4">
					                            <label for="exampleInputName">Sales</label>
                                                <asp:DropDownList ID="txtSales" runat="server" class="form-control">
                                                <asp:ListItem Value="1" Text="IN-SALES-FABR"></asp:ListItem>
                                                </asp:DropDownList>
					                        </div>
					                        <div class="form-group col-md-4">
					                            <label for="exampleInputName">Price Variance</label>
                                                <asp:DropDownList ID="txtPrice_Variance" runat="server" class="form-control">
                                                <asp:ListItem Value="1" Text="AS-CURAS-INVE-ITEMVARIAN"></asp:ListItem>
                                                </asp:DropDownList>
					                        </div>
					                        <div class="form-group col-md-4">
					                            <label for="exampleInputName">Adjustment</label>
                                                <asp:DropDownList ID="txtAdjustment" runat="server" class="form-control">
                                                <asp:ListItem Value="1" Text="EX-EXDIR-STOR-STORESADJT"></asp:ListItem>
                                                </asp:DropDownList>
					                        </div>
					                        <div class="form-group col-md-4">
					                            <label for="exampleInputName">Analyzing Information</label>
					                            <asp:RadioButtonList ID="RdbAnalyzing_Information" runat="server" RepeatColumns="4" class="form-control">
							                        <asp:ListItem Value="0" Text="None" style="padding-right:40px" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="A" style="padding-right:40px"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="B" style="padding-right:40px"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="C" ></asp:ListItem>
                                                </asp:RadioButtonList>
					                        </div>--%>
					                        
					                        <%--<div class="form-group col-md-4">
					                            <label for="exampleInputName">Standard Cost</label>
                                                <asp:TextBox ID="TextBox4" class="form-control"  runat="server"></asp:TextBox>
					                        </div>--%>
					                    </div>
					                </div>
				                </div><!-- panel body end -->
				           <%-- </form>--%>
				            
			            </div><!-- panel white end -->
		                </div><!-- col-12 end -->	
                         <div class="col-md-1"></div>
						<%--</form> --%>
					
						</div> <!-- tab2 end -->
							
                    </div><!-- tab -content end -->
                </div><!-- tab -panel end -->
                
                <!-- Button start -->
				<div class="col-md-12">
					<div class="row">
					    <div class="txtcenter">
                            <div class="col-sm-11">
                                <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                            </div>
                        </div>
					</div>
				</div>                            
                <!-- Button End -->
                
                
                <!-- table start -->
                    <div class="col-md-12">
		                <div class="row">
		                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
		                        <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Item Name</th>
                                                <th> UOM</th>
                                                <th>Min. Qty</th>
                                                <th>Max. Qty</th>
                                                <th>Moving Type</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("ItemShortName")%></td>
                                        <td><%# Eval("PurchaseUOM")%></td>
                                        <td><%# Eval("MinQtyLevel")%></td>
                                        <td><%# Eval("MaxQtyLevel")%></td>
                                        <td><%# Eval("MovingType")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("ItemCode")%>'>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this item details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
		                    </asp:Repeater>		                    
		                </div>
		            </div>
                    <!-- table End -->
                
            </div><!-- panel body end -->
        </div> <!-- panel  end -->
        
</div><!-- col 12 end -->
</div><!-- row end -->
</div>
</form>
</asp:Content>

