﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="account_head.aspx.cs" Inherits="master_forms_account_head" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();               
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>
<%--Code Select List Script Saart--%>
    
    <script type="text/javascript">
         $(document).ready(function () {
            initializer();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer);
        });
        function initializer() {
            $("#<%=txtAccCode.ClientID %>").autocomplete({
                 
                 
                source: function (request, response) {
                
                        
                      $.ajax({
 
                          url: '<%=ResolveUrl("../List_Service.asmx/GetAccountCode_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) {
 
                              response($.map(data.d, function (item) {
 
                                  return {
                                      label: item.split('-')[0],
                                      val: item.split('-')[1],
                                      
                                  }
 
                              }))
 
                          },
 
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      $("#<%=txtAcc_Code_Hide.ClientID %>").val(i.item.val);
                      
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtAcc_Code_Hide.ClientID %>").val(i.item.label);
                      
 
                  }
                  
 
              });
 
          }
      </script>
<%--Code Select List Script Saart--%>

<%--Account Type Select List Script Saart--%>    
<script type="text/javascript">
         $(document).ready(function () {
            initializer_AccountType();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_AccountType);
        });
        function initializer_AccountType() {
            $("#<%=txtAccType.ClientID %>").autocomplete({                 
                source: function (request, response) {                        
                      $.ajax({
                          url: '<%=ResolveUrl("../List_Service.asmx/GetAccountType_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) {
                                  return {
                                      label: item.split('-')[0],
                                      val: item.split('-')[1],                                      
                                  } 
                              })) 
                          }, 
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      $("#<%=txtAcc_Code_Hide.ClientID %>").val(i.item.val);                      
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtAcc_Code_Hide.ClientID %>").val(i.item.label);
                      }
              }); 
          }
</script>
<%--Account Type Select List Script End--%>

<%--Account Group Name Select List Script Saart--%>    
<script type="text/javascript">
         $(document).ready(function () {
            initializer_Account_GroupName();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Account_GroupName);
        });
        function initializer_Account_GroupName() {
            $("#<%=txtAccGroupName.ClientID %>").autocomplete({                 
                source: function (request, response) {                        
                      $.ajax({
                          url: '<%=ResolveUrl("../List_Service.asmx/GetAccountGroupName_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) {
                                  return {
                                      label: item.split('-')[0],
                                      val: item.split('-')[1],                                      
                                  } 
                              })) 
                          }, 
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      $("#<%=txtAcc_Code_Hide.ClientID %>").val(i.item.val);                      
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtAcc_Code_Hide.ClientID %>").val(i.item.label);
                      }
              }); 
          }
</script>
<%--Account Group Name Select List Script End--%>

<div class="page-breadcrumb">
    <ol class="breadcrumb container">
        <h4><li class="active">Account Head</li></h4> 
    </ol>
</div>
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
        <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Account Head</h4>
				</div>
			</div>
				<form class="form-horizontal">
				<div class="panel-body">
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">A/C Code<span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtAccCode" MaxLength="30" class="form-control" runat="server" Style="text-transform: uppercase"></asp:TextBox>
                                <asp:TextBox ID="txtAcc_Code_Hide" class="form-control" runat="server" Visible="false"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtAccCode" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <%--<asp:LinkButton ID="btnSearch" class="btn btn-primary btn-addon  btn-rounded btn-sm" runat="server" OnClick="btnSearch_Click"><i class="fa fa-search"></i>Search</asp:LinkButton>--%>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">A/C Description<span class="mandatory">*</span></label>
					            <asp:TextBox ID="txtAccDesc" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
							    <asp:RequiredFieldValidator ControlToValidate="txtAccDesc" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">A/C Type</label>
					            <asp:TextBox ID="txtAccType" class="form-control" runat="server"></asp:TextBox>
					        </div>					        
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">A/C Group Name</label>
					            <asp:TextBox ID="txtAccGroupName" class="form-control" runat="server"></asp:TextBox>
					        </div>
					    </div>
					</div>
					<!-- Button start -->
					<div class="col-md-12">
					    <div class="row">
					        <div class="txtcenter">
                                <div class="col-sm-11">
                                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                    <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                                </div>
                            </div>
					    </div>
					</div>                            
                    <!-- Button End -->
                    <div class="form-group row"></div>
                    
                    <!-- table start -->
                    <div class="col-md-12">
		                <div class="row">
		                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
		                        <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>A/C Desc</th>
                                                <th>A/C Type</th>
                                                <th>Group Name</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("AccCode")%></td>
                                        <td><%# Eval("AccDesc")%></td>
                                        <td><%# Eval("AccType")%></td>
                                        <td><%# Eval("AccGroupName")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("AccCode")%>'>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("AccCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this account head details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
		                    </asp:Repeater>		                    
		                </div>
		            </div>
                    <!-- table End -->
					
				    </div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		</div><!-- col-9 end -->
		    
	    <div class="col-md-2"></div>
		<!-- Dashboard start -->
		<div class="col-lg-3 col-md-6">
            <div class="panel panel-white" style="height: 100%;">
                <div class="panel-heading">
                    <h4 class="panel-title">Dashboard Details</h4>
                    <div class="panel-control">
                    </div>
                </div>
                <div class="panel-body">
                </div>
            </div>
        </div>  
                        
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-white">
                <div class="panel-body">
                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                       
                    </div>
                </div>
            </div>
            
        </div> 
		<!-- Dashboard End -->
		<div class="col-md-2"></div>
		
      </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 
</asp:Content>

