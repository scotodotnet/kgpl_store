﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="costcenter.aspx.cs" Inherits="master_forms_costcenter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();               
            }
        });
    };
</script>
<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);

    }
</script>

<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">Cost Center</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        
            <div class="col-md-9">
			<div class="panel panel-white">
				<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Cost Center</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
				    <div class="col-md-12">
				        <div class="row">
				            <div class="form-group col-md-4">
				                <label for="exampleInputName">Code <span class="mandatory">*</span></label>
				                <asp:TextBox ID="txtCostCenterCode" MaxLength="30" class="form-control" runat="server" Style="text-transform: uppercase"></asp:TextBox>
				                <asp:RequiredFieldValidator ControlToValidate="txtCostCenterCode" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
				            </div>
				            <div class="form-group col-md-4">
				                <label for="exampleInputName">Cost Center Name <span class="mandatory">*</span></label>
							    <asp:TextBox ID="txtCostcenterName" class="form-control" runat="server"></asp:TextBox>
							    <asp:RequiredFieldValidator ControlToValidate="txtCostcenterName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>						
				            </div>
				            <div class="form-group col-md-4">
				                <label for="exampleInputName">Machine Name <span class="mandatory">*</span></label>
							    <asp:TextBox ID="txtElementName" class="form-control" runat="server"></asp:TextBox>
							    <asp:RequiredFieldValidator ControlToValidate="txtElementName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>						
				            </div>
				        </div>
				    </div>
				    <div class="col-md-12">
				        <div class="row">
				         
                                  <div class="form-group col-md-4">
					            <label for="exampleInputName">Division<span class="mandatory">*</span></label>
					            
					            <asp:DropDownList ID="txtDivision" runat="server" class="js-states form-control" 
                                          AutoPostBack="true" onselectedindexchanged="txtDivision_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ControlToValidate="txtDivision" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                </div>
                                 <div class="form-group col-md-4">
					            <label for="exampleInputName">Department<span class="mandatory">*</span></label>
					            
					            <asp:DropDownList ID="txtDepartmentName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ControlToValidate="txtDepartmentName" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                </div>
				            <div class="form-group col-md-4">
				                <label for="exampleInputName">Description </label>
				                <asp:TextBox ID="txtDescription" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
				                <%--<asp:RequiredFieldValidator ControlToValidate="txtDescription" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
				            </div>
				        </div>
				    </div>
				    <!-- Button start -->
					<div class="col-md-12">
					    <div class="row">
					        <div class="txtcenter">
                                <div class="col-sm-11">
                                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                    <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                                </div>
                            </div>
					    </div>
					</div>                            
                    <!-- Button End -->
						
					<!-- table start --> 
					<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
		            <div class="form-group row"></div>
		            <div class="col-md-12">
		                <div class="row">
		                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
		                        <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Cost Center Name</th>
                                                <th>Machine Name</th>
                                                <th>Description</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("CostCenterCode")%></td>
                                        <td><%# Eval("CostCenterName")%></td>
                                        <td><%# Eval("ElementName")%></td>
                                        <td><%# Eval("Description")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("CostCenterCode")%>'>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("CostCenterCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this cost center details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
		                    </asp:Repeater>		                    
		                </div>
		            </div>						
                    <!-- table end -->
                    
				</div><!-- panel body end -->
				</form>
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
		     
		    
		   <div class="col-md-2"></div>
	
  </div><!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 
 
</asp:Content>

