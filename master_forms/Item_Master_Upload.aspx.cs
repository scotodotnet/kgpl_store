﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class master_forms_Item_Master_Upload : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
    }
    protected void btnupload_Click(object sender, EventArgs e)
    {
        DataTable DT = new DataTable();
        string query = "";

        string ItemCode = ""; int ActiveMode;
        string ItemShortName = ""; string ItemLongName = "";
        string PurchaseUOM = ""; string SalesUOM = "";
        string StockUOM = ""; string ManufacturingUOM = "";
        string ItemType = ""; string ValuationType = "";
        string ExciseDet = ""; string Bed = "";
        string AED = ""; string SED = "";
        string Modvat = ""; string FormType = "";
        string ReorderLevel = ""; decimal ReorderQty;
        decimal MaxQtyLevel; decimal MinQtyLevel;
        decimal SafetyQtyLevel; string Item = "";
        string Consumtion = ""; string Sales = "";
        string PriceVariance = ""; string Adiusment = "";
        decimal StandardCost; decimal PurchaseRate;
        decimal SalesRate; int AnalyzInformation;
        string MovingType = ""; string PurchaseType = "";

        bool ErrFlag = false;

       try
        {
            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

                string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                DataTable dts = new DataTable();

                using (sSourceConnection)
                {

                    sSourceConnection.Open();
                    OleDbCommand command = new OleDbCommand("select * FROM [Sheet1$];", sSourceConnection);
                    sSourceConnection.Close();

                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        command.CommandText = "select * FROM [Sheet1$];";
                        sSourceConnection.Open();
                    }
                    using (OleDbDataReader dr = command.ExecuteReader())
                    {

                        if (dr.HasRows)
                        {

                        }

                    }
                    OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                    objDataAdapter.SelectCommand = command;
                    DataSet ds = new DataSet();

                    objDataAdapter.Fill(ds);
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                    }

                    string constr = ConfigurationManager.AppSettings["ConnectionString"];
                    if (dt.Rows.Count > 0)
                    {
                    //for (int j = 0; j < dt.Rows.Count; j++)
                    //{
                    //    SqlConnection cn = new SqlConnection(constr);
                    //    ItemCode = dt.Rows[j][0].ToString();
                    //    ActiveMode = Convert.ToInt32(dt.Rows[j][1]);
                    //    ItemShortName = dt.Rows[j][2].ToString();
                    //    ItemLongName = dt.Rows[j][3].ToString();
                    //    PurchaseUOM = dt.Rows[j][4].ToString();
                    //    SalesUOM = dt.Rows[j][5].ToString();
                    //    StockUOM = dt.Rows[j][6].ToString();
                    //    ManufacturingUOM = dt.Rows[j][7].ToString();
                    //    ItemType = dt.Rows[j][8].ToString();
                    //    ValuationType = dt.Rows[j][9].ToString();
                    //    ExciseDet = dt.Rows[j][10].ToString();
                    //    Bed = dt.Rows[j][11].ToString();
                    //    AED = dt.Rows[j][12].ToString();
                    //    SED = dt.Rows[j][13].ToString();
                    //    Modvat = dt.Rows[j][14].ToString();
                    //    FormType = dt.Rows[j][15].ToString();
                    //    ReorderLevel = dt.Rows[j][16].ToString();
                    //    ReorderQty = Convert.ToDecimal(dt.Rows[j][17]);
                    //    MaxQtyLevel = Convert.ToDecimal(dt.Rows[j][18]);
                    //    MinQtyLevel = Convert.ToDecimal(dt.Rows[j][19]);
                    //    SafetyQtyLevel = Convert.ToDecimal(dt.Rows[j][20]);
                    //    Item = dt.Rows[j][21].ToString();
                    //    Consumtion = dt.Rows[j][22].ToString();
                    //    Sales = dt.Rows[j][23].ToString();
                    //    PriceVariance = dt.Rows[j][24].ToString();
                    //    Adiusment = dt.Rows[j][25].ToString();
                    //    StandardCost = Convert.ToDecimal(dt.Rows[j][26]);
                    //    PurchaseRate = Convert.ToDecimal(dt.Rows[j][27]);
                    //    SalesRate = Convert.ToDecimal(dt.Rows[j][28]);
                    //    AnalyzInformation = Convert.ToInt32(dt.Rows[j][29]);
                    //    MovingType = dt.Rows[j][30].ToString();
                    //    PurchaseType = dt.Rows[j][31].ToString();

                    //    if (ItemCode == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ItemCode. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (ActiveMode == 0)
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ActiveMode. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (ItemShortName == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ItemShortName. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (ItemLongName == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ItemLongName. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (PurchaseUOM == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter PurchaseUOM. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (SalesUOM == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter SalesUOM. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (StockUOM == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter StockUOM. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (ManufacturingUOM == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ManufacturingUOM. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (ItemType == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ItemType. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (ValuationType == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ValuationType. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (ExciseDet == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ExciseDet. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (Modvat == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Modvat. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (Convert.ToString(ReorderQty) == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ReorderQty. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (Convert.ToString(MaxQtyLevel) == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter MaxQtyLevel. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (Convert.ToString(MinQtyLevel) == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter MinQtyLevel. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (Item == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Item. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (MovingType == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter MovingType. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //    else if (PurchaseType == "")
                    //    {
                    //        j = j + 2;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter PurchaseType. The Row Number is " + j + "');", true);
                    //        //System.Windows.Forms.MessageBox.Show("Enter the Employee Name. the Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    //        ErrFlag = true;
                    //    }
                    //}
            if (!ErrFlag)
            {

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string Division="";
                string DepartmentName="";

                string DivisionVal="";
                string DepartmentNameVal="";


                ItemShortName = dt.Rows[i][0].ToString();
                DivisionVal=dt.Rows[i][1].ToString();
                Division = dt.Rows[i][2].ToString();
                DepartmentNameVal=dt.Rows[i][3].ToString();
                DepartmentName=dt.Rows[i][4].ToString();

                ItemCode = "";
                DataTable DT_Auto = new DataTable();
                string ItemCodeAuto = "";
                string Zero_Join = "";
                query = "Select * from MstItemCodeNumber";
                DT_Auto = objdata.RptEmployeeMultipleDetails(query);
                if (DT_Auto.Rows.Count != 0)
                {
                    ItemCodeAuto = DT_Auto.Rows[0]["ItemCode"].ToString();
                }

                if (ItemCodeAuto != "")
                {
                    if (ItemCodeAuto.Length < 4)
                    {
                        for (int j = ItemCodeAuto.Length; j <= 3; j++)
                        {
                            Zero_Join = Zero_Join + "0";
                        }
                    }

                    string Div_str = Division.Substring(0, 1);
                    string Dept_str = DepartmentName.Substring(0, 1);
                    //string Cost_str = txtCostCenter.SelectedItem.Text.Substring(0, 1);
                    string Item_str = ItemShortName.Substring(0, 1);


                    ItemCode = Item_str + Zero_Join + ItemCodeAuto + "-" + DivisionVal + Div_str + "-" + DepartmentNameVal + Dept_str;   //+"-" + txtCostCenter.SelectedValue + Cost_str;

                    //txtitem_code.Text = ItemCode;

                    string New_Last_No = "";
                    New_Last_No = (Convert.ToDecimal(ItemCodeAuto) + Convert.ToDecimal(1)).ToString();
                    query = "Update MstItemCodeNumber Set ItemCode='" + New_Last_No + "'";
                    objdata.RptEmployeeMultipleDetails(query);


                    ActiveMode = 1;
                    ItemLongName = ItemShortName;
                    PurchaseUOM = "Numbers";
                    SalesUOM = "Numbers";
                    StockUOM = "Numbers";
                    ManufacturingUOM = "Numbers";
                    ItemType = "Spare Parts";
                    ValuationType = "FIFO";
                    ExciseDet = "1";
                    Bed = "";
                    AED = "";
                    SED = "";
                    Modvat = "1";
                    FormType = "";
                    ReorderLevel = "";
                    MaxQtyLevel = 0;
                    MinQtyLevel = 0;
                    StandardCost = 0;
                    PurchaseRate = 0;
                    SalesRate = 0;
                        AnalyzInformation =1;
                    MovingType = "Medium";
                    PurchaseType = "Normal";

                    //query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ItemCode + "'";
                    //DT = objdata.RptEmployeeMultipleDetails(query);

                    //if (DT.Rows.Count == 0)
                    //{



                    query = "Insert Into MstItemMaster(Ccode,Lcode,ItemCode,ActiveMode,ItemShortName,ItemLongName,PurchaseUOM,SalesUOM,StockUOM,ManufacturingUOM,";
                    query = query + " ItemType,ValuationType,ExciseDet,Bed,AED,SED,Modvat,FormType,ReorderLevel,ReorderQty,MaxQtyLevel,MinQtyLevel,SafetyQtyLevel,";
                    query = query + " Item,Consumtion,Sales,PriceVariance,Adiusment,StandardCost,PurchaseRate,SalesRate,AnalyzInformation,MovingType,PurchaseType,";
                    query = query + "DeptCode,DeptName,DivisionCode,Division,DrawingNo,CatalogueNo,";
                    query = query + "UserID,UserName) Values('" + SessionCcode + "',";
                    query = query + " '" + SessionLcode + "','" + ItemCode + "','" + ActiveMode + "','" + ItemShortName + "','" + ItemLongName + "',";
                    query = query + " '" + PurchaseUOM + "','" + SalesUOM + "','" + StockUOM + "','" + ManufacturingUOM + "','" + ItemType + "',";
                    query = query + " '" + ValuationType + "','" + ExciseDet + "','" + Bed + "','" + AED + "','" + SED + "','" + Modvat + "',";
                    query = query + " '" + FormType + "','" + ReorderLevel + "','0','" + MaxQtyLevel + "','" + MinQtyLevel + "','0',";
                    query = query + " '" + Item + "','" + Consumtion + "','" + Sales + "','" + PriceVariance + "','" + Adiusment + "',";
                    query = query + " '" + StandardCost + "','" + PurchaseRate + "','" + SalesRate + "','" + AnalyzInformation + "','" + MovingType + "','" + PurchaseType + "',";
                    query = query + "'" + DepartmentNameVal + "','" + DepartmentName + "','" + DivisionVal + "','" + Division + "','','',";
                    query = query + " '" + SessionUserID + "','" + SessionUserName + "')";

                    objdata.RptEmployeeMultipleDetails(query);


                    //}
                    //else
                    //{
                    //    ErrFlag = true;
                    //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Item Already Exists');", true);
                    //    lblUploadSuccessfully.Text = "Item Already Exists";
                    //}




                }                  
         }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Upload Successfully...');", true);
            
       }
    }
      ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your File Not Upload. Check the file..');", true);
    }
  }
}

catch (Exception ex)
  {
      ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...'" + ex.Message + ");", true);
  }
    
  }
}