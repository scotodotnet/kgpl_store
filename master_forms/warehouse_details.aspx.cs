﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class master_forms_warehouse_details : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Warehouse and Zone";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Master"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        Load_Data();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Warehouse and Zone");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Warehouse & Zone Details...');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Warehouse and Zone");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Warehouse & Zone...');", true);
            }
        }
        //User Rights Check End

        if (!ErrFlag)
        {
            query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseCode.Text + "' and BinName='" + txtBinName.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                if (btnSave.Text == "Update")
                {
                    SaveMode = "Update";
                    query = "Delete from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseCode.Text + "' and BinName='" + txtBinName.Text + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    SaveMode = "Error";

                }

            }
            if (SaveMode != "Error")
            {
                //Insert Compnay Details
                query = "Insert Into MstWarehouse(Ccode,Lcode,WarehouseCode,WarehouseName,ZoneName,BinName,Description,UserID,Username)";
                query = query + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtWarehouseCode.Text + "',";
                query = query + "'" + txtWarehouseName.Text + "','" + txtBinName.Text + "','" + txtBinName.Text + "','" + txtDescription.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);

                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Warehouse Details Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Warehouse Details Updated Successfully');", true);
                }


                Clear_All_Field();
                btnSave.Text = "Save";
                txtWarehouseCode.Enabled = true;
                Load_Data();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Warehouse Details Already Saved');", true);
            }
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtWarehouseCode.Text = ""; txtWarehouseName.Text = ""; txtZoneName.Text = ""; txtBinName.Text = ""; txtDescription.Text = "";
        btnSave.Text = "Save";
        txtWarehouseCode.Enabled = true;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseCode.Text + "' and BinName='" + txtBinName.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtWarehouseName.Text = DT.Rows[0]["WarehouseName"].ToString();
            txtZoneName.Text = DT.Rows[0]["ZoneName"].ToString();
            txtBinName.Text = DT.Rows[0]["BinName"].ToString();
            txtDescription.Text = DT.Rows[0]["Description"].ToString();
            txtWarehouseCode.Enabled = false;
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }
    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        txtWarehouseCode.Text = e.CommandName.ToString();
        txtBinName.Text = e.CommandArgument.ToString();
        btnSearch_Click(sender, e);
    }
    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "3", "2", "Warehouse and Zone");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New Warehouse..');", true);
        }
        //User Rights Check End

        if (!ErrFlag)
        {
            DataTable dtdPOreceipt = new DataTable();
            DataTable dtdORreceipt = new DataTable();
            DataTable dtdUPreceipt = new DataTable();
            DataTable dtdCSreceipt = new DataTable();

            query = "select WarehouseCode,WarehouseName from Pur_Order_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and WarehouseCode='" + e.CommandName.ToString() + "'";
            dtdPOreceipt = objdata.RptEmployeeMultipleDetails(query);

            query = "select WarehouseCode,WarehouseName from Opening_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and WarehouseCode='" + e.CommandName.ToString() + "'";
            dtdORreceipt = objdata.RptEmployeeMultipleDetails(query);

            query = "select WarehouseCode,WarehouseName from Unplanned_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and WarehouseCode='" + e.CommandName.ToString() + "'";
            dtdUPreceipt = objdata.RptEmployeeMultipleDetails(query);

            query = "select WarehouseCode,WarehouseName from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' and WarehouseCode='" + e.CommandName.ToString() + "'";
            dtdCSreceipt = objdata.RptEmployeeMultipleDetails(query);

            if ((dtdPOreceipt.Rows.Count == 0) && (dtdORreceipt.Rows.Count == 0) && (dtdUPreceipt.Rows.Count == 0) && (dtdCSreceipt.Rows.Count == 0))
            {
                DataTable DT = new DataTable();
                query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + e.CommandName.ToString() + "' and BinName='" + e.CommandArgument.ToString() + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    query = "Delete from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + e.CommandName.ToString() + "' and BinName='" + e.CommandArgument.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Warehouse Details Deleted Successfully');", true);
                    Load_Data();
                    Clear_All_Field();
                }
            }
            else
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Sorry WareHouse Name Available in some other table..');", true);
            }
        }
    }
}
