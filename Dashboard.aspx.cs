﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;

using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Dashboard : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Dashboard";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_DashBoard"));
            li.Attributes.Add("class", "droplink active open");
            Year_Add();
            Dept_Chart_Load();
            Fast_Moving_Item_Chart_Load();
            ToP_Purchased_Item_Chart();
        }
    }

    private void Year_Add()
    {
        txtYear.Items.Clear();
        int currentYear = Utility.GetFinancialYear;
        for (int i = 0; i < 10; i++)
        {
            txtYear.Items.Add(new ListItem(currentYear.ToString(), currentYear.ToString()));
            currentYear = currentYear - 1;
        }
    }

    private void Dept_Chart_Load()
    {
        DataTable dt = new DataTable();
        string query = "";
        query = "Select sum(Stock_Qty) as Stock_Qty,WarehouseName from Stock_Current_All";
        query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "'";
        query = query + " group by WarehouseName Order by WarehouseName Asc";
        dt = objdata.RptEmployeeMultipleDetails(query);
        DeptChart.DataSource = dt;
        DeptChart.Series["Series1"].XValueMember = "WarehouseName";
        DeptChart.Series["Series1"].YValueMembers = "Stock_Qty";
        this.DeptChart.Series[0]["PieLabelStyle"] = "Outside";
        //this.DeptChart.ChartAreas[0].Area3DStyle.Enable3D = true;
        //this.DeptChart.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.DeptChart.Series[0].BorderWidth = 1;
        this.DeptChart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.DeptChart.Legends.Add("Legend1");
        this.DeptChart.Legends[0].Enabled = true;
        this.DeptChart.Legends[0].Docking = Docking.Bottom;
        this.DeptChart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.DeptChart.Series[0].LegendText = "#PERCENT{P0}";
        //this.DeptChart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        DeptChart.DataBind();
    }

    private void Fast_Moving_Item_Chart_Load()
    {
        DataTable dt = new DataTable();
        string query = "";
        query = "Select Top 10 sum(MIS.IssueQty) as issue_Qty,MIS.ItemName from Meterial_Issue_Main_Sub MIS";
        query = query + " inner join Meterial_Issue_Main MIM on MIS.Ccode=MIM.Ccode And MIS.Lcode=MIM.Lcode And MIS.Mat_Issue_No=MIM.Mat_Issue_No";
        query = query + " where MIS.Ccode='" + SessionCcode + "' And MIS.Lcode='" + SessionLcode + "' And MIS.FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And MIM.Ccode='" + SessionCcode + "' And MIM.Lcode='" + SessionLcode + "' And MIM.FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And MIM.Mat_Issue_Status='1'";
        query = query + " group by MIS.ItemName Order by sum(MIS.IssueQty) Desc";
        dt = objdata.RptEmployeeMultipleDetails(query);
        FastMovingItemChart.DataSource = dt;
        FastMovingItemChart.Series["Series1"].XValueMember = "ItemName";
        FastMovingItemChart.Series["Series1"].YValueMembers = "issue_Qty";
        this.FastMovingItemChart.Series[0]["PieLabelStyle"] = "Outside";
        this.FastMovingItemChart.Series[0].BorderWidth = 1;
        this.FastMovingItemChart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.FastMovingItemChart.Legends.Add("Legend1");
        this.FastMovingItemChart.Legends[0].Enabled = true;
        this.FastMovingItemChart.Legends[0].Docking = Docking.Bottom;
        this.FastMovingItemChart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.FastMovingItemChart.Series[0].LegendText = "#PERCENT{P0}";
        //this.FastMovingItemChart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        FastMovingItemChart.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

        //int monthInDigit = DateTime.ParseExact(txtMonth.Text, "MMMMM", CultureInfo.InvariantCulture).Month;
        //DateTime Month_First_Date = new DateTime(Convert.ToInt32(txtYear.Text), monthInDigit, 1);
        //DateTime Month_Last_Date = new DateTime(Convert.ToInt32(txtYear.Text), monthInDigit, 1).AddMonths(1).AddDays(-1);

        //DateTime Month_First_Date = DateTime.ParseExact(Month_From_Date, "dd-MM-yyyy", null);
        //DateTime Month_Last_Date = DateTime.ParseExact(Month_To_Date, "dd-MM-yyyy", null);
        DeptPurchaseCostChart.Legends.Clear();
        DataTable dt = new DataTable();
        string query = "";
        query = "Select SPOS.DeptName,sum(SPOS.LineTotal) as Purchase_Amt from Unplanned_Receipt_Main_Sub SPOS";
        query = query + " inner join Unplanned_Receipt_Main SPOM on SPOM.UnPlan_Recp_No=SPOS.UnPlan_Recp_No And SPOM.Ccode=SPOS.Ccode And SPOM.Lcode=SPOS.Lcode";
        query = query + " where SPOS.Ccode='" + SessionCcode + "' And SPOS.Lcode='" + SessionLcode + "'";
        query = query + " And SPOM.Ccode='" + SessionCcode + "' And SPOM.Lcode='" + SessionLcode + "'";
        query = query + " And SPOM.FinYearCode='" + SessionFinYearCode + "' And SPOM.FinYearCode='" + SessionFinYearCode + "'";
        query = query + " And year(CONVERT(DATETIME,SPOS.UnPlan_Recp_Date, 103))='" + txtYear.Text + "' And year(CONVERT(DATETIME,SPOM.UnPlan_Recp_Date, 103))='" + txtYear.Text + "'";

        if (txtMonth.Text != "--Select--")
        {
            query = query + " And DATENAME(MONTH, CONVERT(DATETIME,SPOS.UnPlan_Recp_Date, 103))='" + txtMonth.Text + "' And DATENAME(MONTH, CONVERT(DATETIME,SPOM.UnPlan_Recp_Date, 103))='" + txtMonth.Text + "'";
        }
        query = query + " group by SPOS.DeptName";

        dt = objdata.RptEmployeeMultipleDetails(query);
        DeptPurchaseCostChart.DataSource = dt;
        DeptPurchaseCostChart.Series["Series1"].XValueMember = "DeptName";
        DeptPurchaseCostChart.Series["Series1"].YValueMembers = "Purchase_Amt";
        this.DeptPurchaseCostChart.Series[0]["PieLabelStyle"] = "Outside";
        this.DeptPurchaseCostChart.Series[0].BorderWidth = 1;
        this.DeptPurchaseCostChart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.DeptPurchaseCostChart.Legends.Add("Legend1");
        this.DeptPurchaseCostChart.Legends[0].Enabled = true;
        this.DeptPurchaseCostChart.Legends[0].Docking = Docking.Bottom;
        this.DeptPurchaseCostChart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.DeptPurchaseCostChart.Series[0].LegendText = "#PERCENT{P2}";
        //this.DeptPurchaseCostChart.Series[0].LegendText = "#VALX :#PERCENT{P1}";
        //this.DeptPurchaseCostChart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        DeptPurchaseCostChart.DataBind();

    }

    private void ToP_Purchased_Item_Chart()
    {
        FastPurchaseItemChart.Legends.Clear();
        DataTable dt = new DataTable();
        string query = "";
        query = "Select Top 10 SPOS.DeptName,sum(SPOS.LineTotal) as Purchase_Amt from Unplanned_Receipt_Main_Sub SPOS";
        query = query + " inner join Unplanned_Receipt_Main SPOM on SPOM.UnPlan_Recp_No=SPOS.UnPlan_Recp_No And SPOM.Ccode=SPOS.Ccode And SPOM.Lcode=SPOS.Lcode";
        query = query + " where SPOS.Ccode='" + SessionCcode + "' And SPOS.Lcode='" + SessionLcode + "'";
        query = query + " And SPOM.Ccode='" + SessionCcode + "' And SPOM.Lcode='" + SessionLcode + "'";
        query = query + " And SPOM.FinYearCode='" + SessionFinYearCode + "' And SPOM.FinYearCode='" + SessionFinYearCode + "'";
        //query = query + " And year(SPOS.Std_PO_Date)='" + txtYear.Text + "' And year(SPOM.Std_PO_Date)='" + txtYear.Text + "'";

        //if (txtMonth.Text != "--Select--")
        //{
        //    query = query + " And DATENAME(MONTH, SPOS.Std_PO_Date)='" + txtMonth.Text + "' And DATENAME(MONTH, SPOM.Std_PO_Date)='" + txtMonth.Text + "'";
        //}
        query = query + " group by SPOS.DeptName";

        dt = objdata.RptEmployeeMultipleDetails(query);
        FastPurchaseItemChart.DataSource = dt;
        FastPurchaseItemChart.Series["Series1"].XValueMember = "DeptName";
        FastPurchaseItemChart.Series["Series1"].YValueMembers = "Purchase_Amt";
        this.FastPurchaseItemChart.Series[0]["PieLabelStyle"] = "Outside";
        this.FastPurchaseItemChart.Series[0].BorderWidth = 1;
        this.FastPurchaseItemChart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.FastPurchaseItemChart.Legends.Add("Legend1");
        this.FastPurchaseItemChart.Legends[0].Enabled = true;
        this.FastPurchaseItemChart.Legends[0].Docking = Docking.Bottom;
        this.FastPurchaseItemChart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.FastPurchaseItemChart.Series[0].LegendText = "#PERCENT{P2}";
        //this.FastPurchaseItemChart.Series[0].LegendText = "#VALX :#PERCENT{P1}";
        //this.FastPurchaseItemChart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        FastPurchaseItemChart.DataBind();
    }
}