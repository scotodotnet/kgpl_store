﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="purchase_request_approval.aspx.cs" Inherits="Transaction_purchase_request_approval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>

<%--<script type="text/javascript">
    function Drop_Down_Add_Item(Comp_Code) {
    

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '<%=ResolveUrl("../List_Service.asmx/GetDeptFull_Det") %>',
                data: "{ 'prefix': '" + Comp_Code + "'}",
                dataType: "json",
                success: function(Result) {
                var dropDownListRef = document.getElementById('<%= txtDeptName_Test.ClientID %>');
                    Result = Result.d;
                    $.each(Result, function(key, value) {
                    var option1 = document.createElement("option");
                    option1.text = value.split('|')[1];
                    option1.value = value.split('|')[1];
                    dropDownListRef.options.add(option1);
                    });
                    // Another way of writing
                    //  for (var i = 0; i < Result.length; i++) {
                    // $("#ddlcountry").append("<option value=" + Result[i].ID + ">" + 
                    //     Result[i].Name + "</option>");
                    //  }

                    // One more way of writing
                    // for (var i in Result) {
                    //  $("#ddlcountry").append($("<option></option>").val(Result[i].ID).
                    //   text(Result[i].Name));
                    //  }

                },
                error: function(Result) {
                    alert("Error");
                }
            });
       
    }
</script>--%>


<%--<script type="text/javascript">
    $(document).ready(function() {
        $("#<%=txtDeptCode_test.ClientID%>").change(function() {
            var selectedOption = $('#<%=txtDeptCode_test.ClientID %>').val();

            /*var dropDownListRef = document.getElementById('<%= txtDeptName_Test.ClientID %>');
            var option1 = document.createElement("option");
            option1.text = selectedOption;
            option1.value = selectedOption;
            dropDownListRef.options.add(option1);
            */
            Drop_Down_Add_Item('A');
            //alert("sd" + selectedOption);
        });
    });
</script>--%>

<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Purchase Request Approval</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Purchase Request Approval</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					
					<div class="form-group row">							 
						
					
						<label for="input-Default" class="col-sm-2 control-label">Request Status</label>
						<div class="col-sm-3">
                            <asp:DropDownList ID="txtRequestStatus" runat="server" class="js-states form-control"
                                onselectedindexchanged="txtRequestStatus_SelectedIndexChanged" AutoPostBack="True">
                            </asp:DropDownList>
                            
						</div>
                    </div>
                    
                    <%--Test Code--%>
                    <%--<div class="form-group row">							 
						<label for="input-Default" class="col-sm-2 control-label">Department</label>
						<div class="col-sm-4">
                            <asp:DropDownList ID="txtDeptCode_test" runat="server" class="form-control">
                            </asp:DropDownList>
						</div>
						<div class="col-md-1"></div>
						<label for="input-Default" class="col-sm-2 control-label">Dept Name</label>
						<div class="col-sm-3">
                            <asp:DropDownList ID="txtDeptName_Test" runat="server" class="form-control">
                            </asp:DropDownList>
                            
						</div>
                    </div>--%>
                    <%--Test Code--%>
                    
                    
                    <div class="form-group row"></div>
		            <!-- table start -->
			        <div class="col-md-12">
			            <div class="row">
			                <asp:Repeater ID="Repeater_Pending" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Trans No</th>
                                                <th>Date</th>
                                                <th>Type</th>
                                                <th>Supplier</th>
                                                <th>Tot Qty</th>
                                                <th>Net Amt</th>
                                                <th>Approve</th>
                                                <th>Cancel</th>
                                                <th>View</th>
                                               </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("Transaction_No")%></td>
                                        <td><%# Eval("Date")%></td>
                                        <td><%# Eval("Type_Request_Amend")%></td>
                                        <td><%# Eval("SupplierName")%></td>
                                        <td><%# Eval("TotalQuantity")%></td>
                                        <td><%# Eval("TotalAmt")%></td>
                                       <%-- <td><%# Eval("Approvedby")%></td>--%>
                                        
                                        <td>
                                            <asp:LinkButton ID="btnApproveRequest" class="btn btn-success btn-sm fa fa-check-square"  runat="server" 
                                                Text="" OnCommand="GridApproveRequestClick" CommandArgument='<%# Eval("Transaction_No")%>' CommandName='<%# Eval("Type_Request_Amend")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this Purchase Request details?');">
                                            </asp:LinkButton>
                                         </td>
                                         <td>
                                           <asp:LinkButton ID="LinkButton1" class="btn btn-success btn-sm fa fa-check-square"  runat="server" 
                                                Text="" OnCommand="GridCancelRequestClick" CommandArgument='<%# Eval("Transaction_No")%>' CommandName='<%# Eval("Type_Request_Amend")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this Purchase Request details?');">
                                            </asp:LinkButton>
                                         </td>
                                          
                                        <td>
                                        <asp:LinkButton ID="btnReq_View" class="btn btn-primary btn-sm fa fa-external-link-square"  runat="server" 
                                                        Text="" OnCommand="GridEditPurRequestClick" CommandArgument='<%# Eval("Transaction_No")%>' CommandName='<%# Eval("Type_Request_Amend")%>'>
                                        </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
			            </div>
			        </div>
			        <!-- table End -->
			        
			        <!-- table start -->
			        <div class="col-md-12">
			            <div class="row">
			                <asp:Repeater ID="Repeater_Approve" runat="server" EnableViewState="false" Visible="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                               <th>Trans No</th>
                                                <th>Date</th>
                                                <th>Type</th>
                                                <th>Supplier</th>
                                                <th>Tot Qty</th>
                                                <th>Net Amt</th>
                                               
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("Transaction_No")%></td>
                                        <td><%# Eval("Date")%></td>
                                        <td><%# Eval("Type_Request_Amend")%></td>
                                        <td><%# Eval("SupplierName")%></td>
                                        <td><%# Eval("TotalQuantity")%></td>
                                        <td><%# Eval("TotalAmt")%></td>
                                        
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
			            </div>
			        </div>
			        <!-- table End -->
			        
			         <!-- table start -->
			        <div class="col-md-12">
			            <div class="row">
			                <asp:Repeater ID="Repeater_Rejected" runat="server" EnableViewState="false" Visible="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                               <th>Trans No</th>
                                                <th>Date</th>
                                                <th>Type</th>
                                                <th>Supplier</th>
                                                <th>Tot Qty</th>
                                                <th>Net Amt</th>
                                               
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("Transaction_No")%></td>
                                        <td><%# Eval("Date")%></td>
                                        <td><%# Eval("Type_Request_Amend")%></td>
                                        <td><%# Eval("SupplierName")%></td>
                                        <td><%# Eval("TotalQuantity")%></td>
                                        <td><%# Eval("TotalAmt")%></td>
                                        
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
			            </div>
			        </div>
			        <!-- table End -->
						
                   </div><!-- panel body end -->     
				</form>
			
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		     <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		   <div class="col-md-2"></div>
		    
            
      
         
 </div>     <!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 
   
 
 
</div>

</asp:Content>

