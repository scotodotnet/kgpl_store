﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_purchase_request_approval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Purchase Request Approval";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
            //Department_Code_Add();
            purchase_Request_Status_Add();
            //Department_Code_Add_Test();
        }
        Load_Data_Empty_Grid();
    }

    protected void GridApproveRequestClick(object sender, CommandEventArgs e)
    {
        string query = "";
         //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Purchase Request Approval");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approved Purchase Request..');", true);
        //}
        //User Rights Check End


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + e.CommandArgument.ToString() + "' And Status = '2'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already Cancelled.. Cannot Approved it..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + e.CommandArgument.ToString() + "' And Status = '1'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already get Approved..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + e.CommandArgument.ToString() + "' And Status = '1'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count == 0)
            {
                //Delete Main Table
                query = "Update Pur_Request_Approval set Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Transaction_No='" + e.CommandArgument.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                if (e.CommandName.ToString() == "Amend")
                {
                    query = "Update Pur_Request_Amend_Main set Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Amend_No='" + e.CommandArgument.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    DataTable DT_Req = new DataTable();
                    query = "select Pur_Request_No from Pur_Request_Amend_Main where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Amend_No='" + e.CommandArgument.ToString() + "'";
                    DT_Req = objdata.RptEmployeeMultipleDetails(query);

                    query = "Update Pur_Request_Main set Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Pur_Request_No='" + DT_Req.Rows[0]["Pur_Request_No"].ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                }
                else if (e.CommandName.ToString() == "Request")
                {
                    query = "Update Pur_Request_Main set Status='1',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Pur_Request_No='" + e.CommandArgument.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else if (e.CommandName.ToString() == "Std/PO")
                {
                    query = "Update Std_Purchase_Order_Main set PO_Status='5' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    DataTable DT_STD_PO = new DataTable();
                    query="select *from Pur_Request_Approval where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Transaction_No='" + e.CommandArgument.ToString() + "'";
                    DT_STD_PO = objdata.RptEmployeeMultipleDetails(query);

                    //Insert Purchase Order Approval Table
                    query = "Insert Into Std_Purchase_Order_Approve(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,TotalAmt,UserID,UserName) Values('" + SessionCcode + "',";
                    query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + DT_STD_PO.Rows[0]["Transaction_No"].ToString() + "','" + DT_STD_PO.Rows[0]["Date"].ToString() + "','Std/PO','" + DT_STD_PO.Rows[0]["SupplierName"].ToString() + "','" + DT_STD_PO.Rows[0]["TotalQuantity"].ToString() + "','" + DT_STD_PO.Rows[0]["TotalAmt"].ToString() + "',";
                    query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(query);

                }
                else if (e.CommandName.ToString() == "Blnk/PO")
                {
                    query = "Update Blanket_Purchase_Order_Main set Blanket_PO_Status='5' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Blanket_PO_No='" + e.CommandArgument.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    DataTable DT_Blnk_PO = new DataTable();
                    query = "select *from Pur_Request_Approval where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Transaction_No='" + e.CommandArgument.ToString() + "'";
                    DT_Blnk_PO = objdata.RptEmployeeMultipleDetails(query);

                    //Insert Purchase Order Approval Table
                    query = "Insert Into Std_Purchase_Order_Approve(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_PO,SupplierName,TotalQuantity,UserID,UserName) Values('" + SessionCcode + "',";
                    query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + DT_Blnk_PO.Rows[0]["Transaction_No"].ToString() + "','" + DT_Blnk_PO.Rows[0]["Date"].ToString() + "','Blnk/PO','" + DT_Blnk_PO.Rows[0]["SupplierName"].ToString() + "','" + DT_Blnk_PO.Rows[0]["TotalQuantity"].ToString() + "',";
                    query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
                    objdata.RptEmployeeMultipleDetails(query);

                }

                
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Approved Successfully');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request not is Avail give input correctly..');", true);
            }
        }
    }



    protected void GridCancelRequestClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Purchase Request Approval");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approved Purchase Request..');", true);
        //}
        //User Rights Check End

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + e.CommandArgument.ToString() + "' And Status = '1'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already Approved.. Cannot Cancelled it..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }
        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + e.CommandArgument.ToString() + "' And Status = '2'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Already get Cancelled..');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
        }


        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + e.CommandArgument.ToString() + "' And Status = '2'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count == 0)
            {
                //Delete Main Table
                query = "Update Pur_Request_Approval set Status='2',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Transaction_No='" + e.CommandArgument.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                if (e.CommandName.ToString() == "Amend")
                {
                    query = "Update Pur_Request_Amend_Main set Status='2',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Amend_No='" + e.CommandArgument.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    DataTable DT_Req = new DataTable();
                    query = "select Pur_Request_No from Pur_Request_Amend_Main where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Amend_No='" + e.CommandArgument.ToString() + "'";
                    DT_Req = objdata.RptEmployeeMultipleDetails(query);

                    query = "Update Pur_Request_Main set Status='2',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Pur_Request_No='" + DT_Req.Rows[0]["Pur_Request_No"].ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                }
                else if (e.CommandName.ToString() == "Request")
                {
                    query = "Update Pur_Request_Main set Status='2',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Pur_Request_No='" + e.CommandArgument.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else if (e.CommandName.ToString() == "Std/PO")
                {
                    query = "Update Std_Purchase_Order_Main set PO_Status='6',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Std_PO_No='" + e.CommandArgument.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else if (e.CommandName.ToString() == "Blnk/PO")
                {
                    query = "Update Blanket_Purchase_Order_Main set Blanket_PO_Status='6',Approvedby='" + SessionUserName + "' where Ccode='" + SessionCcode + "' And FinYearCode='" + SessionFinYearCode + "' And Lcode='" + SessionLcode + "' And Blanket_PO_No='" + e.CommandArgument.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }


                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Cancelled Successfully');", true);
                txtRequestStatus_SelectedIndexChanged(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request not is Avail give input correctly..');", true);
            }
        }
    }


   
    private void Load_Data_Empty_Grid()
    {
       
        string query = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();
       

        DT.Columns.Add("Transaction_No");
        DT.Columns.Add("Date");
        DT.Columns.Add("Type_Request_Amend");
        DT.Columns.Add("SupplierName");
        DT.Columns.Add("TotalQuantity");
        DT.Columns.Add("TotalAmt");
        DT.Columns.Add("Approvedby");

        
        if (txtRequestStatus.Text == "Pending List")
        {
            query = "Select Transaction_No,Date,Type_Request_Amend,SupplierName,TotalQuantity,TotalAmt,Approvedby from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And (Status = '0' or Status is null) Order By Transaction_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);
           
            Repeater_Pending.DataSource = DT;
            Repeater_Pending.DataBind();
            Repeater_Pending.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Approved List")
        {
            query = "Select Transaction_No,Date,Type_Request_Amend,SupplierName,TotalQuantity,TotalAmt from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '1' Order By Transaction_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);

            
            Repeater_Approve.DataSource = DT;
            Repeater_Approve.DataBind();
            Repeater_Approve.Visible = true;
            Repeater_Pending.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Rejected List")
        {
            query = "Select Transaction_No,Date,Type_Request_Amend,SupplierName,TotalQuantity,TotalAmt from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '2' Order By Transaction_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_Rejected.DataSource = DT;
            Repeater_Rejected.DataBind();
            Repeater_Rejected.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Pending.Visible = false;
        }
    }

    //private void Department_Code_Add()
    //{
    //    string query = "";
    //    DataTable Main_DT = new DataTable();
    //    txtDepartment.Items.Clear();
    //    query = "Select (DeptName + '-' + DeptCode) as DpetName_Join from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeptName Asc";
    //    Main_DT = objdata.RptEmployeeMultipleDetails(query);
    //    txtDepartment.Items.Add("-Select-");
    //    for (int i = 0; i < Main_DT.Rows.Count; i++)
    //    {
    //        txtDepartment.Items.Add(Main_DT.Rows[i]["DpetName_Join"].ToString());
    //    }
    //}

    private void purchase_Request_Status_Add()
    {
        txtRequestStatus.Items.Clear();
       
        txtRequestStatus.Items.Add("Pending List");
        txtRequestStatus.Items.Add("Approved List");
        txtRequestStatus.Items.Add("Rejected List");
    }

    protected void txtRequestStatus_SelectedIndexChanged(object sender, EventArgs e)
    {

        //string[] Department_Spilit = txtDepartment.Text.Split('-');
        //string Department_Code = "";
        //string Department_Name = "";
        string query = "";
        DataTable DT = new DataTable();
        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();

        DT.Columns.Add("Transaction_No");
        DT.Columns.Add("Date");
        DT.Columns.Add("Type_Request_Amend");
        DT.Columns.Add("SupplierName");
        DT.Columns.Add("TotalQuantity");
        DT.Columns.Add("TotalAmt");
        DT.Columns.Add("Approvedby");

        

       if (txtRequestStatus.Text == "Pending List")
        {
            query = "Select Transaction_No,Date,Type_Request_Amend,SupplierName,TotalQuantity,TotalAmt,Approvedby from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And (Status = '0' or Status is null) Order By Transaction_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);
           
            Repeater_Pending.DataSource = DT;
            Repeater_Pending.DataBind();
            Repeater_Pending.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Approved List")
        {
            query = "Select Transaction_No,Date,Type_Request_Amend,SupplierName,TotalQuantity,TotalAmt from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '1' Order By Transaction_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);

            
            Repeater_Approve.DataSource = DT;
            Repeater_Approve.DataBind();
            Repeater_Approve.Visible = true;
            Repeater_Pending.Visible = false;
            Repeater_Rejected.Visible = false;
        }
        else if (txtRequestStatus.Text == "Rejected List")
        {
            query = "Select Transaction_No,Date,Type_Request_Amend,SupplierName,TotalQuantity,TotalAmt from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Status = '2' Order By Transaction_No Asc";
            DT = objdata.RptEmployeeMultipleDetails(query);


            Repeater_Rejected.DataSource = DT;
            Repeater_Rejected.DataBind();
            Repeater_Rejected.Visible = true;
            Repeater_Approve.Visible = false;
            Repeater_Pending.Visible = false;
        }
    }

    //protected void txtDepartment_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (txtDepartment.Text != "-Select-")
    //    {
    //        txtRequestStatus_SelectedIndexChanged(sender, e);
    //    }
    //}

    //private void Department_Code_Add_Test()
    //{
    //    string query = "";
    //    DataTable Main_DT = new DataTable();
    //    txtDeptCode_test.Items.Clear();
    //    query = "Select DeptCode from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by DeptCode Asc";
    //    Main_DT = objdata.RptEmployeeMultipleDetails(query);
    //    txtDeptCode_test.Items.Add("-Select-");
    //    for (int i = 0; i < Main_DT.Rows.Count; i++)
    //    {
    //        txtDeptCode_test.Items.Add(Main_DT.Rows[i]["DeptCode"].ToString());
    //    }
    //}


    protected void GridEditPurRequestClick(object sender, CommandEventArgs e)
    {
        string PurRequest_No_Str=e.CommandArgument.ToString();
        string PurRequest_Type = e.CommandName.ToString();
        if (PurRequest_Type == "Request")
        {
            Session.Remove("Pur_RequestNo_Approval");
            Session["Pur_RequestNo_Approval"] = PurRequest_No_Str;
            Response.Redirect("purchase_request.aspx");
        }
        else if (PurRequest_Type == "Amend")
        {
            Session.Remove("Pur_RequestNo_Approval");
            Session.Remove("Pur_Request_No_Amend_Approval");
            Session["Pur_Request_No_Amend_Approval"] = PurRequest_No_Str;
            Response.Redirect("purchase_request_amend.aspx");
        }
        else if (PurRequest_Type == "Std/PO")
        {
            Session.Remove("Pur_RequestNo_Approval");
            Session["Pur_RequestNo_Approval"] = PurRequest_No_Str;
            Response.Redirect("~/Purchase_Order/standard_po.aspx");
        }
        else if (PurRequest_Type == "Blnk/PO")
        {
            Session.Remove("Pur_RequestNo_Approval");
            Session["Pur_RequestNo_Approval"] = PurRequest_No_Str;
            Response.Redirect("~/Purchase_Order/blanket_po.aspx");
        }



    }

    


}
