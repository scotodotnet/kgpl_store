﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="purchase_request_amend.aspx.cs" Inherits="Transaction_purchase_request_amend" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#PurRequestNo').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#PurRequestNo').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

<script>
        $(document).ready(function () {
            $('#ItemTable').dataTable();
        });
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable').dataTable();               
            }
        });
    };
</script>


<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
    $find('PurRequestNo_Close').hide();
    $find('Item_Close').hide();
     }
   } 
 </script>
<!--Ajax popup End-->



<%--Purchase Request No Select List Script Saart--%>    
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_PurRequestNo();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_PurRequestNo);
        });
        function initializer_PurRequestNo() {
            $("#<%=txtPurRequestNo.ClientID %>").autocomplete({                 
                source: function (request, response) {
                      $.ajax({
                          url: '<%=ResolveUrl("../List_Service.asmx/GetPurRequestNoSelect_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) { 
                                  return {                                  
                                      label: item.split('|')[0],
                                      val: item.split('|')[0],
                                      pur_request_date:item.split('|')[1],
                                      Othrs:item.split('|')[2],
                                      Approvedby:item.split('|')[3],
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtPurRequestNo.ClientID %>").val(i.item.val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtPurRequestNo.ClientID %>").val(i.item.val);
                      $("#<%=txtDate.ClientID %>").val(i.item.pur_request_date);
                      $("#<%=txtOthers.ClientID %>").val(i.item.Othrs);
                      $("#<%=txtApprovedby.ClientID %>").val(i.item.Approvedby);
                   return false;
                  }
              }); 
          }
      </script>--%>
<%--Purchase Request No Select List Script End--%>

<%--Item Select List Script Saart--%>    
<%--    <script type="text/javascript">
         $(document).ready(function () {
            initializer_Item();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Item);
        });
        function initializer_Item() {
            $("#<%=txtItemCode.ClientID %>").autocomplete({                 
                source: function (request, response) {
                      $.ajax({
                          url: '<%=ResolveUrl("../List_Service.asmx/GetPurRequestItemCodeSelect_Det") %>',
                          data: "{ 'prefix': '" + request.term + "','Request_No': '" + $('#<%=txtPurRequestNo.ClientID %>').val() + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) { 
                                  return {                                  
                                      label: item.split('-')[0] + "-" + item.split('-')[1],
                                      val: item.split('-')[1],
                                      text_val:item.split('-')[0],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtItemCode.ClientID %>").val(i.item.val);
                      //$("#<%=txtItemName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtItemName.ClientID %>").val(i.item.text_val);
                      $("#<%=txtItemCode.ClientID %>").val(i.item.val);
                   return false;
                  }
              }); 
          }
      </script>--%>
<%--Item Select List Script End--%>

<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Purchase Request Amendment</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
        
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Purchase Request Amendment</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Request No<span class="mandatory">*</span></label>
					            
					             <asp:DropDownList ID="txtPurRequestNo" runat="server" class="js-states form-control" AutoPostBack="true" 
                                    onselectedindexchanged="txtPurRequestNo_SelectedIndexChanged">
                                </asp:DropDownList>
					            
					            <%--<asp:TextBox ID="txtPurRequestNo" MaxLength="20" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					             <asp:Button ID="btnPurRequestNo" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnPurRequestNo_Click"/>--%>
                                
                                 <%--<cc1:ModalPopupExtender ID="modalPop_PurRequestNo"  runat="server" PopupControlID="Panel5" TargetControlID="btnPurRequestNo"
                       CancelControlID="BtnPurRequest_Close" BackgroundCssClass="modalBackground" BehaviorID="PurRequestNo_Close">
                    </cc1:ModalPopupExtender>--%>
                         <%--<asp:Panel ID="Panel5" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Purchase Request Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_PurRequestNo" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="PurRequestNo" class="display table">
                         <thead >
                         <tr>
                         <th>PurRequestNo</th>
                         <th>PurRequestDate</th>
                         <th>Others</th>
                         <th>Approvedby</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("PurRequestNo")%></td>
                          <td><%# Eval("PurRequestDate")%></td>
                          <td><%# Eval("Others")%></td>
                          <td><%# Eval("Approvedby")%></td>                                                 
                          <td>
                          <asp:LinkButton ID="btnEdit_PurRequestNo" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_PurRequestNo" CommandArgument='<%# Eval("PurRequestNo")%>' CommandName='<%# Eval("PurRequestDate")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnPurRequest_Close" class="btn btn-rounded" runat="server" Text="Close" />
					     </div>
					     </asp:Panel>--%>
					            
					            
					            <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtPurRequestNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					            
					            
					            <%--<asp:RequiredFieldValidator ControlToValidate="txtPurRequestNo" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Date</label>
					            <asp:TextBox ID="txtDate" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Amend No</label>
					            <asp:Label ID="txtAmendNo" runat="server" class="form-control"></asp:Label>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Amend Date</label>
					            <asp:TextBox ID="txtPurAmendDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtPurAmendDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        </div>
					        </div>
					        
					        
				     <div class="col-md-12">
					    <div class="row">
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Amend by</label>
					            <select name="txtAmendby" id="txtAmendby" class="js-states form-control" runat="server">
	                            </select>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Approved by</label>
					            <asp:TextBox ID="txtApprovedby" class="form-control" runat="server" ReadOnly="true"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Others</label>
					            <asp:TextBox ID="txtOthers" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
					        </div>
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					        
					    </div>
					</div>
					
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Code</label>
                                <asp:TextBox ID="txtItemCode" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>  
                                
                                 <asp:Button ID="btnItemCode" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnItemCode_Click"/>
                                
                         <cc1:ModalPopupExtender ID="modalPop_ItemCode"  runat="server" PopupControlID="Panel6" TargetControlID="btnItemCode"
                       CancelControlID="BtnItemClose" BackgroundCssClass="modalBackground" BehaviorID="Item_Close">
                    </cc1:ModalPopupExtender>
                        
                         <asp:Panel ID="Panel6" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                        Item Details
                    </div>
                     <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_ItemCode" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="ItemTable" class="display table">
                         <thead >
                         <tr>
                         <th>ItemCode</th>
                         <th>ItemName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ItemCode")%></td>
                          <td><%# Eval("ItemName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEdit_ItemCode" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ItemCode" CommandArgument='<%# Eval("ItemCode")%>' CommandName='<%# Eval("ItemName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnItemClose" class="btn btn-rounded" runat="server" Text="Close" />
					     </div>
					     </asp:Panel>
                              
                                
                                
                                <asp:RequiredFieldValidator ControlToValidate="txtItemCode" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>                              
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Name</label>
                                <asp:TextBox ID="txtItemName" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtItemName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator> 
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Amend Qty</label>
                                <asp:TextBox ID="txtAmendQty" class="form-control" runat="server" MaxLength="10"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="txtAmendQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator> 
                                <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtAmendQty" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        </div>
					        </div>
					        <div class="col-md-12">
					    <div class="row">
					        
					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Amend Date</label>
					            <asp:TextBox ID="txtAmendDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtAmendDate" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Item Remarks</label>
					            <asp:TextBox ID="txtItemRemarks" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-4">
					            <br />
					            <asp:Button ID="btnAddItem" class="btn btn-success"  runat="server" Text="Add" ValidationGroup="Item_Validate_Field" OnClick="btnAddItem_Click"/>
					        </div>
					    </div>
					</div>
					
					<!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>S. No</th>
                                                <th>Item Code</th>
                                                <th>Item Name</th>
                                                <th>UOM Code</th>
                                                <th>Reuired Qty</th>
                                                <th>Reuired Date</th>
                                                <th>Amend Qty</th>
                                                <th>Amend Date</th>
                                                <th>Remarks</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("ItemCode")%></td>
                                        <td><%# Eval("ItemName")%></td>
                                        <td><%# Eval("UOMCode")%></td>
                                        <td><%# Eval("ReuiredQty")%></td>
                                        <td><%# Eval("ReuiredDate")%></td>
                                        <td><%# Eval("AmendQty")%></td>
                                        <td><%# Eval("AmendDate")%></td>
                                        <td><%# Eval("ItemRemarks")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("ItemCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
					
					    <!-- Button start -->
                        <div class="txtcenter">
                            <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                            <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                            <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                            <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" Text="Back" OnClick="btnBackRequest_Click"/>
                            <asp:Button ID="btnBackReqApprv" class="btn btn-success" runat="server" 
                                Text="Back" Visible="false" onclick="btnBackReqApprv_Click"/>
                        </div>
                        <!-- Button end -->
                        
                    </div><!-- panel body end -->      
				</form>
			
			</div><!-- panel white end -->
		    </div><!-- col-9 end -->
		    
		    <!-- Dashboard start -->
		     <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                     </div>
                                </div>
                                <div class="panel-body">
                               </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
		    <!-- Dashboard End -->
		    
		    <div class="col-md-2"></div>
		    
            
      
         
 </div>     <!-- col 12 end -->
  </div><!-- row end -->
 </div><!-- main-wrapper end -->
 


</asp:Content>

