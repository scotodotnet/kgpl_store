﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;
using System.Windows.Forms;

public partial class Transaction_Purchase_Enquiry : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEnquiryNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Purchase Enquiry";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            if (Session["Enquiry_No"] == null)
            {
                SessionEnquiryNo = "";
            }
            else
            {
                SessionEnquiryNo = Session["Enquiry_No"].ToString();
                txtEnquiryCode.Text = SessionEnquiryNo;
                btnSearch_Click(sender, e);
            }
            //GridViewClick(sender, e);
            
        }
        
        Load_OLD_data();
        //Load_Data_Empty_Supplier();
        
        Load_Data_Empty_Supp1();
        Load_Data_Empty_Supp2();
        Load_Data_Empty_Supp3();
        Load_Data_Empty_Supp4();
        Load_Data_Empty_Supp5();
        Load_Data_Empty_ItemCode();
        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;
        
        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Purchase Enquiry");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Purchase Enquiry Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Purchase Enquiry");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Purchase Enquiry..');", true);
            }
        }
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Purchase Enquiry", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtEnquiryCode.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {


            query = "Select * from Pur_Enq_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_EnquiryNo='" + txtEnquiryCode.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Pur_Enq_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_EnquiryNo='" + txtEnquiryCode.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Pur_Enq_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_EnquiryNo='" + txtEnquiryCode.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            //txtSuppCode1_test.Value;

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into Pur_Enq_Main(Ccode,Lcode,FinYearCode,FinYearVal,Pur_EnquiryNo,Enquiry_Date,SuppCode1,SupName1,SuppCode2,SupName2,SuppCode3,SupName3,SuppCode4,Supname4,SuppCode5,SupName5,Description,Note,Others,UserID,UserName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtEnquiryCode.Text + "','" + txtDate.Text + "','" + txtSuppCode1.Value + "','" + txtSupplier1.Text + "','" + txtSuppCode2.Value + "','" + txtSupplier2.Text + "','" + txtSuppCode3.Value + "','" + txtSupplier3.Text + "','" + txtSuppCode4.Value + "','" + txtSupplier4.Text + "',";
            query = query + " '" + txtSuppCode5.Value + "','" + txtSupplier5.Text + "','" + txtDescription.Value + "','" + txtNote.Value + "','" + txtOthers.Value + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Pur_Enq_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Pur_EnquiryNo,Enquiry_Date,ItemCode,ItemName,UOMCode,Rate,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtEnquiryCode.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["Rate"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Enquiry Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Enquiry Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Enquiry_No"] = txtEnquiryCode.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtEnquiryCode.Text = ""; txtDate.Text = ""; txtSupplier1.Text = ""; txtSupplier2.Text = "";
        txtSupplier3.Text = ""; txtSupplier4.Text = ""; txtSupplier5.Text = ""; txtDescription.Value = "";
        txtNote.Value = ""; txtOthers.Value = ""; txtItemCode.Text = ""; txtItemName.Text = ""; //txtUOMCode.Text = "";
        txtRate.Text = "";
        txtSuppCode1.Value = ""; txtSuppCode2.Value = ""; txtSuppCode3.Value = ""; txtSuppCode4.Value = ""; txtSuppCode5.Value = "";
        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Enquiry_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        //check with Item Code And Item Name 
        query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCode.Text + "' And ItemShortName='" + txtItemName.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
        }

        if (!ErrFlag)
        {
            //UOM Code get
            string UOM_Code_Str = qry_dt.Rows[0]["PurchaseUOM"].ToString();
            
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCode.Text.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["ItemCode"] = txtItemCode.Text;
                    dr["ItemName"] = txtItemName.Text;
                    dr["UOMCode"] = UOM_Code_Str;
                    dr["Rate"] = txtRate.Text;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    txtItemCode.Text = ""; txtItemName.Text = ""; txtRate.Text = "";
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["ItemCode"] = txtItemCode.Text;
                dr["ItemName"] = txtItemName.Text;
                dr["UOMCode"] = UOM_Code_Str;
                dr["Rate"] = txtRate.Text;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                txtItemCode.Text = ""; txtItemName.Text = ""; txtRate.Text = "";
            }


        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("Rate", typeof(string)));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Pur_Enq_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_EnquiryNo='" + txtEnquiryCode.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["Enquiry_Date"].ToString();
            txtSuppCode1.Value = Main_DT.Rows[0]["SuppCode1"].ToString();
            txtSupplier1.Text = Main_DT.Rows[0]["SupName1"].ToString();
            txtSuppCode2.Value = Main_DT.Rows[0]["SuppCode2"].ToString();
            txtSupplier2.Text = Main_DT.Rows[0]["SupName2"].ToString();
            txtSuppCode3.Value = Main_DT.Rows[0]["SuppCode3"].ToString();
            txtSupplier3.Text = Main_DT.Rows[0]["SupName3"].ToString();
            txtSuppCode4.Value = Main_DT.Rows[0]["SuppCode4"].ToString();
            txtSupplier4.Text = Main_DT.Rows[0]["SupName4"].ToString();
            txtSuppCode5.Value = Main_DT.Rows[0]["SuppCode5"].ToString();
            txtSupplier5.Text = Main_DT.Rows[0]["SupName5"].ToString();
            txtDescription.Value = Main_DT.Rows[0]["Description"].ToString();
            txtNote.Value = Main_DT.Rows[0]["Note"].ToString();
            txtOthers.Value = Main_DT.Rows[0]["Others"].ToString();

            //Pur_Enq_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select ItemCode,ItemName,UOMCode,Rate from Pur_Enq_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_EnquiryNo='" + txtEnquiryCode.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
        
    }

    //private void Load_Data_Enquiry_Grid()
    //{
    //    string query = "";
    //    DataTable DT = new DataTable();
    //    query = "Select Pur_EnquiryNo,Enquiry_Date,SupName1,SupName2 from Pur_Enq_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    //    DT = objdata.RptEmployeeMultipleDetails(query);
    //    Repeater2.DataSource = DT;
    //    Repeater2.DataBind();

    //}

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        txtEnquiryCode.Text = e.CommandName.ToString();
        btnSearch_Click(sender, e);
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Pur_Enq_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_EnquiryNo='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            //Delete Main Table
            query = "Delete from Pur_Enq_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_EnquiryNo='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            //Delete Main Sub Table
            query = "Delete from Pur_Enq_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_EnquiryNo='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Enquiry Details Deleted Successfully');", true);
            //Load_Data_Enquiry_Grid();
            Clear_All_Field();

        }
    }

    protected void btnBackEnquiry_Click(object sender, EventArgs e)
    {
        Response.Redirect("Purchase_Enquiry_Main.aspx");
    }

    private void Transaction_No_Generate(string FormName)
    {
        string Prefix = "";
        string Fin_Year_Join = "";
        string Last_No = "0";
        string query = "";
        string Final_Transaction_No = "";
        string Zero_Join = "";
        DataTable DT = new DataTable();

        query = "Select * from MstNumberingSetup where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FormName='" + FormName + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            Prefix = DT.Rows[0]["Prefix"].ToString();
            Last_No = DT.Rows[0]["EndNo"].ToString();
            Fin_Year_Join = DT.Rows[0]["Suffix"].ToString();

            //Zero Add
            if (Last_No.Length < 4)
            {
                for (int i = Last_No.Length; i <= 4; i++)
                {
                    Zero_Join = Zero_Join + "0";
                }
            }

            Final_Transaction_No = Prefix + Zero_Join + Last_No + Fin_Year_Join;
        }
        else
        {
            Final_Transaction_No = "";
        }

    }

    protected void btnSupp1_Click(object sender, EventArgs e)
    {
        modalPop_Supp1.Show();
    }
    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier1.DataSource = DT;
        Repeater_Supplier1.DataBind();
        Repeater_Supplier1.Visible = true;

    }

    //private void Load_Data_Empty_Supplier()
    //{

    //    string query = "";
    //    DataTable Main_DT = new DataTable();

    //    txtSupplier1.Items.Clear();

    //    query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
    //    Main_DT = objdata.RptEmployeeMultipleDetails(query);
    //    txtSupplier1.DataSource = Main_DT;
    //    DataRow dr = Main_DT.NewRow();
    //    dr["SuppCode"] = "-Select-";
    //    dr["SuppName"] = "-Select-";
    //    Main_DT.Rows.InsertAt(dr, 0);
    //    txtSupplier1.DataTextField = "SuppName";
    //    txtSupplier1.DataValueField = "SuppCode";
    //    txtSupplier1.DataBind();

    //}

    protected void GridViewClick(object sender, CommandEventArgs e)
    {
       // txtSupplier1.SelectedValue = Convert.ToString(e.CommandArgument);
        txtSuppCode1.Value = Convert.ToString(e.CommandArgument);
        //txtSupplier1.SelectedItem.Text = Convert.ToString(e.CommandName);
        txtSupplier1.Text = Convert.ToString(e.CommandName);
    }


    protected void btnSupp2_Click(object sender, EventArgs e)
    {
        modalPop_Supp2.Show();
    }
    private void Load_Data_Empty_Supp2()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier2.DataSource = DT;
        Repeater_Supplier2.DataBind();
        Repeater_Supplier2.Visible = true;

    }

    protected void GridViewClick_Supp2(object sender, CommandEventArgs e)
    {
        txtSuppCode2.Value = Convert.ToString(e.CommandArgument);
        txtSupplier2.Text = Convert.ToString(e.CommandName);
    }


    protected void btnSupp3_Click(object sender, EventArgs e)
    {
        modalPop_Supp3.Show();
    }
    private void Load_Data_Empty_Supp3()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier3.DataSource = DT;
        Repeater_Supplier3.DataBind();
        Repeater_Supplier3.Visible = true;

    }

    protected void GridViewClick_Supp3(object sender, CommandEventArgs e)
    {
        txtSuppCode3.Value= Convert.ToString(e.CommandArgument);
        txtSupplier3.Text = Convert.ToString(e.CommandName);
    }


    protected void btnSupp4_Click(object sender, EventArgs e)
    {
        modalPop_Supp4.Show();
    }
    private void Load_Data_Empty_Supp4()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier4.DataSource = DT;
        Repeater_Supplier4.DataBind();
        Repeater_Supplier4.Visible = true;

    }

    protected void GridViewClick_Supp4(object sender, CommandEventArgs e)
    {
        txtSuppCode4.Value = Convert.ToString(e.CommandArgument);
        txtSupplier4.Text = Convert.ToString(e.CommandName);
    }

    protected void btnSupp5_Click(object sender, EventArgs e)
    {
        modalPop_Supp5.Show();
    }
    private void Load_Data_Empty_Supp5()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_Supplier5.DataSource = DT;
        Repeater_Supplier5.DataBind();
        Repeater_Supplier5.Visible = true;

    }

    protected void GridViewClick_Supp5(object sender, CommandEventArgs e)
    {
        txtSuppCode5.Value = Convert.ToString(e.CommandArgument);
        txtSupplier5.Text = Convert.ToString(e.CommandName);
    }


    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCode.Text= Convert.ToString(e.CommandArgument);
        txtItemName.Text= Convert.ToString(e.CommandName);
    }
       

}
