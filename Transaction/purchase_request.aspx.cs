﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Transaction_purchase_request : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionPurRequestNo;
    string SessionFinYearCode;
    string SessionFinYearVal;
    string SessionPurRequestNoApproval;
    static Decimal ReqQty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Purchase Request";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_Data_Empty_DeptCode();
            
            CostCenter_Add();
            Employee_Code_Add();
            if (Session["Pur_Request_No"] == null)
            {
                SessionPurRequestNo = "";
               
            }
            else
            {
                btnBackReqApprv.Visible = false;             
                SessionPurRequestNo = Session["Pur_Request_No"].ToString();
                txtPurRequestNo.Text = SessionPurRequestNo;
                btnSearch_Click(sender, e);

               
            }

            if (Session["Pur_RequestNo_Approval"] == null)
            {
                SessionPurRequestNoApproval = "";
               
                btnBackReqApprv.Visible = false;
            }
            else
            {
                SessionPurRequestNoApproval = Session["Pur_RequestNo_Approval"].ToString();
                txtPurRequestNo.Text = SessionPurRequestNoApproval;
                btnSearchView_Click(sender, e);

                
            }

            
        }
        Load_OLD_data();
       
        Load_Data_Empty_ItemCode();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }
        if (txtDeptCode.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Department Code...');", true);
        }
        if (txtDeptName.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Department Name...');", true);
        }
        //if (txtCostCenter.Text == "-Select-") 
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Cost Center...');", true);
        //}
        //if (txtCostElement.Value == "-Select-")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Cost Element...');", true);
        //}
        if (txtRequestedby.Value == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Requested by...');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Purchase Request");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Purchase Request Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "5", "2", "Purchase Request");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Purchase Request..');", true);
            }
        }
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "Purchase Request", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtPurRequestNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (btnSave.Text == "Update")
        {
            query = "Select * from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "' And Status='1'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details get Approved Cannot Update it..');", true);
            }
        }



        if (!ErrFlag)
        {
            query = "Select * from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);

                query = "Delete from Pur_Request_Approval where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Transaction_No='" + txtPurRequestNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);

            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into Pur_Request_Main(Ccode,Lcode,FinYearCode,FinYearVal,Pur_Request_No,Pur_Request_Date,DeptCode,DeptName,CostCenter,CostElement,Requestby,Approvedby,Others,TotalReqQty,UserID,UserName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtPurRequestNo.Text + "','" + txtDate.Text + "','" + txtDeptCode.SelectedValue + "','" + txtDeptName.Text + "','" + txtCostCenter.Text + "',";
            query = query + " '" + txtCostElement.Value + "','" + txtRequestedby.Value + "','','" + txtOthers.Text + "','" + ReqQty + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Pur_Request_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,Pur_Request_No,Pur_Request_Date,ItemCode,ItemName,UOMCode,ReuiredQty,ReuiredDate,ItemRemarks,UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtPurRequestNo.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["ReuiredQty"].ToString() + "','" + dt.Rows[i]["ReuiredDate"].ToString() + "','" + dt.Rows[i]["ItemRemarks"].ToString() + "','" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }

          
            
            //Insert Purchase Request Approval Table
            query = "Insert Into Pur_Request_Approval(Ccode,Lcode,FinYearCode,FinYearVal,Transaction_No,Date,Type_Request_Amend,TotalQuantity,UserID,UserName) Values('" + SessionCcode + "',";
            query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtPurRequestNo.Text + "','" + txtDate.Text + "','Request','" + ReqQty + "',";
            query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);



            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Request Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["Pur_Request_No"] = txtPurRequestNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtPurRequestNo.Text = ""; txtDate.Text = ""; txtDeptCode.SelectedValue = "-Select-"; txtDeptName.Text = "";
        txtCostCenter.Text = "-Select-"; txtCostElement.Items.Clear(); txtRequestedby.Value = "-Select-"; 
        txtApprovedby.Text = ""; txtOthers.Text = ""; txtItemCode.Text = ""; txtItemName.Text = "";
        txtReuiredQty.Text = "";
        
        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("Pur_Request_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (txtReuiredQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Reuired Qty...');", true);
        }
        //check with Item Code And Item Name 
        query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + txtItemCode.Text + "' And ItemShortName='" + txtItemName.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
        }

        if (!ErrFlag)
        {
            //UOM Code get
            string UOM_Code_Str = qry_dt.Rows[0]["PurchaseUOM"].ToString();

            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == txtItemCode.Text.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["ItemCode"] = txtItemCode.Text;
                    dr["ItemName"] = txtItemName.Text;
                    dr["UOMCode"] = UOM_Code_Str;
                    dr["ReuiredQty"] = txtReuiredQty.Text;
                    dr["ReuiredDate"] = txtReuiredDate.Text;
                    dr["ItemRemarks"] = txtItemRemarks.Text;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    TotalReqQty();

                    txtItemCode.Text = ""; txtItemName.Text = ""; txtReuiredQty.Text = ""; txtReuiredDate.Text = "";
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["ItemCode"] = txtItemCode.Text;
                dr["ItemName"] = txtItemName.Text;
                dr["UOMCode"] = UOM_Code_Str;
                dr["ReuiredQty"] = txtReuiredQty.Text;
                dr["ReuiredDate"] = txtReuiredDate.Text;
                dr["ItemRemarks"] = txtItemRemarks.Text;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                TotalReqQty();

                txtItemCode.Text = ""; txtItemName.Text = ""; txtReuiredQty.Text = ""; txtReuiredDate.Text = "";
            }
        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ReuiredDate", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemRemarks", typeof(string)));
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
        TotalReqQty();

    }
    public void TotalReqQty()
    {
       
        ReqQty = 0;
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ReqQty = Convert.ToDecimal(ReqQty) + Convert.ToDecimal(dt.Rows[i]["ReuiredQty"]);
        }
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["Pur_Request_Date"].ToString();
            txtDeptCode.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDeptName.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtCostCenter.Text = Main_DT.Rows[0]["CostCenter"].ToString();
         
            txtCostElement.Value = Main_DT.Rows[0]["CostElement"].ToString();
            txtRequestedby.Value = Main_DT.Rows[0]["Requestby"].ToString();
            txtApprovedby.Text = Main_DT.Rows[0]["Approvedby"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();
            
            //Pur_Enq_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select ItemCode,ItemName,UOMCode,ReuiredQty,ReuiredDate,ItemRemarks from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            TotalReqQty();
            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }

    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("purchase_request_main.aspx");
    }

    protected void txtCostCenter_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        txtCostElement.Items.Clear();
        if (txtCostCenter.Text != "" && txtCostCenter.Text != "-Select-")
        {
            query = "Select * from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And CostcenterName='" + txtCostCenter.Text + "' order by ElementName Asc";
            Main_DT = objdata.RptEmployeeMultipleDetails(query);
            txtCostElement.Items.Add("-Select-");
            for (int i = 0; i < Main_DT.Rows.Count; i++)
            {
                txtCostElement.Items.Add(Main_DT.Rows[i]["ElementName"].ToString());
            }
        }
      
    }

    private void CostCenter_Add()
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        txtCostCenter.Items.Clear();
        query = "Select * from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by CostcenterName Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCostCenter.Items.Add("-Select-");
        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {
            txtCostCenter.Items.Add(Main_DT.Rows[i]["CostcenterName"].ToString());
        }
    }

    private void Employee_Code_Add()
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        txtRequestedby.Items.Clear();
        query = "Select * from MstEmployee where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by EmpName Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtRequestedby.Items.Add("-Select-");
        for (int i = 0; i < Main_DT.Rows.Count; i++)
        {
            txtRequestedby.Items.Add(Main_DT.Rows[i]["EmpName"].ToString());
        }
    }


    protected void btnSearchView_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Pur_Request_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["Pur_Request_Date"].ToString();
            txtDeptCode.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            //txtDeptCode_SelectedIndexChanged(sender,e);
           
            txtDeptName.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtCostCenter.Text = Main_DT.Rows[0]["CostCenter"].ToString();
            txtCostCenter_SelectedIndexChanged(sender, e);
            txtCostElement.Value = Main_DT.Rows[0]["CostElement"].ToString();
            txtRequestedby.Value = Main_DT.Rows[0]["Requestby"].ToString();
            txtApprovedby.Text = Main_DT.Rows[0]["Approvedby"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();

            //Pur_Enq_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select ItemCode,ItemName,UOMCode,ReuiredQty,ReuiredDate,ItemRemarks from Pur_Request_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Pur_Request_No='" + txtPurRequestNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            TotalReqQty();
            btnSave.Visible = false;
            btnBackEnquiry.Visible = false;
            btnBackReqApprv.Visible = true;

        }
        else
        {
            Clear_All_Field();
        }

    }
    protected void btnBackReqApprv_Click(object sender, EventArgs e)
    {
        Session.Remove("Pur_RequestNo_Approval");
        Session.Remove("Pur_Request_No_Amend_Approval");
        Session.Remove("Pur_Request_No");
        Response.Redirect("purchase_request_approval.aspx");
    }


    protected void btnDept_Click(object sender, EventArgs e)
    {
       // modalPop_Dept.Show();
    }
    private void Load_Data_Empty_DeptCode()
    {
        string query = "";
        DataTable Main_DT = new DataTable();


        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);



        txtDeptCode.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDeptCode.DataTextField = "DeptName";
        txtDeptCode.DataValueField = "DeptCode";
        txtDeptCode.DataBind();

    }

  

    protected void GridViewClick_Req(object sender, CommandEventArgs e)
    {
        //txtDeptCode.Text = Convert.ToString(e.CommandArgument);
        //txtDeptName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnItemCode_Click(object sender, EventArgs e)
    {
        modalPop_ItemCode.Show();
    }
    private void Load_Data_Empty_ItemCode()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");

        query = "Select ItemCode,ItemShortName as ItemName from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater_ItemCode.DataSource = DT;
        Repeater_ItemCode.DataBind();
        Repeater_ItemCode.Visible = true;

    }

    protected void GridViewClick_ItemCode(object sender, CommandEventArgs e)
    {
        txtItemCode.Text = Convert.ToString(e.CommandArgument);
        txtItemName.Text = Convert.ToString(e.CommandName);
    }


    protected void txtDeptCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable Main_DT = new DataTable();
        //txtDeptName.Items.Clear();
        if (txtDeptCode.Text != "" && txtDeptCode.Text != "-Select-")
        {
            query = "Select DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptCode='" + txtDeptCode.SelectedValue + "'";
            Main_DT = objdata.RptEmployeeMultipleDetails(query);
            
            for (int i = 0; i < Main_DT.Rows.Count; i++)
            {
                //txtDeptName.Items.Add(Main_DT.Rows[i]["DeptName"].ToString());
                txtDeptName.Text = Main_DT.Rows[i]["DeptName"].ToString();
            }
        }
    }
}
