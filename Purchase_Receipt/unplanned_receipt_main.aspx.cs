﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Receipt_unplanned_receipt_main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;

    string RptName = "";
    string ButtonName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Unplanned Receipt";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Load_Data_Enquiry_Grid();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Unplanned Receipt..');", true);
        }
        else
        {
            Session.Remove("Unplan_Recp_No");
            Response.Redirect("unplanned_receipt.aspx");
        }
    }

    protected void GridbtnPrintClick(object sender, CommandEventArgs e)
    {
        string Enquiry_No_Str = e.CommandName.ToString();

       
        string WareHouse = "-Select-";
        string SupplierName = "-Select-";
       
        string ItemName = "-Select-";
        string CostCenterName = "-Select-";
        string FromDate = "";
        string ToDate = "";


        ButtonName = "Issue Slip";

        RptName = "General Receipt Details Report";

        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?CostCenterName=" + CostCenterName + "&WareHouse=" + WareHouse + "&SupplierName=" + SupplierName + "&UPRNumber=" + Enquiry_No_Str + "&ItemName=" + ItemName + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string Enquiry_No_Str = e.CommandName.ToString();
        Session.Remove("Unplan_Recp_No");
        Session["Unplan_Recp_No"] = Enquiry_No_Str;
        Response.Redirect("unplanned_receipt.aspx");

    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "Unplanned Receipt");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New Unplanned Receipt..');", true);
        }
        //User Rights Check End

        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        query = "Select * from Unplanned_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And UnPlan_Recp_No='" + e.CommandName.ToString() + "' And UnPlan_PO_Receipt_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Delete Unplanned Receipt Details Already Approved..');", true);
        }
        //Check With Already Approved End

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Unplanned_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And UnPlan_Recp_No='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //Delete Main Table
                query = "Delete from Unplanned_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And UnPlan_Recp_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                //Delete Main Sub Table
                query = "Delete from Unplanned_Receipt_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And UnPlan_Recp_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Unplanned Receipt Details Deleted Successfully');", true);
                Load_Data_Enquiry_Grid();
            }
        }
    }

    private void Load_Data_Enquiry_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select UnPlan_Recp_No,UnPlan_Recp_Date,Supp_Name,InvNo,InvDate,DCNo,DCDate from Unplanned_Receipt_Main ";
        query = query + " where Ccode ='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' ";
        //query = query + " Order By CONVERT(DATETIME,UnPlan_Recp_Date, 103) Desc ";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }
}
