﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="po_receipt.aspx.cs" Inherits="Purchase_Receipt_po_receipt" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

<script>
$(document).ready(function () {
    $('#Supplier1').dataTable();
});
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Supplier1').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#WareHouse').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#WareHouse').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#POOrderNo').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#POOrderNo').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ItemTable').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#ZoneName').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ZoneName').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#BinName').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#BinName').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#Dept').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Dept').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

    <script>
        $(document).ready(function () {
            $('#CostCenterName').dataTable();
        });
    </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#CostCenterName').dataTable();               
            }
        });
    };
</script>





<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
     $find('Supp1_Close').hide();
     $find('Dept_Close').hide();
  $find('CostCenterName_Close').hide();
    $find('WareHouse_Close').hide();
    $find('POOrderNo_Close').hide();
     
    $find('ItemCode_Close').hide();
    $find('ZoneName_Close').hide();
    $find('BinName_Close').hide();
     }
   } 
 </script>
<!--Ajax popup End-->




<%--Supplier Name Select List Script Start--%>    
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_Supp_Code();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Supp_Code);
        });
        function initializer_Supp_Code() {
            $("#<%=txtSupplierName.ClientID %>").autocomplete({                 
                source: function (request, response) {                        
                      $.ajax({ 
                          url: '<%=ResolveUrl("../List_Service.asmx/GetSupplierCode_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) {
                                  return {
                                      label: item.split('-')[0] + "-" + item.split('-')[1],
                                      val: item.split('-')[1],
                                      text_val:item.split('-')[0],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtSupplierName.ClientID %>").val(i.item.val);
                      //$("#<%=txtSupplierName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtSupplierName.ClientID %>").val(i.item.text_val);
                      $("#<%=txtSuppCodehide.ClientID %>").val(i.item.val);
                   return false;
                  }
              });
          }
      </script>--%>
<%--Supplier Name Select List Script End--%>

<%--Warehouse Name Select List Script Start--%>    
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_Warehouse_Name();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Warehouse_Name);
        });
        function initializer_Warehouse_Name() {
            $("#<%=txtWarehouseName.ClientID %>").autocomplete({                 
                source: function (request, response) {                        
                      $.ajax({ 
                          url: '<%=ResolveUrl("../List_Service.asmx/GetWarehouse_Det") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) {
                                  return {
                                      label: item.split('-')[0] + "-" + item.split('-')[1],
                                      val: item.split('-')[1],
                                      text_val:item.split('-')[0],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtWarehouseName.ClientID %>").val(i.item.val);
                      //$("#<%=txtWarehouseName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtWarehouseName.ClientID %>").val(i.item.text_val);
                      $("#<%=txtWarehouseCodeHide.ClientID %>").val(i.item.val);
                   return false;
                  }
              });
          }
      </script>--%>
<%--Warehouse Name Select List Script End--%>

<%--PO Order No Select List Script Start--%>    
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_PO_Order_No();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_PO_Order_No);
        });
        function initializer_PO_Order_No() {
            $("#<%=txtPOOrderNo.ClientID %>").autocomplete({                 
                source: function (request, response) {
                      $.ajax({
                          url: '<%=ResolveUrl("../List_Service.asmx/GetPOOrderNoSupplierWise_New") %>',
                          data: "{ 'prefix': '" + request.term + "','Supp_No': '" + $('#<%=txtSuppCodehide.ClientID %>').val() + "','OrderType':'" + $("[id*=RdpReceiptType] input:checked").val() + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) { 
                                  return {                                  
                                      label: item.split('|')[0],
                                      val: item.split('|')[1],
                                      text_val:item.split('|')[0],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtPOOrderNo.ClientID %>").val(i.item.val);
                      //$("#<%=txtPOOrderNo.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtPOOrderNo.ClientID %>").val(i.item.text_val);
                      $("#<%=txtPOOrderDate.ClientID %>").val(i.item.val);
                   return false;
                  }
              }); 
          }
      </script>--%>
<%--PO Order No Select List Script End--%>

<%--Item Select List Script Saart--%>    
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_Item();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_Item);
        });
        function initializer_Item() {
            $("#<%=txtItemName.ClientID %>").autocomplete({                 
                source: function (request, response) {
                      $.ajax({
                          url: '<%=ResolveUrl("../List_Service.asmx/GetItemCodeSearchPurchaseOrderWise_New") %>',
                          data: "{ 'prefix': '" + request.term + "','PO_Order_No': '" + $('#<%=txtPOOrderNo.ClientID %>').val() + "','OrderType':'" + $("[id*=RdpReceiptType] input:checked").val() + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) { 
                                  return {                                  
                                      label: item.split('|')[0],
                                      val: item.split('|')[0],
                                      Item_Code:item.split('|')[1],
                                      Order_Qty:item.split('|')[2],
                                      Item_Rate:item.split('|')[3],
                                      Item_bal:item.split('|')[4],                                      
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtItemName.ClientID %>").val(i.item.val);
                      //$("#<%=txtItemName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtItemName.ClientID %>").val(i.item.val);
                      $("#<%=txtItemCodeHide.ClientID %>").val(i.item.Item_Code);
                      $("#<%=txtOrderQty.ClientID %>").val(i.item.Order_Qty);
                      $("#<%=txtRate.ClientID %>").val(i.item.Item_Rate);
                      $("#<%=txtBal.ClientID %>").val(i.item.Item_bal);
                   return false;
                  }
              }); 
          }
      </script>--%>
<%--Item Select List Script End--%>

<%--Zone Name Select List Script Saart--%>    
    <%--<script type="text/javascript">
         $(document).ready(function () {
            initializer_ZoneName();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_ZoneName);
        });
        function initializer_ZoneName() {
            $("#<%=txtZoneName.ClientID %>").autocomplete({                 
                source: function (request, response) {
                      $.ajax({
                          url: '<%=ResolveUrl("../List_Service.asmx/GetZoneListDet") %>',
                          data: "{ 'prefix': '" + request.term + "','WarehouseName': '" + $('#<%=txtWarehouseCodeHide.ClientID %>').val() + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) { 
                                  return {                                  
                                      label: item.split('-')[0],
                                      val: item.split('-')[0],                                     
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtZoneName.ClientID %>").val(i.item.val);
                      //$("#<%=txtZoneName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtZoneName.ClientID %>").val(i.item.val);
                   return false;
                  }
              }); 
          }
      </script>--%>
<%--Zone Name Select List Script End--%>

<%--Bin Name Select List Script Saart--%>    
   <%-- <script type="text/javascript">
         $(document).ready(function () {
            initializer_BinName();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(initializer_BinName);
        });
        function initializer_BinName() {
            $("#<%=txtBinName.ClientID %>").autocomplete({                 
                source: function (request, response) {
                      $.ajax({
                          url: '<%=ResolveUrl("../List_Service.asmx/GetBinListDet") %>',
                          data: "{ 'prefix': '" + request.term + "','WarehouseName': '" + $('#<%=txtWarehouseCodeHide.ClientID %>').val() + "','ZoneName': '" + $('#<%=txtZoneName.ClientID %>').val() + "'}",
                          dataType: "json",
                          type: "POST",
                          dataFilter: function (data) { return data; },
                          contentType: "application/json; charset=utf-8",
                          delay:0,
                          success: function (data) { 
                              response($.map(data.d, function (item) { 
                                  return {                                  
                                      label: item.split('-')[0],
                                      val: item.split('-')[0],                                     
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  }, minLength: 1,
                  focus: function (e, i) {
                      //$("#<%=txtBinName.ClientID %>").val(i.item.val);
                      //$("#<%=txtBinName.ClientID %>").val(i.item.text_val);
                      return false;
                  },
                  select: function (e, i) {
                      $("#<%=txtBinName.ClientID %>").val(i.item.val);
                   return false;
                  }
              }); 
          }
      </script>--%>
<%--Zone Name Select List Script End--%>

<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Purchase Order Receipt</li></h4> 
    </ol>
</div>
                
                        
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
   
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Purchase Order Receipt</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
				    <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">PO Receipt No<span class="mandatory">*</span></label>
					            <asp:Label ID="txtPOReceiptNo" runat="server" class="form-control"></asp:Label>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Date</label>
					            <asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server" AutoComplete="Off"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Receipt Type</label>
					            <asp:RadioButtonList ID="RdpReceiptType" class="form-control" runat="server" RepeatColumns="2" >
                                <asp:ListItem Value="1" Text="Standard" style="padding-right:40px" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="2" Text="General" ></asp:ListItem>
                                </asp:RadioButtonList>
					        </div>
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">					        
					        <div class="form-group col-md-4">
					            <label for="exampleInputName">Supplier Name</label>
					            <asp:DropDownList ID="ddlSupplier" runat="server" class="js-states form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSupplier_SelectedIndexChanged">
                                </asp:DropDownList>
				            </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Invoice No</label>
					            <asp:TextBox ID="txtInvoiceNo" class="form-control" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtInvoiceNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Invoice Date</label>
					            <asp:TextBox ID="txtInvoiceDate" MaxLength="20" class="form-control date-picker" runat="server" AutoComplete="Off"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtInvoiceDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">DC No</label>
					            <asp:TextBox ID="txtDCNo" class="form-control" runat="server"></asp:TextBox>
					           <%-- <asp:RequiredFieldValidator ControlToValidate="txtDCNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">DC Date</label>
					            <asp:TextBox ID="txtDCDate" MaxLength="20" class="form-control date-picker" runat="server" AutoComplete="Off"></asp:TextBox>
					            <%--<asp:RequiredFieldValidator ControlToValidate="txtDCDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDCDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					    </div>
					</div>
					        
					     
                         
                         <div class="col-md-12">
					    <div class="row"> 
					      <div class="form-group col-md-2">
					            <label for="exampleInputName">GP IN No</label>
					            <asp:TextBox ID="txtGPINNo" class="form-control" runat="server"></asp:TextBox>
					            <%--<asp:RequiredFieldValidator ControlToValidate="txtGPINNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">GP IN Date</label>
					            <asp:TextBox ID="txtGPINDate" MaxLength="20" class="form-control date-picker" runat="server" AutoComplete="Off"></asp:TextBox>
					           <%-- <asp:RequiredFieldValidator ControlToValidate="txtGPINDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator17" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtGPINDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>  
                         <div class="form-group col-md-4">
					            <label for="exampleInputName">Others</label>
					            <asp:TextBox ID="txtOthers" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
					        </div>
					    </div>
					</div>    
					<div class="clearfix"></div>				    
				    <div class="timeline-options">
				        <h4 class="panel-title">Item Details</h4>
				    </div>
                                  
					<div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">PO Receipt No<span class="mandatory">*</span></label>
					            <asp:DropDownList ID="ddlPOrdNo" runat="server" class="js-states form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlPOrdNo_SelectedIndexChanged">
                                </asp:DropDownList>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">PO Date</label>
					            <asp:TextBox ID="txtPOOrderDate" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtPOOrderDate" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator14" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtPOOrderDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Item Name</label>
					            <asp:DropDownList ID="ddlItemName" runat="server" class="js-states form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged"></asp:DropDownList>
					        </div>
					          <div class="form-group col-md-2" runat="server" visible="false">
					        <label for="exampleInputName">Zone Name</label>
					 
					      <asp:DropDownList ID="txtZoneName" runat="server" class="js-states form-control" AutoPostBack="true" onselectedindexchanged="txtZoneName_SelectedIndexChanged">
                          </asp:DropDownList>
					             <%--<asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtZoneName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator17" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator> --%>      
					         
					        </div>

                             <div class="form-group col-md-3">
					            <label for="exampleInputName">Department Name</label>
					            
					            <asp:DropDownList ID="txtDepartmentName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					             <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtDepartmentName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator> 
					           
					        </div>
					        
					        
					        </div>
					       </div> 

                     <div class="col-md-12">
					    <div class="row">  
					        
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Cost Center Name</label>
					            
					            <asp:DropDownList ID="txtCostCenterName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					             <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtCostCenterName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator> 
					        </div>
					        
					        
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Warehouse Name</label>
					            
					             <asp:DropDownList ID="txtWarehouseName" runat="server" 
                                    class="js-states form-control" AutoPostBack="true" 
                                    onselectedindexchanged="txtWarehouseName_SelectedIndexChanged">
                                </asp:DropDownList>
					             <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtWarehouseName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator> 
					        </div>

                            <div class="form-group col-md-3">
					            <label for="exampleInputName">Bin Name</label>
					            <asp:TextBox ID="txtBinName" runat="server" class="form-control"></asp:TextBox>
					     <%-- <asp:DropDownList ID="txtBinName" runat="server" class="js-states form-control">
                          </asp:DropDownList>--%>
					      <asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtBinName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                          </asp:RequiredFieldValidator>
					        
					        </div>

                            <div class="form-group col-md-3">
					            <label for="exampleInputName">Order Qty</label>
					            <asp:TextBox ID="txtOrderQty" class="form-control" runat="server" ></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtOrderQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtOrderQty" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					    </div>
					</div>
					     
					       
					   <div class="col-md-12">
					       <div class="row">
					        
					       
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Balance Qty</label>
					            <%--<asp:Label ID="txtBal" runat="server" class="form-control"></asp:Label>--%>
					           <asp:TextBox ID="txtBal" class="form-control" runat="server"></asp:TextBox>
					            
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Received Qty</label>
					            <asp:TextBox ID="txtReceivedQty" class="form-control" runat="server" 
                                    AutoPostBack="true" ontextchanged="txtReceivedQty_TextChanged"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtReceivedQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator15" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtReceivedQty" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					         <div class="form-group col-md-3">
					            <label for="exampleInputName">Rate</label>
					            <asp:TextBox ID="txtRate" class="form-control" runat="server" 
                                     AutoPostBack="true" ontextchanged="txtRate_TextChanged"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtRate" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtRate" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>					        
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Item Total</label>
					            <asp:TextBox ID="txtItemTotal" runat="server" class="form-control" Text="0.0" AutoPostBack="true"></asp:TextBox>
					        </div>
					       </div>
					    </div>
					    <div class="col-md-12">
					        <div class="row">
					         <div class="form-group col-md-2">
					            <label for="exampleInputName">Disc. Per</label>
					            <asp:TextBox ID="txtDiscount_Per" class="form-control" runat="server" Text="0.0" 
                                     AutoPostBack="true" ontextchanged="txtDiscount_Per_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDiscount_Per" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Discount Amount</label>
					            <asp:Label ID="txtDiscount_Amount" runat="server" class="form-control" Text="0.0"></asp:Label>
					         </div>
					         <div class="form-group col-md-2">
					            <label for="exampleInputName">Packing Amount</label>
					            <asp:TextBox ID="txtPackingAmt" runat="server" class="form-control" Text="0.0" 
                                     AutoPostBack="true" ontextchanged="txtPackingAmt_TextChanged"></asp:TextBox>
					         </div>
					          <div class="form-group col-md-2">
					            <label for="exampleInputName">GST Type</label>
					            <asp:DropDownList ID="txtGST_Type" runat="server" class="form-control" 
                                      AutoPostBack="true" onselectedindexchanged="txtGST_Type_SelectedIndexChanged">
                                <%--<asp:ListItem Value="SGST" Text="SGST"></asp:ListItem>
                                <asp:ListItem Value="IGST" Text="IGST"></asp:ListItem>--%>
                                </asp:DropDownList>
					          </div>
					           <div class="form-group col-md-2">
					            <label for="exampleInputName">CGST %</label>
					            <asp:TextBox ID="txtCGSTPer" class="form-control" runat="server" Text="0.0" 
                                       Enabled="false" AutoPostBack="true" ontextchanged="txtCGSTPer_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtCGSTPer" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					         <div class="form-group col-md-2">
					            <label for="exampleInputName">CGST Amt</label>
					            <asp:TextBox ID="txtCGSTAmt" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
					         </div>
					         </div>
					    </div>
					 <div class="col-md-12">
					    <div class="row">
					    <div class="form-group col-md-2">
					            <label for="exampleInputName">SGST %</label>
					            <asp:TextBox ID="txtSGSTPer" class="form-control" runat="server" Text="0.0" 
                                    Enabled="false" AutoPostBack="true" ontextchanged="txtSGSTPer_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtSGSTPer" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
				                <label for="exampleInputName">SGST Amt</label>
				                <asp:TextBox ID="txtSGSTAmt" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
				            </div>
				             <div class="form-group col-md-2">
					            <label for="exampleInputName">IGST %</label>
					            <asp:TextBox ID="txtIGSTPer" class="form-control" runat="server" Text="0.0" 
                                     Enabled="false" AutoPostBack="true" ontextchanged="txtIGSTPer_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtSGSTPer" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
				                <label for="exampleInputName">IGST Amt</label>
				                <asp:TextBox ID="txtIGSTAmt" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
				            </div>
				            <div class="form-group col-md-2">
					            <label for="exampleInputName">BDU Tax%</label>
					            <asp:TextBox ID="txtBDUTax" class="form-control" runat="server" Text="0.0" 
                                    AutoPostBack="true" ontextchanged="txtBDUTax_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtBDUTax" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					         <div class="form-group col-md-2">
					            <label for="exampleInputName">BDU Amt</label>
					            <asp:TextBox ID="txtBDUAmount" class="form-control" runat="server" Text="0.0"></asp:TextBox>
					            
					        </div>
					        </div>
					    </div>
					 <div class="col-md-12">
					    <div class="row">
					    <div class="form-group col-md-2">
					            <label for="exampleInputName">Tax Per</label>
					            <asp:TextBox ID="txtTax" class="form-control" runat="server" Text="0.0" 
                                    AutoPostBack="true" ontextchanged="txtTax_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTax" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
				                <label for="exampleInputName">Tax Amount</label>
				                <asp:Label ID="txtTaxAmt" runat="server" class="form-control" Text="0.0"></asp:Label>
				            </div>
				            <div class="form-group col-md-2">
					            <label for="exampleInputName">Other Amt</label>
					            <asp:TextBox ID="txtOtherCharge" class="form-control" runat="server" Text="0" 
                                    AutoPostBack="true" ontextchanged="txtOtherCharge_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtOtherCharge" ValidChars="0123456789.-">
                                </cc1:FilteredTextBoxExtender>
                                <%--<asp:HiddenField ID="txtFinal_Total_Amt" runat="server" />--%>
					        </div>
					        <div class="form-group col-md-2">
				                <label for="exampleInputName">Total Amt</label>
				                <asp:Label ID="txtFinal_Total_Amt" runat="server" class="form-control" Text="0.0"></asp:Label>
				            </div>
				             <div class="form-group col-md-1">
					            <br />
					            <asp:Button ID="btnAddItem" Width="50" Height="30" class="btn-success"  runat="server" Text="Add" ValidationGroup="Item_Validate_Field"  OnClick="btnAddItem_Click"/>
					        </div>
					        </div>
					    </div>
					 
					
					<!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>PO Order No</th>
                                                <th>PO Date</th>
                                                <%--<th>Item Code</th>--%>
                                                <th>Item Name</th>
                                                <%--<th>Value Type</th>
                                                <th>UOM Code</th>--%>                                                
                                                <th>Order Qty</th>
                                                <th>Rate</th>
                                                <th>Received Qty</th>
                                                <%--<th>Zone</th>--%>
                                                <th>Bin</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("POOrderNo")%></td>
                                        <td><%# Eval("POOrderDate")%></td>
                                        <%--<td><%# Eval("ItemCode")%></td>--%>
                                        <td><%# Eval("ItemName")%></td>
                                        <%--<td><%# Eval("ValuationType")%></td>
                                        <td><%# Eval("UOMCode")%></td>--%>
                                        <td><%# Eval("OrderQty")%></td>
                                        <td><%# Eval("ItemRate")%></td>
                                        <td><%# Eval("ReceivedQty")%></td>
                                        <%--<td><%# Eval("ZoneName")%></td>--%>
                                        <td><%# Eval("BinName")%></td>                                        
                                        <td>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument='Delete' CommandName='<%# Eval("ItemCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->       
					
					<div class="col-md-12">
					    <div class="row">
					     <div class="form-group col-md-10"></div>
					      <div class="form-group col-md-2">
					            <label for="exampleInputName">Total Amt</label>
					            <asp:TextBox ID="txtTotAmt" class="form-control" runat="server" Text="0.0"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTotAmt" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					    </div>
					    </div>
					<div class="col-md-12">
					    <div class="row">
					      <div class="form-group col-md-4"></div>

                            <div class="form-group col-md-2">
					            <label for="exampleInputName">Other Amt</label>
					            <asp:TextBox ID="txtTotOther" class="form-control" runat="server" Text="0" 
                                    AutoPostBack="true" OnTextChanged="txtTotOther_TextChanged" ></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTotOther" ValidChars="0123456789.-">
                                </cc1:FilteredTextBoxExtender>
					        </div>

                            <div class="form-group col-md-2">
					            <label for="exampleInputName">Packing Amt</label>
					            <asp:TextBox ID="txtTotPackAmt" class="form-control" runat="server" Text="0" 
                                    AutoPostBack="true" OnTextChanged="txtTotPackAmt_TextChanged" ></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTotPackAmt" ValidChars="0123456789.-">
                                </cc1:FilteredTextBoxExtender>
					        </div>

					       <div class="form-group col-md-2">
					            <label for="exampleInputName">Add or Less</label>
					            <asp:TextBox ID="txtAddOrLess" class="form-control" runat="server" Text="0" 
                                    AutoPostBack="true" ontextchanged="txtAddOrLess_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtAddOrLess" ValidChars="0123456789.-">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					       
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Net Amt</label>
					            <asp:Label ID="txtNetAmt" runat="server" class="form-control"></asp:Label>
					           
					        </div>
					    </div>
					 </div>    	
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                        <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                        <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" Text="Back" OnClick="btnBackRequest_Click"/>
                        <asp:Button ID="btnApprove" class="btn btn-success" runat="server" Text="Approve" OnClick="btnApprove_Click"/>
                    </div>
                    <!-- Button end -->
                    
                    </div>
				</form>
			
			</div><!-- panel white end -->

		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 <!-- Dashboard start -->
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 
 </div>
 
</asp:Content>

