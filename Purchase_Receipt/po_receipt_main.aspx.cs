﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Receipt_po_receipt_main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionFinYearCode;
    string SessionFinYearVal;


    bool ErrFlag = false;
    string RptName = "";
    string ButtonName = "";
    Boolean Errflag = true;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Purchase Order Receipt";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        Load_Data_Enquiry_Grid();
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //User Rights Check Start
        bool Rights_Check = false;
        Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "PurchaseOrder Receipt");
        if (Rights_Check == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Purchase Order Receipt..');", true);
        }
        else
        {
            Session.Remove("PO_Receipt_No");
            Response.Redirect("po_receipt.aspx");
        }
    }

    protected void GridPrintClick(object sender, CommandEventArgs e)
    {

        string DeptName = "-Select-";
        string WareHouse = "-Select-";
        string SupplierName = "-Select-";
        string PORNumber = e.CommandName.ToString();
        string ItemName = "-Select-";
        string FromDate = "";
        string ToDate = "";


        RptName = "Purchase Order Receipt Details Report";
        ButtonName = "Pur Ord Slip Report";

        // ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?RptName=" + RptName + "&ButtonName=" + ButtonName + "&InvoiceNo=" + e.CommandName.ToString(), "_blank", "");

        ResponseHelper.Redirect("/Reports/ReportDisplay.aspx?DeptName=" + DeptName + "&WareHouse=" + WareHouse + "&SupplierName=" + SupplierName + "&PORNumber=" + PORNumber + "&ItemName=" + ItemName + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&ButtonName=" + ButtonName + "&RptName=" + RptName, "_blank", "");
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        bool ErrFlag = false;

        DataTable dtdpurchase = new DataTable();
        SSQL = "select PO_Receipt_Status from Pur_Order_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Receipt_No='" + e.CommandName.ToString() + "'";
        dtdpurchase = objdata.RptEmployeeMultipleDetails(SSQL);
        string status = dtdpurchase.Rows[0]["PO_Receipt_Status"].ToString();

        if (status == "" || status == "0")
        {
            string Enquiry_No_Str = e.CommandName.ToString();
            Session.Remove("PO_Receipt_No");
            Session["PO_Receipt_No"] = Enquiry_No_Str;
            Response.Redirect("po_receipt.aspx");
        }
        else if (status == "2")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('General Purchase Order Already Rejected..');", true);
        }
        else if (status == "3")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('General Purchase Order put in pending..');", true);
        }
        else if (status == "1")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approved General Purchase Order Cant Edit..');", true);
        }

    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";

        //User Rights Check Start
        bool ErrFlag = false;
        bool Rights_Check = false;

        Rights_Check = CommonClass_Function.DeleteRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "PurchaseOrder Receipt");
        if (Rights_Check == false)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Delete New Purchase Order Receipt..');", true);
        }
        //User Rights Check End

        //Check With Already Approved Start
        DataTable DT_Check = new DataTable();
        query = "Select * from Pur_Order_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Receipt_No='" + e.CommandName.ToString() + "' And PO_Receipt_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Do not Delete Purchase Order Receipt Details Already Approved..');", true);
        }
        //Check With Already Approved End

        if (!ErrFlag)
        {
            DataTable DT = new DataTable();
            query = "Select * from Pur_Order_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Receipt_No='" + e.CommandName.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //Delete Main Table
                query = "Delete from Pur_Order_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Receipt_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                //Delete Main Sub Table
                query = "Delete from Pur_Order_Receipt_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Receipt_No='" + e.CommandName.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Order Receipt Details Deleted Successfully');", true);
                Load_Data_Enquiry_Grid();
            }
        }
    }

    private void Load_Data_Enquiry_Grid()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select PO_Receipt_No,PO_Receipt_Date,Supp_Name,InvNo,InvDate,DCNo,DCDate,WarehouseName ";
        query = query + "From Pur_Order_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' ";
        query = query + "And FinYearCode='" + SessionFinYearCode + "' Order By CONVERT(DATETIME,PO_Receipt_Date, 103) Desc";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }
}
