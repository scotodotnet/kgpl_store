﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Purchase_Receipt_po_receipt : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    TransactionNoGenerate CommonClass_Function = new TransactionNoGenerate();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionPOReceiptNo;
    string SessionFinYearCode;
    string SessionFinYearVal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionFinYearCode = Session["FinYearCode"].ToString();
        SessionFinYearVal = Session["FinYear"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "ERP Stores Module :: Purchase Order Receipt";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Purchase"));
            li.Attributes.Add("class", "droplink active open");

            Load_Data_GST();
            Load_TaxData();

            Initial_Data_Referesh();
            Load_Data_Empty_Dept();
            Load_Data_Empty_CostCenterName();
            Load_Data_Empty_WareHouse();
            Load_Data_Empty_ZoneName();
            Load_Data_Empty_BinName();

            Load_Data_Empty_Supp1();

            //Load_Data_Empty_POOrderNo();
            //Load_Data_Empty_ItemCode();

            if (Session["PO_Receipt_No"] == null)
            {
                SessionPOReceiptNo = "";
            }
            else
            {
                SessionPOReceiptNo = Session["PO_Receipt_No"].ToString();
                txtPOReceiptNo.Text = SessionPOReceiptNo;
                btnSearch_Click(sender, e);
            }
            txtBal.Attributes.Add("readonly", "readonly");
        }
        Load_OLD_data();
       

    }

    private void Load_Data_GST()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtGST_Type.Items.Clear();
        query = "Select *from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtGST_Type.DataSource = Main_DT;
        //DataRow dr = Main_DT.NewRow();
        //dr["GST_Type"] = "-Select-";
        //dr["GST_Type"] = "-Select-";
        //Main_DT.Rows.InsertAt(dr, 0);
        txtGST_Type.DataTextField = "GST_Type";
        txtGST_Type.DataValueField = "GST_Type";
        txtGST_Type.DataBind();

    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Item Details..');", true);
        }

        //Check Supplier Name
        if (RdpReceiptType.SelectedValue == "1")
        {
            query = "Select * from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Supp_Code='" + ddlSupplier.SelectedValue + "' And Supp_Name='" + ddlSupplier.SelectedItem.Text + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
        }
        else
        {
            query = "Select * from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Supp_Code='" + ddlSupplier.SelectedValue + "' And Supp_Name='" + ddlSupplier.SelectedItem.Text + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
        }
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Supplier Name...');", true);
        }
        //Check Warehouse Name
        query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And WarehouseCode='" + txtWarehouseName.SelectedValue + "' And WarehouseName='" + txtWarehouseName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Warehouse Name...');", true);
        }

        //User Rights Check Start
        bool Rights_Check = false;
        if (btnSave.Text == "Update")
        {
            Rights_Check = CommonClass_Function.ModifyRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "PurchaseOrder Receipt");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Modify Purchase Order Receipt Details..');", true);
            }
        }
        else
        {
            Rights_Check = CommonClass_Function.AddRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "PurchaseOrder Receipt");
            if (Rights_Check == false)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Adding New Purchase Order Receipt..');", true);
            }
        }
        //User Rights Check End

        //Auto generate Transaction Function Call
        if (btnSave.Text != "Update")
        {
            if (!ErrFlag)
            {
                TransactionNoGenerate TransNO = new TransactionNoGenerate();
                string Auto_Transaction_No = TransNO.Auto_Generate_No_Numbering_Setup(SessionCcode, SessionLcode, "PurchaseOrder Receipt", SessionFinYearVal);
                if (Auto_Transaction_No == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Auto Generate Transaction No Error... Contact Server Admin');", true);
                }
                else
                {
                    txtPOReceiptNo.Text = Auto_Transaction_No;
                }
            }
        }

        if (!ErrFlag)
        {
            query = "Select * from Pur_Order_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Receipt_No='" + txtPOReceiptNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Check.Rows.Count != 0)
            {
                SaveMode = "Update";
                query = "Delete from Pur_Order_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Receipt_No='" + txtPOReceiptNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Pur_Order_Receipt_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Receipt_No='" + txtPOReceiptNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            //Response.Write(strValue);
            //Insert Main Table
            query = "Insert Into Pur_Order_Receipt_Main(Ccode,Lcode,FinYearCode,FinYearVal,PO_Receipt_No,PO_Receipt_Date,PO_Receipt_Type,";
            query = query + " Supp_Code,Supp_Name,InvNo,InvDate,DCNo,DCDate,DeptCode,DeptName,CostCenterCode,CostCenterName,WarehouseCode,";
            query = query + " WarehouseName,Others,TotalAmt,TotPackAmt,TotOtherAmt,AddOrLess,NetAmount,GPINNo,GPINDate, UserID,UserName)";
            query = query + " Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "',";
            query = query + " '" + txtPOReceiptNo.Text + "','" + txtDate.Text + "','" + RdpReceiptType.SelectedValue + "',";
            query = query + " '" + ddlSupplier.SelectedValue + "','" + ddlSupplier.SelectedItem.Text + "','" + txtInvoiceNo.Text + "',";
            query = query + " '" + txtInvoiceDate.Text + "','" + txtDCNo.Text + "','" + txtDCDate.Text + "',";
            query = query + " '" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "',";
            query = query + " '" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "',";
            query = query + " '" + txtWarehouseName.SelectedValue + "','" + txtWarehouseName.SelectedItem.Text + "',";
            query = query + " '" + txtOthers.Text + "','" + txtTotAmt.Text + "','" + txtTotPackAmt.Text + "','" + txtTotOther.Text + "',";
            query = query + " '" + txtAddOrLess.Text + "','" + txtNetAmt.Text + "','" + txtGPINNo.Text + "','" + txtGPINDate.Text + "',";
            query = query + " '" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                query = "Insert Into Pur_Order_Receipt_Main_Sub(Ccode,Lcode,FinYearCode,FinYearVal,PO_Receipt_No,PO_Receipt_Date,Pur_Order_No,Pur_Order_Date,ItemCode,ItemName,ValuationType,UOMCode,OrderQty,ItemRate,ReceivedQty,ZoneName,BinName,";
                query = query + "ItemTotal,Discount_Per,Discount,TaxPer,TaxAmount,BDUTaxPer,BDUTaxAmount,";
                query = query + "CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,IGSTPer,IGSTAmount,OtherCharge,LineTotal,PackingAmt,";
                query = query + "UserID,UserName) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + SessionFinYearCode + "','" + SessionFinYearVal + "','" + txtPOReceiptNo.Text + "','" + txtDate.Text + "','" + dt.Rows[i]["POOrderNo"].ToString() + "','" + dt.Rows[i]["POOrderDate"].ToString() + "','" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["ValuationType"].ToString() + "','" + dt.Rows[i]["UOMCode"].ToString() + "','" + dt.Rows[i]["OrderQty"].ToString() + "','" + dt.Rows[i]["ItemRate"].ToString() + "','" + dt.Rows[i]["ReceivedQty"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["BinName"].ToString() + "','" + dt.Rows[i]["BinName"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["ItemTotal"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["Discount_Per"].ToString() + "','" + dt.Rows[i]["Discount"].ToString() + "','" + dt.Rows[i]["TaxPer"].ToString() + "','" + dt.Rows[i]["TaxAmount"].ToString() + "','" + dt.Rows[i]["BDUTaxPer"].ToString() + "','" + dt.Rows[i]["BDUTaxAmount"].ToString() + "',";
                query = query + " '" + dt.Rows[i]["CGSTPer"].ToString() + "','" + dt.Rows[i]["CGSTAmount"].ToString() + "','" + dt.Rows[i]["SGSTPer"].ToString() + "','" + dt.Rows[i]["SGSTAmount"].ToString() + "','" + dt.Rows[i]["IGSTPer"].ToString() + "','" + dt.Rows[i]["IGSTAmount"].ToString() + "',";
                query = query + "'" + dt.Rows[i]["OtherCharge"].ToString() + "','" + dt.Rows[i]["LineTotal"].ToString() + "','" + dt.Rows[i]["PackingAmt"].ToString() + "',";
                query = query + "'" + SessionUserID + "','" + SessionUserName + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Order Receipt Details Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Order Receipt Details Updated Successfully');", true);
            }
            //Clear_All_Field();
            Session["PO_Receipt_No"] = txtPOReceiptNo.Text;
            btnSave.Text = "Update";
            //Load_Data_Enquiry_Grid();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtPOReceiptNo.Text = ""; txtDate.Text = ""; ddlSupplier.SelectedItem.Text = "";
        txtInvoiceNo.Text = ""; txtInvoiceDate.Text = ""; txtDCNo.Text = ""; txtDCDate.Text = "";
        txtWarehouseName.SelectedItem.Text = "-Select-";
        txtDepartmentName.SelectedItem.Text = "-Select-"; txtCostCenterName.SelectedItem.Text = "-Select-";
        txtOthers.Text = ""; RdpReceiptType.SelectedValue = "1";
        ddlPOrdNo.SelectedItem.Text = "-Select-"; txtPOOrderDate.Text = ""; ddlItemName.SelectedItem.Text = "-Select-";
        txtOrderQty.Text = ""; txtRate.Text = ""; txtReceivedQty.Text = ""; txtZoneName.SelectedItem.Text = "-Select-";
        txtBinName.Text = ""; txtBal.Text = "";


        txtGPINNo.Text = ""; txtGPINDate.Text = "";

        txtItemTotal.Text = "0.00";
        txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
        txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00"; txtBDUTax.Text = "0.0"; txtBDUAmount.Text = "0.00";
        txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
        txtFinal_Total_Amt.Text = "0.00"; txtPackingAmt.Text = "0.0";

        txtNetAmt.Text = "0.00";
        txtAddOrLess.Text = "0";
        txtTotAmt.Text = "0.00";

        Load_Data_GST();
        btnSave.Text = "Save";
        Initial_Data_Referesh();
        Session.Remove("PO_Receipt_No");
        //Load_Data_Enquiry_Grid();
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";
        decimal RecvQty = Convert.ToDecimal(txtReceivedQty.Text);
        string BalQty = Request.Form[txtBal.UniqueID];

        if (txtOrderQty.Text == "0.0" || txtOrderQty.Text == "0" || txtOrderQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Order Qty...');", true);
        }
        if (txtRate.Text == "0.0" || txtRate.Text == "0" || txtRate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Rate...');", true);
        }
        if (BalQty == "0.0" || BalQty == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Balance Qty is Zero Check the Item...');", true);
        }

        if (txtReceivedQty.Text == "0.0" || txtReceivedQty.Text == "0" || txtReceivedQty.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Received Qty...');", true);
        }
        if (RecvQty > Convert.ToDecimal(BalQty))
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Received Qty.. Received Qty less or equal to Balance Qty.');", true);
        }


        //check with Zone Name And Bin Name
        query = "Select * from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'  And BinName='" + txtBinName.Text + "'"; //And WarehouseCode='" + txtWarehouseName.SelectedValue + "'
        //And ZoneName='" + txtZoneName.SelectedItem.Text + "' And BinName='" + txtBinName.SelectedItem.Text + "'";
        qry_dt = objdata.RptEmployeeMultipleDetails(query);
        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Bin Details..');", true);
        }

        //check with Item Code And Item Name
        if (RdpReceiptType.SelectedValue == "1")
        {
            query = "Select * from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And ItemName='" + ddlItemName.SelectedItem.Text + "' And Std_PO_No='" + ddlPOrdNo.SelectedItem.Text + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
        }
        else
        {
            query = "Select * from General_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And ItemName='" + ddlItemName.SelectedItem.Text + "' And Gen_PO_No='" + ddlPOrdNo.SelectedItem.Text + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
        }

        if (qry_dt.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Item Details..');", true);
        }

        if (!ErrFlag)
        {
            //UOM Code get
            string UOM_Code_Str = qry_dt.Rows[0]["UOMCode"].ToString();
            //Get ValuationType
            query = "Select * from MstItemMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "' And ItemShortName='" + ddlItemName.SelectedItem.Text + "'";
            qry_dt = objdata.RptEmployeeMultipleDetails(query);
            string ValuationType = qry_dt.Rows[0]["ValuationType"].ToString();

            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemCode"].ToString().ToUpper() == ddlItemName.SelectedValue.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Item Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["POOrderNo"] = ddlPOrdNo.SelectedItem.Text;
                    dr["POOrderDate"] = txtPOOrderDate.Text;
                    dr["ItemCode"] = ddlItemName.SelectedValue;
                    dr["ItemName"] = ddlItemName.SelectedItem.Text;
                    dr["ValuationType"] = ValuationType;
                    dr["UOMCode"] = UOM_Code_Str;
                    dr["OrderQty"] = txtOrderQty.Text;
                    dr["ItemRate"] = txtRate.Text;
                    dr["ReceivedQty"] = txtReceivedQty.Text;
                    dr["ZoneName"] = txtBinName.Text;
                    dr["BinName"] = txtBinName.Text;


                    dr["ItemTotal"] = txtItemTotal.Text;
                    dr["Discount_Per"] = txtDiscount_Per.Text;
                    dr["Discount"] = txtDiscount_Amount.Text;
                    dr["TaxPer"] = txtTax.Text;
                    dr["TaxAmount"] = txtTaxAmt.Text;
                    dr["CGSTPer"] = txtCGSTPer.Text;
                    dr["CGSTAmount"] = txtCGSTAmt.Text;
                    dr["SGSTPer"] = txtSGSTPer.Text;
                    dr["SGSTAmount"] = txtSGSTAmt.Text;
                    dr["IGSTPer"] = txtIGSTPer.Text;
                    dr["IGSTAmount"] = txtIGSTAmt.Text;
                    dr["BDUTaxPer"] = txtBDUTax.Text;
                    dr["BDUTaxAmount"] = txtBDUAmount.Text;
                    dr["OtherCharge"] = txtOtherCharge.Text;
                    dr["LineTotal"] = txtFinal_Total_Amt.Text;
                    dr["PackingAmt"] = txtPackingAmt.Text;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    Totalsum();

                    //ddlPOrdNo.SelectedItem.Text = "-Select-"; txtPOOrderDate.Text = "";
                    ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-";
                    txtOrderQty.Text = ""; txtRate.Text = ""; txtReceivedQty.Text = "";
                    //txtZoneName.SelectedItem.Text = "-Select-"; txtBinName.SelectedItem.Text = "-Select-";
                    txtBal.Text = ""; txtItemTotal.Text = "0.00"; txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                    txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00"; txtBDUTax.Text = "0.0"; txtBDUAmount.Text = "0.00";
                    //txtCGSTPer.Text = "0.0"; txtSGSTPer.Text = "0.0"; txtIGSTPer.Text = "0.0";
                    txtCGSTAmt.Text = "0.00";  txtSGSTAmt.Text = "0.00";  txtIGSTAmt.Text = "0.00";
                    txtFinal_Total_Amt.Text = "0.00"; txtPackingAmt.Text = "0.0";
                    Load_Data_GST();
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["POOrderNo"] = ddlPOrdNo.SelectedItem.Text;
                dr["POOrderDate"] = txtPOOrderDate.Text;
                dr["ItemCode"] = ddlItemName.SelectedValue;
                dr["ItemName"] = ddlItemName.SelectedItem.Text;
                dr["ValuationType"] = ValuationType;
                dr["UOMCode"] = UOM_Code_Str;
                dr["OrderQty"] = txtOrderQty.Text;
                dr["ItemRate"] = txtRate.Text;
                dr["ReceivedQty"] = txtReceivedQty.Text;
                dr["ZoneName"] = txtBinName.Text;
                dr["BinName"] = txtBinName.Text;

                dr["ItemTotal"] = txtItemTotal.Text;
                dr["Discount_Per"] = txtDiscount_Per.Text;
                dr["Discount"] = txtDiscount_Amount.Text;
                dr["TaxPer"] = txtTax.Text;
                dr["TaxAmount"] = txtTaxAmt.Text;
                dr["CGSTPer"] = txtCGSTPer.Text;
                dr["CGSTAmount"] = txtCGSTAmt.Text;
                dr["SGSTPer"] = txtSGSTPer.Text;
                dr["SGSTAmount"] = txtSGSTAmt.Text;
                dr["IGSTPer"] = txtIGSTPer.Text;
                dr["IGSTAmount"] = txtIGSTAmt.Text;
                dr["BDUTaxPer"] = txtBDUTax.Text;
                dr["BDUTaxAmount"] = txtBDUAmount.Text;
                dr["OtherCharge"] = txtOtherCharge.Text;
                dr["LineTotal"] = txtFinal_Total_Amt.Text;
                dr["PackingAmt"] = txtPackingAmt.Text;

                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                Totalsum();

                ddlPOrdNo.SelectedItem.Text = ""; txtPOOrderDate.Text = ""; ddlItemName.SelectedValue = "-Select-"; ddlItemName.SelectedItem.Text = "-Select-";
                txtOrderQty.Text = ""; txtRate.Text = ""; txtReceivedQty.Text = ""; txtZoneName.SelectedItem.Text = "-Select-"; txtBinName.Text = "-Select-";
                txtBal.Text = ""; txtItemTotal.Text = "0.00"; txtDiscount_Per.Text = "0.0"; txtDiscount_Amount.Text = "0.00";
                txtTax.Text = "0.0"; txtTaxAmt.Text = "0.00"; txtOtherCharge.Text = "0.00"; txtBDUTax.Text = "0.0"; txtBDUAmount.Text = "0.00";
                txtCGSTPer.Text = "0.0"; txtCGSTAmt.Text = "0.00"; txtSGSTPer.Text = "0.0"; txtSGSTAmt.Text = "0.00"; txtIGSTPer.Text = "0.0"; txtIGSTAmt.Text = "0.00";
                txtFinal_Total_Amt.Text = "0.00"; txtPackingAmt.Text = "0.0";
                Load_Data_GST();
            }
        }
    }

    public void Totalsum()
    {
        string sum = "0";

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sum = (Convert.ToDecimal(sum) + Convert.ToDecimal(dt.Rows[i]["LineTotal"])).ToString();
            txtTotAmt.Text = sum;

        }
        Final_Total_Calculate();
        //txtNetAmt.Text = ((Convert.ToDecimal(txtTotAmt.Text) + Convert.ToDecimal(txtTaxAmt.Text) + Convert.ToDecimal(txtOtherCharge.Text)) - (Convert.ToDecimal(txtDiscount.Text))).ToString();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("POOrderNo", typeof(string)));
        dt.Columns.Add(new DataColumn("POOrderDate", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemCode", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemName", typeof(string)));
        dt.Columns.Add(new DataColumn("ValuationType", typeof(string)));
        dt.Columns.Add(new DataColumn("UOMCode", typeof(string)));
        dt.Columns.Add(new DataColumn("OrderQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ItemRate", typeof(string)));
        dt.Columns.Add(new DataColumn("ReceivedQty", typeof(string)));
        dt.Columns.Add(new DataColumn("ZoneName", typeof(string)));
        dt.Columns.Add(new DataColumn("BinName", typeof(string)));

        dt.Columns.Add(new DataColumn("ItemTotal", typeof(string)));
        dt.Columns.Add(new DataColumn("Discount_Per", typeof(string)));
        dt.Columns.Add(new DataColumn("Discount", typeof(string)));
        dt.Columns.Add(new DataColumn("TaxPer", typeof(string)));
        dt.Columns.Add(new DataColumn("TaxAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("CGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("SGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTPer", typeof(string)));
        dt.Columns.Add(new DataColumn("IGSTAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("BDUTaxPer", typeof(string)));
        dt.Columns.Add(new DataColumn("BDUTaxAmount", typeof(string)));
        dt.Columns.Add(new DataColumn("OtherCharge", typeof(string)));
        dt.Columns.Add(new DataColumn("LineTotal", typeof(string)));

        dt.Columns.Add(new DataColumn("PackingAmt", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["ItemCode"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search Purchase Enquiry
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from Pur_Order_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Receipt_No='" + txtPOReceiptNo.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["PO_Receipt_Date"].ToString();
            RdpReceiptType.SelectedValue = Main_DT.Rows[0]["PO_Receipt_Type"].ToString();
            ddlSupplier.SelectedValue = Main_DT.Rows[0]["Supp_Code"].ToString();
            ddlSupplier.SelectedItem.Text = Main_DT.Rows[0]["Supp_Name"].ToString();
            txtInvoiceNo.Text = Main_DT.Rows[0]["InvNo"].ToString();
            txtInvoiceDate.Text = Main_DT.Rows[0]["InvDate"].ToString();
            txtDCNo.Text = Main_DT.Rows[0]["DCNo"].ToString();
            txtDCDate.Text = Main_DT.Rows[0]["DCDate"].ToString();
            txtDepartmentName.SelectedValue = Main_DT.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = Main_DT.Rows[0]["DeptName"].ToString();
            txtCostCenterName.SelectedValue = Main_DT.Rows[0]["CostCenterCode"].ToString();
            txtCostCenterName.SelectedItem.Text = Main_DT.Rows[0]["CostCenterName"].ToString();
            txtWarehouseName.SelectedValue = Main_DT.Rows[0]["WarehouseCode"].ToString();
            txtWarehouseName.SelectedItem.Text = Main_DT.Rows[0]["WarehouseName"].ToString();
            txtOthers.Text = Main_DT.Rows[0]["Others"].ToString();

            txtTotPackAmt.Text= Main_DT.Rows[0]["TotPackAmt"].ToString();
            txtTotAmt.Text = Main_DT.Rows[0]["TotalAmt"].ToString();
            txtAddOrLess.Text = Main_DT.Rows[0]["AddOrLess"].ToString();
            txtNetAmt.Text = Main_DT.Rows[0]["NetAmount"].ToString();

            txtGPINNo.Text = Main_DT.Rows[0]["GPINNo"].ToString();
            txtGPINDate.Text = Main_DT.Rows[0]["GPINDate"].ToString();



            //Pur_Order_Receipt_Main_Sub Table Load
            DataTable dt = new DataTable();
            query = "Select Pur_Order_No as POOrderNo,Pur_Order_Date as POOrderDate,ItemCode,ItemName,ValuationType,UOMCode,OrderQty,ItemRate,ReceivedQty,ZoneName,BinName,";
            query = query + "ItemTotal,Discount_Per,Discount,TaxPer,TaxAmount,CGSTPer,CGSTAmount,SGSTPer,SGSTAmount,IGSTPer,IGSTAmount,BDUTaxPer,BDUTaxAmount,OtherCharge,LineTotal,";
            query = query + "PackingAmt from Pur_Order_Receipt_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Receipt_No='" + txtPOReceiptNo.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();


            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    protected void btnBackRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect("po_receipt_main.aspx");
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;

        //User Rights Check Start
        bool Rights_Check = false;
        //Rights_Check = CommonClass_Function.ApproveRights_Check(SessionCcode, SessionLcode, SessionUserID, "7", "2", "PurchaseOrder Receipt");
        //if (Rights_Check == false)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You do not have Rights to Approve Purchase Order Receipt...');", true);
        //}
        //User Rights Check End

        query = "Select * from Pur_Order_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Receipt_No='" + txtPOReceiptNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First You have to Register this Purchase Order Receipt Details..');", true);
        }

        query = "Select * from Pur_Order_Receipt_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And PO_Receipt_No='" + txtPOReceiptNo.Text + "' And PO_Receipt_Status='1'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Order Receipt Details Already Approved..');", true);
        }

        if (!ErrFlag)
        {
            query = "Update Pur_Order_Receipt_Main set PO_Receipt_Status='1' where Ccode='" + SessionCcode + "'";
            query = query + "  And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + "  And PO_Receipt_No='" + txtPOReceiptNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            Stock_Add();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Purchase Order Receipt Details Approved Successfully..');", true);
        }
    }

    private void Stock_Add()
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();

        query = "Select * from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtPOReceiptNo.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Check.Rows.Count != 0)
        {
            query = "Delete from Stock_Transaction_Ledger where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And FinYearCode='" + SessionFinYearCode + "' And Trans_No='" + txtPOReceiptNo.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }


        DateTime transDate = Convert.ToDateTime(txtDate.Text);

        //Insert Stock_Transaction_Ledger
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string Qty_Value = "0.0";
            //Qty_Value = (Convert.ToDecimal(dt.Rows[i]["ReceivedQty"].ToString()) * Convert.ToDecimal(dt.Rows[i]["ItemRate"].ToString())).ToString();

            Qty_Value = dt.Rows[i]["LineTotal"].ToString();

            query = "Insert Into Stock_Transaction_Ledger(Ccode,Lcode,FinYearCode,FinYearVal,Trans_No,Trans_Date,Trans_Date_Str,";
            query = query + " Trans_Type,DeptCode,DeptName,CostCenterCode,CostCenterName,ItemCode,ItemName,Open_Qty,Open_Value,PO_Qty,PO_Value,General_Qty,General_Value,Issue_Qty,";
            query = query + " Issue_Value,Issue_Return_Qty,Issue_Return_Value,GP_IN_Qty,GP_IN_Value,GP_OUT_Qty,GP_OUT_Value,Transfer_Qty,Transfer_Value,Damage_Qty,Damage_Value,Spares_ReSales_Qty,Spares_ReSales_Value,WarehouseCode,WarehouseName,ZoneName,BinName,Supp_Code,";
            query = query + " Supp_Name,UserID,UserName) Values('" + SessionCcode + "','" + SessionLcode + "','" + SessionFinYearCode + "',";
            query = query + " '" + SessionFinYearVal + "','" + txtPOReceiptNo.Text + "','" + transDate.ToString("MM/dd/yyyy") + "','" + txtDate.Text + "','PO RECEIPT','" + txtDepartmentName.SelectedValue + "','" + txtDepartmentName.SelectedItem.Text + "','" + txtCostCenterName.SelectedValue + "','" + txtCostCenterName.SelectedItem.Text + "',";
            query = query + " '" + dt.Rows[i]["ItemCode"].ToString() + "','" + dt.Rows[i]["ItemName"].ToString() + "','0.0','0.0',";
            query = query + " '" + dt.Rows[i]["ReceivedQty"].ToString() + "','" + Qty_Value + "','0.0','0.0','0.0',";
            query = query + " '0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','0.0','" + txtWarehouseName.SelectedValue + "','" + txtWarehouseName.SelectedItem.Text + "','" + dt.Rows[i]["BinName"].ToString() + "',";
            query = query + " '" + dt.Rows[i]["BinName"].ToString() + "','" + ddlSupplier.SelectedValue + "','" + ddlSupplier.SelectedItem.Text + "','" + SessionUserID + "','" + SessionUserName + "')";
            objdata.RptEmployeeMultipleDetails(query);

            CommonClass_Function.cls_Trans_Type = "PO RECEIPT";
            CommonClass_Function.cls_ItemCode = dt.Rows[i]["ItemCode"].ToString();
            CommonClass_Function.cls_ItemName = dt.Rows[i]["ItemName"].ToString();
            CommonClass_Function.cls_Add_Qty = dt.Rows[i]["ReceivedQty"].ToString();
            CommonClass_Function.cls_Add_Value = Qty_Value;
            CommonClass_Function.cls_Minus_Qty = "0.0";
            CommonClass_Function.cls_Minus_Value = "0.0";
            CommonClass_Function.cls_DeptCode = txtDepartmentName.SelectedValue;
            CommonClass_Function.cls_DeptName = txtDepartmentName.SelectedItem.Text;
            CommonClass_Function.cls_CostCenterCode = txtCostCenterName.SelectedValue;
            CommonClass_Function.cls_CostCenterName = txtCostCenterName.SelectedItem.Text;
            CommonClass_Function.cls_WarehouseCode = txtWarehouseName.SelectedValue;
            CommonClass_Function.cls_WarehouseName = txtWarehouseName.SelectedItem.Text;
            CommonClass_Function.cls_ZoneName = dt.Rows[i]["BinName"].ToString();
            CommonClass_Function.cls_BinName = dt.Rows[i]["BinName"].ToString();
            CommonClass_Function.cls_Supp_Code = ddlSupplier.SelectedValue;
            CommonClass_Function.cls_Supp_Name = ddlSupplier.SelectedItem.Text;
            CommonClass_Function.cls_UserID = SessionUserID;
            CommonClass_Function.cls_UserName = SessionUserName;
            //CommonClass_Function.cls_Stock_Qty = "";
            //CommonClass_Function.cls_Stock_Value = "";

            CommonClass_Function.Stock_Ledger_And_Current_Stock_Add(SessionCcode, SessionLcode, SessionFinYearCode, SessionFinYearVal, txtPOReceiptNo.Text, txtDate.Text);
        }
    }

    private void Load_Data_Empty_Supp1()
    {

        string query = "";
        DataTable DT = new DataTable();

        DT.Columns.Add("SuppCode");
        DT.Columns.Add("SuppName");

        query = "Select SuppCode,SuppName from MstSupplier where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        ddlSupplier.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["SuppCode"] = "-Select-";
        dr["SuppName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlSupplier.DataTextField = "SuppName";
        ddlSupplier.DataValueField = "SuppCode";

        ddlSupplier.DataBind();

    }

    protected void btnWareHouse_Click(object sender, EventArgs e)
    {
        // modalPop_WareHouse.Show();
    }
    private void Load_Data_Empty_WareHouse()
    {

        string query = "";
        DataTable Main_DT = new DataTable();

        txtWarehouseName.Items.Clear();
        query = "Select WarehouseCode,WarehouseName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtWarehouseName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["WarehouseCode"] = "-Select-";
        dr["WarehouseName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtWarehouseName.DataTextField = "WarehouseName";
        txtWarehouseName.DataValueField = "WarehouseCode";
        txtWarehouseName.DataBind();




    }

    protected void GridViewClick_WareHouse(object sender, CommandEventArgs e)
    {
        //txtWarehouseCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtWarehouseName.Text = Convert.ToString(e.CommandName);

        Load_Data_Empty_ZoneName();
        Load_Data_Empty_BinName();
    }

    private void Load_Data_Empty_POOrderNo()
    {

        string query = "";

        bool error = false;

        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();
        DataTable DT3 = new DataTable();

        string OrderType = "";
        OrderType = RdpReceiptType.SelectedValue;

        DataTable DT = new DataTable();
        DT.Columns.Add("PO_Order_No");
        DT.Columns.Add("PO_Order_Date");

        if (OrderType == "1")
        {
            query = "Select Std_PO_No,Std_PO_Date from Std_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + " And Supp_Code='" + ddlSupplier.SelectedValue + "' And PO_Status='1'";
            DT1 = objdata.RptEmployeeMultipleDetails(query);

            if (DT1.Rows.Count != 0)
            {
                int k = 0;
                for (int i = 0; i < DT1.Rows.Count; i++)
                {
                    error = false;
                    query = "select ItemCode,OrderQty from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Std_PO_No='" + DT1.Rows[i]["Std_PO_No"].ToString() + "'";
                    DT2 = objdata.RptEmployeeMultipleDetails(query);

                    for (int j = 0; j < DT2.Rows.Count; j++)
                    {

                        query = "select Sum(ReceivedQty) as ReceivedQty from Pur_Order_Receipt_Main_Sub where ItemCode='" + DT2.Rows[j]["ItemCode"].ToString() + "' And Pur_Order_No='" + DT1.Rows[i]["Std_PO_No"].ToString() + "'";
                        DT3 = objdata.RptEmployeeMultipleDetails(query);
                        if (DT3.Rows[0]["ReceivedQty"].ToString() != "")
                        {
                            if (Convert.ToDecimal(DT3.Rows[0]["ReceivedQty"]) < Convert.ToDecimal(DT2.Rows[j]["OrderQty"]))
                            {
                                error = true;
                            }
                        }
                        else
                        {
                            error = true;
                        }

                    }
                    if (error)
                    {
                        DT.Rows.Add();
                        DT.Rows[k]["PO_Order_No"] = DT1.Rows[i]["Std_PO_No"].ToString();
                        DT.Rows[k]["PO_Order_Date"] = DT1.Rows[i]["Std_PO_Date"].ToString();
                        k = k + 1;
                    }
                }
            }




        }
        else
        {
            query = "Select Gen_PO_No,Gen_PO_Date from General_Purchase_Order_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + " And Supp_Code='" + ddlSupplier.SelectedValue + "' And PO_Status='1'";
            DT1 = objdata.RptEmployeeMultipleDetails(query);
            if (DT1.Rows.Count != 0)
            {
                int k = 0;
                for (int i = 0; i < DT1.Rows.Count; i++)
                {
                    error = false;
                    query = "select ItemCode,OrderQty from General_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "' And Gen_PO_No='" + DT1.Rows[i]["Gen_PO_No"].ToString() + "'";
                    DT2 = objdata.RptEmployeeMultipleDetails(query);

                    for (int j = 0; j < DT2.Rows.Count; j++)
                    {

                        query = "select Sum(ReceivedQty) as ReceivedQty from Pur_Order_Receipt_Main_Sub where ItemCode='" + DT2.Rows[j]["ItemCode"].ToString() + "' And Pur_Order_No='" + DT1.Rows[i]["Gen_PO_No"].ToString() + "'";
                        DT3 = objdata.RptEmployeeMultipleDetails(query);

                        if (DT3.Rows[0]["ReceivedQty"].ToString() != "" &&  DT3.Rows[0]["ReceivedQty"].ToString() != null)
                        {
                            if (Convert.ToDecimal(DT3.Rows[0]["ReceivedQty"]) < Convert.ToDecimal(DT2.Rows[j]["OrderQty"]))
                            {
                                error = true;
                            }
                        }
                        else
                        {
                            error = true;
                        }
                    }
                    if (error)
                    {
                        DT.Rows.Add();
                        DT.Rows[k]["PO_Order_No"] = DT1.Rows[i]["Gen_PO_No"].ToString();
                        DT.Rows[k]["PO_Order_Date"] = DT1.Rows[i]["Gen_PO_Date"].ToString();
                        k = k + 1;

                    }
                    //DT.Rows.Add();
                    //DT.Rows[k]["PO_Order_No"] = DT1.Rows[i]["Gen_PO_No"].ToString();
                    //DT.Rows[k]["PO_Order_Date"] = DT1.Rows[i]["Gen_PO_Date"].ToString();
                    //k = k + 1;
                }

            }
        }

        ddlPOrdNo.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["PO_Order_No"] = "-Select-";
        dr["PO_Order_Date"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlPOrdNo.DataTextField = "PO_Order_No";
        //ddlPOrdNo.DataValueField = "PO_Order_Date";
        //Repeater_PORecNo.DataSource = DT;
        ddlPOrdNo.DataBind();
        //Repeater_PORecNo.Visible = true;

    }

    private void Load_Data_Empty_ItemCode()
    {


        string query = "";
        string OrderType = "";


        OrderType = RdpReceiptType.SelectedValue;

        DataTable DT = new DataTable();

        if (OrderType == "1")
        {
            query = "Select ItemCode,ItemName from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + " And Std_PO_No='" + ddlPOrdNo.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
        }

        else
        {
            query = "Select ItemCode,ItemName from General_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            query = query + " And Gen_PO_No='" + ddlPOrdNo.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

        }

        ddlItemName.DataSource = DT;

        DataRow dr = DT.NewRow();

        dr["ItemCode"] = "-Select-";
        dr["ItemName"] = "-Select-";

        DT.Rows.InsertAt(dr, 0);

        ddlItemName.DataTextField = "ItemName";
        ddlItemName.DataValueField = "ItemCode";

        ddlItemName.DataBind();
    }

    protected void btnZoneName_Click(object sender, EventArgs e)
    {
        //modalPop_ZoneName.Show();
    }

    private void Load_Data_Empty_ZoneName()
    {

        //string query = "";
        //DataTable Main_DT = new DataTable();

        //txtZoneName.Items.Clear();
        //query = "Select ZoneName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        //query = query + " And WarehouseCode='" + txtWarehouseName.SelectedValue + "'";
        //Main_DT = objdata.RptEmployeeMultipleDetails(query);
        //txtZoneName.DataSource = Main_DT;
        //DataRow dr = Main_DT.NewRow();

        //dr["ZoneName"] = "-Select-";
        //Main_DT.Rows.InsertAt(dr, 0);
        //txtZoneName.DataTextField = "ZoneName";

        //txtZoneName.DataBind();
    }

    protected void GridViewClick_ZoneName(object sender, CommandEventArgs e)
    {
        txtZoneName.Text = Convert.ToString(e.CommandArgument);

        Load_Data_Empty_BinName();
    }


    protected void btnBinName_Click(object sender, EventArgs e)
    {
        // modalPop_BinName.Show();
    }

    private void Load_Data_Empty_BinName()
    {
        //string query = "";
        //DataTable Main_DT = new DataTable();

        //txtBinName.Items.Clear();
        //query = "Select BinName from MstWarehouse where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        ////query = query + " And WarehouseCode='" + txtWarehouseName.SelectedValue + "'"; //And ZoneName='" + txtZoneName.SelectedItem.Text + "'";
        //Main_DT = objdata.RptEmployeeMultipleDetails(query);
        //txtBinName.DataSource = Main_DT;
        //DataRow dr = Main_DT.NewRow();

        //dr["BinName"] = "-Select-";
        //Main_DT.Rows.InsertAt(dr, 0);
        //txtBinName.DataTextField = "BinName";

        //txtBinName.DataBind();

    }

    protected void GridViewClick_BinName(object sender, CommandEventArgs e)
    {
        txtBinName.Text = Convert.ToString(e.CommandArgument);
    }


    protected void btnDept_Click(object sender, EventArgs e)
    {
        // modalPop_Dept.Show();
    }
    private void Load_Data_Empty_Dept()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtDepartmentName.Items.Clear();
        query = "Select DeptCode,DeptName from MstDepartment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDepartmentName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["DeptCode"] = "-Select-";
        dr["DeptName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtDepartmentName.DataTextField = "DeptName";
        txtDepartmentName.DataValueField = "DeptCode";
        txtDepartmentName.DataBind();

    }

    protected void GridViewClick_Dept(object sender, CommandEventArgs e)
    {
        //txtDeptCodeHide.Value = Convert.ToString(e.CommandArgument);
        txtDepartmentName.Text = Convert.ToString(e.CommandName);
    }

    protected void btnCostCenterName_Click(object sender, EventArgs e)
    {
        //modalPop_CostCenterName.Show();
    }
    private void Load_Data_Empty_CostCenterName()
    {
        string query = "";
        DataTable Main_DT = new DataTable();

        txtCostCenterName.Items.Clear();
        query = "Select CostcenterCode,CostcenterName from MstCostCenter where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtCostCenterName.DataSource = Main_DT;
        DataRow dr = Main_DT.NewRow();
        dr["CostcenterCode"] = "-Select-";
        dr["CostcenterName"] = "-Select-";
        Main_DT.Rows.InsertAt(dr, 0);
        txtCostCenterName.DataTextField = "CostcenterName";
        txtCostCenterName.DataValueField = "CostcenterCode";
        txtCostCenterName.DataBind();


    }

    protected void GridViewClick_CostCenterName(object sender, CommandEventArgs e)
    {
        // txtCostCenterCodeHide.Value = Convert.ToString(e.CommandArgument);
        // txtCostCenterName.Text = Convert.ToString(e.CommandName);


    }


    protected void txtWarehouseName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_ZoneName();
        Load_Data_Empty_BinName();

        string query = "";
        DataTable Main_DT = new DataTable();

        //txtWarehouseName.Items.Clear();
        query = "Select BinName from MstWarehouse where WarehouseName='" + txtWarehouseName.SelectedItem.Text + "' And ";
        query = query + "  Ccode='" + SessionCcode + "' And Lcode = '" + SessionLcode + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        txtBinName.Text = Main_DT.Rows[0]["BinName"].ToString();
        //DataRow dr = Main_DT.NewRow();
        //dr["WarehouseCode"] = "-Select-";
        //dr["WarehouseName"] = "-Select-";
        //Main_DT.Rows.InsertAt(dr, 0);
        //txtWarehouseName.DataTextField = "WarehouseName";
        //txtWarehouseName.DataValueField = "WarehouseCode";
        //txtWarehouseName.DataBind();
    }
    protected void txtZoneName_SelectedIndexChanged(object sender, EventArgs e)
    {

        Load_Data_Empty_BinName();
    }


    protected void txtRate_TextChanged(object sender, EventArgs e)
    {
        if (txtRate.Text.ToString() != "") { Total_Calculate(); }
    }
    protected void txtDiscount_Per_TextChanged(object sender, EventArgs e)
    {
        if (txtDiscount_Per.Text.ToString() != "") { Total_Calculate(); }
    }
    protected void txtPackingAmt_TextChanged(object sender, EventArgs e)
    {
        Total_Calculate();
    }
    protected void txtGST_Type_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TaxData();
    }
    protected void txtCGSTPer_TextChanged(object sender, EventArgs e)
    {
        if (txtCGSTPer.Text.ToString() != "") { Total_Calculate(); }
    }
    protected void txtSGSTPer_TextChanged(object sender, EventArgs e)
    {
        if (txtSGSTPer.Text.ToString() != "") { Total_Calculate(); }
    }
    protected void txtIGSTPer_TextChanged(object sender, EventArgs e)
    {
        if (txtIGSTPer.Text.ToString() != "") { Total_Calculate(); }
    }
    protected void txtBDUTax_TextChanged(object sender, EventArgs e)
    {
        if (txtBDUTax.Text.ToString() != "") { Total_Calculate(); }
    }
    protected void txtTax_TextChanged(object sender, EventArgs e)
    {
        if (txtTax.Text.ToString() != "") { Total_Calculate(); }
    }
    protected void txtOtherCharge_TextChanged(object sender, EventArgs e)
    {
        if (txtOtherCharge.Text.ToString() != "") { Total_Calculate(); }
    }
    protected void txtAddOrLess_TextChanged(object sender, EventArgs e)
    {
        if (txtAddOrLess.Text.ToString() != "") { Final_Total_Calculate(); }
    }
    private void Total_Calculate()
    {
        string Qty_Val = "0";
        string Item_Rate = "0";
        string Item_Total = "0";
        string Discount_Percent = "0";
        string Discount_Amt = "0";
        string Tax_Per = "0";
        string Tax_Amt = "0";
        string CGST_Per = "0";
        string CGST_Amt = "0";
        string SGST_Per = "0";
        string SGST_Amt = "0";
        string IGST_Per = "0";
        string IGST_Amt = "0";
        string BDUTax_Per = "0";
        string BDUTax_Amt = "0";
        string Other_Charges = "0";
        string Final_Amount = "0";
        string PackingAmt = "0";

        if (txtReceivedQty.Text != "") { Qty_Val = txtReceivedQty.Text.ToString(); }
        if (txtRate.Text != "") { Item_Rate = txtRate.Text.ToString(); }


        Item_Total = (Convert.ToDecimal(Qty_Val) * Convert.ToDecimal(Item_Rate)).ToString();
        Item_Total = (Math.Round(Convert.ToDecimal(Item_Total), 2, MidpointRounding.AwayFromZero)).ToString();
        if (Convert.ToDecimal(Item_Total) != 0)
        {
            if (Convert.ToDecimal(txtDiscount_Per.Text.ToString()) != 0) { Discount_Percent = txtDiscount_Per.Text.ToString(); }
            if (Convert.ToDecimal(txtTax.Text.ToString()) != 0) { Tax_Per = txtTax.Text.ToString(); }
            if (Convert.ToDecimal(txtBDUTax.Text.ToString()) != 0) { BDUTax_Per = txtBDUTax.Text.ToString(); }
            if (Convert.ToDecimal(txtOtherCharge.Text.ToString()) != 0) { Other_Charges = txtOtherCharge.Text.ToString(); }
            if (Convert.ToDecimal(txtCGSTPer.Text.ToString()) != 0) { CGST_Per = txtCGSTPer.Text.ToString(); }
            if (Convert.ToDecimal(txtSGSTPer.Text.ToString()) != 0) { SGST_Per = txtSGSTPer.Text.ToString(); }
            if (Convert.ToDecimal(txtIGSTPer.Text.ToString()) != 0) { IGST_Per = txtIGSTPer.Text.ToString(); }

            if (Convert.ToDecimal(txtPackingAmt.Text.ToString()) != 0) { PackingAmt = txtPackingAmt.Text.ToString(); }
            


            //Discount Amt Calculate
            if (Convert.ToDecimal(Discount_Percent) != 0)
            {
                Discount_Amt = (Convert.ToDecimal(Item_Total) * Convert.ToDecimal(Discount_Percent)).ToString();
                Discount_Amt = (Convert.ToDecimal(Discount_Amt) / Convert.ToDecimal(100)).ToString();
                Discount_Amt = (Math.Round(Convert.ToDecimal(Discount_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                Discount_Amt = "0.00";
            }
            if (Convert.ToDecimal(BDUTax_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                BDUTax_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(BDUTax_Per)).ToString();
                BDUTax_Amt = (Convert.ToDecimal(BDUTax_Amt) / Convert.ToDecimal(100)).ToString();
                BDUTax_Amt = (Math.Round(Convert.ToDecimal(BDUTax_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                BDUTax_Amt = "0.00";
            }

            if (Convert.ToDecimal(CGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                CGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(CGST_Per)).ToString();
                CGST_Amt = (Convert.ToDecimal(CGST_Amt) / Convert.ToDecimal(100)).ToString();
                CGST_Amt = (Math.Round(Convert.ToDecimal(CGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                CGST_Amt = "0.00";
            }

            if (Convert.ToDecimal(SGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                SGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(SGST_Per)).ToString();
                SGST_Amt = (Convert.ToDecimal(SGST_Amt) / Convert.ToDecimal(100)).ToString();
                SGST_Amt = (Math.Round(Convert.ToDecimal(SGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                SGST_Amt = "0.00";
            }

            if (Convert.ToDecimal(IGST_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                IGST_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(IGST_Per)).ToString();
                IGST_Amt = (Convert.ToDecimal(IGST_Amt) / Convert.ToDecimal(100)).ToString();
                IGST_Amt = (Math.Round(Convert.ToDecimal(IGST_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                IGST_Amt = "0.00";
            }


            //Tax Percentage Calculate
            if (Convert.ToDecimal(Tax_Per) != 0)
            {
                string Item_Discount_Amt = "0.00";
                Item_Discount_Amt = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
                Item_Discount_Amt = (Convert.ToDecimal(Item_Discount_Amt) + Convert.ToDecimal(BDUTax_Amt)).ToString();
                Tax_Amt = (Convert.ToDecimal(Item_Discount_Amt) * Convert.ToDecimal(Tax_Per)).ToString();
                Tax_Amt = (Convert.ToDecimal(Tax_Amt) / Convert.ToDecimal(100)).ToString();
                Tax_Amt = (Math.Round(Convert.ToDecimal(Tax_Amt), 2, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                Tax_Amt = "0.00";
            }


            //Other Charges
            if (txtOtherCharge.Text.ToString() != "") { Other_Charges = txtOtherCharge.Text.ToString(); }

            //Final Amt
            Final_Amount = ((Convert.ToDecimal(Item_Total) + Convert.ToDecimal(PackingAmt)) - Convert.ToDecimal(Discount_Amt)).ToString();
            //Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges)).ToString();
            Final_Amount = (Convert.ToDecimal(Final_Amount) + Convert.ToDecimal(Tax_Amt) + Convert.ToDecimal(BDUTax_Amt) + Convert.ToDecimal(Other_Charges) + Convert.ToDecimal(CGST_Amt) + Convert.ToDecimal(SGST_Amt) + Convert.ToDecimal(IGST_Amt)).ToString();
            Final_Amount = (Math.Round(Convert.ToDecimal(Final_Amount), 2, MidpointRounding.AwayFromZero)).ToString();

            txtItemTotal.Text = Item_Total;
            txtDiscount_Amount.Text = Discount_Amt;
            txtTaxAmt.Text = Tax_Amt;
            txtBDUAmount.Text = BDUTax_Amt;
            txtCGSTAmt.Text = CGST_Amt;
            txtSGSTAmt.Text = SGST_Amt;
            txtIGSTAmt.Text = IGST_Amt;
            txtFinal_Total_Amt.Text = Final_Amount;
            txtNetAmt.Text = Final_Amount;
        }
    }
    private void Load_TaxData()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MsTAX where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GST_Type='" + txtGST_Type.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtCGSTPer.Text = DT.Rows[0]["CGST_Percent"].ToString();
            txtSGSTPer.Text = DT.Rows[0]["SGST_Percent"].ToString();
            txtIGSTPer.Text = DT.Rows[0]["IGST_Percent"].ToString();
        }
        else
        {
            txtCGSTPer.Text = "0";
            txtSGSTPer.Text = "0";
            txtIGSTPer.Text = "0";
        }

        Total_Calculate();
    }

    private void Final_Total_Calculate()
    {

        string Final_NetAmt = "0";
        string AddorLess = "0";
        string TotPacking = "0";
        string TotOther = "0";
        string Item_Total_Amt = txtTotAmt.Text;

        if (Convert.ToDecimal(txtTotPackAmt.Text.ToString()) != 0) { TotPacking = txtTotPackAmt.Text.ToString(); }
        if (Convert.ToDecimal(txtTotOther.Text.ToString()) != 0) { TotOther = txtTotOther.Text.ToString(); }
        if (Convert.ToDecimal(txtAddOrLess.Text.ToString()) != 0) { AddorLess = txtAddOrLess.Text.ToString(); }
        Final_NetAmt = (Convert.ToDecimal(AddorLess) + Convert.ToDecimal(Item_Total_Amt) + Convert.ToDecimal(TotPacking) + Convert.ToDecimal(TotOther)).ToString();
        //txtFinal_Total_Amt.Text = Final_NetAmt;
        txtNetAmt.Text = Final_NetAmt;

    }
    protected void txtReceivedQty_TextChanged(object sender, EventArgs e)
    {
        Total_Calculate();
        Final_Total_Calculate();
    }

    protected void ddlSupplier_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_Empty_POOrderNo();
    }

    protected void ddlPOrdNo_SelectedIndexChanged(object sender, EventArgs e)
    {

        string SSQL = "Select * from General_Purchase_Order_Main where Ccode='"+ SessionCcode +"' And Lcode='" +SessionLcode+ "' And FinYearCode='" +SessionFinYearCode+ "' And Gen_PO_No='" + ddlPOrdNo.SelectedItem.Text + "'";
        DataTable DT = objdata.RptEmployeeMultipleDetails(SSQL);
        
        Load_Data_Empty_ItemCode();

        txtPOOrderDate.Text = DT.Rows[0]["Gen_Po_Date"].ToString();

    }

    protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string OrderType = "";
        string SSQL = "";
        decimal bal = 0;
        DataTable DT = new DataTable();

        DT.Columns.Add("ItemCode");
        DT.Columns.Add("ItemName");
        DT.Columns.Add("OrderQty");
        DT.Columns.Add("Rate");
        DT.Columns.Add("Balance");

        DataTable DT1 = new DataTable();
        DataTable DT2 = new DataTable();

        if (OrderType == "1")
        {
            SSQL = "Select ItemCode,ItemName,OrderQty,Rate from Std_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And Std_PO_No='" + ddlPOrdNo.SelectedItem.Text + "' And ItemName ='" + ddlItemName.SelectedItem.Text + "'";
            DT1 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count > 0)
            {
                int k = 0;
                for (int i = 0; i < DT1.Rows.Count; i++)
                {
                    SSQL = "select Sum(ReceivedQty) as ReceivedQty from Pur_Order_Receipt_Main_Sub where ItemCode='" + ddlItemName.SelectedValue + "' And Pur_Order_No='" + ddlPOrdNo.SelectedItem.Text + "'";
                    DT2 = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DT2.Rows[0]["ReceivedQty"].ToString() != "")
                    {
                        if (Convert.ToDecimal(DT2.Rows[0]["ReceivedQty"]) < Convert.ToDecimal(DT1.Rows[i]["OrderQty"]))
                        {
                            bal = Convert.ToDecimal(DT1.Rows[i]["OrderQty"]) - Convert.ToDecimal(DT2.Rows[0]["ReceivedQty"]);
                        }
                        else if (Convert.ToDecimal(DT2.Rows[0]["ReceivedQty"]) == Convert.ToDecimal(DT1.Rows[i]["OrderQty"]))
                        {
                            bal = 0;
                        }

                    }
                    else
                    {
                        bal = Convert.ToDecimal(DT1.Rows[i]["OrderQty"]);
                    }

                    DT.Rows.Add();
                    DT.Rows[i]["ItemCode"] = DT1.Rows[i]["ItemCode"].ToString();
                    DT.Rows[i]["ItemName"] = DT1.Rows[i]["ItemName"].ToString();
                    DT.Rows[i]["OrderQty"] = DT1.Rows[i]["OrderQty"].ToString();
                    DT.Rows[i]["Rate"] = DT1.Rows[i]["Rate"].ToString();
                    DT.Rows[i]["Balance"] = bal.ToString();
                }

            }
        }
        else
        {
            SSQL = "Select OrderQty,Rate from General_Purchase_Order_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And FinYearCode='" + SessionFinYearCode + "'";
            SSQL = SSQL + " And Gen_PO_No='" + ddlPOrdNo.SelectedItem.Text + "' and ItemName='" + ddlItemName.SelectedItem.Text + "'";
            DT1 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT1.Rows.Count > 0)
            {
                int k = 0;
                for (int i = 0; i < DT1.Rows.Count; i++)
                {
                    SSQL = "select Sum(ReceivedQty) as ReceivedQty from Pur_Order_Receipt_Main_Sub where ItemCode='" + ddlItemName.SelectedValue + "' And Pur_Order_No='" + ddlPOrdNo.SelectedItem.Text + "'";
                    DT2 = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DT2.Rows[0]["ReceivedQty"].ToString() != "")
                    {
                        if (Convert.ToDecimal(DT2.Rows[0]["ReceivedQty"]) < Convert.ToDecimal(DT1.Rows[i]["OrderQty"]))
                        {
                            bal = Convert.ToDecimal(DT1.Rows[i]["OrderQty"]) - Convert.ToDecimal(DT2.Rows[0]["ReceivedQty"]);
                        }
                        else if (Convert.ToDecimal(DT2.Rows[0]["ReceivedQty"]) == Convert.ToDecimal(DT1.Rows[i]["OrderQty"]))
                        {
                            bal = 0;
                        }

                    }
                    else
                    {
                        bal = Convert.ToDecimal(DT1.Rows[i]["OrderQty"]);
                    }

                    DT.Rows.Add();
                    //DT.Rows[i]["ItemCode"] = DT1.Rows[i]["ItemCode"].ToString();
                    //DT.Rows[i]["ItemName"] = DT1.Rows[i]["ItemName"].ToString();
                    DT.Rows[i]["OrderQty"] = DT1.Rows[i]["OrderQty"].ToString();
                    DT.Rows[i]["Rate"] = DT1.Rows[i]["Rate"].ToString();
                    DT.Rows[i]["Balance"] = bal.ToString();
                }
            }

        }


        DataTable DTItemDet = new DataTable();
        SSQL = "Select DeptCode,DeptName,WarehouseCode,WarehouseName,ZoneName,BinName from Stock_Current_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemCode='" + ddlItemName.SelectedValue + "'";
        
        DTItemDet = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DTItemDet.Rows.Count != 0)
        {
            txtDepartmentName.SelectedValue = DTItemDet.Rows[0]["DeptCode"].ToString();
            txtDepartmentName.SelectedItem.Text = DTItemDet.Rows[0]["DeptName"].ToString();
            txtWarehouseName.SelectedValue = DTItemDet.Rows[0]["WarehouseCode"].ToString();
            txtWarehouseName.SelectedItem.Text = DTItemDet.Rows[0]["WarehouseName"].ToString();
            txtWarehouseName_SelectedIndexChanged(sender, e);
            txtZoneName.Text = DTItemDet.Rows[0]["BinName"].ToString();
            txtBinName.Text = DTItemDet.Rows[0]["BinName"].ToString();

            //btnDept.Enabled = false;
            //btnWareHouse.Enabled = false;
        }
        else
        {
            //btnDept.Enabled = true;

            //btnWareHouse.Enabled = true;

            Load_Data_Empty_Dept();
            Load_Data_Empty_WareHouse();
            Load_Data_Empty_CostCenterName();
            Load_Data_Empty_BinName();
        }

        txtOrderQty.Text = DT.Rows[0]["OrderQty"].ToString();
        txtRate.Text = DT.Rows[0]["Rate"].ToString(); 
        txtBal.Text = DT.Rows[0]["Balance"].ToString();

        Total_Calculate();

        Final_Total_Calculate();
    }

    protected void txtTotPackAmt_TextChanged(object sender, EventArgs e)
    {
        Final_Total_Calculate();
    }

    protected void txtTotOther_TextChanged(object sender, EventArgs e)
    {
        Final_Total_Calculate();
    }
}

        
        