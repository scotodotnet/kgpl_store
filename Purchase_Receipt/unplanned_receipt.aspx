﻿<%@ Page Title="" Language="C#" MasterPageFile="../MainPage.master" AutoEventWireup="true" CodeFile="unplanned_receipt.aspx.cs" Inherits="Purchase_Receipt_unplanned_receipt" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("../assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("../assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>

<script type="text/javascript">
    function SaveMsgAlert(msg) {
        alert(msg);
    }
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });
                $('.js-states').select2();
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

<script>
  $(document).ready(function () {
       $('#Supplier1').dataTable();
   });
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Supplier1').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

<script>
 $(document).ready(function () {
   $('#WareHouse').dataTable();
 });
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#WareHouse').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

<script>
 $(document).ready(function () {
    $('#ItemTable').dataTable();
 });
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ItemTable').dataTable();               
            }
        });
    };
</script>



<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

<script>
 $(document).ready(function () {
    $('#ZoneName').dataTable();
 });
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#ZoneName').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

<script>
 $(document).ready(function () {
   $('#BinName').dataTable();
 });
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#BinName').dataTable();               
            }
        });
    };
</script>


<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

<script>
 $(document).ready(function () {
   $('#Dept').dataTable();
 });
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#Dept').dataTable();               
            }
        });
    };
</script>

<link href="<%= ResolveUrl("../assets/css/jquery.dataTables.css") %>" rel="stylesheet" type="text/css"/>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery.dataTables.js") %>'></script>
<script src='<%= ResolveUrl("../assets/js/datatable/jquery-1.10.2.min.js") %>'></script>

<script>
  $(document).ready(function () {
    $('#CostCenterName').dataTable();
  });
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#CostCenterName').dataTable();               
            }
        });
    };
</script>



<!--Ajax popup start-->
<link href="<%= ResolveUrl("../assets/css/Popup.css") %>" rel="stylesheet" type="text/css"/>
<script>
     function pageLoad(sender, args){
      if(!args.get_isPartialLoad()){
        //  add our handler to the document's
        //  keydown event
         $addHandler(document, "keydown", onKeyDown);
       }
  }

function onKeyDown(e){
if(e && e.keyCode == Sys.UI.Key.esc){
    // if the key pressed is the escape key, dismiss the dialog
     $find('Supp1_Close').hide();
     $find('Dept_Close').hide();
    $find('CostCenterName_Close').hide();
     $find('WareHouse_Close').hide();
     $find('Item_Close').hide();
     $find('ZoneName_Close').hide();
     $find('BinName_Close').hide();
     }
   } 
 </script>
<!--Ajax popup End-->



<div class="page-breadcrumb">
    <ol class="breadcrumb container">
       <h4><li class="active">Unplanned Receipt</li></h4> 
    </ol>
</div>
                
                        
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
     
    <div class="page-inner">
   
      <div class="col-md-12">
	   <div class="panel panel-white">
		<div class="panel panel-primary">
		  <div class="panel-heading clearfix">
			<h4 class="panel-title">Unplanned Receipt</h4>
		  </div>
		</div>
		<form class="form-horizontal">
		  <div class="panel-body">
			 <div class="col-md-12">
			  <div class="row">
				 <div class="form-group col-md-3">
					<label for="exampleInputName">Receipt No<span class="mandatory">*</span></label>
					<asp:Label ID="txtReceiptNo" runat="server" class="form-control"></asp:Label>
				 </div>
				 <div class="form-group col-md-2">
					<label for="exampleInputName">Date</label>
					<asp:TextBox ID="txtDate" MaxLength="20" class="form-control date-picker" runat="server" AutoComplete="Off"></asp:TextBox>
					<asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                    </asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                     TargetControlID="txtDate" ValidChars="0123456789./">
                    </cc1:FilteredTextBoxExtender>
				 </div>
				 <div class="form-group col-md-4">
				    <label for="exampleInputName">Supplier Name</label>
					<asp:DropDownList ID="ddlSupplierName" runat="server" class="js-states form-control"></asp:DropDownList>
					<asp:RequiredFieldValidator ControlToValidate="ddlSupplierName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" 
                        runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                    </asp:RequiredFieldValidator>
				 </div>
				 <div class="form-group col-md-3">
					<label for="exampleInputName">Invoice No</label>
					<asp:TextBox ID="txtInvoiceNo" class="form-control" runat="server"></asp:TextBox>
					<asp:RequiredFieldValidator ControlToValidate="txtInvoiceNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                    </asp:RequiredFieldValidator>
				 </div>
			  </div>
			 </div>
			
			 <div class="col-md-12">
				<div class="row">  
				 <asp:TextBox ID="txtDCNo" class="form-control" runat="server" Visible="false" ></asp:TextBox>
				<asp:TextBox ID="txtDCDate" MaxLength="20" class="form-control date-picker" Visible="false"  runat="server"></asp:TextBox>
				<div class="form-group col-md-2">
					            <label for="exampleInputName">Invoice Date</label>
				 <asp:TextBox ID="txtInvoiceDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
					            <%--<asp:RequiredFieldValidator ControlToValidate="txtGPINNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
				<div class="form-group col-md-2">
					            <label for="exampleInputName">GP IN No</label>
					            <asp:TextBox ID="txtGPINNo" class="form-control" runat="server"></asp:TextBox>
					            <%--<asp:RequiredFieldValidator ControlToValidate="txtGPINNo" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">GP IN Date</label>
					            <asp:TextBox ID="txtGPINDate" MaxLength="20" class="form-control date-picker" runat="server"></asp:TextBox>
					           <%-- <asp:RequiredFieldValidator ControlToValidate="txtGPINDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator17" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtGPINDate" ValidChars="0123456789./">
                                </cc1:FilteredTextBoxExtender>
					        </div>  
			<div class="form-group col-md-2">
				<label for="exampleInputName">Cost Center Name</label>
				<asp:DropDownList ID="txtCostCenterName" runat="server" class="js-states form-control">
                </asp:DropDownList>
				<asp:RequiredFieldValidator InitialValue="-Select-" Display="Dynamic" ControlToValidate="txtCostCenterName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                </asp:RequiredFieldValidator> 
			</div>
					         
			<div class="form-group col-md-4">
			    <label for="exampleInputName">Description</label>
			    <asp:TextBox ID="txtDescription" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
			</div>
		        </div>
			 </div>
					
					
					<div class="clearfix"></div>				    
				    <div class="timeline-options">
				        <h4 class="panel-title">Item Details</h4>
				    </div>
				    <div class="col-md-12">
					    <div class="row">
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Item Name</label>
					           
					            <asp:DropDownList ID="ddlItemName" runat="server" class="js-states form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlItemName_SelectedIndexChanged"></asp:DropDownList>
					            <asp:RequiredFieldValidator ControlToValidate="ddlItemName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Department Name</label>
					            <asp:DropDownList ID="txtDepartmentName" runat="server" class="js-states form-control">
                                </asp:DropDownList>
					            
					            <%--<asp:TextBox ID="txtDepartmentName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnDept" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnDept_Click"/>--%>
                                <%--<cc1:ModalPopupExtender ID="modalPop_Dept"  runat="server" PopupControlID="Panel6" TargetControlID="btnDept"
                                   CancelControlID="BtnClear_Dept" BackgroundCssClass="modalBackground" BehaviorID="Dept_Close">
                                </cc1:ModalPopupExtender>--%>
                   
                                <%--<asp:Panel ID="Panel6" runat="server" CssClass="modalPopup" style="display:none" >
                                    <div class="header">Department Details</div>
                                    <div class="body">
                                        <div class="col-md-12 headsize">
					                        <asp:Repeater ID="Repeater_Dept" runat="server" EnableViewState="false">
					                            <HeaderTemplate>
                                                     <table id="Dept" class="display table">
                                                     <thead >
                                                         <tr>
                                                         <th>DeptCode</th>
                                                         <th>DeptName</th>
                                                         <th>View</th>
                                                         </tr>
                                                     </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                  <tr>
                                                      <td><%# Eval("DeptCode")%></td>
                                                      <td><%# Eval("DeptName")%></td>
                                                                               
                                                      <td>
                                                      <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridViewClick_Dept" CommandArgument='<%# Eval("DeptCode")%>' CommandName='<%# Eval("DeptName")%>'>
                                                      </asp:LinkButton>
                                                      </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>                                
					                        </asp:Repeater>
					                    </div>
					                </div>
					                <div class="footer" align="right">
					                    <asp:Button ID="BtnClear_Dept" class="btn btn-rounded" runat="server" Text="Close"/>
					                </div>
					            </asp:Panel>--%>
					            
					            <%--<asp:HiddenField ID="txtDeptCodeHide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtDepartmentName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        
					        
					         <div class="form-group col-md-3">
					            <label for="exampleInputName">Warehouse Name</label>
					            <asp:DropDownList ID="txtWarehouseName" runat="server" 
                                    class="js-states form-control" AutoPostBack="true" 
                                    onselectedindexchanged="txtWarehouseName_SelectedIndexChanged">
                                </asp:DropDownList>
					            
					            
					            <%--<asp:TextBox ID="txtWarehouseName" class="form-control" runat="server" style="float: left;width: 80%;"></asp:TextBox>
					            <asp:Button ID="btnWareHouse" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnWareHouse_Click"/>
					            <cc1:ModalPopupExtender ID="modalPop_WareHouse"  runat="server" PopupControlID="Panel1" TargetControlID="btnWareHouse"
                       CancelControlID="BtnClear_WareHouse" BackgroundCssClass="modalBackground" BehaviorID="WareHouse_Close">
                    </cc1:ModalPopupExtender>--%>
                    
                    <%--<asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            WareHouse Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_WareHouse" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="WareHouse" class="display table">
                         <thead >
                         <tr>
                         <th>WarehouseCode</th>
                         <th>WarehouseName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("WarehouseCode")%></td>
                          <td><%# Eval("WarehouseName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditWareHouse" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_WareHouse" CommandArgument='<%# Eval("WarehouseCode")%>' CommandName='<%# Eval("WarehouseName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_WareHouse" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					           <%-- <asp:HiddenField ID="txtWarehouseCodeHide" runat="server" />
					            <asp:RequiredFieldValidator ControlToValidate="txtWarehouseName" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        
					        
					        <div class="form-group col-md-3" runat="server" visible="false">
					            <label for="exampleInputName">Zone Name</label>
					            <asp:DropDownList ID="txtZoneName" class="js-states form-control" runat="server"></asp:DropDownList>
					            <%--<asp:Button ID="btnZoneName" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnZoneName_Click"/>--%>
					          <%--<cc1:ModalPopupExtender ID="modalPop_ZoneName"  runat="server" PopupControlID="Panel4" TargetControlID="btnZoneName"
                       CancelControlID="BtnClear_ZoneName" BackgroundCssClass="modalBackground" BehaviorID="ZoneName_Close">
                    </cc1:ModalPopupExtender>--%>  
					            <%--<asp:Panel ID="Panel4" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            ZoneName Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_ZoneName" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="ZoneName" class="display table">
                         <thead >
                         <tr>
                         <th>ZoneName</th>
                      
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ZoneName")%></td>
                          <td>
                          <asp:LinkButton ID="btnEditZoneName" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_ZoneName" CommandArgument='<%# Eval("ZoneName")%>' CommandName='Edit'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_ZoneName" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            <%--<asp:RequiredFieldValidator ControlToValidate="txtZoneName" InitialValue="-Select-" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>--%>
					        </div>
					        <div class="form-group col-md-3">
					            <label for="exampleInputName">Bin Name</label>
					            <asp:DropDownList ID="txtBinName" class="js-states form-control" runat="server"></asp:DropDownList>
					             <%--<asp:Button ID="btnBinName" runat="server" Text="+" class="fa fa-plus" 
                                    style="float: left; padding: 9px;" onclick="btnBinName_Click"/>--%>
					             <%--<cc1:ModalPopupExtender ID="modalPop_BinName"  runat="server" PopupControlID="Panel2" TargetControlID="btnBinName"
                       CancelControlID="BtnClear_BinName" BackgroundCssClass="modalBackground" BehaviorID="BinName_Close">
                    </cc1:ModalPopupExtender>--%> 
					   <%--<asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            BinName Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_BinName" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="BinName" class="display table">
                         <thead >
                         <tr>
                         <th>BinName</th>
                      
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("BinName")%></td>
                          <td>
                          <asp:LinkButton ID="btnEditBinName" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick_BinName" CommandArgument='<%# Eval("BinName")%>' CommandName='Edit'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear_BinName" class="btn btn-rounded" runat="server" Text="Close"/>
					     </div>
					     </asp:Panel>--%>
					            
					            
					            <asp:RequiredFieldValidator ControlToValidate="txtBinName" InitialValue="-Select-" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
					        </div>
					        </div>
					        </div>
					        
					     <div class="col-md-12">
					    <div class="row">   
					        
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Recv. Qty</label>
					            <asp:TextBox ID="txtReceivedQty" class="form-control" runat="server"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtReceivedQty" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilTxtPin1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtReceivedQty" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Rate</label>
					            <asp:TextBox ID="txtRate" class="form-control" runat="server" Text="0.00" 
					            AutoPostBack="true" ontextchanged="txtRate_TextChanged"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtRate" ValidationGroup="Item_Validate_Field" class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtRate" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">PrevRate</label>
					            <asp:Label ID="txtPrevRate" class="form-control" runat="server" Text="0.00"></asp:Label>
					            
					        </div>
					        
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Item Total</label>
					            <asp:TextBox ID="txtItemTotal" runat="server" class="form-control" Text="0.0" 
                                    AutoPostBack="true" ontextchanged="txtItemTotal_TextChanged"></asp:TextBox>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Disc. Per</label>
					            <asp:TextBox ID="txtDiscount_Per" class="form-control" runat="server" Text="0.0" 
                                     AutoPostBack="true" ontextchanged="txtDiscount_Per_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDiscount_Per" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Discount Amount</label>
					            <asp:Label ID="txtDiscount_Amount" runat="server" class="form-control" Text="0.0"></asp:Label>
					         </div>
					         <div class="form-group col-md-2" runat="server" visible="false">
					            <label for="exampleInputName">Packing Amount</label>
					            <asp:TextBox ID="txtSPackingAmt" runat="server" class="form-control" Text="0.0" 
                                     AutoPostBack="true" ontextchanged="txtSPackingAmt_TextChanged"></asp:TextBox>
					         </div>
					        
					    </div>
					</div>
					<div class="col-md-12">
					    <div class="row">
					        
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">GST Type</label>
					            <asp:DropDownList ID="txtGST_Type" runat="server" class="form-control" AutoPostBack="true" 
                                    OnSelectedIndexChanged="txtGST_Type_SelectedIndexChanged">
                                <%--<asp:ListItem Value="SGST" Text="SGST"></asp:ListItem>
                                <asp:ListItem Value="IGST" Text="IGST"></asp:ListItem>--%>
                                </asp:DropDownList>
					          </div>
					          <div class="form-group col-md-1">
					            <label for="exampleInputName">CGST %</label>
					            <asp:TextBox ID="txtCGSTPer" class="form-control" runat="server" Text="0.0" Enabled="false" AutoPostBack="true" ontextchanged="txtCGSTPer_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtCGSTPer" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					         <div class="form-group col-md-2">
					            <label for="exampleInputName">CGST Amt</label>
					            <asp:TextBox ID="txtCGSTAmt" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
					         </div>
				          <div class="form-group col-md-1">
					            <label for="exampleInputName">SGST %</label>
					            <asp:TextBox ID="txtSGSTPer" class="form-control" runat="server" Text="0.0" 
                                    AutoPostBack="true" ontextchanged="txtSGSTPer_TextChanged" Enabled="false"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtSGSTPer" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
				                <label for="exampleInputName">SGST Amt</label>
				                <asp:TextBox ID="txtSGSTAmt" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
				            </div>
				           <div class="form-group col-md-1">
					            <label for="exampleInputName">IGST %</label>
					            <asp:TextBox ID="txtIGSTPer" class="form-control" runat="server" Text="0.0" 
                                    AutoPostBack="true" ontextchanged="txtIGSTPer_TextChanged" Enabled="false"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtSGSTPer" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
				                <label for="exampleInputName">IGST Amt</label>
				                <asp:TextBox ID="txtIGSTAmt" class="form-control" runat="server" Text="0.0" Enabled="false"></asp:TextBox>
				            </div>
				           <div class="form-group col-md-1">
					            <label for="exampleInputName">BDU Tax%</label>
					            <asp:TextBox ID="txtBDUTax" class="form-control" runat="server" Text="0.0" 
                                    AutoPostBack="true" ontextchanged="txtBDUTax_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtBDUTax" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					         
				         </div>
				         </div> 
				      
				      <div class="col-md-12">
					    <div class="row">    
					    <div class="form-group col-md-2">
					            <label for="exampleInputName">BDU Amt</label>
					            <asp:TextBox ID="txtBDUAmount" class="form-control" runat="server" Text="0.0"></asp:TextBox>
					            
					        </div>
					        <div class="form-group col-md-1">
					            <label for="exampleInputName">Tax Per</label>
					            <asp:TextBox ID="txtTax" class="form-control" runat="server" Text="0.0" 
                                     AutoPostBack="true" ontextchanged="txtTax_TextChanged"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTax" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        <div class="form-group col-md-2">
				                <label for="exampleInputName">Tax Amount</label>
				                <asp:Label ID="txtTaxAmt" runat="server" class="form-control" Text="0.0"></asp:Label>
				            </div>
					         <div class="form-group col-md-1">
					            <label for="exampleInputName">Other Amt</label>
					            <asp:TextBox ID="txtSOtherAmt" class="form-control" runat="server" 
                                     AutoPostBack="true" OnTextChanged="txtSOtherAmt_TextChanged" Text="0"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtSOtherAmt" ValidChars="0123456789.-">
                                </cc1:FilteredTextBoxExtender>
                                <%--<asp:HiddenField ID="txtFinal_Total_Amt" runat="server" />--%>
					        </div>
					        <div class="form-group col-md-2">
				                <label for="exampleInputName">Total Amt</label>
				                <asp:Label ID="txtFinal_Total_Amt" runat="server" class="form-control" Text="0.0"></asp:Label>
				            </div>
					        <div class="form-group col-md-1">
					            <br />
					            <asp:Button ID="btnAddItem" Width="50" Height="30" class="btn-success"  runat="server" Text="Add" ValidationGroup="Item_Validate_Field"  OnClick="btnAddItem_Click"/>
					        </div>
					     </div>
					     </div>  
                    
                    <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <%--<th>Item Code</th>--%>
                                                <th>Item Name</th>
                                                <%--<th>Value Type</th>--%>
                                                <th>UOM</th>
                                                <%--<th>Zone Name</th>
                                                <th>Bin Name</th>--%>
                                                <th>Recv. Qty</th>
                                                <th>Rate</th>
                                                <th>Item Total</th>
                                                <th>Dis.Per</th>
                                                <th>Dis.Amt</th>
                                                <th>Tax Per</th>
                                                <th>Tax Amt</th>
                                                <th>BDU Per</th>
                                                <th>BDU Amt</th>
                                                <th>Other Charge</th>
                                                <th>Total Amt</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <%--<td><%# Eval("ItemCode")%></td>--%>
                                        <td><%# Eval("ItemName")%></td>
                                        <%--<td><%# Eval("ValuationType")%></td>--%>
                                        <td><%# Eval("UOMCode")%></td>
                                       <%-- <td><%# Eval("ZoneName")%></td>
                                        <td><%# Eval("BinName")%></td>--%>
                                        <td><%# Eval("ReceivedQty")%></td>
                                        <td><%# Eval("ItemRate")%></td>
                                        <td><%# Eval("ItemTotal")%></td>
                                        <td><%# Eval("Discount_Per")%></td>
                                        <td><%# Eval("Discount")%></td>
                                        <td><%# Eval("TaxPer")%></td>
                                        <td><%# Eval("TaxAmount")%></td>
                                         <td><%# Eval("BDUTaxPer")%></td>
                                        <td><%# Eval("BDUTaxAmount")%></td>
                                        <td><%# Eval("OtherCharge")%></td>
                                        <td><%# Eval("LineTotal")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument='Delete' CommandName='<%# Eval("ItemCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
					
					
					
					<div class="col-md-12">
					    <div class="row">
					     <div class="form-group col-md-10"></div>
					      <div class="form-group col-md-2">
					            <label for="exampleInputName">Total Amt</label>
					            <asp:TextBox ID="txtTotAmt" class="form-control" runat="server" Text="0.0"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtTotAmt" ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					    </div>
					    </div>
					
					
					<div class="col-md-12">
					    <div class="row">
					    
                            <div class="form-group col-md-4">
					            <%--<label for="exampleInputName">Discount</label>--%>
					            <asp:TextBox ID="txtDiscount" class="form-control" runat="server" 
                                    ontextchanged="txtDiscount_TextChanged" Text="0" AutoPostBack="true" Visible="false"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtDiscount" ValidChars="0123456789.-">
                                </cc1:FilteredTextBoxExtender>
					        </div>

                            <div class="form-group col-md-2">
					            <label for="exampleInputName">Other Amount</label>
					            <asp:TextBox ID="txtOtherChr" class="form-control" runat="server" 
                                    ontextchanged="txtOtherChr_TextChanged" Text="0" AutoPostBack="true"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtOtherChr" ValidChars="0123456789.-">
                                </cc1:FilteredTextBoxExtender>
					        </div>

                            <div class="form-group col-md-2">
					            <label for="exampleInputName">Packing Amount</label>
					            <asp:TextBox ID="txtPackAmt" class="form-control" runat="server" 
                                    ontextchanged="txtPackAmt_TextChanged" Text="0" AutoPostBack="true"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtPackAmt" ValidChars="0123456789.-">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					       
					        
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Add or Less</label>
					            <asp:TextBox ID="txtAddOrLess" class="form-control" runat="server" 
                                    ontextchanged="txtAddOrLess_TextChanged" Text="0" AutoPostBack="true"></asp:TextBox>
					            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtAddOrLess" ValidChars="0123456789.-">
                                </cc1:FilteredTextBoxExtender>
					        </div>
					        
					        <div class="form-group col-md-2">
					            <label for="exampleInputName">Net Amt</label>
					            <asp:Label ID="txtNetAmt" runat="server" class="form-control"></asp:Label>
					           
					        </div>
					        </div>
					</div>
					
					
					
					<div class="form-group row"></div>	
                    <!-- Button start -->
                    <div class="txtcenter">
                        <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                        <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                        <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                        <asp:Button ID="btnBackEnquiry" class="btn btn-success" runat="server" Text="Back" OnClick="btnBackRequest_Click"/>
                        <asp:Button ID="btnApprove" class="btn btn-success" runat="server" Text="Approve" OnClick="btnApprove_Click"/>
                    </div>
                    <!-- Button end -->
                  </div>      
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 </div>

 
</asp:Content>

