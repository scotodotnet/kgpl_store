﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="PopUPWork.aspx.cs" Inherits="PopUPWork" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
  </script>
    
<!--Ajax popup start-->
<style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            z-index: 6000 !important;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            width: 600px;
            border: 3px solid #6C2C86;
            border-radius: 12px;
            padding: 0;
            z-index: 6001 !important;
        }
        .modalPopup .header
        {
            background-color: #6C2C86;
            height: 30px;
            color: White;
            line-height: 30px;
            text-align: center;
            font-weight: bold;
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;
        }
        .modalPopup .body
        {
            min-height: 50px;
            line-height: 30px;
            text-align: center;
            font-weight: bold;
        }
        .modalPopup .footer
        {
            padding: 6px;
            width:500px;
        }
        .modalPopup .yes, .modalPopup .no
        {
            height: 23px;
            color: White;
            line-height: 23px;
            text-align: center;
            font-weight: bold;
            cursor: pointer;
            border-radius: 4px;
        }
        .modalPopup .yes
        {
            background-color: #6C2C86;
            border: 1px solid #6C2C86;
        }
        .modalPopup .no
        {
            background-color: #9F9F9F;
            border: 1px solid #5C5C5C;
        }
        .Size
        {
        width:70px;
        height:27px;	
        }
        .form_error
        {
        	height:10px;
        	margin-top:0px;
        }
        .headsize
        {
        	margin-top:15px;
        	width:500px;
        }
    </style>

<!--Ajax popup End-->

    
    
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate> 
                                    
                                    
                 <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Department Details</li>
                       <h4>
                       </h4>
                        <h4>
                       </h4>
                        </h4> 
                    </ol>
                </div>
                 
                 
                 <div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
            <div class="col-md-9">
			<div class="panel panel-white">
			    <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Department</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
           
                        <form class="form-horizontal">
                          <div class="panel-body">
                             <div class="col-md-20">
                                <div class="row">
                                
                                
                                <div class="col-md-12">
					    <div class="row">
					        
					       <div class="form-group col-md-4">
					       <label for="exampleInputName">ItemCode</label>
				    <asp:TextBox ID="txtItemCode" class="form-control" runat="server"></asp:TextBox>
				    </div>
				     <div class="form-group col-md-4">
				     <label for="exampleInputName">ItemName</label>
                    <asp:TextBox ID="txtItemName" class="form-control" runat="server"></asp:TextBox>
                    </div>
          
                    
				    <div class="form-group row">
				    <asp:Button ID="btnDept" class="btn btn-primary btn-rounded" runat="server" 
                       Text="ADD" onclick="btnDept_Click"/>
                       </div>
                       </div>
                       </div>
                    <cc1:ModalPopupExtender ID="modalPop"  runat="server" PopupControlID="Panl1" TargetControlID="btnDept"
                       CancelControlID="BtnClear" BackgroundCssClass="modalBackground">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="Panl1" runat="server" CssClass="modalPopup" style="display:none" >
                    <div class="header">
                                            Item Details
                                          </div>
                                          <div class="body">
				     <div class="col-md-12 headsize">
					
					   <asp:Repeater ID="Repeater_StdPurOrder" runat="server" EnableViewState="false">
					    <HeaderTemplate>
                         <table id="example" class="display table">
                         <thead >
                         <tr>
                         <th>ItemCode</th>
                         <th>ItemName</th>
                         <th>View</th>
                         </tr>
                         </thead>
                         </HeaderTemplate>
                         <ItemTemplate>
                          <tr>
                          <td><%# Eval("ItemCode")%></td>
                          <td><%# Eval("ItemName")%></td>
                                                                           
                          <td>
                          <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                            Text="" OnCommand="GridViewClick" CommandArgument='<%# Eval("ItemCode")%>' CommandName='<%# Eval("ItemName")%>'>
                          </asp:LinkButton>
                          </td>
                          </tr>
                          </ItemTemplate>
                         <FooterTemplate></table></FooterTemplate>                                
					     </asp:Repeater>
					     </div>
					     </div>
					     <div class="footer" align="right">
					     <asp:Button ID="BtnClear" class="btn btn-rounded" runat="server" Text="Close" />
					     </div>
					     </asp:Panel>
                      
                         </div>
                         </div>
						 </div>
					
						 </form>
						 </div>
					     </form>
						 </div><!-- panel white end -->
		    
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
                       <!-- Dashboard start -->
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        
                       
                        <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
      </div>        	                 
						            </ContentTemplate>
                             </asp:UpdatePanel>   


</asp:Content>